.class Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;
.super Lcom/samsung/android/sdk/dualscreen/SDualScreenReflector;
.source "SDualScreenManagerReflector.java"


# static fields
.field private static final DEBUG:Z = true

.field static DUALSCREEN_ENUM_FIELD_NAMES:[Ljava/lang/String;

.field static ORDINALS:[I

.field private static final TAG:Ljava/lang/String;

.field private static sKlassCoupledTaskInfo:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static sKlassDualScreen:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static sKlassDualScreenManager:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field static sMethodEnumOrdinal:Ljava/lang/reflect/Method;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInitialized:Z

.field private mInstanceActivity:Ljava/lang/Object;

.field private mInstanceDualScreenManager:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    const-class v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    .line 53
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "MAIN"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "SUB"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "EXPANDED"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "UNKNOWN"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->DUALSCREEN_ENUM_FIELD_NAMES:[Ljava/lang/String;

    .line 60
    sget-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->DUALSCREEN_ENUM_FIELD_NAMES:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->ORDINALS:[I

    .line 63
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->loadKlass()V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 69
    invoke-direct {p0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenReflector;-><init>()V

    .line 51
    iput-boolean v5, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mInitialized:Z

    .line 70
    if-nez p1, :cond_0

    .line 71
    new-instance v5, Ljava/lang/NullPointerException;

    const-string v6, "context is null"

    invoke-direct {v5, v6}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 75
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->loadKlass()V

    .line 78
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/content/Context;

    aput-object v8, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v3

    .line 82
    .local v3, "dualScreenManagerConstructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v3, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mInstanceDualScreenManager:Ljava/lang/Object;

    .line 84
    iput-object p1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mContext:Landroid/content/Context;

    .line 86
    instance-of v5, p1, Landroid/app/Activity;

    if-eqz v5, :cond_1

    .line 87
    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    move-object v1, v0

    .line 88
    .local v1, "activity":Landroid/app/Activity;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 89
    .local v2, "activityClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mInstanceActivity:Ljava/lang/Object;

    .line 90
    const-string v5, "setScreenChangedListner"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Lcom/samsung/android/sdk/dualscreen/SDualScreenListener$ScreenChangeListener;

    aput-object v8, v6, v7

    invoke-static {v2, v5, v6}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 96
    .end local v1    # "activity":Landroid/app/Activity;
    .end local v2    # "activityClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mInitialized:Z

    .line 97
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v6, "completely initialized"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    .line 114
    .end local v3    # "dualScreenManagerConstructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :goto_0
    return-void

    .line 98
    :catch_0
    move-exception v4

    .line 99
    .local v4, "e":Ljava/lang/NoSuchMethodException;
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v6, "NoSuchMethodException !"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-virtual {v4}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 101
    .end local v4    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v4

    .line 102
    .local v4, "e":Ljava/lang/InstantiationException;
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v6, "InstantiationException !"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    invoke-virtual {v4}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 104
    .end local v4    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v4

    .line 105
    .local v4, "e":Ljava/lang/IllegalAccessException;
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v6, "IllegalAccessException !"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    invoke-virtual {v4}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 107
    .end local v4    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v4

    .line 108
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v6, "IllegalArgumentException !"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {v4}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 110
    .end local v4    # "e":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v4

    .line 111
    .local v4, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "InvocationTargetException ! cause="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    invoke-virtual {v4}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method static checkVersion()V
    .locals 3

    .prologue
    .line 567
    const/4 v0, 0x3

    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->getMinimumSdkVersionCode()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 568
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Application using SDK version is old (version 0.3). The device requires (version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->getMinimumSdkVersionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") at least"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572
    :cond_0
    return-void
.end method

.method static convertToNativeDualScreenEnum(Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;)Ljava/lang/Object;
    .locals 2
    .param p0, "screen"    # Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    .prologue
    .line 257
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->loadKlass()V

    .line 258
    if-eqz p0, :cond_0

    .line 259
    sget-object v0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreen:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;->targetOrdinal:I

    aget-object v0, v0, v1

    .line 261
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static convertToSdkCoupledTaskInfo(Ljava/lang/Object;)Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;
    .locals 10
    .param p0, "coupledTaskInfo"    # Ljava/lang/Object;

    .prologue
    const/4 v9, 0x0

    .line 287
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->loadKlass()V

    .line 288
    const/4 v5, -0x1

    .line 289
    .local v5, "taskId":I
    const/4 v4, 0x0

    .line 291
    .local v4, "sdualScreenCoupledTaskInfo":Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;
    const-string v6, "getTaskId()"

    invoke-static {v6}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 292
    const-string v6, "getTaskId()"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v6, p0, v7}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 293
    if-lez v5, :cond_0

    .line 294
    sget-object v6, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "convertToSdkCoupledTaskInfo() : taskId="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    new-instance v4, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;

    .end local v4    # "sdualScreenCoupledTaskInfo":Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;
    invoke-direct {v4, v5}, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;-><init>(I)V

    .line 296
    .restart local v4    # "sdualScreenCoupledTaskInfo":Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;
    const/4 v1, 0x0

    .line 297
    .local v1, "coupledTaskMode":I
    const/4 v3, 0x0

    .line 298
    .local v3, "parentTaskId":I
    const/4 v0, 0x0

    .line 299
    .local v0, "childTaskId":I
    const/4 v2, 0x0

    .line 301
    .local v2, "isCoupled":Z
    const-string v6, "getCoupledTaskMode()"

    invoke-static {v6}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 302
    const-string v6, "getCoupledTaskMode()"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v6, p0, v7}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 307
    :goto_0
    const-string v6, "getParentCoupledTaskId()"

    invoke-static {v6}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 308
    const-string v6, "getParentCoupledTaskId()"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v6, p0, v7}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 313
    :goto_1
    const-string v6, "getChildCoupledTaskId()"

    invoke-static {v6}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 314
    const-string v6, "getChildCoupledTaskId()"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v6, p0, v7}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 319
    :goto_2
    const-string v6, "isCoupled()"

    invoke-static {v6}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 320
    const-string v6, "isCoupled()"

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v6, p0, v7}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 325
    :goto_3
    invoke-virtual {v4, v1}, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->setCoupledTaskMode(I)V

    .line 326
    invoke-virtual {v4, v3}, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->setParentCoupledTaskId(I)V

    .line 327
    invoke-virtual {v4, v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->setChildCoupledTaskId(I)V

    .line 328
    invoke-virtual {v4, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;->setCoupledState(Z)V

    .line 333
    .end local v0    # "childTaskId":I
    .end local v1    # "coupledTaskMode":I
    .end local v2    # "isCoupled":Z
    .end local v3    # "parentTaskId":I
    :cond_0
    :goto_4
    return-object v4

    .line 304
    .restart local v0    # "childTaskId":I
    .restart local v1    # "coupledTaskMode":I
    .restart local v2    # "isCoupled":Z
    .restart local v3    # "parentTaskId":I
    :cond_1
    sget-object v6, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v7, "getCoupledTaskMode() is not loaded"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 310
    :cond_2
    sget-object v6, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v7, "getParentCoupledTaskId() is not loaded"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 316
    :cond_3
    sget-object v6, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v7, "getParentCoupledTaskId() is not loaded"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 322
    :cond_4
    sget-object v6, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v7, "isCoupled() is not loaded"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 331
    .end local v0    # "childTaskId":I
    .end local v1    # "coupledTaskMode":I
    .end local v2    # "isCoupled":Z
    .end local v3    # "parentTaskId":I
    :cond_5
    sget-object v6, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v7, "getTaskId() is not loaded"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4
.end method

.method static convertToSdkDualScreenEnum(Ljava/lang/Object;)Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    .locals 8
    .param p0, "screen"    # Ljava/lang/Object;

    .prologue
    .line 265
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->loadKlass()V

    .line 266
    if-eqz p0, :cond_1

    .line 267
    const/4 v5, -0x1

    .line 269
    .local v5, "ordinal":I
    :try_start_0
    sget-object v6, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sMethodEnumOrdinal:Ljava/lang/reflect/Method;

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v6, p0, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v5

    .line 277
    :goto_0
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;->values()[Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 278
    .local v4, "o":Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    iget v6, v4, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;->targetOrdinal:I

    if-ne v6, v5, :cond_0

    .line 283
    .end local v0    # "arr$":[Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "o":Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    .end local v5    # "ordinal":I
    :goto_2
    return-object v4

    .line 270
    .restart local v5    # "ordinal":I
    :catch_0
    move-exception v1

    .line 271
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 272
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_1
    move-exception v1

    .line 273
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 274
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v1

    .line 275
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    .line 277
    .end local v1    # "e":Ljava/lang/reflect/InvocationTargetException;
    .restart local v0    # "arr$":[Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "o":Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 283
    .end local v0    # "arr$":[Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "o":Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    .end local v5    # "ordinal":I
    :cond_1
    sget-object v4, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;->UNKNOWN:Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    goto :goto_2
.end method

.method static getFocusedScreen()Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 460
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v4, "getFocusedScreen()"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    const-string v0, "getFocusedScreen()"

    .line 462
    .local v0, "methodName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 464
    .local v1, "ret":Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 465
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->loadKlass()V

    .line 466
    sget-object v4, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    const-string v5, "getFocusedScreen"

    move-object v2, v3

    check-cast v2, [Ljava/lang/Class;

    invoke-static {v4, v5, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 470
    :cond_0
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 471
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 476
    .end local v1    # "ret":Ljava/lang/Object;
    :goto_0
    if-eqz v1, :cond_2

    .line 477
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFocusedScreen() ret="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    invoke-static {v1}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->convertToSdkDualScreenEnum(Ljava/lang/Object;)Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    move-result-object v2

    .line 480
    :goto_1
    return-object v2

    .line 473
    .restart local v1    # "ret":Ljava/lang/Object;
    :cond_1
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v3, "getFocusedScreen() is not loaded"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 480
    .end local v1    # "ret":Ljava/lang/Object;
    :cond_2
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;->UNKNOWN:Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    goto :goto_1
.end method

.method static getMinimumSdkVersionCode()I
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 527
    const-string v0, "getMinimumSdkVersionCode()"

    .line 528
    .local v0, "methodName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 530
    .local v1, "ret":Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 531
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->loadKlass()V

    .line 532
    sget-object v4, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    const-string v5, "getMinimumSdkVersionCode"

    move-object v2, v3

    check-cast v2, [Ljava/lang/Class;

    invoke-static {v4, v5, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 536
    :cond_0
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 537
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 539
    .end local v1    # "ret":Ljava/lang/Object;
    :cond_1
    if-eqz v1, :cond_2

    .line 540
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMinimumSdkVersionCode() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 543
    :goto_0
    return v2

    :cond_2
    const/4 v2, -0x1

    goto :goto_0
.end method

.method static getMinimumSdkVersionName()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 547
    const-string v0, "getMinimumSdkVersionName()"

    .line 548
    .local v0, "methodName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 550
    .local v1, "ret":Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 551
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->loadKlass()V

    .line 552
    sget-object v4, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    const-string v5, "getMinimumSdkVersionName"

    move-object v2, v3

    check-cast v2, [Ljava/lang/Class;

    invoke-static {v4, v5, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 556
    :cond_0
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 557
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 559
    .end local v1    # "ret":Ljava/lang/Object;
    :cond_1
    if-eqz v1, :cond_2

    .line 560
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMinimumSdkVersionName() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    check-cast v1, Ljava/lang/String;

    .line 563
    :goto_0
    return-object v1

    :cond_2
    const-string v1, "UNKNOWN"

    goto :goto_0
.end method

.method static getVersionCode()I
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 484
    const-string v0, "getVersionCode()"

    .line 485
    .local v0, "methodName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 487
    .local v1, "ret":Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 488
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->loadKlass()V

    .line 489
    sget-object v4, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    const-string v5, "getVersionCode"

    move-object v2, v3

    check-cast v2, [Ljava/lang/Class;

    invoke-static {v4, v5, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 493
    :cond_0
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 494
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 499
    .end local v1    # "ret":Ljava/lang/Object;
    :goto_0
    if-eqz v1, :cond_2

    .line 500
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVersionCode() ret="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 503
    :goto_1
    return v2

    .line 496
    .restart local v1    # "ret":Ljava/lang/Object;
    :cond_1
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v3, "getVersionCode() is not loaded"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 503
    .end local v1    # "ret":Ljava/lang/Object;
    :cond_2
    const/4 v2, -0x1

    goto :goto_1
.end method

.method static getVersionName()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 507
    const-string v0, "getVersionName()"

    .line 508
    .local v0, "methodName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 510
    .local v1, "ret":Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 511
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->loadKlass()V

    .line 512
    sget-object v4, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    const-string v5, "getVersionName"

    move-object v2, v3

    check-cast v2, [Ljava/lang/Class;

    invoke-static {v4, v5, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 516
    :cond_0
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 517
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 519
    .end local v1    # "ret":Ljava/lang/Object;
    :cond_1
    if-eqz v1, :cond_2

    .line 520
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVersionName() ret="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    check-cast v1, Ljava/lang/String;

    .line 523
    :goto_0
    return-object v1

    :cond_2
    const-string v1, "UNKNOWN"

    goto :goto_0
.end method

.method static loadDualScreenEnumKlass()V
    .locals 22

    .prologue
    .line 186
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreen:Ljava/lang/Class;

    if-nez v17, :cond_3

    .line 188
    :try_start_0
    const-string v17, "com.samsung.android.dualscreen.DualScreen"

    invoke-static/range {v17 .. v17}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v17

    sput-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreen:Ljava/lang/Class;

    .line 189
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreen:Ljava/lang/Class;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Class;->isEnum()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 190
    sget-object v17, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v18, "Enum name:  %s%nEnum constants:  %s%n"

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    sget-object v21, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreen:Ljava/lang/Class;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v19, v20

    const/16 v20, 0x1

    sget-object v21, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreen:Ljava/lang/Class;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v21

    aput-object v21, v19, v20

    invoke-static/range {v17 .. v19}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 193
    .local v13, "msg":Ljava/lang/String;
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-static {v0, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :cond_0
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreen:Ljava/lang/Class;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v4

    .local v4, "arr$":[Ljava/lang/reflect/Method;
    array-length v11, v4

    .local v11, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_0
    if-ge v9, v11, :cond_1

    aget-object v12, v4, v9

    .line 198
    .local v12, "m":Ljava/lang/reflect/Method;
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "DualScreen : available method => "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 201
    .end local v12    # "m":Ljava/lang/reflect/Method;
    :cond_1
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreen:Ljava/lang/Class;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v4

    .local v4, "arr$":[Ljava/lang/reflect/Field;
    array-length v11, v4

    const/4 v9, 0x0

    :goto_1
    if-ge v9, v11, :cond_5

    aget-object v7, v4, v9

    .line 202
    .local v7, "f":Ljava/lang/reflect/Field;
    invoke-virtual {v7}, Ljava/lang/reflect/Field;->isEnumConstant()Z

    move-result v17

    if-eqz v17, :cond_2

    .line 203
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "DualScreen : available fields => "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " (enum constant)"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 206
    :cond_2
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "DualScreen : available fields => "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_2

    .line 234
    .end local v4    # "arr$":[Ljava/lang/reflect/Field;
    .end local v7    # "f":Ljava/lang/reflect/Field;
    .end local v9    # "i$":I
    .end local v11    # "len$":I
    :catch_0
    move-exception v6

    .line 235
    .local v6, "e":Ljava/lang/ClassNotFoundException;
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v18, "loadDualScreenEnums : ClassNotFoundException !"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    invoke-virtual {v6}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    .line 245
    .end local v6    # "e":Ljava/lang/ClassNotFoundException;
    :cond_3
    :goto_3
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sMethodEnumOrdinal:Ljava/lang/reflect/Method;

    if-nez v17, :cond_4

    .line 247
    :try_start_1
    sget-object v18, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreen:Ljava/lang/Class;

    const-string v19, "ordinal"

    const/16 v17, 0x0

    check-cast v17, [Ljava/lang/Class;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v17

    sput-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sMethodEnumOrdinal:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4

    .line 254
    :cond_4
    :goto_4
    return-void

    .line 211
    .restart local v4    # "arr$":[Ljava/lang/reflect/Field;
    .restart local v9    # "i$":I
    .restart local v11    # "len$":I
    :cond_5
    :try_start_2
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->DUALSCREEN_ENUM_FIELD_NAMES:[Ljava/lang/String;

    move-object/from16 v0, v17

    array-length v3, v0

    .line 212
    .local v3, "N":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_5
    if-ge v8, v3, :cond_3

    .line 213
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreen:Ljava/lang/Class;

    sget-object v18, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->DUALSCREEN_ENUM_FIELD_NAMES:[Ljava/lang/String;

    aget-object v18, v18, v8

    invoke-virtual/range {v17 .. v18}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v16

    .line 215
    .local v16, "src":Ljava/lang/reflect/Field;
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v18, "loadDualScreenEnums() : ==================================="

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "loadDualScreenEnums() : DUALSCREEN_ENUM_FIELD_NAMES["

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "]="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreen:Ljava/lang/Class;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    array-length v5, v0

    .line 221
    .local v5, "constansLenth":I
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_6
    if-ge v10, v5, :cond_7

    .line 222
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreen:Ljava/lang/Class;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v17

    aget-object v14, v17, v10

    .line 223
    .local v14, "o":Ljava/lang/Object;
    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v17

    sget-object v18, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->DUALSCREEN_ENUM_FIELD_NAMES:[Ljava/lang/String;

    aget-object v18, v18, v8

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    .line 225
    .local v15, "same":Z
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "loadDualScreenEnums() : o="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", same="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " index="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    if-eqz v15, :cond_6

    .line 230
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->ORDINALS:[I

    aput v10, v17, v8
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    .line 221
    :cond_6
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 212
    .end local v14    # "o":Ljava/lang/Object;
    .end local v15    # "same":Z
    :cond_7
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_5

    .line 237
    .end local v3    # "N":I
    .end local v4    # "arr$":[Ljava/lang/reflect/Field;
    .end local v5    # "constansLenth":I
    .end local v8    # "i":I
    .end local v9    # "i$":I
    .end local v10    # "j":I
    .end local v11    # "len$":I
    .end local v16    # "src":Ljava/lang/reflect/Field;
    :catch_1
    move-exception v6

    .line 238
    .local v6, "e":Ljava/lang/NoSuchFieldException;
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v18, "loadDualScreenEnums : NoSuchFieldException !"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    invoke-virtual {v6}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto/16 :goto_3

    .line 240
    .end local v6    # "e":Ljava/lang/NoSuchFieldException;
    :catch_2
    move-exception v6

    .line 241
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    sget-object v17, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v18, "loadDualScreenEnums : IllegalArgumentException !"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_3

    .line 248
    .end local v6    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v6

    .line 249
    .local v6, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v6}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto/16 :goto_4

    .line 250
    .end local v6    # "e":Ljava/lang/NoSuchMethodException;
    :catch_4
    move-exception v6

    .line 251
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_4
.end method

.method static loadKlass()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 118
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    if-nez v5, :cond_1

    .line 120
    :try_start_0
    const-string v5, "com.samsung.android.dualscreen.DualScreenManager"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    sput-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    .line 122
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    invoke-virtual {v5}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/reflect/Method;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 124
    .local v4, "m":Ljava/lang/reflect/Method;
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DualScreenManager : available method => "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 126
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "m":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    .line 127
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    .line 130
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    :cond_0
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v7, "loadKlass() : DualScreen SDK version (0.3)"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "loadKlass() : Device supports DualScreen version ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->getVersionName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "loadKlass() : Device supports Minimum SDK version ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->getMinimumSdkVersionName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    sget-object v7, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    const-string v8, "getScreen"

    move-object v5, v6

    check-cast v5, [Ljava/lang/Class;

    invoke-static {v7, v8, v5}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 141
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    const-string v7, "getScreen"

    new-array v8, v11, [Ljava/lang/Class;

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v8, v10

    invoke-static {v5, v7, v8}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 145
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    const-string v7, "moveToScreen"

    new-array v8, v11, [Ljava/lang/Class;

    sget-object v9, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreen:Ljava/lang/Class;

    aput-object v9, v8, v10

    invoke-static {v5, v7, v8}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 149
    sget-object v7, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    const-string v8, "switchScreen"

    move-object v5, v6

    check-cast v5, [Ljava/lang/Class;

    invoke-static {v7, v8, v5}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 151
    sget-object v7, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    const-string v8, "swapTopTask"

    move-object v5, v6

    check-cast v5, [Ljava/lang/Class;

    invoke-static {v7, v8, v5}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 153
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    const-string v7, "getCoupledTaskInfo"

    check-cast v6, [Ljava/lang/Class;

    invoke-static {v5, v7, v6}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 156
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkVersion()V

    .line 158
    :cond_1
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->loadDualScreenEnumKlass()V

    .line 159
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassCoupledTaskInfo:Ljava/lang/Class;

    if-nez v5, :cond_3

    .line 161
    :try_start_1
    const-string v5, "com.samsung.android.dualscreen.CoupledTaskInfo"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    sput-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassCoupledTaskInfo:Ljava/lang/Class;

    .line 163
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassCoupledTaskInfo:Ljava/lang/Class;

    invoke-virtual {v5}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    array-length v3, v0

    .restart local v3    # "len$":I
    const/4 v2, 0x0

    .restart local v2    # "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 165
    .restart local v4    # "m":Ljava/lang/reflect/Method;
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CoupledTaskInfo : available method => "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 168
    .end local v4    # "m":Ljava/lang/reflect/Method;
    :cond_2
    sget-object v6, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassCoupledTaskInfo:Ljava/lang/Class;

    const-string v7, "getTaskId"

    const/4 v5, 0x0

    check-cast v5, [Ljava/lang/Class;

    invoke-static {v6, v7, v5}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 170
    sget-object v6, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassCoupledTaskInfo:Ljava/lang/Class;

    const-string v7, "getCoupledTaskMode"

    const/4 v5, 0x0

    check-cast v5, [Ljava/lang/Class;

    invoke-static {v6, v7, v5}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 172
    sget-object v6, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassCoupledTaskInfo:Ljava/lang/Class;

    const-string v7, "getParentCoupledTaskId"

    const/4 v5, 0x0

    check-cast v5, [Ljava/lang/Class;

    invoke-static {v6, v7, v5}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 174
    sget-object v6, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassCoupledTaskInfo:Ljava/lang/Class;

    const-string v7, "getChildCoupledTaskId"

    const/4 v5, 0x0

    check-cast v5, [Ljava/lang/Class;

    invoke-static {v6, v7, v5}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 176
    sget-object v6, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassCoupledTaskInfo:Ljava/lang/Class;

    const-string v7, "isCoupled"

    const/4 v5, 0x0

    check-cast v5, [Ljava/lang/Class;

    invoke-static {v6, v7, v5}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 183
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_3
    :goto_2
    return-void

    .line 179
    :catch_1
    move-exception v1

    .line 180
    .restart local v1    # "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_2
.end method

.method static makeIntent(Landroid/content/Context;Landroid/content/Intent;Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;I)Landroid/content/Intent;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "targetScreen"    # Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    .param p3, "flags"    # I

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 435
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v3, "makeIntent()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    const-string v0, "makeIntent(Context,Intent,DualScreen,int)"

    .line 438
    .local v0, "methodName":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 439
    invoke-static {}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->loadKlass()V

    .line 440
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreenManager:Ljava/lang/Class;

    const-string v3, "makeIntent"

    new-array v4, v10, [Ljava/lang/Class;

    const-class v5, Landroid/content/Context;

    aput-object v5, v4, v6

    const-class v5, Landroid/content/Intent;

    aput-object v5, v4, v7

    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->sKlassDualScreen:Ljava/lang/Class;

    aput-object v5, v4, v8

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v4, v9

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->putMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 449
    :cond_0
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 450
    invoke-static {p2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->convertToNativeDualScreenEnum(Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;)Ljava/lang/Object;

    move-result-object v1

    .line 451
    .local v1, "nativeEnum":Ljava/lang/Object;
    const/4 v2, 0x0

    new-array v3, v10, [Ljava/lang/Object;

    aput-object p0, v3, v6

    aput-object p1, v3, v7

    aput-object v1, v3, v8

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 456
    .end local v1    # "nativeEnum":Ljava/lang/Object;
    :goto_0
    return-object p1

    .line 453
    :cond_1
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v3, "makeIntent(Context,Intent,DualScreen,int) is not loaded"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method getCoupledTaskInfo()Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 405
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v6, "getCoupledTaskInfo()"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    const-string v1, "getCoupledTaskInfo()"

    .line 407
    .local v1, "methodName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 409
    .local v2, "ret":Ljava/lang/Object;
    invoke-static {v1}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 410
    iget-object v5, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mInstanceDualScreenManager:Ljava/lang/Object;

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v1, v5, v6}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 416
    if-eqz v2, :cond_2

    .line 417
    sget-object v4, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getCoupledTaskInfo() ret="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    invoke-static {v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->convertToSdkCoupledTaskInfo(Ljava/lang/Object;)Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;

    move-result-object v4

    .line 428
    .end local v2    # "ret":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v4

    .line 412
    .restart local v2    # "ret":Ljava/lang/Object;
    :cond_1
    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v6, "getCoupledTaskInfo() is not loaded"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 422
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mContext:Landroid/content/Context;

    instance-of v5, v5, Landroid/app/Activity;

    if-eqz v5, :cond_0

    .line 423
    iget-object v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 424
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getTaskId()I

    move-result v3

    .line 425
    .local v3, "taskId":I
    new-instance v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;

    .end local v2    # "ret":Ljava/lang/Object;
    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;-><init>(I)V

    .local v2, "ret":Lcom/samsung/android/sdk/dualscreen/SDualScreenCoupledTaskInfo;
    goto :goto_0
.end method

.method getScreen()Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    .locals 5

    .prologue
    .line 343
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v3, "getScreen()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    const/4 v1, 0x0

    .line 345
    .local v1, "ret":Ljava/lang/Object;
    const-string v0, "getScreen()"

    .line 346
    .local v0, "methodName":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 347
    iget-object v2, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mInstanceDualScreenManager:Ljava/lang/Object;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 349
    .end local v1    # "ret":Ljava/lang/Object;
    :cond_0
    if-eqz v1, :cond_1

    .line 350
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getScreen() ret="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    invoke-static {v1}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->convertToSdkDualScreenEnum(Ljava/lang/Object;)Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    move-result-object v2

    .line 353
    :goto_0
    return-object v2

    :cond_1
    sget-object v2, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;->UNKNOWN:Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    goto :goto_0
.end method

.method getScreen(I)Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;
    .locals 5
    .param p1, "taskId"    # I

    .prologue
    .line 357
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getScreen() : taskId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    const-string v0, "getScreen(int)"

    .line 359
    .local v0, "methodName":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360
    iget-object v1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mInstanceDualScreenManager:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    .line 362
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;->UNKNOWN:Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    goto :goto_0
.end method

.method public initialized()Z
    .locals 1

    .prologue
    .line 337
    iget-boolean v0, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mInitialized:Z

    return v0
.end method

.method moveToScreen(Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;)Z
    .locals 7
    .param p1, "toScreen"    # Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;

    .prologue
    const/4 v4, 0x0

    .line 366
    sget-object v3, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "moveToScreen() : toScreen="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    const-string v1, "moveToScreen(DualScreen)"

    .line 368
    .local v1, "methodName":Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->convertToNativeDualScreenEnum(Lcom/samsung/android/sdk/dualscreen/SDualScreenActivity$DualScreen;)Ljava/lang/Object;

    move-result-object v2

    .line 369
    .local v2, "nativeEnum":Ljava/lang/Object;
    sget-object v3, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "moveToScreen() : nativeEnum="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    invoke-static {v1}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 372
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mInstanceDualScreenManager:Ljava/lang/Object;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v1, v3, v5}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 377
    :goto_0
    return v3

    .line 373
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move v3, v4

    .line 374
    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    move v3, v4

    .line 377
    goto :goto_0
.end method

.method setScreenChangedListner(Lcom/samsung/android/sdk/dualscreen/SDualScreenListener$ScreenChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sdk/dualscreen/SDualScreenListener$ScreenChangeListener;

    .prologue
    .line 397
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v2, "setScreenChangedListner()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    const-string v0, "setScreenChangedListner(ScreenChangeListener)"

    .line 399
    .local v0, "methodName":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 400
    iget-object v1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mInstanceActivity:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    :cond_0
    return-void
.end method

.method swapTopTask()V
    .locals 3

    .prologue
    .line 389
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v2, "swapTopTask()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    const-string v0, "swapTopTask()"

    .line 391
    .local v0, "methodName":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 392
    iget-object v1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mInstanceDualScreenManager:Ljava/lang/Object;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    :cond_0
    return-void
.end method

.method switchScreen()V
    .locals 3

    .prologue
    .line 381
    sget-object v1, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->TAG:Ljava/lang/String;

    const-string v2, "switchScreen()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    const-string v0, "switchScreen()"

    .line 383
    .local v0, "methodName":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->checkMethod(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 384
    iget-object v1, p0, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->mInstanceDualScreenManager:Ljava/lang/Object;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/dualscreen/SDualScreenManagerReflector;->invoke(Ljava/lang/String;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    :cond_0
    return-void
.end method
