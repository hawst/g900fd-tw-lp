.class public Lcom/samsung/android/sdk/dualscreen/SDualScreenConstantsReflector$PackageManager;
.super Ljava/lang/Object;
.source "SDualScreenConstantsReflector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/dualscreen/SDualScreenConstantsReflector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PackageManager"
.end annotation


# static fields
.field public static FEATURE_DUALSCREEN:Ljava/lang/String;

.field static FIELD_NAMES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 64
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "FEATURE_DUALSCREEN"

    aput-object v6, v4, v5

    sput-object v4, Lcom/samsung/android/sdk/dualscreen/SDualScreenConstantsReflector$PackageManager;->FIELD_NAMES:[Ljava/lang/String;

    .line 69
    sget-object v4, Lcom/samsung/android/sdk/dualscreen/SDualScreenConstantsReflector$PackageManager;->FIELD_NAMES:[Ljava/lang/String;

    array-length v0, v4

    .line 70
    .local v0, "N":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 72
    :try_start_0
    const-class v4, Landroid/content/pm/PackageManager;

    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenConstantsReflector$PackageManager;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 74
    .local v3, "src":Ljava/lang/reflect/Field;
    const-class v4, Lcom/samsung/android/sdk/dualscreen/SDualScreenConstantsReflector$PackageManager;

    sget-object v5, Lcom/samsung/android/sdk/dualscreen/SDualScreenConstantsReflector$PackageManager;->FIELD_NAMES:[Ljava/lang/String;

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 75
    .local v1, "dst":Ljava/lang/reflect/Field;
    invoke-virtual {v3, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v1, v4}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    .end local v1    # "dst":Ljava/lang/reflect/Field;
    .end local v3    # "src":Ljava/lang/reflect/Field;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 81
    :cond_0
    return-void

    .line 78
    :catch_0
    move-exception v4

    goto :goto_1

    .line 77
    :catch_1
    move-exception v4

    goto :goto_1

    .line 76
    :catch_2
    move-exception v4

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
