.class public Lcom/samsung/helphub/ExternalHelpActivity;
.super Lcom/samsung/helphub/HelpHubActivityBase;
.source "ExternalHelpActivity.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Lcom/samsung/helphub/fragments/tablet/PageListLoadListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/ExternalHelpActivity$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/helphub/HelpHubActivityBase;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/samsung/helphub/fragments/tablet/PageListLoadListener;"
    }
.end annotation


# static fields
.field private static final BUNDLE_KEY_TARGET_LEVEL:Ljava/lang/String; = "external:target_level"

.field private static final BUNDLE_KEY_TARGET_NAME:Ljava/lang/String; = "external:target_name"

.field private static final EXTERNAL_LAUNCH_ACTION:Ljava/lang/String; = "com.samsung.helphub.HELP"

.field private static final EXTERNAL_LAUNCH_ACTION_VZW:Ljava/lang/String; = "com.samsung.help.helpapplication"

.field private static final EXTERNAL_REQUEST_CATEGORY:Ljava/lang/String; = "helphub:category"

.field private static final EXTERNAL_REQUEST_CATEGORY_VZW:Ljava/lang/String; = "screen"

.field private static final EXTERNAL_REQUEST_ITEM:Ljava/lang/String; = "helphub:item"

.field private static final EXTERNAL_REQUEST_SECTION:Ljava/lang/String; = "helphub:section"

.field private static final HEADER_LOADER_ID:I

.field public static sIsReturnFromLauncher:Z

.field public static sItemOpenReq:Ljava/lang/String;


# instance fields
.field private contentListFragment:Landroid/app/Fragment;

.field private mItemHeader:Lcom/samsung/helphub/headers/HelpHeader;

.field private mNewIntent:Landroid/content/Intent;

.field private mTargetLevel:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

.field private mTargetName:Ljava/lang/String;

.field private mUseDelay:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/helphub/ExternalHelpActivity;->sIsReturnFromLauncher:Z

    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/helphub/ExternalHelpActivity;->sItemOpenReq:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/samsung/helphub/HelpHubActivityBase;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mNewIntent:Landroid/content/Intent;

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mUseDelay:Z

    return-void
.end method

.method private handleExternalRequest(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 187
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 188
    .local v1, "bundle":Landroid/os/Bundle;
    const/4 v4, 0x0

    .line 189
    .local v4, "type":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    const/4 v3, 0x0

    .line 191
    .local v3, "target":Ljava/lang/String;
    if-eqz v1, :cond_b

    .line 192
    const-string v5, "helphub:category"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 193
    const-string v5, "helphub:category"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 194
    sget-object v4, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 195
    const-string v5, "helphub:category"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 219
    :cond_0
    :goto_0
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 220
    if-eqz v3, :cond_6

    .line 221
    const-string v5, "wifi"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v3, "wifi_chn"

    .line 222
    :cond_1
    const-string v5, "screen_mirroring"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v3, "screen_mirroring_chn"

    .line 223
    :cond_2
    const-string v5, "allshare_cast"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v3, "allshare_cast_chn"

    .line 225
    :cond_3
    const-string v5, "ro.product.name"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "trlte"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "ro.product.name"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "tblte"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 226
    :cond_4
    const-string v5, "camera"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v3, "camera_chn"

    .line 227
    :cond_5
    const-string v5, "camera_2nd"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v3, "camera_2nd_chn"

    .line 232
    :cond_6
    if-eqz v4, :cond_d

    .line 235
    new-instance v0, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v0, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 236
    .local v0, "args":Landroid/os/Bundle;
    const-string v5, "external:target_name"

    invoke-virtual {v0, v5, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v5, "external:target_level"

    invoke-virtual {v4}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 238
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/app/LoaderManager;->getLoader(I)Landroid/content/Loader;

    move-result-object v5

    if-eqz v5, :cond_c

    .line 239
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v5

    invoke-virtual {v5, v7, v0, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 246
    .end local v0    # "args":Landroid/os/Bundle;
    :goto_1
    return-void

    .line 196
    :cond_7
    const-string v5, "helphub:section"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 197
    const-string v5, "helphub:section"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 198
    sget-object v4, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 199
    const-string v5, "helphub:section"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 200
    :cond_8
    const-string v5, "helphub:item"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 201
    const-string v5, "helphub:item"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 202
    sget-object v4, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 203
    const-string v5, "helphub:item"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 204
    :cond_9
    const-string v5, "screen"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 205
    const-string v5, "screen"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 206
    const-string v5, "BASDEF"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 207
    sget-object v4, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 211
    :goto_2
    const-string v5, "screen"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 209
    :cond_a
    sget-object v4, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    goto :goto_2

    .line 214
    :cond_b
    new-instance v2, Landroid/content/Intent;

    const-class v5, Lcom/samsung/helphub/HelpHubActivity;

    invoke-direct {v2, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 215
    .local v2, "launchIntent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/samsung/helphub/ExternalHelpActivity;->startActivity(Landroid/content/Intent;)V

    .line 216
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->finish()V

    goto/16 :goto_0

    .line 241
    .end local v2    # "launchIntent":Landroid/content/Intent;
    .restart local v0    # "args":Landroid/os/Bundle;
    :cond_c
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v5

    invoke-virtual {v5, v7, v0, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_1

    .line 244
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_d
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->finish()V

    goto :goto_1
.end method

.method private launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 8
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 249
    new-instance v1, Landroid/os/Bundle;

    const/4 v6, 0x2

    invoke-direct {v1, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 250
    .local v1, "bundle":Landroid/os/Bundle;
    new-instance v0, Ljava/lang/StringBuilder;

    const/4 v6, 0x3

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 251
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v6, "content://com.samsung.helphub.provider.search"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/header/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getOwnerResId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 256
    .local v5, "uri":Landroid/net/Uri;
    sget-object v6, Lcom/samsung/helphub/ExternalHelpActivity$1;->$SwitchMap$com$samsung$helphub$headers$HelpHeader$HelpHeaderType:[I

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 279
    :goto_0
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, Lcom/samsung/helphub/headers/HelpHeader;->setIsExtrenal(Z)V

    .line 280
    const-string v6, "helpub:header"

    invoke-virtual {v1, v6, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 281
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 282
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6, v1}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v2

    .line 283
    .local v2, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 288
    .local v4, "transaction":Landroid/app/FragmentTransaction;
    const v6, 0x7f0d0004

    invoke-virtual {v4, v6, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 289
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 291
    .end local v2    # "fragment":Landroid/app/Fragment;
    .end local v4    # "transaction":Landroid/app/FragmentTransaction;
    :cond_0
    return-void

    .line 259
    :pswitch_0
    const-string v6, "section_list:expanded_section_name"

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    invoke-virtual {p0, v5}, Lcom/samsung/helphub/ExternalHelpActivity;->queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object p1

    .line 264
    goto :goto_0

    .line 267
    :pswitch_1
    invoke-virtual {p0, v5}, Lcom/samsung/helphub/ExternalHelpActivity;->queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v3

    .line 270
    .local v3, "ownerHeader":Lcom/samsung/helphub/headers/HelpHeader;
    const-string v6, "page_fragment_argument:activity_title"

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v7

    invoke-virtual {v1, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 256
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private reconfirmFragmentAddition()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 468
    iget-object v1, p0, Lcom/samsung/helphub/ExternalHelpActivity;->contentListFragment:Landroid/app/Fragment;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/helphub/ExternalHelpActivity;->contentListFragment:Landroid/app/Fragment;

    invoke-virtual {v1}, Landroid/app/Fragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_1

    .line 470
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 471
    .local v0, "transaction":Landroid/app/FragmentTransaction;
    invoke-virtual {v0, v4}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 472
    const v1, 0x7f0d0004

    iget-object v2, p0, Lcom/samsung/helphub/ExternalHelpActivity;->contentListFragment:Landroid/app/Fragment;

    const-string v3, "contents"

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 473
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/LoaderManager;->destroyLoader(I)V

    .line 474
    iget-object v1, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mNewIntent:Landroid/content/Intent;

    if-eqz v1, :cond_1

    const-string v1, "com.samsung.helphub.HELP"

    iget-object v2, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mNewIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.samsung.help.helpapplication"

    iget-object v2, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mNewIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 475
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mUseDelay:Z

    .line 476
    iget-object v1, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mNewIntent:Landroid/content/Intent;

    invoke-direct {p0, v1}, Lcom/samsung/helphub/ExternalHelpActivity;->handleExternalRequest(Landroid/content/Intent;)V

    .line 479
    .end local v0    # "transaction":Landroid/app/FragmentTransaction;
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mNewIntent:Landroid/content/Intent;

    .line 480
    return-void
.end method


# virtual methods
.method public isUseDelay()Z
    .locals 1

    .prologue
    .line 483
    iget-boolean v0, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mUseDelay:Z

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/16 v8, 0x400

    .line 62
    invoke-super {p0, p1}, Lcom/samsung/helphub/HelpHubActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "helphub:item"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "isIntentFromLauncher"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 64
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 65
    .local v2, "bundle":Landroid/os/Bundle;
    sput-boolean v10, Lcom/samsung/helphub/ExternalHelpActivity;->sIsReturnFromLauncher:Z

    .line 66
    if-eqz v2, :cond_0

    .line 67
    const-string v5, "helphub:item"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/samsung/helphub/ExternalHelpActivity;->sItemOpenReq:Ljava/lang/String;

    .line 74
    .end local v2    # "bundle":Landroid/os/Bundle;
    :cond_0
    :goto_0
    if-nez p1, :cond_7

    .line 75
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mNewIntent:Landroid/content/Intent;

    .line 76
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->isTwoPane()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 78
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isLatte()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 79
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v8, v8}, Landroid/view/Window;->setFlags(II)V

    .line 82
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 83
    .local v0, "HelpActionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v9}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 87
    .end local v0    # "HelpActionBar":Landroid/app/ActionBar;
    :cond_1
    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v5

    if-nez v5, :cond_2

    .line 88
    new-instance v5, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;

    invoke-direct {v5}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;-><init>()V

    iput-object v5, p0, Lcom/samsung/helphub/ExternalHelpActivity;->contentListFragment:Landroid/app/Fragment;

    .line 89
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 90
    .local v4, "transaction":Landroid/app/FragmentTransaction;
    invoke-virtual {v4, v9}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 91
    const v5, 0x7f0d0004

    iget-object v6, p0, Lcom/samsung/helphub/ExternalHelpActivity;->contentListFragment:Landroid/app/Fragment;

    const-string v7, "contents"

    invoke-virtual {v4, v5, v6, v7}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 116
    .end local v4    # "transaction":Landroid/app/FragmentTransaction;
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->isTwoPane()Z

    move-result v5

    if-nez v5, :cond_4

    .line 117
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-static {v11}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 118
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v8, v8}, Landroid/view/Window;->setFlags(II)V

    .line 121
    :cond_3
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {v11}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 122
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v8, v8}, Landroid/view/Window;->setFlags(II)V

    .line 125
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 126
    .restart local v0    # "HelpActionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v9}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 130
    .end local v0    # "HelpActionBar":Landroid/app/ActionBar;
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "car_mode_on"

    invoke-static {v5, v6, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 131
    .local v3, "isCarMode":I
    if-ne v3, v10, :cond_5

    .line 132
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/high16 v6, 0x600000

    invoke-virtual {v5, v6}, Landroid/view/Window;->addFlags(I)V

    .line 135
    :cond_5
    iput-boolean v10, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mIsExternal:Z

    .line 136
    return-void

    .line 71
    .end local v3    # "isCarMode":I
    :cond_6
    sput-boolean v9, Lcom/samsung/helphub/ExternalHelpActivity;->sIsReturnFromLauncher:Z

    .line 72
    sput-object v11, Lcom/samsung/helphub/ExternalHelpActivity;->sItemOpenReq:Ljava/lang/String;

    goto/16 :goto_0

    .line 96
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->isTwoPane()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 97
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isLatte()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 98
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v8, v8}, Landroid/view/Window;->setFlags(II)V

    .line 101
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 102
    .restart local v0    # "HelpActionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v9}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 105
    .end local v0    # "HelpActionBar":Landroid/app/ActionBar;
    :cond_8
    const-string v5, "external:target_level"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "external:target_name"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 107
    new-instance v1, Landroid/os/Bundle;

    const/4 v5, 0x2

    invoke-direct {v1, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 108
    .local v1, "args":Landroid/os/Bundle;
    const-string v5, "external:target_level"

    const-string v6, "external:target_level"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 110
    const-string v5, "external:target_name"

    const-string v6, "external:target_name"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v5

    invoke-virtual {v5, v9, v1, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto/16 :goto_1
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 10
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 327
    invoke-static {}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->values()[Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v0

    const-string v1, "external:target_level"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    aget-object v9, v0, v1

    .line 328
    .local v9, "type":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    const-string v0, "external:target_name"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 329
    .local v8, "name":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v0, "content://com.samsung.helphub.provider.search"

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 331
    .local v7, "builder":Ljava/lang/StringBuilder;
    sget-object v0, Lcom/samsung/helphub/ExternalHelpActivity$1;->$SwitchMap$com$samsung$helphub$headers$HelpHeader$HelpHeaderType:[I

    invoke-virtual {v9}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 348
    :goto_0
    iput-object v8, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mTargetName:Ljava/lang/String;

    .line 349
    iput-object v9, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mTargetLevel:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 351
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 333
    :pswitch_0
    const-string v0, "/header/item/"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 337
    :pswitch_1
    const-string v0, "/header/section/"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 341
    :pswitch_2
    const-string v0, "/header/category/"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 331
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 14
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 357
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v12

    if-nez v12, :cond_1

    .line 358
    if-eqz p2, :cond_0

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v12

    if-lez v12, :cond_0

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v12

    if-nez v12, :cond_2

    .line 359
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->finish()V

    .line 444
    :goto_0
    if-eqz p2, :cond_1

    .line 445
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V

    .line 448
    :cond_1
    return-void

    .line 361
    :cond_2
    invoke-static/range {p2 .. p2}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v4

    .line 362
    .local v4, "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->isTwoPane()Z

    move-result v12

    if-eqz v12, :cond_a

    .line 364
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    .line 365
    .local v6, "manager":Landroid/app/FragmentManager;
    const/4 v2, 0x0

    .line 367
    .local v2, "contentsList":Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;
    :try_start_0
    const-string v12, "contents"

    invoke-virtual {v6, v12}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v12

    move-object v0, v12

    check-cast v0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 371
    :goto_1
    if-eqz v2, :cond_4

    .line 372
    sget-object v12, Lcom/samsung/helphub/ExternalHelpActivity$1;->$SwitchMap$com$samsung$helphub$headers$HelpHeader$HelpHeaderType:[I

    invoke-virtual {v4}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_0

    .line 434
    :cond_3
    :goto_2
    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v12

    if-nez v12, :cond_9

    .line 435
    invoke-direct {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->reconfirmFragmentAddition()V

    goto :goto_0

    .line 368
    :catch_0
    move-exception v3

    .line 369
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 374
    .end local v3    # "e":Ljava/lang/Exception;
    :pswitch_0
    iput-object v4, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mItemHeader:Lcom/samsung/helphub/headers/HelpHeader;

    .line 375
    new-instance v1, Ljava/lang/StringBuilder;

    const/4 v12, 0x3

    invoke-direct {v1, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 376
    .local v1, "builder":Ljava/lang/StringBuilder;
    const-string v12, "content://com.samsung.helphub.provider.search"

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/header/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Lcom/samsung/helphub/headers/HelpHeader;->getOwnerResId()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 377
    .local v11, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v11}, Lcom/samsung/helphub/ExternalHelpActivity;->queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v7

    .line 379
    .local v7, "ownerHeader":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {v2, v7}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->openSection(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 382
    const-string v12, "tag:help_page_fragment"

    invoke-virtual {v6, v12}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v8

    check-cast v8, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    .line 383
    .local v8, "pageFragment":Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;
    if-eqz v8, :cond_3

    .line 384
    invoke-virtual {v8}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->getListFragment()Landroid/app/Fragment;

    move-result-object v9

    check-cast v9, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;

    .line 385
    .local v9, "pageList":Lcom/samsung/helphub/fragments/tablet/HelpPagesList;
    if-eqz v9, :cond_3

    .line 386
    invoke-virtual {v9, p0}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->setPageListLoadListener(Lcom/samsung/helphub/fragments/tablet/PageListLoadListener;)V

    goto :goto_2

    .line 392
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v7    # "ownerHeader":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v8    # "pageFragment":Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;
    .end local v9    # "pageList":Lcom/samsung/helphub/fragments/tablet/HelpPagesList;
    .end local v11    # "uri":Landroid/net/Uri;
    :pswitch_1
    invoke-virtual {v2, v4}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->openSection(Lcom/samsung/helphub/headers/HelpHeader;)V

    goto :goto_2

    .line 396
    :pswitch_2
    invoke-virtual {v2, v4}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->openCategory(Lcom/samsung/helphub/headers/HelpHeader;)V

    goto :goto_2

    .line 402
    :cond_4
    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 403
    new-instance v8, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    invoke-direct {v8}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;-><init>()V

    .line 404
    .restart local v8    # "pageFragment":Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;
    const/4 v5, 0x0

    .line 407
    .local v5, "isPage":Z
    invoke-virtual {v4}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v12

    const v13, 0x7f0d0103

    if-eq v12, v13, :cond_5

    invoke-virtual {v4}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v12

    const v13, 0x7f0d0102

    if-eq v12, v13, :cond_5

    invoke-virtual {v4}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v12

    const v13, 0x7f0d0104

    if-eq v12, v13, :cond_5

    invoke-virtual {v4}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v12

    const v13, 0x7f0d009a

    if-eq v12, v13, :cond_5

    invoke-virtual {v4}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v12

    const v13, 0x7f0d00a8

    if-eq v12, v13, :cond_5

    invoke-virtual {v4}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v12

    const v13, 0x7f0d00a7

    if-ne v12, v13, :cond_6

    .line 413
    :cond_5
    const/4 v5, 0x1

    .line 416
    :cond_6
    invoke-virtual {v8}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->isAdded()Z

    move-result v12

    if-nez v12, :cond_8

    .line 417
    invoke-virtual {v4}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v12

    sget-object v13, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v12, v13, :cond_7

    .line 418
    new-instance v1, Ljava/lang/StringBuilder;

    const/4 v12, 0x3

    invoke-direct {v1, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 419
    .restart local v1    # "builder":Ljava/lang/StringBuilder;
    const-string v12, "content://com.samsung.helphub.provider.search"

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/header/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Lcom/samsung/helphub/headers/HelpHeader;->getOwnerResId()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 420
    .restart local v11    # "uri":Landroid/net/Uri;
    invoke-virtual {p0, v11}, Lcom/samsung/helphub/ExternalHelpActivity;->queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v7

    .line 421
    .restart local v7    # "ownerHeader":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {v8, v7, v4, v5}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->setHeaders(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Z)V

    .line 426
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v7    # "ownerHeader":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v11    # "uri":Landroid/net/Uri;
    :goto_3
    invoke-virtual {v6}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v10

    .line 427
    .local v10, "transaction":Landroid/app/FragmentTransaction;
    const v12, 0x7f0d0004

    const-string v13, "tag:help_page_fragment"

    invoke-virtual {v10, v12, v8, v13}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 428
    const/16 v12, 0x1001

    invoke-virtual {v10, v12}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 429
    invoke-virtual {v10}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto/16 :goto_2

    .line 423
    .end local v10    # "transaction":Landroid/app/FragmentTransaction;
    :cond_7
    invoke-virtual {v8, v4, v4, v5}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->setHeaders(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Z)V

    goto :goto_3

    .line 431
    :cond_8
    invoke-virtual {v8, v4, v4, v5}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->switchToHelpPage(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Z)V

    goto/16 :goto_2

    .line 437
    .end local v5    # "isPage":Z
    .end local v8    # "pageFragment":Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;
    :cond_9
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mNewIntent:Landroid/content/Intent;

    goto/16 :goto_0

    .line 441
    .end local v2    # "contentsList":Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;
    .end local v6    # "manager":Landroid/app/FragmentManager;
    :cond_a
    invoke-direct {p0, v4}, Lcom/samsung/helphub/ExternalHelpActivity;->launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;)V

    goto/16 :goto_0

    .line 372
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 39
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/ExternalHelpActivity;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 453
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 168
    invoke-super {p0, p1}, Lcom/samsung/helphub/HelpHubActivityBase;->onNewIntent(Landroid/content/Intent;)V

    .line 169
    if-eqz p1, :cond_2

    const-string v1, "com.samsung.helphub.HELP"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.samsung.help.helpapplication"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 170
    :cond_0
    const-string v1, "helphub:item"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "isIntentFromLauncher"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 171
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 172
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/helphub/ExternalHelpActivity;->sIsReturnFromLauncher:Z

    .line 173
    if-eqz v0, :cond_1

    .line 174
    const-string v1, "helphub:item"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/helphub/ExternalHelpActivity;->sItemOpenReq:Ljava/lang/String;

    .line 181
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->finish()V

    .line 182
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/ExternalHelpActivity;->startActivity(Landroid/content/Intent;)V

    .line 184
    :cond_2
    return-void

    .line 178
    :cond_3
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/helphub/ExternalHelpActivity;->sIsReturnFromLauncher:Z

    .line 179
    const/4 v1, 0x0

    sput-object v1, Lcom/samsung/helphub/ExternalHelpActivity;->sItemOpenReq:Ljava/lang/String;

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 295
    const/4 v1, 0x0

    .line 297
    .local v1, "retval":Z
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 322
    :goto_0
    if-nez v1, :cond_0

    invoke-super {p0, p1}, Lcom/samsung/helphub/HelpHubActivityBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    :goto_1
    return v2

    .line 301
    :pswitch_0
    :try_start_0
    sget-boolean v3, Lcom/samsung/helphub/ExternalHelpActivity;->sIsReturnFromLauncher:Z

    if-eqz v3, :cond_1

    .line 302
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->startHomeScreenSection()V

    .line 315
    :goto_2
    const/4 v1, 0x1

    .line 316
    goto :goto_0

    .line 304
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->isStackEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 305
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->finish()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 312
    :catch_0
    move-exception v0

    .line 313
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_2

    .line 307
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->popBackStack()V

    .line 308
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 309
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 322
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 297
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 151
    invoke-super {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->onResume()V

    .line 152
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 153
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 154
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HELP"

    invoke-static {p0, v0, v1}, Lcom/samsung/helphub/HelpHubCommon;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mUseDelay:Z

    .line 156
    iget-object v0, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mNewIntent:Landroid/content/Intent;

    if-eqz v0, :cond_1

    const-string v0, "com.samsung.helphub.HELP"

    iget-object v1, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mNewIntent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.samsung.help.helpapplication"

    iget-object v1, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mNewIntent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mNewIntent:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/samsung/helphub/ExternalHelpActivity;->handleExternalRequest(Landroid/content/Intent;)V

    .line 160
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->isTwoPane()Z

    move-result v0

    if-nez v0, :cond_1

    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mNewIntent:Landroid/content/Intent;

    .line 164
    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->isTwoPane()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mTargetLevel:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mTargetName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 142
    const-string v0, "external:target_level"

    iget-object v1, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mTargetLevel:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    const-string v0, "external:target_name"

    iget-object v1, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mTargetName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/helphub/HelpHubActivityBase;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 147
    return-void
.end method

.method public pageListLoaded()V
    .locals 4

    .prologue
    .line 457
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "tag:help_page_fragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    .line 459
    .local v0, "pageFragment":Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;
    if-eqz v0, :cond_0

    .line 460
    invoke-virtual {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->getListFragment()Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;

    .line 461
    .local v1, "pageList":Lcom/samsung/helphub/fragments/tablet/HelpPagesList;
    if-eqz v1, :cond_0

    .line 462
    iget-object v2, p0, Lcom/samsung/helphub/ExternalHelpActivity;->mItemHeader:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->openPage(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 465
    .end local v1    # "pageList":Lcom/samsung/helphub/fragments/tablet/HelpPagesList;
    :cond_0
    return-void
.end method

.method public startHomeScreenSection()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 489
    sget-boolean v1, Lcom/samsung/helphub/ExternalHelpActivity;->sIsReturnFromLauncher:Z

    if-eqz v1, :cond_0

    .line 490
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 491
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "external:target_name"

    const-string v2, "homescreen"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    const-string v1, "external:target_level"

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 493
    sput-boolean v3, Lcom/samsung/helphub/ExternalHelpActivity;->sIsReturnFromLauncher:Z

    .line 494
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/LoaderManager;->getLoader(I)Landroid/content/Loader;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 495
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v3, v0, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 500
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 497
    .restart local v0    # "args":Landroid/os/Bundle;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/ExternalHelpActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v3, v0, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0
.end method
