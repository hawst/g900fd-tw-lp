.class public final Lcom/samsung/helphub/utility/PreferenceHelper;
.super Ljava/lang/Object;
.source "PreferenceHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;,
        Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;
    }
.end annotation


# static fields
.field private static final PREFERENCE_DATA_SELECTION:Ljava/lang/String; = "DATA_SELECTION"

.field private static final PREFERENCE_LEFT_PANEL_RATIO:Ljava/lang/String; = "LEFT_PANEL_RATIO"

.field private static final PREFERENCE_MOBILE_CHECKED:Ljava/lang/String; = "MOBILE_CHECKED"

.field private static final PREFERENCE_WLAN_CHECKED:Ljava/lang/String; = "WLAN_CHECKED"

.field private static final SHARED_PREFS_NAME:Ljava/lang/String; = "com.samsung.helphub.helphub_preferences"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method public static getCheckedPreference(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "preference"    # Ljava/lang/String;

    .prologue
    .line 86
    const-string v2, "com.samsung.helphub.helphub_preferences"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 87
    .local v1, "pref":Landroid/content/SharedPreferences;
    sget-object v0, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->NONE_CHECKED:Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    .line 89
    .local v0, "data":Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;
    if-eqz v1, :cond_0

    .line 90
    sget-object v2, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->NONE_CHECKED:Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    invoke-virtual {v2}, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->valueOf(Ljava/lang/String;)Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    move-result-object v0

    .line 92
    :cond_0
    return-object v0
.end method

.method public static getDataPreference(Landroid/content/Context;)Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    const-string v2, "com.samsung.helphub.helphub_preferences"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 67
    .local v1, "pref":Landroid/content/SharedPreferences;
    sget-object v0, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->NONE:Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;

    .line 69
    .local v0, "data":Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;
    if-eqz v1, :cond_0

    .line 70
    const-string v2, "DATA_SELECTION"

    sget-object v3, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->NONE:Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;

    invoke-virtual {v3}, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->valueOf(Ljava/lang/String;)Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;

    move-result-object v0

    .line 73
    :cond_0
    return-object v0
.end method

.method public static getLeftPanelRatio(Landroid/content/Context;)F
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 105
    const-string v2, "com.samsung.helphub.helphub_preferences"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 106
    .local v1, "pref":Landroid/content/SharedPreferences;
    const/4 v0, 0x0

    .line 107
    .local v0, "data":F
    if-eqz v1, :cond_0

    .line 108
    const-string v2, "LEFT_PANEL_RATIO"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    .line 110
    :cond_0
    return v0
.end method

.method public static setCheckedPreference(Landroid/content/Context;Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "selection"    # Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;
    .param p2, "preference"    # Ljava/lang/String;

    .prologue
    .line 76
    const-string v2, "com.samsung.helphub.helphub_preferences"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 77
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 79
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    :goto_0
    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p1}, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p2, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 81
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 83
    :cond_0
    return-void

    .line 77
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    goto :goto_0
.end method

.method public static setDataPreference(Landroid/content/Context;Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "selection"    # Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;

    .prologue
    .line 49
    const-string v2, "com.samsung.helphub.helphub_preferences"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 50
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 52
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    :goto_0
    if-eqz v0, :cond_0

    .line 53
    const-string v2, "DATA_SELECTION"

    invoke-virtual {p1}, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 54
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 56
    :cond_0
    return-void

    .line 50
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    goto :goto_0
.end method

.method public static setLeftPanelRatio(Landroid/content/Context;F)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ratio"    # F

    .prologue
    .line 96
    const-string v2, "com.samsung.helphub.helphub_preferences"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 97
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 98
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    :goto_0
    if-eqz v0, :cond_0

    .line 99
    const-string v2, "LEFT_PANEL_RATIO"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 100
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 102
    :cond_0
    return-void

    .line 97
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    goto :goto_0
.end method
