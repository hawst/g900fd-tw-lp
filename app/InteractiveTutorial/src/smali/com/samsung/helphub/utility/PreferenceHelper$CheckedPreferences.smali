.class public final enum Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;
.super Ljava/lang/Enum;
.source "PreferenceHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/utility/PreferenceHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CheckedPreferences"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

.field public static final enum CHECKED:Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

.field public static final enum NONE_CHECKED:Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    const-string v1, "NONE_CHECKED"

    invoke-direct {v0, v1, v2}, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->NONE_CHECKED:Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    .line 39
    new-instance v0, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    const-string v1, "CHECKED"

    invoke-direct {v0, v1, v3}, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->CHECKED:Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    .line 35
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    sget-object v1, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->NONE_CHECKED:Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->CHECKED:Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->$VALUES:[Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 35
    const-class v0, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    return-object v0
.end method

.method public static values()[Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->$VALUES:[Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    invoke-virtual {v0}, [Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    return-object v0
.end method
