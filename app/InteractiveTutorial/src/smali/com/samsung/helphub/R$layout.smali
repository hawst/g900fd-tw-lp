.class public final Lcom/samsung/helphub/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final action_memo_launch:I = 0x7f040000

.field public static final action_memo_link:I = 0x7f040001

.field public static final actionbar_search:I = 0x7f040002

.field public static final add_contact_only:I = 0x7f040003

.field public static final air_button_introduction_to_air_button:I = 0x7f040004

.field public static final air_button_introduction_to_air_button_att:I = 0x7f040005

.field public static final air_button_using_features_in_air_command:I = 0x7f040006

.field public static final air_button_using_features_in_air_command_image_clip:I = 0x7f040007

.field public static final air_button_using_features_in_air_command_smart_select:I = 0x7f040008

.field public static final air_command_using_other_air_command_features:I = 0x7f040009

.field public static final air_gesture_browse:I = 0x7f04000a

.field public static final air_gesture_call_accept:I = 0x7f04000b

.field public static final air_gesture_indicator:I = 0x7f04000c

.field public static final air_gesture_jump:I = 0x7f04000d

.field public static final air_gesture_move:I = 0x7f04000e

.field public static final air_gesture_quick_glance:I = 0x7f04000f

.field public static final air_view_information_preview:I = 0x7f040010

.field public static final air_view_introduction_to_air_view:I = 0x7f040011

.field public static final air_view_magnifier:I = 0x7f040012

.field public static final air_view_pen_finger_introduction_to_air_view:I = 0x7f040013

.field public static final air_view_pen_icon_labels:I = 0x7f040014

.field public static final air_view_pen_information_preview:I = 0x7f040015

.field public static final air_view_pen_introduction_to_air_view:I = 0x7f040016

.field public static final air_view_pen_list_scrolling:I = 0x7f040017

.field public static final air_view_pen_progress_preview:I = 0x7f040018

.field public static final air_view_pen_speed_dial_preview:I = 0x7f040019

.field public static final air_view_pointer:I = 0x7f04001a

.field public static final air_view_progress_bar:I = 0x7f04001b

.field public static final air_view_speed_dial:I = 0x7f04001c

.field public static final allshare_howto_cast_dongle:I = 0x7f04001d

.field public static final allshare_learn_nfc:I = 0x7f04001e

.field public static final allshare_smart_tv:I = 0x7f04001f

.field public static final battery_power_saving_mode:I = 0x7f040020

.field public static final battery_task_manager:I = 0x7f040021

.field public static final bluetooth_bluetooth_setup:I = 0x7f040022

.field public static final bluetooth_send_picture_via_bluetooth:I = 0x7f040023

.field public static final call_accept_reject:I = 0x7f040024

.field public static final call_accept_reject_2nd_lcd:I = 0x7f040025

.field public static final call_accept_reject_easy_mode:I = 0x7f040026

.field public static final call_drag_and_drop:I = 0x7f040027

.field public static final call_drag_and_drop_2nd_lcd:I = 0x7f040028

.field public static final call_incoming_call_while_using_app:I = 0x7f040029

.field public static final camera_camera_modes:I = 0x7f04002a

.field public static final camera_camera_modes_chn:I = 0x7f04002b

.field public static final camera_camera_modes_easymode:I = 0x7f04002c

.field public static final camera_changing_camera_mode:I = 0x7f04002d

.field public static final camera_introduction:I = 0x7f04002e

.field public static final camera_introduction_2nd_lcd:I = 0x7f04002f

.field public static final camera_introduction_easymode:I = 0x7f040030

.field public static final camera_introduction_easymode_2nd_lcd:I = 0x7f040031

.field public static final camera_quick_settings:I = 0x7f040032

.field public static final camera_record_videos:I = 0x7f040033

.field public static final camera_record_videos_2nd_lcd:I = 0x7f040034

.field public static final camera_record_videos_easymode:I = 0x7f040035

.field public static final camera_select_effects:I = 0x7f040036

.field public static final camera_select_mode:I = 0x7f040037

.field public static final camera_settings:I = 0x7f040038

.field public static final camera_settings_2nd_lcd:I = 0x7f040039

.field public static final camera_take_dual_picture:I = 0x7f04003a

.field public static final camera_take_picture:I = 0x7f04003b

.field public static final camera_take_picture_2nd_lcd:I = 0x7f04003c

.field public static final camera_viewing_pics_and_videos:I = 0x7f04003d

.field public static final camera_viewing_pics_and_videos_2nd_lcd:I = 0x7f04003e

.field public static final car_mode_enabling_car_mode:I = 0x7f04003f

.field public static final car_mode_registering_cars:I = 0x7f040040

.field public static final car_mode_using_voice_command:I = 0x7f040041

.field public static final chart_builder_create_bar_chart:I = 0x7f040042

.field public static final chart_builder_create_frame_edit:I = 0x7f040043

.field public static final chart_builder_create_line_chart:I = 0x7f040044

.field public static final chart_builder_create_pie_chart:I = 0x7f040045

.field public static final chart_builder_create_table:I = 0x7f040046

.field public static final chatonv_howto_use:I = 0x7f040047

.field public static final cocktail_service_editing_express_me:I = 0x7f040048

.field public static final cocktail_service_managing_panels:I = 0x7f040049

.field public static final cocktail_service_navigating_panels:I = 0x7f04004a

.field public static final cocktail_service_using_night_clock:I = 0x7f04004b

.field public static final cocktail_service_using_quick_tools:I = 0x7f04004c

.field public static final cocktail_service_waking_up_edge_screen:I = 0x7f04004d

.field public static final connection_dialog:I = 0x7f04004e

.field public static final contacts_add:I = 0x7f04004f

.field public static final contacts_setup_speed_dial:I = 0x7f040050

.field public static final data_connection_dialog:I = 0x7f040051

.field public static final data_usage_checking:I = 0x7f040052

.field public static final data_usage_set_data_usage_limit:I = 0x7f040053

.field public static final detail_activity_layout:I = 0x7f040054

.field public static final dual_camera_dual_picture:I = 0x7f040055

.field public static final dual_camera_introduction:I = 0x7f040056

.field public static final dual_camera_modes:I = 0x7f040057

.field public static final dual_camera_record_videos:I = 0x7f040058

.field public static final dual_camera_take_pictures:I = 0x7f040059

.field public static final easy_homescreen_add_apps:I = 0x7f04005a

.field public static final easy_homescreen_add_contacts:I = 0x7f04005b

.field public static final easy_homescreen_navigate:I = 0x7f04005c

.field public static final easy_homescreen_remove_apps:I = 0x7f04005d

.field public static final easy_homescreen_view_more_apps:I = 0x7f04005e

.field public static final edit_quick_settings:I = 0x7f04005f

.field public static final editing_homescreen:I = 0x7f040060

.field public static final email_change_signature:I = 0x7f040061

.field public static final email_composing_emails:I = 0x7f040062

.field public static final email_create_filtering:I = 0x7f040063

.field public static final email_managing_emails:I = 0x7f040064

.field public static final email_reading_emails:I = 0x7f040065

.field public static final email_set_priority_sender:I = 0x7f040066

.field public static final email_setup_account:I = 0x7f040067

.field public static final email_setup_pop_imap:I = 0x7f040068

.field public static final emeeting_create_meeting:I = 0x7f040069

.field public static final emeeting_introduction:I = 0x7f04006a

.field public static final emeeting_join_meeting:I = 0x7f04006b

.field public static final emeeting_meeting_screen:I = 0x7f04006c

.field public static final emeeting_requirements:I = 0x7f04006d

.field public static final enable_setting_dialog:I = 0x7f04006e

.field public static final expandable_layout:I = 0x7f04006f

.field public static final external_help_page:I = 0x7f040070

.field public static final fingerprints_use_fingerprint:I = 0x7f040071

.field public static final galaxyfinder_how_to_improve_searching:I = 0x7f040072

.field public static final galaxyfinder_how_to_start_searching:I = 0x7f040073

.field public static final galaxyfinder_search_by_handwriting:I = 0x7f040074

.field public static final galaxyfinder_sort_search_results:I = 0x7f040075

.field public static final gallery_3d_panorama:I = 0x7f040076

.field public static final gallery_contextual_tag:I = 0x7f040077

.field public static final gallery_contextual_tag_k:I = 0x7f040078

.field public static final gallery_delete_picture:I = 0x7f040079

.field public static final gallery_delete_picture_easy:I = 0x7f04007a

.field public static final gallery_face_tagging:I = 0x7f04007b

.field public static final gallery_filtering_picture:I = 0x7f04007c

.field public static final gallery_golf_shot:I = 0x7f04007d

.field public static final gallery_howto_collage:I = 0x7f04007e

.field public static final gallery_howto_copy:I = 0x7f04007f

.field public static final gallery_howto_copy_onmenu:I = 0x7f040080

.field public static final gallery_howto_edit:I = 0x7f040081

.field public static final gallery_howto_edit_onmenu:I = 0x7f040082

.field public static final gallery_howto_entertainer:I = 0x7f040083

.field public static final gallery_howto_entertainer_onmenu:I = 0x7f040084

.field public static final gallery_howto_selection_mode:I = 0x7f040085

.field public static final gallery_howto_selection_mode_onmenu:I = 0x7f040086

.field public static final gallery_playing_videos:I = 0x7f040087

.field public static final gallery_sharing_picture:I = 0x7f040088

.field public static final gallery_sharing_picture_easy:I = 0x7f040089

.field public static final gallery_sound_scene:I = 0x7f04008a

.field public static final gallery_viewing_album:I = 0x7f04008b

.field public static final gallery_viewing_album_easy:I = 0x7f04008c

.field public static final gallery_viewing_picture:I = 0x7f04008d

.field public static final gallery_viewing_picture_and_resizing_thumbnails:I = 0x7f04008e

.field public static final gallery_viewing_picture_and_resizing_thumbnails_easy:I = 0x7f04008f

.field public static final group_play_create_group:I = 0x7f040090

.field public static final group_play_introduction:I = 0x7f040091

.field public static final group_play_join_group:I = 0x7f040092

.field public static final group_play_music_sharing:I = 0x7f040093

.field public static final group_play_sharing_contents:I = 0x7f040094

.field public static final group_play_video_sharing:I = 0x7f040095

.field public static final guest_lounge_access_home:I = 0x7f040096

.field public static final guest_lounge_introduction:I = 0x7f040097

.field public static final help_page_container:I = 0x7f040098

.field public static final help_page_fragment:I = 0x7f040099

.field public static final help_page_group_item:I = 0x7f04009a

.field public static final help_web_page:I = 0x7f04009b

.field public static final helphub_activity_detail_title_twopane:I = 0x7f04009c

.field public static final helphub_widget:I = 0x7f04009d

.field public static final homescreen_add_app:I = 0x7f04009e

.field public static final homescreen_add_widget:I = 0x7f04009f

.field public static final homescreen_change_wallpaper:I = 0x7f0400a0

.field public static final homescreen_create_folder:I = 0x7f0400a1

.field public static final homescreen_flipboard_briefing:I = 0x7f0400a2

.field public static final homescreen_move_items:I = 0x7f0400a3

.field public static final homescreen_navigate:I = 0x7f0400a4

.field public static final homescreen_remove_items:I = 0x7f0400a5

.field public static final homescreen_resize_widget:I = 0x7f0400a6

.field public static final homescreen_using_home:I = 0x7f0400a7

.field public static final homescreen_using_mymagazine:I = 0x7f0400a8

.field public static final homescreen_view_all_apps:I = 0x7f0400a9

.field public static final internet_help_v01:I = 0x7f0400aa

.field public static final keyboard_continuous_input:I = 0x7f0400ab

.field public static final keyboard_handwriting:I = 0x7f0400ac

.field public static final keyboard_handwriting_gesture:I = 0x7f0400ad

.field public static final keyboard_predictive_text:I = 0x7f0400ae

.field public static final keyboard_samsung_keyboard:I = 0x7f0400af

.field public static final keyboard_samsung_keyboard_china:I = 0x7f0400b0

.field public static final keys_how_to_use:I = 0x7f0400b1

.field public static final keys_using_keys_on_device:I = 0x7f0400b2

.field public static final life_times_howto_browser_daily_life:I = 0x7f0400b3

.field public static final life_times_howto_create_episode:I = 0x7f0400b4

.field public static final life_times_howto_discover_report:I = 0x7f0400b5

.field public static final list_category_headers:I = 0x7f0400b6

.field public static final list_category_headers_item:I = 0x7f0400b7

.field public static final list_expandable_item:I = 0x7f0400b8

.field public static final list_page_expandable:I = 0x7f0400b9

.field public static final list_page_expandable_group_item:I = 0x7f0400ba

.field public static final list_search_result:I = 0x7f0400bb

.field public static final list_search_result_item:I = 0x7f0400bc

.field public static final list_section_expandable:I = 0x7f0400bd

.field public static final list_section_expandable_child_item:I = 0x7f0400be

.field public static final list_section_expandable_group_item:I = 0x7f0400bf

.field public static final list_section_headers:I = 0x7f0400c0

.field public static final list_section_headers_item:I = 0x7f0400c1

.field public static final list_sorting_contents:I = 0x7f0400c2

.field public static final list_video_bubble_item:I = 0x7f0400c3

.field public static final list_with_help_items:I = 0x7f0400c4

.field public static final magazine_home_adding_widget:I = 0x7f0400c5

.field public static final magazine_home_change_layout:I = 0x7f0400c6

.field public static final magazine_home_move_widget:I = 0x7f0400c7

.field public static final magazine_home_nagavigating:I = 0x7f0400c8

.field public static final magazine_home_remove_widget:I = 0x7f0400c9

.field public static final magazine_home_resize_widget:I = 0x7f0400ca

.field public static final main_activity_layout:I = 0x7f0400cb

.field public static final managing_applications:I = 0x7f0400cc

.field public static final managing_contacts:I = 0x7f0400cd

.field public static final messages_sending_messages:I = 0x7f0400ce

.field public static final messages_sending_multimedia_messages:I = 0x7f0400cf

.field public static final messages_viewing_full_conversation:I = 0x7f0400d0

.field public static final messaging_add_priority_sender:I = 0x7f0400d1

.field public static final messaging_send_messages:I = 0x7f0400d2

.field public static final messaging_send_multimedia:I = 0x7f0400d3

.field public static final motion_palm_to_capture_screen:I = 0x7f0400d4

.field public static final motion_palm_to_mute:I = 0x7f0400d5

.field public static final motion_pan_to_browse_image:I = 0x7f0400d6

.field public static final motion_pan_to_move_icon:I = 0x7f0400d7

.field public static final motion_pick_up_to_direct_call:I = 0x7f0400d8

.field public static final motion_pick_up_to_smart_alert:I = 0x7f0400d9

.field public static final motion_shake_to_update:I = 0x7f0400da

.field public static final motion_tilt_to_zoom:I = 0x7f0400db

.field public static final motion_turn_over_to_mute:I = 0x7f0400dc

.field public static final multi_window_bezel:I = 0x7f0400dd

.field public static final multi_window_bezel_template:I = 0x7f0400de

.field public static final multi_window_drag_and_drop_content:I = 0x7f0400df

.field public static final multi_window_edit_items:I = 0x7f0400e0

.field public static final multi_window_opening:I = 0x7f0400e1

.field public static final multi_window_opening_by_recents:I = 0x7f0400e2

.field public static final multi_window_opening_by_tray:I = 0x7f0400e3

.field public static final multi_window_recent_apps:I = 0x7f0400e4

.field public static final multi_window_show_hide_tray:I = 0x7f0400e5

.field public static final multi_window_template:I = 0x7f0400e6

.field public static final multi_window_tray:I = 0x7f0400e7

.field public static final multi_window_using:I = 0x7f0400e8

.field public static final multi_window_view:I = 0x7f0400e9

.field public static final my_magazine_introduction:I = 0x7f0400ea

.field public static final mymagazine_change_category:I = 0x7f0400eb

.field public static final mymagazine_navigate:I = 0x7f0400ec

.field public static final nfc_set_default_method:I = 0x7f0400ed

.field public static final nfc_turn_on_nfc:I = 0x7f0400ee

.field public static final nfc_turn_on_nfc_att:I = 0x7f0400ef

.field public static final nfc_turn_on_sbeam:I = 0x7f0400f0

.field public static final nfc_using_tap_and_pay:I = 0x7f0400f1

.field public static final notifications_expandable:I = 0x7f0400f2

.field public static final notifications_panel_access2:I = 0x7f0400f3

.field public static final notifications_panel_check:I = 0x7f0400f4

.field public static final notifications_panel_enabledisable_functions:I = 0x7f0400f5

.field public static final notifications_use_panel:I = 0x7f0400f6

.field public static final onehand_reduce_screen:I = 0x7f0400f7

.field public static final onehand_side_key_panel:I = 0x7f0400f8

.field public static final onehand_side_menu:I = 0x7f0400f9

.field public static final online_accessories:I = 0x7f0400fa

.field public static final online_help:I = 0x7f0400fb

.field public static final overlays_list_layout:I = 0x7f0400fc

.field public static final phone_add_contact:I = 0x7f0400fd

.field public static final phone_make_call:I = 0x7f0400fe

.field public static final phone_making_a_call:I = 0x7f0400ff

.field public static final phone_making_a_call_easy_mode:I = 0x7f040100

.field public static final phone_manage_contacts:I = 0x7f040101

.field public static final phone_manage_contacts_easy_mode:I = 0x7f040102

.field public static final phone_save_contact:I = 0x7f040103

.field public static final phone_using_menu_during_call:I = 0x7f040104

.field public static final phone_using_menu_during_call_easy_mode:I = 0x7f040105

.field public static final photo_reader_auto_mode:I = 0x7f040106

.field public static final photo_reader_capture:I = 0x7f040107

.field public static final photo_reader_detect_text:I = 0x7f040108

.field public static final photo_reader_realtime:I = 0x7f040109

.field public static final photo_reader_translate:I = 0x7f04010a

.field public static final popup_opening:I = 0x7f04010b

.field public static final private_mode_move_to_private:I = 0x7f04010c

.field public static final private_mode_remove_from_private:I = 0x7f04010d

.field public static final quickconnect_connecting_to_neaby_devices:I = 0x7f04010e

.field public static final quickconnect_send_or_play_onscreen_content:I = 0x7f04010f

.field public static final ringtone_change_ringtone:I = 0x7f040110

.field public static final s_planner_summary:I = 0x7f040111

.field public static final safety_emergency_mode:I = 0x7f040112

.field public static final safety_geo_life:I = 0x7f040113

.field public static final safety_geo_news:I = 0x7f040114

.field public static final safety_help_message:I = 0x7f040115

.field public static final sbrowser_add_bookmark:I = 0x7f040116

.field public static final sbrowser_open_new_tab:I = 0x7f040117

.field public static final sbrowser_quick_access:I = 0x7f040118

.field public static final sbrowser_quick_access_easy_mode:I = 0x7f040119

.field public static final sbrowser_reading_list:I = 0x7f04011a

.field public static final sbrowser_search_engine:I = 0x7f04011b

.field public static final sbrowser_using_search_bar:I = 0x7f04011c

.field public static final sbrowser_window_manager:I = 0x7f04011d

.field public static final sconnect_how_to_use_sconnect:I = 0x7f04011e

.field public static final scrapbook_collecting_content:I = 0x7f04011f

.field public static final scrapbook_using_scrapbook:I = 0x7f040120

.field public static final simple_list_item:I = 0x7f040121

.field public static final simple_list_layout:I = 0x7f040122

.field public static final smart_clip_pen_crop:I = 0x7f040123

.field public static final smart_clip_reshape:I = 0x7f040124

.field public static final smart_remote_introduction:I = 0x7f040125

.field public static final smart_screen_introduction:I = 0x7f040126

.field public static final smart_screen_smart_pause:I = 0x7f040127

.field public static final smart_screen_smart_rotation:I = 0x7f040128

.field public static final smart_screen_smart_scroll:I = 0x7f040129

.field public static final smart_screen_smart_stay:I = 0x7f04012a

.field public static final smemo_account_settings:I = 0x7f04012b

.field public static final smemo_create_and_save:I = 0x7f04012c

.field public static final smemo_drawing_tools:I = 0x7f04012d

.field public static final smemo_erase_tools:I = 0x7f04012e

.field public static final smemo_export:I = 0x7f04012f

.field public static final smemo_idea_sketch:I = 0x7f040130

.field public static final smemo_import:I = 0x7f040131

.field public static final smemo_manage_folders:I = 0x7f040132

.field public static final smemo_manage_objects:I = 0x7f040133

.field public static final smemo_manage_page_order:I = 0x7f040134

.field public static final smemo_sketch_effect:I = 0x7f040135

.field public static final smemo_zoom_and_pan:I = 0x7f040136

.field public static final snote_action_memo:I = 0x7f040137

.field public static final snote_add_index_on_page:I = 0x7f040138

.field public static final snote_add_page_exist_note:I = 0x7f040139

.field public static final snote_add_pages_on_edit:I = 0x7f04013a

.field public static final snote_add_template_edit_mode:I = 0x7f04013b

.field public static final snote_change_modes_on_edit:I = 0x7f04013c

.field public static final snote_create_new_note:I = 0x7f04013d

.field public static final snote_edit_index_on_edit:I = 0x7f04013e

.field public static final snote_editing_handwritings:I = 0x7f04013f

.field public static final snote_editing_layout:I = 0x7f040140

.field public static final snote_editing_snote:I = 0x7f040141

.field public static final snote_erasing_handwritings:I = 0x7f040142

.field public static final snote_go_to_the_library:I = 0x7f040143

.field public static final snote_how_to_set_lock:I = 0x7f040144

.field public static final snote_how_to_show_the_grid_on_page:I = 0x7f040145

.field public static final snote_magnified_note:I = 0x7f040146

.field public static final snote_panning_on_finger_mode:I = 0x7f040147

.field public static final snote_panning_on_pen_mode:I = 0x7f040148

.field public static final snote_pen_preset:I = 0x7f040149

.field public static final snote_resizing_toolbar_setting_on_edit:I = 0x7f04014a

.field public static final snote_selection_mode:I = 0x7f04014b

.field public static final snote_show_hide_the_toolbar_on_edit:I = 0x7f04014c

.field public static final snote_snap_note:I = 0x7f04014d

.field public static final snote_take_picture_record_video:I = 0x7f04014e

.field public static final snote_use_pen_setting:I = 0x7f04014f

.field public static final snote_use_the_grid_on_page:I = 0x7f040150

.field public static final snote_use_zoom_pad:I = 0x7f040151

.field public static final snote_using_snote:I = 0x7f040152

.field public static final snote_using_text_mode:I = 0x7f040153

.field public static final snote_view_page_preview:I = 0x7f040154

.field public static final snote_zoom_in_on_edit:I = 0x7f040155

.field public static final sortandsearch_view:I = 0x7f040156

.field public static final spen_gesture_capture_screen:I = 0x7f040157

.field public static final spen_gesture_open_action_memo:I = 0x7f040158

.field public static final spen_gesture_select_items:I = 0x7f040159

.field public static final spen_gesture_select_text:I = 0x7f04015a

.field public static final story_album_combine_albums:I = 0x7f04015b

.field public static final story_album_create_filter_album:I = 0x7f04015c

.field public static final story_album_create_gallery:I = 0x7f04015d

.field public static final story_album_create_recommended_albums:I = 0x7f04015e

.field public static final story_album_criteria_settings:I = 0x7f04015f

.field public static final story_album_edit_memo:I = 0x7f040160

.field public static final story_album_import_export_albums:I = 0x7f040161

.field public static final story_album_introduction:I = 0x7f040162

.field public static final story_album_select_album:I = 0x7f040163

.field public static final story_album_share_albums:I = 0x7f040164

.field public static final story_album_view_similar_pic:I = 0x7f040165

.field public static final svoice_access_svoice:I = 0x7f040166

.field public static final svoice_access_to_drive_mode:I = 0x7f040167

.field public static final svoice_barge_in:I = 0x7f040168

.field public static final svoice_bt_tag_parsing:I = 0x7f040169

.field public static final svoice_connect_vehicle:I = 0x7f04016a

.field public static final svoice_driving_mode_tips:I = 0x7f04016b

.field public static final svoice_edit_what_you_said:I = 0x7f04016c

.field public static final svoice_enable_driving:I = 0x7f04016d

.field public static final svoice_enable_hands_free:I = 0x7f04016e

.field public static final svoice_functions_available_in_driving_mode:I = 0x7f04016f

.field public static final svoice_functions_available_in_hands_free_mode:I = 0x7f040170

.field public static final svoice_hands_free_mode_tips:I = 0x7f040171

.field public static final svoice_how_to_use_voice_commands:I = 0x7f040172

.field public static final svoice_use_svoice:I = 0x7f040173

.field public static final svoice_using_svoice_hands_free:I = 0x7f040174

.field public static final svoice_voice_control:I = 0x7f040175

.field public static final svoice_wakeup_command:I = 0x7f040176

.field public static final svoice_wakeup_say:I = 0x7f040177

.field public static final sync_google_account:I = 0x7f040178

.field public static final sync_samsung_account:I = 0x7f040179

.field public static final tethering_bluetooth_tethering:I = 0x7f04017a

.field public static final tethering_bluetooth_tethering_vzw:I = 0x7f04017b

.field public static final tethering_mobile_hotspot:I = 0x7f04017c

.field public static final tethering_mobile_hotspot_vzw:I = 0x7f04017d

.field public static final tethering_portable_wifi_hotspot:I = 0x7f04017e

.field public static final tethering_usb_tethering:I = 0x7f04017f

.field public static final tethering_usb_tethering_tmo:I = 0x7f040180

.field public static final tethering_usb_tethering_vzw:I = 0x7f040181

.field public static final translator_search_phrases:I = 0x7f040182

.field public static final translator_supported_languages:I = 0x7f040183

.field public static final translator_translate_some:I = 0x7f040184

.field public static final tutorial_access_notification_panel_layout:I = 0x7f040185

.field public static final tutorial_access_quick_panel_layout:I = 0x7f040186

.field public static final tutorial_drag_down_layout:I = 0x7f040187

.field public static final tutorial_expand_notification_panel_layout:I = 0x7f040188

.field public static final unlock_add_widget:I = 0x7f040189

.field public static final unlock_changing_wallpaper:I = 0x7f04018a

.field public static final unlock_changing_wallpaper_festival:I = 0x7f04018b

.field public static final unlock_checking_notifications:I = 0x7f04018c

.field public static final unlock_edit_lock_screen:I = 0x7f04018d

.field public static final unlock_expand_widget:I = 0x7f04018e

.field public static final unlock_lock_screen_navigation:I = 0x7f04018f

.field public static final unlock_missed_event:I = 0x7f040190

.field public static final unlock_opening_camera:I = 0x7f040191

.field public static final unlock_set_screen_lock:I = 0x7f040192

.field public static final unlock_set_screen_lock_2014:I = 0x7f040193

.field public static final unlock_unlock_device:I = 0x7f040194

.field public static final upgrade_device_samsung_kies:I = 0x7f040195

.field public static final upgrade_over_the_air:I = 0x7f040196

.field public static final using_assistant_menu:I = 0x7f040197

.field public static final using_babycrying_detectors:I = 0x7f040198

.field public static final using_direct_access:I = 0x7f040199

.field public static final using_download_booster:I = 0x7f04019a

.field public static final using_easy_mode:I = 0x7f04019b

.field public static final using_power_saving_mode:I = 0x7f04019c

.field public static final using_sound_detectors:I = 0x7f04019d

.field public static final using_talkback:I = 0x7f04019e

.field public static final using_ultra_power_saving_mode:I = 0x7f04019f

.field public static final wifi_connect_to_wifi_network:I = 0x7f0401a0

.field public static final wifi_dialog:I = 0x7f0401a1

.field public static final writing_buddy_expand_mode:I = 0x7f0401a2

.field public static final writing_buddy_filling_in_forms:I = 0x7f0401a3

.field public static final writing_buddy_gestures:I = 0x7f0401a4

.field public static final writing_buddy_writing_on_applications:I = 0x7f0401a5


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
