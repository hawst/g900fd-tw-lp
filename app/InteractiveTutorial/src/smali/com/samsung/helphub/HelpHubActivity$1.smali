.class Lcom/samsung/helphub/HelpHubActivity$1;
.super Ljava/lang/Object;
.source "HelpHubActivity.java"

# interfaces
.implements Landroid/app/FragmentManager$OnBackStackChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/HelpHubActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/HelpHubActivity;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/HelpHubActivity;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/helphub/HelpHubActivity$1;->this$0:Lcom/samsung/helphub/HelpHubActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackStackChanged()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 54
    iget-object v2, p0, Lcom/samsung/helphub/HelpHubActivity$1;->this$0:Lcom/samsung/helphub/HelpHubActivity;

    invoke-virtual {v2}, Lcom/samsung/helphub/HelpHubActivity;->isStackEmpty()Z

    move-result v1

    .line 55
    .local v1, "isStackEmpty":Z
    iget-object v2, p0, Lcom/samsung/helphub/HelpHubActivity$1;->this$0:Lcom/samsung/helphub/HelpHubActivity;

    invoke-virtual {v2}, Lcom/samsung/helphub/HelpHubActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 56
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-nez v1, :cond_0

    move v2, v3

    :goto_0
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 57
    if-nez v1, :cond_1

    :goto_1
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 58
    return-void

    :cond_0
    move v2, v4

    .line 56
    goto :goto_0

    :cond_1
    move v3, v4

    .line 57
    goto :goto_1
.end method
