.class public Lcom/samsung/helphub/HelpHubActivityBase;
.super Landroid/app/Activity;
.source "HelpHubActivityBase.java"


# static fields
.field private static isEasyMode:Z


# instance fields
.field private alert:Landroid/app/AlertDialog;

.field private mDialog:Landroid/app/AlertDialog$Builder;

.field private mFragments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Fragment;",
            ">;>;"
        }
    .end annotation
.end field

.field protected mIsExternal:Z

.field private mLastFragment:Landroid/app/Fragment;

.field protected mTwoPane:Z

.field private preservedFragments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/helphub/HelpHubActivityBase;->isEasyMode:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 31
    iput-object v0, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mDialog:Landroid/app/AlertDialog$Builder;

    .line 32
    iput-object v0, p0, Lcom/samsung/helphub/HelpHubActivityBase;->alert:Landroid/app/AlertDialog;

    .line 35
    iput-boolean v1, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mTwoPane:Z

    .line 36
    iput-boolean v1, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mIsExternal:Z

    .line 38
    iput-object v0, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mLastFragment:Landroid/app/Fragment;

    .line 39
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/samsung/helphub/HelpHubActivityBase;->preservedFragments:Ljava/util/HashSet;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mFragments:Ljava/util/List;

    return-void
.end method

.method private makePreservedMapFragments()V
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/samsung/helphub/HelpHubActivityBase;->preservedFragments:Ljava/util/HashSet;

    const-string v1, "com.samsung.helphub.fragments.tablet.HeaderContentsList"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 272
    iget-object v0, p0, Lcom/samsung/helphub/HelpHubActivityBase;->preservedFragments:Ljava/util/HashSet;

    const-string v1, "com.samsung.helphub.fragments.tablet.HelpPageFragment"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 273
    iget-object v0, p0, Lcom/samsung/helphub/HelpHubActivityBase;->preservedFragments:Ljava/util/HashSet;

    const-string v1, "com.samsung.helphub.fragments.tablet.HelpPagesList"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 274
    iget-object v0, p0, Lcom/samsung/helphub/HelpHubActivityBase;->preservedFragments:Ljava/util/HashSet;

    const-string v1, "com.samsung.helphub.fragments.tablet.ExternalHeaderContentsList"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 275
    iget-object v0, p0, Lcom/samsung/helphub/HelpHubActivityBase;->preservedFragments:Ljava/util/HashSet;

    const-string v1, "com.samsung.helphub.fragments.tablet.HeaderContentsSortingList"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 276
    return-void
.end method


# virtual methods
.method public checkCreateByIntent()Z
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x0

    return v0
.end method

.method public getItemNum()I
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x0

    return v0
.end method

.method protected final isStackEmpty()Z
    .locals 1

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTwoPane()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mTwoPane:Z

    return v0
.end method

.method public onAttachFragment(Landroid/app/Fragment;)V
    .locals 2
    .param p1, "fragment"    # Landroid/app/Fragment;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mFragments:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    iput-object p1, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mLastFragment:Landroid/app/Fragment;

    .line 71
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v2, 0x7f0d0004

    .line 122
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 123
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->isTwoPane()Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 126
    invoke-virtual {p0, v2}, Lcom/samsung/helphub/HelpHubActivityBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f020165

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    invoke-virtual {p0, v2}, Lcom/samsung/helphub/HelpHubActivityBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f020163

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 43
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v2, 0x7f0400cb

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/HelpHubActivityBase;->setContentView(I)V

    .line 50
    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 51
    const v2, 0x7f0d0004

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/HelpHubActivityBase;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 52
    iput-boolean v0, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mTwoPane:Z

    .line 59
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "easy_mode_switch"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_2

    :goto_1
    sput-boolean v0, Lcom/samsung/helphub/HelpHubActivityBase;->isEasyMode:Z

    .line 60
    invoke-direct {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->makePreservedMapFragments()V

    .line 61
    return-void

    .line 55
    :cond_1
    const v2, 0x7f0d0087

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/HelpHubActivityBase;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 56
    iput-boolean v0, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mTwoPane:Z

    goto :goto_0

    :cond_2
    move v0, v1

    .line 59
    goto :goto_1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 210
    const/4 v1, 0x0

    .line 211
    .local v1, "result":Z
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const v3, 0x7f0d0004

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 213
    .local v0, "fragment":Landroid/app/Fragment;
    instance-of v2, v0, Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;

    if-eqz v2, :cond_0

    move-object v2, v0

    .line 214
    check-cast v2, Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;

    invoke-interface {v2, p1, p2}, Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 216
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mTwoPane:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    if-nez v1, :cond_1

    .line 217
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const v3, 0x7f0d0089

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 219
    instance-of v2, v0, Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;

    if-eqz v2, :cond_1

    move-object v2, v0

    .line 220
    check-cast v2, Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;

    invoke-interface {v2, p1, p2}, Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 223
    :cond_1
    if-nez v1, :cond_2

    .line 224
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 226
    :cond_2
    return v1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 186
    const/4 v2, 0x0

    .line 187
    .local v2, "result":Z
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const v4, 0x7f0d0004

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    .line 189
    .local v1, "fragment":Landroid/app/Fragment;
    instance-of v3, v1, Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;

    if-eqz v3, :cond_0

    move-object v3, v1

    .line 190
    check-cast v3, Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;

    invoke-interface {v3, p1, p2}, Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    .line 192
    :cond_0
    iget-boolean v3, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mTwoPane:Z

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    if-nez v2, :cond_1

    .line 193
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const v4, 0x7f0d0089

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    .line 195
    instance-of v3, v1, Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;

    if-eqz v3, :cond_1

    move-object v3, v1

    .line 196
    check-cast v3, Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;

    invoke-interface {v3, p1, p2}, Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    .line 199
    :cond_1
    if-nez v2, :cond_2

    .line 201
    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 206
    :cond_2
    :goto_0
    return v2

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x0

    .line 109
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 110
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "easy_mode_switch"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    sput-boolean v0, Lcom/samsung/helphub/HelpHubActivityBase;->isEasyMode:Z

    .line 111
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 138
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 140
    iget-boolean v6, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mIsExternal:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mFragments:Ljava/util/List;

    if-eqz v6, :cond_0

    .line 141
    iget-object v6, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mFragments:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-gtz v6, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->finish()V

    .line 143
    new-instance v1, Landroid/content/Intent;

    const-class v6, Lcom/samsung/helphub/HelpHubActivity;

    invoke-direct {v1, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 144
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/HelpHubActivityBase;->startActivity(Landroid/content/Intent;)V

    .line 148
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "easy_mode_switch"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-nez v6, :cond_2

    move v2, v4

    .line 149
    .local v2, "mState":Z
    :goto_0
    const v6, 0x7f0a06bc

    invoke-virtual {p0, v6}, Lcom/samsung/helphub/HelpHubActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 155
    .local v0, "extraString":Ljava/lang/String;
    const v6, 0x7f0a06ba

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {p0, v6, v4}, Lcom/samsung/helphub/HelpHubActivityBase;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 158
    .local v3, "popupMessage":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 159
    if-eqz v2, :cond_1

    .line 160
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mDialog:Landroid/app/AlertDialog$Builder;

    .line 161
    iget-object v4, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mDialog:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/helphub/HelpHubActivityBase;->alert:Landroid/app/AlertDialog;

    .line 162
    iget-object v4, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mDialog:Landroid/app/AlertDialog$Builder;

    const v6, 0x7f0a001e

    invoke-virtual {v4, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v6, 0x7f0a0064

    new-instance v7, Lcom/samsung/helphub/HelpHubActivityBase$1;

    invoke-direct {v7, p0}, Lcom/samsung/helphub/HelpHubActivityBase$1;-><init>(Lcom/samsung/helphub/HelpHubActivityBase;)V

    invoke-virtual {v4, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 172
    iget-object v4, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mDialog:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/helphub/HelpHubActivityBase;->alert:Landroid/app/AlertDialog;

    .line 181
    :cond_1
    :goto_1
    return-void

    .end local v0    # "extraString":Ljava/lang/String;
    .end local v2    # "mState":Z
    .end local v3    # "popupMessage":Ljava/lang/String;
    :cond_2
    move v2, v5

    .line 148
    goto :goto_0

    .line 174
    .restart local v0    # "extraString":Ljava/lang/String;
    .restart local v2    # "mState":Z
    .restart local v3    # "popupMessage":Ljava/lang/String;
    :cond_3
    sget-boolean v4, Lcom/samsung/helphub/HelpHubActivityBase;->isEasyMode:Z

    if-eq v4, v2, :cond_1

    .line 177
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->finish()V

    .line 178
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/samsung/helphub/HelpHubActivity;

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 179
    .restart local v1    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/HelpHubActivityBase;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x0

    .line 115
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 116
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "easy_mode_switch"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    sput-boolean v0, Lcom/samsung/helphub/HelpHubActivityBase;->isEasyMode:Z

    .line 118
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 96
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 97
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "easy_mode_switch"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 98
    .local v0, "mState1":Z
    :cond_0
    if-nez v0, :cond_1

    sget-boolean v1, Lcom/samsung/helphub/HelpHubActivityBase;->isEasyMode:Z

    if-eq v1, v0, :cond_1

    .line 101
    iget-object v1, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mDialog:Landroid/app/AlertDialog$Builder;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/helphub/HelpHubActivityBase;->alert:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    .line 102
    iget-object v1, p0, Lcom/samsung/helphub/HelpHubActivityBase;->alert:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->cancel()V

    .line 105
    :cond_1
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 6
    .param p1, "level"    # I

    .prologue
    .line 75
    const/16 v5, 0x28

    if-lt p1, v5, :cond_2

    iget-boolean v5, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mIsExternal:Z

    if-nez v5, :cond_2

    .line 76
    iget-object v5, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mFragments:Ljava/util/List;

    if-eqz v5, :cond_2

    .line 77
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 78
    .local v4, "transaction":Landroid/app/FragmentTransaction;
    iget-object v5, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mFragments:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    .line 79
    .local v3, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/app/Fragment;>;"
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    .line 80
    .local v0, "fragment":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 82
    .local v1, "fragmentName":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/helphub/HelpHubActivityBase;->preservedFragments:Ljava/util/HashSet;

    invoke-virtual {v5, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mLastFragment:Landroid/app/Fragment;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 83
    invoke-virtual {v4, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 84
    iget-object v5, p0, Lcom/samsung/helphub/HelpHubActivityBase;->mFragments:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 88
    .end local v0    # "fragment":Landroid/app/Fragment;
    .end local v1    # "fragmentName":Ljava/lang/String;
    .end local v3    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/app/Fragment;>;"
    :cond_1
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 91
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "transaction":Landroid/app/FragmentTransaction;
    :cond_2
    return-void
.end method

.method public processIntent()V
    .locals 0

    .prologue
    .line 268
    return-void
.end method

.method protected queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 243
    const/4 v6, 0x0

    .line 244
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 246
    .local v7, "result":Lcom/samsung/helphub/headers/HelpHeader;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 248
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    invoke-static {v6}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 252
    :cond_0
    if-eqz v6, :cond_1

    .line 253
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 256
    :cond_1
    return-object v7

    .line 252
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 253
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method
