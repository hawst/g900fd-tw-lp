.class public final Lcom/samsung/helphub/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final SS_YOU_CAN_SAY_PS_WHEN_IN_S_VOICE:I = 0x7f0a088f

.field public static final SS_YOU_CAN_SAY_PS_WHEN_IN_S_VOICE_OR_WHILE_THE_SCREEN_IS_OFF_MSG:I = 0x7f0a088e

.field public static final actionmemo_help_item_action_link_title:I = 0x7f0a07f4

.field public static final actionmemo_help_item_dot:I = 0x7f0a07f7

.field public static final actionmemo_help_item_launch_body:I = 0x7f0a07f5

.field public static final actionmemo_help_item_launch_title:I = 0x7f0a07f3

.field public static final actionmemo_help_item_link_browser:I = 0x7f0a07fc

.field public static final actionmemo_help_item_link_call:I = 0x7f0a07f8

.field public static final actionmemo_help_item_link_contact:I = 0x7f0a07f9

.field public static final actionmemo_help_item_link_email:I = 0x7f0a07fb

.field public static final actionmemo_help_item_link_map:I = 0x7f0a07fd

.field public static final actionmemo_help_item_link_message:I = 0x7f0a07fa

.field public static final actionmemo_help_item_link_task:I = 0x7f0a07fe

.field public static final actionmemo_help_item_link_text_1:I = 0x7f0a07f6

.field public static final adding_contact_desc1_1:I = 0x7f0a09d6

.field public static final adding_contact_desc1_1_tts:I = 0x7f0a09d7

.field public static final adding_contact_desc1_2:I = 0x7f0a09d8

.field public static final adding_contact_desc2_1:I = 0x7f0a09dc

.field public static final adding_contact_desc2_2:I = 0x7f0a09dd

.field public static final adding_contact_desc3_1:I = 0x7f0a09df

.field public static final adding_contact_desc3_2:I = 0x7f0a09e0

.field public static final adding_contact_desc3_2_tts:I = 0x7f0a09e1

.field public static final adding_contact_header1:I = 0x7f0a09d5

.field public static final adding_contact_header2:I = 0x7f0a09db

.field public static final adding_contact_header3:I = 0x7f0a09de

.field public static final adding_contact_tip1:I = 0x7f0a09d9

.field public static final adding_contact_tip1_tts:I = 0x7f0a09da

.field public static final air_button_help_item_contact_recipient_insert:I = 0x7f0a0770

.field public static final air_button_help_item_contact_recipient_insert_text:I = 0x7f0a0a2a

.field public static final air_button_help_item_display_functional_menu:I = 0x7f0a0771

.field public static final air_button_help_item_display_functional_menu_text:I = 0x7f0a0a2b

.field public static final air_button_help_item_display_pen_launcher:I = 0x7f0a0772

.field public static final air_button_help_item_display_pen_launcher_text:I = 0x7f0a0774

.field public static final air_button_help_item_general_air_command:I = 0x7f0a0773

.field public static final air_button_help_item_insert_attach_contents:I = 0x7f0a076f

.field public static final air_button_help_item_introduction_to_air_button:I = 0x7f0a076e

.field public static final air_button_help_item_using_features_in_air_command:I = 0x7f0a078c

.field public static final air_button_help_item_using_features_in_air_command_easyclip_more_info_text:I = 0x7f0a0790

.field public static final air_button_help_item_using_features_in_air_command_easyclip_text:I = 0x7f0a078f

.field public static final air_button_help_item_using_features_in_air_command_image_clip_text_00:I = 0x7f0a07a3

.field public static final air_button_help_item_using_features_in_air_command_image_clip_text_01:I = 0x7f0a07a4

.field public static final air_button_help_item_using_features_in_air_command_image_clip_text_01_tts:I = 0x7f0a07a5

.field public static final air_button_help_item_using_features_in_air_command_penable_more_info_text:I = 0x7f0a078e

.field public static final air_button_help_item_using_features_in_air_command_penable_text:I = 0x7f0a078d

.field public static final air_button_help_item_using_features_in_air_command_smart_select_text_00:I = 0x7f0a0795

.field public static final air_button_help_item_using_features_in_air_command_smart_select_text_01:I = 0x7f0a0796

.field public static final air_button_help_item_using_features_in_air_command_smart_select_text_01_tts:I = 0x7f0a0797

.field public static final air_button_help_item_using_features_in_air_command_smart_select_text_02:I = 0x7f0a0798

.field public static final air_button_help_item_using_features_in_air_command_smart_select_text_03:I = 0x7f0a0799

.field public static final air_button_help_item_using_features_in_air_command_smart_select_text_03_tts:I = 0x7f0a079a

.field public static final air_button_help_item_using_features_in_air_command_smart_select_text_04:I = 0x7f0a079e

.field public static final air_button_help_item_using_features_in_air_command_smart_select_text_05:I = 0x7f0a079f

.field public static final air_button_help_item_using_features_in_air_command_smart_select_text_tip_1_1:I = 0x7f0a079b

.field public static final air_button_help_item_using_features_in_air_command_smart_select_text_tip_1_2:I = 0x7f0a079c

.field public static final air_button_help_item_using_features_in_air_command_smart_select_text_tip_1_3:I = 0x7f0a079d

.field public static final air_button_help_item_using_features_in_air_command_smart_select_text_tip_2_1:I = 0x7f0a07a0

.field public static final air_button_help_item_using_features_in_air_command_smart_select_text_tip_2_2:I = 0x7f0a07a1

.field public static final air_button_help_item_using_features_in_air_command_smart_select_text_tip_2_2_tts:I = 0x7f0a07a2

.field public static final air_button_help_item_using_features_in_air_command_smart_select_title_00:I = 0x7f0a0792

.field public static final air_button_help_item_using_features_in_air_command_smart_select_title_01:I = 0x7f0a0793

.field public static final air_button_help_item_using_features_in_air_command_smart_select_title_02:I = 0x7f0a0794

.field public static final air_button_help_item_using_features_in_air_command_title:I = 0x7f0a0791

.field public static final air_button_introduction_action_memo_description:I = 0x7f0a0776

.field public static final air_button_introduction_additional_features:I = 0x7f0a0a2e

.field public static final air_button_introduction_additional_features_description:I = 0x7f0a0a2f

.field public static final air_button_introduction_easy_clip_description:I = 0x7f0a0779

.field public static final air_button_introduction_pen_window_description:I = 0x7f0a0a2d

.field public static final air_button_introduction_scrap_booker_description:I = 0x7f0a0777

.field public static final air_button_introduction_screen_write_description:I = 0x7f0a0778

.field public static final air_button_introduction_sfinder_description:I = 0x7f0a0a2c

.field public static final air_button_introduction_to_use_air_command:I = 0x7f0a0775

.field public static final air_button_options_disable_summary:I = 0x7f0a07a7

.field public static final air_button_setting_dialog_air_button_title:I = 0x7f0a07a6

.field public static final air_command_help_item_adding_recipients:I = 0x7f0a0a29

.field public static final air_command_help_item_inserting_attaching_content:I = 0x7f0a0a27

.field public static final air_command_help_item_inserting_attaching_text:I = 0x7f0a0a28

.field public static final air_command_help_item_using_other_air_command_features:I = 0x7f0a078b

.field public static final air_gesture_air_browse_description:I = 0x7f0a0490

.field public static final air_gesture_air_browse_gallery:I = 0x7f0a0491

.field public static final air_gesture_air_browse_internet:I = 0x7f0a0492

.field public static final air_gesture_air_browse_music:I = 0x7f0a0493

.field public static final air_gesture_air_browse_music_lock:I = 0x7f0a0494

.field public static final air_gesture_air_browse_smemo:I = 0x7f0a0495

.field public static final air_gesture_air_browse_snote:I = 0x7f0a0496

.field public static final air_gesture_air_call_accept_description:I = 0x7f0a049d

.field public static final air_gesture_air_jump_email_list:I = 0x7f0a048f

.field public static final air_gesture_air_jump_scroll_list:I = 0x7f0a048d

.field public static final air_gesture_air_jump_web_pages:I = 0x7f0a048e

.field public static final air_gesture_air_move_calendar:I = 0x7f0a049c

.field public static final air_gesture_air_move_home_applist:I = 0x7f0a0499

.field public static final air_gesture_air_move_home_screen:I = 0x7f0a0498

.field public static final air_gesture_air_move_scalendar:I = 0x7f0a049a

.field public static final air_gesture_air_move_splanner:I = 0x7f0a049b

.field public static final air_gesture_air_move_use_one_hand:I = 0x7f0a0497

.field public static final air_gesture_indicator_cm:I = 0x7f0a048a

.field public static final air_gesture_indicator_description:I = 0x7f0a048b

.field public static final air_gesture_indicator_recognizes:I = 0x7f0a0489

.field public static final air_gesture_indicator_sensor_position:I = 0x7f0a0488

.field public static final air_gesture_put_device_screen_upwards:I = 0x7f0a048c

.field public static final air_view_information_preview_calendar:I = 0x7f0a0525

.field public static final air_view_information_preview_calendar_new:I = 0x7f0a0526

.field public static final air_view_information_preview_gallery:I = 0x7f0a0528

.field public static final air_view_information_preview_gallery_text:I = 0x7f0a0529

.field public static final air_view_information_preview_messaging:I = 0x7f0a052a

.field public static final air_view_information_preview_messaging_text:I = 0x7f0a052b

.field public static final air_view_information_preview_preview_information:I = 0x7f0a0523

.field public static final air_view_information_preview_splanner_text_switchable:I = 0x7f0a0527

.field public static final air_view_introduction_when_finger_tip:I = 0x7f0a0522

.field public static final air_view_magnifier_hovers_screen:I = 0x7f0a01a0

.field public static final air_view_pen_dialog_finger_mode_body:I = 0x7f0a01b3

.field public static final air_view_pen_dialog_finger_mode_function_body:I = 0x7f0a01b6

.field public static final air_view_pen_dialog_pen_finger_mode_body:I = 0x7f0a01b4

.field public static final air_view_pen_dialog_pen_finger_mode_function_body:I = 0x7f0a01b7

.field public static final air_view_pen_dialog_pen_mode_body:I = 0x7f0a01b2

.field public static final air_view_pen_dialog_pen_mode_function_body:I = 0x7f0a01b5

.field public static final air_view_pen_dialog_pen_mode_icon_label_body:I = 0x7f0a01b8

.field public static final air_view_pen_dialog_pen_mode_list_scrolling_body:I = 0x7f0a01b9

.field public static final air_view_pen_dialog_talkback_body:I = 0x7f0a01b1

.field public static final air_view_pen_dialog_title:I = 0x7f0a01b0

.field public static final air_view_pen_icon_labels:I = 0x7f0a01ac

.field public static final air_view_pen_insert_spen:I = 0x7f0a01ae

.field public static final air_view_pen_list_scrolling:I = 0x7f0a01ad

.field public static final air_view_pen_only_dialog_air_view_body:I = 0x7f0a01bc

.field public static final air_view_pen_only_dialog_function_body:I = 0x7f0a01bd

.field public static final air_view_pen_only_dialog_talkback_body:I = 0x7f0a01bb

.field public static final air_view_pen_only_information_preview_description:I = 0x7f0a0524

.field public static final air_view_pen_progress_preview_description:I = 0x7f0a01a8

.field public static final air_view_pen_progress_preview_description_pen:I = 0x7f0a01a9

.field public static final air_view_pen_pull_out_spen:I = 0x7f0a01af

.field public static final air_view_pen_speed_dial_preview_description:I = 0x7f0a01aa

.field public static final air_view_pen_speed_dial_preview_description_for_pen:I = 0x7f0a01ab

.field public static final air_view_pointer_when_finger_tip:I = 0x7f0a051f

.field public static final air_view_progress_bar_hover_over_progress_bar:I = 0x7f0a0520

.field public static final air_view_speed_dial_linked_to_contact:I = 0x7f0a0521

.field public static final alert:I = 0x7f0a0586

.field public static final allshare_howto_cast_dongle_connect_dongle:I = 0x7f0a03db

.field public static final allshare_howto_cast_dongle_press_reset:I = 0x7f0a03dc

.field public static final allshare_howto_cast_dongle_select_product:I = 0x7f0a03de

.field public static final allshare_howto_cast_dongle_start_allsharecast:I = 0x7f0a03dd

.field public static final allshare_learn_nfc_Moreinfo_notice:I = 0x7f0a03e3

.field public static final allshare_learn_nfc_follow_dongle_connection_step:I = 0x7f0a03e1

.field public static final allshare_learn_nfc_link_nfc_information:I = 0x7f0a03e2

.field public static final allshare_learn_nfc_start_allsharecast:I = 0x7f0a03df

.field public static final allshare_learn_nfc_touch_nfc_sticker:I = 0x7f0a03e0

.field public static final allshare_learn_smarttv_moreinfo:I = 0x7f0a03e6

.field public static final allshare_learn_smarttv_moreinfo_notice:I = 0x7f0a03e7

.field public static final allshare_learn_smarttv_start_allsharecast_1:I = 0x7f0a03e4

.field public static final allshare_learn_smarttv_start_allsharecast_2:I = 0x7f0a03e5

.field public static final app_name:I = 0x7f0a001e

.field public static final bullet:I = 0x7f0a006a

.field public static final button_text_cancel:I = 0x7f0a0063

.field public static final button_text_ok:I = 0x7f0a0064

.field public static final button_text_step_by_step:I = 0x7f0a006c

.field public static final button_text_try:I = 0x7f0a02c7

.field public static final button_text_try_it:I = 0x7f0a006d

.field public static final button_text_try_local:I = 0x7f0a02c8

.field public static final button_text_user_manual:I = 0x7f0a006e

.field public static final call_accept_reject_tip1:I = 0x7f0a0457

.field public static final call_accept_reject_tip2:I = 0x7f0a0458

.field public static final call_accept_reject_tip2_tts:I = 0x7f0a046e

.field public static final call_drug_up_panel:I = 0x7f0a044f

.field public static final call_drug_up_panel_for_kk:I = 0x7f0a0462

.field public static final call_drug_up_panel_for_kk_tts:I = 0x7f0a046b

.field public static final call_incoming_voice_call2:I = 0x7f0a0459

.field public static final call_incoming_voice_call2_tts:I = 0x7f0a046c

.field public static final call_incoming_voice_call3:I = 0x7f0a045a

.field public static final call_incoming_voice_call3_tts:I = 0x7f0a046d

.field public static final call_incoming_voice_call_for_2nd_lcd_1:I = 0x7f0a045e

.field public static final call_incoming_voice_call_for_2nd_lcd_1_tts:I = 0x7f0a0460

.field public static final call_incoming_voice_call_for_2nd_lcd_2:I = 0x7f0a045f

.field public static final call_incoming_voice_call_for_2nd_lcd_2_tts:I = 0x7f0a0461

.field public static final call_incoming_voice_call_for_kk_1:I = 0x7f0a045c

.field public static final call_incoming_voice_call_for_kk_1_tts:I = 0x7f0a0469

.field public static final call_incoming_voice_call_for_kk_2:I = 0x7f0a045d

.field public static final call_incoming_voice_call_for_kk_2_tts:I = 0x7f0a046a

.field public static final call_incoming_voice_title2:I = 0x7f0a045b

.field public static final call_menu_during_call_detail:I = 0x7f0a0450

.field public static final call_menu_during_call_list1:I = 0x7f0a0451

.field public static final call_menu_during_call_list1_tts:I = 0x7f0a046f

.field public static final call_menu_during_call_list2:I = 0x7f0a0452

.field public static final call_menu_during_call_list2_tts:I = 0x7f0a0470

.field public static final call_menu_during_call_list3:I = 0x7f0a0453

.field public static final call_menu_during_call_list3_tts:I = 0x7f0a0471

.field public static final call_menu_during_call_list4:I = 0x7f0a0454

.field public static final call_menu_during_call_list4_tts:I = 0x7f0a0472

.field public static final call_menu_during_call_list5:I = 0x7f0a0455

.field public static final call_menu_during_call_list5_tts:I = 0x7f0a0473

.field public static final call_menu_during_call_list6:I = 0x7f0a0456

.field public static final call_menu_during_call_list6_tts:I = 0x7f0a0474

.field public static final camera_auto:I = 0x7f0a03bd

.field public static final camera_beauty:I = 0x7f0a03bc

.field public static final camera_camera_modes_choose_modes:I = 0x7f0a037a

.field public static final camera_changing_the_camera_mode_manage_mode:I = 0x7f0a0391

.field public static final camera_changing_the_camera_mode_mode:I = 0x7f0a0390

.field public static final camera_changing_the_settings_text_1:I = 0x7f0a039e

.field public static final camera_changing_the_settings_text_1_tts:I = 0x7f0a039f

.field public static final camera_changing_the_settings_text_2:I = 0x7f0a03a0

.field public static final camera_creating_setting_edit_quick_settings:I = 0x7f0a03aa

.field public static final camera_creating_setting_more_tip:I = 0x7f0a03a9

.field public static final camera_creating_setting_shortcut:I = 0x7f0a03a1

.field public static final camera_creating_setting_shortcut_text_1:I = 0x7f0a03a2

.field public static final camera_creating_setting_shortcut_text_1_tts:I = 0x7f0a03a3

.field public static final camera_creating_setting_shortcut_text_2:I = 0x7f0a03a4

.field public static final camera_creating_setting_shortcut_text_3:I = 0x7f0a03a5

.field public static final camera_creating_setting_shortcut_text_3_tts:I = 0x7f0a03a6

.field public static final camera_creating_setting_shortcut_tip_1:I = 0x7f0a03a7

.field public static final camera_creating_setting_shortcut_tip_2:I = 0x7f0a03a8

.field public static final camera_dual:I = 0x7f0a03bf

.field public static final camera_gifmaker:I = 0x7f0a03c4

.field public static final camera_introduction_change_mode:I = 0x7f0a0375

.field public static final camera_introduction_change_mode2:I = 0x7f0a0376

.field public static final camera_introduction_change_setting:I = 0x7f0a036c

.field public static final camera_introduction_display_mode:I = 0x7f0a036d

.field public static final camera_introduction_flash:I = 0x7f0a0371

.field public static final camera_introduction_hdr:I = 0x7f0a0379

.field public static final camera_introduction_learn_about_camera_mode:I = 0x7f0a0370

.field public static final camera_introduction_open_effects_panel:I = 0x7f0a0373

.field public static final camera_introduction_open_quick_settings:I = 0x7f0a0372

.field public static final camera_introduction_selective_focus:I = 0x7f0a0378

.field public static final camera_introduction_show_gallery:I = 0x7f0a036e

.field public static final camera_introduction_show_gallery2:I = 0x7f0a036f

.field public static final camera_introduction_start_recording:I = 0x7f0a0377

.field public static final camera_introduction_switch_camera:I = 0x7f0a036a

.field public static final camera_introduction_take_pictures:I = 0x7f0a0374

.field public static final camera_introduction_turn_dual_camera:I = 0x7f0a036b

.field public static final camera_learn_more_about_camera_mode:I = 0x7f0a039a

.field public static final camera_learn_more_about_camera_mode_text_1:I = 0x7f0a039b

.field public static final camera_learn_more_about_camera_mode_text_1_tts:I = 0x7f0a039c

.field public static final camera_learn_more_about_camera_mode_text_2:I = 0x7f0a039d

.field public static final camera_learn_more_about_mode_auto:I = 0x7f0a03ab

.field public static final camera_learn_more_about_mode_beauty:I = 0x7f0a03ac

.field public static final camera_learn_more_about_mode_bestface:I = 0x7f0a03b2

.field public static final camera_learn_more_about_mode_bestphoto:I = 0x7f0a03b1

.field public static final camera_learn_more_about_mode_drama:I = 0x7f0a03b3

.field public static final camera_learn_more_about_mode_dual:I = 0x7f0a03ae

.field public static final camera_learn_more_about_mode_eraser:I = 0x7f0a03b4

.field public static final camera_learn_more_about_mode_gifmaker:I = 0x7f0a03b9

.field public static final camera_learn_more_about_mode_panningshot:I = 0x7f0a03b5

.field public static final camera_learn_more_about_mode_panorama:I = 0x7f0a03ad

.field public static final camera_learn_more_about_mode_selectivefocus:I = 0x7f0a03ba

.field public static final camera_learn_more_about_mode_selfie:I = 0x7f0a03b8

.field public static final camera_learn_more_about_mode_selfiealarm:I = 0x7f0a03b7

.field public static final camera_learn_more_about_mode_shotandmore:I = 0x7f0a03b0

.field public static final camera_learn_more_about_mode_virtualtour:I = 0x7f0a03af

.field public static final camera_learn_more_about_mode_wideselfie:I = 0x7f0a03b6

.field public static final camera_panorama:I = 0x7f0a03bb

.field public static final camera_record_videos_record_videos:I = 0x7f0a0387

.field public static final camera_recording_videos_text_1:I = 0x7f0a0388

.field public static final camera_recording_videos_text_1_tts:I = 0x7f0a0389

.field public static final camera_recording_videos_text_2:I = 0x7f0a038a

.field public static final camera_recording_videos_text_2_tts:I = 0x7f0a038b

.field public static final camera_recording_videos_tip_snapshot:I = 0x7f0a038c

.field public static final camera_recording_videos_tip_snapshot_tts:I = 0x7f0a038d

.field public static final camera_selecting_camera_mode:I = 0x7f0a0392

.field public static final camera_selecting_camera_mode_text_1:I = 0x7f0a0393

.field public static final camera_selecting_camera_mode_text_1_not_bold:I = 0x7f0a0394

.field public static final camera_selecting_camera_mode_text_2:I = 0x7f0a0395

.field public static final camera_selecting_camera_mode_tip_1:I = 0x7f0a0396

.field public static final camera_selecting_camera_mode_tip_1_tts:I = 0x7f0a0397

.field public static final camera_selecting_camera_mode_tip_2:I = 0x7f0a0398

.field public static final camera_selecting_camera_mode_tip_2_tts:I = 0x7f0a0399

.field public static final camera_selectivefocus:I = 0x7f0a03c5

.field public static final camera_selfie:I = 0x7f0a03c1

.field public static final camera_selfiealarm:I = 0x7f0a03c2

.field public static final camera_shotandmore:I = 0x7f0a03be

.field public static final camera_take_picture_take_pictures:I = 0x7f0a037b

.field public static final camera_taking_pictures_text2_2:I = 0x7f0a037d

.field public static final camera_taking_pictures_text2_2_tts:I = 0x7f0a037e

.field public static final camera_taking_pictures_text_1:I = 0x7f0a037c

.field public static final camera_taking_pictures_tip_exposure:I = 0x7f0a0384

.field public static final camera_taking_pictures_tip_exposure_tts:I = 0x7f0a0385

.field public static final camera_taking_pictures_tip_focus:I = 0x7f0a037f

.field public static final camera_taking_pictures_tip_focus_tts:I = 0x7f0a0380

.field public static final camera_taking_pictures_tip_selective_focus:I = 0x7f0a0381

.field public static final camera_taking_pictures_tip_switch_camera:I = 0x7f0a0382

.field public static final camera_taking_pictures_tip_switch_camera_tts:I = 0x7f0a0383

.field public static final camera_taking_pictures_tip_volumekey:I = 0x7f0a0386

.field public static final camera_viewing_pics_and_videos_text_1:I = 0x7f0a038e

.field public static final camera_viewing_pics_and_videos_text_1_tts:I = 0x7f0a038f

.field public static final camera_virtualtour:I = 0x7f0a03c0

.field public static final camera_wideselfie:I = 0x7f0a03c3

.field public static final car_mode_help_title_ans_rej_call:I = 0x7f0a096f

.field public static final car_mode_help_title_making_call:I = 0x7f0a096e

.field public static final car_mode_help_title_returning_to_car_mode:I = 0x7f0a096d

.field public static final car_mode_help_title_sending_msg:I = 0x7f0a0970

.field public static final car_mode_help_title_setting_dest:I = 0x7f0a0971

.field public static final car_mode_registering_cars_3_talkback:I = 0x7f0a099b

.field public static final carmode_ans_rej_call_1:I = 0x7f0a0983

.field public static final carmode_ans_rej_call_2:I = 0x7f0a0984

.field public static final carmode_ans_rej_call_tip_1:I = 0x7f0a0985

.field public static final carmode_ans_rej_call_tip_2:I = 0x7f0a0986

.field public static final carmode_ans_rej_call_tip_3:I = 0x7f0a0987

.field public static final carmode_enabling_carmode_1:I = 0x7f0a0978

.field public static final carmode_enabling_carmode_1_talkback:I = 0x7f0a0993

.field public static final carmode_enabling_carmode_2:I = 0x7f0a0979

.field public static final carmode_enabling_carmode_2_talkback:I = 0x7f0a0994

.field public static final carmode_enabling_carmode_tip:I = 0x7f0a097a

.field public static final carmode_making_call_01:I = 0x7f0a0996

.field public static final carmode_making_call_1:I = 0x7f0a097e

.field public static final carmode_making_call_2:I = 0x7f0a097f

.field public static final carmode_making_call_3:I = 0x7f0a0980

.field public static final carmode_making_call_tip_1:I = 0x7f0a0981

.field public static final carmode_making_call_tip_2:I = 0x7f0a0982

.field public static final carmode_register_carmode_1:I = 0x7f0a0972

.field public static final carmode_register_carmode_1_talkback:I = 0x7f0a0995

.field public static final carmode_register_carmode_2:I = 0x7f0a0973

.field public static final carmode_register_carmode_3:I = 0x7f0a0974

.field public static final carmode_register_carmode_4:I = 0x7f0a0975

.field public static final carmode_register_carmode_5:I = 0x7f0a0976

.field public static final carmode_register_carmode_tip:I = 0x7f0a0977

.field public static final carmode_returning_to_carmode_1:I = 0x7f0a097b

.field public static final carmode_returning_to_carmode_2:I = 0x7f0a097c

.field public static final carmode_returning_to_carmode_tip:I = 0x7f0a097d

.field public static final carmode_sending_msg_01:I = 0x7f0a0997

.field public static final carmode_sending_msg_2:I = 0x7f0a0988

.field public static final carmode_sending_msg_3:I = 0x7f0a0989

.field public static final carmode_sending_msg_4:I = 0x7f0a098a

.field public static final carmode_sending_msg_5:I = 0x7f0a098b

.field public static final carmode_sending_msg_tip_1:I = 0x7f0a098c

.field public static final carmode_sending_msg_tip_2:I = 0x7f0a098d

.field public static final carmode_setting_dest_01:I = 0x7f0a0998

.field public static final carmode_setting_dest_02:I = 0x7f0a0999

.field public static final carmode_setting_dest_2:I = 0x7f0a098e

.field public static final carmode_setting_dest_3:I = 0x7f0a098f

.field public static final carmode_setting_dest_4:I = 0x7f0a0990

.field public static final carmode_setting_dest_tip_1:I = 0x7f0a0991

.field public static final carmode_setting_dest_tip_2:I = 0x7f0a0992

.field public static final carmode_setting_dest_tip_3:I = 0x7f0a099a

.field public static final carrier_vzw_basic_title_calling:I = 0x7f0a05a8

.field public static final carrier_vzw_basic_title_calling_and_messaging:I = 0x7f0a05a7

.field public static final carrier_vzw_basic_title_device_navigation:I = 0x7f0a05a6

.field public static final carrier_vzw_basic_title_email_setup:I = 0x7f0a05aa

.field public static final carrier_vzw_basic_title_texting_with_verizon_messages:I = 0x7f0a05a9

.field public static final carrier_vzw_basic_title_wifi_and_bt:I = 0x7f0a05ab

.field public static final carrier_vzw_basic_title_wifi_enhanced:I = 0x7f0a05ac

.field public static final carrier_vzw_howto_fragment_additional_video_caption:I = 0x7f0a05ae

.field public static final carrier_vzw_howto_fragment_basic_video_caption:I = 0x7f0a05ad

.field public static final carrier_vzw_howto_fragment_my_verizon_mobile_video_caption:I = 0x7f0a05af

.field public static final carrier_vzw_howto_item_sub_title_basic_video:I = 0x7f0a059f

.field public static final carrier_vzw_howto_item_summary_additional_video:I = 0x7f0a05a3

.field public static final carrier_vzw_howto_item_summary_basic_video:I = 0x7f0a05a1

.field public static final carrier_vzw_howto_item_summary_my_verizon_mobile_video:I = 0x7f0a05a5

.field public static final carrier_vzw_howto_item_title_additional_video:I = 0x7f0a05a2

.field public static final carrier_vzw_howto_item_title_basic_video:I = 0x7f0a05a0

.field public static final carrier_vzw_howto_item_title_my_verizon_mobile_video:I = 0x7f0a05a4

.field public static final category_accessories:I = 0x7f0a0134

.field public static final category_applications:I = 0x7f0a0131

.field public static final category_basics:I = 0x7f0a0130

.field public static final category_enjoying_new_features:I = 0x7f0a012e

.field public static final category_enjoying_new_features_2nd_line:I = 0x7f0a012c

.field public static final category_enjoying_new_features_summary:I = 0x7f0a012d

.field public static final category_getting_started:I = 0x7f0a012f

.field public static final category_how_to_videos:I = 0x7f0a05b0

.field public static final category_icon_glossary:I = 0x7f0a05b5

.field public static final category_my_verizon_mobile:I = 0x7f0a05b2

.field public static final category_my_verizon_mobile_video:I = 0x7f0a05b1

.field public static final category_online_help:I = 0x7f0a0133

.field public static final category_settings:I = 0x7f0a0132

.field public static final category_useful_tips:I = 0x7f0a05b3

.field public static final category_user_manual:I = 0x7f0a05b4

.field public static final chartbuilder_help_item_changing_chart_type:I = 0x7f0a080e

.field public static final chartbuilder_help_item_chart_frame_edit:I = 0x7f0a0803

.field public static final chartbuilder_help_item_chart_frame_edit_text_1:I = 0x7f0a0808

.field public static final chartbuilder_help_item_create_bar_chart:I = 0x7f0a0800

.field public static final chartbuilder_help_item_create_bar_chart_text_1:I = 0x7f0a0805

.field public static final chartbuilder_help_item_create_chart_title:I = 0x7f0a0809

.field public static final chartbuilder_help_item_create_line_chart:I = 0x7f0a07ff

.field public static final chartbuilder_help_item_create_line_chart_text_1:I = 0x7f0a0804

.field public static final chartbuilder_help_item_create_pie_chart:I = 0x7f0a0801

.field public static final chartbuilder_help_item_create_pie_chart_text_1:I = 0x7f0a0806

.field public static final chartbuilder_help_item_create_table:I = 0x7f0a0802

.field public static final chartbuilder_help_item_create_table_text_1:I = 0x7f0a0807

.field public static final chartbuilder_help_item_create_table_title:I = 0x7f0a080a

.field public static final chartbuilder_help_item_edit_chart_title:I = 0x7f0a080b

.field public static final chartbuilder_help_item_insert_to_snote:I = 0x7f0a080d

.field public static final chartbuilder_help_item_insert_to_snote_title:I = 0x7f0a080c

.field public static final cocktail_service_editing_express_message_1:I = 0x7f0a07da

.field public static final cocktail_service_editing_express_message_1_tts:I = 0x7f0a07db

.field public static final cocktail_service_editing_express_message_2:I = 0x7f0a07dc

.field public static final cocktail_service_editing_express_message_3:I = 0x7f0a07dd

.field public static final cocktail_service_editing_express_message_3_tts:I = 0x7f0a07de

.field public static final cocktail_service_editing_express_message_4:I = 0x7f0a07df

.field public static final cocktail_service_editing_express_message_4_tts:I = 0x7f0a07e0

.field public static final cocktail_service_editing_express_message_5:I = 0x7f0a07e1

.field public static final cocktail_service_editing_express_message_5_tts:I = 0x7f0a07e2

.field public static final cocktail_service_editing_express_message_6:I = 0x7f0a07e3

.field public static final cocktail_service_editing_express_message_6_tts:I = 0x7f0a07e4

.field public static final cocktail_service_managing_panels_message_1:I = 0x7f0a07d5

.field public static final cocktail_service_managing_panels_message_2:I = 0x7f0a07d6

.field public static final cocktail_service_managing_panels_message_3:I = 0x7f0a07d7

.field public static final cocktail_service_managing_panels_message_4:I = 0x7f0a07d8

.field public static final cocktail_service_managing_panels_message_4_tts:I = 0x7f0a07d9

.field public static final cocktail_service_navigating_panels_message_1:I = 0x7f0a07d4

.field public static final cocktail_service_using_night_clock_message_1:I = 0x7f0a07f1

.field public static final cocktail_service_using_night_clock_message_2:I = 0x7f0a07f2

.field public static final cocktail_service_using_quick_tools_message_1:I = 0x7f0a07e5

.field public static final cocktail_service_using_quick_tools_message_1_tts:I = 0x7f0a07e6

.field public static final cocktail_service_using_quick_tools_message_2:I = 0x7f0a07e7

.field public static final cocktail_service_using_quick_tools_message_2_tts:I = 0x7f0a07e8

.field public static final cocktail_service_using_quick_tools_message_3:I = 0x7f0a07e9

.field public static final cocktail_service_using_quick_tools_message_3_tts:I = 0x7f0a07ea

.field public static final cocktail_service_using_quick_tools_message_4:I = 0x7f0a07eb

.field public static final cocktail_service_using_quick_tools_message_4_tts:I = 0x7f0a07ec

.field public static final cocktail_service_using_quick_tools_message_5:I = 0x7f0a07ed

.field public static final cocktail_service_using_quick_tools_message_5_tts:I = 0x7f0a07ee

.field public static final cocktail_service_using_quick_tools_message_6:I = 0x7f0a07ef

.field public static final cocktail_service_using_quick_tools_message_6_tts:I = 0x7f0a07f0

.field public static final cocktail_service_waking_up_edge_screen_message_1:I = 0x7f0a07d2

.field public static final cocktail_service_waking_up_edge_screen_message_2:I = 0x7f0a07d3

.field public static final collage_input_images_numbers_2:I = 0x7f0a0367

.field public static final collage_input_images_numbers_4:I = 0x7f0a0368

.field public static final collecting_content_saving_seleted_content:I = 0x7f0a09fd

.field public static final collecting_content_saving_seleted_content_body:I = 0x7f0a09fe

.field public static final collecting_content_sharing_content:I = 0x7f0a09ff

.field public static final collecting_content_sharing_content_body:I = 0x7f0a0a00

.field public static final collecting_content_using_the_spen:I = 0x7f0a09fb

.field public static final collecting_content_using_the_spen_body:I = 0x7f0a09fc

.field public static final connect_to_wlan_continue:I = 0x7f0a08c7

.field public static final connect_via_mobile_network_body:I = 0x7f0a08c8

.field public static final contacts_add_open_contact_1:I = 0x7f0a052c

.field public static final contacts_add_open_contact_2:I = 0x7f0a052d

.field public static final contacts_add_open_contact_3:I = 0x7f0a052e

.field public static final contacts_add_open_contact_4:I = 0x7f0a052f

.field public static final contacts_setup_speed_dial_open_contact_1:I = 0x7f0a0530

.field public static final contacts_setup_speed_dial_open_contact_2:I = 0x7f0a0531

.field public static final contacts_setup_speed_dial_open_contact_3:I = 0x7f0a0532

.field public static final contacts_setup_speed_dial_open_contact_4:I = 0x7f0a0533

.field public static final contacts_setup_speed_dial_open_contact_4_cmcc:I = 0x7f0a0534

.field public static final dash:I = 0x7f0a006b

.field public static final data_conn_dialog_checkbox_text:I = 0x7f0a0592

.field public static final data_connection_chn_title:I = 0x7f0a08c5

.field public static final data_dialog_summary:I = 0x7f0a0591

.field public static final data_usage_help_item_checking_text_1:I = 0x7f0a0937

.field public static final data_usage_help_item_checking_text_2:I = 0x7f0a0938

.field public static final data_usage_help_item_data_usage_set_limit_text_1:I = 0x7f0a0939

.field public static final data_usage_help_item_data_usage_set_limit_text_2:I = 0x7f0a093a

.field public static final data_usage_help_item_data_usage_set_limit_text_3:I = 0x7f0a093b

.field public static final data_usage_menu_cycle_change_usage_report_tip:I = 0x7f0a0936

.field public static final deleting_a_picture_step1_for_tts:I = 0x7f0a0345

.field public static final deleting_multiple_pictures_step1:I = 0x7f0a0343

.field public static final deleting_multiple_pictures_step2:I = 0x7f0a0344

.field public static final deleting_multiple_pictures_step3_for_tts:I = 0x7f0a0346

.field public static final dialog_connection_button_cancel:I = 0x7f0a0597

.field public static final dialog_connection_button_setup_wifi:I = 0x7f0a0594

.field public static final dialog_connection_button_setup_wifi_chn:I = 0x7f0a0593

.field public static final dialog_connection_button_use_network:I = 0x7f0a0596

.field public static final dialog_data_connection_button_setup_wifi:I = 0x7f0a0595

.field public static final dialog_help_in_easy_mode_is_not_supported:I = 0x7f0a06b9

.field public static final dialog_help_in_easy_mode_is_not_supported_common:I = 0x7f0a06ba

.field public static final dialog_help_in_easy_mode_is_not_supported_common_mode:I = 0x7f0a06bc

.field public static final dialog_help_in_easy_mode_is_not_supported_vzw:I = 0x7f0a06bb

.field public static final dialog_no_wifi_summary:I = 0x7f0a058f

.field public static final dialog_no_wifi_summary_open:I = 0x7f0a0590

.field public static final dialog_no_wifi_title:I = 0x7f0a058e

.field public static final dialog_sort_mode_item_by_alphabet:I = 0x7f0a0136

.field public static final dialog_sort_mode_item_by_category:I = 0x7f0a0135

.field public static final dialog_use_data_connection_title:I = 0x7f0a0587

.field public static final dialog_use_motion_button:I = 0x7f0a02d6

.field public static final dialog_writing_buddy_disable:I = 0x7f0a07c7

.field public static final dialog_writing_buddy_disable_off_function:I = 0x7f0a07c9

.field public static final dialog_writing_buddy_disable_off_function_title:I = 0x7f0a07c8

.field public static final dialog_writing_buddy_disable_title:I = 0x7f0a07c6

.field public static final dict_apps:I = 0x7f0a0012

.field public static final dict_bluetooth:I = 0x7f0a0007

.field public static final dict_chart_builder_section_title:I = 0x7f0a000e

.field public static final dict_data:I = 0x7f0a0013

.field public static final dict_email:I = 0x7f0a0008

.field public static final dict_galaxyfinder_section_title:I = 0x7f0a000d

.field public static final dict_gps:I = 0x7f0a0014

.field public static final dict_homescreen:I = 0x7f0a000a

.field public static final dict_life_times:I = 0x7f0a001a

.field public static final dict_mobile_hotspot:I = 0x7f0a0015

.field public static final dict_mymagazine_section_title:I = 0x7f0a000f

.field public static final dict_nfc:I = 0x7f0a0016

.field public static final dict_pictures:I = 0x7f0a0009

.field public static final dict_ringtone:I = 0x7f0a000b

.field public static final dict_shealth_section_title:I = 0x7f0a0000

.field public static final dict_smemo_section_title:I = 0x7f0a0001

.field public static final dict_snote_section_title:I = 0x7f0a0003

.field public static final dict_software_update:I = 0x7f0a0017

.field public static final dict_stransalter_section_title:I = 0x7f0a0005

.field public static final dict_svoice_item_title:I = 0x7f0a0004

.field public static final dict_svoice_section_title:I = 0x7f0a0002

.field public static final dict_sync:I = 0x7f0a0018

.field public static final dict_visual_voice_mail:I = 0x7f0a0019

.field public static final dict_vzw_my_verizon_videos_title:I = 0x7f0a0011

.field public static final dict_vzw_wifi_bluetooth_item_title:I = 0x7f0a0010

.field public static final dict_wallpaper:I = 0x7f0a000c

.field public static final dict_wifi:I = 0x7f0a0006

.field public static final downloadable_dialog_popup_text:I = 0x7f0a0067

.field public static final downloadable_dialog_popup_text_chn:I = 0x7f0a0066

.field public static final downloadable_dialog_popup_text_wifi:I = 0x7f0a0069

.field public static final downloadable_dialog_popup_text_wifi_chn:I = 0x7f0a0068

.field public static final dual_camera_tap_to_dual_picture:I = 0x7f0a03f5

.field public static final editing_homescreen_text_3:I = 0x7f0a09c0

.field public static final editing_homescreen_text_4:I = 0x7f0a09c1

.field public static final editing_homescreen_text_5:I = 0x7f0a09c2

.field public static final editing_homescreen_text_5_tb:I = 0x7f0a09c3

.field public static final editing_homescreen_tip:I = 0x7f0a09c4

.field public static final email_change_signature_open_email:I = 0x7f0a04f4

.field public static final email_composing_emails_composing_1:I = 0x7f0a04d8

.field public static final email_composing_emails_composing_1_tts:I = 0x7f0a04d9

.field public static final email_composing_emails_composing_2:I = 0x7f0a04da

.field public static final email_composing_emails_composing_3:I = 0x7f0a04db

.field public static final email_composing_emails_composing_3_tts:I = 0x7f0a04dc

.field public static final email_composing_emails_composing_title:I = 0x7f0a04d7

.field public static final email_composing_emails_signature_1:I = 0x7f0a04de

.field public static final email_composing_emails_signature_1_tts:I = 0x7f0a04df

.field public static final email_composing_emails_signature_2:I = 0x7f0a04e0

.field public static final email_composing_emails_signature_3:I = 0x7f0a04e1

.field public static final email_composing_emails_signature_4:I = 0x7f0a04e2

.field public static final email_composing_emails_signature_5:I = 0x7f0a04e3

.field public static final email_composing_emails_signature_title:I = 0x7f0a04dd

.field public static final email_create_filtering_open_email:I = 0x7f0a04f3

.field public static final email_managing_emails_1:I = 0x7f0a04e4

.field public static final email_managing_emails_2:I = 0x7f0a04e5

.field public static final email_managing_emails_tip_1:I = 0x7f0a04e6

.field public static final email_managing_emails_tip_1_tts:I = 0x7f0a04e7

.field public static final email_managing_emails_tip_2:I = 0x7f0a04e8

.field public static final email_managing_emails_tip_2_tts:I = 0x7f0a04e9

.field public static final email_managing_emails_tip_3:I = 0x7f0a04ea

.field public static final email_managing_emails_tip_3_tts:I = 0x7f0a04eb

.field public static final email_managing_emails_tip_4:I = 0x7f0a04ec

.field public static final email_managing_emails_tip_4_tts:I = 0x7f0a04ed

.field public static final email_managing_emails_tip_5:I = 0x7f0a04ee

.field public static final email_managing_emails_tip_5_tts:I = 0x7f0a04ef

.field public static final email_reading_emails_reading_1:I = 0x7f0a04d2

.field public static final email_reading_emails_reading_1_tts:I = 0x7f0a04d3

.field public static final email_reading_emails_reading_2:I = 0x7f0a04d6

.field public static final email_reading_emails_reading_tip_1:I = 0x7f0a04d4

.field public static final email_reading_emails_reading_tip_1_tts:I = 0x7f0a04d5

.field public static final email_reading_emails_reading_title:I = 0x7f0a04d1

.field public static final email_reading_emails_viewing_1:I = 0x7f0a04c3

.field public static final email_reading_emails_viewing_2:I = 0x7f0a04c6

.field public static final email_reading_emails_viewing_tip_1:I = 0x7f0a04c4

.field public static final email_reading_emails_viewing_tip_1_tts:I = 0x7f0a04c5

.field public static final email_reading_emails_viewing_tip_2_1:I = 0x7f0a04c7

.field public static final email_reading_emails_viewing_tip_2_1_tts:I = 0x7f0a04c8

.field public static final email_reading_emails_viewing_tip_2_2:I = 0x7f0a04c9

.field public static final email_reading_emails_viewing_tip_2_2_tts:I = 0x7f0a04ca

.field public static final email_reading_emails_viewing_tip_2_3:I = 0x7f0a04cb

.field public static final email_reading_emails_viewing_tip_2_3_tts:I = 0x7f0a04cc

.field public static final email_reading_emails_viewing_tip_2_4:I = 0x7f0a04cd

.field public static final email_reading_emails_viewing_tip_2_4_tts:I = 0x7f0a04ce

.field public static final email_reading_emails_viewing_tip_2_5:I = 0x7f0a04cf

.field public static final email_reading_emails_viewing_tip_2_5_tts:I = 0x7f0a04d0

.field public static final email_reading_emails_viewing_title:I = 0x7f0a04c2

.field public static final email_set_priority_sender_open_email:I = 0x7f0a04f2

.field public static final email_setting_up_email_accounts_1:I = 0x7f0a04bd

.field public static final email_setting_up_email_accounts_2:I = 0x7f0a04be

.field public static final email_setting_up_email_accounts_3:I = 0x7f0a04bf

.field public static final email_setting_up_email_accounts_tip_1:I = 0x7f0a04c0

.field public static final email_setting_up_email_accounts_tip_2:I = 0x7f0a04c1

.field public static final email_setup_account_open_email:I = 0x7f0a04f0

.field public static final email_setup_pop_imap_open_email:I = 0x7f0a04f1

.field public static final emeeting_help_item_create_meeting:I = 0x7f0a0812

.field public static final emeeting_help_item_create_meeting_text_1:I = 0x7f0a081d

.field public static final emeeting_help_item_create_meeting_text_2:I = 0x7f0a081e

.field public static final emeeting_help_item_create_meeting_text_3:I = 0x7f0a081f

.field public static final emeeting_help_item_introduction:I = 0x7f0a0815

.field public static final emeeting_help_item_introduction_to_emeeting:I = 0x7f0a0810

.field public static final emeeting_help_item_join_meeting:I = 0x7f0a0813

.field public static final emeeting_help_item_join_meeting_text_1:I = 0x7f0a0820

.field public static final emeeting_help_item_meeting_screen:I = 0x7f0a0814

.field public static final emeeting_help_item_meeting_screen_text_1:I = 0x7f0a082b

.field public static final emeeting_help_item_meeting_screen_text_10:I = 0x7f0a0834

.field public static final emeeting_help_item_meeting_screen_text_2:I = 0x7f0a082c

.field public static final emeeting_help_item_meeting_screen_text_3:I = 0x7f0a082d

.field public static final emeeting_help_item_meeting_screen_text_4:I = 0x7f0a082e

.field public static final emeeting_help_item_meeting_screen_text_5:I = 0x7f0a082f

.field public static final emeeting_help_item_meeting_screen_text_6:I = 0x7f0a0830

.field public static final emeeting_help_item_meeting_screen_text_7:I = 0x7f0a0831

.field public static final emeeting_help_item_meeting_screen_text_8:I = 0x7f0a0832

.field public static final emeeting_help_item_meeting_screen_text_9:I = 0x7f0a0833

.field public static final emeeting_help_item_meeting_screen_title_1:I = 0x7f0a0821

.field public static final emeeting_help_item_meeting_screen_title_10:I = 0x7f0a082a

.field public static final emeeting_help_item_meeting_screen_title_2:I = 0x7f0a0822

.field public static final emeeting_help_item_meeting_screen_title_3:I = 0x7f0a0823

.field public static final emeeting_help_item_meeting_screen_title_4:I = 0x7f0a0824

.field public static final emeeting_help_item_meeting_screen_title_5:I = 0x7f0a0825

.field public static final emeeting_help_item_meeting_screen_title_6:I = 0x7f0a0826

.field public static final emeeting_help_item_meeting_screen_title_7:I = 0x7f0a0827

.field public static final emeeting_help_item_meeting_screen_title_8:I = 0x7f0a0828

.field public static final emeeting_help_item_meeting_screen_title_9:I = 0x7f0a0829

.field public static final emeeting_help_item_requirements:I = 0x7f0a0811

.field public static final emeeting_help_item_requirements_image_text_1:I = 0x7f0a0816

.field public static final emeeting_help_item_requirements_image_text_2:I = 0x7f0a0817

.field public static final emeeting_help_item_requirements_text_1:I = 0x7f0a0818

.field public static final emeeting_help_item_requirements_text_2:I = 0x7f0a0819

.field public static final emeeting_help_item_requirements_text_3:I = 0x7f0a081a

.field public static final emeeting_help_item_requirements_text_4:I = 0x7f0a081b

.field public static final emeeting_help_item_requirements_text_5:I = 0x7f0a081c

.field public static final enable_palm_motion:I = 0x7f0a0582

.field public static final expandable_list_item_fold_descr:I = 0x7f0a001b

.field public static final expandable_the_notification_panel_01:I = 0x7f0a0126

.field public static final feature_not_supported_restricted_profile:I = 0x7f0a06b3

.field public static final filtering_pictures_info1:I = 0x7f0a0333

.field public static final filtering_pictures_info2:I = 0x7f0a0334

.field public static final filtering_pictures_info2_for_m2:I = 0x7f0a0335

.field public static final filtering_pictures_step1:I = 0x7f0a0331

.field public static final filtering_pictures_step1_for_tts:I = 0x7f0a0336

.field public static final filtering_pictures_step2:I = 0x7f0a0332

.field public static final fingerprints_setting_fingerprints_2:I = 0x7f0a076a

.field public static final fingerprints_setting_fingerprints_3:I = 0x7f0a076b

.field public static final fingerprints_use_fingerprints_1:I = 0x7f0a0764

.field public static final fingerprints_use_fingerprints_2:I = 0x7f0a0766

.field public static final fingerprints_use_fingerprints_3:I = 0x7f0a0767

.field public static final fingerprints_use_fingerprints_4:I = 0x7f0a0769

.field public static final fingerprints_use_fingerprints_manager:I = 0x7f0a0765

.field public static final fingerprints_use_fingerprints_tip_1:I = 0x7f0a0762

.field public static final fingerprints_use_fingerprints_tip_2:I = 0x7f0a0763

.field public static final fingerprints_use_touchfingerprints_3:I = 0x7f0a0768

.field public static final galaxyfinder_bullet:I = 0x7f0a075c

.field public static final galaxyfinder_help_item_how_to_improve_searching:I = 0x7f0a074f

.field public static final galaxyfinder_help_item_how_to_start_searching:I = 0x7f0a074e

.field public static final galaxyfinder_help_item_how_to_start_searching_text_0:I = 0x7f0a0753

.field public static final galaxyfinder_help_item_how_to_start_searching_text_3:I = 0x7f0a0751

.field public static final galaxyfinder_help_item_how_to_start_searching_text_4:I = 0x7f0a0752

.field public static final galaxyfinder_help_item_search_by_handwriting_2:I = 0x7f0a0755

.field public static final galaxyfinder_help_item_searching_by_handwriting:I = 0x7f0a0754

.field public static final galaxyfinder_help_item_searching_sfinder:I = 0x7f0a074b

.field public static final galaxyfinder_help_item_searching_text_2:I = 0x7f0a0756

.field public static final galaxyfinder_help_item_sort_alphabetical:I = 0x7f0a075a

.field public static final galaxyfinder_help_item_sort_criteria:I = 0x7f0a0757

.field public static final galaxyfinder_help_item_sort_currently_used_category:I = 0x7f0a0759

.field public static final galaxyfinder_help_item_sort_custom:I = 0x7f0a075b

.field public static final galaxyfinder_help_item_sort_frequently_used_category:I = 0x7f0a0758

.field public static final galaxyfinder_help_item_sort_search_results:I = 0x7f0a0750

.field public static final gallery_contextual_tag_1:I = 0x7f0a0351

.field public static final gallery_contextual_tag_face_information:I = 0x7f0a0354

.field public static final gallery_contextual_tag_k:I = 0x7f0a0355

.field public static final gallery_contextual_tag_location_tag:I = 0x7f0a0353

.field public static final gallery_contextual_tag_weather_tag:I = 0x7f0a0352

.field public static final gallery_copy_to_another_image_2:I = 0x7f0a035d

.field public static final gallery_copy_to_another_image_3:I = 0x7f0a035e

.field public static final gallery_copy_to_another_image_4:I = 0x7f0a035f

.field public static final gallery_copy_to_another_image_5_modified:I = 0x7f0a0360

.field public static final gallery_face_reshaping_2:I = 0x7f0a0361

.field public static final gallery_face_reshaping_3:I = 0x7f0a0362

.field public static final gallery_face_tagging_open_picture:I = 0x7f0a0350

.field public static final gallery_howto_edit_2:I = 0x7f0a0357

.field public static final gallery_howto_edit_3:I = 0x7f0a0358

.field public static final gallery_make_collages_2:I = 0x7f0a0366

.field public static final gallery_make_collages_3:I = 0x7f0a0369

.field public static final gallery_open:I = 0x7f0a0356

.field public static final gallery_playing_videos_begin_playback:I = 0x7f0a034f

.field public static final gallery_playing_videos_select_album:I = 0x7f0a034e

.field public static final gallery_press_the_menu:I = 0x7f0a0365

.field public static final gallery_press_the_menu_for_select:I = 0x7f0a0363

.field public static final gallery_select_a_picture:I = 0x7f0a0364

.field public static final gallery_select_a_picture_from_gallery_and_tap_ps_the_edit_icon:I = 0x7f0a035c

.field public static final gallery_selection_mode_1:I = 0x7f0a0359

.field public static final gallery_selection_mode_2:I = 0x7f0a035a

.field public static final gallery_selection_mode_3:I = 0x7f0a035b

.field public static final gallery_viewing_picture_scroll_picture:I = 0x7f0a034c

.field public static final gallery_viewing_picture_select_album:I = 0x7f0a034b

.field public static final gallery_viewing_picture_zoom_in_out:I = 0x7f0a034d

.field public static final gesture_guide_delete_character:I = 0x7f0a07c1

.field public static final gesture_guide_delete_space:I = 0x7f0a07c3

.field public static final gesture_guide_delete_word:I = 0x7f0a07c0

.field public static final gesture_guide_description:I = 0x7f0a07be

.field public static final gesture_guide_edit:I = 0x7f0a07bf

.field public static final gesture_guide_insert:I = 0x7f0a07c2

.field public static final glance_view_app4_2:I = 0x7f0a0892

.field public static final glance_view_features_description2_2:I = 0x7f0a0893

.field public static final globalairbutton_app1:I = 0x7f0a077b

.field public static final globalairbutton_app1_summary:I = 0x7f0a0782

.field public static final globalairbutton_app1_summary_without_dialing:I = 0x7f0a0783

.field public static final globalairbutton_app2:I = 0x7f0a077c

.field public static final globalairbutton_app2_summary:I = 0x7f0a0784

.field public static final globalairbutton_app3:I = 0x7f0a077d

.field public static final globalairbutton_app3_summary:I = 0x7f0a0787

.field public static final globalairbutton_app4:I = 0x7f0a077e

.field public static final globalairbutton_app4_summary:I = 0x7f0a0785

.field public static final globalairbutton_app5:I = 0x7f0a077f

.field public static final globalairbutton_app5_summary:I = 0x7f0a0786

.field public static final globalairbutton_app7:I = 0x7f0a0780

.field public static final globalairbutton_app8:I = 0x7f0a0781

.field public static final globalairbutton_app_count:I = 0x7f0a077a

.field public static final group_play_depending_on_network_connection:I = 0x7f0a0407

.field public static final group_play_devices_can_merge_video_screen:I = 0x7f0a0411

.field public static final group_play_introduction:I = 0x7f0a03fe

.field public static final group_play_introduction_including_music:I = 0x7f0a03ff

.field public static final group_play_introduction_including_music_video:I = 0x7f0a0401

.field public static final group_play_introduction_including_video:I = 0x7f0a0400

.field public static final group_play_master_can_change_screen:I = 0x7f0a040e

.field public static final group_play_master_can_change_video_screen:I = 0x7f0a0410

.field public static final group_play_master_selects_music:I = 0x7f0a040c

.field public static final group_play_master_selects_video:I = 0x7f0a040f

.field public static final group_play_set_password:I = 0x7f0a040a

.field public static final group_play_set_speaker_automatically:I = 0x7f0a040d

.field public static final group_play_to_join_press_button:I = 0x7f0a040b

.field public static final group_play_to_listen_press_button:I = 0x7f0a0402

.field public static final group_play_to_make_documents_button:I = 0x7f0a0405

.field public static final group_play_to_play_press_button:I = 0x7f0a0406

.field public static final group_play_to_start_new_group_press_button:I = 0x7f0a0408

.field public static final group_play_to_view_press_button:I = 0x7f0a0403

.field public static final group_play_to_watch_press_button:I = 0x7f0a0404

.field public static final group_play_your_device_set_as_ap:I = 0x7f0a0409

.field public static final guest_lounge_access_home_and_drag:I = 0x7f0a0416

.field public static final guest_lounge_access_home_to_access_home:I = 0x7f0a0415

.field public static final guest_lounge_introduction_create_space:I = 0x7f0a0412

.field public static final guest_lounge_introduction_have_own_space:I = 0x7f0a0414

.field public static final guest_lounge_introduction_protect_personal_data:I = 0x7f0a0413

.field public static final hands_free:I = 0x7f0a0448

.field public static final help_browser_text:I = 0x7f0a08cc

.field public static final help_moments_add_log:I = 0x7f0a08ce

.field public static final help_moments_press_to_create:I = 0x7f0a08cd

.field public static final help_reports_scroll:I = 0x7f0a08cf

.field public static final help_reports_tap:I = 0x7f0a08d0

.field public static final help_supporting2_predictive_text_string_for_wifi:I = 0x7f0a0128

.field public static final help_text_more_info:I = 0x7f0a0065

.field public static final hs_add_apps_open_applications:I = 0x7f0a04f7

.field public static final hs_add_apps_open_applications_2_k_common:I = 0x7f0a04ff

.field public static final hs_add_apps_open_applications_3_k_common:I = 0x7f0a0500

.field public static final hs_add_apps_open_applications_k_common:I = 0x7f0a04fe

.field public static final hs_add_widget_open_applications:I = 0x7f0a04f8

.field public static final hs_add_widget_open_applications_2_k_common:I = 0x7f0a0502

.field public static final hs_add_widget_open_applications_3_k_common:I = 0x7f0a0503

.field public static final hs_add_widget_open_applications_4_k_common:I = 0x7f0a0504

.field public static final hs_add_widget_open_applications_k_common:I = 0x7f0a0501

.field public static final hs_change_wallpaper_hold_empty_area_1_k_common:I = 0x7f0a050b

.field public static final hs_change_wallpaper_hold_empty_area_2_k_common:I = 0x7f0a050c

.field public static final hs_change_wallpaper_hold_empty_area_3_k_common:I = 0x7f0a050d

.field public static final hs_change_wallpaper_hold_empty_area_k_common:I = 0x7f0a050a

.field public static final hs_change_wallpaper_tip:I = 0x7f0a04fd

.field public static final hs_create_floder_add_application:I = 0x7f0a04fb

.field public static final hs_create_folder_open_application_1_k_common:I = 0x7f0a0506

.field public static final hs_create_folder_open_application_1_k_common_tts:I = 0x7f0a0507

.field public static final hs_create_folder_open_application_k_common:I = 0x7f0a0505

.field public static final hs_move_items_hold_item:I = 0x7f0a04f9

.field public static final hs_navigate_flick_screen:I = 0x7f0a04f5

.field public static final hs_remove_items_hold_item_k_common:I = 0x7f0a0508

.field public static final hs_remove_items_hold_item_k_common_tts:I = 0x7f0a0509

.field public static final hs_remove_items_tip:I = 0x7f0a04fc

.field public static final hs_resize_widgets_hold_widget:I = 0x7f0a04fa

.field public static final hs_using_flipboard_briefing_2_common:I = 0x7f0a050f

.field public static final hs_using_flipboard_briefing_3_common:I = 0x7f0a0510

.field public static final hs_using_flipboard_briefing_common:I = 0x7f0a050e

.field public static final hs_view_all_apps_tap_applications:I = 0x7f0a04f6

.field public static final hs_view_more_apps_tap_applications:I = 0x7f0a0876

.field public static final icon_glossary_alarm_and_calendar_alarm_set:I = 0x7f0a05fa

.field public static final icon_glossary_alarm_and_calendar_alarm_snoozed:I = 0x7f0a05fc

.field public static final icon_glossary_alarm_and_calendar_upcoming_event:I = 0x7f0a05fb

.field public static final icon_glossary_battery_battery_critically_low:I = 0x7f0a0601

.field public static final icon_glossary_battery_battery_full:I = 0x7f0a05fe

.field public static final icon_glossary_battery_battery_low:I = 0x7f0a0600

.field public static final icon_glossary_battery_charging_battery:I = 0x7f0a05ff

.field public static final icon_glossary_battery_empty_battery:I = 0x7f0a0683

.field public static final icon_glossary_battery_full_battery:I = 0x7f0a0681

.field public static final icon_glossary_battery_low_battery:I = 0x7f0a0682

.field public static final icon_glossary_call_auto_rejected_call:I = 0x7f0a05ba

.field public static final icon_glossary_call_in_call:I = 0x7f0a05b7

.field public static final icon_glossary_call_microphone_muted:I = 0x7f0a05bb

.field public static final icon_glossary_call_missed_call:I = 0x7f0a05b8

.field public static final icon_glossary_call_reject_call:I = 0x7f0a05b9

.field public static final icon_glossary_call_speakerphone_on:I = 0x7f0a05bc

.field public static final icon_glossary_connectivity_bluetooth_connected:I = 0x7f0a05e4

.field public static final icon_glossary_connectivity_bluetooth_on:I = 0x7f0a05e3

.field public static final icon_glossary_connectivity_car_mode:I = 0x7f0a05e7

.field public static final icon_glossary_connectivity_car_mode_spr:I = 0x7f0a05e8

.field public static final icon_glossary_connectivity_headset_connected:I = 0x7f0a05e6

.field public static final icon_glossary_connectivity_mobile_hotspot_on:I = 0x7f0a05e0

.field public static final icon_glossary_connectivity_nfc_on:I = 0x7f0a05e5

.field public static final icon_glossary_connectivity_printing:I = 0x7f0a05eb

.field public static final icon_glossary_connectivity_sd_card_error:I = 0x7f0a05ea

.field public static final icon_glossary_connectivity_sd_card_full:I = 0x7f0a05e9

.field public static final icon_glossary_connectivity_sync_error:I = 0x7f0a05df

.field public static final icon_glossary_connectivity_syncing:I = 0x7f0a05de

.field public static final icon_glossary_connectivity_usb_connect:I = 0x7f0a05e1

.field public static final icon_glossary_connectivity_usb_connect_fail:I = 0x7f0a05e2

.field public static final icon_glossary_email_attach:I = 0x7f0a065a

.field public static final icon_glossary_email_bg_color:I = 0x7f0a066e

.field public static final icon_glossary_email_bold:I = 0x7f0a066a

.field public static final icon_glossary_email_compose:I = 0x7f0a0658

.field public static final icon_glossary_email_delete:I = 0x7f0a0659

.field public static final icon_glossary_email_email_not_sent:I = 0x7f0a05ef

.field public static final icon_glossary_email_favorite:I = 0x7f0a0676

.field public static final icon_glossary_email_font_color:I = 0x7f0a066d

.field public static final icon_glossary_email_high_priority:I = 0x7f0a0677

.field public static final icon_glossary_email_insert:I = 0x7f0a0665

.field public static final icon_glossary_email_italic:I = 0x7f0a066c

.field public static final icon_glossary_email_low_priority:I = 0x7f0a0678

.field public static final icon_glossary_email_move:I = 0x7f0a0660

.field public static final icon_glossary_email_new_email:I = 0x7f0a05ed

.field public static final icon_glossary_email_new_gmail:I = 0x7f0a05ee

.field public static final icon_glossary_email_quick:I = 0x7f0a0669

.field public static final icon_glossary_email_redo:I = 0x7f0a0668

.field public static final icon_glossary_email_refresh:I = 0x7f0a065b

.field public static final icon_glossary_email_reply:I = 0x7f0a065c

.field public static final icon_glossary_email_reply_all:I = 0x7f0a065d

.field public static final icon_glossary_email_save:I = 0x7f0a065e

.field public static final icon_glossary_email_search:I = 0x7f0a065f

.field public static final icon_glossary_email_underline:I = 0x7f0a066b

.field public static final icon_glossary_email_undo:I = 0x7f0a0667

.field public static final icon_glossary_message_contact:I = 0x7f0a066f

.field public static final icon_glossary_message_create:I = 0x7f0a0661

.field public static final icon_glossary_message_email_not_sent:I = 0x7f0a0674

.field public static final icon_glossary_message_fail_message:I = 0x7f0a0666

.field public static final icon_glossary_message_message_not_sent:I = 0x7f0a0675

.field public static final icon_glossary_message_new_email:I = 0x7f0a0670

.field public static final icon_glossary_message_new_gmail_message:I = 0x7f0a0671

.field public static final icon_glossary_message_new_message:I = 0x7f0a0672

.field public static final icon_glossary_message_receive:I = 0x7f0a0663

.field public static final icon_glossary_message_restore:I = 0x7f0a0664

.field public static final icon_glossary_message_send:I = 0x7f0a0662

.field public static final icon_glossary_message_voicemail:I = 0x7f0a0673

.field public static final icon_glossary_messages_message_not_sent:I = 0x7f0a05f2

.field public static final icon_glossary_messages_new_message:I = 0x7f0a05f1

.field public static final icon_glossary_messages_new_voice_mail:I = 0x7f0a05f3

.field public static final icon_glossary_messages_new_voice_mail_spr:I = 0x7f0a05f4

.field public static final icon_glossary_messages_urgent_voice_mail:I = 0x7f0a05f7

.field public static final icon_glossary_messages_urgent_voice_mail_spr:I = 0x7f0a05f8

.field public static final icon_glossary_messages_voice_mail_not_sent:I = 0x7f0a05f5

.field public static final icon_glossary_messages_voice_mail_not_sent_spr:I = 0x7f0a05f6

.field public static final icon_glossary_network_3g_connected:I = 0x7f0a05c3

.field public static final icon_glossary_network_3g_connected_spr:I = 0x7f0a05c4

.field public static final icon_glossary_network_3g_in_use:I = 0x7f0a05c5

.field public static final icon_glossary_network_4g_lte_connected:I = 0x7f0a05c6

.field public static final icon_glossary_network_4g_lte_connected_spr:I = 0x7f0a05c7

.field public static final icon_glossary_network_4g_lte_in_use:I = 0x7f0a05c8

.field public static final icon_glossary_network_airplane_mode:I = 0x7f0a05d3

.field public static final icon_glossary_network_app_update_available:I = 0x7f0a05d5

.field public static final icon_glossary_network_app_update_successful:I = 0x7f0a05d6

.field public static final icon_glossary_network_download_booster_in_use:I = 0x7f0a05db

.field public static final icon_glossary_network_download_complete:I = 0x7f0a05d8

.field public static final icon_glossary_network_downloading:I = 0x7f0a05d7

.field public static final icon_glossary_network_emergency_alert:I = 0x7f0a05dc

.field public static final icon_glossary_network_location_on:I = 0x7f0a05d4

.field public static final icon_glossary_network_network_extender:I = 0x7f0a05d0

.field public static final icon_glossary_network_no_signal:I = 0x7f0a05cd

.field public static final icon_glossary_network_no_sim_card:I = 0x7f0a05d1

.field public static final icon_glossary_network_roaming:I = 0x7f0a05ce

.field public static final icon_glossary_network_roaming_vzw:I = 0x7f0a05cf

.field public static final icon_glossary_network_signal_strength:I = 0x7f0a05cc

.field public static final icon_glossary_network_unknown_sim_card:I = 0x7f0a05d2

.field public static final icon_glossary_network_upload_complete:I = 0x7f0a05da

.field public static final icon_glossary_network_uploading:I = 0x7f0a05d9

.field public static final icon_glossary_network_wifi_connected:I = 0x7f0a05ca

.field public static final icon_glossary_network_wifi_status:I = 0x7f0a05c9

.field public static final icon_glossary_network_wifi_within_range:I = 0x7f0a05cb

.field public static final icon_glossary_other_icons_air_browse:I = 0x7f0a0606

.field public static final icon_glossary_other_icons_blocking_mode:I = 0x7f0a0608

.field public static final icon_glossary_other_icons_cooling_down:I = 0x7f0a060a

.field public static final icon_glossary_other_icons_flashlight_on:I = 0x7f0a060b

.field public static final icon_glossary_other_icons_geo_news:I = 0x7f0a0613

.field public static final icon_glossary_other_icons_keyboard:I = 0x7f0a0605

.field public static final icon_glossary_other_icons_more_notifications:I = 0x7f0a0603

.field public static final icon_glossary_other_icons_pen:I = 0x7f0a0685

.field public static final icon_glossary_other_icons_personal_page_enabled:I = 0x7f0a0684

.field public static final icon_glossary_other_icons_power_saving_mode_on:I = 0x7f0a060e

.field public static final icon_glossary_other_icons_private_mode:I = 0x7f0a0609

.field public static final icon_glossary_other_icons_s_pen_detached:I = 0x7f0a0614

.field public static final icon_glossary_other_icons_screenshot_saved:I = 0x7f0a0604

.field public static final icon_glossary_other_icons_security_warning:I = 0x7f0a0610

.field public static final icon_glossary_other_icons_sent_help_message:I = 0x7f0a0612

.field public static final icon_glossary_other_icons_sim_card_full:I = 0x7f0a0611

.field public static final icon_glossary_other_icons_smart_stay:I = 0x7f0a0607

.field public static final icon_glossary_other_icons_sound_detector_on:I = 0x7f0a060c

.field public static final icon_glossary_other_icons_ultra_power_saving_mode_on:I = 0x7f0a060f

.field public static final icon_glossary_other_icons_voice_recorder:I = 0x7f0a060d

.field public static final icon_glossary_phone_in_call:I = 0x7f0a061e

.field public static final icon_glossary_phone_internet_call:I = 0x7f0a0625

.field public static final icon_glossary_phone_microphone_muted:I = 0x7f0a0621

.field public static final icon_glossary_phone_miss_call:I = 0x7f0a061f

.field public static final icon_glossary_phone_mms:I = 0x7f0a0627

.field public static final icon_glossary_phone_received:I = 0x7f0a062a

.field public static final icon_glossary_phone_reject_call:I = 0x7f0a0620

.field public static final icon_glossary_phone_sent:I = 0x7f0a0629

.field public static final icon_glossary_phone_sms:I = 0x7f0a0626

.field public static final icon_glossary_phone_speakerphone_on:I = 0x7f0a0622

.field public static final icon_glossary_phone_voice_call:I = 0x7f0a0624

.field public static final icon_glossary_phone_voice_mail:I = 0x7f0a0628

.field public static final icon_glossary_sound_pause:I = 0x7f0a05c1

.field public static final icon_glossary_sound_play:I = 0x7f0a05c0

.field public static final icon_glossary_sound_silent_mode:I = 0x7f0a05be

.field public static final icon_glossary_sound_vibration_mode:I = 0x7f0a05bf

.field public static final icon_glossary_splanner_add_sticker:I = 0x7f0a067c

.field public static final icon_glossary_splanner_alarm_set:I = 0x7f0a067e

.field public static final icon_glossary_splanner_create:I = 0x7f0a0679

.field public static final icon_glossary_splanner_high_priority:I = 0x7f0a067f

.field public static final icon_glossary_splanner_import_calendar:I = 0x7f0a067b

.field public static final icon_glossary_splanner_low_priority:I = 0x7f0a0680

.field public static final icon_glossary_splanner_repeat:I = 0x7f0a067a

.field public static final icon_glossary_splanner_upcoming_events:I = 0x7f0a067d

.field public static final icon_glossary_system_status_3g_connected:I = 0x7f0a0645

.field public static final icon_glossary_system_status_3g_connected_tri:I = 0x7f0a0646

.field public static final icon_glossary_system_status_3g_in_use:I = 0x7f0a0647

.field public static final icon_glossary_system_status_4g_lte_connected:I = 0x7f0a0648

.field public static final icon_glossary_system_status_4g_lte_connected_tri:I = 0x7f0a0649

.field public static final icon_glossary_system_status_4g_lte_in_use:I = 0x7f0a064a

.field public static final icon_glossary_system_status_air_browse:I = 0x7f0a0686

.field public static final icon_glossary_system_status_app_update_available:I = 0x7f0a0655

.field public static final icon_glossary_system_status_app_update_successful:I = 0x7f0a0656

.field public static final icon_glossary_system_status_auto_rejected_call:I = 0x7f0a0623

.field public static final icon_glossary_system_status_bluetooth:I = 0x7f0a0634

.field public static final icon_glossary_system_status_bluetooth_connected:I = 0x7f0a0635

.field public static final icon_glossary_system_status_car_mode:I = 0x7f0a063b

.field public static final icon_glossary_system_status_car_mode_vzw:I = 0x7f0a063c

.field public static final icon_glossary_system_status_cdma_roaming:I = 0x7f0a0641

.field public static final icon_glossary_system_status_download_complete:I = 0x7f0a0653

.field public static final icon_glossary_system_status_downloading:I = 0x7f0a0652

.field public static final icon_glossary_system_status_emergency_alert:I = 0x7f0a0657

.field public static final icon_glossary_system_status_flight_mode:I = 0x7f0a064b

.field public static final icon_glossary_system_status_gsm_roaming:I = 0x7f0a0642

.field public static final icon_glossary_system_status_headset_connected:I = 0x7f0a0638

.field public static final icon_glossary_system_status_location:I = 0x7f0a064f

.field public static final icon_glossary_system_status_location_services_off:I = 0x7f0a0651

.field public static final icon_glossary_system_status_location_services_on:I = 0x7f0a0650

.field public static final icon_glossary_system_status_mobile_hotspot_on:I = 0x7f0a0636

.field public static final icon_glossary_system_status_network_extender:I = 0x7f0a064e

.field public static final icon_glossary_system_status_nfc_on:I = 0x7f0a0637

.field public static final icon_glossary_system_status_no_sim_card:I = 0x7f0a064c

.field public static final icon_glossary_system_status_pause:I = 0x7f0a062c

.field public static final icon_glossary_system_status_play:I = 0x7f0a062b

.field public static final icon_glossary_system_status_private_mode:I = 0x7f0a0687

.field public static final icon_glossary_system_status_rssi:I = 0x7f0a0640

.field public static final icon_glossary_system_status_rssi_no_signal:I = 0x7f0a0643

.field public static final icon_glossary_system_status_rssi_no_signal_vzw:I = 0x7f0a0644

.field public static final icon_glossary_system_status_sd_card_error:I = 0x7f0a0639

.field public static final icon_glossary_system_status_silent_mode:I = 0x7f0a062d

.field public static final icon_glossary_system_status_smart_stay:I = 0x7f0a0688

.field public static final icon_glossary_system_status_sync:I = 0x7f0a0630

.field public static final icon_glossary_system_status_sync_error:I = 0x7f0a0631

.field public static final icon_glossary_system_status_unknown_sim_card:I = 0x7f0a064d

.field public static final icon_glossary_system_status_upload_complete:I = 0x7f0a0654

.field public static final icon_glossary_system_status_usb_connect:I = 0x7f0a0632

.field public static final icon_glossary_system_status_usb_connect_fail:I = 0x7f0a0633

.field public static final icon_glossary_system_status_usb_debugging_mode:I = 0x7f0a063a

.field public static final icon_glossary_system_status_vibration:I = 0x7f0a062f

.field public static final icon_glossary_system_status_vibration_mode:I = 0x7f0a062e

.field public static final icon_glossary_system_status_wifi_connected:I = 0x7f0a063e

.field public static final icon_glossary_system_status_wifi_status:I = 0x7f0a063d

.field public static final icon_glossary_system_status_wifi_within_range:I = 0x7f0a063f

.field public static final icon_glossary_volte_camera_off:I = 0x7f0a061a

.field public static final icon_glossary_volte_camera_on:I = 0x7f0a0619

.field public static final icon_glossary_volte_hd_voice_call:I = 0x7f0a0617

.field public static final icon_glossary_volte_share_video:I = 0x7f0a061b

.field public static final icon_glossary_volte_switch_camera:I = 0x7f0a061c

.field public static final icon_glossary_volte_video_call:I = 0x7f0a0618

.field public static final icon_glossary_volte_wifi_calling:I = 0x7f0a061d

.field public static final imageview_navigate_home_screen:I = 0x7f0a0122

.field public static final imageview_view_all_apps:I = 0x7f0a0123

.field public static final item1_quickconnect_description_1:I = 0x7f0a08af

.field public static final item1_quickconnect_description_1_verizon:I = 0x7f0a08b0

.field public static final item1_quickconnect_description_2:I = 0x7f0a08b1

.field public static final item1_quickconnect_description_3:I = 0x7f0a08b2

.field public static final item1_quickconnect_description_3_tts:I = 0x7f0a08b3

.field public static final item1_quickconnect_tip_1:I = 0x7f0a08b4

.field public static final item1_quickconnect_tip_1_verizon:I = 0x7f0a08b5

.field public static final item2_quickconnect_description_1:I = 0x7f0a08b6

.field public static final item2_quickconnect_description_2:I = 0x7f0a08b7

.field public static final item2_quickconnect_description_3:I = 0x7f0a08b8

.field public static final item_accessibility_assistant_menu:I = 0x7f0a0900

.field public static final item_accessibility_baby_crying_detector:I = 0x7f0a08ff

.field public static final item_accessibility_direct_access:I = 0x7f0a0901

.field public static final item_accessibility_sound_detectors:I = 0x7f0a08fe

.field public static final item_accessibility_talkback:I = 0x7f0a08fd

.field public static final item_air_gesture_browser:I = 0x7f0a019b

.field public static final item_air_gesture_call_accept:I = 0x7f0a019d

.field public static final item_air_gesture_indicator:I = 0x7f0a0198

.field public static final item_air_gesture_jump:I = 0x7f0a019a

.field public static final item_air_gesture_move:I = 0x7f0a019c

.field public static final item_air_gesture_quick_glance:I = 0x7f0a0199

.field public static final item_air_view_icon_labels:I = 0x7f0a01a5

.field public static final item_air_view_information_preview:I = 0x7f0a019f

.field public static final item_air_view_introduction:I = 0x7f0a019e

.field public static final item_air_view_list_scrolling:I = 0x7f0a01a6

.field public static final item_air_view_magnifier:I = 0x7f0a01a1

.field public static final item_air_view_pointer:I = 0x7f0a01a2

.field public static final item_air_view_progress_bar:I = 0x7f0a01a3

.field public static final item_air_view_speed_dial:I = 0x7f0a01a4

.field public static final item_allshare_howto_cast_dongle:I = 0x7f0a0347

.field public static final item_allshare_learn_nfc:I = 0x7f0a0348

.field public static final item_allshare_smart_tv:I = 0x7f0a0349

.field public static final item_basic_definition_apps:I = 0x7f0a068d

.field public static final item_basic_definition_apps_desc:I = 0x7f0a068e

.field public static final item_basic_definition_bluetooth:I = 0x7f0a068f

.field public static final item_basic_definition_bluetooth_desc:I = 0x7f0a0690

.field public static final item_basic_definition_data:I = 0x7f0a0691

.field public static final item_basic_definition_data_desc:I = 0x7f0a0692

.field public static final item_basic_definition_flight_mode:I = 0x7f0a068b

.field public static final item_basic_definition_flight_mode_desc:I = 0x7f0a068c

.field public static final item_basic_definition_gps:I = 0x7f0a0693

.field public static final item_basic_definition_gps_desc:I = 0x7f0a0694

.field public static final item_basic_definition_mobile_hotspot:I = 0x7f0a0695

.field public static final item_basic_definition_mobile_hotspot_desc:I = 0x7f0a0696

.field public static final item_basic_definition_nfc:I = 0x7f0a0697

.field public static final item_basic_definition_nfc_desc:I = 0x7f0a0698

.field public static final item_basic_definition_software_update:I = 0x7f0a0699

.field public static final item_basic_definition_software_update_desc:I = 0x7f0a069a

.field public static final item_basic_definition_sync:I = 0x7f0a069b

.field public static final item_basic_definition_sync_desc:I = 0x7f0a069c

.field public static final item_basic_definition_visual_voice_mail:I = 0x7f0a069d

.field public static final item_basic_definition_visual_voice_mail_desc:I = 0x7f0a069e

.field public static final item_basic_definition_widget:I = 0x7f0a06a1

.field public static final item_basic_definition_widget_desc:I = 0x7f0a06a2

.field public static final item_basic_definition_wifi:I = 0x7f0a069f

.field public static final item_basic_definition_wifi_desc:I = 0x7f0a06a0

.field public static final item_battery_power_sawing_mode:I = 0x7f0a02a1

.field public static final item_bluetooth_tethering_01:I = 0x7f0a084a

.field public static final item_bluetooth_tethering_02:I = 0x7f0a084b

.field public static final item_bluetooth_tethering_03:I = 0x7f0a084e

.field public static final item_bluetooth_tethering_tether_your_device_vzw:I = 0x7f0a084c

.field public static final item_bluetooth_tethering_work_with_vzw:I = 0x7f0a084d

.field public static final item_bt_connect:I = 0x7f0a02ab

.field public static final item_bt_send_content:I = 0x7f0a02aa

.field public static final item_call_accept_reject_for_kk:I = 0x7f0a044d

.field public static final item_call_drag_and_drop:I = 0x7f0a02b6

.field public static final item_call_reject_with_message:I = 0x7f0a02b5

.field public static final item_call_reject_with_message_for_kk:I = 0x7f0a044e

.field public static final item_camera_camera_modes:I = 0x7f0a02f8

.field public static final item_camera_changing_camera_mode:I = 0x7f0a02fa

.field public static final item_camera_changing_the_settings:I = 0x7f0a02fb

.field public static final item_camera_introduction:I = 0x7f0a02f2

.field public static final item_camera_record_videos:I = 0x7f0a02f6

.field public static final item_camera_recording_videos:I = 0x7f0a02f7

.field public static final item_camera_take_dual_picture:I = 0x7f0a02f5

.field public static final item_camera_take_picture:I = 0x7f0a02f3

.field public static final item_camera_taking_pictures:I = 0x7f0a02f4

.field public static final item_camera_viewing_pics_and_videos:I = 0x7f0a02fc

.field public static final item_chatonv_howto_use:I = 0x7f0a030e

.field public static final item_checking_data_usage:I = 0x7f0a0934

.field public static final item_cocktail_service_editing_express_me:I = 0x7f0a07cf

.field public static final item_cocktail_service_managing_panels:I = 0x7f0a07ce

.field public static final item_cocktail_service_navigating_panels:I = 0x7f0a07cd

.field public static final item_cocktail_service_using_night_clock:I = 0x7f0a07d1

.field public static final item_cocktail_service_using_quick_tools:I = 0x7f0a07d0

.field public static final item_cocktail_service_waking_up_edge_screen:I = 0x7f0a07cc

.field public static final item_collecting_content:I = 0x7f0a09fa

.field public static final item_contacts_add:I = 0x7f0a02b8

.field public static final item_contacts_setup_speed_dial:I = 0x7f0a02b9

.field public static final item_dc_camera_modes:I = 0x7f0a02f9

.field public static final item_dc_dual_picture:I = 0x7f0a01c1

.field public static final item_dc_introduction:I = 0x7f0a01be

.field public static final item_dc_record_videos:I = 0x7f0a01c0

.field public static final item_dc_take_pictures:I = 0x7f0a01bf

.field public static final item_download_booster_using_download_booster:I = 0x7f0a0897

.field public static final item_easy_hs_add_apps_text_1:I = 0x7f0a086f

.field public static final item_easy_hs_add_apps_text_2:I = 0x7f0a0870

.field public static final item_easy_hs_add_contacts:I = 0x7f0a086d

.field public static final item_easy_hs_add_contacts_text_1:I = 0x7f0a0871

.field public static final item_easy_hs_add_contacts_text_2:I = 0x7f0a0872

.field public static final item_easy_hs_add_contacts_text_3:I = 0x7f0a0873

.field public static final item_easy_hs_remove_apps_text_1:I = 0x7f0a0874

.field public static final item_easy_hs_remove_apps_text_2:I = 0x7f0a0875

.field public static final item_easy_hs_view_more_apps:I = 0x7f0a086e

.field public static final item_edit_quick_settings_edit_quick_settings:I = 0x7f0a093e

.field public static final item_editing_homescreen:I = 0x7f0a09a1

.field public static final item_email_change_signature:I = 0x7f0a02e4

.field public static final item_email_composing_emails:I = 0x7f0a02df

.field public static final item_email_create_filtering:I = 0x7f0a02e3

.field public static final item_email_managing_emails:I = 0x7f0a02e0

.field public static final item_email_reading_emails:I = 0x7f0a02de

.field public static final item_email_set_priority_sender:I = 0x7f0a02e2

.field public static final item_email_setup_account:I = 0x7f0a02dc

.field public static final item_email_setup_account_2:I = 0x7f0a02dd

.field public static final item_email_setup_pop_imap:I = 0x7f0a02e1

.field public static final item_enabling_car_mode:I = 0x7f0a096b

.field public static final item_gallery_category_tag:I = 0x7f0a0307

.field public static final item_gallery_contextual_tag:I = 0x7f0a0302

.field public static final item_gallery_date_tag:I = 0x7f0a0306

.field public static final item_gallery_deleting_a_picture:I = 0x7f0a0341

.field public static final item_gallery_deleting_multiple_pictures:I = 0x7f0a0342

.field public static final item_gallery_deleting_picture_title:I = 0x7f0a0340

.field public static final item_gallery_face_tagging:I = 0x7f0a0301

.field public static final item_gallery_filtering_pictures:I = 0x7f0a0330

.field public static final item_gallery_howto_collage:I = 0x7f0a030a

.field public static final item_gallery_howto_copy:I = 0x7f0a030c

.field public static final item_gallery_howto_edit:I = 0x7f0a0309

.field public static final item_gallery_howto_entertainer:I = 0x7f0a030d

.field public static final item_gallery_howto_selection_mode:I = 0x7f0a030b

.field public static final item_gallery_location_tag:I = 0x7f0a0304

.field public static final item_gallery_person_tag:I = 0x7f0a0305

.field public static final item_gallery_playing_videos:I = 0x7f0a0300

.field public static final item_gallery_resizing_thumbnails:I = 0x7f0a0329

.field public static final item_gallery_sharing_a_picture:I = 0x7f0a0338

.field public static final item_gallery_sharing_multiple_pictures:I = 0x7f0a0339

.field public static final item_gallery_sharing_picture_title:I = 0x7f0a0337

.field public static final item_gallery_user_tag:I = 0x7f0a0308

.field public static final item_gallery_viewing_album:I = 0x7f0a032c

.field public static final item_gallery_viewing_picture:I = 0x7f0a02ff

.field public static final item_gallery_weather_tag:I = 0x7f0a0303

.field public static final item_group_play_create_group:I = 0x7f0a03f8

.field public static final item_group_play_introduction:I = 0x7f0a03f7

.field public static final item_group_play_join_group:I = 0x7f0a03f9

.field public static final item_group_play_music_sharing:I = 0x7f0a03fb

.field public static final item_group_play_sharing_contents:I = 0x7f0a03fa

.field public static final item_group_play_video_sharing:I = 0x7f0a03fc

.field public static final item_guest_lounge_access_home:I = 0x7f0a02fe

.field public static final item_guest_lounge_introduction:I = 0x7f0a02fd

.field public static final item_hs_add_apps:I = 0x7f0a020d

.field public static final item_hs_add_apps_k_common:I = 0x7f0a0217

.field public static final item_hs_add_widget:I = 0x7f0a020e

.field public static final item_hs_add_widget_k_common:I = 0x7f0a0218

.field public static final item_hs_change_wallpaper_k_common:I = 0x7f0a0216

.field public static final item_hs_create_floder_1:I = 0x7f0a0219

.field public static final item_hs_create_floder_k_common:I = 0x7f0a0214

.field public static final item_hs_move_items:I = 0x7f0a020f

.field public static final item_hs_navigate:I = 0x7f0a020b

.field public static final item_hs_remove_items:I = 0x7f0a0210

.field public static final item_hs_remove_items_k_common:I = 0x7f0a0215

.field public static final item_hs_resize_widgets:I = 0x7f0a0211

.field public static final item_hs_using_flipboard_briefing:I = 0x7f0a021a

.field public static final item_hs_using_homescreen_k_common:I = 0x7f0a0213

.field public static final item_hs_using_mymagazine:I = 0x7f0a0212

.field public static final item_hs_view_all_apps:I = 0x7f0a020c

.field public static final item_icon_glossary_alarm_and_calendar:I = 0x7f0a05f9

.field public static final item_icon_glossary_battery:I = 0x7f0a05fd

.field public static final item_icon_glossary_call:I = 0x7f0a05b6

.field public static final item_icon_glossary_connectivity:I = 0x7f0a05dd

.field public static final item_icon_glossary_email:I = 0x7f0a05ec

.field public static final item_icon_glossary_email_and_messaging:I = 0x7f0a0615

.field public static final item_icon_glossary_messages:I = 0x7f0a05f0

.field public static final item_icon_glossary_network:I = 0x7f0a05c2

.field public static final item_icon_glossary_other_icons:I = 0x7f0a0602

.field public static final item_icon_glossary_sound:I = 0x7f0a05bd

.field public static final item_icon_glossary_volte:I = 0x7f0a0616

.field public static final item_keyboard_continuous_input:I = 0x7f0a021e

.field public static final item_keyboard_enabling_predictive_text:I = 0x7f0a021f

.field public static final item_keyboard_handwriting:I = 0x7f0a021c

.field public static final item_keyboard_handwriting_gesture:I = 0x7f0a021d

.field public static final item_keyboard_samsung_chineseime:I = 0x7f0a0253

.field public static final item_keyboard_samung_keyboard:I = 0x7f0a021b

.field public static final item_keys_how_to_use:I = 0x7f0a01fe

.field public static final item_lock_screen_changing_wallpaper:I = 0x7f0a0035

.field public static final item_lock_screen_changing_wallpaper_1:I = 0x7f0a0036

.field public static final item_lock_screen_changing_wallpaper_2:I = 0x7f0a003a

.field public static final item_lock_screen_changing_wallpaper_3:I = 0x7f0a003d

.field public static final item_lock_screen_changing_wallpaper_festival:I = 0x7f0a005f

.field public static final item_lock_screen_checking_event_notifications:I = 0x7f0a002d

.field public static final item_lock_screen_opening_camera:I = 0x7f0a002c

.field public static final item_managing_applications:I = 0x7f0a09a0

.field public static final item_managing_contacts:I = 0x7f0a099f

.field public static final item_messages_sending_messages:I = 0x7f0a02b2

.field public static final item_messages_sending_multimedia_messages:I = 0x7f0a02b3

.field public static final item_messages_viewing_full_conversation:I = 0x7f0a02b4

.field public static final item_messaging_add_priority_sender:I = 0x7f0a0890

.field public static final item_messaging_add_priority_sender_text:I = 0x7f0a0891

.field public static final item_messaging_send_messages:I = 0x7f0a02b0

.field public static final item_messaging_send_multimedia:I = 0x7f0a02b1

.field public static final item_mobile_broadband_for_more_information:I = 0x7f0a086c

.field public static final item_mobile_broadband_tether_your_device_vzw:I = 0x7f0a0868

.field public static final item_mobile_broadband_tether_your_device_vzw_phone:I = 0x7f0a0869

.field public static final item_mobile_broadband_work_with:I = 0x7f0a086a

.field public static final item_mobile_broadband_your_device_has_vzw:I = 0x7f0a086b

.field public static final item_mobile_hotspot_configure_mobile_hotspot_vzw:I = 0x7f0a0867

.field public static final item_mobile_hotspot_set_many_devices:I = 0x7f0a085e

.field public static final item_mobile_hotspot_to_configure:I = 0x7f0a085d

.field public static final item_mobile_hotspot_to_turn:I = 0x7f0a085c

.field public static final item_mobile_hotspot_turn_your_device_vzw:I = 0x7f0a0865

.field public static final item_mobile_hotspot_turn_your_phone:I = 0x7f0a085a

.field public static final item_mobile_hotspot_turn_your_phone2:I = 0x7f0a085b

.field public static final item_mobile_hotspot_unable_to_use_device_app_vzw:I = 0x7f0a0866

.field public static final item_motion_palm_to_capture_screen:I = 0x7f0a031a

.field public static final item_motion_palm_to_mute:I = 0x7f0a031b

.field public static final item_motion_pan_to_browse_image:I = 0x7f0a0316

.field public static final item_motion_pan_to_move_icon:I = 0x7f0a0317

.field public static final item_motion_pick_up_to_direct_call:I = 0x7f0a0313

.field public static final item_motion_pick_up_to_smart_alert:I = 0x7f0a0314

.field public static final item_motion_shake_to_update:I = 0x7f0a0318

.field public static final item_motion_tilt_to_zoom:I = 0x7f0a0315

.field public static final item_motion_turn_over_to_mute:I = 0x7f0a0319

.field public static final item_move_to_private:I = 0x7f0a09f3

.field public static final item_mw_accessing:I = 0x7f0a01c3

.field public static final item_mw_bezel:I = 0x7f0a01cc

.field public static final item_mw_drag_and_drop_content:I = 0x7f0a01ca

.field public static final item_mw_edit_items:I = 0x7f0a01c7

.field public static final item_mw_opening:I = 0x7f0a01cd

.field public static final item_mw_opening_as_popup:I = 0x7f0a01d1

.field public static final item_mw_opening_by_recents:I = 0x7f0a01d0

.field public static final item_mw_opening_from_tray:I = 0x7f0a01cf

.field public static final item_mw_pop_up_view:I = 0x7f0a01c4

.field public static final item_mw_recent_apps:I = 0x7f0a01cb

.field public static final item_mw_show_hide_tray:I = 0x7f0a01c2

.field public static final item_mw_template:I = 0x7f0a01c8

.field public static final item_mw_template_tablet:I = 0x7f0a01c9

.field public static final item_mw_tray:I = 0x7f0a01c6

.field public static final item_mw_using:I = 0x7f0a01ce

.field public static final item_mw_view:I = 0x7f0a01c5

.field public static final item_my_magazine_change_category:I = 0x7f0a087b

.field public static final item_my_magazine_introduction:I = 0x7f0a0879

.field public static final item_my_magazine_navigate:I = 0x7f0a087a

.field public static final item_nfc_payment:I = 0x7f0a02a6

.field public static final item_nfc_tap_and_pay:I = 0x7f0a02a7

.field public static final item_nfc_turn_on_nfc:I = 0x7f0a02a2

.field public static final item_nfc_turn_on_sbeam:I = 0x7f0a02a3

.field public static final item_nfc_turning_on_nfc:I = 0x7f0a02a4

.field public static final item_nfc_turning_on_sbeam:I = 0x7f0a02a5

.field public static final item_notifications_check:I = 0x7f0a0209

.field public static final item_notifications_enabledisable_function:I = 0x7f0a020a

.field public static final item_notifications_expandable:I = 0x7f0a0208

.field public static final item_notifications_panel_access_one_finger_title:I = 0x7f0a0539

.field public static final item_notifications_panel_access_quick_settings:I = 0x7f0a0206

.field public static final item_notifications_panel_access_two_finger_title:I = 0x7f0a053a

.field public static final item_notifications_panel_check_title_one:I = 0x7f0a053d

.field public static final item_notifications_panel_check_title_two:I = 0x7f0a053e

.field public static final item_notifications_use_panel:I = 0x7f0a0207

.field public static final item_phone_add_contact:I = 0x7f0a09c7

.field public static final item_phone_make_calls:I = 0x7f0a02ad

.field public static final item_phone_making_call:I = 0x7f0a09c6

.field public static final item_phone_managing_contacts:I = 0x7f0a09c8

.field public static final item_phone_save_contact:I = 0x7f0a02ae

.field public static final item_phone_using_menu:I = 0x7f0a02af

.field public static final item_photo_reader_auto_mode:I = 0x7f0a01d2

.field public static final item_photo_reader_capture:I = 0x7f0a01f6

.field public static final item_photo_reader_detect_text:I = 0x7f0a01d4

.field public static final item_photo_reader_realtime:I = 0x7f0a01f5

.field public static final item_photo_reader_translate:I = 0x7f0a01d3

.field public static final item_portable_hotspot_01:I = 0x7f0a0846

.field public static final item_portable_hotspot_02:I = 0x7f0a0847

.field public static final item_portable_hotspot_03:I = 0x7f0a0848

.field public static final item_portable_hotspot_04:I = 0x7f0a0849

.field public static final item_portable_hotspot_2014_help_01:I = 0x7f0a0852

.field public static final item_portable_hotspot_2014_help_02:I = 0x7f0a0853

.field public static final item_portable_hotspot_2014_help_03:I = 0x7f0a0854

.field public static final item_portable_hotspot_2014_help_03_tts:I = 0x7f0a0855

.field public static final item_portable_hotspot_2014_help_04:I = 0x7f0a0856

.field public static final item_portable_hotspot_Tip_01:I = 0x7f0a0857

.field public static final item_portable_hotspot_Tip_01_chn:I = 0x7f0a0858

.field public static final item_portable_hotspot_Tip_02:I = 0x7f0a0859

.field public static final item_quickconnect_tip:I = 0x7f0a08ae

.field public static final item_registering_cars:I = 0x7f0a096a

.field public static final item_remove_from_private:I = 0x7f0a09f4

.field public static final item_ringtone_change:I = 0x7f0a02ac

.field public static final item_safety_using_emergencymode_title:I = 0x7f0a08d5

.field public static final item_safety_using_geolife_title:I = 0x7f0a08d4

.field public static final item_safety_using_geonews_title:I = 0x7f0a08d3

.field public static final item_safety_using_sending_title:I = 0x7f0a08d6

.field public static final item_sbrowser_add_bookmark1_new:I = 0x7f0a0046

.field public static final item_sbrowser_add_bookmark_new:I = 0x7f0a0044

.field public static final item_sbrowser_add_bookmark_new_1b:I = 0x7f0a0045

.field public static final item_sbrowser_open_new_tab_new:I = 0x7f0a0047

.field public static final item_sbrowser_quick_access1_new:I = 0x7f0a0051

.field public static final item_sbrowser_quick_access2_new:I = 0x7f0a0052

.field public static final item_sbrowser_quick_access_new:I = 0x7f0a0050

.field public static final item_sbrowser_reading_list:I = 0x7f0a006f

.field public static final item_sbrowser_search_engine_new:I = 0x7f0a0048

.field public static final item_sbrowser_using_search_bar_new:I = 0x7f0a0053

.field public static final item_sconnect_description_1:I = 0x7f0a08a5

.field public static final item_sconnect_description_2:I = 0x7f0a08a7

.field public static final item_sconnect_description_3:I = 0x7f0a08a9

.field public static final item_sconnect_title_1:I = 0x7f0a08a4

.field public static final item_sconnect_title_2:I = 0x7f0a08a6

.field public static final item_sconnect_title_3:I = 0x7f0a08a8

.field public static final item_settings_data_usage_limit:I = 0x7f0a0935

.field public static final item_smart_remote_introduction:I = 0x7f0a01d7

.field public static final item_smart_screen_introduction:I = 0x7f0a01d8

.field public static final item_smart_screen_smart_pause:I = 0x7f0a01db

.field public static final item_smart_screen_smart_rotation:I = 0x7f0a01da

.field public static final item_smart_screen_smart_scroll:I = 0x7f0a01dc

.field public static final item_smart_screen_smart_stay:I = 0x7f0a01d9

.field public static final item_smemo_account_settings:I = 0x7f0a02ee

.field public static final item_smemo_create_and_save:I = 0x7f0a02f1

.field public static final item_smemo_drawing_tools:I = 0x7f0a02e7

.field public static final item_smemo_erase_tools:I = 0x7f0a02e9

.field public static final item_smemo_export:I = 0x7f0a02f0

.field public static final item_smemo_idea_sketch:I = 0x7f0a02ea

.field public static final item_smemo_import:I = 0x7f0a02ef

.field public static final item_smemo_manage_folders:I = 0x7f0a02ec

.field public static final item_smemo_manage_objects:I = 0x7f0a02e5

.field public static final item_smemo_manage_page_order:I = 0x7f0a02ed

.field public static final item_smemo_sketch_effect:I = 0x7f0a02eb

.field public static final item_smemo_text_tools:I = 0x7f0a02e8

.field public static final item_smemo_zoom_and_pan:I = 0x7f0a02e6

.field public static final item_snote_action_memo:I = 0x7f0a06ed

.field public static final item_snote_add_index_on_page:I = 0x7f0a06dd

.field public static final item_snote_add_page_exist_note:I = 0x7f0a06e2

.field public static final item_snote_add_pages_on_edit:I = 0x7f0a06dc

.field public static final item_snote_add_template_edit_mode:I = 0x7f0a06e4

.field public static final item_snote_change_modes_on_edit:I = 0x7f0a06d7

.field public static final item_snote_create_new_notes:I = 0x7f0a06e7

.field public static final item_snote_edit_index_on_edit:I = 0x7f0a06de

.field public static final item_snote_editing_handwriting:I = 0x7f0a06d3

.field public static final item_snote_editing_layout:I = 0x7f0a06d5

.field public static final item_snote_editing_snote:I = 0x7f0a06ea

.field public static final item_snote_erasing_handwriting:I = 0x7f0a06d4

.field public static final item_snote_go_to_the_library:I = 0x7f0a06d2

.field public static final item_snote_how_to_set_lock:I = 0x7f0a06d1

.field public static final item_snote_how_to_show_the_grid_on_page:I = 0x7f0a06e0

.field public static final item_snote_magnified_note:I = 0x7f0a06ee

.field public static final item_snote_panning_on_finger_mode:I = 0x7f0a06d9

.field public static final item_snote_panning_on_pen_mode:I = 0x7f0a06da

.field public static final item_snote_pen_preset:I = 0x7f0a06eb

.field public static final item_snote_picture_video:I = 0x7f0a06e5

.field public static final item_snote_resizing_toolbar_setting_on_edit:I = 0x7f0a06df

.field public static final item_snote_selection_mode:I = 0x7f0a06e3

.field public static final item_snote_show_hide_the_toolbar_on_edit:I = 0x7f0a06db

.field public static final item_snote_snap_note:I = 0x7f0a06ec

.field public static final item_snote_use_the_grid_on_page:I = 0x7f0a06e1

.field public static final item_snote_using_pen_settings:I = 0x7f0a06e6

.field public static final item_snote_using_snote:I = 0x7f0a06e9

.field public static final item_snote_using_text_mode:I = 0x7f0a06d6

.field public static final item_snote_using_zoom_pad:I = 0x7f0a06e8

.field public static final item_snote_view_page_preview:I = 0x7f0a06d0

.field public static final item_snote_zoom_in_on_edit:I = 0x7f0a06d8

.field public static final item_spen_gesture_capture_screen:I = 0x7f0a0886

.field public static final item_spen_gesture_capture_screen_text:I = 0x7f0a088a

.field public static final item_spen_gesture_open_action_memo:I = 0x7f0a0885

.field public static final item_spen_gesture_open_action_meno_text:I = 0x7f0a0889

.field public static final item_spen_gesture_pen_select_item:I = 0x7f0a088d

.field public static final item_spen_gesture_pen_select_text:I = 0x7f0a088c

.field public static final item_spen_gesture_select_items:I = 0x7f0a0888

.field public static final item_spen_gesture_select_text:I = 0x7f0a0887

.field public static final item_spen_gesture_select_text_text:I = 0x7f0a088b

.field public static final item_story_album_combine_albums:I = 0x7f0a01df

.field public static final item_story_album_combine_gallery:I = 0x7f0a01de

.field public static final item_story_album_create_filter_album:I = 0x7f0a01e5

.field public static final item_story_album_create_recommended_album:I = 0x7f0a01e4

.field public static final item_story_album_edit_memo:I = 0x7f0a01e3

.field public static final item_story_album_import_export_albums:I = 0x7f0a01e1

.field public static final item_story_album_introduction:I = 0x7f0a01dd

.field public static final item_story_album_share_albums:I = 0x7f0a01e0

.field public static final item_story_album_view_similiar_pic:I = 0x7f0a01e2

.field public static final item_svoice_access_svoice:I = 0x7f0a01e6

.field public static final item_svoice_connect_vehicle:I = 0x7f0a01ec

.field public static final item_svoice_connecting_to_vehicles:I = 0x7f0a01fb

.field public static final item_svoice_driving_mode_tips:I = 0x7f0a01ed

.field public static final item_svoice_editing_what_you_have_said:I = 0x7f0a01fa

.field public static final item_svoice_enable_driving:I = 0x7f0a01ea

.field public static final item_svoice_enable_hands_free:I = 0x7f0a01eb

.field public static final item_svoice_functions_available_in_driving_mode:I = 0x7f0a01ee

.field public static final item_svoice_how_to_use_voice_commands:I = 0x7f0a01f0

.field public static final item_svoice_opening_svoice:I = 0x7f0a01f8

.field public static final item_svoice_tips_for_using_svoice_hands_free:I = 0x7f0a01fd

.field public static final item_svoice_using_svoice:I = 0x7f0a01f9

.field public static final item_svoice_using_svoice_hands_free:I = 0x7f0a01f1

.field public static final item_svoice_using_svoice_hands_free_functions:I = 0x7f0a01fc

.field public static final item_svoice_voice_control:I = 0x7f0a01ef

.field public static final item_svoice_wakeup_command:I = 0x7f0a01e9

.field public static final item_svoice_wakeup_s_voice:I = 0x7f0a01e8

.field public static final item_svoice_wakeup_say:I = 0x7f0a01e7

.field public static final item_sync_google_account:I = 0x7f0a0109

.field public static final item_sync_google_account1:I = 0x7f0a010a

.field public static final item_sync_google_account2:I = 0x7f0a010b

.field public static final item_sync_google_account3:I = 0x7f0a010c

.field public static final item_sync_google_account4:I = 0x7f0a010d

.field public static final item_sync_google_account5:I = 0x7f0a010e

.field public static final item_sync_google_account5_vzw_phone:I = 0x7f0a010f

.field public static final item_sync_samsung_account:I = 0x7f0a0102

.field public static final item_sync_samsung_account1:I = 0x7f0a0103

.field public static final item_sync_samsung_account2:I = 0x7f0a0104

.field public static final item_sync_samsung_account3:I = 0x7f0a0105

.field public static final item_sync_samsung_account4:I = 0x7f0a0106

.field public static final item_sync_samsung_account5:I = 0x7f0a0107

.field public static final item_sync_samsung_account5_vzw_phone:I = 0x7f0a0108

.field public static final item_translator_search_phrases:I = 0x7f0a01f3

.field public static final item_translator_supported_languages:I = 0x7f0a01f4

.field public static final item_translator_translate_some:I = 0x7f0a01f2

.field public static final item_unlock_add_widget:I = 0x7f0a0203

.field public static final item_unlock_edit_lock_screen:I = 0x7f0a0204

.field public static final item_unlock_expand_widget:I = 0x7f0a0202

.field public static final item_unlock_lock_screen_navigation:I = 0x7f0a0201

.field public static final item_unlock_missed_event:I = 0x7f0a0205

.field public static final item_unlock_set_screen_lock:I = 0x7f0a01ff

.field public static final item_unlock_unlock_device:I = 0x7f0a0200

.field public static final item_upgrade_over_the_air:I = 0x7f0a01d6

.field public static final item_upgrade_samsung_kies:I = 0x7f0a01d5

.field public static final item_upgrade_updating_your_device_2014:I = 0x7f0a09ea

.field public static final item_usb_tethering_01:I = 0x7f0a084f

.field public static final item_usb_tethering_02:I = 0x7f0a0850

.field public static final item_usb_tethering_03:I = 0x7f0a0851

.field public static final item_usb_tethering_for_window:I = 0x7f0a0863

.field public static final item_usb_tethering_to_tether_01:I = 0x7f0a0860

.field public static final item_usb_tethering_to_tether_01_1:I = 0x7f0a0861

.field public static final item_usb_tethering_to_tether_03:I = 0x7f0a0862

.field public static final item_usb_tethering_use_tethering:I = 0x7f0a085f

.field public static final item_usb_tethering_when_you_connect:I = 0x7f0a0864

.field public static final item_using_easy_mode:I = 0x7f0a099e

.field public static final item_using_power_saving_mode:I = 0x7f0a015c

.field public static final item_using_scrapbook:I = 0x7f0a09f9

.field public static final item_using_ultra_power_saving_mode:I = 0x7f0a015d

.field public static final item_using_voice_command:I = 0x7f0a096c

.field public static final item_wifi_connect_on_device_help:I = 0x7f0a02a8

.field public static final item_wifi_connect_on_device_help_chn:I = 0x7f0a02a9

.field public static final item_writing_buddy_expand_mode:I = 0x7f0a07c4

.field public static final item_writing_buddy_filling_in_forms:I = 0x7f0a07b9

.field public static final item_writing_buddy_gestures:I = 0x7f0a07bb

.field public static final item_writing_buddy_writing_on_applications:I = 0x7f0a07ba

.field public static final items_fingerprints_register_fingerprints:I = 0x7f0a075f

.field public static final items_fingerprints_setting_fingerprint:I = 0x7f0a0761

.field public static final items_fingerprints_use_fingerprint:I = 0x7f0a0760

.field public static final items_tethering_title_2014_help:I = 0x7f0a083a

.field public static final keyboard_continuous_input_insert_key:I = 0x7f0a029c

.field public static final keyboard_enabling_predictive_text_desctiption1:I = 0x7f0a024f

.field public static final keyboard_enabling_predictive_text_desctiption2:I = 0x7f0a0250

.field public static final keyboard_enabling_predictive_text_desctiption3:I = 0x7f0a0251

.field public static final keyboard_enabling_predictive_text_desctiption3_for_talkback:I = 0x7f0a025a

.field public static final keyboard_handwriting_gesture_delete_one:I = 0x7f0a0291

.field public static final keyboard_handwriting_gesture_delete_one_tiny:I = 0x7f0a0292

.field public static final keyboard_handwriting_gesture_delete_space:I = 0x7f0a0295

.field public static final keyboard_handwriting_gesture_delete_word:I = 0x7f0a028f

.field public static final keyboard_handwriting_gesture_delete_word_tiny:I = 0x7f0a0290

.field public static final keyboard_handwriting_gesture_edit:I = 0x7f0a028e

.field public static final keyboard_handwriting_gesture_enter_complete:I = 0x7f0a0296

.field public static final keyboard_handwriting_gesture_enter_complete_gesture:I = 0x7f0a0298

.field public static final keyboard_handwriting_gesture_enter_complete_tiny:I = 0x7f0a0297

.field public static final keyboard_handwriting_gesture_enter_handwriting:I = 0x7f0a028d

.field public static final keyboard_handwriting_gesture_enter_stroke:I = 0x7f0a0299

.field public static final keyboard_handwriting_gesture_enter_stroke_gesture:I = 0x7f0a029b

.field public static final keyboard_handwriting_gesture_enter_stroke_tiny:I = 0x7f0a029a

.field public static final keyboard_handwriting_gesture_space:I = 0x7f0a0293

.field public static final keyboard_handwriting_gesture_text_insert:I = 0x7f0a0294

.field public static final keyboard_handwriting_not_overwrite:I = 0x7f0a0286

.field public static final keyboard_handwriting_recognition_each_char:I = 0x7f0a028b

.field public static final keyboard_handwriting_recognition_word:I = 0x7f0a028c

.field public static final keyboard_handwriting_recognize_char:I = 0x7f0a0287

.field public static final keyboard_handwriting_support_handwriting:I = 0x7f0a0288

.field public static final keyboard_handwriting_support_words:I = 0x7f0a0289

.field public static final keyboard_handwriting_support_words_2:I = 0x7f0a028a

.field public static final keyboard_handwriting_write_parallel:I = 0x7f0a0285

.field public static final keyboard_handwriting_written_one_char:I = 0x7f0a0284

.field public static final keyboard_samsung_chineseime_change_inputmode:I = 0x7f0a0276

.field public static final keyboard_samsung_chineseime_change_keyboard_type:I = 0x7f0a0280

.field public static final keyboard_samsung_chineseime_change_language_description:I = 0x7f0a0260

.field public static final keyboard_samsung_chineseime_change_language_two:I = 0x7f0a025f

.field public static final keyboard_samsung_chineseime_changing_input_mode:I = 0x7f0a0267

.field public static final keyboard_samsung_chineseime_changing_input_mode_description:I = 0x7f0a0268

.field public static final keyboard_samsung_chineseime_changing_keyboard_theme:I = 0x7f0a0274

.field public static final keyboard_samsung_chineseime_changing_keyboard_theme_description:I = 0x7f0a0275

.field public static final keyboard_samsung_chineseime_changing_keyboard_type:I = 0x7f0a0272

.field public static final keyboard_samsung_chineseime_changing_keyboard_type_description:I = 0x7f0a0273

.field public static final keyboard_samsung_chineseime_changing_the_keyboard_theme:I = 0x7f0a0281

.field public static final keyboard_samsung_chineseime_changing_the_keyboard_theme_icon:I = 0x7f0a0282

.field public static final keyboard_samsung_chineseime_edit_text:I = 0x7f0a027a

.field public static final keyboard_samsung_chineseime_inserting_emoticons:I = 0x7f0a0278

.field public static final keyboard_samsung_chineseime_inserting_emoticons_description:I = 0x7f0a026b

.field public static final keyboard_samsung_chineseime_inserting_text_templates:I = 0x7f0a0270

.field public static final keyboard_samsung_chineseime_inserting_text_templates_description:I = 0x7f0a0271

.field public static final keyboard_samsung_chineseime_number_button:I = 0x7f0a025d

.field public static final keyboard_samsung_chineseime_number_mode:I = 0x7f0a025c

.field public static final keyboard_samsung_chineseime_number_mode_description:I = 0x7f0a025e

.field public static final keyboard_samsung_chineseime_opening_clipboard:I = 0x7f0a0265

.field public static final keyboard_samsung_chineseime_opening_clipboard_description:I = 0x7f0a0266

.field public static final keyboard_samsung_chineseime_opening_keyboard_settings:I = 0x7f0a0263

.field public static final keyboard_samsung_chineseime_opening_keyboard_settings_description:I = 0x7f0a0264

.field public static final keyboard_samsung_chineseime_opening_text_editing_panel:I = 0x7f0a026c

.field public static final keyboard_samsung_chineseime_opening_text_editing_panel_description:I = 0x7f0a026d

.field public static final keyboard_samsung_chineseime_symbol_button:I = 0x7f0a0256

.field public static final keyboard_samsung_chineseime_symbol_mode:I = 0x7f0a0255

.field public static final keyboard_samsung_chineseime_symbol_mode_description:I = 0x7f0a025b

.field public static final keyboard_samsung_chineseime_tap_back:I = 0x7f0a0254

.field public static final keyboard_samsung_chineseime_tap_change_inputmode_icon:I = 0x7f0a0277

.field public static final keyboard_samsung_chineseime_tap_edit_text_icon:I = 0x7f0a027b

.field public static final keyboard_samsung_chineseime_tap_inserting_emoticons_icon:I = 0x7f0a0279

.field public static final keyboard_samsung_chineseime_tap_temporary_handwriting_icon:I = 0x7f0a027d

.field public static final keyboard_samsung_chineseime_tap_text_template_icon:I = 0x7f0a027f

.field public static final keyboard_samsung_chineseime_temporary_handwriting:I = 0x7f0a027c

.field public static final keyboard_samsung_chineseime_text_template:I = 0x7f0a027e

.field public static final keyboard_samsung_chineseime_toolbar_icon:I = 0x7f0a0262

.field public static final keyboard_samsung_chineseime_toolbar_mode:I = 0x7f0a0261

.field public static final keyboard_samsung_chineseime_using_handwriting_temporarily:I = 0x7f0a026e

.field public static final keyboard_samsung_chineseime_using_handwriting_temporarily_description:I = 0x7f0a026f

.field public static final keyboard_samsung_chineseime_using_voice_input:I = 0x7f0a0269

.field public static final keyboard_samsung_chineseime_using_voide_input_description:I = 0x7f0a026a

.field public static final keyboard_samsung_keyboard_hanja:I = 0x7f0a07a8

.field public static final keyboard_samsung_keyboard_tap_hanja_icon:I = 0x7f0a07a9

.field public static final keyboard_samung_keyboard_change_input_method:I = 0x7f0a0229

.field public static final keyboard_samung_keyboard_change_keyboard_type:I = 0x7f0a024a

.field public static final keyboard_samung_keyboard_change_language:I = 0x7f0a022a

.field public static final keyboard_samung_keyboard_change_language_description:I = 0x7f0a022b

.field public static final keyboard_samung_keyboard_change_language_description_for_talkback:I = 0x7f0a0258

.field public static final keyboard_samung_keyboard_change_language_tip:I = 0x7f0a022c

.field public static final keyboard_samung_keyboard_change_language_two:I = 0x7f0a022d

.field public static final keyboard_samung_keyboard_change_method:I = 0x7f0a0228

.field public static final keyboard_samung_keyboard_change_mode:I = 0x7f0a0224

.field public static final keyboard_samung_keyboard_change_mode_description:I = 0x7f0a0225

.field public static final keyboard_samung_keyboard_change_mode_description_for_talkback:I = 0x7f0a0257

.field public static final keyboard_samung_keyboard_clipboard:I = 0x7f0a0246

.field public static final keyboard_samung_keyboard_close_keyboard:I = 0x7f0a0222

.field public static final keyboard_samung_keyboard_cursor_handler:I = 0x7f0a0235

.field public static final keyboard_samung_keyboard_emoji_emoticon:I = 0x7f0a024d

.field public static final keyboard_samung_keyboard_handwriting:I = 0x7f0a0242

.field public static final keyboard_samung_keyboard_hold_multi_mode_icon:I = 0x7f0a0239

.field public static final keyboard_samung_keyboard_hold_multi_mode_icon_lra:I = 0x7f0a023e

.field public static final keyboard_samung_keyboard_hold_multi_mode_icon_na:I = 0x7f0a023d

.field public static final keyboard_samung_keyboard_hold_multi_mode_icon_na_no_hwr:I = 0x7f0a023f

.field public static final keyboard_samung_keyboard_hold_multi_mode_icon_tablet:I = 0x7f0a023b

.field public static final keyboard_samung_keyboard_keyboard_setting:I = 0x7f0a0248

.field public static final keyboard_samung_keyboard_move_cursor:I = 0x7f0a022e

.field public static final keyboard_samung_keyboard_move_cursor_description:I = 0x7f0a022f

.field public static final keyboard_samung_keyboard_move_cursor_tip:I = 0x7f0a0230

.field public static final keyboard_samung_keyboard_move_handle:I = 0x7f0a0233

.field public static final keyboard_samung_keyboard_multi_mode_icon:I = 0x7f0a0236

.field public static final keyboard_samung_keyboard_multi_mode_icon_description:I = 0x7f0a0237

.field public static final keyboard_samung_keyboard_multi_mode_icon_description_for_talkback:I = 0x7f0a0259

.field public static final keyboard_samung_keyboard_multi_mode_icon_description_us:I = 0x7f0a0238

.field public static final keyboard_samung_keyboard_multi_mode_icon_na:I = 0x7f0a023c

.field public static final keyboard_samung_keyboard_multi_mode_icon_tablet:I = 0x7f0a023a

.field public static final keyboard_samung_keyboard_open_keyboard:I = 0x7f0a0220

.field public static final keyboard_samung_keyboard_select_text:I = 0x7f0a0231

.field public static final keyboard_samung_keyboard_tap_back:I = 0x7f0a0223

.field public static final keyboard_samung_keyboard_tap_change_keyboard_type_icon:I = 0x7f0a024b

.field public static final keyboard_samung_keyboard_tap_change_keyboard_type_icon_one_hand:I = 0x7f0a024c

.field public static final keyboard_samung_keyboard_tap_clipboard_icon:I = 0x7f0a0247

.field public static final keyboard_samung_keyboard_tap_emoji_emoticon_icon:I = 0x7f0a024e

.field public static final keyboard_samung_keyboard_tap_handwriting_icon:I = 0x7f0a0243

.field public static final keyboard_samung_keyboard_tap_input_button:I = 0x7f0a0226

.field public static final keyboard_samung_keyboard_tap_input_mode_button:I = 0x7f0a0227

.field public static final keyboard_samung_keyboard_tap_select_word:I = 0x7f0a0234

.field public static final keyboard_samung_keyboard_tap_settings_icon:I = 0x7f0a0249

.field public static final keyboard_samung_keyboard_tap_text_field:I = 0x7f0a0221

.field public static final keyboard_samung_keyboard_tap_text_input:I = 0x7f0a0232

.field public static final keyboard_samung_keyboard_tap_text_recognition_icon:I = 0x7f0a0245

.field public static final keyboard_samung_keyboard_tap_voice_icon:I = 0x7f0a0241

.field public static final keyboard_samung_keyboard_text_recognition:I = 0x7f0a0244

.field public static final keyboard_samung_keyboard_voice_input:I = 0x7f0a0240

.field public static final keys_back_key:I = 0x7f0a0074

.field public static final keys_back_key_to_return:I = 0x7f0a009b

.field public static final keys_back_key_to_return_tts:I = 0x7f0a009c

.field public static final keys_hold_recent_key:I = 0x7f0a0095

.field public static final keys_hold_recent_key_tts:I = 0x7f0a0096

.field public static final keys_home_key:I = 0x7f0a0078

.field public static final keys_home_key_to_return:I = 0x7f0a0098

.field public static final keys_home_key_to_return_sreminder:I = 0x7f0a0099

.field public static final keys_menu_key:I = 0x7f0a007c

.field public static final keys_more_options_will_be_shown:I = 0x7f0a009e

.field public static final keys_more_options_will_be_shown_tts:I = 0x7f0a009f

.field public static final keys_open_camera_app:I = 0x7f0a008f

.field public static final keys_power_key:I = 0x7f0a0070

.field public static final keys_press_folder_back_key:I = 0x7f0a0077

.field public static final keys_press_folder_home_key:I = 0x7f0a007a

.field public static final keys_press_folder_home_key_sreminder:I = 0x7f0a007b

.field public static final keys_press_home_key:I = 0x7f0a0079

.field public static final keys_press_menu_key:I = 0x7f0a007d

.field public static final keys_press_menu_key_search:I = 0x7f0a007e

.field public static final keys_press_power_key:I = 0x7f0a0071

.field public static final keys_press_press_back_key:I = 0x7f0a0075

.field public static final keys_press_press_hold_back_key:I = 0x7f0a0076

.field public static final keys_press_press_volume_key:I = 0x7f0a0073

.field public static final keys_press_recent_key:I = 0x7f0a0080

.field public static final keys_press_recent_key_vzw_1:I = 0x7f0a0081

.field public static final keys_press_recent_key_vzw_2:I = 0x7f0a0082

.field public static final keys_press_the_volume_keys_to_zoom:I = 0x7f0a008b

.field public static final keys_press_the_volumn_key:I = 0x7f0a0089

.field public static final keys_recent_key:I = 0x7f0a007f

.field public static final keys_recent_key_to_open_a_list:I = 0x7f0a0093

.field public static final keys_recent_key_to_view_the_app:I = 0x7f0a0091

.field public static final keys_recent_key_to_view_the_app_tts:I = 0x7f0a0092

.field public static final keys_shutter_key_to_open_camera:I = 0x7f0a008e

.field public static final keys_shutter_key_to_take_pictures:I = 0x7f0a008d

.field public static final keys_some_older_apps:I = 0x7f0a00a0

.field public static final keys_some_older_apps_tts:I = 0x7f0a00a1

.field public static final keys_tap_recently_opened_app:I = 0x7f0a0094

.field public static final keys_the_back_key:I = 0x7f0a009a

.field public static final keys_the_home_key:I = 0x7f0a0097

.field public static final keys_the_more_options_button:I = 0x7f0a009d

.field public static final keys_the_power_key:I = 0x7f0a0085

.field public static final keys_the_recent_key:I = 0x7f0a0090

.field public static final keys_the_shutter_key:I = 0x7f0a008c

.field public static final keys_the_volume_key:I = 0x7f0a0088

.field public static final keys_to_power_off:I = 0x7f0a0086

.field public static final keys_use_the_power_key:I = 0x7f0a0087

.field public static final keys_use_the_volume_key:I = 0x7f0a008a

.field public static final keys_using_keys_on_device:I = 0x7f0a0083

.field public static final keys_using_keys_on_device_summary:I = 0x7f0a0084

.field public static final keys_volume_key:I = 0x7f0a0072

.field public static final keys_while_in_an_application:I = 0x7f0a00a2

.field public static final learn_blocking_background_data:I = 0x7f0a015e

.field public static final learn_blocking_background_data_step_1:I = 0x7f0a015f

.field public static final learn_blocking_background_data_step_2:I = 0x7f0a0160

.field public static final learn_blocking_background_data_step_3:I = 0x7f0a0161

.field public static final learn_common_motion_direct_call_summary:I = 0x7f0a02ca

.field public static final learn_common_motion_double_tap_to_top_summary:I = 0x7f0a02cc

.field public static final learn_common_motion_item_title_direct_call:I = 0x7f0a02bd

.field public static final learn_common_motion_item_title_double_tap_to_top:I = 0x7f0a02bf

.field public static final learn_common_motion_item_title_pan_to_browse_images:I = 0x7f0a02c2

.field public static final learn_common_motion_item_title_pan_to_move_icon:I = 0x7f0a02c1

.field public static final learn_common_motion_item_title_quick_glance:I = 0x7f0a02bc

.field public static final learn_common_motion_item_title_shake_to_update:I = 0x7f0a02c3

.field public static final learn_common_motion_item_title_smart_alert:I = 0x7f0a02be

.field public static final learn_common_motion_item_title_tilt_to_zoom:I = 0x7f0a02c0

.field public static final learn_common_motion_item_title_turn_over_to_mute_pause:I = 0x7f0a02c4

.field public static final learn_common_motion_pan_to_browse_images_summary:I = 0x7f0a02cf

.field public static final learn_common_motion_pan_to_move_icon_summary:I = 0x7f0a02ce

.field public static final learn_common_motion_quick_glance_summary:I = 0x7f0a02c9

.field public static final learn_common_motion_shake_to_update_summary:I = 0x7f0a02d0

.field public static final learn_common_motion_smart_alert_summary:I = 0x7f0a02cb

.field public static final learn_common_motion_tilt_to_zoom_summary:I = 0x7f0a02cd

.field public static final learn_common_motion_turn_over_to_mute_pause_summary:I = 0x7f0a02d1

.field public static final learn_common_motions_title:I = 0x7f0a02bb

.field public static final learn_enabling_greyscale_mode:I = 0x7f0a0162

.field public static final learn_enabling_greyscale_mode_step_1:I = 0x7f0a0163

.field public static final learn_enabling_greyscale_mode_step_2:I = 0x7f0a0164

.field public static final learn_enabling_greyscale_mode_step_3:I = 0x7f0a0165

.field public static final learn_hand_motion_palm_swipe_to_capture_summary:I = 0x7f0a02d2

.field public static final learn_hand_motion_palm_touch_to_mute_pause_summary:I = 0x7f0a02d3

.field public static final learn_homescreen_image_add_widget:I = 0x7f0a0121

.field public static final learn_motion_item_title_palm_swipe_to_capture:I = 0x7f0a02c5

.field public static final learn_motion_item_title_palm_touch_to_mute_pause:I = 0x7f0a02c6

.field public static final learn_motions_activity_title:I = 0x7f0a02ba

.field public static final learn_multiwindow_item_accessing_text_1:I = 0x7f0a00a3

.field public static final learn_multiwindow_item_accessing_text_2:I = 0x7f0a00a4

.field public static final learn_multiwindow_item_drag_and_drop_text_1:I = 0x7f0a00b9

.field public static final learn_multiwindow_item_drag_and_drop_text_1_noimage:I = 0x7f0a00ba

.field public static final learn_multiwindow_item_edit_multi_window_items_text_1:I = 0x7f0a00b1

.field public static final learn_multiwindow_item_edit_multi_window_items_text_1_tablet:I = 0x7f0a00b3

.field public static final learn_multiwindow_item_edit_multi_window_items_text_2:I = 0x7f0a00b2

.field public static final learn_multiwindow_item_edit_multi_window_items_text_2_tablet:I = 0x7f0a00b4

.field public static final learn_multiwindow_item_edit_multi_window_items_text_3_tablet:I = 0x7f0a00b5

.field public static final learn_multiwindow_item_edit_multi_window_items_text_4_tablet:I = 0x7f0a00b6

.field public static final learn_multiwindow_item_multi_window_tray_text_1:I = 0x7f0a00ae

.field public static final learn_multiwindow_item_multi_window_tray_text_2:I = 0x7f0a00af

.field public static final learn_multiwindow_item_multi_window_tray_text_3:I = 0x7f0a00b0

.field public static final learn_multiwindow_item_opening_as_popup_text_1:I = 0x7f0a00d7

.field public static final learn_multiwindow_item_opening_as_popup_text_2:I = 0x7f0a00d8

.field public static final learn_multiwindow_item_opening_as_popup_text_3:I = 0x7f0a00d9

.field public static final learn_multiwindow_item_opening_as_popup_text_4:I = 0x7f0a00da

.field public static final learn_multiwindow_item_opening_as_popup_text_5:I = 0x7f0a00db

.field public static final learn_multiwindow_item_opening_as_popup_text_6:I = 0x7f0a00dc

.field public static final learn_multiwindow_item_opening_as_popup_text_7:I = 0x7f0a00dd

.field public static final learn_multiwindow_item_opening_as_popup_text_8:I = 0x7f0a00de

.field public static final learn_multiwindow_item_opening_multi_window_text_1:I = 0x7f0a00be

.field public static final learn_multiwindow_item_opening_multi_window_text_2:I = 0x7f0a00bf

.field public static final learn_multiwindow_item_opening_multi_window_text_3:I = 0x7f0a00c0

.field public static final learn_multiwindow_item_opening_multi_window_text_4:I = 0x7f0a00c1

.field public static final learn_multiwindow_item_opening_multi_window_text_5:I = 0x7f0a00c2

.field public static final learn_multiwindow_item_opening_multi_window_text_6:I = 0x7f0a00c3

.field public static final learn_multiwindow_item_opening_multi_window_text_7:I = 0x7f0a00c4

.field public static final learn_multiwindow_item_pop_up_view_text_1:I = 0x7f0a00a6

.field public static final learn_multiwindow_item_recent_apps_text_1:I = 0x7f0a00bb

.field public static final learn_multiwindow_item_show_multi_window_tray_text_1:I = 0x7f0a00a5

.field public static final learn_multiwindow_item_show_multi_window_tray_text_2:I = 0x7f0a00a7

.field public static final learn_multiwindow_item_template_multi_window_text_1:I = 0x7f0a00b7

.field public static final learn_multiwindow_item_template_multi_window_text_1_tablet:I = 0x7f0a00b8

.field public static final learn_multiwindow_item_use_multi_window_text_1:I = 0x7f0a00aa

.field public static final learn_multiwindow_item_use_multi_window_text_2:I = 0x7f0a00ab

.field public static final learn_multiwindow_item_use_multi_window_text_2_tablet:I = 0x7f0a00ac

.field public static final learn_multiwindow_item_use_multi_window_text_3:I = 0x7f0a00ad

.field public static final learn_multiwindow_item_using_multi_window_bezel_text_1:I = 0x7f0a00a8

.field public static final learn_multiwindow_item_using_multi_window_bezel_text_1_tablet:I = 0x7f0a00a9

.field public static final learn_multiwindow_item_using_multiwindow_text_1:I = 0x7f0a00c5

.field public static final learn_multiwindow_item_using_multiwindow_text_10:I = 0x7f0a00d4

.field public static final learn_multiwindow_item_using_multiwindow_text_10_tts:I = 0x7f0a00d5

.field public static final learn_multiwindow_item_using_multiwindow_text_11:I = 0x7f0a00d6

.field public static final learn_multiwindow_item_using_multiwindow_text_2:I = 0x7f0a00c6

.field public static final learn_multiwindow_item_using_multiwindow_text_2_tts:I = 0x7f0a00c7

.field public static final learn_multiwindow_item_using_multiwindow_text_3:I = 0x7f0a00c8

.field public static final learn_multiwindow_item_using_multiwindow_text_3_tts:I = 0x7f0a00c9

.field public static final learn_multiwindow_item_using_multiwindow_text_4:I = 0x7f0a00ca

.field public static final learn_multiwindow_item_using_multiwindow_text_4_tts:I = 0x7f0a00cb

.field public static final learn_multiwindow_item_using_multiwindow_text_5:I = 0x7f0a00cc

.field public static final learn_multiwindow_item_using_multiwindow_text_5_tts:I = 0x7f0a00cd

.field public static final learn_multiwindow_item_using_multiwindow_text_6:I = 0x7f0a00ce

.field public static final learn_multiwindow_item_using_multiwindow_text_6_tts:I = 0x7f0a00cf

.field public static final learn_multiwindow_item_using_multiwindow_text_7:I = 0x7f0a00d0

.field public static final learn_multiwindow_item_using_multiwindow_text_8:I = 0x7f0a00d1

.field public static final learn_multiwindow_item_using_multiwindow_text_8_tts:I = 0x7f0a00d2

.field public static final learn_multiwindow_item_using_multiwindow_text_9:I = 0x7f0a00d3

.field public static final learn_ultra_power_saving_mode_step_2:I = 0x7f0a0166

.field public static final learn_ultra_power_saving_mode_step_3:I = 0x7f0a0167

.field public static final learn_ultra_power_saving_mode_step_4:I = 0x7f0a0168

.field public static final learn_ultra_power_saving_mode_step_5:I = 0x7f0a0169

.field public static final learn_ultra_power_saving_mode_step_6:I = 0x7f0a016a

.field public static final learn_ultra_power_saving_mode_step_7:I = 0x7f0a016b

.field public static final learn_ultra_power_saving_mode_step_8:I = 0x7f0a016c

.field public static final learn_ultra_power_saving_mode_step_8_chn:I = 0x7f0a016d

.field public static final lock_screen_changing_wallpaper_1_1:I = 0x7f0a0037

.field public static final lock_screen_changing_wallpaper_1_2:I = 0x7f0a0038

.field public static final lock_screen_changing_wallpaper_1_3:I = 0x7f0a0039

.field public static final lock_screen_changing_wallpaper_2_1:I = 0x7f0a003b

.field public static final lock_screen_changing_wallpaper_2_2:I = 0x7f0a003c

.field public static final lock_screen_changing_wallpaper_3_1:I = 0x7f0a003e

.field public static final lock_screen_changing_wallpaper_3_2:I = 0x7f0a003f

.field public static final lock_screen_changing_wallpaper_3_3:I = 0x7f0a0040

.field public static final lock_screen_changing_wallpaper_festival_1:I = 0x7f0a0060

.field public static final lock_screen_changing_wallpaper_festival_2:I = 0x7f0a0061

.field public static final lock_screen_changing_wallpaper_festival_3:I = 0x7f0a0062

.field public static final lock_screen_changing_wallpaper_tip_1:I = 0x7f0a0041

.field public static final lock_screen_changing_wallpaper_tip_2:I = 0x7f0a0042

.field public static final main_activity_title:I = 0x7f0a001f

.field public static final making_call_desc1_1:I = 0x7f0a09ca

.field public static final making_call_desc1_2:I = 0x7f0a09cb

.field public static final making_call_desc1_2_tts:I = 0x7f0a09cc

.field public static final making_call_desc2_1:I = 0x7f0a09cf

.field public static final making_call_desc3_1:I = 0x7f0a09d2

.field public static final making_call_desc4_1:I = 0x7f0a09d4

.field public static final making_call_header1:I = 0x7f0a09c9

.field public static final making_call_header2:I = 0x7f0a09ce

.field public static final making_call_header3:I = 0x7f0a09d1

.field public static final making_call_header4:I = 0x7f0a09d3

.field public static final making_call_tip1:I = 0x7f0a09cd

.field public static final making_call_tip2:I = 0x7f0a09d0

.field public static final managing_applications_text_1_1:I = 0x7f0a09ba

.field public static final managing_applications_text_1_2:I = 0x7f0a09bb

.field public static final managing_applications_text_2_3:I = 0x7f0a09bc

.field public static final managing_applications_text_2_3_tb:I = 0x7f0a09bd

.field public static final managing_applications_text_2_5:I = 0x7f0a09be

.field public static final managing_applications_text_2_5_tb:I = 0x7f0a09bf

.field public static final managing_applications_title_1:I = 0x7f0a09b8

.field public static final managing_applications_title_2:I = 0x7f0a09b9

.field public static final managing_contact_desc1_1:I = 0x7f0a09e3

.field public static final managing_contact_desc1_2:I = 0x7f0a09e4

.field public static final managing_contact_desc1_2_tts:I = 0x7f0a09e5

.field public static final managing_contact_desc2_1:I = 0x7f0a09e7

.field public static final managing_contact_desc2_2:I = 0x7f0a09e8

.field public static final managing_contact_desc2_2_tts:I = 0x7f0a09e9

.field public static final managing_contact_header1:I = 0x7f0a09e2

.field public static final managing_contact_header2:I = 0x7f0a09e6

.field public static final managing_contacts_text_1_1:I = 0x7f0a09af

.field public static final managing_contacts_text_1_2:I = 0x7f0a09b0

.field public static final managing_contacts_text_1_3:I = 0x7f0a09b1

.field public static final managing_contacts_text_1_tip:I = 0x7f0a09b2

.field public static final managing_contacts_text_2_3:I = 0x7f0a09b3

.field public static final managing_contacts_text_2_3_tb:I = 0x7f0a09b4

.field public static final managing_contacts_text_2_4:I = 0x7f0a09b5

.field public static final managing_contacts_text_2_5:I = 0x7f0a09b6

.field public static final managing_contacts_text_2_5_tb:I = 0x7f0a09b7

.field public static final managing_contacts_title_1:I = 0x7f0a09ad

.field public static final managing_contacts_title_2:I = 0x7f0a09ae

.field public static final menu_search:I = 0x7f0a012b

.field public static final messages_continuing_conversations:I = 0x7f0a0477

.field public static final messages_more_info:I = 0x7f0a0487

.field public static final messages_sending_messages_1:I = 0x7f0a0478

.field public static final messages_sending_messages_1_tts:I = 0x7f0a0479

.field public static final messages_sending_messages_2:I = 0x7f0a047a

.field public static final messages_sending_messages_2_tts:I = 0x7f0a047b

.field public static final messages_sending_messages_3:I = 0x7f0a047c

.field public static final messages_sending_messages_3_1:I = 0x7f0a047d

.field public static final messages_sending_messages_3_tts:I = 0x7f0a047e

.field public static final messages_sending_messages_4:I = 0x7f0a047f

.field public static final messages_sending_messages_5:I = 0x7f0a0480

.field public static final messages_sending_multimedia_messages_3_tts:I = 0x7f0a0481

.field public static final messages_sending_multimedia_messages_4:I = 0x7f0a0482

.field public static final messages_sending_new_messages:I = 0x7f0a0476

.field public static final messages_viewing_full_conversation_1:I = 0x7f0a0483

.field public static final messages_viewing_full_conversation_2:I = 0x7f0a0484

.field public static final messages_viewing_full_conversation_tip:I = 0x7f0a0485

.field public static final messages_viewing_full_conversation_tip_tts:I = 0x7f0a0486

.field public static final more_info:I = 0x7f0a03c7

.field public static final more_info_title:I = 0x7f0a074d

.field public static final motion_call_displayed_contact:I = 0x7f0a031c

.field public static final motion_disabled_message:I = 0x7f0a02d7

.field public static final motion_is_disabled_summary:I = 0x7f0a02d5

.field public static final motion_is_disabled_title:I = 0x7f0a02d4

.field public static final motion_palm_to_capture_description:I = 0x7f0a0324

.field public static final motion_palm_to_mute_description:I = 0x7f0a0325

.field public static final motion_pan_to_browse_description:I = 0x7f0a031f

.field public static final motion_pan_to_move_description:I = 0x7f0a0320

.field public static final motion_pick_up_smart_alert_description:I = 0x7f0a031d

.field public static final motion_ring_message:I = 0x7f0a02da

.field public static final motion_ringer_attention:I = 0x7f0a02d8

.field public static final motion_shake_to_update_description:I = 0x7f0a0321

.field public static final motion_shake_to_update_description_basicapp:I = 0x7f0a0322

.field public static final motion_tilt_to_zoom_description:I = 0x7f0a031e

.field public static final motion_turn_over_to_mute_description:I = 0x7f0a0323

.field public static final motion_vibrate_only:I = 0x7f0a02d9

.field public static final motion_view_albums_list_description:I = 0x7f0a0326

.field public static final motion_view_video_chapter_list_description:I = 0x7f0a0327

.field public static final mymagazine_enabling_description_1:I = 0x7f0a0a19

.field public static final mymagazine_enabling_description_2:I = 0x7f0a0a1a

.field public static final mymagazine_enabling_description_2_TTS:I = 0x7f0a0a1b

.field public static final mymagazine_enabling_description_3:I = 0x7f0a0a1c

.field public static final mymagazine_enabling_my_magazine:I = 0x7f0a0a18

.field public static final mymagazine_help_arts:I = 0x7f0a0a1e

.field public static final mymagazine_help_books:I = 0x7f0a0a21

.field public static final mymagazine_help_business:I = 0x7f0a0a20

.field public static final mymagazine_help_change_category_item_change_categories:I = 0x7f0a087f

.field public static final mymagazine_help_change_category_item_open_drawer:I = 0x7f0a087e

.field public static final mymagazine_help_content_will_be_available_later:I = 0x7f0a0a24

.field public static final mymagazine_help_food:I = 0x7f0a0a22

.field public static final mymagazine_help_introduction_mymagazine:I = 0x7f0a087d

.field public static final mymagazine_help_introduction_of_my_magazine:I = 0x7f0a087c

.field public static final mymagazine_help_mymagazine:I = 0x7f0a0a1d

.field public static final mymagazine_help_navigate_step1:I = 0x7f0a0880

.field public static final mymagazine_help_navigate_step2:I = 0x7f0a0881

.field public static final mymagazine_help_navigate_step3:I = 0x7f0a0882

.field public static final mymagazine_help_news:I = 0x7f0a0a1f

.field public static final mymagazine_help_todays_picks:I = 0x7f0a0a23

.field public static final mymagazine_reordering_content:I = 0x7f0a0a15

.field public static final mymagazine_reordering_content_description_1:I = 0x7f0a0a16

.field public static final mymagazine_reordering_content_description_2:I = 0x7f0a0a17

.field public static final mymagazine_setting_content_categories:I = 0x7f0a0a11

.field public static final mymagazine_setting_content_categories_description_1:I = 0x7f0a0a12

.field public static final mymagazine_setting_content_categories_description_1_TTS:I = 0x7f0a0a13

.field public static final mymagazine_setting_content_categories_description_2:I = 0x7f0a0a14

.field public static final mymagazine_viewing_content:I = 0x7f0a0a0e

.field public static final mymagazine_viewing_content_description_1:I = 0x7f0a0a0f

.field public static final mymagazine_viewing_content_description_2:I = 0x7f0a0a10

.field public static final network_is_currently_unavailable:I = 0x7f0a0585

.field public static final nfc_open_settings:I = 0x7f0a0545

.field public static final nfc_payment_description:I = 0x7f0a0559

.field public static final nfc_payment_description_2:I = 0x7f0a055a

.field public static final nfc_payment_description_3:I = 0x7f0a055b

.field public static final nfc_select_connections_tab:I = 0x7f0a0546

.field public static final nfc_select_sbeam:I = 0x7f0a0547

.field public static final nfc_tap_and_pay_desc1:I = 0x7f0a0551

.field public static final nfc_tap_and_pay_desc2:I = 0x7f0a0552

.field public static final nfc_tap_and_pay_desc3:I = 0x7f0a0553

.field public static final nfc_tap_and_pay_desc4:I = 0x7f0a0554

.field public static final nfc_tap_and_pay_desc5:I = 0x7f0a0555

.field public static final nfc_tap_and_pay_desc6:I = 0x7f0a0556

.field public static final nfc_tap_and_pay_desc6_usa:I = 0x7f0a0557

.field public static final nfc_tap_and_pay_desc7:I = 0x7f0a0558

.field public static final nfc_toggle_sbeam:I = 0x7f0a0548

.field public static final nfc_turn_on_tap:I = 0x7f0a054f

.field public static final nfc_turn_on_tap_tts:I = 0x7f0a0550

.field public static final no_data_connection:I = 0x7f0a0127

.field public static final notifcations_panel_tip_one:I = 0x7f0a0543

.field public static final notifcations_panel_tip_two:I = 0x7f0a0544

.field public static final notifications_expandable_display_larger:I = 0x7f0a0538

.field public static final notifications_panel_access_one_finger_desc:I = 0x7f0a053b

.field public static final notifications_panel_access_two_finger_desc:I = 0x7f0a053c

.field public static final notifications_panel_check_expand_desc:I = 0x7f0a0540

.field public static final notifications_panel_check_view_desc:I = 0x7f0a053f

.field public static final notifications_panel_enabledisable_function_desc_one:I = 0x7f0a0541

.field public static final notifications_panel_enabledisable_function_desc_two:I = 0x7f0a0542

.field public static final notifications_use_panel_ongoing_list:I = 0x7f0a0537

.field public static final notifications_use_panel_tap_button:I = 0x7f0a0535

.field public static final notifications_use_panel_toggle_button:I = 0x7f0a0536

.field public static final number_0:I = 0x7f0a002b

.field public static final number_1:I = 0x7f0a0021

.field public static final number_10:I = 0x7f0a002a

.field public static final number_2:I = 0x7f0a0022

.field public static final number_3:I = 0x7f0a0023

.field public static final number_4:I = 0x7f0a0024

.field public static final number_5:I = 0x7f0a0025

.field public static final number_6:I = 0x7f0a0026

.field public static final number_7:I = 0x7f0a0027

.field public static final number_8:I = 0x7f0a0028

.field public static final number_9:I = 0x7f0a0029

.field public static final onehand_resizing_screen_size:I = 0x7f0a0944

.field public static final onehand_resizing_screen_size_desc:I = 0x7f0a0945

.field public static final onehand_resizing_screen_sub_title_1:I = 0x7f0a0950

.field public static final onehand_resizing_screen_sub_title_2:I = 0x7f0a0951

.field public static final onehand_resizing_screen_sub_title_3:I = 0x7f0a0952

.field public static final onehand_resizing_screen_text_1:I = 0x7f0a0946

.field public static final onehand_resizing_screen_text_2:I = 0x7f0a0947

.field public static final onehand_resizing_screen_text_3:I = 0x7f0a0948

.field public static final onehand_resizing_screen_text_3_desc:I = 0x7f0a0949

.field public static final onehand_resizing_screen_text_4:I = 0x7f0a094a

.field public static final onehand_resizing_screen_text_4_desc:I = 0x7f0a094b

.field public static final onehand_resizing_screen_text_5:I = 0x7f0a094c

.field public static final onehand_resizing_screen_text_6:I = 0x7f0a094d

.field public static final onehand_resizing_screen_text_7:I = 0x7f0a094e

.field public static final onehand_resizing_screen_text_8:I = 0x7f0a094f

.field public static final onehand_using_side_key_panel:I = 0x7f0a095f

.field public static final onehand_using_side_key_panel_sub_title_1:I = 0x7f0a0966

.field public static final onehand_using_side_key_panel_sub_title_2:I = 0x7f0a0967

.field public static final onehand_using_side_key_panel_text_1:I = 0x7f0a0960

.field public static final onehand_using_side_key_panel_text_2:I = 0x7f0a0961

.field public static final onehand_using_side_key_panel_text_2_desc:I = 0x7f0a0962

.field public static final onehand_using_side_key_panel_text_3:I = 0x7f0a0963

.field public static final onehand_using_side_key_panel_text_4:I = 0x7f0a0964

.field public static final onehand_using_side_key_panel_text_5:I = 0x7f0a0965

.field public static final onehand_using_side_menu:I = 0x7f0a0953

.field public static final onehand_using_side_menu_text_1:I = 0x7f0a0954

.field public static final onehand_using_side_menu_text_2:I = 0x7f0a0955

.field public static final onehand_using_side_menu_text_3:I = 0x7f0a0956

.field public static final onehand_using_side_menu_text_4:I = 0x7f0a0957

.field public static final onehand_using_side_menu_text_5:I = 0x7f0a0958

.field public static final onehand_using_side_menu_text_6:I = 0x7f0a0959

.field public static final onehand_using_side_menu_text_7:I = 0x7f0a095a

.field public static final onehand_using_side_menu_text_8:I = 0x7f0a095b

.field public static final onehand_using_side_menu_text_9:I = 0x7f0a095c

.field public static final onehand_using_side_sub_title_1:I = 0x7f0a095d

.field public static final onehand_using_side_sub_title_2:I = 0x7f0a095e

.field public static final palm_motion_use_popup_msg:I = 0x7f0a0583

.field public static final permission_description_carrier:I = 0x7f0a06be

.field public static final permission_description_get_data:I = 0x7f0a06c0

.field public static final permission_description_open_help_page:I = 0x7f0a06c4

.field public static final permission_description_search_reconcile:I = 0x7f0a06c6

.field public static final permission_description_write_data:I = 0x7f0a06c2

.field public static final permission_label_carrier:I = 0x7f0a06bd

.field public static final permission_label_get_data:I = 0x7f0a06bf

.field public static final permission_label_open_help_page:I = 0x7f0a06c3

.field public static final permission_label_search_reconcile:I = 0x7f0a06c5

.field public static final permission_label_write_data:I = 0x7f0a06c1

.field public static final photo_reader_auto_mode_recognize_each_function:I = 0x7f0a03eb

.field public static final photo_reader_auto_mode_recognize_function:I = 0x7f0a03ea

.field public static final photo_reader_auto_mode_recognize_information:I = 0x7f0a03e9

.field public static final photo_reader_auto_mode_recognize_phone:I = 0x7f0a03e8

.field public static final photo_reader_capture_link_app:I = 0x7f0a0561

.field public static final photo_reader_detect_text_capture_mode:I = 0x7f0a03f0

.field public static final photo_reader_detect_text_capture_target_image:I = 0x7f0a03f1

.field public static final photo_reader_detect_text_display:I = 0x7f0a03f4

.field public static final photo_reader_detect_text_handler:I = 0x7f0a03f3

.field public static final photo_reader_detect_text_text:I = 0x7f0a03f2

.field public static final photo_reader_realtime_link_app:I = 0x7f0a0560

.field public static final photo_reader_translate_capture_mode:I = 0x7f0a03ec

.field public static final photo_reader_translate_capture_target_image:I = 0x7f0a03ed

.field public static final photo_reader_translate_device_recognise:I = 0x7f0a03ef

.field public static final photo_reader_translate_tap_word:I = 0x7f0a03ee

.field public static final physicalkey_image1:I = 0x7f0a0124

.field public static final pin_board_help_item_easy_clip:I = 0x7f0a07ad

.field public static final pin_board_help_item_easy_clip_summary:I = 0x7f0a07af

.field public static final pin_board_help_item_scrapbooker:I = 0x7f0a07ae

.field public static final pin_board_help_item_scrapbooker_summary:I = 0x7f0a07b0

.field public static final preloaded_browser_item_title_caption_use:I = 0x7f0a011b

.field public static final preloaded_browser_use_text_1:I = 0x7f0a011c

.field public static final preloaded_browser_use_text_2:I = 0x7f0a011d

.field public static final preloaded_browser_use_text_bookmarks:I = 0x7f0a011e

.field public static final preloaded_browser_use_text_new_tab:I = 0x7f0a011f

.field public static final preloaded_messaging_item_open_messaging:I = 0x7f0a0463

.field public static final preloaded_messaging_item_recipients:I = 0x7f0a0465

.field public static final preloaded_messaging_item_start_messaging:I = 0x7f0a0464

.field public static final preloaded_messaging_item_tap_message_field:I = 0x7f0a0466

.field public static final preloaded_messaging_send_picture_message_attach_picture:I = 0x7f0a0467

.field public static final preloaded_messaging_send_picture_message_take_picture:I = 0x7f0a0468

.field public static final preloaded_phone_drag_and_drop:I = 0x7f0a02b7

.field public static final preloaded_phone_drag_and_drop_tts:I = 0x7f0a0475

.field public static final preloaded_phone_enter_the_number_you_want_to_dial:I = 0x7f0a0112

.field public static final preloaded_phone_enter_the_number_you_want_to_save:I = 0x7f0a0115

.field public static final preloaded_phone_make_call_enter_number:I = 0x7f0a0118

.field public static final preloaded_phone_make_call_enter_number_without_video:I = 0x7f0a0119

.field public static final preloaded_phone_open_phone:I = 0x7f0a0111

.field public static final preloaded_phone_save_a_contact_from_dialer_text:I = 0x7f0a011a

.field public static final preloaded_phone_select_a_saving_option_enter_the_contact_details:I = 0x7f0a0117

.field public static final preloaded_phone_tap_add_to_contacts:I = 0x7f0a0116

.field public static final preloaded_phone_tap_to_make_a_video_call:I = 0x7f0a0114

.field public static final preloaded_phone_tap_to_make_a_voice_call:I = 0x7f0a0113

.field public static final private_mode_move_to_private_description:I = 0x7f0a09f5

.field public static final private_mode_remove_from_private_description:I = 0x7f0a09f6

.field public static final quick_settings_help_text_1:I = 0x7f0a093f

.field public static final quick_settings_help_text_2:I = 0x7f0a0940

.field public static final quick_settings_tip_text:I = 0x7f0a0941

.field public static final resizing_thumbnails_info1:I = 0x7f0a032a

.field public static final resizing_thumbnails_info2:I = 0x7f0a032b

.field public static final ripple_effect_message:I = 0x7f0a02db

.field public static final safety_emer_mode_desc1:I = 0x7f0a08e2

.field public static final safety_emer_mode_desc2:I = 0x7f0a08e3

.field public static final safety_emer_mode_desc2_1:I = 0x7f0a08e6

.field public static final safety_emer_mode_desc2_1_noicon:I = 0x7f0a08e7

.field public static final safety_emer_mode_desc2_2:I = 0x7f0a08e8

.field public static final safety_emer_mode_desc2_2_noicon:I = 0x7f0a08e9

.field public static final safety_emer_mode_desc2_3:I = 0x7f0a08ea

.field public static final safety_emer_mode_desc2_3_noicon:I = 0x7f0a08eb

.field public static final safety_emer_mode_desc2_4:I = 0x7f0a08ec

.field public static final safety_emer_mode_desc2_4_noicon:I = 0x7f0a08ed

.field public static final safety_emer_mode_tip:I = 0x7f0a08e4

.field public static final safety_emer_mode_title1:I = 0x7f0a08e1

.field public static final safety_emer_mode_title2:I = 0x7f0a08e5

.field public static final safety_geo_life_desc2:I = 0x7f0a08d9

.field public static final safety_geo_life_tip1:I = 0x7f0a08df

.field public static final safety_geo_life_tip2:I = 0x7f0a08e0

.field public static final safety_geo_news_desc2:I = 0x7f0a08d8

.field public static final safety_geo_news_tip1:I = 0x7f0a08da

.field public static final safety_geo_news_tip2:I = 0x7f0a08db

.field public static final safety_geo_news_tip3:I = 0x7f0a08dc

.field public static final safety_geo_news_tip4:I = 0x7f0a08dd

.field public static final safety_geo_news_tip5:I = 0x7f0a08de

.field public static final safety_help_msg_desc1:I = 0x7f0a08ef

.field public static final safety_help_msg_desc1_noicon:I = 0x7f0a08f0

.field public static final safety_help_msg_desc2:I = 0x7f0a08f1

.field public static final safety_help_msg_desc3:I = 0x7f0a08f2

.field public static final safety_help_msg_desc3_noicon:I = 0x7f0a08f3

.field public static final safety_help_msg_desc4:I = 0x7f0a08f4

.field public static final safety_help_msg_desc5:I = 0x7f0a08f5

.field public static final safety_help_msg_desc6:I = 0x7f0a08fa

.field public static final safety_help_msg_tip1:I = 0x7f0a08f6

.field public static final safety_help_msg_tip2:I = 0x7f0a08f7

.field public static final safety_help_msg_tip3:I = 0x7f0a08f8

.field public static final safety_help_msg_title1:I = 0x7f0a08ee

.field public static final safety_help_msg_title2:I = 0x7f0a08f9

.field public static final safety_tip_title:I = 0x7f0a08d7

.field public static final sbeam_items_1:I = 0x7f0a054a

.field public static final sbeam_items_1_usa:I = 0x7f0a0549

.field public static final sbeam_items_2:I = 0x7f0a054b

.field public static final sbeam_items_3:I = 0x7f0a054c

.field public static final sbeam_items_3_tts:I = 0x7f0a054d

.field public static final sbeam_tip_description:I = 0x7f0a054e

.field public static final sbrowser_add_bookmark_open_internet_23_new:I = 0x7f0a004b

.field public static final sbrowser_add_bookmark_open_internet_24_new:I = 0x7f0a004c

.field public static final sbrowser_add_bookmark_open_internet_2_new1:I = 0x7f0a0056

.field public static final sbrowser_add_bookmark_step1_talkback:I = 0x7f0a0059

.field public static final sbrowser_add_bookmark_step2_talkback:I = 0x7f0a005a

.field public static final sbrowser_add_bookmark_step_1_int_tutorial:I = 0x7f0a0057

.field public static final sbrowser_open_internet:I = 0x7f0a03c8

.field public static final sbrowser_open_internet1_new:I = 0x7f0a004a

.field public static final sbrowser_open_internet_new:I = 0x7f0a0054

.field public static final sbrowser_open_internet_new1:I = 0x7f0a0058

.field public static final sbrowser_open_internet_new_new:I = 0x7f0a0049

.field public static final sbrowser_open_new_tab_open_internet_2_new:I = 0x7f0a004d

.field public static final sbrowser_open_window_step1_talkback:I = 0x7f0a005c

.field public static final sbrowser_open_window_step2_talkback:I = 0x7f0a005d

.field public static final sbrowser_quick_access_2_new:I = 0x7f0a0055

.field public static final sbrowser_quick_access_step1:I = 0x7f0a005e

.field public static final sbrowser_reading_list_3:I = 0x7f0a03c9

.field public static final sbrowser_setting_homepage_talkback:I = 0x7f0a005b

.field public static final sbrowser_using_search_bar_open_internet_21_new:I = 0x7f0a004f

.field public static final sbrowser_using_search_bar_open_internet_2_new:I = 0x7f0a004e

.field public static final search_activity_title:I = 0x7f0a0020

.field public static final search_edit_text_hint:I = 0x7f0a034a

.field public static final search_no_result_found:I = 0x7f0a06b4

.field public static final search_no_result_found_button:I = 0x7f0a06b7

.field public static final search_no_result_found_button_user_menual:I = 0x7f0a06b8

.field public static final search_no_result_found_online:I = 0x7f0a06b5

.field public static final search_no_result_found_user_manual:I = 0x7f0a06b6

.field public static final section_accessibility:I = 0x7f0a08fb

.field public static final section_accessibility_summary:I = 0x7f0a08fc

.field public static final section_actionmemo:I = 0x7f0a0a25

.field public static final section_actionmemo_summary:I = 0x7f0a0a26

.field public static final section_air_button:I = 0x7f0a076c

.field public static final section_air_button_summary:I = 0x7f0a076d

.field public static final section_air_gesture:I = 0x7f0a0137

.field public static final section_air_gesture_summary:I = 0x7f0a0138

.field public static final section_air_view:I = 0x7f0a0139

.field public static final section_air_view_pen_only_summary:I = 0x7f0a01ba

.field public static final section_air_view_pen_summary:I = 0x7f0a01a7

.field public static final section_air_view_summary:I = 0x7f0a013a

.field public static final section_allshare_cast:I = 0x7f0a0191

.field public static final section_allshare_cast_chn:I = 0x7f0a0192

.field public static final section_allshare_cast_summary:I = 0x7f0a0193

.field public static final section_basic_definition:I = 0x7f0a0689

.field public static final section_basic_definition_summary:I = 0x7f0a068a

.field public static final section_battery_conservation:I = 0x7f0a0159

.field public static final section_battery_conservation_summary:I = 0x7f0a015a

.field public static final section_bluetooth:I = 0x7f0a0175

.field public static final section_bluetooth_summary:I = 0x7f0a0176

.field public static final section_bluetooth_summary_on_device_help:I = 0x7f0a0177

.field public static final section_call:I = 0x7f0a017d

.field public static final section_call_summary:I = 0x7f0a017e

.field public static final section_camera:I = 0x7f0a018c

.field public static final section_camera_summary:I = 0x7f0a018d

.field public static final section_camera_summary2:I = 0x7f0a018e

.field public static final section_car_mode:I = 0x7f0a0968

.field public static final section_car_mode_summary:I = 0x7f0a0969

.field public static final section_chineseime:I = 0x7f0a0252

.field public static final section_chineseime_summary:I = 0x7f0a0283

.field public static final section_cocktail_service:I = 0x7f0a07ca

.field public static final section_cocktail_service_summary:I = 0x7f0a07cb

.field public static final section_contacts:I = 0x7f0a0183

.field public static final section_contacts_summary:I = 0x7f0a0184

.field public static final section_data_usage:I = 0x7f0a0932

.field public static final section_data_usage_summary:I = 0x7f0a0933

.field public static final section_download_booster:I = 0x7f0a0894

.field public static final section_download_booster_summary:I = 0x7f0a0895

.field public static final section_download_booster_summary_chn:I = 0x7f0a0896

.field public static final section_dual_camera:I = 0x7f0a013b

.field public static final section_dual_camera_summary:I = 0x7f0a013c

.field public static final section_easy_mode:I = 0x7f0a099d

.field public static final section_easy_mode_summary:I = 0x7f0a099c

.field public static final section_edit_quick_settings:I = 0x7f0a093c

.field public static final section_edit_quick_settings_summary:I = 0x7f0a093d

.field public static final section_email:I = 0x7f0a0185

.field public static final section_email_summary:I = 0x7f0a0186

.field public static final section_email_summary_2:I = 0x7f0a0187

.field public static final section_emeeting:I = 0x7f0a080f

.field public static final section_fingerprints:I = 0x7f0a075d

.field public static final section_fingerprints_summary:I = 0x7f0a075e

.field public static final section_galaxyfinder:I = 0x7f0a0749

.field public static final section_galaxyfinder_summary:I = 0x7f0a074a

.field public static final section_gallery:I = 0x7f0a018f

.field public static final section_gallery_summary:I = 0x7f0a0190

.field public static final section_group_play:I = 0x7f0a03f6

.field public static final section_group_play_summary:I = 0x7f0a03fd

.field public static final section_homescreen:I = 0x7f0a0156

.field public static final section_homescreen_summary:I = 0x7f0a0157

.field public static final section_keyboard:I = 0x7f0a0158

.field public static final section_keyboard_summary:I = 0x7f0a029d

.field public static final section_keyboard_summary_2014:I = 0x7f0a029e

.field public static final section_keys:I = 0x7f0a014d

.field public static final section_keys_summary:I = 0x7f0a014e

.field public static final section_keys_summary_tablet:I = 0x7f0a014f

.field public static final section_life_times_item_browser_daily_life:I = 0x7f0a08c9

.field public static final section_life_times_item_create_episode:I = 0x7f0a08ca

.field public static final section_life_times_item_discover_report:I = 0x7f0a08cb

.field public static final section_magazine_home:I = 0x7f0a0877

.field public static final section_magazine_home_item_adding_widget:I = 0x7f0a08ba

.field public static final section_magazine_home_item_adding_widget_description:I = 0x7f0a08c0

.field public static final section_magazine_home_item_change_layout:I = 0x7f0a08be

.field public static final section_magazine_home_item_change_layout_description_1:I = 0x7f0a08c4

.field public static final section_magazine_home_item_move_widget:I = 0x7f0a08bb

.field public static final section_magazine_home_item_move_widget_description_1:I = 0x7f0a08c1

.field public static final section_magazine_home_item_navigating:I = 0x7f0a08b9

.field public static final section_magazine_home_item_navigating_description_1:I = 0x7f0a08bf

.field public static final section_magazine_home_item_remove_widget:I = 0x7f0a08bc

.field public static final section_magazine_home_item_remove_widget_description_1:I = 0x7f0a08c2

.field public static final section_magazine_home_item_resize_widget:I = 0x7f0a08bd

.field public static final section_magazine_home_item_resize_widget_description_1:I = 0x7f0a08c3

.field public static final section_magazine_home_summary:I = 0x7f0a0878

.field public static final section_message:I = 0x7f0a017a

.field public static final section_message_new_summary:I = 0x7f0a017c

.field public static final section_message_summary:I = 0x7f0a017b

.field public static final section_mobile_hotspot:I = 0x7f0a083c

.field public static final section_mobile_hotspot_summary:I = 0x7f0a083f

.field public static final section_mobile_hotspot_summary_vzw:I = 0x7f0a0840

.field public static final section_mobile_hotspot_vzw:I = 0x7f0a083d

.field public static final section_motion:I = 0x7f0a013d

.field public static final section_motion_summary_basic:I = 0x7f0a013e

.field public static final section_multi_window:I = 0x7f0a013f

.field public static final section_multi_window_summary:I = 0x7f0a0140

.field public static final section_nfc:I = 0x7f0a016e

.field public static final section_nfc_summary:I = 0x7f0a016f

.field public static final section_notifications:I = 0x7f0a0152

.field public static final section_notifications_new:I = 0x7f0a0153

.field public static final section_notifications_summary:I = 0x7f0a0154

.field public static final section_notifications_summary2:I = 0x7f0a0155

.field public static final section_onehand:I = 0x7f0a0942

.field public static final section_onehand_summary:I = 0x7f0a0943

.field public static final section_phone:I = 0x7f0a017f

.field public static final section_phone_new_summary:I = 0x7f0a09c5

.field public static final section_phone_summary:I = 0x7f0a0180

.field public static final section_photo_readers:I = 0x7f0a0141

.field public static final section_photo_readers_summary:I = 0x7f0a0142

.field public static final section_pin_board:I = 0x7f0a07aa

.field public static final section_pin_board_summary:I = 0x7f0a07ab

.field public static final section_pin_board_summary_general:I = 0x7f0a07ac

.field public static final section_power_saving_mode_summary:I = 0x7f0a015b

.field public static final section_private_mode:I = 0x7f0a09f1

.field public static final section_private_mode_summary:I = 0x7f0a09f2

.field public static final section_quickconnect:I = 0x7f0a08aa

.field public static final section_quickconnect_items_1:I = 0x7f0a08ac

.field public static final section_quickconnect_items_2:I = 0x7f0a08ad

.field public static final section_quickconnect_summary:I = 0x7f0a08ab

.field public static final section_ringtone:I = 0x7f0a0178

.field public static final section_ringtone_summary:I = 0x7f0a0179

.field public static final section_s_health:I = 0x7f0a0143

.field public static final section_s_health_summary:I = 0x7f0a0144

.field public static final section_s_voice:I = 0x7f0a014b

.field public static final section_s_voice_summary:I = 0x7f0a014c

.field public static final section_safety_assistance:I = 0x7f0a08d1

.field public static final section_safety_assistance_summary:I = 0x7f0a08d2

.field public static final section_sbrowser:I = 0x7f0a018a

.field public static final section_sbrowser_summary:I = 0x7f0a018b

.field public static final section_sbrowser_summary_new:I = 0x7f0a0043

.field public static final section_scrapbook:I = 0x7f0a09f7

.field public static final section_scrapbook_summary:I = 0x7f0a09f8

.field public static final section_smart_clip:I = 0x7f0a07b1

.field public static final section_smart_clip_summary:I = 0x7f0a07b2

.field public static final section_smart_remote:I = 0x7f0a0145

.field public static final section_smart_remote_summary:I = 0x7f0a0146

.field public static final section_smart_screen:I = 0x7f0a0147

.field public static final section_smart_screen_summary:I = 0x7f0a0148

.field public static final section_smemo:I = 0x7f0a0188

.field public static final section_smemo_summary:I = 0x7f0a0189

.field public static final section_snote:I = 0x7f0a06ce

.field public static final section_snote_summary:I = 0x7f0a06cf

.field public static final section_spen_gesture:I = 0x7f0a0883

.field public static final section_spen_gesture_summary:I = 0x7f0a0884

.field public static final section_story_album:I = 0x7f0a0149

.field public static final section_story_album_summary:I = 0x7f0a014a

.field public static final section_tethering:I = 0x7f0a0838

.field public static final section_tethering_summary:I = 0x7f0a083e

.field public static final section_tethering_summary_2014_help:I = 0x7f0a083b

.field public static final section_tethering_title_2014_help:I = 0x7f0a0839

.field public static final section_translator:I = 0x7f0a0196

.field public static final section_translator_summary:I = 0x7f0a0197

.field public static final section_unlock:I = 0x7f0a0150

.field public static final section_unlock_summary:I = 0x7f0a0151

.field public static final section_unlock_summary_2014:I = 0x7f0a002e

.field public static final section_upgrade:I = 0x7f0a0181

.field public static final section_upgrade_summary:I = 0x7f0a0182

.field public static final section_upgrade_summary_2014:I = 0x7f0a09eb

.field public static final section_voice_control:I = 0x7f0a0194

.field public static final section_voice_control_summary:I = 0x7f0a0195

.field public static final section_wifi:I = 0x7f0a0170

.field public static final section_wifi_chn:I = 0x7f0a0171

.field public static final section_wifi_summary:I = 0x7f0a0172

.field public static final section_wifi_summary_on_device_help:I = 0x7f0a0173

.field public static final section_wifi_summary_on_device_help_chn:I = 0x7f0a0174

.field public static final section_writing_buddy:I = 0x7f0a07b7

.field public static final section_writing_buddy_summary:I = 0x7f0a07b8

.field public static final setting_dialog_air_browse:I = 0x7f0a0567

.field public static final setting_dialog_air_button:I = 0x7f0a078a

.field public static final setting_dialog_air_button_title:I = 0x7f0a0788

.field public static final setting_dialog_air_call_accept:I = 0x7f0a0569

.field public static final setting_dialog_air_gesture_title:I = 0x7f0a0564

.field public static final setting_dialog_air_jump:I = 0x7f0a0566

.field public static final setting_dialog_air_move:I = 0x7f0a0568

.field public static final setting_dialog_air_view_disabled:I = 0x7f0a0572

.field public static final setting_dialog_air_view_hover_zoom:I = 0x7f0a0575

.field public static final setting_dialog_air_view_information:I = 0x7f0a056e

.field public static final setting_dialog_air_view_magnification:I = 0x7f0a0574

.field public static final setting_dialog_air_view_magnifier:I = 0x7f0a00bc

.field public static final setting_dialog_air_view_progress_bar:I = 0x7f0a0570

.field public static final setting_dialog_air_view_speed_dial:I = 0x7f0a056f

.field public static final setting_dialog_air_view_talkback:I = 0x7f0a0573

.field public static final setting_dialog_air_view_title:I = 0x7f0a056a

.field public static final setting_dialog_air_view_try_information:I = 0x7f0a056b

.field public static final setting_dialog_air_view_try_progress:I = 0x7f0a056d

.field public static final setting_dialog_air_view_try_speed_dial:I = 0x7f0a056c

.field public static final setting_dialog_air_view_try_webpage_magnifier:I = 0x7f0a00bd

.field public static final setting_dialog_display_functional_menu:I = 0x7f0a0789

.field public static final setting_dialog_motion_title:I = 0x7f0a0562

.field public static final setting_dialog_pickup_to_direct_call_summary:I = 0x7f0a0563

.field public static final setting_dialog_quick_glance:I = 0x7f0a0565

.field public static final setting_dialog_show_status_bar:I = 0x7f0a0571

.field public static final setting_dialog_smart_pause:I = 0x7f0a057b

.field public static final setting_dialog_smart_rotation:I = 0x7f0a057a

.field public static final setting_dialog_smart_rotation_title:I = 0x7f0a0579

.field public static final setting_dialog_smart_screen_title:I = 0x7f0a0576

.field public static final setting_dialog_smart_scroll:I = 0x7f0a057d

.field public static final setting_dialog_smart_scroll_title:I = 0x7f0a057c

.field public static final setting_dialog_smart_stay:I = 0x7f0a0578

.field public static final setting_dialog_smart_stay_title:I = 0x7f0a0577

.field public static final settings_accessibility_using_assisatant_menu_text_1_2:I = 0x7f0a0919

.field public static final settings_accessibility_using_assisatant_menu_text_1_3:I = 0x7f0a091a

.field public static final settings_accessibility_using_assisatant_menu_text_1_4:I = 0x7f0a091b

.field public static final settings_accessibility_using_assisatant_menu_text_1_4_tts:I = 0x7f0a091c

.field public static final settings_accessibility_using_assisatant_menu_text_1_5:I = 0x7f0a091d

.field public static final settings_accessibility_using_assisatant_menu_text_1_5_tts:I = 0x7f0a091e

.field public static final settings_accessibility_using_assisatant_menu_text_1_6:I = 0x7f0a091f

.field public static final settings_accessibility_using_assisatant_menu_text_1_title:I = 0x7f0a0918

.field public static final settings_accessibility_using_assisatant_menu_text_2_6:I = 0x7f0a0922

.field public static final settings_accessibility_using_assisatant_menu_text_2_6_tts:I = 0x7f0a0923

.field public static final settings_accessibility_using_assisatant_menu_text_2_7:I = 0x7f0a0924

.field public static final settings_accessibility_using_assisatant_menu_text_2_title:I = 0x7f0a0921

.field public static final settings_accessibility_using_assisatant_menu_tip_1:I = 0x7f0a0920

.field public static final settings_accessibility_using_assisatant_menu_tip_2_1:I = 0x7f0a0925

.field public static final settings_accessibility_using_assisatant_menu_tip_2_1_tts:I = 0x7f0a0926

.field public static final settings_accessibility_using_assisatant_menu_tip_2_2:I = 0x7f0a0927

.field public static final settings_accessibility_using_assisatant_menu_tip_2_2_tts:I = 0x7f0a0928

.field public static final settings_accessibility_using_assisatant_menu_tip_2_3:I = 0x7f0a0929

.field public static final settings_accessibility_using_assisatant_menu_tip_2_3_tts:I = 0x7f0a092a

.field public static final settings_accessibility_using_assisatant_menu_tip_2_4:I = 0x7f0a092b

.field public static final settings_accessibility_using_assisatant_menu_tip_2_4_tts:I = 0x7f0a092c

.field public static final settings_accessibility_using_baby_crying_text_1_2:I = 0x7f0a090d

.field public static final settings_accessibility_using_baby_crying_text_1_3:I = 0x7f0a090e

.field public static final settings_accessibility_using_baby_crying_text_1_4:I = 0x7f0a090f

.field public static final settings_accessibility_using_baby_crying_text_1_5:I = 0x7f0a0910

.field public static final settings_accessibility_using_baby_crying_tip_1:I = 0x7f0a0911

.field public static final settings_accessibility_using_direct_access_text_1_2:I = 0x7f0a092d

.field public static final settings_accessibility_using_direct_access_text_1_3:I = 0x7f0a092e

.field public static final settings_accessibility_using_direct_access_text_1_4:I = 0x7f0a092f

.field public static final settings_accessibility_using_direct_access_text_1_5:I = 0x7f0a0930

.field public static final settings_accessibility_using_direct_access_tip_1:I = 0x7f0a0931

.field public static final settings_accessibility_using_sound_detectors_text_1_2:I = 0x7f0a0912

.field public static final settings_accessibility_using_sound_detectors_text_1_3:I = 0x7f0a0913

.field public static final settings_accessibility_using_sound_detectors_text_1_4:I = 0x7f0a0914

.field public static final settings_accessibility_using_sound_detectors_text_1_5:I = 0x7f0a0915

.field public static final settings_accessibility_using_sound_detectors_tip_1:I = 0x7f0a0916

.field public static final settings_accessibility_using_sound_detectors_tip_2:I = 0x7f0a0917

.field public static final settings_accessibility_using_talkback_text_1_1:I = 0x7f0a0903

.field public static final settings_accessibility_using_talkback_text_1_2:I = 0x7f0a0904

.field public static final settings_accessibility_using_talkback_text_1_3:I = 0x7f0a0905

.field public static final settings_accessibility_using_talkback_text_1_4:I = 0x7f0a0906

.field public static final settings_accessibility_using_talkback_text_1_4_tts:I = 0x7f0a0907

.field public static final settings_accessibility_using_talkback_text_1_title:I = 0x7f0a0902

.field public static final settings_accessibility_using_talkback_text_2_3:I = 0x7f0a090a

.field public static final settings_accessibility_using_talkback_text_2_4:I = 0x7f0a090b

.field public static final settings_accessibility_using_talkback_text_2_title:I = 0x7f0a0909

.field public static final settings_accessibility_using_talkback_tip_1:I = 0x7f0a0908

.field public static final settings_accessibility_using_talkback_tip_2:I = 0x7f0a090c

.field public static final settings_account_summary:I = 0x7f0a0101

.field public static final settings_account_title:I = 0x7f0a0100

.field public static final settings_bluetooth_bluetooth_connect_more_info:I = 0x7f0a00f8

.field public static final settings_bluetooth_bluetooth_connect_text1:I = 0x7f0a00f3

.field public static final settings_bluetooth_bluetooth_connect_text2:I = 0x7f0a00f4

.field public static final settings_bluetooth_bluetooth_connect_text2_tts:I = 0x7f0a00f5

.field public static final settings_bluetooth_bluetooth_connect_text3:I = 0x7f0a00f6

.field public static final settings_bluetooth_bluetooth_connect_text4:I = 0x7f0a00f7

.field public static final settings_bluetooth_bluetooth_connect_tip_description:I = 0x7f0a00f9

.field public static final settings_bluetooth_bluetooth_setup_text2_1:I = 0x7f0a00ee

.field public static final settings_bluetooth_bluetooth_setup_text2_2:I = 0x7f0a00ef

.field public static final settings_bluetooth_bluetooth_setup_text2_3:I = 0x7f0a00f0

.field public static final settings_bluetooth_bluetooth_setup_text2_4:I = 0x7f0a00f1

.field public static final settings_bluetooth_bluetooth_setup_text2_5:I = 0x7f0a00f2

.field public static final settings_power_saving_set_text:I = 0x7f0a0110

.field public static final settings_power_saving_set_text_settings_2013:I = 0x7f0a0836

.field public static final settings_ringtone_change_ringtone1:I = 0x7f0a00fb

.field public static final settings_ringtone_change_ringtone2:I = 0x7f0a00fc

.field public static final settings_ringtone_change_ringtone2_settings_2013:I = 0x7f0a0835

.field public static final settings_ringtone_change_ringtone3:I = 0x7f0a00fd

.field public static final settings_ringtone_change_ringtone4:I = 0x7f0a00fe

.field public static final settings_ringtone_change_ringtone5:I = 0x7f0a00ff

.field public static final settings_ringtone_change_ringtone_text:I = 0x7f0a00fa

.field public static final settings_using_download_booster_text_1_1:I = 0x7f0a0898

.field public static final settings_using_download_booster_text_1_1_2nd_depth:I = 0x7f0a0899

.field public static final settings_using_download_booster_text_1_2:I = 0x7f0a089a

.field public static final settings_using_download_booster_text_1_2_tts:I = 0x7f0a089b

.field public static final settings_using_download_booster_tip_1_1:I = 0x7f0a089c

.field public static final settings_using_download_booster_tip_1_2:I = 0x7f0a089d

.field public static final settings_using_download_booster_tip_1_2_chn:I = 0x7f0a089f

.field public static final settings_using_download_booster_tip_1_2_vzw_phone:I = 0x7f0a089e

.field public static final settings_using_download_booster_tip_1_3:I = 0x7f0a08a0

.field public static final settings_using_download_booster_tip_1_3_chn:I = 0x7f0a08a1

.field public static final settings_using_download_booster_tip_1_4:I = 0x7f0a08a2

.field public static final settings_using_download_booster_tip_1_4_chn:I = 0x7f0a08a3

.field public static final settings_wifi_connect_to_wifi_text_on_device_help_1:I = 0x7f0a00e6

.field public static final settings_wifi_connect_to_wifi_text_on_device_help_1_chn:I = 0x7f0a00e7

.field public static final settings_wifi_connect_to_wifi_text_on_device_help_2:I = 0x7f0a00e8

.field public static final settings_wifi_connect_to_wifi_text_on_device_help_2_chn:I = 0x7f0a00ea

.field public static final settings_wifi_connect_to_wifi_text_on_device_help_2_tts:I = 0x7f0a00e9

.field public static final settings_wifi_connect_to_wifi_text_on_device_help_3:I = 0x7f0a00eb

.field public static final sharing_a_picture_step1:I = 0x7f0a033a

.field public static final sharing_a_picture_step1_for_tts:I = 0x7f0a033e

.field public static final sharing_multiple_pictures_step1:I = 0x7f0a033b

.field public static final sharing_multiple_pictures_step2:I = 0x7f0a033c

.field public static final sharing_multiple_pictures_step3:I = 0x7f0a033d

.field public static final sharing_multiple_pictures_step3_for_tts:I = 0x7f0a033f

.field public static final show_more_results_expand:I = 0x7f0a001c

.field public static final show_more_results_fold:I = 0x7f0a001d

.field public static final smart_clip_help_item_pen_crop:I = 0x7f0a07b3

.field public static final smart_clip_help_item_pen_crop_text:I = 0x7f0a07b5

.field public static final smart_clip_help_item_reshape:I = 0x7f0a07b4

.field public static final smart_clip_help_item_reshape_text:I = 0x7f0a07b6

.field public static final smart_remote_introduction:I = 0x7f0a0417

.field public static final smart_remote_introduction_without_yosemite:I = 0x7f0a0418

.field public static final smart_screen_introduction:I = 0x7f0a0419

.field public static final smart_screen_may_not_work:I = 0x7f0a041a

.field public static final smart_screen_smart_pause_description:I = 0x7f0a0420

.field public static final smart_screen_smart_rotation_description:I = 0x7f0a041f

.field public static final smart_screen_smart_scroll_email:I = 0x7f0a0423

.field public static final smart_screen_smart_scroll_face_orientation:I = 0x7f0a042b

.field public static final smart_screen_smart_scroll_face_orientation_text:I = 0x7f0a042c

.field public static final smart_screen_smart_scroll_information_1:I = 0x7f0a0424

.field public static final smart_screen_smart_scroll_information_2:I = 0x7f0a0425

.field public static final smart_screen_smart_scroll_information_3:I = 0x7f0a0426

.field public static final smart_screen_smart_scroll_information_3_new:I = 0x7f0a0427

.field public static final smart_screen_smart_scroll_information_4:I = 0x7f0a0428

.field public static final smart_screen_smart_scroll_information_4_new:I = 0x7f0a0429

.field public static final smart_screen_smart_scroll_information_5:I = 0x7f0a042a

.field public static final smart_screen_smart_scroll_internet:I = 0x7f0a0422

.field public static final smart_screen_smart_scroll_supportedBy:I = 0x7f0a0421

.field public static final smart_screen_smart_scroll_tilting_device:I = 0x7f0a042d

.field public static final smart_screen_smart_scroll_tilting_device_text:I = 0x7f0a042e

.field public static final smart_screen_smart_stay_description:I = 0x7f0a041e

.field public static final smart_screen_when_fron_camera_is_used:I = 0x7f0a041d

.field public static final smart_screen_when_front_camera_fails:I = 0x7f0a041b

.field public static final smart_screen_when_source_of_light:I = 0x7f0a041c

.field public static final smart_scroll_use_popup_msg:I = 0x7f0a057f

.field public static final smart_scroll_use_popup_title:I = 0x7f0a057e

.field public static final smart_stay_use_popup_msg:I = 0x7f0a0581

.field public static final smart_stay_use_popup_title:I = 0x7f0a0580

.field public static final smemo_account_settings_start_snote:I = 0x7f0a04b9

.field public static final smemo_account_settings_synced_automatically:I = 0x7f0a04ba

.field public static final smemo_create_and_save_start_snote:I = 0x7f0a049e

.field public static final smemo_drawing_tools_select_snote:I = 0x7f0a04a1

.field public static final smemo_drawing_tools_set_detail_pen:I = 0x7f0a04a2

.field public static final smemo_erase_tools_tap_erase_type:I = 0x7f0a04a3

.field public static final smemo_export_start_snote:I = 0x7f0a04bc

.field public static final smemo_idea_sketch_add_object:I = 0x7f0a04a5

.field public static final smemo_idea_sketch_select_idea_sketch:I = 0x7f0a04a6

.field public static final smemo_idea_sketch_select_illustration:I = 0x7f0a04a9

.field public static final smemo_idea_sketch_select_language:I = 0x7f0a04a7

.field public static final smemo_idea_sketch_select_snote:I = 0x7f0a04a4

.field public static final smemo_idea_sketch_write_category_name:I = 0x7f0a04a8

.field public static final smemo_import_start_snote:I = 0x7f0a04bb

.field public static final smemo_manage_folders_pasue_record:I = 0x7f0a04b0

.field public static final smemo_manage_folders_record_sketch:I = 0x7f0a04af

.field public static final smemo_manage_folders_select_snote:I = 0x7f0a04ae

.field public static final smemo_manage_objects_tap_insert_icon:I = 0x7f0a049f

.field public static final smemo_manage_page_order_copy_imediately:I = 0x7f0a04b8

.field public static final smemo_manage_page_order_copy_pages:I = 0x7f0a04b5

.field public static final smemo_manage_page_order_rearrange_pages:I = 0x7f0a04b3

.field public static final smemo_manage_page_order_reorder_page:I = 0x7f0a04b4

.field public static final smemo_manage_page_order_select_done:I = 0x7f0a04b7

.field public static final smemo_manage_page_order_select_pages:I = 0x7f0a04b6

.field public static final smemo_manage_page_order_select_snote:I = 0x7f0a04b1

.field public static final smemo_manage_page_order_show_thumbnail:I = 0x7f0a04b2

.field public static final smemo_sketch_effect_see_option_list:I = 0x7f0a04ab

.field public static final smemo_sketch_effect_select_effect:I = 0x7f0a04ac

.field public static final smemo_sketch_effect_select_sketch_effect:I = 0x7f0a04ad

.field public static final smemo_sketch_effect_select_snote:I = 0x7f0a04aa

.field public static final smemo_zoom_and_pan_select_snote:I = 0x7f0a04a0

.field public static final snote_action_memo_0:I = 0x7f0a0732

.field public static final snote_action_memo_0_1:I = 0x7f0a0733

.field public static final snote_action_memo_1:I = 0x7f0a0734

.field public static final snote_action_memo_1_1:I = 0x7f0a0735

.field public static final snote_action_memo_2:I = 0x7f0a0736

.field public static final snote_action_memo_2_1:I = 0x7f0a0737

.field public static final snote_action_memo_2_1_tts:I = 0x7f0a073a

.field public static final snote_action_memo_3:I = 0x7f0a0738

.field public static final snote_action_memo_3_1:I = 0x7f0a0739

.field public static final snote_action_memo_3_1_tts:I = 0x7f0a073b

.field public static final snote_add_index_on_page_description:I = 0x7f0a070d

.field public static final snote_add_page_exist_note_description:I = 0x7f0a06ef

.field public static final snote_add_page_exist_note_description_tts:I = 0x7f0a06f0

.field public static final snote_add_pages_on_edit_description:I = 0x7f0a070c

.field public static final snote_add_template_edit_page_description:I = 0x7f0a06f3

.field public static final snote_change_modes_on_edit_description:I = 0x7f0a0706

.field public static final snote_create_new_notes_description:I = 0x7f0a06f6

.field public static final snote_create_new_notes_description_tts:I = 0x7f0a0743

.field public static final snote_edit_index_on_edit_description:I = 0x7f0a070e

.field public static final snote_edit_layout_description:I = 0x7f0a06f7

.field public static final snote_editing_handwriting_description_1:I = 0x7f0a0715

.field public static final snote_editing_handwriting_description_2:I = 0x7f0a0716

.field public static final snote_editing_handwriting_description_3:I = 0x7f0a0717

.field public static final snote_editing_handwriting_description_6:I = 0x7f0a0718

.field public static final snote_editing_handwriting_description_7:I = 0x7f0a0719

.field public static final snote_editing_handwriting_description_mr:I = 0x7f0a071a

.field public static final snote_editing_layout_description_1:I = 0x7f0a071f

.field public static final snote_editing_layout_description_2:I = 0x7f0a0720

.field public static final snote_editing_layout_description_3:I = 0x7f0a0721

.field public static final snote_editing_layout_description_4:I = 0x7f0a0722

.field public static final snote_editing_layout_description_5:I = 0x7f0a0723

.field public static final snote_editing_snote_5_1:I = 0x7f0a0727

.field public static final snote_editing_snote_5_1_tts:I = 0x7f0a073c

.field public static final snote_erase_handwriting_description:I = 0x7f0a06f8

.field public static final snote_erasing_handwriting_description_1:I = 0x7f0a071b

.field public static final snote_erasing_handwriting_description_2:I = 0x7f0a071c

.field public static final snote_erasing_handwriting_description_3:I = 0x7f0a071d

.field public static final snote_erasing_handwriting_description_4:I = 0x7f0a071e

.field public static final snote_go_to_the_library_description:I = 0x7f0a0705

.field public static final snote_how_to_set_lock_description_1:I = 0x7f0a0700

.field public static final snote_how_to_set_lock_description_2:I = 0x7f0a0701

.field public static final snote_how_to_set_lock_description_3:I = 0x7f0a0702

.field public static final snote_how_to_set_lock_description_4:I = 0x7f0a0703

.field public static final snote_how_to_set_lock_description_5:I = 0x7f0a0704

.field public static final snote_how_to_show_the_grid_on_page_description_1:I = 0x7f0a0710

.field public static final snote_how_to_show_the_grid_on_page_description_2:I = 0x7f0a0711

.field public static final snote_how_to_show_the_grid_on_page_description_3:I = 0x7f0a0712

.field public static final snote_how_to_show_the_grid_on_page_description_ps:I = 0x7f0a0713

.field public static final snote_magnified_note_1:I = 0x7f0a0744

.field public static final snote_magnified_note_2:I = 0x7f0a0745

.field public static final snote_magnified_note_3:I = 0x7f0a0746

.field public static final snote_magnified_note_4:I = 0x7f0a0747

.field public static final snote_magnified_note_5:I = 0x7f0a0748

.field public static final snote_panning_on_finger_mode_description:I = 0x7f0a0708

.field public static final snote_panning_on_pen_mode_description:I = 0x7f0a0709

.field public static final snote_pen_preset_1:I = 0x7f0a0728

.field public static final snote_pen_preset_1_1:I = 0x7f0a0729

.field public static final snote_pen_preset_1_1_tts:I = 0x7f0a073d

.field public static final snote_pen_preset_2:I = 0x7f0a072a

.field public static final snote_pen_preset_2_1:I = 0x7f0a072b

.field public static final snote_pen_preset_2_1_tts:I = 0x7f0a073e

.field public static final snote_picture_video_description:I = 0x7f0a06f5

.field public static final snote_resizing_toolbar_setting_on_edit_description:I = 0x7f0a070f

.field public static final snote_show_hide_the_toolbar_on_edit_description:I = 0x7f0a070a

.field public static final snote_show_hide_the_toolbar_on_edit_description_2:I = 0x7f0a070b

.field public static final snote_snap_note_1:I = 0x7f0a072c

.field public static final snote_snap_note_1_1:I = 0x7f0a072d

.field public static final snote_snap_note_1_1_tts:I = 0x7f0a073f

.field public static final snote_snap_note_1_2:I = 0x7f0a072e

.field public static final snote_snap_note_1_2_tts:I = 0x7f0a0740

.field public static final snote_snap_note_2:I = 0x7f0a072f

.field public static final snote_snap_note_2_1:I = 0x7f0a0730

.field public static final snote_snap_note_2_1_tts:I = 0x7f0a0741

.field public static final snote_snap_note_2_2:I = 0x7f0a0731

.field public static final snote_snap_note_2_2_tts:I = 0x7f0a0742

.field public static final snote_use_pen_setting_description:I = 0x7f0a06f4

.field public static final snote_use_selection_mode_description:I = 0x7f0a06f1

.field public static final snote_use_selection_mode_description_tts:I = 0x7f0a06f2

.field public static final snote_use_text_mode_description:I = 0x7f0a06f9

.field public static final snote_use_text_mode_description_tts:I = 0x7f0a06fa

.field public static final snote_use_the_grid_on_page_description:I = 0x7f0a0714

.field public static final snote_use_zoom_pad_description_1:I = 0x7f0a06fb

.field public static final snote_use_zoom_pad_description_2:I = 0x7f0a06fc

.field public static final snote_use_zoom_pad_description_3:I = 0x7f0a06fd

.field public static final snote_use_zoom_pad_description_4:I = 0x7f0a06fe

.field public static final snote_using_text_mode_description_1:I = 0x7f0a0724

.field public static final snote_using_text_mode_description_2:I = 0x7f0a0725

.field public static final snote_using_text_mode_description_3:I = 0x7f0a0726

.field public static final snote_view_page_preview_description:I = 0x7f0a06ff

.field public static final snote_zoom_in_on_edit_description:I = 0x7f0a0707

.field public static final softwareupdate_help_description_step_1_2014:I = 0x7f0a09ec

.field public static final softwareupdate_help_description_step_2_2014:I = 0x7f0a09ed

.field public static final softwareupdate_help_description_step_3_2014:I = 0x7f0a09ee

.field public static final softwareupdate_help_description_step_4_2014:I = 0x7f0a09f0

.field public static final softwareupdate_help_description_step_4_2014_chn:I = 0x7f0a09ef

.field public static final step_by_step_guide:I = 0x7f0a0120

.field public static final stms_appgroup:I = 0x7f0a0a0d

.field public static final story_album_combine_albums_combine_to_new:I = 0x7f0a03d2

.field public static final story_album_create_filter_album_create_with_search_option:I = 0x7f0a03d1

.field public static final story_album_create_gallery_create_album:I = 0x7f0a03d3

.field public static final story_album_edit_memo_tap_memo_area:I = 0x7f0a03da

.field public static final story_album_import_export_albums_export_albums:I = 0x7f0a03d6

.field public static final story_album_import_export_albums_export_albums_MyFiles:I = 0x7f0a03d7

.field public static final story_album_import_export_albums_import_albums:I = 0x7f0a03d8

.field public static final story_album_introduction_create_album:I = 0x7f0a03ca

.field public static final story_album_introduction_publish_digital_album:I = 0x7f0a03cb

.field public static final story_album_share_albums_share_album:I = 0x7f0a03d4

.field public static final story_album_share_albums_to_open:I = 0x7f0a03d5

.field public static final story_album_use_suggested_album_1:I = 0x7f0a03cc

.field public static final story_album_use_suggested_album_2:I = 0x7f0a03cd

.field public static final story_album_use_suggested_album_3:I = 0x7f0a03ce

.field public static final story_album_use_suggested_album_4:I = 0x7f0a03cf

.field public static final story_album_use_suggested_album_5:I = 0x7f0a03d0

.field public static final story_album_view_similiar_pic_only_one:I = 0x7f0a03d9

.field public static final svoice_access_svoice_access:I = 0x7f0a0434

.field public static final svoice_connect_vehicle_description:I = 0x7f0a0433

.field public static final svoice_connecting_to_vehicles_first:I = 0x7f0a0443

.field public static final svoice_driving_mode_tips_description:I = 0x7f0a0439

.field public static final svoice_edit_what_you_said_first:I = 0x7f0a0440

.field public static final svoice_edit_what_you_said_second_k:I = 0x7f0a01f7

.field public static final svoice_enable_driving_safety:I = 0x7f0a0435

.field public static final svoice_enable_driving_switch_svoice:I = 0x7f0a0436

.field public static final svoice_enable_hands_free_safety:I = 0x7f0a0441

.field public static final svoice_enable_hands_free_switch_svoice:I = 0x7f0a0442

.field public static final svoice_functions_available_in_driving_mode_description:I = 0x7f0a043a

.field public static final svoice_tips_for_using_svoice_hands_free_first:I = 0x7f0a0447

.field public static final svoice_using_svoice_expand_button_t_tts:I = 0x7f0a0a0b

.field public static final svoice_using_svoice_first_for_k:I = 0x7f0a0449

.field public static final svoice_using_svoice_first_for_t:I = 0x7f0a0a08

.field public static final svoice_using_svoice_hands_free_first:I = 0x7f0a0444

.field public static final svoice_using_svoice_hands_free_first_new:I = 0x7f0a0445

.field public static final svoice_using_svoice_hands_free_functions_first:I = 0x7f0a0446

.field public static final svoice_using_svoice_microphone_button_t_tts:I = 0x7f0a0a0c

.field public static final svoice_using_svoice_second_for_k:I = 0x7f0a044a

.field public static final svoice_using_svoice_second_for_t:I = 0x7f0a0a09

.field public static final svoice_using_svoice_third_for_k:I = 0x7f0a044b

.field public static final svoice_using_svoice_third_for_t:I = 0x7f0a0a0a

.field public static final svoice_using_svoice_tip_body_for_k:I = 0x7f0a044c

.field public static final svoice_voice_control_use_alarm_camera_music:I = 0x7f0a043c

.field public static final svoice_voice_control_use_alarm_camera_music_line_feed:I = 0x7f0a043f

.field public static final svoice_voice_control_use_voice:I = 0x7f0a043b

.field public static final svoice_voice_control_use_voice_call_alarm_camera_music:I = 0x7f0a043e

.field public static final svoice_voice_control_use_voice_call_chaton_alarm_camera_music:I = 0x7f0a043d

.field public static final svoice_wakeup_command_choose_word:I = 0x7f0a0437

.field public static final svoice_wakeup_command_multiple_wakeup:I = 0x7f0a0438

.field public static final svoice_wakeup_first:I = 0x7f0a0430

.field public static final svoice_wakeup_say_hear_beep:I = 0x7f0a0431

.field public static final svoice_wakeup_say_say_hi:I = 0x7f0a042f

.field public static final svoice_wakeup_second:I = 0x7f0a0432

.field public static final tab_more_option_button:I = 0x7f0a09ac

.field public static final tap_done:I = 0x7f0a09a9

.field public static final tap_edit:I = 0x7f0a09ab

.field public static final tap_image:I = 0x7f0a09aa

.field public static final tethering_help_item_bluetooth_tethering:I = 0x7f0a0844

.field public static final tethering_help_item_hotspot_vzw:I = 0x7f0a0842

.field public static final tethering_help_item_settings_up_mobile_hotspot:I = 0x7f0a0845

.field public static final tethering_help_item_usb_tethering:I = 0x7f0a0841

.field public static final tethering_help_item_usb_tethering_vzw:I = 0x7f0a0843

.field public static final tip:I = 0x7f0a03c6

.field public static final tipbox_title:I = 0x7f0a074c

.field public static final translator_search_phrases_select_category:I = 0x7f0a055d

.field public static final translator_supported_languages_summary1:I = 0x7f0a055e

.field public static final translator_supported_languages_summary2:I = 0x7f0a055f

.field public static final translator_translate_some_select_language:I = 0x7f0a055c

.field public static final try_multi_window_attention_check_try_it_popup_title:I = 0x7f0a00e4

.field public static final try_multi_window_enable_check_try_it_popup_msg:I = 0x7f0a00e5

.field public static final try_multi_window_view_close:I = 0x7f0a00e3

.field public static final try_multi_window_view_drag_and_drop:I = 0x7f0a00e1

.field public static final try_multi_window_view_fullscreen:I = 0x7f0a00e2

.field public static final try_multi_window_view_switch_application:I = 0x7f0a00df

.field public static final try_multi_window_view_switch_window:I = 0x7f0a00e0

.field public static final tutorial_expand_notification_panel_drag_down:I = 0x7f0a06cb

.field public static final tutorial_expand_notification_panel_drag_down_complete:I = 0x7f0a06cc

.field public static final tutorial_helptext_change_lang:I = 0x7f0a02a0

.field public static final tutorial_invaild_action:I = 0x7f0a06cd

.field public static final tutorial_notification_panel_drag_down:I = 0x7f0a06c7

.field public static final tutorial_notification_panel_drag_down_complete:I = 0x7f0a06c9

.field public static final tutorial_quick_panel_drag_down:I = 0x7f0a06c8

.field public static final tutorial_quick_panel_drag_down_complete:I = 0x7f0a06ca

.field public static final tutorial_title_change_lang:I = 0x7f0a029f

.field public static final unavailable_update:I = 0x7f0a0584

.field public static final unlock_add_widget_add_widget:I = 0x7f0a051c

.field public static final unlock_add_widget_swipe_top_area:I = 0x7f0a051b

.field public static final unlock_edit_lock_screen_change_order:I = 0x7f0a051d

.field public static final unlock_expand_widget_expand_widget:I = 0x7f0a051a

.field public static final unlock_lock_screen_navigation_navigate_lock:I = 0x7f0a0518

.field public static final unlock_lock_screen_navigation_navigate_lock_2:I = 0x7f0a0519

.field public static final unlock_missed_event:I = 0x7f0a051e

.field public static final unlock_set_screen_lock_checking_events_1:I = 0x7f0a0033

.field public static final unlock_set_screen_lock_checking_events_2:I = 0x7f0a0034

.field public static final unlock_set_screen_lock_open_camera:I = 0x7f0a0032

.field public static final unlock_set_screen_lock_open_setting:I = 0x7f0a0511

.field public static final unlock_set_screen_lock_open_settings:I = 0x7f0a002f

.field public static final unlock_set_screen_lock_select_lock_screen:I = 0x7f0a0513

.field public static final unlock_set_screen_lock_select_lock_type:I = 0x7f0a0516

.field public static final unlock_set_screen_lock_select_method:I = 0x7f0a0031

.field public static final unlock_set_screen_lock_select_screen_lock:I = 0x7f0a0515

.field public static final unlock_set_screen_lock_select_the_lock_screen:I = 0x7f0a0514

.field public static final unlock_set_screen_lock_tab_my_device:I = 0x7f0a0512

.field public static final unlock_set_screen_lock_tab_my_device_settings_2013:I = 0x7f0a0837

.field public static final unlock_set_screen_lock_tap_screenlock:I = 0x7f0a0030

.field public static final unlock_unlock_device_swipe_screen:I = 0x7f0a0517

.field public static final unlock_unlock_the_device:I = 0x7f0a0125

.field public static final upgrade_over_the_air_app_screen:I = 0x7f0a06a7

.field public static final upgrade_over_the_air_descripstion:I = 0x7f0a06a8

.field public static final upgrade_over_the_air_descripstion_att:I = 0x7f0a06aa

.field public static final upgrade_over_the_air_descripstion_h_att:I = 0x7f0a06ab

.field public static final upgrade_over_the_air_descripstion_new:I = 0x7f0a06a9

.field public static final upgrade_over_the_air_direct_upgrade:I = 0x7f0a06a6

.field public static final upgrade_samsung_kies_launch_kies:I = 0x7f0a06a3

.field public static final upgrade_samsung_kies_turn_off:I = 0x7f0a06a4

.field public static final upgrade_samsung_kies_while_upgrading:I = 0x7f0a06a5

.field public static final url_additional_video_en:I = 0x7f0a0589

.field public static final url_available_accessories_common:I = 0x7f0a06b2

.field public static final url_online_help_common:I = 0x7f0a06b1

.field public static final url_useful_tips_en:I = 0x7f0a058a

.field public static final url_useful_tips_es:I = 0x7f0a058b

.field public static final url_user_manual_en:I = 0x7f0a058c

.field public static final url_user_manual_es:I = 0x7f0a058d

.field public static final using_easy_mode_text_1_1:I = 0x7f0a09a4

.field public static final using_easy_mode_text_1_2:I = 0x7f0a09a5

.field public static final using_easy_mode_text_1_3:I = 0x7f0a09a6

.field public static final using_easy_mode_text_2_1:I = 0x7f0a09a7

.field public static final using_easy_mode_text_2_2:I = 0x7f0a09a8

.field public static final using_easy_mode_title_1:I = 0x7f0a09a2

.field public static final using_easy_mode_title_2:I = 0x7f0a09a3

.field public static final using_scrapbook_categorising_content:I = 0x7f0a0a01

.field public static final using_scrapbook_categorising_content_body:I = 0x7f0a0a02

.field public static final using_scrapbook_highlight_text:I = 0x7f0a0a03

.field public static final using_scrapbook_highlight_text_body:I = 0x7f0a0a04

.field public static final using_scrapbook_highlight_text_more_colour:I = 0x7f0a0a05

.field public static final using_scrapbook_highlight_text_more_eraser:I = 0x7f0a0a06

.field public static final using_scrapbook_highlight_text_more_highlight:I = 0x7f0a0a07

.field public static final video_air_call_accept_descripstion:I = 0x7f0a06ae

.field public static final video_air_gesture_progress_preview_descripstion:I = 0x7f0a06ac

.field public static final video_smart_pause_descripstion:I = 0x7f0a06ad

.field public static final video_sound_shot:I = 0x7f0a06af

.field public static final video_sound_shot_descripstion:I = 0x7f0a06b0

.field public static final view_more_text:I = 0x7f0a012a

.field public static final viewing_a_picture_step1:I = 0x7f0a030f

.field public static final viewing_a_picture_step2:I = 0x7f0a0310

.field public static final viewing_a_picture_tip1:I = 0x7f0a0311

.field public static final viewing_a_picture_tip1_for_tts:I = 0x7f0a0328

.field public static final viewing_a_picture_tip2:I = 0x7f0a0312

.field public static final viewing_album_step1:I = 0x7f0a032d

.field public static final viewing_album_step1_for_tts:I = 0x7f0a032f

.field public static final viewing_album_step2:I = 0x7f0a032e

.field public static final vzw_dialog_use_data_connection_title:I = 0x7f0a0588

.field public static final vzw_file_name_calling_and_messaging:I = 0x7f0a059a

.field public static final vzw_file_name_device_navigation:I = 0x7f0a0599

.field public static final vzw_file_name_email_setup:I = 0x7f0a059c

.field public static final vzw_file_name_my_verizon:I = 0x7f0a0598

.field public static final vzw_file_name_texting_with_verizon_messages:I = 0x7f0a059b

.field public static final vzw_file_name_wifi_and_bt:I = 0x7f0a059d

.field public static final vzw_file_name_wifi_enhanced:I = 0x7f0a059e

.field public static final welcome_text_summary:I = 0x7f0a0129

.field public static final wifi_tip_description:I = 0x7f0a00ec

.field public static final wifi_tip_description_chn:I = 0x7f0a00ed

.field public static final wlan_connection_chn_title:I = 0x7f0a08c6

.field public static final writing_buddy_expand_mode_description:I = 0x7f0a07c5

.field public static final writing_buddy_filling_in_forms_description:I = 0x7f0a07bc

.field public static final writing_buddy_writing_on_applications_description:I = 0x7f0a07bd


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
