.class public Lcom/samsung/helphub/search/ReconcileDBHelper;
.super Ljava/lang/Object;
.source "ReconcileDBHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/search/ReconcileDBHelper$1;,
        Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;,
        Lcom/samsung/helphub/search/ReconcileDBHelper$ReconcileAction;
    }
.end annotation


# instance fields
.field private mAction:Lcom/samsung/helphub/search/ReconcileDBHelper$ReconcileAction;

.field private mContext:Landroid/content/Context;

.field private mDatabase:Landroid/database/sqlite/SQLiteDatabase;

.field private mHidedCategoryHeaderId:[I

.field private mHidedSectionHeaderId:[I

.field private mHidedSectionItemId:[I


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V
    .locals 1
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-object v0, Lcom/samsung/helphub/search/ReconcileDBHelper$ReconcileAction;->FILL_DATABASE:Lcom/samsung/helphub/search/ReconcileDBHelper$ReconcileAction;

    iput-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mAction:Lcom/samsung/helphub/search/ReconcileDBHelper$ReconcileAction;

    .line 41
    iput-object p1, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    .line 42
    iput-object p2, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mContext:Landroid/content/Context;

    .line 43
    return-void
.end method

.method private addHeaders(Lcom/samsung/helphub/headers/HelpHeader;ILandroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Landroid/content/Context;)I
    .locals 12
    .param p1, "owner"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p2, "xmlId"    # I
    .param p3, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p5, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            "I",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;",
            ">;",
            "Landroid/content/Context;",
            ")I"
        }
    .end annotation

    .prologue
    .line 150
    .local p4, "phraseData":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    const/4 v10, 0x0

    .line 151
    .local v10, "result":I
    move-object/from16 v0, p5

    invoke-static {p2, v0}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->getSubHeaders(ILandroid/content/Context;)Ljava/util/List;

    move-result-object v8

    .line 152
    .local v8, "headers":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    .line 154
    .local v2, "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-direct {p0, v2}, Lcom/samsung/helphub/search/ReconcileDBHelper;->isHiddenHeader(Lcom/samsung/helphub/headers/HelpHeader;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    .line 155
    invoke-direct/range {v1 .. v6}, Lcom/samsung/helphub/search/ReconcileDBHelper;->addHelpHeader(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Landroid/content/Context;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v2

    .line 156
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getSubHeadersArrayId()I

    move-result v3

    .line 157
    .local v3, "subHeaders":I
    if-lez v3, :cond_1

    move-object v1, p0

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    .line 158
    invoke-direct/range {v1 .. v6}, Lcom/samsung/helphub/search/ReconcileDBHelper;->addHeaders(Lcom/samsung/helphub/headers/HelpHeader;ILandroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Landroid/content/Context;)I

    move-result v7

    .line 159
    .local v7, "count":I
    new-instance v11, Landroid/content/ContentValues;

    const/4 v1, 0x1

    invoke-direct {v11, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 160
    .local v11, "values":Landroid/content/ContentValues;
    const-string v1, "items_count"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v11, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 161
    const-string v1, "header"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "header.rowid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p3, v1, v11, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 165
    .end local v7    # "count":I
    .end local v11    # "values":Landroid/content/ContentValues;
    :cond_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 168
    .end local v2    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v3    # "subHeaders":I
    :cond_2
    return v10
.end method

.method private addHelpHeader(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Landroid/content/Context;)Lcom/samsung/helphub/headers/HelpHeader;
    .locals 10
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p2, "owner"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p3, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p5, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;",
            ">;",
            "Landroid/content/Context;",
            ")",
            "Lcom/samsung/helphub/headers/HelpHeader;"
        }
    .end annotation

    .prologue
    .line 202
    .local p4, "data":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    new-instance v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;

    const/4 v6, 0x0

    invoke-direct {v1, v6}, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;-><init>(Lcom/samsung/helphub/search/ReconcileDBHelper$1;)V

    .line 203
    .local v1, "itemData":Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;
    const-string v6, "INSERT INTO header VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"

    invoke-virtual {p3, v6}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v4

    .line 205
    .local v4, "statment":Landroid/database/sqlite/SQLiteStatement;
    const/4 v6, 0x1

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 206
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 207
    const/4 v6, 0x2

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v7

    invoke-virtual {p5, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 212
    :goto_0
    const/4 v6, 0x3

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 213
    const/4 v6, 0x4

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getTitlePs1Id()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 214
    const/4 v6, 0x5

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getTitlePs2Id()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 216
    const/4 v6, 0x6

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryId()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 217
    const/4 v6, 0x7

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryPs1Id()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 218
    const/16 v6, 0x8

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryPs2Id()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 220
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_3

    .line 221
    const/16 v6, 0x9

    invoke-virtual {v4, v6}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 226
    :goto_1
    const/16 v6, 0xa

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getItemLayoutID()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 228
    const/16 v6, 0xb

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 230
    const/16 v6, 0xc

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getSubHeadersArrayId()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 232
    const/16 v6, 0xd

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getIconId()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 234
    if-nez p2, :cond_4

    .line 235
    const/16 v6, 0xe

    const/4 v7, -0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 240
    :goto_2
    const/16 v6, 0xf

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getDictionaryId()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 242
    const/16 v6, 0x10

    const-wide/16 v8, 0x0

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 244
    if-nez p2, :cond_6

    .line 245
    const/16 v8, 0x11

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getIsPluginRes()Z

    move-result v6

    if-eqz v6, :cond_5

    const-wide/16 v6, 0x1

    :goto_3
    invoke-virtual {v4, v8, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 255
    :goto_4
    if-nez p2, :cond_a

    .line 256
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getPluginAppId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 257
    const/16 v6, 0x12

    invoke-virtual {v4, v6}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 273
    :goto_5
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v2

    .line 275
    .local v2, "headerRowID":J
    iput-wide v2, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->HeaderRowId:J

    .line 276
    const/4 v6, 0x1

    iput-boolean v6, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->IsPredefined:Z

    .line 277
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v6

    iput v6, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhraseStringId:I

    .line 278
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getTitlePs1Id()I

    move-result v6

    iput v6, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhrasePs1Id:I

    .line 279
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getTitlePs2Id()I

    move-result v6

    iput v6, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhrasePs2Id:I

    .line 280
    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getDictionaryId()I

    move-result v0

    .line 283
    .local v0, "dictionaryID":I
    const/4 v6, -0x1

    if-eq v0, v6, :cond_0

    .line 284
    new-instance v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;

    .end local v1    # "itemData":Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;
    const/4 v6, 0x0

    invoke-direct {v1, v6}, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;-><init>(Lcom/samsung/helphub/search/ReconcileDBHelper$1;)V

    .line 285
    .restart local v1    # "itemData":Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;
    iput-wide v2, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->HeaderRowId:J

    .line 286
    const/4 v6, 0x1

    iput-boolean v6, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->IsPredefined:Z

    .line 287
    iput v0, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhraseStringId:I

    .line 288
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getTitlePs1Id()I

    move-result v6

    iput v6, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhrasePs1Id:I

    .line 289
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getTitlePs2Id()I

    move-result v6

    iput v6, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhrasePs2Id:I

    .line 290
    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryId()I

    move-result v5

    .line 293
    .local v5, "summary":I
    if-lez v5, :cond_1

    .line 294
    new-instance v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;

    .end local v1    # "itemData":Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;
    const/4 v6, 0x0

    invoke-direct {v1, v6}, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;-><init>(Lcom/samsung/helphub/search/ReconcileDBHelper$1;)V

    .line 295
    .restart local v1    # "itemData":Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;
    iput-wide v2, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->HeaderRowId:J

    .line 296
    const/4 v6, 0x1

    iput-boolean v6, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->IsPredefined:Z

    .line 297
    iput v5, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhraseStringId:I

    .line 298
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryPs1Id()I

    move-result v6

    iput v6, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhrasePs1Id:I

    .line 299
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryPs2Id()I

    move-result v6

    iput v6, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhrasePs2Id:I

    .line 300
    invoke-interface {p4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 302
    :cond_1
    long-to-int v6, v2

    invoke-virtual {p1, v6}, Lcom/samsung/helphub/headers/HelpHeader;->setRecordId(I)V

    .line 303
    return-object p1

    .line 209
    .end local v0    # "dictionaryID":I
    .end local v2    # "headerRowID":J
    .end local v5    # "summary":I
    :cond_2
    const/4 v6, 0x2

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 223
    :cond_3
    const/16 v6, 0x9

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto/16 :goto_1

    .line 237
    :cond_4
    const/16 v6, 0xe

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto/16 :goto_2

    .line 245
    :cond_5
    const-wide/16 v6, 0x0

    goto/16 :goto_3

    .line 247
    :cond_6
    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getIsPluginRes()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 248
    const/16 v6, 0x11

    const-wide/16 v8, 0x1

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 249
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, Lcom/samsung/helphub/headers/HelpHeader;->setIsPluginRes(Z)V

    goto/16 :goto_4

    .line 251
    :cond_7
    const/16 v8, 0x11

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getIsPluginRes()Z

    move-result v6

    if-eqz v6, :cond_8

    const-wide/16 v6, 0x1

    :goto_6
    invoke-virtual {v4, v8, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    goto/16 :goto_4

    :cond_8
    const-wide/16 v6, 0x0

    goto :goto_6

    .line 259
    :cond_9
    const/16 v6, 0x12

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getPluginAppId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 262
    :cond_a
    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getPluginAppId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 263
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getPluginAppId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 264
    const/16 v6, 0x12

    invoke-virtual {v4, v6}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto/16 :goto_5

    .line 266
    :cond_b
    const/16 v6, 0x12

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getPluginAppId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto/16 :goto_5

    .line 269
    :cond_c
    const/16 v6, 0x12

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getPluginAppId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 270
    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getPluginAppId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Lcom/samsung/helphub/headers/HelpHeader;->setPluginAppId(Ljava/lang/String;)V

    goto/16 :goto_5
.end method

.method private addPhrasesForCurrentLocale(Ljava/util/List;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V
    .locals 9
    .param p2, "locale"    # Ljava/lang/String;
    .param p3, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p4, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 309
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x5

    if-ne v5, v6, :cond_2

    .line 310
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 311
    .local v3, "resources":Landroid/content/res/Resources;
    new-instance v4, Landroid/content/ContentValues;

    const/4 v5, 0x4

    invoke-direct {v4, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 313
    .local v4, "values":Landroid/content/ContentValues;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;

    .line 314
    .local v1, "phrase":Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;
    const-string v5, "header_id"

    iget-wide v6, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->HeaderRowId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 318
    iget v5, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhraseStringId:I

    iget v6, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhrasePs1Id:I

    iget v7, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhrasePs2Id:I

    invoke-direct {p0, p4, v5, v6, v7}, Lcom/samsung/helphub/search/ReconcileDBHelper;->makePSText(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v2

    .line 320
    .local v2, "phraseString":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 321
    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    goto :goto_0

    .line 324
    :cond_0
    const-string v5, "phrase"

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const-string v5, "phrase_hex"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/helphub/search/NameNormalizer;->lettersAndDigitsOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "(?i)s beam"

    const-string v8, "s beam sbeam"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "(?i)s pen"

    const-string v8, "s pen spen"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "(?i)s memo"

    const-string v8, "s memo smemo"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "(?i)s view"

    const-string v8, "s view sview"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "(?i)s note"

    const-string v8, "s note snote"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/helphub/search/NameNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const-string v6, "is_predefined"

    iget-boolean v5, v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->IsPredefined:Z

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    :goto_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 334
    const-string v5, "language"

    invoke-virtual {v4, v5, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    const-string v5, "search_phrase"

    const/4 v6, 0x0

    invoke-virtual {p3, v5, v6, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 337
    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    goto/16 :goto_0

    .line 333
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 340
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "phrase":Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;
    .end local v2    # "phraseString":Ljava/lang/String;
    .end local v3    # "resources":Landroid/content/res/Resources;
    .end local v4    # "values":Landroid/content/ContentValues;
    :cond_2
    return-void
.end method

.method private getPhraseData(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/List;
    .locals 14
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 384
    const/16 v0, 0x8

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "header.rowid"

    aput-object v0, v2, v1

    const-string v0, "title"

    aput-object v0, v2, v3

    const-string v0, "dictionary"

    aput-object v0, v2, v4

    const-string v0, "summary"

    aput-object v0, v2, v5

    const/4 v0, 0x4

    const-string v1, "titleps1"

    aput-object v1, v2, v0

    const/4 v0, 0x5

    const-string v1, "titleps2"

    aput-object v1, v2, v0

    const/4 v0, 0x6

    const-string v1, "summaryps1"

    aput-object v1, v2, v0

    const/4 v0, 0x7

    const-string v1, "summaryps1"

    aput-object v1, v2, v0

    .line 391
    .local v2, "COLUMNS":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 392
    .local v11, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    const/4 v8, 0x0

    .line 394
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v1, "header"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 395
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 396
    new-instance v12, Ljava/util/ArrayList;

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {v12, v0}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 399
    .end local v11    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    .local v12, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    :cond_0
    :try_start_1
    new-instance v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;

    const/4 v0, 0x0

    invoke-direct {v10, v0}, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;-><init>(Lcom/samsung/helphub/search/ReconcileDBHelper$1;)V

    .line 400
    .local v10, "item":Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->HeaderRowId:J

    .line 401
    const/4 v0, 0x1

    iput-boolean v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->IsPredefined:Z

    .line 402
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhraseStringId:I

    .line 403
    const/4 v0, 0x4

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhrasePs1Id:I

    .line 404
    const/4 v0, 0x5

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhrasePs2Id:I

    .line 405
    invoke-interface {v12, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 406
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 407
    .local v9, "dictionary":I
    if-lez v9, :cond_1

    .line 408
    new-instance v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;

    .end local v10    # "item":Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;
    const/4 v0, 0x0

    invoke-direct {v10, v0}, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;-><init>(Lcom/samsung/helphub/search/ReconcileDBHelper$1;)V

    .line 409
    .restart local v10    # "item":Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->HeaderRowId:J

    .line 410
    const/4 v0, 0x0

    iput-boolean v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->IsPredefined:Z

    .line 411
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhraseStringId:I

    .line 412
    const/4 v0, 0x4

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhrasePs1Id:I

    .line 413
    const/4 v0, 0x5

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhrasePs2Id:I

    .line 414
    invoke-interface {v12, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416
    :cond_1
    const/4 v0, 0x3

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 417
    .local v13, "summary":I
    if-lez v13, :cond_2

    .line 418
    new-instance v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;

    .end local v10    # "item":Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;
    const/4 v0, 0x0

    invoke-direct {v10, v0}, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;-><init>(Lcom/samsung/helphub/search/ReconcileDBHelper$1;)V

    .line 419
    .restart local v10    # "item":Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->HeaderRowId:J

    .line 420
    const/4 v0, 0x1

    iput-boolean v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->IsPredefined:Z

    .line 421
    const/4 v0, 0x3

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhraseStringId:I

    .line 422
    const/4 v0, 0x6

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhrasePs1Id:I

    .line 423
    const/4 v0, 0x7

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v10, Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;->PhrasePs2Id:I

    .line 424
    invoke-interface {v12, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 426
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    move-object v11, v12

    .line 429
    .end local v9    # "dictionary":I
    .end local v10    # "item":Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;
    .end local v12    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    .end local v13    # "summary":I
    .restart local v11    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    :cond_3
    if-eqz v8, :cond_4

    .line 430
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 433
    :cond_4
    return-object v11

    .line 429
    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v8, :cond_5

    .line 430
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 429
    .end local v11    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    .restart local v12    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    :catchall_1
    move-exception v0

    move-object v11, v12

    .end local v12    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    .restart local v11    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    goto :goto_0
.end method

.method private handleAddLanguage()V
    .locals 4

    .prologue
    .line 125
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "locale":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, v0, v2}, Lcom/samsung/helphub/search/ReconcileDBHelper;->isCurrentLocalSupported(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 127
    iget-object v2, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, v2}, Lcom/samsung/helphub/search/ReconcileDBHelper;->getPhraseData(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/List;

    move-result-object v1

    .line 128
    .local v1, "phraseData":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    if-eqz v1, :cond_0

    .line 129
    iget-object v2, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v3, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/samsung/helphub/search/ReconcileDBHelper;->addPhrasesForCurrentLocale(Ljava/util/List;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    .line 132
    .end local v1    # "phraseData":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    :cond_0
    return-void
.end method

.method private handleFillDatabase()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 110
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->getHiddenCategoryHeaderId(Landroid/content/Context;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mHidedCategoryHeaderId:[I

    .line 111
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->getHiddenSectionHeaderId(Landroid/content/Context;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mHidedSectionHeaderId:[I

    .line 112
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->getHiddenItemHeaderId(Landroid/content/Context;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mHidedSectionItemId:[I

    .line 113
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    .line 114
    .local v6, "locale":Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 115
    .local v4, "phraseData":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/search/ReconcileDBHelper$ItemPhraseData;>;"
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "easy_mode_switch"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 117
    const v2, 0x7f060001

    iget-object v3, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v5, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mContext:Landroid/content/Context;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/helphub/search/ReconcileDBHelper;->addHeaders(Lcom/samsung/helphub/headers/HelpHeader;ILandroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Landroid/content/Context;)I

    .line 121
    :goto_0
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v1, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v6, v0, v1}, Lcom/samsung/helphub/search/ReconcileDBHelper;->addPhrasesForCurrentLocale(Ljava/util/List;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    .line 122
    return-void

    .line 119
    :cond_0
    const/high16 v2, 0x7f060000

    iget-object v3, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v5, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mContext:Landroid/content/Context;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/helphub/search/ReconcileDBHelper;->addHeaders(Lcom/samsung/helphub/headers/HelpHeader;ILandroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Landroid/content/Context;)I

    goto :goto_0
.end method

.method private handleReconcileDatabase()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DELETE FROM header;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DELETE FROM search_phrase;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 137
    invoke-direct {p0}, Lcom/samsung/helphub/search/ReconcileDBHelper;->handleFillDatabase()V

    .line 138
    return-void
.end method

.method private isCurrentLocalSupported(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 12
    .param p1, "locale"    # Ljava/lang/String;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 358
    const/4 v9, 0x0

    .line 359
    .local v9, "result":Z
    const/4 v8, 0x0

    .line 361
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v1, "search_phrase"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "count(language)"

    aput-object v3, v2, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "language=\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 365
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_2

    move v9, v10

    .line 369
    :cond_0
    :goto_0
    if-eqz v8, :cond_1

    .line 370
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 373
    :cond_1
    return v9

    :cond_2
    move v9, v11

    .line 366
    goto :goto_0

    .line 369
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    .line 370
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private isHiddenHeader(Lcom/samsung/helphub/headers/HelpHeader;)Z
    .locals 7
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 172
    const/4 v4, 0x0

    .line 173
    .local v4, "result":Z
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v5

    sget-object v6, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v5, v6, :cond_2

    .line 174
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mHidedCategoryHeaderId:[I

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget v2, v0, v1

    .line 175
    .local v2, "item":I
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v5

    if-ne v5, v2, :cond_1

    .line 176
    const/4 v4, 0x1

    .line 197
    .end local v0    # "arr$":[I
    .end local v1    # "i$":I
    .end local v2    # "item":I
    .end local v3    # "len$":I
    :cond_0
    :goto_1
    return v4

    .line 174
    .restart local v0    # "arr$":[I
    .restart local v1    # "i$":I
    .restart local v2    # "item":I
    .restart local v3    # "len$":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 180
    .end local v0    # "arr$":[I
    .end local v1    # "i$":I
    .end local v2    # "item":I
    .end local v3    # "len$":I
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v5

    sget-object v6, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v5, v6, :cond_4

    .line 181
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mHidedSectionHeaderId:[I

    .restart local v0    # "arr$":[I
    array-length v3, v0

    .restart local v3    # "len$":I
    const/4 v1, 0x0

    .restart local v1    # "i$":I
    :goto_2
    if-ge v1, v3, :cond_0

    aget v2, v0, v1

    .line 182
    .restart local v2    # "item":I
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v5

    if-ne v5, v2, :cond_3

    .line 183
    const/4 v4, 0x1

    .line 184
    goto :goto_1

    .line 181
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 188
    .end local v0    # "arr$":[I
    .end local v1    # "i$":I
    .end local v2    # "item":I
    .end local v3    # "len$":I
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v5

    sget-object v6, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v5, v6, :cond_0

    .line 189
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mHidedSectionItemId:[I

    .restart local v0    # "arr$":[I
    array-length v3, v0

    .restart local v3    # "len$":I
    const/4 v1, 0x0

    .restart local v1    # "i$":I
    :goto_3
    if-ge v1, v3, :cond_0

    aget v2, v0, v1

    .line 190
    .restart local v2    # "item":I
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v5

    if-ne v5, v2, :cond_5

    .line 191
    const/4 v4, 0x1

    .line 192
    goto :goto_1

    .line 189
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private makePSText(Landroid/content/Context;III)Ljava/lang/String;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textID"    # I
    .param p3, "TextPs1Id"    # I
    .param p4, "TextPs2Id"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 437
    const/4 v0, 0x0

    .line 440
    .local v0, "TextPsCount":I
    if-eq p3, v2, :cond_0

    if-eqz p3, :cond_0

    .line 441
    add-int/lit8 v0, v0, 0x1

    .line 442
    :cond_0
    if-eq p4, v2, :cond_1

    if-eqz p4, :cond_1

    .line 443
    add-int/lit8 v0, v0, 0x1

    .line 445
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 453
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 456
    .local v1, "result":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 447
    .end local v1    # "result":Ljava/lang/String;
    :pswitch_0
    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 448
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0

    .line 450
    .end local v1    # "result":Ljava/lang/String;
    :pswitch_1
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 451
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0

    .line 445
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public addLanguage()V
    .locals 2

    .prologue
    .line 64
    sget-object v1, Lcom/samsung/helphub/search/ReconcileDBHelper$ReconcileAction;->ADD_LANGUAGE:Lcom/samsung/helphub/search/ReconcileDBHelper$ReconcileAction;

    iput-object v1, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mAction:Lcom/samsung/helphub/search/ReconcileDBHelper$ReconcileAction;

    .line 65
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 66
    .local v0, "thread":Ljava/lang/Thread;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 67
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 68
    return-void
.end method

.method public fillDB()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 50
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/helphub/search/ReconcileDBHelper;->handleFillDatabase()V

    .line 51
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 61
    return-void

    .line 53
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public reconcileDatabase()V
    .locals 4

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "header"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 74
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "search_phrase"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 76
    invoke-virtual {p0}, Lcom/samsung/helphub/search/ReconcileDBHelper;->fillDB()V

    .line 77
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 81
    return-void

    .line 79
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 87
    :try_start_0
    sget-object v0, Lcom/samsung/helphub/search/ReconcileDBHelper$1;->$SwitchMap$com$samsung$helphub$search$ReconcileDBHelper$ReconcileAction:[I

    iget-object v1, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mAction:Lcom/samsung/helphub/search/ReconcileDBHelper$ReconcileAction;

    invoke-virtual {v1}, Lcom/samsung/helphub/search/ReconcileDBHelper$ReconcileAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 103
    :goto_0
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    iget-object v0, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 107
    return-void

    .line 89
    :pswitch_0
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/helphub/search/ReconcileDBHelper;->handleFillDatabase()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 105
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/helphub/search/ReconcileDBHelper;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 93
    :pswitch_1
    :try_start_2
    invoke-direct {p0}, Lcom/samsung/helphub/search/ReconcileDBHelper;->handleAddLanguage()V

    goto :goto_0

    .line 97
    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/helphub/search/ReconcileDBHelper;->handleReconcileDatabase()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
