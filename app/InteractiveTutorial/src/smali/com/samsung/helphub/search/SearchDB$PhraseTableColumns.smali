.class public interface abstract Lcom/samsung/helphub/search/SearchDB$PhraseTableColumns;
.super Ljava/lang/Object;
.source "SearchDB.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/search/SearchDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PhraseTableColumns"
.end annotation


# static fields
.field public static final HEADER_ID:Ljava/lang/String; = "header_id"

.field public static final IS_PREDEFINED:Ljava/lang/String; = "is_predefined"

.field public static final LANGUAGE:Ljava/lang/String; = "language"

.field public static final PHRASE:Ljava/lang/String; = "phrase"

.field public static final PHRASE_HEX:Ljava/lang/String; = "phrase_hex"
