.class public Lcom/samsung/helphub/search/SearchProvider;
.super Landroid/content/ContentProvider;
.source "SearchProvider.java"


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.helphub.provider.search"

.field private static final MATCHER_FINDO_SEARCH_SUGGEST:I = 0xe

.field private static final MATCHER_HEADER:I = 0x2

.field private static final MATCHER_HEADER_BY_ID:I = 0x3

.field private static final MATCHER_HEADER_BY_OWNER:I = 0xc

.field private static final MATCHER_HEADER_CATEGORY:I = 0x4

.field private static final MATCHER_HEADER_CATEGORY_BY_NAME:I = 0x5

.field private static final MATCHER_HEADER_ITEM:I = 0xd

.field private static final MATCHER_HEADER_ITEM_BY_NAME:I = 0x8

.field private static final MATCHER_HEADER_SEARCH:I = 0x9

.field private static final MATCHER_HEADER_SECTION:I = 0x6

.field private static final MATCHER_HEADER_SECTION_BY_NAME:I = 0x7

.field private static final MATCHER_SUGGESTION:I = 0x1

.field private static final MATCHER_UPDATE_LANGUAGE:I = 0xb

.field private static final MATCHER_UPDATE_RECONCILE:I = 0xa

.field public static final PROVIDER_URI:Ljava/lang/String; = "content://com.samsung.helphub.provider.search"

.field private static final SUGGESTIONS_CATEGORY_PROJECTION:[Ljava/lang/String;

.field private static final SUGGESTIONS_CATEGORY_SUMMARY_PROJECTION:[Ljava/lang/String;

.field private static final SUGGESTIONS_CATEGORY_TABLE:Ljava/lang/String; = "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase MATCH ? AND dict.header_id=item.rowid)"

.field private static final SUGGESTIONS_CATEGORY_TABLE_ALL:Ljava/lang/String; = "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.header_id=item.rowid)"

.field private static final SUGGESTIONS_CATEGORY_TABLE_BY_PHRASE_HEX:Ljava/lang/String; = "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase_hex MATCH ? AND dict.header_id=item.rowid)"

.field private static final SUGGESTIONS_CATEGORY_TABLE_LIKE:Ljava/lang/String; = "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase LIKE ? AND dict.header_id=item.rowid)"

.field private static final SUGGESTIONS_CURSOR_COLUMNS:[Ljava/lang/String;

.field private static final SUGGESTIONS_CURSOR_GS_COLUMNS:[Ljava/lang/String;

.field private static final SUGGESTIONS_ITEMS_PROJECTION:[Ljava/lang/String;

.field private static final SUGGESTIONS_ITEMS_SUMMARY_PROJECTION:[Ljava/lang/String;

.field private static final SUGGESTIONS_ITEMS_TABLE:Ljava/lang/String; = "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase MATCH ? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid INNER JOIN header AS category ON sect.owner_id=category.rowid"

.field private static final SUGGESTIONS_ITEMS_TABLE_ALL:Ljava/lang/String; = "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid INNER JOIN header AS category ON sect.owner_id=category.rowid"

.field private static final SUGGESTIONS_ITEMS_TABLE_BY_PHRASE_HEX:Ljava/lang/String; = "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase_hex MATCH ? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid INNER JOIN header AS category ON sect.owner_id=category.rowid"

.field private static final SUGGESTIONS_ITEMS_TABLE_LIKE:Ljava/lang/String; = "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase LIKE ? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid INNER JOIN header AS category ON sect.owner_id=category.rowid"

.field private static final SUGGESTIONS_SECTIONS_PROJECTION:[Ljava/lang/String;

.field private static final SUGGESTIONS_SECTIONS_SUMMARY_PROJECTION:[Ljava/lang/String;

.field private static final SUGGESTIONS_SECTIONS_TABLE:Ljava/lang/String; = "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase MATCH ? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid"

.field private static final SUGGESTIONS_SECTIONS_TABLE_ALL:Ljava/lang/String; = "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid"

.field private static final SUGGESTIONS_SECTIONS_TABLE_BY_PHRASE_HEX:Ljava/lang/String; = "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase_hex MATCH ? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid"

.field private static final SUGGESTIONS_SECTIONS_TABLE_LIKE:Ljava/lang/String; = "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase LIKE ? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid"

.field private static final SUGGESTION_REQUEST_PROJECTION:[Ljava/lang/String;

.field private static final SUGGESTION_REQUEST_SELECTION:Ljava/lang/String; = "name  MATCH ?"

.field private static final SUGGEST_URI_PATH_REGEX_QUERY:Ljava/lang/String; = "search_suggest_regex_query"

.field private static final sUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mDatabase:Landroid/database/sqlite/SQLiteDatabase;

.field private mDatabaseHelper:Lcom/samsung/helphub/search/SearchDBHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32
    invoke-static {}, Lcom/samsung/helphub/search/SearchProvider;->buildUriMatcher()Landroid/content/UriMatcher;

    move-result-object v0

    sput-object v0, Lcom/samsung/helphub/search/SearchProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 153
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "name as suggest_text_1"

    aput-object v1, v0, v4

    const-string v1, "name as suggest_text_2"

    aput-object v1, v0, v5

    const-string v1, "suggest_intent_data_id"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTION_REQUEST_PROJECTION:[Ljava/lang/String;

    .line 366
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "suggest_text_1"

    aput-object v1, v0, v4

    const-string v1, "suggest_text_2"

    aput-object v1, v0, v5

    const-string v1, "suggest_intent_data_id"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_CURSOR_COLUMNS:[Ljava/lang/String;

    .line 371
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "suggest_text_1"

    aput-object v1, v0, v4

    const-string v1, "suggest_text_2"

    aput-object v1, v0, v5

    const-string v1, "suggest_group"

    aput-object v1, v0, v6

    const-string v1, "suggest_icon_1"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "suggest_intent_data_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_CURSOR_GS_COLUMNS:[Ljava/lang/String;

    .line 427
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "item.rowid"

    aput-object v1, v0, v3

    const-string v1, "item.title"

    aput-object v1, v0, v4

    const-string v1, "sect.title"

    aput-object v1, v0, v5

    const-string v1, "category.title"

    aput-object v1, v0, v6

    const-string v1, "item.name"

    aput-object v1, v0, v7

    sput-object v0, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_ITEMS_PROJECTION:[Ljava/lang/String;

    .line 470
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "item.rowid"

    aput-object v1, v0, v3

    const-string v1, "item.title"

    aput-object v1, v0, v4

    const-string v1, "sect.title"

    aput-object v1, v0, v5

    const-string v1, "item.name"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_SECTIONS_PROJECTION:[Ljava/lang/String;

    .line 474
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "item.rowid"

    aput-object v1, v0, v3

    const-string v1, "item.title"

    aput-object v1, v0, v4

    const-string v1, "item.name"

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_CATEGORY_PROJECTION:[Ljava/lang/String;

    .line 478
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "item.rowid"

    aput-object v1, v0, v3

    const-string v1, "item.title"

    aput-object v1, v0, v4

    const-string v1, "sect.title"

    aput-object v1, v0, v5

    const-string v1, "category.title"

    aput-object v1, v0, v6

    const-string v1, "item.titleps1"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "item.titleps2"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_ITEMS_SUMMARY_PROJECTION:[Ljava/lang/String;

    .line 482
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "item.rowid"

    aput-object v1, v0, v3

    const-string v1, "item.title"

    aput-object v1, v0, v4

    const-string v1, "sect.title"

    aput-object v1, v0, v5

    const-string v1, "item.summary"

    aput-object v1, v0, v6

    const-string v1, "item.summaryps1"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "item.summaryps2"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_SECTIONS_SUMMARY_PROJECTION:[Ljava/lang/String;

    .line 486
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "item.rowid"

    aput-object v1, v0, v3

    const-string v1, "item.title"

    aput-object v1, v0, v4

    const-string v1, "item.icon"

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_CATEGORY_SUMMARY_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method private static buildUriMatcher()Landroid/content/UriMatcher;
    .locals 5

    .prologue
    const/16 v4, 0xe

    const/4 v3, 0x1

    .line 285
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 286
    .local v0, "result":Landroid/content/UriMatcher;
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "search_suggest_query"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 287
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "search_suggest_query/*"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 288
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "header"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 289
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "header/#"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 290
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "header/item/*"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 291
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "header/category"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 292
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "header/category/*"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 293
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "header/section"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 294
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "header/section/*"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 295
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "header/search"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 296
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "update/reconcile"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 297
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "update/language"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 298
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "header/owner/#"

    const/16 v3, 0xc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 299
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "header/item"

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 300
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "search_suggest_regex_query"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 301
    const-string v1, "com.samsung.helphub.provider.search"

    const-string v2, "search_suggest_regex_query/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 302
    return-object v0
.end method

.method private generateSuggestions(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 167
    const/4 v9, 0x0

    .line 168
    .local v9, "result":Landroid/database/Cursor;
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 172
    .local v4, "selectionArgs":[Ljava/lang/String;
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 173
    .local v0, "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "header"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 176
    new-instance v8, Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-direct {v8, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 177
    .local v8, "columnMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "_id"

    const-string v2, "header.rowid AS _id"

    invoke-virtual {v8, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    const-string v1, "suggest_text_1"

    const-string v2, "name AS suggest_text_1"

    invoke-virtual {v8, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    const-string v1, "suggest_text_2"

    const-string v2, "name AS suggest_text_2"

    invoke-virtual {v8, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    const-string v1, "suggest_intent_data_id"

    const-string v2, "rowid AS suggest_intent_data_id"

    invoke-virtual {v8, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    const-string v1, "suggest_shortcut_id"

    const-string v2, "rowid AS suggest_shortcut_id"

    invoke-virtual {v8, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    invoke-virtual {v0, v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 188
    iget-object v1, p0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v2, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTION_REQUEST_PROJECTION:[Ljava/lang/String;

    const-string v3, "name  MATCH ?"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 191
    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_0

    .line 192
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 193
    const/4 v9, 0x0

    .line 196
    :cond_0
    return-object v9
.end method

.method private generateSuggestionsForGS(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 33
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 522
    new-instance v25, Landroid/database/MatrixCursor;

    sget-object v5, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_CURSOR_GS_COLUMNS:[Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-direct {v0, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 523
    .local v25, "result":Landroid/database/MatrixCursor;
    const-string v23, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase_hex MATCH ? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid INNER JOIN header AS category ON sect.owner_id=category.rowid"

    .line 524
    .local v23, "item_table":Ljava/lang/String;
    const-string v27, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase_hex MATCH ? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid"

    .line 525
    .local v27, "section_table":Ljava/lang/String;
    const-string v20, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase_hex MATCH ? AND dict.header_id=item.rowid)"

    .line 527
    .local v20, "category_table":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 529
    const/4 v5, 0x1

    new-array v8, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v5

    .line 533
    .local v8, "selectionArgs":[Ljava/lang/String;
    const-string v23, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid INNER JOIN header AS category ON sect.owner_id=category.rowid"

    .line 534
    const-string v27, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid"

    .line 535
    const-string v20, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.header_id=item.rowid)"

    .line 550
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/helphub/search/SearchProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v24

    .line 553
    .local v24, "res":Landroid/content/res/Resources;
    const/16 v21, 0x0

    .line 555
    .local v21, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v4, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v4}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 556
    .local v4, "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 557
    const-string v5, "item.level=2"

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 558
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v6, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_ITEMS_SUMMARY_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const-string v9, "item.title"

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 560
    if-eqz v21, :cond_1

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 565
    :cond_0
    const/4 v5, 0x1

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v32

    .line 566
    .local v32, "titleId":I
    const/4 v5, 0x4

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 567
    .local v18, "TextPs1Id":I
    const/4 v5, 0x5

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 568
    .local v19, "TextPs2Id":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/helphub/search/SearchProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, p0

    move/from16 v1, v32

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/samsung/helphub/search/SearchProvider;->makePSText(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v31

    .line 570
    .local v31, "title":Ljava/lang/String;
    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    .line 571
    .local v28, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v5, 0x3

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " > "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x2

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 573
    invoke-virtual/range {v25 .. v25}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 576
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_0

    .line 579
    .end local v18    # "TextPs1Id":I
    .end local v19    # "TextPs2Id":I
    .end local v28    # "stringBuilder":Ljava/lang/StringBuilder;
    .end local v31    # "title":Ljava/lang/String;
    .end local v32    # "titleId":I
    :cond_1
    if-eqz v21, :cond_2

    .line 580
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 586
    :cond_2
    :try_start_1
    new-instance v9, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v9}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 587
    .end local v4    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .local v9, "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :try_start_2
    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 588
    const-string v5, "item.level=1"

    invoke-virtual {v9, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 590
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v11, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_SECTIONS_SUMMARY_PROJECTION:[Ljava/lang/String;

    const/4 v12, 0x0

    const-string v14, "item.title"

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v13, v8

    invoke-virtual/range {v9 .. v16}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 592
    if-eqz v21, :cond_4

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 595
    :cond_3
    const/4 v5, 0x3

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v30

    .line 596
    .local v30, "summaryId":I
    if-gez v30, :cond_c

    .line 597
    const-string v29, ""

    .line 605
    .local v29, "summary":Ljava/lang/String;
    :goto_1
    invoke-virtual/range {v25 .. v25}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v5, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    const/4 v6, 0x2

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 607
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    move-result v5

    if-nez v5, :cond_3

    .line 610
    .end local v29    # "summary":Ljava/lang/String;
    .end local v30    # "summaryId":I
    :cond_4
    if-eqz v21, :cond_5

    .line 611
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 617
    :cond_5
    :try_start_3
    new-instance v4, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v4}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 618
    .end local v9    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v4    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :try_start_4
    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 619
    const-string v5, "item.level=0"

    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 620
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v12, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_CATEGORY_SUMMARY_PROJECTION:[Ljava/lang/String;

    const/4 v13, 0x0

    const-string v15, "item.title"

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v10, v4

    move-object v14, v8

    invoke-virtual/range {v10 .. v17}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 622
    if-eqz v21, :cond_7

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 624
    :cond_6
    const/4 v5, 0x2

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 625
    .local v22, "iconId":I
    invoke-virtual/range {v25 .. v25}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v21

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 627
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-result v5

    if-nez v5, :cond_6

    .line 630
    .end local v22    # "iconId":I
    :cond_7
    if-eqz v21, :cond_8

    .line 631
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 634
    :cond_8
    return-object v25

    .line 540
    .end local v4    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v8    # "selectionArgs":[Ljava/lang/String;
    .end local v21    # "cursor":Landroid/database/Cursor;
    .end local v24    # "res":Landroid/content/res/Resources;
    :cond_9
    const-string v5, "-"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 541
    const/16 v5, 0x2d

    const/16 v6, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    .line 543
    :cond_a
    invoke-direct/range {p0 .. p1}, Lcom/samsung/helphub/search/SearchProvider;->searchword(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 545
    .local v26, "searhWord":Ljava/lang/String;
    const/4 v5, 0x2

    new-array v8, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v5

    const/4 v5, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "*"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v5

    .restart local v8    # "selectionArgs":[Ljava/lang/String;
    goto/16 :goto_0

    .line 579
    .end local v26    # "searhWord":Ljava/lang/String;
    .restart local v21    # "cursor":Landroid/database/Cursor;
    .restart local v24    # "res":Landroid/content/res/Resources;
    :catchall_0
    move-exception v5

    if-eqz v21, :cond_b

    .line 580
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v5

    .line 600
    .restart local v9    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v30    # "summaryId":I
    :cond_c
    const/4 v5, 0x4

    :try_start_5
    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 601
    .restart local v18    # "TextPs1Id":I
    const/4 v5, 0x5

    move-object/from16 v0, v21

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 603
    .restart local v19    # "TextPs2Id":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/helphub/search/SearchProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v0, p0

    move/from16 v1, v30

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/samsung/helphub/search/SearchProvider;->makePSText(Landroid/content/Context;III)Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    move-result-object v29

    .restart local v29    # "summary":Ljava/lang/String;
    goto/16 :goto_1

    .line 610
    .end local v9    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v18    # "TextPs1Id":I
    .end local v19    # "TextPs2Id":I
    .end local v29    # "summary":Ljava/lang/String;
    .end local v30    # "summaryId":I
    .restart local v4    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :catchall_1
    move-exception v5

    :goto_2
    if-eqz v21, :cond_d

    .line 611
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    :cond_d
    throw v5

    .line 630
    .end local v4    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v9    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :catchall_2
    move-exception v5

    move-object v4, v9

    .end local v9    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v4    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :goto_3
    if-eqz v21, :cond_e

    .line 631
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    :cond_e
    throw v5

    .line 630
    :catchall_3
    move-exception v5

    goto :goto_3

    .line 610
    .end local v4    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v9    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :catchall_4
    move-exception v5

    move-object v4, v9

    .end local v9    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v4    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    goto :goto_2
.end method

.method private generateSuggestionsWithPath(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 35
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 645
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 646
    const/16 v30, 0x0

    .line 829
    :cond_0
    :goto_0
    return-object v30

    .line 650
    :cond_1
    const-string v3, "-"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 651
    const/16 v3, 0x2d

    const/16 v4, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    .line 653
    :cond_2
    new-instance v27, Ljava/lang/StringBuilder;

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 654
    .local v27, "lowcase_query":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4, v5}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 655
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 656
    .local v34, "titlecase_query":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->toTitleCase(C)C

    move-result v4

    move-object/from16 v0, v34

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 657
    new-instance v30, Landroid/database/MatrixCursor;

    sget-object v3, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_CURSOR_COLUMNS:[Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-direct {v0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 658
    .local v30, "result":Landroid/database/MatrixCursor;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v28

    .line 660
    .local v28, "mLanguage":Ljava/lang/String;
    const/4 v3, 0x2

    new-array v11, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v28, v11, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v34

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v11, v3

    .line 665
    .local v11, "selectionArgs_titlecase":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/helphub/search/SearchProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v29

    .line 667
    .local v29, "res":Landroid/content/res/Resources;
    const/16 v25, 0x0

    .line 669
    .local v25, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 670
    .local v2, "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v3, "zh_CN"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "zh_TW"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "zh_HK"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "ko_KR"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 671
    :cond_3
    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v28, v6, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3

    .line 672
    .local v6, "selectionArgs_lowcase":[Ljava/lang/String;
    const-string v3, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase LIKE ? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid INNER JOIN header AS category ON sect.owner_id=category.rowid"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 677
    :goto_1
    const-string v3, "item.level=2"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 678
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v4, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_ITEMS_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const-string v7, "item.title"

    const/4 v8, 0x0

    const-string v9, "item.name COLLATE NOCASE ASC"

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 680
    if-eqz v25, :cond_5

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 685
    :cond_4
    const/4 v3, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    .line 686
    .local v33, "titleId":I
    const v3, 0x7f0a01d7

    move/from16 v0, v33

    if-ne v0, v3, :cond_1c

    .line 687
    const v3, 0x7f0a0145

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 688
    .local v26, "extraString":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v26, v3, v4

    move-object/from16 v0, v29

    move/from16 v1, v33

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    .line 692
    .end local v26    # "extraString":Ljava/lang/String;
    .local v32, "title":Ljava/lang/String;
    :goto_2
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    .line 693
    .local v31, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v3, 0x3

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v31

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x2

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 695
    invoke-virtual/range {v30 .. v30}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    move-object/from16 v0, v32

    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 698
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_4

    .line 701
    .end local v31    # "stringBuilder":Ljava/lang/StringBuilder;
    .end local v32    # "title":Ljava/lang/String;
    .end local v33    # "titleId":I
    :cond_5
    if-eqz v25, :cond_6

    .line 702
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 706
    :cond_6
    const-string v3, "uk_UA"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "ru_RU"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_27

    .line 708
    :cond_7
    :try_start_1
    new-instance v7, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v7}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 709
    .end local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .local v7, "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :try_start_2
    const-string v3, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase MATCH ? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid INNER JOIN header AS category ON sect.owner_id=category.rowid"

    invoke-virtual {v7, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 710
    const-string v3, "item.level=2"

    invoke-virtual {v7, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 711
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v9, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_ITEMS_PROJECTION:[Ljava/lang/String;

    const/4 v10, 0x0

    const-string v12, "item.title"

    const/4 v13, 0x0

    const-string v14, "item.name COLLATE NOCASE ASC"

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 713
    if-eqz v25, :cond_9

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 718
    :cond_8
    const/4 v3, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    .line 719
    .restart local v33    # "titleId":I
    const v3, 0x7f0a01d7

    move/from16 v0, v33

    if-ne v0, v3, :cond_1d

    .line 720
    const v3, 0x7f0a0145

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 721
    .restart local v26    # "extraString":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v26, v3, v4

    move-object/from16 v0, v29

    move/from16 v1, v33

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    .line 725
    .end local v26    # "extraString":Ljava/lang/String;
    .restart local v32    # "title":Ljava/lang/String;
    :goto_3
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    .line 726
    .restart local v31    # "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v3, 0x3

    move-object/from16 v0, v25

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v31

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " > "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x2

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 728
    invoke-virtual/range {v30 .. v30}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    move-object/from16 v0, v32

    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 731
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_c

    move-result v3

    if-nez v3, :cond_8

    .line 734
    .end local v31    # "stringBuilder":Ljava/lang/StringBuilder;
    .end local v32    # "title":Ljava/lang/String;
    .end local v33    # "titleId":I
    :cond_9
    if-eqz v25, :cond_a

    .line 735
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 741
    :cond_a
    :goto_4
    :try_start_3
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_a

    .line 742
    .end local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :try_start_4
    const-string v3, "zh_CN"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string v3, "zh_TW"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string v3, "zh_HK"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string v3, "ko_KR"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 743
    :cond_b
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/4 v3, 0x0

    aput-object v28, v16, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v16, v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_b

    .line 744
    .end local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    .local v16, "selectionArgs_lowcase":[Ljava/lang/String;
    :try_start_5
    const-string v3, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase LIKE ? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 749
    :goto_5
    const-string v3, "item.level=1"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 751
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v14, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_SECTIONS_PROJECTION:[Ljava/lang/String;

    const/4 v15, 0x0

    const-string v17, "item.title"

    const/16 v18, 0x0

    const-string v19, "item.name COLLATE NOCASE ASC"

    move-object v12, v2

    invoke-virtual/range {v12 .. v19}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 753
    if-eqz v25, :cond_d

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 755
    :cond_c
    invoke-virtual/range {v30 .. v30}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x2

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 757
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-result v3

    if-nez v3, :cond_c

    .line 760
    :cond_d
    if-eqz v25, :cond_e

    .line 761
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 765
    :cond_e
    const-string v3, "uk_UA"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    const-string v3, "ru_RU"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_26

    .line 767
    :cond_f
    :try_start_6
    new-instance v7, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v7}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 768
    .end local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :try_start_7
    const-string v3, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase MATCH ? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid"

    invoke-virtual {v7, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 769
    const-string v3, "item.level=1"

    invoke-virtual {v7, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 771
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v9, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_SECTIONS_PROJECTION:[Ljava/lang/String;

    const/4 v10, 0x0

    const-string v12, "item.title"

    const/4 v13, 0x0

    const-string v14, "item.name COLLATE NOCASE ASC"

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 773
    if-eqz v25, :cond_11

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 775
    :cond_10
    invoke-virtual/range {v30 .. v30}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x2

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 777
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_9

    move-result v3

    if-nez v3, :cond_10

    .line 780
    :cond_11
    if-eqz v25, :cond_12

    .line 781
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 787
    :cond_12
    :goto_6
    :try_start_8
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_7

    .line 788
    .end local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :try_start_9
    const-string v3, "zh_CN"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    const-string v3, "zh_TW"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    const-string v3, "zh_HK"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    const-string v3, "ko_KR"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_22

    .line 789
    :cond_13
    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v28, v6, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_8

    .line 790
    .end local v16    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    :try_start_a
    const-string v3, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase LIKE ? AND dict.header_id=item.rowid)"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 795
    :goto_7
    const-string v3, "item.level=0"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 796
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_CATEGORY_PROJECTION:[Ljava/lang/String;

    const/16 v20, 0x0

    const-string v22, "item.title"

    const/16 v23, 0x0

    const-string v24, "item.name COLLATE NOCASE ASC"

    move-object/from16 v17, v2

    move-object/from16 v21, v6

    invoke-virtual/range {v17 .. v24}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 798
    if-eqz v25, :cond_15

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_15

    .line 800
    :cond_14
    invoke-virtual/range {v30 .. v30}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 802
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    move-result v3

    if-nez v3, :cond_14

    .line 805
    :cond_15
    if-eqz v25, :cond_16

    .line 806
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 810
    :cond_16
    const-string v3, "uk_UA"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    const-string v3, "ru_RU"

    move-object/from16 v0, v28

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 812
    :cond_17
    :try_start_b
    new-instance v7, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v7}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    .line 813
    .end local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :try_start_c
    const-string v3, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase MATCH ? AND dict.header_id=item.rowid)"

    invoke-virtual {v7, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 814
    const-string v3, "item.level=0"

    invoke-virtual {v7, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 815
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v9, Lcom/samsung/helphub/search/SearchProvider;->SUGGESTIONS_CATEGORY_PROJECTION:[Ljava/lang/String;

    const/4 v10, 0x0

    const-string v12, "item.title"

    const/4 v13, 0x0

    const-string v14, "item.name COLLATE NOCASE ASC"

    invoke-virtual/range {v7 .. v14}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 817
    if-eqz v25, :cond_19

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_19

    .line 819
    :cond_18
    invoke-virtual/range {v30 .. v30}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 821
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    move-result v3

    if-nez v3, :cond_18

    .line 824
    :cond_19
    if-eqz v25, :cond_25

    .line 825
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    move-object v2, v7

    .end local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    goto/16 :goto_0

    .line 674
    .end local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    :cond_1a
    const/4 v3, 0x2

    :try_start_d
    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v28, v6, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3

    .line 675
    .restart local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    const-string v3, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase MATCH ? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid INNER JOIN header AS category ON sect.owner_id=category.rowid"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_1

    .line 701
    .end local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    :catchall_0
    move-exception v3

    if-eqz v25, :cond_1b

    .line 702
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    :cond_1b
    throw v3

    .line 690
    .restart local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v33    # "titleId":I
    :cond_1c
    :try_start_e
    move-object/from16 v0, v29

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    move-result-object v32

    .restart local v32    # "title":Ljava/lang/String;
    goto/16 :goto_2

    .line 723
    .end local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v32    # "title":Ljava/lang/String;
    .restart local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :cond_1d
    :try_start_f
    move-object/from16 v0, v29

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_c

    move-result-object v32

    .restart local v32    # "title":Ljava/lang/String;
    goto/16 :goto_3

    .line 734
    .end local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v32    # "title":Ljava/lang/String;
    .end local v33    # "titleId":I
    .restart local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :catchall_1
    move-exception v3

    :goto_8
    if-eqz v25, :cond_1e

    .line 735
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    :cond_1e
    throw v3

    .line 746
    :cond_1f
    const/4 v3, 0x2

    :try_start_10
    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/4 v3, 0x0

    aput-object v28, v16, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v16, v3
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_b

    .line 747
    .end local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v16    # "selectionArgs_lowcase":[Ljava/lang/String;
    :try_start_11
    const-string v3, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase MATCH ? AND dict.header_id=item.rowid) INNER JOIN header AS sect ON item.owner_id=sect.rowid"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    goto/16 :goto_5

    .line 760
    :catchall_2
    move-exception v3

    move-object/from16 v6, v16

    .end local v16    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    :goto_9
    if-eqz v25, :cond_20

    .line 761
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    :cond_20
    throw v3

    .line 780
    .end local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v16    # "selectionArgs_lowcase":[Ljava/lang/String;
    :catchall_3
    move-exception v3

    :goto_a
    if-eqz v25, :cond_21

    .line 781
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    :cond_21
    throw v3

    .line 792
    :cond_22
    const/4 v3, 0x2

    :try_start_12
    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v28, v6, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "*"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v3
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_8

    .line 793
    .end local v16    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    :try_start_13
    const-string v3, "header AS item INNER JOIN search_phrase AS dict ON (dict.language=? AND dict.phrase MATCH ? AND dict.header_id=item.rowid)"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_4

    goto/16 :goto_7

    .line 805
    :catchall_4
    move-exception v3

    :goto_b
    if-eqz v25, :cond_23

    .line 806
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    :cond_23
    throw v3

    .line 824
    :catchall_5
    move-exception v3

    :goto_c
    if-eqz v25, :cond_24

    .line 825
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    :cond_24
    throw v3

    .line 824
    .end local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :catchall_6
    move-exception v3

    move-object v2, v7

    .end local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    goto :goto_c

    .line 805
    .end local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v16    # "selectionArgs_lowcase":[Ljava/lang/String;
    :catchall_7
    move-exception v3

    move-object v2, v7

    .end local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    move-object/from16 v6, v16

    .end local v16    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    goto :goto_b

    .end local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v16    # "selectionArgs_lowcase":[Ljava/lang/String;
    :catchall_8
    move-exception v3

    move-object/from16 v6, v16

    .end local v16    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    goto :goto_b

    .line 780
    .end local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v16    # "selectionArgs_lowcase":[Ljava/lang/String;
    :catchall_9
    move-exception v3

    move-object v2, v7

    .end local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    goto :goto_a

    .line 760
    .end local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v16    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :catchall_a
    move-exception v3

    move-object v2, v7

    .end local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    goto :goto_9

    :catchall_b
    move-exception v3

    goto :goto_9

    .line 734
    .end local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :catchall_c
    move-exception v3

    move-object v2, v7

    .end local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    goto/16 :goto_8

    .end local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :cond_25
    move-object v2, v7

    .end local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    goto/16 :goto_0

    .end local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v16    # "selectionArgs_lowcase":[Ljava/lang/String;
    :cond_26
    move-object v7, v2

    .end local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    goto/16 :goto_6

    .end local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v16    # "selectionArgs_lowcase":[Ljava/lang/String;
    .restart local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v6    # "selectionArgs_lowcase":[Ljava/lang/String;
    :cond_27
    move-object v7, v2

    .end local v2    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v7    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    goto/16 :goto_4
.end method

.method private getHeader(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "level"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 201
    const/4 v8, 0x0

    .line 202
    .local v8, "result":Landroid/database/Cursor;
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 203
    .local v0, "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "header"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 205
    invoke-direct {p0}, Lcom/samsung/helphub/search/SearchProvider;->mapHeaderColumns()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 207
    sget-object v1, Lcom/samsung/helphub/search/SearchProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 208
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rowid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 213
    :cond_0
    :goto_0
    if-eqz p4, :cond_1

    aget-object v1, p4, v3

    if-eqz v1, :cond_1

    .line 214
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, p4, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p4, v3

    .line 215
    const-string p3, "name  MATCH ?"

    .line 217
    :cond_1
    iget-object v1, p0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 219
    return-object v8

    .line 209
    :cond_2
    if-ltz p6, :cond_0

    .line 210
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "level="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private getHeaderByName(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 257
    const/4 v11, 0x0

    .line 258
    .local v11, "result":Landroid/database/Cursor;
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v12

    .line 260
    .local v12, "segments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v8, -0x1

    .line 261
    .local v8, "deep":I
    const/4 v1, 0x2

    invoke-interface {v12, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 262
    .local v10, "name":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-interface {v12, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 263
    .local v9, "level":Ljava/lang/String;
    const-string v1, "item"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 264
    sget-object v1, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v8

    .line 270
    :cond_0
    :goto_0
    if-ltz v8, :cond_1

    .line 271
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 272
    .local v0, "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "header"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 275
    invoke-direct {p0}, Lcom/samsung/helphub/search/SearchProvider;->mapHeaderColumns()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 277
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "level="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "name"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 279
    iget-object v1, p0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 281
    .end local v0    # "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    :cond_1
    return-object v11

    .line 265
    :cond_2
    const-string v1, "section"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 266
    sget-object v1, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v8

    goto :goto_0

    .line 267
    :cond_3
    const-string v1, "category"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 268
    sget-object v1, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v8

    goto :goto_0
.end method

.method private getHeaderByOwner(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 225
    const/4 v8, 0x0

    .line 226
    .local v8, "result":Landroid/database/Cursor;
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 227
    .local v0, "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "header"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 229
    invoke-direct {p0}, Lcom/samsung/helphub/search/SearchProvider;->mapHeaderColumns()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 230
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "owner_id=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 231
    iget-object v1, p0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 233
    return-object v8
.end method

.method private makePSText(Landroid/content/Context;III)Ljava/lang/String;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textID"    # I
    .param p3, "TextPs1Id"    # I
    .param p4, "TextPs2Id"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 492
    const/4 v0, 0x0

    .line 495
    .local v0, "TextPsCount":I
    if-eq p3, v2, :cond_0

    .line 496
    add-int/lit8 v0, v0, 0x1

    .line 497
    :cond_0
    if-eq p4, v2, :cond_1

    .line 498
    add-int/lit8 v0, v0, 0x1

    .line 500
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 508
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 511
    .local v1, "result":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 502
    .end local v1    # "result":Ljava/lang/String;
    :pswitch_0
    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 503
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0

    .line 505
    .end local v1    # "result":Ljava/lang/String;
    :pswitch_1
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 506
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0

    .line 500
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private mapHeaderColumns()Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 306
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 307
    .local v0, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "_id"

    const-string v2, "rowid AS _id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    const-string v1, "header.rowid"

    const-string v2, "header.rowid AS _id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    const-string v1, "title"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    const-string v1, "titleps1"

    const-string v2, "titleps1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    const-string v1, "titleps2"

    const-string v2, "titleps2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    const-string v1, "summary"

    const-string v2, "summary"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    const-string v1, "summaryps1"

    const-string v2, "summaryps1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    const-string v1, "summaryps2"

    const-string v2, "summaryps2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    const-string v1, "level"

    const-string v2, "level"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    const-string v1, "fragment"

    const-string v2, "fragment"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    const-string v1, "layout"

    const-string v2, "layout"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    const-string v1, "header_res_id"

    const-string v2, "header_res_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    const-string v1, "items"

    const-string v2, "items"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    const-string v1, "icon"

    const-string v2, "icon"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    const-string v1, "owner_id"

    const-string v2, "owner_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    const-string v1, "items_count"

    const-string v2, "items_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    const-string v1, "is_plugin_res"

    const-string v2, "is_plugin_res"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    const-string v1, "plugin_app_id"

    const-string v2, "plugin_app_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    return-object v0
.end method

.method private queryHeaderBySelection(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "level"    # I

    .prologue
    const/4 v5, 0x0

    .line 238
    const/4 v8, 0x0

    .line 239
    .local v8, "result":Landroid/database/Cursor;
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 240
    .local v0, "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v1, "header"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 242
    invoke-direct {p0}, Lcom/samsung/helphub/search/SearchProvider;->mapHeaderColumns()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 243
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "level="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 244
    iget-object v1, p0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 246
    return-object v8
.end method

.method private searchHeaderByKeyWord(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 18
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "columns"    # [Ljava/lang/String;

    .prologue
    .line 833
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "\""

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 834
    :cond_0
    const/4 v15, 0x0

    .line 880
    :cond_1
    :goto_0
    return-object v15

    .line 838
    :cond_2
    const-string v2, "-"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 839
    const/16 v2, 0x2d

    const/16 v3, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    .line 843
    :cond_3
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 844
    .local v1, "builder":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/helphub/search/SearchProvider;->mapHeaderColumns()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 845
    const-string v2, "header INNER JOIN search_phrase ON (header.rowid=search_phrase.header_id)"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 849
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v14

    .line 850
    .local v14, "mLanguage":Ljava/lang/String;
    const-string v2, "zh_CN"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "zh_TW"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "zh_HK"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "ko_KR"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 851
    :cond_4
    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v14, v5, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    .line 852
    .local v5, "selectionArgs":[Ljava/lang/String;
    const-string v2, "language=? AND phrase LIKE ?"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 860
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v4, 0x0

    const-string v6, "header_id"

    const/4 v7, 0x0

    const-string v8, "level DESC, name COLLATE NOCASE ASC"

    move-object/from16 v3, p2

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 863
    .local v15, "result":Landroid/database/Cursor;
    if-eqz v15, :cond_5

    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_1

    .line 867
    :cond_5
    const/4 v2, 0x2

    new-array v10, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v10, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v10, v2

    .line 872
    .local v10, "selectionArgsLike":[Ljava/lang/String;
    new-instance v6, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v6}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 873
    .local v6, "builderLike":Landroid/database/sqlite/SQLiteQueryBuilder;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/helphub/search/SearchProvider;->mapHeaderColumns()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 874
    const-string v2, "header INNER JOIN search_phrase ON (header.rowid=search_phrase.header_id)"

    invoke-virtual {v6, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 877
    const-string v2, "language=? AND phrase_hex LIKE ?"

    invoke-virtual {v6, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 878
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v9, 0x0

    const-string v11, "header_id"

    const/4 v12, 0x0

    const-string v13, "level DESC, name COLLATE NOCASE ASC"

    move-object/from16 v8, p2

    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .local v16, "resultLike":Landroid/database/Cursor;
    move-object/from16 v15, v16

    .line 880
    goto/16 :goto_0

    .line 855
    .end local v5    # "selectionArgs":[Ljava/lang/String;
    .end local v6    # "builderLike":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v10    # "selectionArgsLike":[Ljava/lang/String;
    .end local v15    # "result":Landroid/database/Cursor;
    .end local v16    # "resultLike":Landroid/database/Cursor;
    :cond_6
    invoke-direct/range {p0 .. p1}, Lcom/samsung/helphub/search/SearchProvider;->searchword(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 856
    .local v17, "searchword":Ljava/lang/String;
    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v14, v5, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "*"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v2

    .line 857
    .restart local v5    # "selectionArgs":[Ljava/lang/String;
    const-string v2, "language=? AND phrase_hex MATCH ?"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method private searchword(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 885
    const-string v2, ""

    .line 886
    .local v2, "result":Ljava/lang/String;
    const-string v0, ""

    .line 888
    .local v0, "convert_word":Ljava/lang/String;
    const-string v4, "\\["

    const-string v5, ""

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "\\]"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 889
    const-string v4, "\\&| "

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 891
    .local v3, "searchWord":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_1

    .line 892
    aget-object v4, v3, v1

    invoke-static {v4}, Lcom/samsung/helphub/search/NameNormalizer;->lettersAndDigitsOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 893
    const-string v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 894
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Lcom/samsung/helphub/search/NameNormalizer;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 891
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 896
    :cond_1
    return-object v2
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 333
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "There is no way to delete "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 140
    const/4 v0, 0x0

    .line 141
    .local v0, "result":Ljava/lang/String;
    sget-object v1, Lcom/samsung/helphub/search/SearchProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 147
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown Uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 143
    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    .line 150
    return-object v0

    .line 141
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 363
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "There is no way to insert "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()Z
    .locals 3

    .prologue
    .line 61
    new-instance v1, Lcom/samsung/helphub/search/SearchDBHelper;

    invoke-virtual {p0}, Lcom/samsung/helphub/search/SearchProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/helphub/search/SearchDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/helphub/search/SearchProvider;->mDatabaseHelper:Lcom/samsung/helphub/search/SearchDBHelper;

    .line 63
    :try_start_0
    iget-object v1, p0, Lcom/samsung/helphub/search/SearchProvider;->mDatabaseHelper:Lcom/samsung/helphub/search/SearchDBHelper;

    invoke-virtual {v1}, Lcom/samsung/helphub/search/SearchDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 74
    const/4 v8, 0x0

    .line 75
    .local v8, "result":Landroid/database/Cursor;
    sget-object v0, Lcom/samsung/helphub/search/SearchProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v7

    .line 76
    .local v7, "i":I
    packed-switch v7, :pswitch_data_0

    .line 130
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown Uri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :pswitch_1
    if-nez p4, :cond_1

    .line 79
    const/4 v9, 0x0

    .line 81
    .local v9, "searchString":Ljava/lang/String;
    invoke-direct {p0, v9}, Lcom/samsung/helphub/search/SearchProvider;->generateSuggestionsForGS(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 132
    .end local v9    # "searchString":Ljava/lang/String;
    :goto_0
    if-eqz v8, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/samsung/helphub/search/SearchProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.samsung.helphub.provider.search"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v8, v0, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 135
    :cond_0
    return-object v8

    .line 84
    :cond_1
    aget-object v0, p4, v1

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    .line 85
    .restart local v9    # "searchString":Ljava/lang/String;
    const-string v0, "["

    const-string v1, ""

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 86
    const-string v0, "]"

    const-string v1, ""

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 88
    invoke-direct {p0, v9}, Lcom/samsung/helphub/search/SearchProvider;->generateSuggestionsForGS(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 90
    goto :goto_0

    .line 92
    .end local v9    # "searchString":Ljava/lang/String;
    :pswitch_2
    aget-object v0, p4, v1

    invoke-direct {p0, v0}, Lcom/samsung/helphub/search/SearchProvider;->generateSuggestionsWithPath(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 93
    goto :goto_0

    .line 96
    :pswitch_3
    sget-object v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/helphub/search/SearchProvider;->queryHeaderBySelection(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v8

    .line 98
    goto :goto_0

    .line 101
    :pswitch_4
    sget-object v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/helphub/search/SearchProvider;->getHeader(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v8

    .line 103
    goto :goto_0

    .line 106
    :pswitch_5
    sget-object v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/helphub/search/SearchProvider;->getHeader(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v8

    .line 108
    goto :goto_0

    .line 112
    :pswitch_6
    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/helphub/search/SearchProvider;->getHeader(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v8

    .line 113
    goto :goto_0

    .line 118
    :pswitch_7
    invoke-direct {p0, p1, p2}, Lcom/samsung/helphub/search/SearchProvider;->getHeaderByName(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 119
    goto :goto_0

    .line 122
    :pswitch_8
    aget-object v0, p4, v1

    invoke-direct {p0, v0, p2}, Lcom/samsung/helphub/search/SearchProvider;->searchHeaderByKeyWord(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 123
    goto :goto_0

    .line 126
    :pswitch_9
    invoke-direct/range {p0 .. p5}, Lcom/samsung/helphub/search/SearchProvider;->getHeaderByOwner(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 127
    goto/16 :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_6
        :pswitch_6
        :pswitch_3
        :pswitch_7
        :pswitch_4
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_5
        :pswitch_1
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 339
    const/4 v2, 0x0

    .line 340
    .local v2, "result":I
    sget-object v3, Lcom/samsung/helphub/search/SearchProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 355
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "There is no way to update "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 342
    :pswitch_0
    invoke-static {}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->reset()V

    .line 343
    new-instance v1, Lcom/samsung/helphub/search/ReconcileDBHelper;

    iget-object v3, p0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0}, Lcom/samsung/helphub/search/SearchProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/samsung/helphub/search/ReconcileDBHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    .line 344
    .local v1, "reconciler":Lcom/samsung/helphub/search/ReconcileDBHelper;
    invoke-virtual {v1}, Lcom/samsung/helphub/search/ReconcileDBHelper;->reconcileDatabase()V

    .line 345
    const-string v3, "content://com.samsung.helphub.provider.search/header/category"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 346
    .local v0, "notifyUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/helphub/search/SearchProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 358
    .end local v0    # "notifyUri":Landroid/net/Uri;
    :goto_0
    return v2

    .line 350
    .end local v1    # "reconciler":Lcom/samsung/helphub/search/ReconcileDBHelper;
    :pswitch_1
    new-instance v1, Lcom/samsung/helphub/search/ReconcileDBHelper;

    iget-object v3, p0, Lcom/samsung/helphub/search/SearchProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0}, Lcom/samsung/helphub/search/SearchProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/samsung/helphub/search/ReconcileDBHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    .line 351
    .restart local v1    # "reconciler":Lcom/samsung/helphub/search/ReconcileDBHelper;
    invoke-virtual {v1}, Lcom/samsung/helphub/search/ReconcileDBHelper;->addLanguage()V

    goto :goto_0

    .line 340
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
