.class public Lcom/samsung/helphub/search/SearchDBHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SearchDBHelper.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    const-string v0, "search.db"

    const/4 v1, 0x0

    const/16 v2, 0x9

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 17
    iput-object p1, p0, Lcom/samsung/helphub/search/SearchDBHelper;->mContext:Landroid/content/Context;

    .line 18
    return-void
.end method

.method private createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 46
    const-string v0, "CREATE VIRTUAL TABLE header USING fts3 (level  INTEGER,name TEXT, title INTEGER, titleps1 INTEGER, titleps2 INTEGER, summary INTEGER, summaryps1 INTEGER, summaryps2 INTEGER, fragment TEXT,layout INTEGER,header_res_id INTEGER,items INTEGER,icon INTEGER,owner_id INTEGER,dictionary INTEGER,items_count INTEGER,is_plugin_res INTEGER,plugin_app_id TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 73
    const-string v0, "CREATE VIRTUAL TABLE  search_phrase USING fts3 (header_id INTEGER,phrase  TEXT COLLATE NOCASE,phrase_hex TEXT, is_predefined BOOLEAN, language TEXT)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method private deleteTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 86
    const-string v0, "DROP TABLE IF EXISTS header"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 87
    const-string v0, "DROP TABLE IF EXISTS search_phrase"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 88
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/samsung/helphub/search/SearchDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 24
    new-instance v0, Lcom/samsung/helphub/search/ReconcileDBHelper;

    iget-object v1, p0, Lcom/samsung/helphub/search/SearchDBHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, p1, v1}, Lcom/samsung/helphub/search/ReconcileDBHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    .line 25
    .local v0, "reconciler":Lcom/samsung/helphub/search/ReconcileDBHelper;
    invoke-virtual {v0}, Lcom/samsung/helphub/search/ReconcileDBHelper;->fillDB()V

    .line 26
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/samsung/helphub/search/SearchDBHelper;->deleteTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 39
    invoke-direct {p0, p1}, Lcom/samsung/helphub/search/SearchDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 40
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/samsung/helphub/search/SearchDBHelper;->deleteTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 31
    invoke-direct {p0, p1}, Lcom/samsung/helphub/search/SearchDBHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 32
    new-instance v0, Lcom/samsung/helphub/search/ReconcileDBHelper;

    iget-object v1, p0, Lcom/samsung/helphub/search/SearchDBHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, p1, v1}, Lcom/samsung/helphub/search/ReconcileDBHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    .line 33
    .local v0, "reconciler":Lcom/samsung/helphub/search/ReconcileDBHelper;
    invoke-virtual {v0}, Lcom/samsung/helphub/search/ReconcileDBHelper;->fillDB()V

    .line 34
    return-void
.end method
