.class public interface abstract Lcom/samsung/helphub/search/SearchDB$HeaderTableColumns;
.super Ljava/lang/Object;
.source "SearchDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/search/SearchDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "HeaderTableColumns"
.end annotation


# static fields
.field public static final CONCRETE_ID:Ljava/lang/String; = "header.rowid"

.field public static final DICTIONARY_ID:Ljava/lang/String; = "dictionary"

.field public static final FRAGMENT:Ljava/lang/String; = "fragment"

.field public static final HEADER_LEVEL:Ljava/lang/String; = "level"

.field public static final HEADER_NAME:Ljava/lang/String; = "name"

.field public static final HEADER_RES_ID:Ljava/lang/String; = "header_res_id"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final IS_PLUGIN_RES:Ljava/lang/String; = "is_plugin_res"

.field public static final ITEMS:Ljava/lang/String; = "items"

.field public static final ITEMS_COUNT:Ljava/lang/String; = "items_count"

.field public static final LAYOUT:Ljava/lang/String; = "layout"

.field public static final OWNER_ID:Ljava/lang/String; = "owner_id"

.field public static final PLUGIN_APP_ID:Ljava/lang/String; = "plugin_app_id"

.field public static final SUMMARY:Ljava/lang/String; = "summary"

.field public static final SUMMARY_PS_1:Ljava/lang/String; = "summaryps1"

.field public static final SUMMARY_PS_2:Ljava/lang/String; = "summaryps2"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TITLE_PS_1:Ljava/lang/String; = "titleps1"

.field public static final TITLE_PS_2:Ljava/lang/String; = "titleps2"
