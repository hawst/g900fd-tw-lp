.class public Lcom/samsung/helphub/HelpHubSecondDepthActivity;
.super Lcom/samsung/helphub/HelpHubActivityBase;
.source "HelpHubSecondDepthActivity.java"


# static fields
.field private static currentBundle:Landroid/os/Bundle;

.field private static currentFragment:Ljava/lang/String;


# instance fields
.field protected mBackStackListener:Landroid/app/FragmentManager$OnBackStackChangedListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/helphub/HelpHubActivityBase;-><init>()V

    .line 22
    new-instance v0, Lcom/samsung/helphub/HelpHubSecondDepthActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity$1;-><init>(Lcom/samsung/helphub/HelpHubSecondDepthActivity;)V

    iput-object v0, p0, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->mBackStackListener:Landroid/app/FragmentManager$OnBackStackChangedListener;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v8, 0x400

    const/4 v7, 0x0

    .line 35
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 36
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "fragment_name"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->currentFragment:Ljava/lang/String;

    .line 37
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    sput-object v4, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->currentBundle:Landroid/os/Bundle;

    .line 39
    const-string v4, "HelpHubActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[currentFragment]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->currentFragment:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/Window;->requestFeature(I)Z

    .line 45
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v8, v8}, Landroid/view/Window;->setFlags(II)V

    .line 48
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 49
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    invoke-static {v4}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 53
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/helphub/HelpHubActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->mBackStackListener:Landroid/app/FragmentManager$OnBackStackChangedListener;

    invoke-virtual {v4, v5}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 57
    sget-object v4, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->currentFragment:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    if-nez p1, :cond_1

    .line 58
    sget-object v4, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->currentFragment:Ljava/lang/String;

    sget-object v5, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->currentBundle:Landroid/os/Bundle;

    invoke-static {p0, v4, v5}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v1

    .line 59
    .local v1, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 61
    .local v3, "transaction":Landroid/app/FragmentTransaction;
    invoke-virtual {v3, v7}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 63
    const v4, 0x7f0d0004

    sget-object v5, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->currentFragment:Ljava/lang/String;

    invoke-virtual {v3, v4, v1, v5}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    .line 66
    .end local v1    # "fragment":Landroid/app/Fragment;
    .end local v3    # "transaction":Landroid/app/FragmentTransaction;
    :cond_1
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->mTwoPane:Z

    if-nez v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->mBackStackListener:Landroid/app/FragmentManager$OnBackStackChangedListener;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->removeOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->mBackStackListener:Landroid/app/FragmentManager$OnBackStackChangedListener;

    .line 75
    :cond_0
    invoke-super {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->onDestroy()V

    .line 76
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 96
    const/4 v1, 0x0

    .line 98
    .local v1, "retval":Z
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 121
    :goto_0
    if-nez v1, :cond_0

    invoke-super {p0, p1}, Lcom/samsung/helphub/HelpHubActivityBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    :goto_1
    return v2

    .line 101
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v3

    if-nez v3, :cond_1

    .line 102
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->finish()V

    goto :goto_0

    .line 104
    :cond_1
    iget-boolean v3, p0, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->mTwoPane:Z

    if-eqz v3, :cond_2

    .line 105
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 106
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    const v4, 0x7f0a001e

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setTitle(I)V

    .line 109
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->popBackStack()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :goto_2
    const/4 v1, 0x1

    .line 115
    goto :goto_0

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_2

    .line 121
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 80
    invoke-super {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->onResume()V

    .line 81
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "HELP"

    invoke-static {p0, v2, v3}, Lcom/samsung/helphub/HelpHubCommon;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    sget-object v2, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->currentFragment:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    sget-object v3, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->currentFragment:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 86
    sget-object v2, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->currentFragment:Ljava/lang/String;

    sget-object v3, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->currentBundle:Landroid/os/Bundle;

    invoke-static {p0, v2, v3}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v0

    .line 87
    .local v0, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 88
    .local v1, "transaction":Landroid/app/FragmentTransaction;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 89
    const v2, 0x7f0d0004

    sget-object v3, Lcom/samsung/helphub/HelpHubSecondDepthActivity;->currentFragment:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 92
    .end local v0    # "fragment":Landroid/app/Fragment;
    .end local v1    # "transaction":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method
