.class public Lcom/samsung/helphub/broadcast/HelpHubBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HelpHubBroadcastReceiver.java"


# static fields
.field private static final ACTION_EASY_MODE_CHANGED:Ljava/lang/String; = "com.android.launcher.action.EASY_MODE_CHANGE"

.field private static final LIFE_TIMES_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.app.lifetimes"

.field private static final STORY_ALBUM_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.app.episodes"

.field private static final S_GROUP_PLAY_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.groupcast"

.field private static final S_HEALTH_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.shealth"

.field private static final S_NOTE_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.snote"

.field private static final S_TRANSLATOR_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.translator"

.field private static final TAG:Ljava/lang/String; = "HelpHubBroadcastReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 27
    if-nez p2, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 29
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 30
    .local v0, "action":Ljava/lang/String;
    const-string v4, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 32
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    .line 33
    .local v2, "data":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 34
    const-string v4, "com.samsung.android.app.episodes"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "com.sec.android.app.translator"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "com.samsung.groupcast"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "com.samsung.android.app.lifetimes"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "com.samsung.android.snote"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "com.sec.android.app.shealth"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 40
    :cond_3
    const-string v4, "content://com.samsung.helphub.provider.search/update/reconcile"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 41
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_0

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v3, v5, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 46
    .end local v2    # "data":Ljava/lang/String;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_4
    const-string v4, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 47
    const-string v4, "content://com.samsung.helphub.provider.search/update/reconcile"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 48
    .restart local v3    # "uri":Landroid/net/Uri;
    if-eqz v3, :cond_5

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v3, v5, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 52
    :cond_5
    const-string v4, "activity"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 54
    .local v1, "am":Landroid/app/ActivityManager;
    const-string v4, "HelpHubBroadcastReceiver"

    const-string v5, "FORCE CLOSE PACKAGE :  com.samsung.helphub"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    const-string v4, "com.samsung.helphub"

    invoke-virtual {v1, v4}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
