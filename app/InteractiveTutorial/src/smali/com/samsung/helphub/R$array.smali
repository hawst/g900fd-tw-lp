.class public final Lcom/samsung/helphub/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final accessibility_assistant_menu_cursor_icon:I = 0x7f07000e

.field public static final accessibility_assistant_menu_icon:I = 0x7f07000d

.field public static final accessibility_assistant_menu_left_right_icon:I = 0x7f07000f

.field public static final accessibility_assistant_menu_magnifier_icon:I = 0x7f070012

.field public static final accessibility_assistant_menu_move_icon:I = 0x7f070011

.field public static final accessibility_assistant_menu_up_down_icon:I = 0x7f070010

.field public static final accessibility_baby_crying_more_icon:I = 0x7f07000c

.field public static final accessibility_baby_crying_play_icon:I = 0x7f07000b

.field public static final accessibility_on_off_settings_icon:I = 0x7f07000a

.field public static final accessibility_settings_icon:I = 0x7f070009

.field public static final battery_add_icons:I = 0x7f07001e

.field public static final bluetooth_pictures_bluetooth_icon:I = 0x7f070026

.field public static final bluetooth_pictures_gallery_icon:I = 0x7f070025

.field public static final bluetooth_setup_bluetooth_icon:I = 0x7f070023

.field public static final bluetooth_setup_settings_icon:I = 0x7f070022

.field public static final bluetooth_setup_switch_icon:I = 0x7f070024

.field public static final call_accept_reject_icons:I = 0x7f070027

.field public static final camera_setting_icons:I = 0x7f070028

.field public static final collage_input_images_numbers_range:I = 0x7f070029

.field public static final contacts_add_icons_1:I = 0x7f07002a

.field public static final contacts_add_icons_2:I = 0x7f07002b

.field public static final dialog_sort_mode_items:I = 0x7f070000

.field public static final download_booster_on_off_settings_icon:I = 0x7f07002d

.field public static final download_booster_quickpanel_settings_icon:I = 0x7f07002e

.field public static final download_booster_settings_icon:I = 0x7f07002c

.field public static final email_add_icons:I = 0x7f070030

.field public static final email_composing_emails_icon_compose:I = 0x7f07008c

.field public static final email_composing_emails_icon_drawer:I = 0x7f07008e

.field public static final email_composing_emails_icon_send:I = 0x7f07008d

.field public static final email_managing_emails_icon_delete:I = 0x7f07008f

.field public static final email_managing_emails_icon_markAsReadUnread:I = 0x7f070093

.field public static final email_managing_emails_icon_move:I = 0x7f070090

.field public static final email_managing_emails_icon_moveAll:I = 0x7f070091

.field public static final email_managing_emails_icon_removeAddStar:I = 0x7f070092

.field public static final email_reading_emails_icon_another:I = 0x7f07008b

.field public static final email_reading_emails_icon_folder:I = 0x7f07008a

.field public static final email_viewing_emails_icon_forward:I = 0x7f070087

.field public static final email_viewing_emails_icon_more:I = 0x7f070084

.field public static final email_viewing_emails_icon_next:I = 0x7f070088

.field public static final email_viewing_emails_icon_previous:I = 0x7f070089

.field public static final email_viewing_emails_icon_reply:I = 0x7f070085

.field public static final email_viewing_emails_icon_replyToAll:I = 0x7f070086

.field public static final emer_mode_add:I = 0x7f070067

.field public static final emer_mode_alarm:I = 0x7f070064

.field public static final emer_mode_sms:I = 0x7f070066

.field public static final emer_mode_torch:I = 0x7f070063

.field public static final emer_mode_torch_alarm:I = 0x7f070065

.field public static final gallery_add_icons:I = 0x7f070032

.field public static final gallery_copy_icons:I = 0x7f070033

.field public static final gallery_funny_icons:I = 0x7f070034

.field public static final help_icons_setting:I = 0x7f07002f

.field public static final internet_add_icons:I = 0x7f070042

.field public static final internet_help_ic_managing_multiple_windows:I = 0x7f07006f

.field public static final internet_search_icons:I = 0x7f070043

.field public static final managing_multiple_windows:I = 0x7f070001

.field public static final message_add_icons:I = 0x7f07004a

.field public static final messaging_help_send_icons:I = 0x7f07004b

.field public static final motion_palm_swipe_capture:I = 0x7f070053

.field public static final motion_palm_swipe_mute:I = 0x7f070054

.field public static final motion_pan_to_browse:I = 0x7f070050

.field public static final motion_pan_to_move_icon:I = 0x7f07004f

.field public static final motion_pick_up_direct_call:I = 0x7f07004c

.field public static final motion_pick_up_smart_alert:I = 0x7f07004d

.field public static final motion_shake:I = 0x7f070051

.field public static final motion_tile_to_zoom:I = 0x7f07004e

.field public static final motion_turnover:I = 0x7f070052

.field public static final multi_window_bezel_edit_images:I = 0x7f07005a

.field public static final multi_window_edit_images:I = 0x7f070058

.field public static final multi_window_template_images:I = 0x7f070059

.field public static final nfc_add_icons:I = 0x7f07005b

.field public static final nfc_beam_icons:I = 0x7f07005c

.field public static final phone_add_icons:I = 0x7f07005e

.field public static final phone_help_accept_reject_call:I = 0x7f070062

.field public static final phone_help_make_call_1:I = 0x7f07005f

.field public static final phone_help_make_call_2:I = 0x7f070060

.field public static final phone_help_make_call_3:I = 0x7f070061

.field public static final ringtone_add_icons:I = 0x7f07001f

.field public static final ringtone_add_icons2:I = 0x7f070020

.field public static final safety_onoff:I = 0x7f070068

.field public static final samsungkeyboard_keyboard_voice:I = 0x7f07006e

.field public static final searching_with_sfinder_icons:I = 0x7f070031

.field public static final send_message_icon:I = 0x7f070069

.field public static final send_message_icon_green:I = 0x7f07006a

.field public static final send_message_images:I = 0x7f07006b

.field public static final send_message_images_triggerkey_up:I = 0x7f07006c

.field public static final send_msg_slide_time:I = 0x7f07006d

.field public static final smemo_create_pen_icons:I = 0x7f07007c

.field public static final smemo_create_text_icons:I = 0x7f07007d

.field public static final snote_drawing_tools_icons:I = 0x7f070079

.field public static final snote_erase_tools_icons:I = 0x7f07007b

.field public static final snote_selection_mode_icons:I = 0x7f07007e

.field public static final snote_snap_note_icons_1:I = 0x7f07007f

.field public static final snote_snap_note_icons_2:I = 0x7f070080

.field public static final snote_text_tools_icons:I = 0x7f07007a

.field public static final svoice_add_icons_1:I = 0x7f070081

.field public static final svoice_add_icons_2:I = 0x7f070082

.field public static final svoice_add_icons_3:I = 0x7f070083

.field public static final tutorial_Spen_Gesture_capture_screen:I = 0x7f070003

.field public static final tutorial_Spen_Gesture_capture_screen_show_time:I = 0x7f070004

.field public static final tutorial_Spen_Gesture_open_action_memo:I = 0x7f070005

.field public static final tutorial_Spen_Gesture_open_action_memo_show_time:I = 0x7f070006

.field public static final tutorial_Spen_Gesture_select_text:I = 0x7f070007

.field public static final tutorial_Spen_Gesture_select_text_show_time:I = 0x7f070008

.field public static final tutorial_air_view_icon_labels:I = 0x7f070019

.field public static final tutorial_air_view_list_scrolling:I = 0x7f07001a

.field public static final tutorial_air_view_magnifier:I = 0x7f07001d

.field public static final tutorial_air_view_pen_gallery:I = 0x7f070014

.field public static final tutorial_air_view_pen_splanner:I = 0x7f070013

.field public static final tutorial_air_view_preview_progress_bar:I = 0x7f07001b

.field public static final tutorial_air_view_preview_speed_dial:I = 0x7f07001c

.field public static final tutorial_air_view_progress_preview_finger:I = 0x7f070016

.field public static final tutorial_air_view_progress_preview_pen:I = 0x7f070015

.field public static final tutorial_air_view_speed_dial_finger:I = 0x7f070018

.field public static final tutorial_air_view_speed_dial_pen:I = 0x7f070017

.field public static final tutorial_gesture_browse:I = 0x7f070035

.field public static final tutorial_gesture_browse_show_slide_time:I = 0x7f070036

.field public static final tutorial_gesture_call_accept:I = 0x7f070037

.field public static final tutorial_gesture_call_accept_show_slide_time:I = 0x7f070038

.field public static final tutorial_gesture_jump:I = 0x7f070039

.field public static final tutorial_gesture_jump_show_slide_time:I = 0x7f07003a

.field public static final tutorial_gesture_move:I = 0x7f07003b

.field public static final tutorial_gesture_move_show_slide_time:I = 0x7f07003c

.field public static final tutorial_gesture_quick_glance:I = 0x7f07003d

.field public static final tutorial_gesture_quick_glance_show_slide_time:I = 0x7f07003e

.field public static final tutorial_magazine_launcher_adding_widget:I = 0x7f070045

.field public static final tutorial_magazine_launcher_change_layout:I = 0x7f070049

.field public static final tutorial_magazine_launcher_move_widget:I = 0x7f070046

.field public static final tutorial_magazine_launcher_navigate:I = 0x7f070044

.field public static final tutorial_magazine_launcher_remove_widget:I = 0x7f070047

.field public static final tutorial_magazine_launcher_resize_widget:I = 0x7f070048

.field public static final tutorial_motion_quick_glance:I = 0x7f070040

.field public static final tutorial_motion_quick_glance_show_slide_time:I = 0x7f070041

.field public static final tutorial_ripple_effect:I = 0x7f070056

.field public static final tutorial_ripple_effect_show_slide_time:I = 0x7f070057

.field public static final tutorial_rocking_motion_slide_time:I = 0x7f07003f

.field public static final tutorial_smart_clip_pen_crop:I = 0x7f070070

.field public static final tutorial_smart_screen_introduction:I = 0x7f070071

.field public static final tutorial_smart_screen_pause:I = 0x7f070073

.field public static final tutorial_smart_screen_rotation:I = 0x7f070072

.field public static final tutorial_smart_screen_scroll_face:I = 0x7f070075

.field public static final tutorial_smart_screen_scroll_face_show_slide_time:I = 0x7f070077

.field public static final tutorial_smart_screen_scroll_tilt:I = 0x7f070074

.field public static final tutorial_smart_screen_scroll_tilt_show_slide_time:I = 0x7f070076

.field public static final tutorial_smart_screen_stay:I = 0x7f070078

.field public static final tutorial_video_chapter_list:I = 0x7f070055

.field public static final using_features_in_air_command_penable_text:I = 0x7f07005d

.field public static final wakeup_command:I = 0x7f070002

.field public static final wifi_add_icons:I = 0x7f070021


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
