.class public final Lcom/samsung/helphub/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final accessories_bg_color:I = 0x7f09002a

.field public static final active_title:I = 0x7f09002e

.field public static final breadcrumb_separator:I = 0x7f09000a

.field public static final button_normal_color:I = 0x7f090031

.field public static final button_normal_text_color:I = 0x7f09000d

.field public static final button_pressed_color:I = 0x7f090030

.field public static final button_pressed_text_color:I = 0x7f090007

.field public static final button_text:I = 0x7f09003b

.field public static final button_text_2014:I = 0x7f09003c

.field public static final button_text_expanded:I = 0x7f09003d

.field public static final button_text_group:I = 0x7f09003e

.field public static final category_child_background:I = 0x7f09002c

.field public static final category_group_background:I = 0x7f09002b

.field public static final contents_list_child_title:I = 0x7f09003f

.field public static final dialog_button_shadow:I = 0x7f090010

.field public static final dialog_header_text_color:I = 0x7f09000e

.field public static final dialog_text_color:I = 0x7f09000f

.field public static final expandable_list_background:I = 0x7f090032

.field public static final expandable_list_child_background:I = 0x7f090005

.field public static final expandable_list_child_text:I = 0x7f09000c

.field public static final expandable_list_children_divider:I = 0x7f090006

.field public static final expandable_list_icon_color:I = 0x7f090034

.field public static final expandable_list_sub_titlebar_text:I = 0x7f090033

.field public static final galaxy_findre_notice_color:I = 0x7f09002f

.field public static final gesture_guide_layout_bg_color:I = 0x7f090037

.field public static final gesture_guide_list_item_layout_bg_color:I = 0x7f090036

.field public static final gesture_guide_list_item_text_bg_color:I = 0x7f090039

.field public static final gesture_guide_list_item_text_color:I = 0x7f090038

.field public static final help_tip_background_color:I = 0x7f090027

.field public static final help_tip_box_background_color:I = 0x7f090024

.field public static final help_tip_box_background_color_for_t:I = 0x7f09003a

.field public static final help_tip_box_text_color:I = 0x7f090025

.field public static final help_tip_text_color:I = 0x7f090026

.field public static final helphub_activity_background:I = 0x7f090001

.field public static final helphub_title_text:I = 0x7f090000

.field public static final keyboard_Gray_color:I = 0x7f090029

.field public static final keyboard_tiny_color:I = 0x7f090028

.field public static final list_categroy_headers_item_title:I = 0x7f090012

.field public static final list_help_item_title:I = 0x7f090002

.field public static final list_item_headers_item_title_collapsed:I = 0x7f090022

.field public static final list_item_headers_item_title_expanded:I = 0x7f090023

.field public static final list_secondary_text_color:I = 0x7f090008

.field public static final list_section_headers_item_title:I = 0x7f090013

.field public static final main_activity_background:I = 0x7f090004

.field public static final main_activity_group_title:I = 0x7f09000b

.field public static final main_activity_item_title:I = 0x7f090003

.field public static final selected_title:I = 0x7f09002d

.field public static final theme_category_item_text_color:I = 0x7f09001b

.field public static final theme_category_new_features_text_color:I = 0x7f090019

.field public static final theme_category_new_features_text_view_more_color:I = 0x7f09001a

.field public static final theme_help_page_item_group_text_color:I = 0x7f09001f

.field public static final theme_help_page_item_text_color:I = 0x7f09001d

.field public static final theme_help_page_item_text_color_pen_select:I = 0x7f09001e

.field public static final theme_help_page_item_title_color:I = 0x7f09001c

.field public static final theme_item_text_color:I = 0x7f090014

.field public static final theme_item_text_shadow_color:I = 0x7f090015

.field public static final theme_main_activity_background:I = 0x7f090021

.field public static final theme_main_activity_item_title:I = 0x7f090020

.field public static final theme_section_item_summary_text_color:I = 0x7f090018

.field public static final theme_section_item_title_text_color:I = 0x7f090016

.field public static final theme_section_item_title_text_color_select:I = 0x7f090017

.field public static final third_depth_tip:I = 0x7f090035

.field public static final tw_primary_text_holo_light:I = 0x7f090040

.field public static final two_pane_list_background:I = 0x7f090009

.field public static final welcome_text:I = 0x7f090011


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 576
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
