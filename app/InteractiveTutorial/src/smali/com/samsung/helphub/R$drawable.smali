.class public final Lcom/samsung/helphub/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final accessibility_icon:I = 0x7f020000

.field public static final action_bar_icon_search_selector:I = 0x7f020001

.field public static final action_bar_icon_sort_alphabetically_selector:I = 0x7f020002

.field public static final adding_applications:I = 0x7f020003

.field public static final air_button_infopreview_introduction_1:I = 0x7f020004

.field public static final air_button_infopreview_introduction_2:I = 0x7f020005

.field public static final air_button_infopreview_introduction_3:I = 0x7f020006

.field public static final air_button_infopreview_introduction_4:I = 0x7f020007

.field public static final air_button_infopreview_introduction_5:I = 0x7f020008

.field public static final air_button_infopreview_introduction_att_1:I = 0x7f020009

.field public static final air_button_infopreview_introduction_att_2:I = 0x7f02000a

.field public static final air_view_icon_label_01:I = 0x7f02000b

.field public static final air_view_icon_label_02:I = 0x7f02000c

.field public static final air_view_icon_label_03:I = 0x7f02000d

.field public static final air_view_information_preview_gallery_01:I = 0x7f02000e

.field public static final air_view_information_preview_gallery_02:I = 0x7f02000f

.field public static final air_view_information_preview_gallery_03:I = 0x7f020010

.field public static final air_view_information_preview_splanner_01:I = 0x7f020011

.field public static final air_view_information_preview_splanner_02:I = 0x7f020012

.field public static final air_view_information_preview_splanner_03:I = 0x7f020013

.field public static final air_view_introduction:I = 0x7f020014

.field public static final air_view_list_scrolling_01:I = 0x7f020015

.field public static final air_view_list_scrolling_02:I = 0x7f020016

.field public static final air_view_list_scrolling_03:I = 0x7f020017

.field public static final air_view_magnifier_01:I = 0x7f020018

.field public static final air_view_magnifier_02:I = 0x7f020019

.field public static final air_view_magnifier_03:I = 0x7f02001a

.field public static final air_view_pen_introduction_1:I = 0x7f02001b

.field public static final air_view_pen_introduction_2:I = 0x7f02001c

.field public static final air_view_pointer:I = 0x7f02001d

.field public static final air_view_progress_preview_pen_01:I = 0x7f02001e

.field public static final air_view_progress_preview_pen_02:I = 0x7f02001f

.field public static final air_view_progress_preview_pen_03:I = 0x7f020020

.field public static final air_view_speed_dial_pen_01:I = 0x7f020021

.field public static final air_view_speed_dial_pen_02:I = 0x7f020022

.field public static final air_view_speed_dial_pen_03:I = 0x7f020023

.field public static final air_view_speeddial_01:I = 0x7f020024

.field public static final air_view_speeddial_02:I = 0x7f020025

.field public static final air_view_speeddial_03:I = 0x7f020026

.field public static final air_view_video_preview_01:I = 0x7f020027

.field public static final air_view_video_preview_02:I = 0x7f020028

.field public static final air_view_video_preview_03:I = 0x7f020029

.field public static final allshare_howto_cast_01:I = 0x7f02002a

.field public static final allshare_howto_cast_02:I = 0x7f02002b

.field public static final allshare_howto_cast_03:I = 0x7f02002c

.field public static final allshare_howto_cast_04:I = 0x7f02002d

.field public static final allshare_learn_nfc_01:I = 0x7f02002e

.field public static final allshare_learn_nfc_02:I = 0x7f02002f

.field public static final allshare_learn_nfc_03:I = 0x7f020030

.field public static final assistant_menu_button:I = 0x7f020031

.field public static final bluetooth:I = 0x7f020032

.field public static final booster_quickpanel_ic:I = 0x7f020033

.field public static final booster_setting_ic:I = 0x7f020034

.field public static final button_background_selector:I = 0x7f020035

.field public static final button_next:I = 0x7f020036

.field public static final button_previous:I = 0x7f020037

.field public static final button_selector:I = 0x7f020038

.field public static final button_text_selector:I = 0x7f020039

.field public static final call:I = 0x7f02003a

.field public static final call_menu_during_call:I = 0x7f02003b

.field public static final call_menu_during_call_china:I = 0x7f02003c

.field public static final call_menu_during_call_easy_mode:I = 0x7f02003d

.field public static final call_menu_during_call_easy_mode_china:I = 0x7f02003e

.field public static final call_menu_during_call_kor:I = 0x7f02003f

.field public static final camera:I = 0x7f020040

.field public static final camera_help_changing_setting:I = 0x7f020041

.field public static final camera_help_changing_setting_2nd:I = 0x7f020042

.field public static final camera_introduction:I = 0x7f020043

.field public static final camera_introduction_01:I = 0x7f020044

.field public static final camera_introduction_01_2nd:I = 0x7f020045

.field public static final camera_introduction_2nd:I = 0x7f020046

.field public static final camera_introduction_easymode:I = 0x7f020047

.field public static final camera_introduction_easymode_2nd:I = 0x7f020048

.field public static final carmode_sub_call_02_ic_01:I = 0x7f020049

.field public static final carmode_sub_launch_01_ic_01:I = 0x7f02004a

.field public static final carmode_sub_launch_02_ic_01:I = 0x7f02004b

.field public static final carmode_sub_set_01_ic_01:I = 0x7f02004c

.field public static final carmode_sub_set_01_ic_02:I = 0x7f02004d

.field public static final category_caption:I = 0x7f02004e

.field public static final category_item_selector:I = 0x7f02004f

.field public static final category_list_bottom_selector:I = 0x7f020050

.field public static final category_list_middle_selector:I = 0x7f020051

.field public static final category_list_top_selector:I = 0x7f020052

.field public static final category_new_feature_selector:I = 0x7f020053

.field public static final cocktail_bar_service_help_01:I = 0x7f020054

.field public static final cocktail_bar_service_help_02:I = 0x7f020055

.field public static final cocktail_bar_service_help_04:I = 0x7f020056

.field public static final cocktail_bar_service_help_05:I = 0x7f020057

.field public static final cocktail_bar_service_help_06:I = 0x7f020058

.field public static final cocktail_bar_service_help_using_night_clock_01:I = 0x7f020059

.field public static final cocktail_bar_service_help_waking_up_edge_screen_01:I = 0x7f02005a

.field public static final cocktail_bar_service_help_waking_up_edge_screen_02:I = 0x7f02005b

.field public static final contacts:I = 0x7f02005c

.field public static final contacts_add:I = 0x7f02005d

.field public static final create:I = 0x7f02005e

.field public static final cursor_icon:I = 0x7f02005f

.field public static final cursor_image:I = 0x7f020060

.field public static final cursor_image_no_magnify:I = 0x7f020061

.field public static final easy_help_ic_add:I = 0x7f020062

.field public static final easy_help_ic_contactsadd:I = 0x7f020063

.field public static final easy_help_ic_more:I = 0x7f020064

.field public static final easy_help_navigatehome:I = 0x7f020065

.field public static final easy_help_viewmoreapp:I = 0x7f020066

.field public static final easymode_help_img_addapps:I = 0x7f020067

.field public static final easymode_help_img_addcontacts:I = 0x7f020068

.field public static final easymode_help_img_deleteapps:I = 0x7f020069

.field public static final easymode_help_img_deletecontacts:I = 0x7f02006a

.field public static final edit:I = 0x7f02006b

.field public static final email:I = 0x7f02006c

.field public static final email_sub_compose_01_ic_01:I = 0x7f02006d

.field public static final email_sub_compose_02_ic_01:I = 0x7f02006e

.field public static final email_sub_detail_view_01_ic_01:I = 0x7f02006f

.field public static final email_sub_detail_view_02_ic_01:I = 0x7f020070

.field public static final email_sub_detail_view_02_ic_02:I = 0x7f020071

.field public static final email_sub_detail_view_02_ic_03:I = 0x7f020072

.field public static final email_sub_detail_view_02_ic_04:I = 0x7f020073

.field public static final email_sub_detail_view_02_ic_05:I = 0x7f020074

.field public static final email_sub_folders_01_ic_01:I = 0x7f020075

.field public static final email_sub_folders_01_ic_02:I = 0x7f020076

.field public static final email_sub_launch_01_ic_01:I = 0x7f020077

.field public static final email_sub_list_02_ic_01:I = 0x7f020078

.field public static final email_sub_list_02_ic_02:I = 0x7f020079

.field public static final email_sub_list_02_ic_03:I = 0x7f02007a

.field public static final email_sub_list_02_ic_04:I = 0x7f02007b

.field public static final email_sub_list_02_ic_05:I = 0x7f02007c

.field public static final email_sub_list_02_ic_06:I = 0x7f02007d

.field public static final email_sub_list_02_ic_07:I = 0x7f02007e

.field public static final email_sub_signature_01_ic_01:I = 0x7f02007f

.field public static final expandable_fold_close_btn:I = 0x7f020080

.field public static final expandable_fold_open_btn:I = 0x7f020081

.field public static final expandable_item_selector:I = 0x7f020082

.field public static final expandable_page_fold_close_btn:I = 0x7f020083

.field public static final expandable_page_fold_open_btn:I = 0x7f020084

.field public static final favorite:I = 0x7f020085

.field public static final filling_in_forms_button:I = 0x7f020086

.field public static final galaxy_finder_help_symbol:I = 0x7f020087

.field public static final gallery_contextual_tag:I = 0x7f020088

.field public static final gallery_easyux_help_sharing_img_02:I = 0x7f020089

.field public static final gallery_easyux_help_sharing_img_chn_02:I = 0x7f02008a

.field public static final gallery_easyux_help_viewing_img_01:I = 0x7f02008b

.field public static final gallery_easyux_help_viewing_img_chn_01:I = 0x7f02008c

.field public static final gallery_help_ic_delete:I = 0x7f02008d

.field public static final gallery_help_ic_drawer:I = 0x7f02008e

.field public static final gallery_help_ic_more:I = 0x7f02008f

.field public static final gallery_help_ic_share:I = 0x7f020090

.field public static final gallery_help_resizing_thumbnails_01:I = 0x7f020091

.field public static final gallery_help_resizing_thumbnails_02:I = 0x7f020092

.field public static final gallery_help_sharing_img_01:I = 0x7f020093

.field public static final gallery_help_sharing_img_02:I = 0x7f020094

.field public static final gallery_help_viewing_img_01:I = 0x7f020095

.field public static final gallery_help_viewing_img_02:I = 0x7f020096

.field public static final gesture_browse_01:I = 0x7f020097

.field public static final gesture_browse_02:I = 0x7f020098

.field public static final gesture_browse_03:I = 0x7f020099

.field public static final gesture_browse_04:I = 0x7f02009a

.field public static final gesture_callaccept_01:I = 0x7f02009b

.field public static final gesture_callaccept_02:I = 0x7f02009c

.field public static final gesture_callaccept_03:I = 0x7f02009d

.field public static final gesture_indicator:I = 0x7f02009e

.field public static final gesture_jump_01:I = 0x7f02009f

.field public static final gesture_jump_02:I = 0x7f0200a0

.field public static final gesture_jump_03:I = 0x7f0200a1

.field public static final gesture_jump_04:I = 0x7f0200a2

.field public static final gesture_move_01:I = 0x7f0200a3

.field public static final gesture_move_02:I = 0x7f0200a4

.field public static final gesture_move_03:I = 0x7f0200a5

.field public static final gesture_move_04:I = 0x7f0200a6

.field public static final gesture_move_05:I = 0x7f0200a7

.field public static final gesture_move_06:I = 0x7f0200a8

.field public static final gesture_quick_glance_01:I = 0x7f0200a9

.field public static final gesture_quick_glance_02:I = 0x7f0200aa

.field public static final gesture_quick_glance_03:I = 0x7f0200ab

.field public static final gesture_quick_glance_04:I = 0x7f0200ac

.field public static final gesture_quick_glance_05:I = 0x7f0200ad

.field public static final group_play_introduction:I = 0x7f0200ae

.field public static final groupcast_mobile_help_ap:I = 0x7f0200af

.field public static final help_3rd_depth:I = 0x7f0200b0

.field public static final help_3rd_depth_2nd:I = 0x7f0200b1

.field public static final help_3rd_edge_2nd:I = 0x7f0200b2

.field public static final help_3rd_ic_drag_drop:I = 0x7f0200b3

.field public static final help_accept_04:I = 0x7f0200b4

.field public static final help_accept_05:I = 0x7f0200b5

.field public static final help_action_bar_icon:I = 0x7f0200b6

.field public static final help_bluetooth_icon:I = 0x7f0200b7

.field public static final help_camera_settings:I = 0x7f0200b8

.field public static final help_capture_screen_1:I = 0x7f0200b9

.field public static final help_capture_screen_2:I = 0x7f0200ba

.field public static final help_capture_screen_3:I = 0x7f0200bb

.field public static final help_check_device_list_img_01:I = 0x7f0200bc

.field public static final help_endcall_icon:I = 0x7f0200bd

.field public static final help_expanded_hardkey:I = 0x7f0200be

.field public static final help_extra_volume_icon:I = 0x7f0200bf

.field public static final help_gesture07_01:I = 0x7f0200c0

.field public static final help_gesture07_02:I = 0x7f0200c1

.field public static final help_gesture07_03:I = 0x7f0200c2

.field public static final help_gesture07_04:I = 0x7f0200c3

.field public static final help_gesture07_05:I = 0x7f0200c4

.field public static final help_gesture07_06:I = 0x7f0200c5

.field public static final help_gesture07_07:I = 0x7f0200c6

.field public static final help_glossary_icon_full:I = 0x7f0200c7

.field public static final help_group_sub_title:I = 0x7f0200c8

.field public static final help_ic_camera:I = 0x7f0200c9

.field public static final help_ic_lock:I = 0x7f0200ca

.field public static final help_icon_account:I = 0x7f0200cb

.field public static final help_icon_cancel:I = 0x7f0200cc

.field public static final help_icon_controller:I = 0x7f0200cd

.field public static final help_icon_delta_app:I = 0x7f0200ce

.field public static final help_icon_delta_check:I = 0x7f0200cf

.field public static final help_icon_delta_handler:I = 0x7f0200d0

.field public static final help_icon_delta_minus:I = 0x7f0200d1

.field public static final help_icon_delta_move:I = 0x7f0200d2

.field public static final help_icon_delta_pencil:I = 0x7f0200d3

.field public static final help_icon_delta_quick_contact:I = 0x7f0200d4

.field public static final help_icon_drag_drops:I = 0x7f0200d5

.field public static final help_icon_end_guest:I = 0x7f0200d6

.field public static final help_icon_end_meeting:I = 0x7f0200d7

.field public static final help_icon_end_presentation:I = 0x7f0200d8

.field public static final help_icon_end_presenter:I = 0x7f0200d9

.field public static final help_icon_exercise:I = 0x7f0200da

.field public static final help_icon_exit_meeting:I = 0x7f0200db

.field public static final help_icon_focus_select:I = 0x7f0200dc

.field public static final help_icon_hdr:I = 0x7f0200dd

.field public static final help_icon_heartrate:I = 0x7f0200de

.field public static final help_icon_maximize:I = 0x7f0200df

.field public static final help_icon_menu:I = 0x7f0200e0

.field public static final help_icon_multi_window:I = 0x7f0200e1

.field public static final help_icon_open_whiteboard:I = 0x7f0200e2

.field public static final help_icon_pedometer:I = 0x7f0200e3

.field public static final help_icon_popup_view:I = 0x7f0200e4

.field public static final help_icon_save_meeting_files:I = 0x7f0200e5

.field public static final help_icon_split:I = 0x7f0200e6

.field public static final help_icon_start_presentation:I = 0x7f0200e7

.field public static final help_icon_sub_tray:I = 0x7f0200e8

.field public static final help_icon_switch:I = 0x7f0200e9

.field public static final help_icon_view_the_presenters_screen:I = 0x7f0200ea

.field public static final help_icons_about_device:I = 0x7f0200eb

.field public static final help_icons_accept_call:I = 0x7f0200ec

.field public static final help_icons_applications:I = 0x7f0200ed

.field public static final help_icons_bluetooth:I = 0x7f0200ee

.field public static final help_icons_calendar:I = 0x7f0200ef

.field public static final help_icons_call:I = 0x7f0200f0

.field public static final help_icons_camcoder:I = 0x7f0200f1

.field public static final help_icons_camera:I = 0x7f0200f2

.field public static final help_icons_camera_btn_arrow:I = 0x7f0200f3

.field public static final help_icons_camera_btn_gallery:I = 0x7f0200f4

.field public static final help_icons_camera_btn_switch:I = 0x7f0200f5

.field public static final help_icons_camera_btn_view:I = 0x7f0200f6

.field public static final help_icons_camera_download:I = 0x7f0200f7

.field public static final help_icons_camera_mode:I = 0x7f0200f8

.field public static final help_icons_camera_mode_manage:I = 0x7f0200f9

.field public static final help_icons_camera_more:I = 0x7f0200fa

.field public static final help_icons_camera_settings:I = 0x7f0200fb

.field public static final help_icons_camera_stop:I = 0x7f0200fc

.field public static final help_icons_cancel:I = 0x7f0200fd

.field public static final help_icons_capture:I = 0x7f0200fe

.field public static final help_icons_capture_2nd:I = 0x7f0200ff

.field public static final help_icons_check:I = 0x7f020100

.field public static final help_icons_contacts:I = 0x7f020101

.field public static final help_icons_contacts_create_new:I = 0x7f020102

.field public static final help_icons_dualshot:I = 0x7f020103

.field public static final help_icons_dummy:I = 0x7f020104

.field public static final help_icons_email:I = 0x7f020105

.field public static final help_icons_email_add:I = 0x7f020106

.field public static final help_icons_email_delete:I = 0x7f020107

.field public static final help_icons_email_menu:I = 0x7f020108

.field public static final help_icons_folder:I = 0x7f020109

.field public static final help_icons_gallery:I = 0x7f02010a

.field public static final help_icons_gallery_copy_to_other:I = 0x7f02010b

.field public static final help_icons_gallery_edit:I = 0x7f02010c

.field public static final help_icons_gallery_play:I = 0x7f02010d

.field public static final help_icons_gallery_portrait:I = 0x7f02010e

.field public static final help_icons_gallery_save:I = 0x7f02010f

.field public static final help_icons_graph:I = 0x7f020110

.field public static final help_icons_homescreen_settings:I = 0x7f020111

.field public static final help_icons_indicator_sweep:I = 0x7f020112

.field public static final help_icons_internet:I = 0x7f020113

.field public static final help_icons_internet_icon_add_multiwindow:I = 0x7f020114

.field public static final help_icons_internet_icon_bookmark:I = 0x7f020115

.field public static final help_icons_internet_icon_multiwindow:I = 0x7f020116

.field public static final help_icons_list:I = 0x7f020117

.field public static final help_icons_message:I = 0x7f020118

.field public static final help_icons_message_icon_attach:I = 0x7f020119

.field public static final help_icons_message_new:I = 0x7f02011a

.field public static final help_icons_messaging:I = 0x7f02011b

.field public static final help_icons_more_setting:I = 0x7f02011c

.field public static final help_icons_nfc:I = 0x7f02011d

.field public static final help_icons_nfc_att:I = 0x7f02011e

.field public static final help_icons_phone:I = 0x7f02011f

.field public static final help_icons_phone_icons_addicon:I = 0x7f020120

.field public static final help_icons_photo_reader_add:I = 0x7f020121

.field public static final help_icons_photo_reader_capture_mode:I = 0x7f020122

.field public static final help_icons_photo_reader_detect_text:I = 0x7f020123

.field public static final help_icons_photo_reader_target_image_mode:I = 0x7f020124

.field public static final help_icons_powersaving:I = 0x7f020125

.field public static final help_icons_reference:I = 0x7f020126

.field public static final help_icons_reject_call:I = 0x7f020127

.field public static final help_icons_sbeam:I = 0x7f020128

.field public static final help_icons_security:I = 0x7f020129

.field public static final help_icons_setting:I = 0x7f02012a

.field public static final help_icons_shutter:I = 0x7f02012b

.field public static final help_icons_smemo_drawingmode:I = 0x7f02012c

.field public static final help_icons_smemo_erasermode:I = 0x7f02012d

.field public static final help_icons_smemo_erasersize:I = 0x7f02012e

.field public static final help_icons_smemo_insert:I = 0x7f02012f

.field public static final help_icons_smemo_more:I = 0x7f020130

.field public static final help_icons_smemo_pen_pencil:I = 0x7f020131

.field public static final help_icons_smemo_pencolour:I = 0x7f020132

.field public static final help_icons_smemo_penpreset:I = 0x7f020133

.field public static final help_icons_smemo_pensize:I = 0x7f020134

.field public static final help_icons_smemo_pentype:I = 0x7f020135

.field public static final help_icons_smemo_plus:I = 0x7f020136

.field public static final help_icons_smemo_recordvoice:I = 0x7f020137

.field public static final help_icons_smemo_text:I = 0x7f020138

.field public static final help_icons_sound:I = 0x7f020139

.field public static final help_icons_vtcall:I = 0x7f02013a

.field public static final help_icons_wifi:I = 0x7f02013b

.field public static final help_icons_wifi_on_device_help:I = 0x7f02013c

.field public static final help_im_camera:I = 0x7f02013d

.field public static final help_im_checking_event:I = 0x7f02013e

.field public static final help_image_fingerprint_01:I = 0x7f02013f

.field public static final help_image_fingerprint_02:I = 0x7f020140

.field public static final help_image_fingerprint_03:I = 0x7f020141

.field public static final help_image_focused_holo:I = 0x7f020142

.field public static final help_image_introduction:I = 0x7f020143

.field public static final help_image_meeting_screen:I = 0x7f020144

.field public static final help_image_play_normal:I = 0x7f020145

.field public static final help_image_pressed_holo:I = 0x7f020146

.field public static final help_image_requirements01:I = 0x7f020147

.field public static final help_image_requirements02:I = 0x7f020148

.field public static final help_image_selected_holo:I = 0x7f020149

.field public static final help_image_touchfingerprint_01:I = 0x7f02014a

.field public static final help_image_touchfingerprint_03:I = 0x7f02014b

.field public static final help_img_01:I = 0x7f02014c

.field public static final help_img_02:I = 0x7f02014d

.field public static final help_img_03:I = 0x7f02014e

.field public static final help_img_04:I = 0x7f02014f

.field public static final help_intro:I = 0x7f020150

.field public static final help_key_back_key:I = 0x7f020151

.field public static final help_key_back_key_tb:I = 0x7f020152

.field public static final help_key_device:I = 0x7f020153

.field public static final help_key_home_key:I = 0x7f020154

.field public static final help_key_home_key_tb:I = 0x7f020155

.field public static final help_key_more_option_button:I = 0x7f020156

.field public static final help_key_more_option_button_tb:I = 0x7f020157

.field public static final help_key_power_key:I = 0x7f020158

.field public static final help_key_power_key_tb:I = 0x7f020159

.field public static final help_key_recent_key:I = 0x7f02015a

.field public static final help_key_recent_key_sub:I = 0x7f02015b

.field public static final help_key_recent_key_tb:I = 0x7f02015c

.field public static final help_key_shutter_key:I = 0x7f02015d

.field public static final help_key_volume_key:I = 0x7f02015e

.field public static final help_key_volume_key_tb:I = 0x7f02015f

.field public static final help_list_dot:I = 0x7f020160

.field public static final help_list_group_sub_title:I = 0x7f020161

.field public static final help_main_actionbar_divider:I = 0x7f020162

.field public static final help_main_bg:I = 0x7f020163

.field public static final help_main_bg_1depth:I = 0x7f020164

.field public static final help_main_bg_l:I = 0x7f020165

.field public static final help_main_bg_l_1depth:I = 0x7f020166

.field public static final help_main_bubble:I = 0x7f020167

.field public static final help_main_expander_bg:I = 0x7f020168

.field public static final help_main_expander_bg_bottom:I = 0x7f020169

.field public static final help_main_expander_bg_single:I = 0x7f02016a

.field public static final help_main_expander_bg_top:I = 0x7f02016b

.field public static final help_main_expander_list_divider:I = 0x7f02016c

.field public static final help_main_image_default:I = 0x7f02016d

.field public static final help_main_image_default_h:I = 0x7f02016e

.field public static final help_main_list_icon_01:I = 0x7f02016f

.field public static final help_main_list_icon_02:I = 0x7f020170

.field public static final help_main_list_icon_04:I = 0x7f020171

.field public static final help_main_list_icon_05:I = 0x7f020172

.field public static final help_main_list_icon_06:I = 0x7f020173

.field public static final help_main_list_icon_08:I = 0x7f020174

.field public static final help_main_list_icon_09:I = 0x7f020175

.field public static final help_main_new_features_image_camera:I = 0x7f020176

.field public static final help_main_new_features_image_camera_press:I = 0x7f020177

.field public static final help_main_new_features_image_default:I = 0x7f020178

.field public static final help_main_new_features_image_default_02:I = 0x7f020179

.field public static final help_main_new_features_image_default_02_h:I = 0x7f02017a

.field public static final help_main_new_features_image_default_land:I = 0x7f02017b

.field public static final help_main_new_features_image_default_land_nopadding:I = 0x7f02017c

.field public static final help_main_new_features_image_default_nopadding:I = 0x7f02017d

.field public static final help_main_new_features_image_group_play:I = 0x7f02017e

.field public static final help_main_new_features_image_group_play_press:I = 0x7f02017f

.field public static final help_main_new_features_image_motion:I = 0x7f020180

.field public static final help_main_new_features_image_motion_press:I = 0x7f020181

.field public static final help_main_new_features_more:I = 0x7f020182

.field public static final help_main_new_features_play:I = 0x7f020183

.field public static final help_main_popup:I = 0x7f020184

.field public static final help_main_popup_bottom:I = 0x7f020185

.field public static final help_main_popup_bottom_focus:I = 0x7f020186

.field public static final help_main_popup_bottom_press:I = 0x7f020187

.field public static final help_main_popup_focus:I = 0x7f020188

.field public static final help_main_popup_middle:I = 0x7f020189

.field public static final help_main_popup_middle_focus:I = 0x7f02018a

.field public static final help_main_popup_press:I = 0x7f02018b

.field public static final help_main_popup_top:I = 0x7f02018c

.field public static final help_main_popup_top_focus:I = 0x7f02018d

.field public static final help_main_popup_top_press:I = 0x7f02018e

.field public static final help_more:I = 0x7f02018f

.field public static final help_mute_icon:I = 0x7f020190

.field public static final help_number_icon:I = 0x7f020191

.field public static final help_open_action_memo_1_3:I = 0x7f020192

.field public static final help_open_action_memo_2_4:I = 0x7f020193

.field public static final help_open_action_memo_5:I = 0x7f020194

.field public static final help_popup:I = 0x7f020195

.field public static final help_popup_bottom:I = 0x7f020196

.field public static final help_popup_bottom_focus:I = 0x7f020197

.field public static final help_popup_bottom_press:I = 0x7f020198

.field public static final help_popup_expander_shadow:I = 0x7f020199

.field public static final help_popup_focus:I = 0x7f02019a

.field public static final help_popup_middle:I = 0x7f02019b

.field public static final help_popup_middle_focus:I = 0x7f02019c

.field public static final help_popup_middle_press:I = 0x7f02019d

.field public static final help_popup_picker_b_c:I = 0x7f02019e

.field public static final help_popup_picker_bg_w_01:I = 0x7f02019f

.field public static final help_popup_picker_t_c:I = 0x7f0201a0

.field public static final help_popup_press:I = 0x7f0201a1

.field public static final help_popup_search:I = 0x7f0201a2

.field public static final help_popup_top:I = 0x7f0201a3

.field public static final help_popup_top_focus:I = 0x7f0201a4

.field public static final help_popup_top_press:I = 0x7f0201a5

.field public static final help_power_saving_mode:I = 0x7f0201a6

.field public static final help_power_saving_mode_grayscale:I = 0x7f0201a7

.field public static final help_quickconnect_main_icon:I = 0x7f0201a8

.field public static final help_reject_04:I = 0x7f0201a9

.field public static final help_s_voice_1:I = 0x7f0201aa

.field public static final help_s_voice_2:I = 0x7f0201ab

.field public static final help_select_text_1:I = 0x7f0201ac

.field public static final help_select_text_2:I = 0x7f0201ad

.field public static final help_select_text_3:I = 0x7f0201ae

.field public static final help_smart_select_01:I = 0x7f0201af

.field public static final help_smart_select_02:I = 0x7f0201b0

.field public static final help_speaker_icon:I = 0x7f0201b1

.field public static final help_text_bg:I = 0x7f0201b2

.field public static final help_text_dot:I = 0x7f0201b3

.field public static final help_up_arrow:I = 0x7f0201b4

.field public static final help_widget_icon:I = 0x7f0201b5

.field public static final home:I = 0x7f0201b6

.field public static final home_navigate_home_screens:I = 0x7f0201b7

.field public static final home_view_all_applications:I = 0x7f0201b8

.field public static final homescreen_addwidget:I = 0x7f0201b9

.field public static final homescreen_deleteapp:I = 0x7f0201ba

.field public static final homescreen_folder:I = 0x7f0201bb

.field public static final homescreen_wallpaper:I = 0x7f0201bc

.field public static final ic_help_add_contact:I = 0x7f0201bd

.field public static final ic_help_addapp:I = 0x7f0201be

.field public static final ic_help_back:I = 0x7f0201bf

.field public static final ic_help_delete:I = 0x7f0201c0

.field public static final ic_help_easymode:I = 0x7f0201c1

.field public static final ic_help_fingerprint:I = 0x7f0201c2

.field public static final ic_help_image:I = 0x7f0201c3

.field public static final ic_help_image_h:I = 0x7f0201c4

.field public static final ic_help_list:I = 0x7f0201c5

.field public static final ic_help_more:I = 0x7f0201c6

.field public static final ic_help_moreoptions:I = 0x7f0201c7

.field public static final ic_help_popup_category:I = 0x7f0201c8

.field public static final ic_help_popup_location:I = 0x7f0201c9

.field public static final ic_help_popup_tag:I = 0x7f0201ca

.field public static final ic_help_popup_time:I = 0x7f0201cb

.field public static final ic_help_recent_key:I = 0x7f0201cc

.field public static final ic_help_sbeam:I = 0x7f0201cd

.field public static final ic_help_search:I = 0x7f0201ce

.field public static final ic_help_switch_btn:I = 0x7f0201cf

.field public static final ic_help_switch_btn_kor:I = 0x7f0201d0

.field public static final ic_help_voice:I = 0x7f0201d1

.field public static final ic_menu_althabetically:I = 0x7f0201d2

.field public static final ic_menu_search:I = 0x7f0201d3

.field public static final ic_menu_sort:I = 0x7f0201d4

.field public static final ic_search_favorite_off:I = 0x7f0201d5

.field public static final ic_snote_back:I = 0x7f0201d6

.field public static final ic_snote_create:I = 0x7f0201d7

.field public static final ic_snote_eraser:I = 0x7f0201d8

.field public static final ic_snote_front:I = 0x7f0201d9

.field public static final ic_snote_magnified_notepad:I = 0x7f0201da

.field public static final ic_snote_new:I = 0x7f0201db

.field public static final ic_snote_option:I = 0x7f0201dc

.field public static final ic_snote_selection:I = 0x7f0201dd

.field public static final ic_snote_template:I = 0x7f0201de

.field public static final ic_snote_text:I = 0x7f0201df

.field public static final ic_snote_x:I = 0x7f0201e0

.field public static final icn_add:I = 0x7f0201e1

.field public static final icn_parsing_auto_correction_help:I = 0x7f0201e2

.field public static final icn_parsing_keep_help:I = 0x7f0201e3

.field public static final icn_save_to_scrapbook_help:I = 0x7f0201e4

.field public static final icon_fullscreen_help:I = 0x7f0201e5

.field public static final icon_help_cancle:I = 0x7f0201e6

.field public static final icon_help_edit:I = 0x7f0201e7

.field public static final icon_help_effect:I = 0x7f0201e8

.field public static final icon_help_gallery:I = 0x7f0201e9

.field public static final icon_help_handwriting:I = 0x7f0201ea

.field public static final icon_help_light:I = 0x7f0201eb

.field public static final icon_help_ruler:I = 0x7f0201ec

.field public static final icon_help_stopwatch:I = 0x7f0201ed

.field public static final icon_help_text:I = 0x7f0201ee

.field public static final icon_help_timer:I = 0x7f0201ef

.field public static final icon_help_voice:I = 0x7f0201f0

.field public static final imageviewer_selector:I = 0x7f0201f1

.field public static final internet:I = 0x7f0201f2

.field public static final internet_book_mark:I = 0x7f0201f3

.field public static final internet_help_ic_metaphore:I = 0x7f0201f4

.field public static final internet_help_ic_multiple_windows_02:I = 0x7f0201f5

.field public static final internet_star_off:I = 0x7f0201f6

.field public static final internet_star_on:I = 0x7f0201f7

.field public static final keyboard_change_global_language:I = 0x7f0201f8

.field public static final keyboard_change_input_method:I = 0x7f0201f9

.field public static final keyboard_change_input_method_china:I = 0x7f0201fa

.field public static final keyboard_change_input_mode:I = 0x7f0201fb

.field public static final keyboard_change_input_mode_china:I = 0x7f0201fc

.field public static final keyboard_change_keyboard_type:I = 0x7f0201fd

.field public static final keyboard_change_keyboard_type_floating:I = 0x7f0201fe

.field public static final keyboard_change_keyboard_type_normal:I = 0x7f0201ff

.field public static final keyboard_change_keyboard_type_split:I = 0x7f020200

.field public static final keyboard_change_language:I = 0x7f020201

.field public static final keyboard_changing_keyboard_theme_china:I = 0x7f020202

.field public static final keyboard_clipboard:I = 0x7f020203

.field public static final keyboard_clipboard_china:I = 0x7f020204

.field public static final keyboard_close_keyboard:I = 0x7f020205

.field public static final keyboard_continuous_input:I = 0x7f020206

.field public static final keyboard_edit_text_input_china:I = 0x7f020207

.field public static final keyboard_gesture_01:I = 0x7f020208

.field public static final keyboard_gesture_02:I = 0x7f020209

.field public static final keyboard_gesture_03:I = 0x7f02020a

.field public static final keyboard_gesture_04:I = 0x7f02020b

.field public static final keyboard_gesture_05:I = 0x7f02020c

.field public static final keyboard_gesture_06:I = 0x7f02020d

.field public static final keyboard_gesture_07:I = 0x7f02020e

.field public static final keyboard_gesture_08:I = 0x7f02020f

.field public static final keyboard_gesture_09:I = 0x7f020210

.field public static final keyboard_handwriting:I = 0x7f020211

.field public static final keyboard_handwriting_arrow:I = 0x7f020212

.field public static final keyboard_handwriting_cursive_eng_right:I = 0x7f020213

.field public static final keyboard_handwriting_cursive_kor_left:I = 0x7f020214

.field public static final keyboard_handwriting_cursive_kor_right:I = 0x7f020215

.field public static final keyboard_handwriting_multi_line_left:I = 0x7f020216

.field public static final keyboard_handwriting_multi_line_right:I = 0x7f020217

.field public static final keyboard_handwriting_o:I = 0x7f020218

.field public static final keyboard_handwriting_overwrite_right:I = 0x7f020219

.field public static final keyboard_handwriting_parallel_left:I = 0x7f02021a

.field public static final keyboard_handwriting_parallel_right:I = 0x7f02021b

.field public static final keyboard_handwriting_recognize_left:I = 0x7f02021c

.field public static final keyboard_handwriting_write_next_left:I = 0x7f02021d

.field public static final keyboard_handwriting_write_next_right:I = 0x7f02021e

.field public static final keyboard_handwriting_x:I = 0x7f02021f

.field public static final keyboard_inserting_emoticons_china:I = 0x7f020220

.field public static final keyboard_keyboard_setting:I = 0x7f020221

.field public static final keyboard_keyboard_voice:I = 0x7f020222

.field public static final keyboard_language_and_input:I = 0x7f020223

.field public static final keyboard_language_china:I = 0x7f020224

.field public static final keyboard_number_china:I = 0x7f020225

.field public static final keyboard_open_keyboard:I = 0x7f020226

.field public static final keyboard_setting_china:I = 0x7f020227

.field public static final keyboard_settings:I = 0x7f020228

.field public static final keyboard_settings_edittext:I = 0x7f020229

.field public static final keyboard_switch:I = 0x7f02022a

.field public static final keyboard_symbol_china:I = 0x7f02022b

.field public static final keyboard_temporary_handwriting_china:I = 0x7f02022c

.field public static final keyboard_text_cursor:I = 0x7f02022d

.field public static final keyboard_text_recognition:I = 0x7f02022e

.field public static final keyboard_text_recognition_china:I = 0x7f02022f

.field public static final keyboard_text_select_handle_left:I = 0x7f020230

.field public static final keyboard_text_select_handle_left_right:I = 0x7f020231

.field public static final keyboard_text_select_handle_middle:I = 0x7f020232

.field public static final keyboard_text_select_handle_right:I = 0x7f020233

.field public static final keyboard_text_template_china:I = 0x7f020234

.field public static final keyboard_toolbar_china:I = 0x7f020235

.field public static final keyboard_tutorial_emoji:I = 0x7f020236

.field public static final keyboard_tutorial_hanja:I = 0x7f020237

.field public static final keyboard_voice_input:I = 0x7f020238

.field public static final keyboard_voice_input_china:I = 0x7f020239

.field public static final left_right_scroll_button:I = 0x7f02023a

.field public static final life_times_help_add_moment:I = 0x7f02023b

.field public static final list_contents_child_bg:I = 0x7f02023c

.field public static final list_contents_group_bg:I = 0x7f02023d

.field public static final lock_screen_add_widget:I = 0x7f02023e

.field public static final lock_screen_edit_lock_screen:I = 0x7f02023f

.field public static final lock_screen_expand_widget:I = 0x7f020240

.field public static final lock_screen_navigate_lock_screen:I = 0x7f020241

.field public static final lock_screen_unlock_device:I = 0x7f020242

.field public static final lock_screen_unlock_device2:I = 0x7f020243

.field public static final magazine_ic_arrow:I = 0x7f020244

.field public static final magazine_ic_more:I = 0x7f020245

.field public static final magazine_ic_setting:I = 0x7f020246

.field public static final magazine_im_reordering:I = 0x7f020247

.field public static final magazine_im_viewing:I = 0x7f020248

.field public static final magazine_launcher_help_101:I = 0x7f020249

.field public static final magazine_launcher_help_201:I = 0x7f02024a

.field public static final magazine_launcher_help_202:I = 0x7f02024b

.field public static final magazine_launcher_help_203:I = 0x7f02024c

.field public static final magazine_launcher_help_301:I = 0x7f02024d

.field public static final magazine_launcher_help_302:I = 0x7f02024e

.field public static final magazine_launcher_help_303:I = 0x7f02024f

.field public static final magazine_launcher_help_304:I = 0x7f020250

.field public static final magazine_launcher_help_403:I = 0x7f020251

.field public static final magazine_launcher_help_404:I = 0x7f020252

.field public static final magazine_launcher_help_502:I = 0x7f020253

.field public static final magazine_launcher_help_503:I = 0x7f020254

.field public static final magazine_launcher_help_504:I = 0x7f020255

.field public static final magazine_launcher_help_602:I = 0x7f020256

.field public static final magazine_launcher_help_603:I = 0x7f020257

.field public static final magnifier_button:I = 0x7f020258

.field public static final main_new_feature_selector_land:I = 0x7f020259

.field public static final main_new_feature_selector_portrait:I = 0x7f02025a

.field public static final main_title_default_image:I = 0x7f02025b

.field public static final main_title_default_image_land:I = 0x7f02025c

.field public static final messaging:I = 0x7f02025d

.field public static final messaging_help_attach:I = 0x7f02025e

.field public static final messaging_help_contacts:I = 0x7f02025f

.field public static final messaging_help_new:I = 0x7f020260

.field public static final messaging_help_save:I = 0x7f020261

.field public static final messaging_help_send:I = 0x7f020262

.field public static final messaging_help_send_1:I = 0x7f020263

.field public static final messaging_help_send_2:I = 0x7f020264

.field public static final more_option_button:I = 0x7f020265

.field public static final motion:I = 0x7f020266

.field public static final motion_palm_swipe_capture_01:I = 0x7f020267

.field public static final motion_palm_swipe_capture_02:I = 0x7f020268

.field public static final motion_palm_swipe_capture_03:I = 0x7f020269

.field public static final motion_palm_swipe_capture_04:I = 0x7f02026a

.field public static final motion_palm_swipe_mute_01:I = 0x7f02026b

.field public static final motion_palm_swipe_mute_02:I = 0x7f02026c

.field public static final motion_palm_swipe_mute_03:I = 0x7f02026d

.field public static final motion_pan_to_browse_01:I = 0x7f02026e

.field public static final motion_pan_to_browse_02:I = 0x7f02026f

.field public static final motion_pan_to_browse_03:I = 0x7f020270

.field public static final motion_pan_to_browse_04:I = 0x7f020271

.field public static final motion_pan_to_browse_05:I = 0x7f020272

.field public static final motion_pan_to_browse_06:I = 0x7f020273

.field public static final motion_pan_to_browse_07:I = 0x7f020274

.field public static final motion_pan_to_browse_08:I = 0x7f020275

.field public static final motion_pan_to_move_icon_01:I = 0x7f020276

.field public static final motion_pan_to_move_icon_02:I = 0x7f020277

.field public static final motion_pan_to_move_icon_03:I = 0x7f020278

.field public static final motion_pan_to_move_icon_04:I = 0x7f020279

.field public static final motion_pick_up_direct_call_01:I = 0x7f02027a

.field public static final motion_pick_up_direct_call_02:I = 0x7f02027b

.field public static final motion_pick_up_direct_call_03:I = 0x7f02027c

.field public static final motion_pick_up_smart_alert_01:I = 0x7f02027d

.field public static final motion_pick_up_smart_alert_02:I = 0x7f02027e

.field public static final motion_pick_up_smart_alert_03:I = 0x7f02027f

.field public static final motion_quick_glance01:I = 0x7f020280

.field public static final motion_quick_glance02:I = 0x7f020281

.field public static final motion_quick_glance03:I = 0x7f020282

.field public static final motion_shake_01:I = 0x7f020283

.field public static final motion_shake_02:I = 0x7f020284

.field public static final motion_shake_03:I = 0x7f020285

.field public static final motion_shake_04:I = 0x7f020286

.field public static final motion_tile_to_zoom_01:I = 0x7f020287

.field public static final motion_tile_to_zoom_02:I = 0x7f020288

.field public static final motion_tile_to_zoom_03:I = 0x7f020289

.field public static final motion_tile_to_zoom_04:I = 0x7f02028a

.field public static final motion_turnover_01:I = 0x7f02028b

.field public static final motion_turnover_02:I = 0x7f02028c

.field public static final motion_turnover_03:I = 0x7f02028d

.field public static final motion_view_album_list_01:I = 0x7f02028e

.field public static final motion_view_album_list_02:I = 0x7f02028f

.field public static final motion_view_album_list_03:I = 0x7f020290

.field public static final motion_view_album_list_04:I = 0x7f020291

.field public static final move_button:I = 0x7f020292

.field public static final multi_window_drag_and_drop:I = 0x7f020293

.field public static final multi_window_edit_items:I = 0x7f020294

.field public static final multi_window_edit_items_one:I = 0x7f020295

.field public static final multi_window_edit_items_pocket_open_btn:I = 0x7f020296

.field public static final multi_window_multi_instance:I = 0x7f020297

.field public static final multi_window_pocket_create:I = 0x7f020298

.field public static final multi_window_show_hide_tray:I = 0x7f020299

.field public static final multi_window_show_hide_tray_one:I = 0x7f02029a

.field public static final multi_window_tray:I = 0x7f02029b

.field public static final multi_window_tray_one:I = 0x7f02029c

.field public static final multi_window_view:I = 0x7f02029d

.field public static final multi_window_view_5:I = 0x7f02029e

.field public static final multi_window_view_non_app:I = 0x7f02029f

.field public static final multi_window_view_non_app_drag:I = 0x7f0202a0

.field public static final multi_window_view_non_app_drag_full:I = 0x7f0202a1

.field public static final multi_window_view_non_app_full:I = 0x7f0202a2

.field public static final multi_window_view_non_drag:I = 0x7f0202a3

.field public static final multi_window_view_non_drag_full:I = 0x7f0202a4

.field public static final multi_window_view_non_full:I = 0x7f0202a5

.field public static final multi_window_view_one:I = 0x7f0202a6

.field public static final multiwindow_app_switching_help:I = 0x7f0202a7

.field public static final multiwindow_help_image_mtrl_06:I = 0x7f0202a8

.field public static final multiwindow_help_img_01:I = 0x7f0202a9

.field public static final multiwindow_help_img_01_chn:I = 0x7f0202aa

.field public static final multiwindow_help_img_02:I = 0x7f0202ab

.field public static final multiwindow_help_img_04:I = 0x7f0202ac

.field public static final multiwindow_help_tray_drop_box_done_icon:I = 0x7f0202ad

.field public static final multiwindow_help_tray_edit_icon:I = 0x7f0202ae

.field public static final multiwindow_tray_app_switching_icon:I = 0x7f0202af

.field public static final multiwindow_tray_dragmode_icon:I = 0x7f0202b0

.field public static final multiwindow_try_bezel_tray_close:I = 0x7f0202b1

.field public static final multiwindow_try_bezel_tray_dragmode:I = 0x7f0202b2

.field public static final multiwindow_try_bezel_tray_fullscreen:I = 0x7f0202b3

.field public static final multiwindow_try_bezel_tray_recents:I = 0x7f0202b4

.field public static final multiwindow_try_bezel_tray_switch_window:I = 0x7f0202b5

.field public static final mux_drawer_ic:I = 0x7f0202b6

.field public static final mux_setting_ic:I = 0x7f0202b7

.field public static final new_feature_selector_camera:I = 0x7f0202b8

.field public static final new_feature_selector_motion:I = 0x7f0202b9

.field public static final new_feature_selector_play:I = 0x7f0202ba

.field public static final nfc_payment:I = 0x7f0202bb

.field public static final notification:I = 0x7f0202bc

.field public static final notification_access_the_notification_panel_01:I = 0x7f0202bd

.field public static final notification_access_the_notification_panel_02:I = 0x7f0202be

.field public static final notification_use_expandable_notification:I = 0x7f0202bf

.field public static final notification_use_the_notification_panel_01:I = 0x7f0202c0

.field public static final notification_use_the_notification_panel_02:I = 0x7f0202c1

.field public static final notification_use_the_notification_panel_03:I = 0x7f0202c2

.field public static final one_handed_operation_02:I = 0x7f0202c3

.field public static final optionmenu_icon_menu:I = 0x7f0202c4

.field public static final optionmenu_icon_multi_windows:I = 0x7f0202c5

.field public static final pen_select_help_01:I = 0x7f0202c6

.field public static final pen_select_help_02:I = 0x7f0202c7

.field public static final pen_select_help_03:I = 0x7f0202c8

.field public static final phone:I = 0x7f0202c9

.field public static final phone_help_app:I = 0x7f0202ca

.field public static final phone_help_call_img:I = 0x7f0202cb

.field public static final phone_help_video_call:I = 0x7f0202cc

.field public static final phone_help_voice_call:I = 0x7f0202cd

.field public static final play_button:I = 0x7f0202ce

.field public static final powersaving:I = 0x7f0202cf

.field public static final privatemode_setting_add:I = 0x7f0202d0

.field public static final privatemode_setting_remove:I = 0x7f0202d1

.field public static final quickmemo_help_icon_brows:I = 0x7f0202d2

.field public static final quickmemo_help_icon_contact:I = 0x7f0202d3

.field public static final quickmemo_help_icon_email:I = 0x7f0202d4

.field public static final quickmemo_help_icon_gps:I = 0x7f0202d5

.field public static final quickmemo_help_icon_message:I = 0x7f0202d6

.field public static final quickmemo_help_icon_phone:I = 0x7f0202d7

.field public static final quickmemo_help_icon_task:I = 0x7f0202d8

.field public static final rec_button:I = 0x7f0202d9

.field public static final removing_items:I = 0x7f0202da

.field public static final ringtone:I = 0x7f0202db

.field public static final ripple_effect01:I = 0x7f0202dc

.field public static final ripple_effect02:I = 0x7f0202dd

.field public static final ripple_effect03:I = 0x7f0202de

.field public static final s_browser_multi_windows:I = 0x7f0202df

.field public static final s_health_help_device:I = 0x7f0202e0

.field public static final s_health_help_icon_add:I = 0x7f0202e1

.field public static final s_health_help_icon_exercise:I = 0x7f0202e2

.field public static final s_health_help_icon_heartrate:I = 0x7f0202e3

.field public static final s_health_help_icon_menu:I = 0x7f0202e4

.field public static final s_health_help_icon_pedometer:I = 0x7f0202e5

.field public static final s_health_help_icon_plus:I = 0x7f0202e6

.field public static final s_health_help_image_about_shealth:I = 0x7f0202e7

.field public static final s_health_help_imgae_blood_glucose:I = 0x7f0202e8

.field public static final s_health_help_imgae_blood_pressure:I = 0x7f0202e9

.field public static final s_health_help_imgae_comfort_zone:I = 0x7f0202ea

.field public static final s_health_help_imgae_exercise_mate:I = 0x7f0202eb

.field public static final s_health_help_imgae_food_tracker:I = 0x7f0202ec

.field public static final s_health_help_imgae_healthboard:I = 0x7f0202ed

.field public static final s_health_help_imgae_news:I = 0x7f0202ee

.field public static final s_health_help_imgae_running_mate:I = 0x7f0202ef

.field public static final s_health_help_imgae_sleep_monitor:I = 0x7f0202f0

.field public static final s_health_help_imgae_walk_mate:I = 0x7f0202f1

.field public static final s_health_help_imgae_weight:I = 0x7f0202f2

.field public static final s_health_help_text_dot:I = 0x7f0202f3

.field public static final safety_emergency_mode:I = 0x7f0202f4

.field public static final safety_geo_news:I = 0x7f0202f5

.field public static final safety_geo_news_att:I = 0x7f0202f6

.field public static final safety_help_add:I = 0x7f0202f7

.field public static final safety_help_alarm:I = 0x7f0202f8

.field public static final safety_help_icons_app:I = 0x7f0202f9

.field public static final safety_help_icons_app_green:I = 0x7f0202fa

.field public static final safety_help_onoff:I = 0x7f0202fb

.field public static final safety_help_sms:I = 0x7f0202fc

.field public static final safety_help_sms_att:I = 0x7f0202fd

.field public static final safety_help_torch:I = 0x7f0202fe

.field public static final safety_send_message_01:I = 0x7f0202ff

.field public static final safety_send_message_02:I = 0x7f020300

.field public static final safety_send_message_triggerkeyup_01:I = 0x7f020301

.field public static final safety_send_message_triggerkeyup_02:I = 0x7f020302

.field public static final sconnect_help_icon_down:I = 0x7f020303

.field public static final scrapbook_icn_category_help:I = 0x7f020304

.field public static final scrapbook_icn_eraser_help:I = 0x7f020305

.field public static final scrapbook_icn_highlight_color_help:I = 0x7f020306

.field public static final scrapbook_icn_highlight_help:I = 0x7f020307

.field public static final scrapbook_icn_share_help:I = 0x7f020308

.field public static final scrapbook_img_help_pen:I = 0x7f020309

.field public static final scrapbook_img_help_screen:I = 0x7f02030a

.field public static final section_grid_selector:I = 0x7f02030b

.field public static final section_item_selector:I = 0x7f02030c

.field public static final section_list_child_bottom_selector:I = 0x7f02030d

.field public static final section_list_child_middle_selector:I = 0x7f02030e

.field public static final section_list_child_selector:I = 0x7f02030f

.field public static final section_list_child_selector_bottom:I = 0x7f020310

.field public static final section_list_child_selector_single:I = 0x7f020311

.field public static final section_list_child_selector_top:I = 0x7f020312

.field public static final section_list_group_selector_collapsed:I = 0x7f020313

.field public static final section_list_group_selector_expanded:I = 0x7f020314

.field public static final section_list_selector:I = 0x7f020315

.field public static final setting_lower_power_home_image_wo_mdnie:I = 0x7f020316

.field public static final setting_safety_geoinfo_preview:I = 0x7f020317

.field public static final sfinder_help_fx:I = 0x7f020318

.field public static final shape_tip:I = 0x7f020319

.field public static final show_or_hide:I = 0x7f02031a

.field public static final smart_screen_introduction_01:I = 0x7f02031b

.field public static final smart_screen_introduction_02:I = 0x7f02031c

.field public static final smart_screen_introduction_03:I = 0x7f02031d

.field public static final smart_screen_introduction_04:I = 0x7f02031e

.field public static final smart_screen_introduction_05:I = 0x7f02031f

.field public static final smart_screen_introduction_06:I = 0x7f020320

.field public static final smart_screen_introduction_07:I = 0x7f020321

.field public static final smart_screen_introduction_08:I = 0x7f020322

.field public static final smart_screen_smart_rotation_01:I = 0x7f020323

.field public static final smart_screen_smart_rotation_02:I = 0x7f020324

.field public static final smart_screen_smart_rotation_03:I = 0x7f020325

.field public static final smart_screen_smart_rotation_04:I = 0x7f020326

.field public static final smart_screen_smart_stay_01:I = 0x7f020327

.field public static final smart_screen_smart_stay_02:I = 0x7f020328

.field public static final smart_screen_smart_stay_03:I = 0x7f020329

.field public static final smart_screen_smart_stay_04:I = 0x7f02032a

.field public static final smart_screen_smart_stay_eye:I = 0x7f02032b

.field public static final snote_formula_match:I = 0x7f02032c

.field public static final snote_handwriting_to_text:I = 0x7f02032d

.field public static final snote_help_action_memo_help_ic_attach:I = 0x7f02032e

.field public static final snote_help_action_memo_help_ic_continue_to_snote:I = 0x7f02032f

.field public static final snote_help_actionmemo_1:I = 0x7f020330

.field public static final snote_help_actionmemo_2:I = 0x7f020331

.field public static final snote_help_ic_addpreset:I = 0x7f020332

.field public static final snote_help_ic_automode:I = 0x7f020333

.field public static final snote_help_ic_camera:I = 0x7f020334

.field public static final snote_help_ic_preset:I = 0x7f020335

.field public static final snote_help_ic_snapnote_effect:I = 0x7f020336

.field public static final snote_help_ic_snapnote_selection:I = 0x7f020337

.field public static final snote_help_screen_magnified_note:I = 0x7f020338

.field public static final snote_help_screen_recent:I = 0x7f020339

.field public static final snote_help_screen_toolbar:I = 0x7f02033a

.field public static final snote_help_screen_toolbar_bended:I = 0x7f02033b

.field public static final snote_help_screen_usingpen_01:I = 0x7f02033c

.field public static final snote_help_screen_usingpen_01_bended:I = 0x7f02033d

.field public static final snote_help_screen_usingpen_02:I = 0x7f02033e

.field public static final snote_help_screen_usingpen_03:I = 0x7f02033f

.field public static final snote_help_snap_note_01:I = 0x7f020340

.field public static final snote_help_snap_note_02:I = 0x7f020341

.field public static final snote_productivity_tools:I = 0x7f020342

.field public static final snote_toolbar_hide_handler:I = 0x7f020343

.field public static final snote_zoom_and_pan:I = 0x7f020344

.field public static final sort_and_search_background_selector:I = 0x7f020345

.field public static final sort_and_search_background_selector_ripple:I = 0x7f020346

.field public static final story_album_edit_memo:I = 0x7f020347

.field public static final story_album_introduction_01:I = 0x7f020348

.field public static final story_album_introduction_02:I = 0x7f020349

.field public static final story_album_view_similar_pic:I = 0x7f02034a

.field public static final svoice_access_svoice:I = 0x7f02034b

.field public static final svoice_edit_what_you_said:I = 0x7f02034c

.field public static final svoice_use_svoice:I = 0x7f02034d

.field public static final svoice_use_svoice_1:I = 0x7f02034e

.field public static final svoice_use_svoice_2:I = 0x7f02034f

.field public static final svoice_use_svoice_3:I = 0x7f020350

.field public static final tablet_help_case_1_01:I = 0x7f020351

.field public static final tablet_help_case_1_02:I = 0x7f020352

.field public static final tablet_help_case_1_03:I = 0x7f020353

.field public static final tablet_help_case_1_04:I = 0x7f020354

.field public static final tethering_icon:I = 0x7f020355

.field public static final try_multi_window_view_help_app_switch:I = 0x7f020356

.field public static final try_multi_window_view_help_close:I = 0x7f020357

.field public static final try_multi_window_view_help_drag_drops:I = 0x7f020358

.field public static final try_multi_window_view_help_fullscreen:I = 0x7f020359

.field public static final try_multi_window_view_help_switch:I = 0x7f02035a

.field public static final tutorial_emotion_keypad:I = 0x7f02035b

.field public static final tutorial_handler:I = 0x7f02035c

.field public static final tutorial_handler_grey:I = 0x7f02035d

.field public static final tutorial_handler_l:I = 0x7f02035e

.field public static final tutorial_handler_pressed:I = 0x7f02035f

.field public static final tutorial_handler_pressed_l:I = 0x7f020360

.field public static final tutorial_home:I = 0x7f020361

.field public static final tutorial_home_l:I = 0x7f020362

.field public static final tutorial_noti_close:I = 0x7f020363

.field public static final tutorial_noti_close_l:I = 0x7f020364

.field public static final tutorial_noti_empty:I = 0x7f020365

.field public static final tutorial_noti_open:I = 0x7f020366

.field public static final tutorial_noti_open_l:I = 0x7f020367

.field public static final tutorial_notification_bottom_line:I = 0x7f020368

.field public static final tutorial_open_keypad:I = 0x7f020369

.field public static final tutorial_quick_panel:I = 0x7f02036a

.field public static final tutorial_quick_panel_l:I = 0x7f02036b

.field public static final tutorial_smart_pause_01:I = 0x7f02036c

.field public static final tutorial_smart_pause_02:I = 0x7f02036d

.field public static final tutorial_smart_scroll_01:I = 0x7f02036e

.field public static final tutorial_smart_scroll_02:I = 0x7f02036f

.field public static final tutorial_smart_scroll_03:I = 0x7f020370

.field public static final tutorial_smart_scroll_04:I = 0x7f020371

.field public static final tutorial_smart_scroll_05:I = 0x7f020372

.field public static final tutorial_smart_scroll_06:I = 0x7f020373

.field public static final tutorial_smart_scroll_face_orientation_01:I = 0x7f020374

.field public static final tutorial_smart_scroll_face_orientation_02:I = 0x7f020375

.field public static final tutorial_smart_scroll_face_orientation_03:I = 0x7f020376

.field public static final tutorial_smart_scroll_face_orientation_04:I = 0x7f020377

.field public static final tutorial_smart_scroll_face_orientation_05:I = 0x7f020378

.field public static final tutorial_smart_scroll_face_orientation_06:I = 0x7f020379

.field public static final tutorial_start_now_01:I = 0x7f02037a

.field public static final tutorial_start_now_02:I = 0x7f02037b

.field public static final tutorial_start_now_03:I = 0x7f02037c

.field public static final tutorial_start_now_04:I = 0x7f02037d

.field public static final tw_ab_bottom_transparent_light_holo:I = 0x7f02037e

.field public static final tw_action_bar_icon_list_disabled_holo_light:I = 0x7f02037f

.field public static final tw_action_bar_icon_list_holo_light:I = 0x7f020380

.field public static final tw_action_bar_icon_search_disable_holo_light:I = 0x7f020381

.field public static final tw_action_bar_icon_search_holo_light:I = 0x7f020382

.field public static final tw_action_bar_icon_search_holo_light_disabled:I = 0x7f020383

.field public static final tw_action_bar_icon_search_holo_light_press:I = 0x7f020384

.field public static final tw_action_bar_icon_sort_alphabetically_disable_holo_light:I = 0x7f020385

.field public static final tw_action_bar_icon_sort_alphabetically_holo_light:I = 0x7f020386

.field public static final tw_action_bar_icon_sort_alphabetically_holo_light_disabled:I = 0x7f020387

.field public static final tw_action_bar_icon_sort_alphabetically_holo_light_press:I = 0x7f020388

.field public static final tw_action_item_background_focused_holo_light:I = 0x7f020389

.field public static final tw_action_item_background_pressed_holo_light:I = 0x7f02038a

.field public static final tw_action_item_background_selected_holo_light:I = 0x7f02038b

.field public static final tw_btn_default_focused_holo:I = 0x7f02038c

.field public static final tw_btn_default_focused_holo_light:I = 0x7f02038d

.field public static final tw_btn_default_normal_holo:I = 0x7f02038e

.field public static final tw_btn_default_normal_holo_light:I = 0x7f02038f

.field public static final tw_btn_default_pressed_holo:I = 0x7f020390

.field public static final tw_btn_default_pressed_holo_light:I = 0x7f020391

.field public static final tw_btn_expander_max_holo_light:I = 0x7f020392

.field public static final tw_btn_expander_max_pressed_holo_light:I = 0x7f020393

.field public static final tw_btn_expander_min_holo_light:I = 0x7f020394

.field public static final tw_btn_expander_min_pressed_holo_light:I = 0x7f020395

.field public static final tw_btn_icon_next_holo_light:I = 0x7f020396

.field public static final tw_btn_icon_previous_holo_light:I = 0x7f020397

.field public static final tw_btn_next_default_holo_light:I = 0x7f020398

.field public static final tw_btn_next_disabled_holo_light:I = 0x7f020399

.field public static final tw_btn_next_focused_holo_light:I = 0x7f02039a

.field public static final tw_btn_next_normal_holo_light:I = 0x7f02039b

.field public static final tw_btn_next_pressed_holo_light:I = 0x7f02039c

.field public static final tw_btn_next_selected_holo_light:I = 0x7f02039d

.field public static final tw_btn_previous_default_holo_light:I = 0x7f02039e

.field public static final tw_btn_previous_disabled_holo_light:I = 0x7f02039f

.field public static final tw_btn_previous_focused_holo_light:I = 0x7f0203a0

.field public static final tw_btn_previous_normal_holo_light:I = 0x7f0203a1

.field public static final tw_btn_previous_pressed_holo_light:I = 0x7f0203a2

.field public static final tw_btn_previous_selected_holo_light:I = 0x7f0203a3

.field public static final tw_buttonbarbutton_selector_default_holo_dark:I = 0x7f0203a4

.field public static final tw_buttonbarbutton_selector_disabled_holo_dark:I = 0x7f0203a5

.field public static final tw_buttonbarbutton_selector_focused_holo_dark:I = 0x7f0203a6

.field public static final tw_buttonbarbutton_selector_pressed_holo_dark:I = 0x7f0203a7

.field public static final tw_buttonbarbutton_selector_selected_holo_dark:I = 0x7f0203a8

.field public static final tw_dialog_middle_holo_light:I = 0x7f0203a9

.field public static final tw_dialog_top_medium_holo_light:I = 0x7f0203aa

.field public static final tw_expandable_list_bg_holo_dark:I = 0x7f0203ab

.field public static final tw_expandable_list_bg_holo_light:I = 0x7f0203ac

.field public static final tw_expandable_list_left_focused_holo_light:I = 0x7f0203ad

.field public static final tw_expandable_list_left_holo_light:I = 0x7f0203ae

.field public static final tw_expandable_list_left_pressed_holo_light:I = 0x7f0203af

.field public static final tw_expandable_sub_list_left_focused_holo_light:I = 0x7f0203b0

.field public static final tw_expandable_sub_list_left_holo_light:I = 0x7f0203b1

.field public static final tw_expander_close_holo_light:I = 0x7f0203b2

.field public static final tw_expander_close_pressed_holo_light:I = 0x7f0203b3

.field public static final tw_expander_closed_holo_light:I = 0x7f0203b4

.field public static final tw_expander_closed_pressed_holo_light:I = 0x7f0203b5

.field public static final tw_expander_list_focused_holo_light:I = 0x7f0203b6

.field public static final tw_expander_open_holo_light:I = 0x7f0203b7

.field public static final tw_expander_open_pressed_holo_light:I = 0x7f0203b8

.field public static final tw_list_divider_holo_light:I = 0x7f0203b9

.field public static final tw_list_focused_holo:I = 0x7f0203ba

.field public static final tw_list_focused_holo_light:I = 0x7f0203bb

.field public static final tw_list_pressed_holo_light:I = 0x7f0203bc

.field public static final tw_list_section_divider_focused_holo_light:I = 0x7f0203bd

.field public static final tw_list_section_divider_holo:I = 0x7f0203be

.field public static final tw_list_section_divider_pressed_holo_light:I = 0x7f0203bf

.field public static final tw_list_selected_holo_light:I = 0x7f0203c0

.field public static final tw_popup_button_background:I = 0x7f0203c1

.field public static final tw_preference_contents_bg_holo_light:I = 0x7f0203c2

.field public static final tw_preference_contents_list_arrow_holo_light:I = 0x7f0203c3

.field public static final tw_preference_contents_list_arrow_pressed_holo_light:I = 0x7f0203c4

.field public static final tw_preference_contents_list_divider_holo_light:I = 0x7f0203c5

.field public static final tw_preference_contents_list_no_arrow_left_holo_light:I = 0x7f0203c6

.field public static final tw_preference_contents_list_scrollbar_handle_holo_light:I = 0x7f0203c7

.field public static final tw_preference_header_item_background_holo_light:I = 0x7f0203c8

.field public static final tw_preference_header_list_arrow_focused_holo_light:I = 0x7f0203c9

.field public static final tw_preference_header_list_arrow_pressed_holo_light:I = 0x7f0203ca

.field public static final tw_primary_text_holo:I = 0x7f0203cb

.field public static final tw_secondary_text_holo:I = 0x7f0203cc

.field public static final tw_sub_header_bg_dark:I = 0x7f0203cd

.field public static final unlock:I = 0x7f0203ce

.field public static final up_down_scroll_button:I = 0x7f0203cf

.field public static final wifi:I = 0x7f0203d0

.field public static final wifi_direct_connect_help_01:I = 0x7f0203d1

.field public static final wifi_direct_connect_help_02:I = 0x7f0203d2

.field public static final wifi_direct_connect_help_03:I = 0x7f0203d3

.field public static final wifi_direct_connect_help_04:I = 0x7f0203d4

.field public static final wifi_direct_connect_help_05:I = 0x7f0203d5

.field public static final wifi_direct_connect_help_06:I = 0x7f0203d6

.field public static final writing_buddy_expand_mode:I = 0x7f0203d7

.field public static final writing_buddy_filling_in_forms:I = 0x7f0203d8

.field public static final writing_buddy_gesture_guide01:I = 0x7f0203d9

.field public static final writing_buddy_gesture_guide02:I = 0x7f0203da

.field public static final writing_buddy_gesture_guide03:I = 0x7f0203db

.field public static final writing_buddy_gesture_guide04:I = 0x7f0203dc

.field public static final writing_buddy_gesture_guide05:I = 0x7f0203dd

.field public static final writing_buddy_gesture_guide06:I = 0x7f0203de

.field public static final writing_buddy_gesture_guide07:I = 0x7f0203df

.field public static final writing_buddy_gesture_guide08:I = 0x7f0203e0

.field public static final writing_buddy_writing_on_applications:I = 0x7f0203e1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
