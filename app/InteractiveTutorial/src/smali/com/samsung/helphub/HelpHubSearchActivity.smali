.class public final Lcom/samsung/helphub/HelpHubSearchActivity;
.super Lcom/samsung/helphub/HelpHubActivityBase;
.source "HelpHubSearchActivity.java"


# static fields
.field private static bFromSearchProvider:Z

.field private static bIsFirstFragment:Z


# instance fields
.field private mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/helphub/HelpHubSearchActivity;->bFromSearchProvider:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/helphub/HelpHubActivityBase;-><init>()V

    return-void
.end method

.method private excludedExpandableHeader(Lcom/samsung/helphub/headers/HelpHeader;)Z
    .locals 2
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 153
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    const v1, 0x7f0d0103

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    const v1, 0x7f0d0102

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    const v1, 0x7f0d0104

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleIntent(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const v8, 0x7f0d0004

    const/4 v7, 0x1

    .line 65
    const-string v5, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 66
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/samsung/helphub/HelpHubSearchActivity;->queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v3

    .line 67
    .local v3, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v3, :cond_0

    .line 68
    sput-boolean v7, Lcom/samsung/helphub/HelpHubSearchActivity;->bFromSearchProvider:Z

    .line 69
    invoke-direct {p0, v3}, Lcom/samsung/helphub/HelpHubSearchActivity;->launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 92
    .end local v3    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    const-string v5, "android.intent.action.SEARCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 72
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v7}, Landroid/os/Bundle;-><init>(I)V

    .line 73
    .local v0, "args":Landroid/os/Bundle;
    const-string v5, "search_list_arg:query"

    const-string v6, "query"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v5, "com.samsung.helphub.fragments.HelpSearchList"

    invoke-static {p0, v5, v0}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v2

    .line 77
    .local v2, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v5

    const-string v6, "main"

    invoke-virtual {v5, v8, v2, v6}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 81
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v2    # "fragment":Landroid/app/Fragment;
    :cond_2
    const/4 v1, 0x0

    .line 82
    .local v1, "destination":Ljava/lang/String;
    const/4 v5, 0x0

    sput-boolean v5, Lcom/samsung/helphub/HelpHubSearchActivity;->bFromSearchProvider:Z

    .line 84
    const-class v5, Lcom/samsung/helphub/fragments/SearchResultViewList;

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    .line 86
    invoke-static {p0, v1}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 87
    .restart local v2    # "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 89
    .local v4, "transaction":Landroid/app/FragmentTransaction;
    const-string v5, "main"

    invoke-virtual {v4, v8, v2, v5}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 90
    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method private launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 6
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 103
    new-instance v0, Landroid/os/Bundle;

    const/4 v4, 0x2

    invoke-direct {v0, v4}, Landroid/os/Bundle;-><init>(I)V

    .line 107
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v4

    sget-object v5, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v4, v5, :cond_2

    invoke-direct {p0, p1}, Lcom/samsung/helphub/HelpHubSearchActivity;->excludedExpandableHeader(Lcom/samsung/helphub/headers/HelpHeader;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 109
    const-string v4, "section_list:expanded_section_name"

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://com.samsung.helphub.provider.search/header/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getOwnerResId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/helphub/HelpHubSearchActivity;->queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object p1

    .line 125
    :cond_0
    :goto_0
    const-string v4, "helpub:header"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 126
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 127
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->isVideoPath()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 128
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p0}, Lcom/samsung/helphub/HelpHubCommon;->startVideo(Ljava/lang/String;Landroid/content/Context;)V

    .line 147
    :cond_1
    :goto_1
    return-void

    .line 115
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v4

    sget-object v5, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v4, v5, :cond_0

    .line 116
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://com.samsung.helphub.provider.search/header/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getOwnerResId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/helphub/HelpHubSearchActivity;->queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v2

    .line 120
    .local v2, "owner":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-direct {p0, v2}, Lcom/samsung/helphub/HelpHubSearchActivity;->launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 122
    const-string v4, "page_fragment_argument:activity_title"

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 132
    .end local v2    # "owner":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v0}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v1

    .line 133
    .local v1, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 138
    .local v3, "transaction":Landroid/app/FragmentTransaction;
    const v4, 0x7f0d0004

    invoke-virtual {v3, v4, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 139
    sget-boolean v4, Lcom/samsung/helphub/HelpHubSearchActivity;->bIsFirstFragment:Z

    if-nez v4, :cond_4

    .line 141
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 143
    :cond_4
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 144
    const/4 v4, 0x0

    sput-boolean v4, Lcom/samsung/helphub/HelpHubSearchActivity;->bIsFirstFragment:Z

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x100

    const/16 v3, 0x400

    .line 38
    invoke-super {p0, p1}, Lcom/samsung/helphub/HelpHubActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const/4 v2, 0x1

    sput-boolean v2, Lcom/samsung/helphub/HelpHubSearchActivity;->bIsFirstFragment:Z

    .line 40
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v5}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 43
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 46
    :cond_0
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v5}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 47
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 49
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/Window;->clearFlags(I)V

    .line 50
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 51
    .local v0, "HelpActionBar":Landroid/app/ActionBar;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 53
    .end local v0    # "HelpActionBar":Landroid/app/ActionBar;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 55
    .local v1, "intent":Landroid/content/Intent;
    if-nez p1, :cond_2

    .line 56
    if-eqz v1, :cond_2

    .line 57
    invoke-direct {p0, v1}, Lcom/samsung/helphub/HelpHubSearchActivity;->handleIntent(Landroid/content/Intent;)V

    .line 61
    :cond_2
    const-string v2, "input_method"

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/HelpHubSearchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    iput-object v2, p0, Lcom/samsung/helphub/HelpHubSearchActivity;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 62
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 221
    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    .line 223
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    .line 224
    const/4 v0, 0x1

    .line 226
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/samsung/helphub/HelpHubActivityBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 231
    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    .line 232
    const/4 v0, 0x1

    .line 234
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/samsung/helphub/HelpHubActivityBase;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x4

    const/4 v0, 0x1

    .line 206
    const/16 v1, 0x52

    if-ne p1, v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 216
    :goto_0
    return v0

    .line 208
    :cond_0
    if-ne p1, v2, :cond_1

    sget-boolean v1, Lcom/samsung/helphub/HelpHubSearchActivity;->bFromSearchProvider:Z

    if-eqz v1, :cond_1

    .line 209
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->finish()V

    goto :goto_0

    .line 211
    :cond_1
    if-ne p1, v2, :cond_2

    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 212
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/app/FragmentManager;->popBackStack(II)V

    goto :goto_0

    .line 216
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/samsung/helphub/HelpHubActivityBase;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 239
    invoke-super {p0, p1}, Lcom/samsung/helphub/HelpHubActivityBase;->onNewIntent(Landroid/content/Intent;)V

    .line 240
    invoke-direct {p0, p1}, Lcom/samsung/helphub/HelpHubSearchActivity;->handleIntent(Landroid/content/Intent;)V

    .line 241
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 160
    const/4 v2, 0x0

    .line 162
    .local v2, "retval":Z
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 201
    :goto_0
    if-nez v2, :cond_0

    invoke-super {p0, p1}, Lcom/samsung/helphub/HelpHubActivityBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move v3, v4

    :cond_1
    return v3

    .line 164
    :pswitch_0
    iget-object v5, p0, Lcom/samsung/helphub/HelpHubSearchActivity;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getApplicationWindowToken()Landroid/os/IBinder;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 167
    iget-boolean v5, p0, Lcom/samsung/helphub/HelpHubSearchActivity;->mTwoPane:Z

    if-nez v5, :cond_3

    .line 169
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->isStackEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 170
    sget-boolean v5, Lcom/samsung/helphub/HelpHubSearchActivity;->bFromSearchProvider:Z

    if-eqz v5, :cond_2

    .line 171
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-class v6, Lcom/samsung/helphub/HelpHubActivity;

    invoke-virtual {v5, p0, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    .line 172
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v5, 0x4000000

    invoke-virtual {v1, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 173
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/HelpHubSearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 176
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->finish()V

    .line 194
    :cond_3
    :goto_1
    const/4 v2, 0x1

    .line 195
    goto :goto_0

    .line 180
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v5

    if-ne v5, v4, :cond_5

    sget-boolean v5, Lcom/samsung/helphub/HelpHubSearchActivity;->bFromSearchProvider:Z

    if-nez v5, :cond_5

    .line 181
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-class v6, Lcom/samsung/helphub/HelpHubActivity;

    invoke-virtual {v5, p0, v6}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    .line 182
    .restart local v1    # "intent":Landroid/content/Intent;
    const/high16 v5, 0x4000000

    invoke-virtual {v1, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 183
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/HelpHubSearchActivity;->startActivity(Landroid/content/Intent;)V

    .line 186
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->popBackStack()V

    .line 187
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 188
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 162
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 96
    invoke-super {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->onResume()V

    .line 97
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 98
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubSearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 100
    return-void
.end method
