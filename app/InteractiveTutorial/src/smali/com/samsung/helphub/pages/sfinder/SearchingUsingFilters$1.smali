.class Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters$1;
.super Ljava/lang/Object;
.source "SearchingUsingFilters.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters;)V
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters$1;->this$0:Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 17
    iget-object v2, p0, Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters$1;->this$0:Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters;

    # getter for: Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters;->access$000(Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 40
    :goto_0
    return-void

    .line 19
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters$1;->this$0:Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters;->access$102(Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters;Z)Z

    .line 23
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.samsung.android.app.galaxyfinder"

    const-string v4, "com.samsung.android.app.galaxyfinder.GalaxyFinderActivity"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 29
    .local v1, "mIntent":Landroid/content/Intent;
    const-string v2, "HelpMode"

    const-string v3, "filterSearching"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    iget-object v2, p0, Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters$1;->this$0:Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters;

    invoke-virtual {v2, v1}, Lcom/samsung/helphub/pages/sfinder/SearchingUsingFilters;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 33
    .end local v1    # "mIntent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "Help_StoryAlbum"

    const-string v3, "call com.samsung.android.app.episodes.ui.timeline.TimelineActivity. gallery"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
