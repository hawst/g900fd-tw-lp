.class public Lcom/samsung/helphub/pages/storyalbum/ImportAndExport;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "ImportAndExport.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    return-void
.end method

.method private isDropboxExist()Z
    .locals 6

    .prologue
    .line 35
    const/4 v2, 0x0

    .line 36
    .local v2, "result":Z
    const/4 v1, 0x0

    .line 38
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/storyalbum/ImportAndExport;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.android.cloudagent"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 42
    :goto_0
    if-eqz v1, :cond_0

    .line 43
    const/4 v2, 0x1

    .line 45
    :cond_0
    return v2

    .line 39
    :catch_0
    move-exception v0

    .line 40
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 19
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 21
    .local v0, "result":Landroid/view/View;
    invoke-direct {p0}, Lcom/samsung/helphub/pages/storyalbum/ImportAndExport;->isDropboxExist()Z

    move-result v2

    if-nez v2, :cond_0

    .line 22
    const v2, 0x7f0d0032

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 23
    .local v1, "tv":Landroid/widget/TextView;
    const v2, 0x7f0a03d7

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 26
    .end local v1    # "tv":Landroid/widget/TextView;
    :cond_0
    return-object v0
.end method
