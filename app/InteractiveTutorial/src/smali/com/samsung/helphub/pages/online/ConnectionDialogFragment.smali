.class public Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;
.super Landroid/app/DialogFragment;
.source "ConnectionDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field mIsMobileConnected:Z

.field private mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;)Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 85
    const/4 v1, 0x0

    .line 86
    .local v1, "rememberSelection":Z
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 87
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_2

    .line 88
    sget-object v3, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->NONE_CHECKED:Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    .line 90
    .local v3, "selection":Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;
    const v4, 0x7f0d02ba

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 92
    .local v2, "rememberSelectionBox":Landroid/widget/CheckBox;
    if-eqz v2, :cond_0

    .line 93
    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 96
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 112
    sget-object v3, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->NONE_CHECKED:Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    .line 116
    :cond_1
    :goto_0
    iget-boolean v4, p0, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->mIsMobileConnected:Z

    if-eqz v4, :cond_3

    .line 117
    invoke-virtual {v0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "MOBILE_CHECKED"

    invoke-static {v4, v3, v5}, Lcom/samsung/helphub/utility/PreferenceHelper;->setCheckedPreference(Landroid/content/Context;Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;Ljava/lang/String;)V

    .line 122
    .end local v2    # "rememberSelectionBox":Landroid/widget/CheckBox;
    .end local v3    # "selection":Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;
    :cond_2
    :goto_1
    return-void

    .line 98
    .restart local v2    # "rememberSelectionBox":Landroid/widget/CheckBox;
    .restart local v3    # "selection":Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;
    :pswitch_0
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    invoke-interface {v4}, Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;->onClickMobileConnection()V

    .line 99
    if-eqz v1, :cond_1

    .line 100
    sget-object v3, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->CHECKED:Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    goto :goto_0

    .line 105
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    invoke-interface {v4}, Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;->onClickCancel()V

    .line 106
    sget-object v3, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->NONE_CHECKED:Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    .line 107
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 119
    :cond_3
    invoke-virtual {v0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "WLAN_CHECKED"

    invoke-static {v4, v3, v5}, Lcom/samsung/helphub/utility/PreferenceHelper;->setCheckedPreference(Landroid/content/Context;Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;Ljava/lang/String;)V

    goto :goto_1

    .line 96
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const/4 v0, 0x1

    const v1, 0x7f0e003a

    invoke-virtual {p0, v0, v1}, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->setStyle(II)V

    .line 29
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 34
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f04004e

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 36
    .local v1, "dialogLayout":Landroid/view/View;
    const v3, 0x7f0d02b9

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 37
    .local v2, "message":Landroid/widget/TextView;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0a0064

    invoke-virtual {v3, v4, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0a0063

    invoke-virtual {v3, v4, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment$1;

    invoke-direct {v4, p0}, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment$1;-><init>(Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 58
    .local v0, "dialog":Landroid/app/Dialog;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v6}, Lcom/samsung/helphub/HelpHubCommon;->isNetworkConnected(Landroid/content/Context;I)Z

    move-result v3

    iput-boolean v3, p0, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->mIsMobileConnected:Z

    .line 60
    iget-boolean v3, p0, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->mIsMobileConnected:Z

    if-eqz v3, :cond_0

    .line 61
    const v3, 0x7f0a08c5

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setTitle(I)V

    .line 62
    const v3, 0x7f0a08c8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 68
    :goto_0
    invoke-virtual {p0, v6}, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->setCancelable(Z)V

    .line 70
    return-object v0

    .line 64
    :cond_0
    const v3, 0x7f0a08c6

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setTitle(I)V

    .line 65
    const v3, 0x7f0a08c7

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public setDialogListener(Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    .line 80
    return-void
.end method
