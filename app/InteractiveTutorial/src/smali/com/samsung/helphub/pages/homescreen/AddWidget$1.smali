.class Lcom/samsung/helphub/pages/homescreen/AddWidget$1;
.super Ljava/lang/Object;
.source "AddWidget.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/homescreen/AddWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/homescreen/AddWidget;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/homescreen/AddWidget;)V
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/samsung/helphub/pages/homescreen/AddWidget$1;->this$0:Lcom/samsung/helphub/pages/homescreen/AddWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 31
    iget-object v2, p0, Lcom/samsung/helphub/pages/homescreen/AddWidget$1;->this$0:Lcom/samsung/helphub/pages/homescreen/AddWidget;

    # getter for: Lcom/samsung/helphub/pages/homescreen/AddWidget;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/homescreen/AddWidget;->access$000(Lcom/samsung/helphub/pages/homescreen/AddWidget;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 35
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/homescreen/AddWidget$1;->this$0:Lcom/samsung/helphub/pages/homescreen/AddWidget;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/homescreen/AddWidget;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/homescreen/AddWidget;->access$102(Lcom/samsung/helphub/pages/homescreen/AddWidget;Z)Z

    .line 37
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.sec.android.app.launcher"

    const-string v3, "com.android.launcher2.Launcher"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .local v0, "componentName":Landroid/content/ComponentName;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 39
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 40
    const-string v2, "homescreen:guide_mode"

    const-string v3, "add_widgets"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    const-string v2, "help_activity_type"

    iget-object v3, p0, Lcom/samsung/helphub/pages/homescreen/AddWidget$1;->this$0:Lcom/samsung/helphub/pages/homescreen/AddWidget;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/homescreen/AddWidget;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    iget-object v2, p0, Lcom/samsung/helphub/pages/homescreen/AddWidget$1;->this$0:Lcom/samsung/helphub/pages/homescreen/AddWidget;

    invoke-virtual {v2, v1}, Lcom/samsung/helphub/pages/homescreen/AddWidget;->startActivity(Landroid/content/Intent;)V

    .line 43
    iget-object v2, p0, Lcom/samsung/helphub/pages/homescreen/AddWidget$1;->this$0:Lcom/samsung/helphub/pages/homescreen/AddWidget;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/homescreen/AddWidget;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ExternalHelpActivity"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 44
    iget-object v2, p0, Lcom/samsung/helphub/pages/homescreen/AddWidget$1;->this$0:Lcom/samsung/helphub/pages/homescreen/AddWidget;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/homescreen/AddWidget;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
