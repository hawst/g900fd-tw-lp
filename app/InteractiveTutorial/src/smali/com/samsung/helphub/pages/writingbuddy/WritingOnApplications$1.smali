.class Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;
.super Ljava/lang/Object;
.source "WritingOnApplications.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 87
    iget-object v5, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;

    invoke-virtual {v5}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "pen_writing_buddy"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_0

    move v2, v3

    .line 90
    .local v2, "masterStatus":Z
    :goto_0
    iget-object v5, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;

    invoke-virtual {v5}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "accessibility_display_magnification_enabled"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_1

    move v0, v3

    .line 94
    .local v0, "MagnificationResult":Z
    :goto_1
    iget-object v4, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;

    # getter for: Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->isClicked:Z
    invoke-static {v4}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->access$000(Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 128
    :goto_2
    return-void

    .end local v0    # "MagnificationResult":Z
    .end local v2    # "masterStatus":Z
    :cond_0
    move v2, v4

    .line 87
    goto :goto_0

    .restart local v2    # "masterStatus":Z
    :cond_1
    move v0, v4

    .line 90
    goto :goto_1

    .line 98
    .restart local v0    # "MagnificationResult":Z
    :cond_2
    iget-object v4, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;

    # setter for: Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->isClicked:Z
    invoke-static {v4, v3}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->access$102(Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;Z)Z

    .line 100
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;

    const v5, 0x7f0a07c9

    invoke-virtual {v4, v5}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\n- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;

    const v5, 0x7f0a0573

    invoke-virtual {v4, v5}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;

    const v5, 0x7f0a0574

    invoke-virtual {v4, v5}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 106
    .local v1, "dialog_content":Ljava/lang/String;
    if-nez v2, :cond_3

    iget-object v3, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_3

    if-nez v0, :cond_3

    .line 108
    iget-object v3, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;

    const v4, 0x7f0a07c6

    const v5, 0x7f0a07c7

    # invokes: Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->showEnableSettingDialog(II)V
    invoke-static {v3, v4, v5}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->access$200(Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;II)V

    goto :goto_2

    .line 114
    :cond_3
    iget-object v3, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_4

    if-eqz v0, :cond_5

    .line 116
    :cond_4
    iget-object v3, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;

    const v4, 0x7f0a07c8

    # invokes: Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->showEnableSettingDialog(ILjava/lang/String;)V
    invoke-static {v3, v4, v1}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->access$300(Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;ILjava/lang/String;)V

    goto :goto_2

    .line 124
    :cond_5
    iget-object v3, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->startTutorial()V

    goto :goto_2
.end method
