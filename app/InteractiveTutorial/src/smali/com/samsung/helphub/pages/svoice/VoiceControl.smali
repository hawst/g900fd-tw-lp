.class public Lcom/samsung/helphub/pages/svoice/VoiceControl;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "VoiceControl.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 99
    new-instance v0, Lcom/samsung/helphub/pages/svoice/VoiceControl$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/svoice/VoiceControl$1;-><init>(Lcom/samsung/helphub/pages/svoice/VoiceControl;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/svoice/VoiceControl;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/svoice/VoiceControl;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/svoice/VoiceControl;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/svoice/VoiceControl;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/svoice/VoiceControl;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/svoice/VoiceControl;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/svoice/VoiceControl;->isClicked:Z

    return p1
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/helphub/pages/svoice/VoiceControl;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public hasChatOnV()Z
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/VoiceControl;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 91
    .local v0, "context":Landroid/content/Context;
    const-string v1, "com.coolots.chaton"

    invoke-static {v0, v1}, Lcom/samsung/helphub/HelpHubCommon;->isPackageExists(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.coolots.chatonforcanada"

    invoke-static {v0, v1}, Lcom/samsung/helphub/HelpHubCommon;->isPackageExists(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 94
    :cond_0
    const/4 v1, 0x1

    .line 96
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasDialing()Z
    .locals 3

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/VoiceControl;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 80
    .local v0, "context":Landroid/content/Context;
    const-string v2, "phone"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 82
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v2

    if-nez v2, :cond_0

    .line 84
    const/4 v2, 0x0

    .line 86
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v6, 0x7f0a043d

    .line 26
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 29
    .local v1, "result":Landroid/view/View;
    if-eqz v1, :cond_2

    .line 32
    const v4, 0x7f0d0033

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 38
    .local v0, "helpHubTextView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/VoiceControl;->hasChatOnV()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/VoiceControl;->hasDialing()Z

    move-result v4

    if-nez v4, :cond_4

    :cond_0
    if-eqz v0, :cond_4

    const-string v4, "v1awifikx"

    const-string v5, "ro.product.name"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 42
    const/4 v2, 0x0

    .line 43
    .local v2, "showDisplayString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/VoiceControl;->hasDialing()Z

    move-result v4

    if-nez v4, :cond_3

    .line 45
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/VoiceControl;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a043f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 52
    :goto_0
    if-nez v2, :cond_1

    .line 53
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/VoiceControl;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 57
    :cond_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    .end local v2    # "showDisplayString":Ljava/lang/String;
    :goto_1
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->hasNewVoiceControlConcept()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 71
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 75
    .end local v0    # "helpHubTextView":Landroid/widget/TextView;
    :cond_2
    return-object v1

    .line 48
    .restart local v0    # "helpHubTextView":Landroid/widget/TextView;
    .restart local v2    # "showDisplayString":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/VoiceControl;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a043e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 59
    .end local v2    # "showDisplayString":Ljava/lang/String;
    :cond_4
    const/4 v3, 0x0

    .line 60
    .local v3, "showDisplayString_v1awifi":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/VoiceControl;->hasChatOnV()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/VoiceControl;->hasDialing()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 61
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/VoiceControl;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 67
    :goto_2
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 64
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/VoiceControl;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a043c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method
