.class Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;
.super Ljava/lang/Object;
.source "AirViewSpeedDial.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v10, 0x7f0a056f

    const v9, 0x7f0a056c

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 70
    iget-object v7, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    invoke-virtual {v7}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "finger_air_view"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-eqz v7, :cond_0

    move v2, v5

    .line 71
    .local v2, "masterStatus":Z
    :goto_0
    iget-object v7, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    invoke-virtual {v7}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "finger_air_view_speed_dial_tip"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-eqz v7, :cond_1

    move v4, v5

    .line 72
    .local v4, "settingStatusSpeedDial":Z
    :goto_1
    iget-object v7, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    invoke-virtual {v7}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "finger_air_view_full_text"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-eqz v7, :cond_2

    move v3, v5

    .line 73
    .local v3, "settingStatusFullText":Z
    :goto_2
    iget-object v7, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    invoke-virtual {v7}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "accessibility_display_magnification_enabled"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-eqz v7, :cond_3

    move v0, v5

    .line 74
    .local v0, "MagnificationResult":Z
    :goto_3
    iget-object v6, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    # getter for: Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->isClicked:Z
    invoke-static {v6}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->access$000(Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 93
    :goto_4
    return-void

    .end local v0    # "MagnificationResult":Z
    .end local v2    # "masterStatus":Z
    .end local v3    # "settingStatusFullText":Z
    .end local v4    # "settingStatusSpeedDial":Z
    :cond_0
    move v2, v6

    .line 70
    goto :goto_0

    .restart local v2    # "masterStatus":Z
    :cond_1
    move v4, v6

    .line 71
    goto :goto_1

    .restart local v4    # "settingStatusSpeedDial":Z
    :cond_2
    move v3, v6

    .line 72
    goto :goto_2

    .restart local v3    # "settingStatusFullText":Z
    :cond_3
    move v0, v6

    .line 73
    goto :goto_3

    .line 76
    .restart local v0    # "MagnificationResult":Z
    :cond_4
    iget-object v6, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    # setter for: Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->isClicked:Z
    invoke-static {v6, v5}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->access$102(Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;Z)Z

    .line 77
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    invoke-virtual {v6, v10}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    const v7, 0x7f0a0572

    invoke-virtual {v6, v7}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    const v7, 0x7f0a0573

    invoke-virtual {v6, v7}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    const v7, 0x7f0a0574

    invoke-virtual {v6, v7}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, "dialog_content":Ljava/lang/String;
    if-eqz v2, :cond_5

    if-eqz v4, :cond_5

    if-nez v3, :cond_6

    :cond_5
    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    invoke-virtual {v5}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_6

    if-nez v0, :cond_6

    .line 83
    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->showEnableSettingDialog(II)V
    invoke-static {v5, v9, v10}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->access$200(Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;II)V

    goto :goto_4

    .line 86
    :cond_6
    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    invoke-virtual {v5}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_7

    if-eqz v0, :cond_8

    .line 87
    :cond_7
    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->showEnableSettingDialog(ILjava/lang/String;)V
    invoke-static {v5, v9, v1}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->access$300(Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;ILjava/lang/String;)V

    goto/16 :goto_4

    .line 91
    :cond_8
    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;

    invoke-virtual {v5}, Lcom/samsung/helphub/pages/airview/AirViewSpeedDial;->startTutorial()V

    goto/16 :goto_4
.end method
