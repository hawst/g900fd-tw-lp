.class public Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "AirViewInformationPreview.java"


# instance fields
.field protected isClicked:Z

.field private mEnableSettingDialogFragment:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->isClicked:Z

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->checkSettingStatus()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->showSPlannerSettingDialog(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->showGallerySettingDialog(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->showEMailSettingDialog(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->turnOnRelevantSetting()V

    return-void
.end method

.method private checkSettingStatus()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 246
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "finger_air_view"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_2

    move v0, v3

    .line 247
    .local v0, "masterStatus":Z
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "finger_air_view_information_preview"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_3

    move v1, v3

    .line 248
    .local v1, "settingStatus":Z
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "finger_air_view_full_text"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_4

    move v2, v3

    .line 249
    .local v2, "settingStatusFullText":Z
    :goto_2
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    :cond_0
    move v3, v4

    .line 252
    :cond_1
    return v3

    .end local v0    # "masterStatus":Z
    .end local v1    # "settingStatus":Z
    .end local v2    # "settingStatusFullText":Z
    :cond_2
    move v0, v4

    .line 246
    goto :goto_0

    .restart local v0    # "masterStatus":Z
    :cond_3
    move v1, v4

    .line 247
    goto :goto_1

    .restart local v1    # "settingStatus":Z
    :cond_4
    move v2, v4

    .line 248
    goto :goto_2
.end method

.method private showEMailSettingDialog(ILjava/lang/String;)V
    .locals 3
    .param p1, "titleID"    # I
    .param p2, "summaryID"    # Ljava/lang/String;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 203
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0064

    new-instance v2, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$12;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$12;-><init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0063

    new-instance v2, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$11;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$11;-><init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$10;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$10;-><init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 230
    return-void
.end method

.method private showGallerySettingDialog(ILjava/lang/String;)V
    .locals 3
    .param p1, "titleID"    # I
    .param p2, "summaryID"    # Ljava/lang/String;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 167
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0064

    new-instance v2, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$9;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$9;-><init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0063

    new-instance v2, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$8;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$8;-><init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$7;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$7;-><init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 197
    return-void
.end method

.method private showSPlannerSettingDialog(ILjava/lang/String;)V
    .locals 3
    .param p1, "titleID"    # I
    .param p2, "summaryID"    # Ljava/lang/String;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 135
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0064

    new-instance v2, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$6;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$6;-><init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0063

    new-instance v2, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$5;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$5;-><init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$4;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$4;-><init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 161
    return-void
.end method

.method private turnOnRelevantSetting()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 255
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/helphub/utility/TackBackUtility;->turnOffTalkBack(Landroid/content/Context;)Z

    .line 256
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accessibility_display_magnification_enabled"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 258
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 259
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view_highlight"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 260
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view_information_preview"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 261
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view_full_text"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 263
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.gesture.FINGER_AIR_VIEW_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 264
    .local v0, "finger_air_view_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 265
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 266
    return-void
.end method


# virtual methods
.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->isClicked:Z

    .line 236
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 31
    .local v4, "res":Landroid/content/res/Resources;
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v5

    .line 34
    .local v5, "result":Landroid/view/View;
    const v6, 0x7f0d0229

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 39
    .local v3, "messageAirviewPreview":Landroid/widget/RelativeLayout;
    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 42
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "accessibility_display_magnification_enabled"

    invoke-static {v6, v7, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_0

    const/4 v0, 0x1

    .line 43
    .local v0, "MagnificationResult":Z
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f0a056e

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f0a0572

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n\n- "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f0a0573

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n- "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const v7, 0x7f0a0574

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 47
    .local v2, "dialog_content":Ljava/lang/String;
    const v6, 0x7f0d0224

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 48
    .local v1, "button":Landroid/widget/Button;
    new-instance v6, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;

    invoke-direct {v6, p0, v0, v2}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;-><init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;ZLjava/lang/String;)V

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    const v6, 0x7f0d0228

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "button":Landroid/widget/Button;
    check-cast v1, Landroid/widget/Button;

    .line 68
    .restart local v1    # "button":Landroid/widget/Button;
    new-instance v6, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$2;

    invoke-direct {v6, p0, v0, v2}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$2;-><init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;ZLjava/lang/String;)V

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    const v6, 0x7f0d022d

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "button":Landroid/widget/Button;
    check-cast v1, Landroid/widget/Button;

    .line 100
    .restart local v1    # "button":Landroid/widget/Button;
    new-instance v6, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;

    invoke-direct {v6, p0, v0, v2}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;-><init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;ZLjava/lang/String;)V

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    return-object v5
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 241
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->onResume()V

    .line 242
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->isClicked:Z

    .line 243
    return-void
.end method
