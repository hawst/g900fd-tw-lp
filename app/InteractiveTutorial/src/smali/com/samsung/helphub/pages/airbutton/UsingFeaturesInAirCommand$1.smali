.class Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$1;
.super Ljava/lang/Object;
.source "UsingFeaturesInAirCommand.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    const/4 v0, 0x0

    .line 57
    .local v0, "hoverZoom":Z
    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    # getter for: Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->access$000(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/app/Activity;

    move-result-object v3

    const-string v4, "com.sec.feature.overlaymagnifier"

    invoke-static {v3, v4}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 58
    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    # getter for: Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->access$000(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "accessibility_magnifier"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_2

    move v0, v1

    .line 60
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    # getter for: Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->access$000(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/helphub/utility/Utils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    # getter for: Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->access$000(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "accessibility_display_magnification_enabled"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v1, :cond_1

    if-eqz v0, :cond_3

    .line 63
    :cond_1
    iget-object v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    # invokes: Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->showTalkBackDisableDialog(Landroid/view/View;)V
    invoke-static {v1, p1}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->access$100(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;Landroid/view/View;)V

    .line 75
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 58
    goto :goto_0

    .line 67
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_1

    .line 69
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    iget-object v2, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    # getter for: Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mPenableHelpIntent:Landroid/content/Intent;
    invoke-static {v2}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->access$200(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 72
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    iget-object v2, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    # getter for: Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mEasyClipHelpIntent:Landroid/content/Intent;
    invoke-static {v2}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->access$300(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 67
    :pswitch_data_0
    .packed-switch 0x7f0d0081
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
