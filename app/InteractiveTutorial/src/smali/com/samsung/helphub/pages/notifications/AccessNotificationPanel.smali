.class public Lcom/samsung/helphub/pages/notifications/AccessNotificationPanel;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "AccessNotificationPanel.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 30
    new-instance v0, Lcom/samsung/helphub/pages/notifications/AccessNotificationPanel$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/notifications/AccessNotificationPanel$1;-><init>(Lcom/samsung/helphub/pages/notifications/AccessNotificationPanel;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/notifications/AccessNotificationPanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/AccessNotificationPanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 21
    .local v2, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/notifications/AccessNotificationPanel;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 22
    const v3, 0x7f0d03a7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 23
    .local v0, "mImageView_01":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/notifications/AccessNotificationPanel;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "notification_access_the_notification_panel_01"

    invoke-static {v3, v0, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 24
    const v3, 0x7f0d03a8

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 25
    .local v1, "mImageView_02":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/notifications/AccessNotificationPanel;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "notification_access_the_notification_panel_02"

    invoke-static {v3, v1, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 27
    .end local v0    # "mImageView_01":Landroid/widget/ImageView;
    .end local v1    # "mImageView_02":Landroid/widget/ImageView;
    :cond_0
    return-object v2
.end method
