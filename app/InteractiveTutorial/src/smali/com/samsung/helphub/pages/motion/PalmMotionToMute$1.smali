.class Lcom/samsung/helphub/pages/motion/PalmMotionToMute$1;
.super Ljava/lang/Object;
.source "PalmMotionToMute.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/motion/PalmMotionToMute;->getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToMute;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/motion/PalmMotionToMute;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToMute$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToMute;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 76
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToMute$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToMute;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/motion/PalmMotionToMute;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v2

    const-string v6, "SEC_FLOATING_FEATURE_SETTINGS_MOTION_CONCEPT_2013"

    invoke-virtual {v2, v6}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "surface_motion_engine"

    :goto_0
    invoke-static {v5, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    move v0, v3

    .line 77
    .local v0, "masterStatus":Z
    :goto_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToMute$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToMute;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/motion/PalmMotionToMute;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v2

    const-string v6, "SEC_FLOATING_FEATURE_SETTINGS_MOTION_CONCEPT_2014"

    invoke-virtual {v2, v6}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "motion_merged_mute_pause"

    :goto_2
    invoke-static {v5, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_3

    move v1, v3

    .line 79
    .local v1, "settingStatus":Z
    :goto_3
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToMute$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToMute;

    # getter for: Lcom/samsung/helphub/pages/motion/PalmMotionToMute;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/motion/PalmMotionToMute;->access$000(Lcom/samsung/helphub/pages/motion/PalmMotionToMute;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 93
    :goto_4
    return-void

    .line 76
    .end local v0    # "masterStatus":Z
    .end local v1    # "settingStatus":Z
    :cond_0
    const-string v2, "master_motion"

    goto :goto_0

    :cond_1
    move v0, v4

    goto :goto_1

    .line 77
    .restart local v0    # "masterStatus":Z
    :cond_2
    const-string v2, "surface_palm_touch"

    goto :goto_2

    :cond_3
    move v1, v4

    goto :goto_3

    .line 81
    .restart local v1    # "settingStatus":Z
    :cond_4
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToMute$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToMute;

    # setter for: Lcom/samsung/helphub/pages/motion/PalmMotionToMute;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/motion/PalmMotionToMute;->access$102(Lcom/samsung/helphub/pages/motion/PalmMotionToMute;Z)Z

    .line 82
    if-eqz v0, :cond_5

    if-nez v1, :cond_6

    .line 83
    :cond_5
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToMute$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToMute;

    const v3, 0x7f0a0562

    const v4, 0x7f0a0563

    # invokes: Lcom/samsung/helphub/pages/motion/PalmMotionToMute;->showEnableSettingDialog(II)V
    invoke-static {v2, v3, v4}, Lcom/samsung/helphub/pages/motion/PalmMotionToMute;->access$200(Lcom/samsung/helphub/pages/motion/PalmMotionToMute;II)V

    goto :goto_4

    .line 86
    :cond_6
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToMute$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToMute;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/motion/PalmMotionToMute;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 87
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToMute$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToMute;

    # invokes: Lcom/samsung/helphub/pages/motion/PalmMotionToMute;->makeTalkBackDisablePopup()V
    invoke-static {v2}, Lcom/samsung/helphub/pages/motion/PalmMotionToMute;->access$300(Lcom/samsung/helphub/pages/motion/PalmMotionToMute;)V

    goto :goto_4

    .line 90
    :cond_7
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToMute$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToMute;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/motion/PalmMotionToMute;->startTutorial()V

    goto :goto_4
.end method
