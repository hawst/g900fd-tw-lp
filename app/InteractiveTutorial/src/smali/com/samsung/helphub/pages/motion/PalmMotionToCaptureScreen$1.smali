.class Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$1;
.super Ljava/lang/Object;
.source "PalmMotionToCaptureScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 76
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v2

    const-string v6, "SEC_FLOATING_FEATURE_SETTINGS_MOTION_CONCEPT_2013"

    invoke-virtual {v2, v6}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "surface_motion_engine"

    :goto_0
    invoke-static {v5, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_2

    move v0, v3

    .line 77
    .local v0, "masterStatus":Z
    :goto_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "surface_palm_swipe"

    invoke-static {v2, v5, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_3

    move v1, v3

    .line 79
    .local v1, "settingStatus":Z
    :goto_2
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;

    # setter for: Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->access$002(Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;Z)Z

    .line 80
    if-eqz v0, :cond_0

    if-nez v1, :cond_4

    .line 81
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;

    const v3, 0x7f0a0562

    const v4, 0x7f0a0563

    # invokes: Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->showEnableSettingDialog(II)V
    invoke-static {v2, v3, v4}, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->access$100(Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;II)V

    .line 90
    :goto_3
    return-void

    .line 76
    .end local v0    # "masterStatus":Z
    .end local v1    # "settingStatus":Z
    :cond_1
    const-string v2, "master_motion"

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1

    .restart local v0    # "masterStatus":Z
    :cond_3
    move v1, v4

    .line 77
    goto :goto_2

    .line 84
    .restart local v1    # "settingStatus":Z
    :cond_4
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 85
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;

    # invokes: Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->makeTalkBackDisablePopup()V
    invoke-static {v2}, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->access$200(Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;)V

    goto :goto_3

    .line 87
    :cond_5
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$1;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->startTutorial()V

    goto :goto_3
.end method
