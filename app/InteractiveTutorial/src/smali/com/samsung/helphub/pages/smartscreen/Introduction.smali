.class public Lcom/samsung/helphub/pages/smartscreen/Introduction;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "Introduction.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v6

    .line 24
    .local v6, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 25
    const v7, 0x7f0d03dd

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 27
    .local v1, "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    if-eqz v1, :cond_1

    .line 28
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const-string v8, "tutorial_smart_screen_introduction"

    const-string v9, "array"

    const-string v10, "com.samsung.helpplugin"

    invoke-static {v7, v8, v9, v10}, Lcom/samsung/helphub/HelpHubCommon;->getResourceId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 30
    .local v5, "resID":I
    const/4 v7, -0x1

    if-eq v5, v7, :cond_2

    .line 31
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 33
    .local v4, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v7, "com.samsung.helpplugin"

    invoke-virtual {v4, v7}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    .line 35
    .local v2, "mResources":Landroid/content/res/Resources;
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 36
    .local v3, "mTypedArray":Landroid/content/res/TypedArray;
    if-eqz v3, :cond_0

    .line 37
    invoke-virtual {v1, v3}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->setPluginDrawables(Landroid/content/res/TypedArray;)V

    .line 38
    :cond_0
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    .end local v1    # "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    .end local v2    # "mResources":Landroid/content/res/Resources;
    .end local v3    # "mTypedArray":Landroid/content/res/TypedArray;
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    .end local v5    # "resID":I
    :cond_1
    :goto_0
    return-object v6

    .line 39
    .restart local v1    # "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    .restart local v4    # "pm":Landroid/content/pm/PackageManager;
    .restart local v5    # "resID":I
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 44
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    :cond_2
    const-string v7, "HelpHub2.0"

    const-string v8, "The resource not downloaded yet"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
