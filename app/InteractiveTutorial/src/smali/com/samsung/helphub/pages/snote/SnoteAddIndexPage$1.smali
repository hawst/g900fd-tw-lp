.class Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage$1;
.super Ljava/lang/Object;
.source "SnoteAddIndexPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;)V
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage$1;->this$0:Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 15
    const-string v1, "com.samsung.android.snote"

    .line 16
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage$1;->this$0:Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;

    iget-object v3, p0, Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage$1;->this$0:Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;->access$000(Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 17
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage$1;->this$0:Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;

    # getter for: Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;->access$100(Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 33
    :goto_0
    return-void

    .line 21
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage$1;->this$0:Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;->access$202(Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;Z)Z

    .line 23
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.samsung.android.snote"

    const-string v4, "com.samsung.android.snote.helper.HelperActivity"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 26
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "snote:section"

    const-string v3, "add_index_edit"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 28
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage$1;->this$0:Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 31
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage$1;->this$0:Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;

    # invokes: Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;->access$300(Lcom/samsung/helphub/pages/snote/SnoteAddIndexPage;Ljava/lang/String;)V

    goto :goto_0
.end method
