.class public Lcom/samsung/helphub/pages/sconnect/ConnectingToNearbyDevices;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "ConnectingToNearbyDevices.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method

.method private isVerizon()Z
    .locals 2

    .prologue
    .line 33
    const-string v0, "VZW"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 19
    .local v2, "result":Landroid/view/View;
    invoke-direct {p0}, Lcom/samsung/helphub/pages/sconnect/ConnectingToNearbyDevices;->isVerizon()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 20
    const v3, 0x7f0d03b4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 22
    .local v0, "description1":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 23
    const v3, 0x7f0a08b0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 25
    :cond_0
    const v3, 0x7f0d03b5

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 26
    .local v1, "moreInfo":Landroid/widget/TextView;
    if-eqz v1, :cond_1

    .line 27
    const v3, 0x7f0a08b5

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 29
    .end local v0    # "description1":Landroid/widget/TextView;
    .end local v1    # "moreInfo":Landroid/widget/TextView;
    :cond_1
    return-object v2
.end method
