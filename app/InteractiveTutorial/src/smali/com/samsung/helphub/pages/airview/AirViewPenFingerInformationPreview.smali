.class public Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;
.super Lcom/samsung/helphub/pages/AirViewPenFingerPage;
.source "AirViewPenFingerInformationPreview.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# instance fields
.field final functionID:I

.field isSplanner:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;-><init>()V

    .line 22
    const v0, 0x7f0a019f

    iput v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->functionID:I

    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->isSplanner:Z

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 19
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->checkPenFingerModePopup(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->isClicked:Z

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->isClicked:Z

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 19
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->checkPenFingerModePopup(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 109
    return-object p0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->isClicked:Z

    .line 89
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->checkTalkbackAndMagnification()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->setTalkbackAndMagnification()V

    .line 80
    :cond_0
    const-string v0, "pen_hovering_information_preview"

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->setEnableSettings(Ljava/lang/String;)V

    .line 81
    const-string v0, "finger_air_view_information_preview"

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->setEnableSettings(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->startTutorial()V

    .line 84
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v3

    .line 28
    .local v3, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 29
    const v4, 0x7f0d0233

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 31
    .local v2, "mImageViewer_splanner":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "tutorial_air_view_pen_splanner"

    invoke-static {v4, v2, v5}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 33
    const v4, 0x7f0d0235

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 35
    .local v1, "mImageViewer_gallery":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "tutorial_air_view_pen_gallery"

    invoke-static {v4, v1, v5}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 37
    .end local v1    # "mImageViewer_gallery":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    .end local v2    # "mImageViewer_splanner":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_0
    const v4, 0x7f0d0234

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 38
    .local v0, "button":Landroid/widget/Button;
    new-instance v4, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview$1;

    invoke-direct {v4, p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview$1;-><init>(Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    const v4, 0x7f0d0236

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "button":Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 50
    .restart local v0    # "button":Landroid/widget/Button;
    new-instance v4, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview$2;

    invoke-direct {v4, p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview$2;-><init>(Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    return-object v3
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->onResume()V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->isClicked:Z

    .line 71
    return-void
.end method

.method protected startTutorial()V
    .locals 3

    .prologue
    .line 93
    iget-boolean v1, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->isSplanner:Z

    if-eqz v1, :cond_0

    .line 94
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.intent.calendar.help"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 95
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->startActivity(Landroid/content/Intent;)V

    .line 105
    :goto_0
    return-void

    .line 97
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 98
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "image/jpg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    const-string v1, "com.sec.android.gallery3d"

    const-string v2, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    const-string v1, "IsHelpMode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 101
    const-string v1, "HelpMode"

    const-string v2, "INFORMATION_PREVIEW"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerInformationPreview;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
