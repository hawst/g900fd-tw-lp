.class Lcom/samsung/helphub/pages/settings/WiFiConnectPage$1;
.super Ljava/lang/Object;
.source "WiFiConnectPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/settings/WiFiConnectPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/settings/WiFiConnectPage;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/settings/WiFiConnectPage;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/samsung/helphub/pages/settings/WiFiConnectPage$1;->this$0:Lcom/samsung/helphub/pages/settings/WiFiConnectPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 36
    iget-object v1, p0, Lcom/samsung/helphub/pages/settings/WiFiConnectPage$1;->this$0:Lcom/samsung/helphub/pages/settings/WiFiConnectPage;

    # getter for: Lcom/samsung/helphub/pages/settings/WiFiConnectPage;->isClicked:Z
    invoke-static {v1}, Lcom/samsung/helphub/pages/settings/WiFiConnectPage;->access$000(Lcom/samsung/helphub/pages/settings/WiFiConnectPage;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/pages/settings/WiFiConnectPage$1;->this$0:Lcom/samsung/helphub/pages/settings/WiFiConnectPage;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/helphub/pages/settings/WiFiConnectPage;->isClicked:Z
    invoke-static {v1, v2}, Lcom/samsung/helphub/pages/settings/WiFiConnectPage;->access$102(Lcom/samsung/helphub/pages/settings/WiFiConnectPage;Z)Z

    .line 39
    const-string v1, "WiFiConnectPage"

    const-string v2, "onClick() is called"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 43
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.Settings$WifiSettingsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 45
    const-string v1, "settings:guide_mode"

    const-string v2, "wifi"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 48
    iget-object v1, p0, Lcom/samsung/helphub/pages/settings/WiFiConnectPage$1;->this$0:Lcom/samsung/helphub/pages/settings/WiFiConnectPage;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/settings/WiFiConnectPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
