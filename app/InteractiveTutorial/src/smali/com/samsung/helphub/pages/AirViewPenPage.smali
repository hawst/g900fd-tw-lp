.class public abstract Lcom/samsung/helphub/pages/AirViewPenPage;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "AirViewPenPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/pages/AirViewPenPage$7;,
        Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;
    }
.end annotation


# instance fields
.field protected isClicked:Z

.field private mEnableSettingDialogFragment:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/AirViewPenPage;->isClicked:Z

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenPage;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    return-void
.end method


# virtual methods
.method protected checkAirviewStatus(Ljava/lang/String;)Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;
    .locals 6
    .param p1, "FunctionMode"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 58
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "pen_hovering"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    .line 60
    .local v1, "masterStatus":Z
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, p1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_1

    move v0, v2

    .line 63
    .local v0, "functionStatus":Z
    :goto_1
    if-nez v1, :cond_2

    .line 64
    sget-object v2, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;->DisabledAirView:Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    .line 68
    :goto_2
    return-object v2

    .end local v0    # "functionStatus":Z
    .end local v1    # "masterStatus":Z
    :cond_0
    move v1, v3

    .line 58
    goto :goto_0

    .restart local v1    # "masterStatus":Z
    :cond_1
    move v0, v3

    .line 60
    goto :goto_1

    .line 65
    .restart local v0    # "functionStatus":Z
    :cond_2
    if-nez v0, :cond_3

    .line 66
    sget-object v2, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;->DisabledFuction:Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    goto :goto_2

    .line 68
    :cond_3
    sget-object v2, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;->AllEnalbed:Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    goto :goto_2
.end method

.method protected checkPenModePopup(Ljava/lang/String;I)V
    .locals 6
    .param p1, "fuctionMode"    # Ljava/lang/String;
    .param p2, "functionStringID"    # I

    .prologue
    const v5, 0x7f0a01b0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 31
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 33
    .local v0, "res":Landroid/content/res/Resources;
    sget-object v1, Lcom/samsung/helphub/pages/AirViewPenPage$7;->$SwitchMap$com$samsung$helphub$pages$AirViewPenPage$AirViewStatus:[I

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/AirViewPenPage;->checkAirviewStatus(Ljava/lang/String;)Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 51
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->startTutorial()V

    .line 54
    :goto_0
    return-void

    .line 35
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->checkTalkbackAndMagnification()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 36
    const v1, 0x7f0a01bb

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/samsung/helphub/pages/AirViewPenPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 39
    :cond_0
    const v1, 0x7f0a01bc

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/samsung/helphub/pages/AirViewPenPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 44
    :pswitch_1
    const v1, 0x7f0a01bd

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/samsung/helphub/pages/AirViewPenPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 48
    :pswitch_2
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->startTutorial()V

    goto :goto_0

    .line 33
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected checkTalkbackAndMagnification()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 73
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accessibility_display_magnification_enabled"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method protected abstract getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenPage;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenPage;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenPage;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 97
    :cond_0
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->onDestroy()V

    .line 98
    return-void
.end method

.method protected setEnableSettings(Ljava/lang/String;)V
    .locals 4
    .param p1, "FunctionMode"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 82
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "pen_hovering"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 83
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, p1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 85
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.gesture.AIR_VIEW_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 86
    .local v0, "air_view_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 87
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 88
    return-void
.end method

.method protected setTalkbackAndMagnification()V
    .locals 3

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/helphub/utility/TackBackUtility;->turnOffTalkBack(Landroid/content/Context;)Z

    .line 78
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accessibility_display_magnification_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 79
    return-void
.end method

.method protected final showEnableSettingDialog(II)V
    .locals 4
    .param p1, "titleID"    # I
    .param p2, "summaryID"    # I

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    move-result-object v0

    .line 147
    .local v0, "dialogListener":Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0064

    new-instance v3, Lcom/samsung/helphub/pages/AirViewPenPage$6;

    invoke-direct {v3, p0, v0}, Lcom/samsung/helphub/pages/AirViewPenPage$6;-><init>(Lcom/samsung/helphub/pages/AirViewPenPage;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0063

    new-instance v3, Lcom/samsung/helphub/pages/AirViewPenPage$5;

    invoke-direct {v3, p0, v0}, Lcom/samsung/helphub/pages/AirViewPenPage$5;-><init>(Lcom/samsung/helphub/pages/AirViewPenPage;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/helphub/pages/AirViewPenPage$4;

    invoke-direct {v2, p0, v0}, Lcom/samsung/helphub/pages/AirViewPenPage$4;-><init>(Lcom/samsung/helphub/pages/AirViewPenPage;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/pages/AirViewPenPage;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 172
    return-void
.end method

.method protected final showEnableSettingDialog(ILjava/lang/String;)V
    .locals 4
    .param p1, "titleID"    # I
    .param p2, "summaryID"    # Ljava/lang/String;

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    move-result-object v0

    .line 110
    .local v0, "dialogListener":Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0064

    new-instance v3, Lcom/samsung/helphub/pages/AirViewPenPage$3;

    invoke-direct {v3, p0, v0}, Lcom/samsung/helphub/pages/AirViewPenPage$3;-><init>(Lcom/samsung/helphub/pages/AirViewPenPage;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0063

    new-instance v3, Lcom/samsung/helphub/pages/AirViewPenPage$2;

    invoke-direct {v3, p0, v0}, Lcom/samsung/helphub/pages/AirViewPenPage$2;-><init>(Lcom/samsung/helphub/pages/AirViewPenPage;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/helphub/pages/AirViewPenPage$1;

    invoke-direct {v2, p0, v0}, Lcom/samsung/helphub/pages/AirViewPenPage$1;-><init>(Lcom/samsung/helphub/pages/AirViewPenPage;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/pages/AirViewPenPage;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 134
    return-void
.end method

.method protected abstract startTutorial()V
.end method
