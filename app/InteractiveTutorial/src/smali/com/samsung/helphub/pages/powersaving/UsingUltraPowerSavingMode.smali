.class public Lcom/samsung/helphub/pages/powersaving/UsingUltraPowerSavingMode;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "UsingUltraPowerSavingMode.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 22
    .local v2, "result":Landroid/view/View;
    const v4, 0x7f0d0440

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 24
    .local v0, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/powersaving/UsingUltraPowerSavingMode;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 25
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/powersaving/UsingUltraPowerSavingMode;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "help_power_saving_mode"

    invoke-static {v4, v0, v5}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/powersaving/UsingUltraPowerSavingMode;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/helphub/utility/Utils;->isWifiOnly(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 37
    const v4, 0x7f0d0442

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 38
    .local v1, "mobieData":Landroid/widget/LinearLayout;
    if-eqz v1, :cond_1

    .line 39
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 41
    .end local v1    # "mobieData":Landroid/widget/LinearLayout;
    :cond_1
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 42
    const v4, 0x7f0d005b

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 43
    .local v3, "wifistr":Landroid/widget/TextView;
    if-eqz v3, :cond_2

    const v4, 0x7f0a016d

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 45
    .end local v3    # "wifistr":Landroid/widget/TextView;
    :cond_2
    return-object v2
.end method
