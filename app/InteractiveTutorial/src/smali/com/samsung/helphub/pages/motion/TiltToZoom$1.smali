.class Lcom/samsung/helphub/pages/motion/TiltToZoom$1;
.super Ljava/lang/Object;
.source "TiltToZoom.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/motion/TiltToZoom;->getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/motion/TiltToZoom;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/motion/TiltToZoom;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/samsung/helphub/pages/motion/TiltToZoom$1;->this$0:Lcom/samsung/helphub/pages/motion/TiltToZoom;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 68
    const-string v1, "com.sec.android.gallery3d"

    .line 70
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/helphub/pages/motion/TiltToZoom$1;->this$0:Lcom/samsung/helphub/pages/motion/TiltToZoom;

    iget-object v6, p0, Lcom/samsung/helphub/pages/motion/TiltToZoom$1;->this$0:Lcom/samsung/helphub/pages/motion/TiltToZoom;

    invoke-virtual {v6}, Lcom/samsung/helphub/pages/motion/TiltToZoom;->getActivity()Landroid/app/Activity;

    move-result-object v6

    # invokes: Lcom/samsung/helphub/pages/motion/TiltToZoom;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v5, v6, v1}, Lcom/samsung/helphub/pages/motion/TiltToZoom;->access$000(Lcom/samsung/helphub/pages/motion/TiltToZoom;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 71
    iget-object v5, p0, Lcom/samsung/helphub/pages/motion/TiltToZoom$1;->this$0:Lcom/samsung/helphub/pages/motion/TiltToZoom;

    invoke-virtual {v5}, Lcom/samsung/helphub/pages/motion/TiltToZoom;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "master_motion"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_0

    move v0, v3

    .line 72
    .local v0, "masterStatus":Z
    :goto_0
    iget-object v5, p0, Lcom/samsung/helphub/pages/motion/TiltToZoom$1;->this$0:Lcom/samsung/helphub/pages/motion/TiltToZoom;

    invoke-virtual {v5}, Lcom/samsung/helphub/pages/motion/TiltToZoom;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "motion_zooming"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_1

    move v2, v3

    .line 73
    .local v2, "settingStatus":Z
    :goto_1
    iget-object v4, p0, Lcom/samsung/helphub/pages/motion/TiltToZoom$1;->this$0:Lcom/samsung/helphub/pages/motion/TiltToZoom;

    # getter for: Lcom/samsung/helphub/pages/motion/TiltToZoom;->isClicked:Z
    invoke-static {v4}, Lcom/samsung/helphub/pages/motion/TiltToZoom;->access$100(Lcom/samsung/helphub/pages/motion/TiltToZoom;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 85
    .end local v0    # "masterStatus":Z
    .end local v2    # "settingStatus":Z
    :goto_2
    return-void

    :cond_0
    move v0, v4

    .line 71
    goto :goto_0

    .restart local v0    # "masterStatus":Z
    :cond_1
    move v2, v4

    .line 72
    goto :goto_1

    .line 75
    .restart local v2    # "settingStatus":Z
    :cond_2
    iget-object v4, p0, Lcom/samsung/helphub/pages/motion/TiltToZoom$1;->this$0:Lcom/samsung/helphub/pages/motion/TiltToZoom;

    # setter for: Lcom/samsung/helphub/pages/motion/TiltToZoom;->isClicked:Z
    invoke-static {v4, v3}, Lcom/samsung/helphub/pages/motion/TiltToZoom;->access$202(Lcom/samsung/helphub/pages/motion/TiltToZoom;Z)Z

    .line 76
    if-eqz v0, :cond_3

    if-nez v2, :cond_4

    .line 77
    :cond_3
    iget-object v3, p0, Lcom/samsung/helphub/pages/motion/TiltToZoom$1;->this$0:Lcom/samsung/helphub/pages/motion/TiltToZoom;

    const v4, 0x7f0a0562

    const v5, 0x7f0a0563

    # invokes: Lcom/samsung/helphub/pages/motion/TiltToZoom;->showEnableSettingDialog(II)V
    invoke-static {v3, v4, v5}, Lcom/samsung/helphub/pages/motion/TiltToZoom;->access$300(Lcom/samsung/helphub/pages/motion/TiltToZoom;II)V

    goto :goto_2

    .line 80
    :cond_4
    iget-object v3, p0, Lcom/samsung/helphub/pages/motion/TiltToZoom$1;->this$0:Lcom/samsung/helphub/pages/motion/TiltToZoom;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/motion/TiltToZoom;->startTutorial()V

    goto :goto_2

    .line 83
    .end local v0    # "masterStatus":Z
    .end local v2    # "settingStatus":Z
    :cond_5
    iget-object v3, p0, Lcom/samsung/helphub/pages/motion/TiltToZoom$1;->this$0:Lcom/samsung/helphub/pages/motion/TiltToZoom;

    # invokes: Lcom/samsung/helphub/pages/motion/TiltToZoom;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v3, v1}, Lcom/samsung/helphub/pages/motion/TiltToZoom;->access$400(Lcom/samsung/helphub/pages/motion/TiltToZoom;Ljava/lang/String;)V

    goto :goto_2
.end method
