.class public Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "MultiWindowTray.java"


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field mMultiWindowHelpUtil:Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 41
    new-instance v0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray$1;-><init>(Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static isNewMultiWindowHidden(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    const/4 v2, 0x1

    .line 62
    .local v2, "result":Z
    const/4 v1, 0x0

    .line 63
    .local v1, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 65
    if-eqz v1, :cond_0

    .line 66
    const-string v3, "com.sec.feature.multiwindow"

    invoke-virtual {v1, v3}, Landroid/content/pm/PackageManager;->getSystemFeatureLevel(Ljava/lang/String;)I

    move-result v0

    .line 67
    .local v0, "iFeatureLevel":I
    const/4 v3, 0x2

    if-ge v0, v3, :cond_1

    .line 68
    const/4 v2, 0x1

    .line 73
    .end local v0    # "iFeatureLevel":I
    :cond_0
    :goto_0
    return v2

    .line 70
    .restart local v0    # "iFeatureLevel":I
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    new-instance v2, Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;->mMultiWindowHelpUtil:Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

    .line 34
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 35
    .local v1, "result":Landroid/view/View;
    const v2, 0x7f0d03a1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 36
    .local v0, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "multi_window_tray"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 38
    return-object v1
.end method
