.class Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray$1;
.super Ljava/lang/Object;
.source "MultiWindowTray.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray$1;->this$0:Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 45
    iget-object v1, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray$1;->this$0:Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;

    iget-object v1, v1, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;->mMultiWindowHelpUtil:Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

    iget-object v2, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray$1;->this$0:Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;->checkTalkbackEnable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    iget-object v1, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray$1;->this$0:Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;

    iget-object v1, v1, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;->mMultiWindowHelpUtil:Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

    const v2, 0x7f0a00e4

    const v3, 0x7f0a00e5

    const-string v4, "com.sec.android.app.flashBarService.action.TryMultiWindowTray"

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;->showTalkBackDisablePopup(IILjava/lang/String;)V

    .line 52
    :goto_0
    return-void

    .line 49
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.flashBarService.action.TryMultiWindowTray"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 50
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray$1;->this$0:Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTray;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
