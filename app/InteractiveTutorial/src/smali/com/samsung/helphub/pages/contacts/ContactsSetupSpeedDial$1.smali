.class Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial$1;
.super Ljava/lang/Object;
.source "ContactsSetupSpeedDial.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 32
    const-string v1, "com.android.contacts"

    .line 34
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;

    iget-object v3, p0, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->access$000(Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 35
    iget-object v2, p0, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;

    # getter for: Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->access$100(Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 44
    :goto_0
    return-void

    .line 37
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->access$202(Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;Z)Z

    .line 38
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.contacts.action.SPEEDDIAL_TUTORIAL"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 39
    .local v0, "mIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 42
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial$1;->this$0:Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;

    # invokes: Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->access$300(Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;Ljava/lang/String;)V

    goto :goto_0
.end method
