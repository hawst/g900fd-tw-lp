.class Lcom/samsung/helphub/pages/gesture/AirMove$1;
.super Ljava/lang/Object;
.source "AirMove.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/gesture/AirMove;->getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/gesture/AirMove;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/gesture/AirMove;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/helphub/pages/gesture/AirMove$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirMove;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 62
    iget-object v4, p0, Lcom/samsung/helphub/pages/gesture/AirMove$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirMove;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/gesture/AirMove;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "air_motion_engine"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_0

    move v0, v2

    .line 63
    .local v0, "masterStatus":Z
    :goto_0
    iget-object v4, p0, Lcom/samsung/helphub/pages/gesture/AirMove$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirMove;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/gesture/AirMove;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "air_motion_item_move"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_1

    move v1, v2

    .line 64
    .local v1, "settingStatus":Z
    :goto_1
    iget-object v3, p0, Lcom/samsung/helphub/pages/gesture/AirMove$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirMove;

    # getter for: Lcom/samsung/helphub/pages/gesture/AirMove;->isClicked:Z
    invoke-static {v3}, Lcom/samsung/helphub/pages/gesture/AirMove;->access$000(Lcom/samsung/helphub/pages/gesture/AirMove;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 74
    :goto_2
    return-void

    .end local v0    # "masterStatus":Z
    .end local v1    # "settingStatus":Z
    :cond_0
    move v0, v3

    .line 62
    goto :goto_0

    .restart local v0    # "masterStatus":Z
    :cond_1
    move v1, v3

    .line 63
    goto :goto_1

    .line 66
    .restart local v1    # "settingStatus":Z
    :cond_2
    iget-object v3, p0, Lcom/samsung/helphub/pages/gesture/AirMove$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirMove;

    # setter for: Lcom/samsung/helphub/pages/gesture/AirMove;->isClicked:Z
    invoke-static {v3, v2}, Lcom/samsung/helphub/pages/gesture/AirMove;->access$102(Lcom/samsung/helphub/pages/gesture/AirMove;Z)Z

    .line 67
    if-eqz v0, :cond_3

    if-nez v1, :cond_4

    .line 68
    :cond_3
    iget-object v2, p0, Lcom/samsung/helphub/pages/gesture/AirMove$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirMove;

    const v3, 0x7f0a0564

    const v4, 0x7f0a0568

    # invokes: Lcom/samsung/helphub/pages/gesture/AirMove;->showEnableSettingDialog(II)V
    invoke-static {v2, v3, v4}, Lcom/samsung/helphub/pages/gesture/AirMove;->access$200(Lcom/samsung/helphub/pages/gesture/AirMove;II)V

    goto :goto_2

    .line 72
    :cond_4
    iget-object v2, p0, Lcom/samsung/helphub/pages/gesture/AirMove$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirMove;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/gesture/AirMove;->startTutorial()V

    goto :goto_2
.end method
