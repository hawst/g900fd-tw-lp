.class public Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "ContactsSetupSpeedDial.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 28
    new-instance v0, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial$1;-><init>(Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->isClicked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->isClicked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/helphub/pages/contacts/ContactsSetupSpeedDial;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 22
    .local v1, "result":Landroid/view/View;
    const v2, 0x7f0d0035

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 25
    .local v0, "mListItem4":Landroid/widget/TextView;
    return-object v1
.end method
