.class Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome$1;
.super Ljava/lang/Object;
.source "TranslatorTranslateSome.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome$1;->this$0:Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 16
    const-string v1, "com.sec.android.app.translator"

    .line 18
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome$1;->this$0:Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;

    iget-object v3, p0, Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome$1;->this$0:Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;->access$000(Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 19
    iget-object v2, p0, Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome$1;->this$0:Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;

    # getter for: Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;->access$100(Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 30
    :goto_0
    return-void

    .line 21
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome$1;->this$0:Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;

    # setter for: Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;->isClicked:Z
    invoke-static {v2, v4}, Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;->access$202(Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;Z)Z

    .line 22
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 23
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.translator.START_HELP_MODE_TRANSLATE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 24
    const-string v2, "helpMode"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 25
    iget-object v2, p0, Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome$1;->this$0:Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 28
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome$1;->this$0:Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;

    # invokes: Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;->access$300(Lcom/samsung/helphub/pages/translator/TranslatorTranslateSome;Ljava/lang/String;)V

    goto :goto_0
.end method
