.class public Lcom/samsung/helphub/pages/account/SamsungAccountPageFragment;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "SamsungAccountPageFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 25
    .local v0, "result":Landroid/view/View;
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "VZW"

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isCurrentCarrier(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 26
    const v2, 0x7f0d0036

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 27
    .local v1, "textChange":Landroid/widget/TextView;
    const v2, 0x7f0a0108

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 29
    .end local v1    # "textChange":Landroid/widget/TextView;
    :cond_0
    return-object v0
.end method
