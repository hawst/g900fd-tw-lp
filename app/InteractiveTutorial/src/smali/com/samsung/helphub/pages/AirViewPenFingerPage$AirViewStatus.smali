.class public final enum Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;
.super Ljava/lang/Enum;
.source "AirViewPenFingerPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/AirViewPenFingerPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AirViewStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

.field public static final enum AllEnalbed:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

.field public static final enum DisabledAirView:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

.field public static final enum DisabledFingerMode:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

.field public static final enum DisabledFuction:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

.field public static final enum DisabledPenMode:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 52
    new-instance v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    const-string v1, "DisabledAirView"

    invoke-direct {v0, v1, v7, v3}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->DisabledAirView:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    new-instance v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    const-string v1, "DisabledPenMode"

    invoke-direct {v0, v1, v3, v4}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->DisabledPenMode:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    new-instance v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    const-string v1, "DisabledFingerMode"

    invoke-direct {v0, v1, v4, v5}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->DisabledFingerMode:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    new-instance v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    const-string v1, "DisabledFuction"

    invoke-direct {v0, v1, v5, v6}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->DisabledFuction:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    new-instance v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    const-string v1, "AllEnalbed"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->AllEnalbed:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    .line 51
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    sget-object v1, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->DisabledAirView:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->DisabledPenMode:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->DisabledFingerMode:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->DisabledFuction:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->AllEnalbed:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->$VALUES:[Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput p3, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->value:I

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    const-class v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    return-object v0
.end method

.method public static values()[Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->$VALUES:[Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    invoke-virtual {v0}, [Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    return-object v0
.end method
