.class Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;
.super Ljava/lang/Object;
.source "AirViewMagnifier.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/airview/AirViewMagnifier;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v9, 0x7f0a00bd

    const v8, 0x7f0a00bc

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 67
    iget-object v6, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    invoke-virtual {v6}, Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "finger_air_view"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_0

    move v2, v4

    .line 68
    .local v2, "masterStatus":Z
    :goto_0
    iget-object v6, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    invoke-virtual {v6}, Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "finger_air_view_magnifier"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_1

    move v3, v4

    .line 69
    .local v3, "settingStatus":Z
    :goto_1
    iget-object v6, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    invoke-virtual {v6}, Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "accessibility_display_magnification_enabled"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_2

    move v0, v4

    .line 71
    .local v0, "MagnificationResult":Z
    :goto_2
    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    # getter for: Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->isClicked:Z
    invoke-static {v5}, Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->access$000(Lcom/samsung/helphub/pages/airview/AirViewMagnifier;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 89
    :goto_3
    return-void

    .end local v0    # "MagnificationResult":Z
    .end local v2    # "masterStatus":Z
    .end local v3    # "settingStatus":Z
    :cond_0
    move v2, v5

    .line 67
    goto :goto_0

    .restart local v2    # "masterStatus":Z
    :cond_1
    move v3, v5

    .line 68
    goto :goto_1

    .restart local v3    # "settingStatus":Z
    :cond_2
    move v0, v5

    .line 69
    goto :goto_2

    .line 73
    .restart local v0    # "MagnificationResult":Z
    :cond_3
    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    # setter for: Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->isClicked:Z
    invoke-static {v5, v4}, Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->access$102(Lcom/samsung/helphub/pages/airview/AirViewMagnifier;Z)Z

    .line 74
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    invoke-virtual {v5, v8}, Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    const v6, 0x7f0a0572

    invoke-virtual {v5, v6}, Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n- "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    const v6, 0x7f0a0573

    invoke-virtual {v5, v6}, Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n- "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    const v6, 0x7f0a0574

    invoke-virtual {v5, v6}, Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "dialog_content":Ljava/lang/String;
    if-eqz v2, :cond_4

    if-nez v3, :cond_5

    :cond_4
    iget-object v4, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_5

    if-nez v0, :cond_5

    .line 79
    iget-object v4, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->showEnableSettingDialog(II)V
    invoke-static {v4, v9, v8}, Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->access$200(Lcom/samsung/helphub/pages/airview/AirViewMagnifier;II)V

    goto :goto_3

    .line 82
    :cond_5
    iget-object v4, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_6

    if-eqz v0, :cond_7

    .line 83
    :cond_6
    iget-object v4, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->showEnableSettingDialog(ILjava/lang/String;)V
    invoke-static {v4, v9, v1}, Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->access$300(Lcom/samsung/helphub/pages/airview/AirViewMagnifier;ILjava/lang/String;)V

    goto/16 :goto_3

    .line 87
    :cond_7
    iget-object v4, p0, Lcom/samsung/helphub/pages/airview/AirViewMagnifier$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewMagnifier;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/airview/AirViewMagnifier;->startTutorial()V

    goto/16 :goto_3
.end method
