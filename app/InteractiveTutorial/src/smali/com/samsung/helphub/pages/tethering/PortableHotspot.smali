.class public Lcom/samsung/helphub/pages/tethering/PortableHotspot;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "PortableHotspot.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 37
    new-instance v0, Lcom/samsung/helphub/pages/tethering/PortableHotspot$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/tethering/PortableHotspot$1;-><init>(Lcom/samsung/helphub/pages/tethering/PortableHotspot;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/tethering/PortableHotspot;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/tethering/PortableHotspot;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/tethering/PortableHotspot;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/tethering/PortableHotspot;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/tethering/PortableHotspot;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/tethering/PortableHotspot;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/tethering/PortableHotspot;->isClicked:Z

    return p1
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/helphub/pages/tethering/PortableHotspot;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v8, 0x7f0d0035

    .line 22
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 23
    .local v0, "result":Landroid/view/View;
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isSettingsSupportMenuTreeForK()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "ro.product.name"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ja"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "ro.product.name"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "jf"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "ro.product.device"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ks01lte"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 24
    :cond_0
    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 25
    .local v2, "webview":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/tethering/PortableHotspot;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0849

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, " <a href=http://www.android.com/tether#wifi>http://www.android.com/tether#wifi</a></body></html>"

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 26
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 29
    .end local v2    # "webview":Landroid/widget/TextView;
    :cond_1
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 30
    const/4 v1, 0x0

    .line 31
    .local v1, "tv":Landroid/widget/TextView;
    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "tv":Landroid/widget/TextView;
    check-cast v1, Landroid/widget/TextView;

    .line 32
    .restart local v1    # "tv":Landroid/widget/TextView;
    const v3, 0x7f0a0858

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 35
    .end local v1    # "tv":Landroid/widget/TextView;
    :cond_2
    return-object v0
.end method
