.class public Lcom/samsung/helphub/pages/multiwindow/MultiWindowShow;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "MultiWindowShow.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 32
    new-instance v0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowShow$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowShow$1;-><init>(Lcom/samsung/helphub/pages/multiwindow/MultiWindowShow;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowShow;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowShow;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 23
    .local v1, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowShow;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 24
    const v2, 0x7f0d03a0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 26
    .local v0, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowShow;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "multi_window_show_hide_tray"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 29
    .end local v0    # "mImageView":Landroid/widget/ImageView;
    :cond_0
    return-object v1
.end method
