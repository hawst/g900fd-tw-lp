.class public Lcom/samsung/helphub/pages/HelpPageFragment;
.super Landroid/app/Fragment;
.source "HelpPageFragment.java"


# static fields
.field public static final ARGUMENTS_KEY_ACTIVITY_TITLE:Ljava/lang/String; = "page_fragment_argument:activity_title"

.field private static final BUNDLE_KEY_ACTIVITY_TITLE:Ljava/lang/String; = "page_fragment:activity_title"

.field private static final BUNDLE_KEY_HELP_HEADER:Ljava/lang/String; = "page_fragment:help_header"


# instance fields
.field private mActivityTitleResId:I

.field private mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/helphub/pages/HelpPageFragment;->mActivityTitleResId:I

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    .line 47
    return-void
.end method

.method private makePSText(III)Ljava/lang/String;
    .locals 7
    .param p1, "textID"    # I
    .param p2, "TextPs1Id"    # I
    .param p3, "TextPs2Id"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v2, -0x1

    .line 359
    const/4 v0, 0x0

    .line 362
    .local v0, "TextPsCount":I
    if-eq p2, v2, :cond_0

    .line 363
    add-int/lit8 v0, v0, 0x1

    .line 364
    :cond_0
    if-eq p3, v2, :cond_1

    .line 365
    add-int/lit8 v0, v0, 0x1

    .line 367
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 375
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 378
    .local v1, "result":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 369
    .end local v1    # "result":Ljava/lang/String;
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, p1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 370
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0

    .line 372
    .end local v1    # "result":Ljava/lang/String;
    :pswitch_1
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, p1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 373
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0

    .line 367
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected getPkgVersionName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 382
    const-string v2, ""

    .line 384
    .local v2, "version":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/16 v4, 0x80

    invoke-virtual {v3, p2, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 385
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    .line 386
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 391
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return-object v2

    .line 388
    :catch_0
    move-exception v0

    .line 389
    .local v0, "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method protected isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 342
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v0

    .line 344
    .local v0, "enable":I
    const/4 v3, 0x2

    if-eq v3, v0, :cond_0

    const/4 v3, 0x3

    if-ne v3, v0, :cond_1

    .line 346
    :cond_0
    const-string v3, "Help_TryIt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is diabled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    .end local v0    # "enable":I
    :goto_0
    return v2

    .line 349
    .restart local v0    # "enable":I
    :cond_1
    const-string v3, "Help_TryIt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is enabled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    const/4 v2, 0x1

    goto :goto_0

    .line 352
    .end local v0    # "enable":I
    :catch_0
    move-exception v1

    .line 353
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    const-string v3, "Help_TryIt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not installed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected makeDisablePopup(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 332
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 333
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.popupuireceiver"

    const-string v2, "com.sec.android.app.popupuireceiver.DisableApp"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 335
    const-string v1, "app_package_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 337
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/helphub/pages/HelpPageFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 338
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 51
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/samsung/helphub/HelpHubActivityBase;

    invoke-virtual {v5}, Lcom/samsung/helphub/HelpHubActivityBase;->isTwoPane()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 56
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 57
    .local v3, "parent":Landroid/view/ViewGroup;
    instance-of v5, v3, Landroid/widget/ScrollView;

    if-eqz v5, :cond_0

    .line 58
    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 60
    :cond_0
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 61
    .local v1, "childrenCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 62
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 63
    .local v0, "child":Landroid/view/View;
    instance-of v5, v0, Landroid/widget/ScrollView;

    if-eqz v5, :cond_1

    .line 64
    invoke-virtual {v0, v6}, Landroid/view/View;->setFocusable(Z)V

    .line 61
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 70
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childrenCount":I
    .end local v2    # "i":I
    .end local v3    # "parent":Landroid/view/ViewGroup;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 71
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0d0016

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 72
    .local v4, "view":Landroid/view/View;
    if-eqz v4, :cond_3

    .line 73
    const/16 v5, 0x40

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 77
    .end local v4    # "view":Landroid/view/View;
    :cond_3
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 34
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    if-eqz p3, :cond_2

    .line 85
    const-string v30, "page_fragment:help_header"

    move-object/from16 v0, p3

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v30

    check-cast v30, Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    .line 87
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    if-eqz v30, :cond_0

    .line 88
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v30

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/helphub/pages/HelpPageFragment;->mActivityTitleResId:I

    .line 110
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    if-eqz v30, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/headers/HelpHeader;->getItemLayoutID()I

    move-result v30

    if-gtz v30, :cond_3

    .line 111
    :cond_1
    new-instance v30, Ljava/lang/IllegalArgumentException;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "There is no Help item header or expected data within. class="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v30

    .line 95
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    .line 96
    .local v5, "args":Landroid/os/Bundle;
    if-eqz v5, :cond_0

    .line 97
    const-string v30, "helpub:header"

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_0

    .line 98
    const-string v30, "helpub:header"

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v30

    check-cast v30, Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    .line 99
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    if-eqz v30, :cond_0

    .line 100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v30

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/helphub/pages/HelpPageFragment;->mActivityTitleResId:I

    goto :goto_0

    .line 116
    .end local v5    # "args":Landroid/os/Bundle;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/headers/HelpHeader;->getItemLayoutID()I

    move-result v30

    const/16 v31, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v30

    move-object/from16 v2, p2

    move/from16 v3, v31

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v20

    .line 119
    .local v20, "result":Landroid/view/View;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v30

    check-cast v30, Lcom/samsung/helphub/HelpHubActivityBase;

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/HelpHubActivityBase;->isTwoPane()Z

    move-result v30

    if-nez v30, :cond_4

    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v30

    if-eqz v30, :cond_5

    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v30

    if-nez v30, :cond_5

    .line 120
    :cond_4
    const v30, 0x7f0d0016

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    .line 121
    .local v27, "view":Landroid/view/View;
    if-eqz v27, :cond_5

    .line 122
    const/16 v30, 0x8

    move-object/from16 v0, v27

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 162
    .end local v27    # "view":Landroid/view/View;
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v30

    check-cast v30, Lcom/samsung/helphub/HelpHubActivityBase;

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/HelpHubActivityBase;->isTwoPane()Z

    move-result v30

    if-eqz v30, :cond_6

    .line 163
    const v30, 0x7f0d0112

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    .line 164
    .restart local v27    # "view":Landroid/view/View;
    if-eqz v27, :cond_6

    .line 165
    const/16 v30, 0x8

    move-object/from16 v0, v27

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 169
    .end local v27    # "view":Landroid/view/View;
    :cond_6
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v30

    if-eqz v30, :cond_b

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/headers/HelpHeader;->getItemLayoutID()I

    move-result v30

    const v31, 0x7f040196

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_7

    .line 171
    const v30, 0x7f0d005b

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v28

    check-cast v28, Landroid/widget/TextView;

    .line 172
    .local v28, "wifistr":Landroid/widget/TextView;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f0a09ef

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 173
    .local v29, "wifistr_chn":Ljava/lang/String;
    if-eqz v28, :cond_7

    invoke-virtual/range {v28 .. v29}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    .end local v28    # "wifistr":Landroid/widget/TextView;
    .end local v29    # "wifistr_chn":Ljava/lang/String;
    :cond_7
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isMultiSimModel()Z

    move-result v30

    if-eqz v30, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/headers/HelpHeader;->getItemLayoutID()I

    move-result v30

    const v31, 0x7f0400ce

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_b

    .line 177
    const v30, 0x7f0d0034

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    .line 178
    .restart local v27    # "view":Landroid/view/View;
    if-eqz v27, :cond_8

    .line 179
    const/16 v30, 0x8

    move-object/from16 v0, v27

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 181
    :cond_8
    const v30, 0x7f0d0037

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    .line 182
    if-eqz v27, :cond_9

    .line 183
    const/16 v30, 0x8

    move-object/from16 v0, v27

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 185
    :cond_9
    const v30, 0x7f0d0038

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    .line 186
    if-eqz v27, :cond_a

    .line 187
    const/16 v30, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 189
    :cond_a
    const v30, 0x7f0d0039

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    .line 190
    if-eqz v27, :cond_b

    .line 191
    const/16 v30, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 197
    .end local v27    # "view":Landroid/view/View;
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/headers/HelpHeader;->getItemLayoutID()I

    move-result v30

    const v31, 0x7f0400b1

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_c

    .line 198
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v30

    const-string v31, "com.sec.feature.multiwindow"

    invoke-virtual/range {v30 .. v31}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    .line 199
    .local v4, "UseMultiWindow":Z
    if-nez v4, :cond_c

    .line 200
    const v30, 0x7f0d0361

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v25

    check-cast v25, Landroid/widget/TextView;

    .line 201
    .local v25, "textView":Landroid/widget/TextView;
    if-eqz v25, :cond_c

    .line 202
    const/16 v30, 0x8

    move-object/from16 v0, v25

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 208
    .end local v4    # "UseMultiWindow":Z
    .end local v25    # "textView":Landroid/widget/TextView;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/headers/HelpHeader;->getItemLayoutID()I

    move-result v30

    const v31, 0x7f04006d

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_d

    .line 209
    const v30, 0x7f0d0036

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 210
    .local v9, "eMeetingTextView":Landroid/widget/TextView;
    const v30, 0x7f0a081c

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/pages/HelpPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v30

    const/16 v31, 0x2

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    const/16 v33, 0x14

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    aput-object v33, v31, v32

    const/16 v32, 0x1

    const/16 v33, 0xb

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    aput-object v33, v31, v32

    invoke-static/range {v30 .. v31}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 211
    .local v8, "eMeetingRequire":Ljava/lang/String;
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    .end local v8    # "eMeetingRequire":Ljava/lang/String;
    .end local v9    # "eMeetingTextView":Landroid/widget/TextView;
    :cond_d
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isGridListMenu()Z

    move-result v30

    if-eqz v30, :cond_e

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/headers/HelpHeader;->getItemLayoutID()I

    move-result v30

    const v31, 0x7f0400f0

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_e

    .line 217
    const v30, 0x7f0d0032

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 218
    .local v21, "sbeamView":Lcom/samsung/helphub/widget/HelpHubTextView;
    const v30, 0x7f0d01e2

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 219
    .local v22, "sbeamView2":Lcom/samsung/helphub/widget/HelpHubTextView;
    if-eqz v21, :cond_e

    if-eqz v22, :cond_e

    .line 220
    const/16 v30, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setVisibility(I)V

    .line 221
    const/16 v30, 0x8

    move-object/from16 v0, v22

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setVisibility(I)V

    .line 226
    .end local v21    # "sbeamView":Lcom/samsung/helphub/widget/HelpHubTextView;
    .end local v22    # "sbeamView2":Lcom/samsung/helphub/widget/HelpHubTextView;
    :cond_e
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v30

    if-nez v30, :cond_f

    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isAmericano()Z

    move-result v30

    if-nez v30, :cond_f

    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaCTCModel()Z

    move-result v30

    if-eqz v30, :cond_10

    .line 227
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/headers/HelpHeader;->getItemLayoutID()I

    move-result v30

    const v31, 0x7f0400ee

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_10

    .line 228
    const v30, 0x7f0d0032

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 229
    .local v16, "nfcView":Lcom/samsung/helphub/widget/HelpHubTextView;
    const v30, 0x7f0d01e2

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 230
    .local v17, "nfcView2":Lcom/samsung/helphub/widget/HelpHubTextView;
    if-eqz v16, :cond_10

    if-eqz v17, :cond_10

    .line 231
    const/16 v30, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setVisibility(I)V

    .line 232
    const/16 v30, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setVisibility(I)V

    .line 238
    .end local v16    # "nfcView":Lcom/samsung/helphub/widget/HelpHubTextView;
    .end local v17    # "nfcView2":Lcom/samsung/helphub/widget/HelpHubTextView;
    :cond_10
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v30

    if-nez v30, :cond_11

    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isAmericano()Z

    move-result v30

    if-nez v30, :cond_11

    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaCTCModel()Z

    move-result v30

    if-eqz v30, :cond_12

    .line 239
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/headers/HelpHeader;->getItemLayoutID()I

    move-result v30

    const v31, 0x7f0400ef

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_12

    .line 240
    const v30, 0x7f0d0032

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 241
    .restart local v16    # "nfcView":Lcom/samsung/helphub/widget/HelpHubTextView;
    const v30, 0x7f0d01e2

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 242
    .restart local v17    # "nfcView2":Lcom/samsung/helphub/widget/HelpHubTextView;
    if-eqz v16, :cond_12

    if-eqz v17, :cond_12

    .line 243
    const/16 v30, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setVisibility(I)V

    .line 244
    const/16 v30, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setVisibility(I)V

    .line 249
    .end local v16    # "nfcView":Lcom/samsung/helphub/widget/HelpHubTextView;
    .end local v17    # "nfcView2":Lcom/samsung/helphub/widget/HelpHubTextView;
    :cond_12
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v30

    if-nez v30, :cond_13

    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isAmericano()Z

    move-result v30

    if-eqz v30, :cond_14

    .line 250
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/headers/HelpHeader;->getItemLayoutID()I

    move-result v30

    const v31, 0x7f0400f1

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_14

    .line 251
    const v30, 0x7f0d01e3

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 252
    .local v23, "tapandpayView":Lcom/samsung/helphub/widget/HelpHubTextView;
    const v30, 0x7f0d01e4

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 253
    .local v24, "tapandpayView2":Lcom/samsung/helphub/widget/HelpHubTextView;
    if-eqz v23, :cond_14

    if-eqz v24, :cond_14

    .line 254
    const/16 v30, 0x8

    move-object/from16 v0, v23

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setVisibility(I)V

    .line 255
    const/16 v30, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setVisibility(I)V

    .line 261
    .end local v23    # "tapandpayView":Lcom/samsung/helphub/widget/HelpHubTextView;
    .end local v24    # "tapandpayView2":Lcom/samsung/helphub/widget/HelpHubTextView;
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/headers/HelpHeader;->getItemLayoutID()I

    move-result v30

    const v31, 0x7f040071

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_15

    .line 262
    const v30, 0x7f0d003f

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 263
    .local v18, "registerFinger":Landroid/widget/TextView;
    const v30, 0x7f0a0766

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/pages/HelpPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v30

    const/16 v31, 0x1

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    const v33, 0x7f0a0765

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/pages/HelpPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v31, v32

    invoke-static/range {v30 .. v31}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 265
    .local v10, "fingerScanner":Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/samsung/android/fingerprint/FingerprintManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/fingerprint/FingerprintManager;

    move-result-object v15

    .line 268
    .local v15, "mFingerPrintManager":Lcom/samsung/android/fingerprint/FingerprintManager;
    invoke-static {}, Lcom/samsung/android/fingerprint/FingerprintManager;->getSensorType()I

    move-result v30

    const/16 v31, 0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_15

    .line 270
    const v30, 0x7f0d0040

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 271
    .local v19, "registerFinger2":Landroid/widget/TextView;
    const v30, 0x7f0a0767

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/pages/HelpPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v30

    const/16 v31, 0x1

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-virtual {v15}, Lcom/samsung/android/fingerprint/FingerprintManager;->getEnrollRepeatCount()I

    move-result v33

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    aput-object v33, v31, v32

    invoke-static/range {v30 .. v31}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 272
    .local v11, "fingerScanner2":Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    const v30, 0x7f0d02cb

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 274
    .local v12, "image1":Landroid/widget/ImageView;
    const v30, 0x7f02013f

    move/from16 v0, v30

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 275
    const v30, 0x7f0d02d1

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    .line 276
    .local v13, "image2":Landroid/widget/ImageView;
    const v30, 0x7f020141

    move/from16 v0, v30

    invoke-virtual {v13, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 281
    .end local v10    # "fingerScanner":Ljava/lang/String;
    .end local v11    # "fingerScanner2":Ljava/lang/String;
    .end local v12    # "image1":Landroid/widget/ImageView;
    .end local v13    # "image2":Landroid/widget/ImageView;
    .end local v15    # "mFingerPrintManager":Lcom/samsung/android/fingerprint/FingerprintManager;
    .end local v18    # "registerFinger":Landroid/widget/TextView;
    .end local v19    # "registerFinger2":Landroid/widget/TextView;
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/samsung/helphub/headers/HelpHeader;->getItemLayoutID()I

    move-result v30

    const v31, 0x7f040115

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_1a

    .line 282
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isPowerKeyOnTop()Z

    move-result v30

    if-eqz v30, :cond_16

    .line 283
    const v30, 0x7f0d03c4

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 285
    .local v14, "imageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    const v30, 0x7f07006c

    move/from16 v0, v30

    invoke-virtual {v14, v0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->setDrawablesArray(I)V

    .line 289
    .end local v14    # "imageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_16
    const v30, 0x7f0d01d8

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 290
    .local v6, "descView":Lcom/samsung/helphub/widget/HelpHubTextView;
    const v30, 0x7f0d01d9

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 293
    .local v7, "descViewGreen":Lcom/samsung/helphub/widget/HelpHubTextView;
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isATTDevice()Z

    move-result v30

    if-nez v30, :cond_17

    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isTMODevice()Z

    move-result v30

    if-eqz v30, :cond_1b

    .line 294
    :cond_17
    if-eqz v6, :cond_18

    .line 295
    const/16 v30, 0x8

    move/from16 v0, v30

    invoke-virtual {v6, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->setVisibility(I)V

    .line 296
    :cond_18
    if-eqz v7, :cond_19

    .line 297
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v7, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->setVisibility(I)V

    .line 304
    :cond_19
    :goto_1
    const v30, 0x7f0d01d7

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TextView;

    .line 305
    .local v26, "tv":Landroid/widget/TextView;
    if-eqz v26, :cond_1a

    .line 306
    const v30, 0x7f0a08fa

    const/16 v31, 0x1

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    const/16 v33, 0x3

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    aput-object v33, v31, v32

    move-object/from16 v0, p0

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lcom/samsung/helphub/pages/HelpPageFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v26

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    .end local v6    # "descView":Lcom/samsung/helphub/widget/HelpHubTextView;
    .end local v7    # "descViewGreen":Lcom/samsung/helphub/widget/HelpHubTextView;
    .end local v26    # "tv":Landroid/widget/TextView;
    :cond_1a
    return-object v20

    .line 299
    .restart local v6    # "descView":Lcom/samsung/helphub/widget/HelpHubTextView;
    .restart local v7    # "descViewGreen":Lcom/samsung/helphub/widget/HelpHubTextView;
    :cond_1b
    if-eqz v7, :cond_1c

    .line 300
    const/16 v30, 0x8

    move/from16 v0, v30

    invoke-virtual {v7, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->setVisibility(I)V

    .line 301
    :cond_1c
    if-eqz v6, :cond_19

    .line 302
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v6, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 315
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 317
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/HelpHubActivityBase;

    invoke-virtual {v1}, Lcom/samsung/helphub/HelpHubActivityBase;->isTwoPane()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->isExternal()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget v1, p0, Lcom/samsung/helphub/pages/HelpPageFragment;->mActivityTitleResId:I

    if-lez v1, :cond_1

    .line 320
    iget v1, p0, Lcom/samsung/helphub/pages/HelpPageFragment;->mActivityTitleResId:I

    iget-object v2, p0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitlePs1Id()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getTitlePs2Id()I

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/helphub/pages/HelpPageFragment;->makePSText(III)Ljava/lang/String;

    move-result-object v0

    .line 321
    .local v0, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 323
    .end local v0    # "title":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 327
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 328
    const-string v0, "page_fragment:help_header"

    iget-object v1, p0, Lcom/samsung/helphub/pages/HelpPageFragment;->mHelpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 329
    const-string v0, "page_fragment:activity_title"

    iget v1, p0, Lcom/samsung/helphub/pages/HelpPageFragment;->mActivityTitleResId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 330
    return-void
.end method
