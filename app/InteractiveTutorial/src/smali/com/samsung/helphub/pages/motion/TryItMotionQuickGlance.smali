.class public Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "TryItMotionQuickGlance.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "TryItMotionQuickGlance"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;->showEnableSettingDialog(II)V

    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 57
    return-object p0
.end method

.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance$1;-><init>(Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;)V

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;->isClicked:Z

    .line 53
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 37
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "master_arc_motion"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 38
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "arc_motion_quick_glance"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 39
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.motions.ARC_MOTION_SETTINGS_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 40
    .local v0, "master_changed":Landroid/content/Intent;
    const-string v2, "isEnable"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 41
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 42
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.motions.ARC_MOTION_QUICK_GLANCE_SETTINGS_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 43
    .local v1, "setting_changed":Landroid/content/Intent;
    const-string v2, "isEnable"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 44
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 46
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;->startTutorial()V

    .line 47
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 25
    .local v1, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 26
    const v2, 0x7f0d0220

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 28
    .local v0, "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "tutorial_gesture_quick_glance"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 31
    .end local v0    # "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_0
    return-object v1
.end method

.method protected startTutorial()V
    .locals 2

    .prologue
    .line 84
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 85
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v1, "samsung.pickuptutorial.MOTIONGLANCEVIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/motion/TryItMotionQuickGlance;->startActivity(Landroid/content/Intent;)V

    .line 89
    return-void
.end method
