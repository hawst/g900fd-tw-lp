.class public Lcom/samsung/helphub/pages/airview/AirViewProgressBar;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "AirViewProgressBar.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# instance fields
.field public mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    .line 147
    new-instance v0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$2;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$2;-><init>(Lcom/samsung/helphub/pages/airview/AirViewProgressBar;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/airview/AirViewProgressBar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewProgressBar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->showEnableSettingDialog(II)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/airview/AirViewProgressBar;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewProgressBar;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->showEnableSettingDialog(ILjava/lang/String;)V

    return-void
.end method

.method private sendBroadcastForNetworkErrorPopup(I)V
    .locals 3
    .param p1, "status"    # I

    .prologue
    .line 140
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 141
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.popupuireceiver"

    const-string v2, "com.sec.android.app.popupuireceiver.popupNetworkError"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    const-string v1, "network_err_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 144
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->startActivityForResult(Landroid/content/Intent;I)V

    .line 145
    return-void
.end method


# virtual methods
.method public checkNetworkState()V
    .locals 6

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 119
    new-instance v1, Lcom/samsung/helphub/UpdateCheckThread;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->mHandler:Landroid/os/Handler;

    const-string v5, "com.samsung.helpplugin"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/samsung/helphub/UpdateCheckThread;-><init>(Landroid/content/Context;ZLandroid/os/Handler;Ljava/lang/String;)V

    .line 121
    .local v1, "thread":Lcom/samsung/helphub/UpdateCheckThread;
    invoke-virtual {v1}, Lcom/samsung/helphub/UpdateCheckThread;->start()V

    .line 137
    .end local v1    # "thread":Lcom/samsung/helphub/UpdateCheckThread;
    :goto_0
    return-void

    .line 123
    :cond_0
    const/4 v0, -0x1

    .line 124
    .local v0, "networkStatus":I
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isFligtMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 125
    const/4 v0, 0x0

    .line 135
    :goto_1
    invoke-direct {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->sendBroadcastForNetworkErrorPopup(I)V

    goto :goto_0

    .line 126
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isMobileDataOff(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 127
    const/4 v0, 0x1

    goto :goto_1

    .line 128
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isRoamingOff(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 129
    const/4 v0, 0x2

    goto :goto_1

    .line 130
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isReachToDataLimit(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 131
    const/4 v0, 0x3

    goto :goto_1

    .line 133
    :cond_4
    const/4 v0, 0x4

    goto :goto_1
.end method

.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 63
    return-object p0
.end method

.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;-><init>(Lcom/samsung/helphub/pages/airview/AirViewProgressBar;)V

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 42
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/helphub/utility/TackBackUtility;->turnOffTalkBack(Landroid/content/Context;)Z

    .line 43
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accessibility_display_magnification_enabled"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 45
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 46
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view_highlight"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 47
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view_pregress_bar_preview"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 49
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.gesture.FINGER_AIR_VIEW_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 50
    .local v0, "finger_air_view_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 51
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 53
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->startTutorial()V

    .line 54
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 31
    .local v1, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 32
    const v2, 0x7f0d023b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 34
    .local v0, "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "tutorial_air_view_preview_progress_bar"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 37
    .end local v0    # "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_0
    return-object v1
.end method

.method protected startTutorial()V
    .locals 3

    .prologue
    .line 96
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.START_HELP_VIDEO_PROGRESS_BAR_PREVIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 112
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "file:///system/media/video/video_help.mp4"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "video/mp4"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->startActivity(Landroid/content/Intent;)V

    .line 115
    return-void
.end method
