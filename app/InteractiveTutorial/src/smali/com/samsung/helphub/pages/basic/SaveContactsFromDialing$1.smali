.class Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing$1;
.super Ljava/lang/Object;
.source "SaveContactsFromDialing.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing;)V
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing$1;->this$0:Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 18
    iget-object v1, p0, Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing$1;->this$0:Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing;

    # getter for: Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing;->isClicked:Z
    invoke-static {v1}, Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing;->access$000(Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    :goto_0
    return-void

    .line 20
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing$1;->this$0:Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing;->isClicked:Z
    invoke-static {v1, v2}, Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing;->access$102(Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing;Z)Z

    .line 24
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.contacts.action.DialerHelpActivity"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 28
    .local v0, "mIntent":Landroid/content/Intent;
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "mIntent":Landroid/content/Intent;
    const-string v1, "com.android.contacts.action.DialerHelpActivity"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 30
    .restart local v0    # "mIntent":Landroid/content/Intent;
    const-string v1, "DialerGuideMode"

    const-string v2, "SAVE_CONTACTS_KEYPAD_TUTORIAL"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    iget-object v1, p0, Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing$1;->this$0:Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/basic/SaveContactsFromDialing;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
