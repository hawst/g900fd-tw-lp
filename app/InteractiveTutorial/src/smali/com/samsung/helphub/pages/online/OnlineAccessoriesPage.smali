.class public Lcom/samsung/helphub/pages/online/OnlineAccessoriesPage;
.super Lcom/samsung/helphub/pages/online/WebFragmentBase;
.source "OnlineAccessoriesPage.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getUrl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 12
    iget-boolean v2, p0, Lcom/samsung/helphub/pages/online/OnlineAccessoriesPage;->mTwoPane:Z

    if-nez v2, :cond_0

    .line 13
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/OnlineAccessoriesPage;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const v3, 0x7f0a0134

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(I)V

    .line 18
    :cond_0
    const-string v2, "ro.csc.sales_code"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 20
    .local v0, "custId":Ljava/lang/String;
    const-string v2, "CHN"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "CHM"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "CHU"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "CTC"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 22
    :cond_1
    const-string v1, "http://www.samsung.com.cn/siv/accessories"

    .line 27
    .local v1, "result":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 24
    .end local v1    # "result":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/OnlineAccessoriesPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a06b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0
.end method
