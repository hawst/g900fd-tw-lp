.class public Lcom/samsung/helphub/pages/storyalbum/Introduction;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "Introduction.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, -0x1

    .line 22
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v6

    .line 23
    .local v6, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/storyalbum/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 24
    const v7, 0x7f0d03f9

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 26
    .local v1, "mImageView_01":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 27
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/storyalbum/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const-string v8, "story_album_introduction_01"

    const-string v9, "drawable"

    const-string v10, "com.samsung.helpplugin"

    invoke-static {v7, v8, v9, v10}, Lcom/samsung/helphub/HelpHubCommon;->getResourceId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 30
    .local v5, "resId":I
    if-eq v5, v11, :cond_2

    .line 31
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/storyalbum/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 33
    .local v4, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v7, "com.samsung.helpplugin"

    invoke-virtual {v4, v7}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v3

    .line 35
    .local v3, "mResources":Landroid/content/res/Resources;
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    .end local v3    # "mResources":Landroid/content/res/Resources;
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    .end local v5    # "resId":I
    :cond_0
    :goto_0
    const v7, 0x7f0d03fa

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 47
    .local v2, "mImageView_02":Landroid/widget/ImageView;
    if-eqz v2, :cond_1

    .line 48
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/storyalbum/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const-string v8, "story_album_introduction_02"

    const-string v9, "drawable"

    const-string v10, "com.samsung.helpplugin"

    invoke-static {v7, v8, v9, v10}, Lcom/samsung/helphub/HelpHubCommon;->getResourceId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 51
    .restart local v5    # "resId":I
    if-eq v5, v11, :cond_3

    .line 52
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/storyalbum/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 54
    .restart local v4    # "pm":Landroid/content/pm/PackageManager;
    :try_start_1
    const-string v7, "com.samsung.helpplugin"

    invoke-virtual {v4, v7}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v3

    .line 56
    .restart local v3    # "mResources":Landroid/content/res/Resources;
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 67
    .end local v1    # "mImageView_01":Landroid/widget/ImageView;
    .end local v2    # "mImageView_02":Landroid/widget/ImageView;
    .end local v3    # "mResources":Landroid/content/res/Resources;
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    .end local v5    # "resId":I
    :cond_1
    :goto_1
    return-object v6

    .line 36
    .restart local v1    # "mImageView_01":Landroid/widget/ImageView;
    .restart local v4    # "pm":Landroid/content/pm/PackageManager;
    .restart local v5    # "resId":I
    :catch_0
    move-exception v0

    .line 38
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 41
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    :cond_2
    const-string v7, "HelpHub2.0"

    const-string v8, "The resource not downloaded yet"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 57
    .restart local v2    # "mImageView_02":Landroid/widget/ImageView;
    .restart local v4    # "pm":Landroid/content/pm/PackageManager;
    :catch_1
    move-exception v0

    .line 59
    .restart local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 62
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    :cond_3
    const-string v7, "HelpHub2.0"

    const-string v8, "The resource not downloaded yet"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
