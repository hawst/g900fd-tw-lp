.class public Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;
.super Landroid/widget/FrameLayout;
.source "TutorialDragDownAnimView.java"


# static fields
.field private static final START_DRAG_UP1:I = 0x1

.field private static final START_DRAG_UP2:I = 0x2

.field private static final START_DRAG_UP3:I = 0x3

.field private static final START_DRAG_UP4:I = 0x4

.field private static final TAG:Ljava/lang/String; = "STATUSBAR-DropDownAnimView"

.field private static final TUTORIAL_FINISH:I = 0x5


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDragDownImageView:[Landroid/widget/ImageView;

.field private mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;

.field mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x4

    .line 73
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    new-array v0, v1, [Landroid/graphics/drawable/AnimationDrawable;

    iput-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;

    .line 39
    new-array v0, v1, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownImageView:[Landroid/widget/ImageView;

    .line 123
    new-instance v0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;-><init>(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mHandler:Landroid/os/Handler;

    .line 77
    iput-object p1, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mContext:Landroid/content/Context;

    .line 81
    const v0, 0x7f040187

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 85
    iget-object v1, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownImageView:[Landroid/widget/ImageView;

    const/4 v2, 0x0

    const v0, 0x7f0d0423

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    .line 87
    iget-object v1, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownImageView:[Landroid/widget/ImageView;

    const/4 v2, 0x1

    const v0, 0x7f0d0424

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    .line 89
    iget-object v1, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownImageView:[Landroid/widget/ImageView;

    const/4 v2, 0x2

    const v0, 0x7f0d0425

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    .line 91
    iget-object v1, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownImageView:[Landroid/widget/ImageView;

    const/4 v2, 0x3

    const v0, 0x7f0d0426

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    .line 95
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)[Landroid/graphics/drawable/AnimationDrawable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)[Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownImageView:[Landroid/widget/ImageView;

    return-object v0
.end method

.method private hideDragDownAnimation()V
    .locals 2

    .prologue
    .line 241
    const-string v0, "STATUSBAR-DropDownAnimView"

    const-string v1, "hideDragDownAnimation()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    invoke-direct {p0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->stopDragDownAnimation()V

    .line 245
    return-void
.end method

.method private showDragDownAnimation()V
    .locals 2

    .prologue
    .line 261
    const-string v0, "STATUSBAR-DropDownAnimView"

    const-string v1, "showDragDownAnimation()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    invoke-direct {p0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->startDragDownAnimation()V

    .line 265
    return-void
.end method

.method private startDragDownAnimation()V
    .locals 2

    .prologue
    .line 251
    const-string v0, "STATUSBAR-DropDownAnimView"

    const-string v1, "startDragDownAnimation()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 255
    return-void
.end method

.method private stopDragDownAnimation()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 225
    const-string v0, "STATUSBAR-DropDownAnimView"

    const-string v1, "stopDragDownAnimation()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;

    aget-object v0, v0, v3

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;

    aget-object v0, v0, v4

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 233
    :cond_2
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;

    aget-object v0, v0, v5

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;

    aget-object v0, v0, v5

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 235
    :cond_3
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 117
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 119
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 275
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 277
    invoke-direct {p0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->stopDragDownAnimation()V

    .line 279
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 103
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 107
    invoke-direct {p0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->showDragDownAnimation()V

    .line 109
    return-void
.end method
