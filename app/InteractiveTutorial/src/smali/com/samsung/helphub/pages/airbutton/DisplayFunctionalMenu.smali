.class public Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "DisplayFunctionalMenu.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->isClicked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;
    .param p1, "x1"    # Z

    .prologue
    .line 13
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->isClicked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->showEnableSettingDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 58
    return-object p0
.end method

.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;-><init>(Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;)V

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->isClicked:Z

    .line 54
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 43
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "air_button_onoff"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 45
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.gesture.AIR_VIEW_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 46
    .local v0, "air_view_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 47
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 48
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->startTutorial()V

    .line 49
    return-void
.end method

.method protected startTutorial()V
    .locals 3

    .prologue
    .line 63
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 64
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "image/jpg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    const-string v1, "com.sec.android.gallery3d"

    const-string v2, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    const-string v1, "IsHelpMode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 67
    const-string v1, "HelpMode"

    const-string v2, "AIR_BUTTON"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->startActivity(Landroid/content/Intent;)V

    .line 69
    return-void
.end method
