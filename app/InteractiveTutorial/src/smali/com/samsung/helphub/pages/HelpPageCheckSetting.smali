.class public abstract Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "HelpPageCheckSetting.java"


# instance fields
.field private mEnableSettingDialogFragment:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    return-void
.end method


# virtual methods
.method protected abstract getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 25
    :cond_0
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onDestroy()V

    .line 26
    return-void
.end method

.method protected final showEnableSettingDialog(II)V
    .locals 4
    .param p1, "titleID"    # I
    .param p2, "summaryID"    # I

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    move-result-object v0

    .line 72
    .local v0, "dialogListener":Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0064

    new-instance v3, Lcom/samsung/helphub/pages/HelpPageCheckSetting$6;

    invoke-direct {v3, p0, v0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting$6;-><init>(Lcom/samsung/helphub/pages/HelpPageCheckSetting;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0063

    new-instance v3, Lcom/samsung/helphub/pages/HelpPageCheckSetting$5;

    invoke-direct {v3, p0, v0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting$5;-><init>(Lcom/samsung/helphub/pages/HelpPageCheckSetting;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/helphub/pages/HelpPageCheckSetting$4;

    invoke-direct {v2, p0, v0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting$4;-><init>(Lcom/samsung/helphub/pages/HelpPageCheckSetting;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 98
    return-void
.end method

.method protected final showEnableSettingDialog(ILjava/lang/String;)V
    .locals 4
    .param p1, "titleID"    # I
    .param p2, "summaryID"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    move-result-object v0

    .line 36
    .local v0, "dialogListener":Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0064

    new-instance v3, Lcom/samsung/helphub/pages/HelpPageCheckSetting$3;

    invoke-direct {v3, p0, v0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting$3;-><init>(Lcom/samsung/helphub/pages/HelpPageCheckSetting;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0063

    new-instance v3, Lcom/samsung/helphub/pages/HelpPageCheckSetting$2;

    invoke-direct {v3, p0, v0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting$2;-><init>(Lcom/samsung/helphub/pages/HelpPageCheckSetting;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/helphub/pages/HelpPageCheckSetting$1;

    invoke-direct {v2, p0, v0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting$1;-><init>(Lcom/samsung/helphub/pages/HelpPageCheckSetting;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 62
    return-void
.end method

.method protected final showEnableSettingDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "titleID"    # Ljava/lang/String;
    .param p2, "summaryID"    # Ljava/lang/String;

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    move-result-object v0

    .line 107
    .local v0, "dialogListener":Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0064

    new-instance v3, Lcom/samsung/helphub/pages/HelpPageCheckSetting$9;

    invoke-direct {v3, p0, v0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting$9;-><init>(Lcom/samsung/helphub/pages/HelpPageCheckSetting;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0063

    new-instance v3, Lcom/samsung/helphub/pages/HelpPageCheckSetting$8;

    invoke-direct {v3, p0, v0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting$8;-><init>(Lcom/samsung/helphub/pages/HelpPageCheckSetting;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/helphub/pages/HelpPageCheckSetting$7;

    invoke-direct {v2, p0, v0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting$7;-><init>(Lcom/samsung/helphub/pages/HelpPageCheckSetting;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 132
    return-void
.end method

.method protected abstract startTutorial()V
.end method
