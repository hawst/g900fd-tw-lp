.class public Lcom/samsung/helphub/pages/AllShareCastDonglePage;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "AllShareCastDonglePage.java"


# instance fields
.field TAG:Ljava/lang/String;

.field mStartBtn:Landroid/widget/Button;

.field mstartButtonOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    .line 23
    const-string v0, "AllShareCastDOnglePage"

    iput-object v0, p0, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->TAG:Ljava/lang/String;

    .line 66
    new-instance v0, Lcom/samsung/helphub/pages/AllShareCastDonglePage$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/AllShareCastDonglePage$1;-><init>(Lcom/samsung/helphub/pages/AllShareCastDonglePage;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->mstartButtonOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 29
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v4

    .line 31
    .local v4, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 32
    iget-object v5, p0, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->TAG:Ljava/lang/String;

    const-string v6, "isDownloadable access"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    const v5, 0x7f0d023d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 35
    .local v0, "mImageView_01":Landroid/widget/ImageView;
    const v5, 0x7f0d0240

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 37
    .local v1, "mImageView_02":Landroid/widget/ImageView;
    const v5, 0x7f0d0243

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 39
    .local v2, "mImageView_03":Landroid/widget/ImageView;
    const v5, 0x7f0d0246

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 42
    .local v3, "mImageView_04":Landroid/widget/ImageView;
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 43
    iget-object v5, p0, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->TAG:Ljava/lang/String;

    const-string v6, "isDownloadable Tablet"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "tablet_help_case_1_01"

    invoke-static {v5, v0, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "tablet_help_case_1_02"

    invoke-static {v5, v1, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "tablet_help_case_1_03"

    invoke-static {v5, v2, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "tablet_help_case_1_04"

    invoke-static {v5, v3, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 58
    .end local v0    # "mImageView_01":Landroid/widget/ImageView;
    .end local v1    # "mImageView_02":Landroid/widget/ImageView;
    .end local v2    # "mImageView_03":Landroid/widget/ImageView;
    .end local v3    # "mImageView_04":Landroid/widget/ImageView;
    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    .line 59
    const v5, 0x7f0d024a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->mStartBtn:Landroid/widget/Button;

    .line 60
    iget-object v5, p0, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->mStartBtn:Landroid/widget/Button;

    iget-object v6, p0, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->mstartButtonOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    iget-object v5, p0, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->mStartBtn:Landroid/widget/Button;

    const/4 v6, 0x1

    const/high16 v7, 0x41980000    # 19.0f

    invoke-virtual {v5, v6, v7}, Landroid/widget/Button;->setTextSize(IF)V

    .line 63
    :cond_1
    return-object v4

    .line 50
    .restart local v0    # "mImageView_01":Landroid/widget/ImageView;
    .restart local v1    # "mImageView_02":Landroid/widget/ImageView;
    .restart local v2    # "mImageView_03":Landroid/widget/ImageView;
    .restart local v3    # "mImageView_04":Landroid/widget/ImageView;
    :cond_2
    iget-object v5, p0, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->TAG:Ljava/lang/String;

    const-string v6, "isDownloadable phone"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "wifi_direct_connect_help_03"

    invoke-static {v5, v0, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "wifi_direct_connect_help_04"

    invoke-static {v5, v1, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "wifi_direct_connect_help_02"

    invoke-static {v5, v2, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastDonglePage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "wifi_direct_connect_help_05"

    invoke-static {v5, v3, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0
.end method
