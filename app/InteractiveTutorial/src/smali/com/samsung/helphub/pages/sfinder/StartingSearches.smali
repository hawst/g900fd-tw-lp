.class public Lcom/samsung/helphub/pages/sfinder/StartingSearches;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "StartingSearches.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    return-void
.end method


# virtual methods
.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 29
    .local v2, "view":Landroid/view/View;
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 30
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/sfinder/StartingSearches;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.feature.spen_usp"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 33
    .local v0, "bSPenEnabled":Z
    if-nez v0, :cond_0

    .line 34
    const v3, 0x7f0d0033

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 35
    .local v1, "helpText":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/sfinder/StartingSearches;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0751

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    .end local v0    # "bSPenEnabled":Z
    .end local v1    # "helpText":Landroid/widget/TextView;
    :cond_0
    return-object v2
.end method
