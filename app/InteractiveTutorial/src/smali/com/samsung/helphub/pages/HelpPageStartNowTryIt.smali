.class public abstract Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "HelpPageStartNowTryIt.java"


# instance fields
.field protected isClicked:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->isClicked:Z

    return-void
.end method


# virtual methods
.method protected abstract getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 19
    .local v2, "result":Landroid/view/View;
    const v3, 0x7f0d0081

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 20
    .local v0, "button":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 21
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->getTryItOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    :cond_0
    const v3, 0x7f0d0082

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 25
    .local v1, "button2":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 26
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->getTryItOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    :cond_1
    return-object v2
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->onResume()V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->isClicked:Z

    .line 36
    return-void
.end method
