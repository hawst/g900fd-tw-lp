.class public Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "ContactRecipientInsert.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    .line 15
    new-instance v0, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert$1;-><init>(Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;
    .param p1, "x1"    # Z

    .prologue
    .line 13
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 13
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->showEnableSettingDialog(II)V

    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 56
    return-object p0
.end method

.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->isClicked:Z

    .line 52
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 39
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "air_view_master_onoff"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 40
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "air_view_mode"

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 41
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "air_button_onoff"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 43
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.gesture.AIR_VIEW_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 44
    .local v0, "air_view_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 45
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 46
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->startTutorial()V

    .line 47
    return-void
.end method

.method protected startTutorial()V
    .locals 3

    .prologue
    .line 66
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    const-string v2, "sms:"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 67
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v1, "helpContactTutorial"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 68
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 69
    return-void
.end method
