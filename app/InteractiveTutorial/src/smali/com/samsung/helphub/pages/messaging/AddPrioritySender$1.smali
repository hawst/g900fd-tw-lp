.class Lcom/samsung/helphub/pages/messaging/AddPrioritySender$1;
.super Ljava/lang/Object;
.source "AddPrioritySender.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/messaging/AddPrioritySender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/messaging/AddPrioritySender;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/messaging/AddPrioritySender;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/samsung/helphub/pages/messaging/AddPrioritySender$1;->this$0:Lcom/samsung/helphub/pages/messaging/AddPrioritySender;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 16
    const-string v0, "com.android.mms"

    .line 17
    .local v0, "pkgName":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/helphub/pages/messaging/AddPrioritySender$1;->this$0:Lcom/samsung/helphub/pages/messaging/AddPrioritySender;

    iget-object v2, p0, Lcom/samsung/helphub/pages/messaging/AddPrioritySender$1;->this$0:Lcom/samsung/helphub/pages/messaging/AddPrioritySender;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/messaging/AddPrioritySender;->getActivity()Landroid/app/Activity;

    move-result-object v2

    # invokes: Lcom/samsung/helphub/pages/messaging/AddPrioritySender;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v1, v2, v0}, Lcom/samsung/helphub/pages/messaging/AddPrioritySender;->access$000(Lcom/samsung/helphub/pages/messaging/AddPrioritySender;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 18
    iget-object v1, p0, Lcom/samsung/helphub/pages/messaging/AddPrioritySender$1;->this$0:Lcom/samsung/helphub/pages/messaging/AddPrioritySender;

    # getter for: Lcom/samsung/helphub/pages/messaging/AddPrioritySender;->isClicked:Z
    invoke-static {v1}, Lcom/samsung/helphub/pages/messaging/AddPrioritySender;->access$100(Lcom/samsung/helphub/pages/messaging/AddPrioritySender;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 27
    :goto_0
    return-void

    .line 22
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/pages/messaging/AddPrioritySender$1;->this$0:Lcom/samsung/helphub/pages/messaging/AddPrioritySender;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/helphub/pages/messaging/AddPrioritySender;->isClicked:Z
    invoke-static {v1, v2}, Lcom/samsung/helphub/pages/messaging/AddPrioritySender;->access$202(Lcom/samsung/helphub/pages/messaging/AddPrioritySender;Z)Z

    .line 23
    iget-object v1, p0, Lcom/samsung/helphub/pages/messaging/AddPrioritySender$1;->this$0:Lcom/samsung/helphub/pages/messaging/AddPrioritySender;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.android.mms.help.PRIORITY_SENDER"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/pages/messaging/AddPrioritySender;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 25
    :cond_1
    iget-object v1, p0, Lcom/samsung/helphub/pages/messaging/AddPrioritySender$1;->this$0:Lcom/samsung/helphub/pages/messaging/AddPrioritySender;

    # invokes: Lcom/samsung/helphub/pages/messaging/AddPrioritySender;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/samsung/helphub/pages/messaging/AddPrioritySender;->access$300(Lcom/samsung/helphub/pages/messaging/AddPrioritySender;Ljava/lang/String;)V

    goto :goto_0
.end method
