.class Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard$1;
.super Ljava/lang/Object;
.source "KeyboardSamsungKeyboard.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard$1;->this$0:Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "arg0"    # Landroid/text/Editable;

    .prologue
    .line 75
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 81
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 54
    iget-object v3, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard$1;->this$0:Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 55
    .local v2, "mActivity":Landroid/app/Activity;
    if-nez v2, :cond_1

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "default_input_method"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "inputMethod":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v3, "com.sec.android.inputmethod/.SamsungKeypad"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 60
    iget-object v3, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard$1;->this$0:Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;

    # getter for: Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->mTutorialEditText:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->access$000(Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "*#0*#"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 62
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 63
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "com.sec.android.inputmethod.RequestImeInfo"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    const-string v3, "CODE"

    const-string v4, "*#0*#"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    iget-object v3, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard$1;->this$0:Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 67
    iget-object v3, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard$1;->this$0:Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;

    # getter for: Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->mTutorialEditText:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->access$000(Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;)Landroid/widget/EditText;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
