.class public Lcom/samsung/helphub/pages/basic/UnlockDevice;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "UnlockDevice.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 35
    new-instance v0, Lcom/samsung/helphub/pages/basic/UnlockDevice$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/basic/UnlockDevice$1;-><init>(Lcom/samsung/helphub/pages/basic/UnlockDevice;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/basic/UnlockDevice;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/helphub/pages/basic/UnlockDevice;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 25
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 26
    .local v2, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/basic/UnlockDevice;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 27
    const v3, 0x7f0d0438

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 28
    .local v1, "mImageViewVer":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/basic/UnlockDevice;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "lock_screen_unlock_device"

    invoke-static {v3, v1, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 29
    const v3, 0x7f0d0439

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 30
    .local v0, "mImageViewLand":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/basic/UnlockDevice;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "lock_screen_unlock_device"

    invoke-static {v3, v0, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 32
    .end local v0    # "mImageViewLand":Landroid/widget/ImageView;
    .end local v1    # "mImageViewVer":Landroid/widget/ImageView;
    :cond_0
    return-object v2
.end method
