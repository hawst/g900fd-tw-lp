.class Lcom/samsung/helphub/pages/motion/PanToMoveIcon$1;
.super Ljava/lang/Object;
.source "PanToMoveIcon.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/motion/PanToMoveIcon;->getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/motion/PanToMoveIcon;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/motion/PanToMoveIcon;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/helphub/pages/motion/PanToMoveIcon$1;->this$0:Lcom/samsung/helphub/pages/motion/PanToMoveIcon;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 62
    iget-object v4, p0, Lcom/samsung/helphub/pages/motion/PanToMoveIcon$1;->this$0:Lcom/samsung/helphub/pages/motion/PanToMoveIcon;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/motion/PanToMoveIcon;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "master_motion"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_0

    move v0, v2

    .line 64
    .local v0, "masterStatus":Z
    :goto_0
    iget-object v4, p0, Lcom/samsung/helphub/pages/motion/PanToMoveIcon$1;->this$0:Lcom/samsung/helphub/pages/motion/PanToMoveIcon;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/motion/PanToMoveIcon;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "motion_panning"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_1

    move v1, v2

    .line 65
    .local v1, "settingStatus":Z
    :goto_1
    iget-object v3, p0, Lcom/samsung/helphub/pages/motion/PanToMoveIcon$1;->this$0:Lcom/samsung/helphub/pages/motion/PanToMoveIcon;

    # getter for: Lcom/samsung/helphub/pages/motion/PanToMoveIcon;->isClicked:Z
    invoke-static {v3}, Lcom/samsung/helphub/pages/motion/PanToMoveIcon;->access$000(Lcom/samsung/helphub/pages/motion/PanToMoveIcon;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 74
    :goto_2
    return-void

    .end local v0    # "masterStatus":Z
    .end local v1    # "settingStatus":Z
    :cond_0
    move v0, v3

    .line 62
    goto :goto_0

    .restart local v0    # "masterStatus":Z
    :cond_1
    move v1, v3

    .line 64
    goto :goto_1

    .line 67
    .restart local v1    # "settingStatus":Z
    :cond_2
    iget-object v3, p0, Lcom/samsung/helphub/pages/motion/PanToMoveIcon$1;->this$0:Lcom/samsung/helphub/pages/motion/PanToMoveIcon;

    # setter for: Lcom/samsung/helphub/pages/motion/PanToMoveIcon;->isClicked:Z
    invoke-static {v3, v2}, Lcom/samsung/helphub/pages/motion/PanToMoveIcon;->access$102(Lcom/samsung/helphub/pages/motion/PanToMoveIcon;Z)Z

    .line 68
    if-eqz v0, :cond_3

    if-nez v1, :cond_4

    .line 69
    :cond_3
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PanToMoveIcon$1;->this$0:Lcom/samsung/helphub/pages/motion/PanToMoveIcon;

    const v3, 0x7f0a0562

    const v4, 0x7f0a0563

    # invokes: Lcom/samsung/helphub/pages/motion/PanToMoveIcon;->showEnableSettingDialog(II)V
    invoke-static {v2, v3, v4}, Lcom/samsung/helphub/pages/motion/PanToMoveIcon;->access$200(Lcom/samsung/helphub/pages/motion/PanToMoveIcon;II)V

    goto :goto_2

    .line 72
    :cond_4
    iget-object v2, p0, Lcom/samsung/helphub/pages/motion/PanToMoveIcon$1;->this$0:Lcom/samsung/helphub/pages/motion/PanToMoveIcon;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/motion/PanToMoveIcon;->startTutorial()V

    goto :goto_2
.end method
