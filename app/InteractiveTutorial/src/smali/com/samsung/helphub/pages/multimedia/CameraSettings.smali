.class public Lcom/samsung/helphub/pages/multimedia/CameraSettings;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "CameraSettings.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    .line 38
    new-instance v0, Lcom/samsung/helphub/pages/multimedia/CameraSettings$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/multimedia/CameraSettings$1;-><init>(Lcom/samsung/helphub/pages/multimedia/CameraSettings;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/multimedia/CameraSettings;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v3

    .line 21
    .local v3, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multimedia/CameraSettings;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 22
    const v4, 0x7f0d001f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 23
    .local v2, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multimedia/CameraSettings;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "help_camera_settings"

    invoke-static {v4, v2, v5}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 26
    .end local v2    # "mImageView":Landroid/widget/ImageView;
    :cond_0
    const v4, 0x7f0d0081

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 27
    .local v0, "changeSettingsButton":Landroid/view/View;
    const v4, 0x7f0d0082

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 28
    .local v1, "createSettingShortcutButton":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 29
    iget-object v4, p0, Lcom/samsung/helphub/pages/multimedia/CameraSettings;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    :cond_1
    if-eqz v1, :cond_2

    .line 32
    iget-object v4, p0, Lcom/samsung/helphub/pages/multimedia/CameraSettings;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    :cond_2
    return-object v3
.end method
