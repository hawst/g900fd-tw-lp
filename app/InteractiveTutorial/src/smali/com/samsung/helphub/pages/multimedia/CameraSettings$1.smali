.class Lcom/samsung/helphub/pages/multimedia/CameraSettings$1;
.super Ljava/lang/Object;
.source "CameraSettings.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/multimedia/CameraSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/multimedia/CameraSettings;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/multimedia/CameraSettings;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/samsung/helphub/pages/multimedia/CameraSettings$1;->this$0:Lcom/samsung/helphub/pages/multimedia/CameraSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 42
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 44
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 58
    :goto_0
    return-void

    .line 46
    :pswitch_0
    const-string v1, "com.sec.android.app.camera"

    const-string v2, "com.sec.android.app.camera.Camera"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    const-string v1, "help_change_camera_settings"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 48
    iget-object v1, p0, Lcom/samsung/helphub/pages/multimedia/CameraSettings$1;->this$0:Lcom/samsung/helphub/pages/multimedia/CameraSettings;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/multimedia/CameraSettings;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 51
    :pswitch_1
    const-string v1, "com.sec.android.app.camera"

    const-string v2, "com.sec.android.app.camera.Camera"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const-string v1, "help_create_setting_shortcut"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 53
    iget-object v1, p0, Lcom/samsung/helphub/pages/multimedia/CameraSettings$1;->this$0:Lcom/samsung/helphub/pages/multimedia/CameraSettings;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/multimedia/CameraSettings;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x7f0d0081
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
