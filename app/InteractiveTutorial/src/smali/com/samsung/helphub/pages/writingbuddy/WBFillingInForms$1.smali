.class Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;
.super Ljava/lang/Object;
.source "WBFillingInForms.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v11, 0x7f0a07b7

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 73
    iget-object v8, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    invoke-virtual {v8}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "pen_writing_buddy"

    invoke-static {v8, v9, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    if-eqz v8, :cond_1

    move v5, v6

    .line 74
    .local v5, "masterStatus":Z
    :goto_0
    iget-object v8, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    invoke-virtual {v8}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "accessibility_display_magnification_enabled"

    invoke-static {v8, v9, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    if-eqz v8, :cond_2

    move v1, v6

    .line 75
    .local v1, "MagnificationResult":Z
    :goto_1
    const/4 v0, 0x0

    .line 77
    .local v0, "HoverZoomResult":Z
    iget-object v8, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    invoke-virtual {v8}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "com.sec.feature.overlaymagnifier"

    invoke-static {v8, v9}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 78
    iget-object v8, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    invoke-virtual {v8}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "accessibility_magnifier"

    invoke-static {v8, v9, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    if-eqz v8, :cond_3

    move v0, v6

    .line 81
    :cond_0
    :goto_2
    iget-object v8, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    # getter for: Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->isClicked:Z
    invoke-static {v8}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->access$000(Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 102
    :goto_3
    return-void

    .end local v0    # "HoverZoomResult":Z
    .end local v1    # "MagnificationResult":Z
    .end local v5    # "masterStatus":Z
    :cond_1
    move v5, v7

    .line 73
    goto :goto_0

    .restart local v5    # "masterStatus":Z
    :cond_2
    move v1, v7

    .line 74
    goto :goto_1

    .restart local v0    # "HoverZoomResult":Z
    .restart local v1    # "MagnificationResult":Z
    :cond_3
    move v0, v7

    .line 78
    goto :goto_2

    .line 83
    :cond_4
    iget-object v8, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    # setter for: Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->isClicked:Z
    invoke-static {v8, v6}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->access$102(Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;Z)Z

    .line 84
    iget-object v8, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    const v9, 0x7f0a07c6

    invoke-virtual {v8, v9}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-array v9, v6, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    invoke-virtual {v10, v11}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 86
    .local v4, "dialog_title":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    const v9, 0x7f0a07c7

    invoke-virtual {v8, v9}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    invoke-virtual {v10, v11}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    iget-object v10, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    invoke-virtual {v10, v11}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 88
    .local v3, "dialog_content_disable":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    const v10, 0x7f0a07c9

    invoke-virtual {v9, v10}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    invoke-virtual {v10, v11}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v7

    invoke-static {v9, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n\n- "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    const v8, 0x7f0a0573

    invoke-virtual {v7, v8}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n- "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    const v8, 0x7f0a0574

    invoke-virtual {v7, v8}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n- "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    const v8, 0x7f0a0575

    invoke-virtual {v7, v8}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 93
    .local v2, "dialog_content":Ljava/lang/String;
    if-nez v5, :cond_5

    iget-object v6, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    invoke-virtual {v6}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_5

    if-nez v1, :cond_5

    if-nez v0, :cond_5

    .line 94
    iget-object v6, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    # invokes: Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->showEnableSettingDialog(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v4, v3}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->access$200(Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 96
    :cond_5
    iget-object v6, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    invoke-virtual {v6}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_6

    if-nez v1, :cond_6

    if-eqz v0, :cond_7

    .line 97
    :cond_6
    iget-object v6, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    # invokes: Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->showEnableSettingDialog(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v4, v2}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->access$300(Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 100
    :cond_7
    iget-object v6, p0, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms$1;->this$0:Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;

    invoke-virtual {v6}, Lcom/samsung/helphub/pages/writingbuddy/WBFillingInForms;->startTutorial()V

    goto/16 :goto_3
.end method
