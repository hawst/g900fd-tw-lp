.class public Lcom/samsung/helphub/pages/groupplay/Introduction;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "Introduction.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v4

    .line 22
    .local v4, "view":Landroid/view/View;
    if-eqz v4, :cond_1

    .line 23
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/groupplay/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->isMusicLiveShareHeaderHidden(Landroid/content/Context;)Z

    move-result v1

    .line 24
    .local v1, "musicHidden":Z
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/groupplay/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->isMultiVisionHeaderHidden(Landroid/content/Context;)Z

    move-result v3

    .line 25
    .local v3, "videoHidden":Z
    const v5, 0x7f0d02e2

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 26
    .local v0, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/groupplay/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 27
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/groupplay/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "group_play_introduction"

    invoke-static {v5, v0, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 28
    if-eqz v1, :cond_2

    .line 29
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/groupplay/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "groupcast_mobile_help_ap"

    invoke-static {v5, v0, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 44
    :cond_0
    :goto_0
    const v5, 0x7f0d02e3

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 45
    .local v2, "textView":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 46
    if-nez v1, :cond_4

    if-eqz v3, :cond_4

    .line 47
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/groupplay/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a03ff

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    .end local v0    # "mImageView":Landroid/widget/ImageView;
    .end local v1    # "musicHidden":Z
    .end local v2    # "textView":Landroid/widget/TextView;
    .end local v3    # "videoHidden":Z
    :cond_1
    :goto_1
    return-object v4

    .line 32
    .restart local v0    # "mImageView":Landroid/widget/ImageView;
    .restart local v1    # "musicHidden":Z
    .restart local v3    # "videoHidden":Z
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/groupplay/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "group_play_introduction"

    invoke-static {v5, v0, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0

    .line 36
    :cond_3
    if-eqz v0, :cond_0

    .line 37
    if-eqz v1, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/groupplay/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0200af

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 49
    .restart local v2    # "textView":Landroid/widget/TextView;
    :cond_4
    if-eqz v1, :cond_5

    if-nez v3, :cond_5

    .line 50
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/groupplay/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0400

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 52
    :cond_5
    if-nez v1, :cond_1

    if-nez v3, :cond_1

    .line 53
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/groupplay/Introduction;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0401

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
