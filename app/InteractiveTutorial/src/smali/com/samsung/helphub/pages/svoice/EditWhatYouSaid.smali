.class public Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "EditWhatYouSaid.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 29
    new-instance v0, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid$1;-><init>(Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->isClicked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->isClicked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 22
    .local v1, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 23
    const v2, 0x7f0d03fd

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 24
    .local v0, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "svoice_edit_what_you_said"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 26
    .end local v0    # "mImageView":Landroid/widget/ImageView;
    :cond_0
    return-object v1
.end method
