.class public Lcom/samsung/helphub/pages/storyalbum/ViewSimilarPictures;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "ViewSimilarPictures.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v5

    .line 23
    .local v5, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/storyalbum/ViewSimilarPictures;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 24
    const v6, 0x7f0d03fb

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 26
    .local v1, "mImageView":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 27
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/storyalbum/ViewSimilarPictures;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-string v7, "story_album_view_similar_pic"

    const-string v8, "drawable"

    const-string v9, "com.samsung.helpplugin"

    invoke-static {v6, v7, v8, v9}, Lcom/samsung/helphub/HelpHubCommon;->getResourceId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 30
    .local v4, "resId":I
    const/4 v6, -0x1

    if-eq v4, v6, :cond_1

    .line 31
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/storyalbum/ViewSimilarPictures;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 33
    .local v3, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v6, "com.samsung.helpplugin"

    invoke-virtual {v3, v6}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    .line 35
    .local v2, "mResources":Landroid/content/res/Resources;
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .end local v1    # "mImageView":Landroid/widget/ImageView;
    .end local v2    # "mResources":Landroid/content/res/Resources;
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    .end local v4    # "resId":I
    :cond_0
    :goto_0
    return-object v5

    .line 36
    .restart local v1    # "mImageView":Landroid/widget/ImageView;
    .restart local v3    # "pm":Landroid/content/pm/PackageManager;
    .restart local v4    # "resId":I
    :catch_0
    move-exception v0

    .line 38
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 41
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    const-string v6, "HelpHub2.0"

    const-string v7, "The resource not downloaded yet"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
