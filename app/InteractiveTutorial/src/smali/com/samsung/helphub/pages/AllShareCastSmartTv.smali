.class public Lcom/samsung/helphub/pages/AllShareCastSmartTv;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "AllShareCastSmartTv.java"


# instance fields
.field TAG:Ljava/lang/String;

.field mstartButtonOnClickListener:Landroid/view/View$OnClickListener;

.field startBtn:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    .line 24
    const-string v0, "AllShareCastSmartTv"

    iput-object v0, p0, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->TAG:Ljava/lang/String;

    .line 59
    new-instance v0, Lcom/samsung/helphub/pages/AllShareCastSmartTv$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/AllShareCastSmartTv$1;-><init>(Lcom/samsung/helphub/pages/AllShareCastSmartTv;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->mstartButtonOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 29
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 31
    .local v2, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 32
    const v3, 0x7f0d0254

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 34
    .local v0, "mImageView_01":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "img_allshare_learn_smarttv_01"

    invoke-static {v3, v0, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 35
    const v3, 0x7f0d0257

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 37
    .local v1, "mImageView_02":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "img_allshare_learn_smarttv_02"

    invoke-static {v3, v1, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 39
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 40
    iget-object v3, p0, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->TAG:Ljava/lang/String;

    const-string v4, "isDownloadable Tablet"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "tablet_help_case_1_01"

    invoke-static {v3, v0, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "tablet_help_case_1_02"

    invoke-static {v3, v1, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 51
    .end local v0    # "mImageView_01":Landroid/widget/ImageView;
    .end local v1    # "mImageView_02":Landroid/widget/ImageView;
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 52
    const v3, 0x7f0d025b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->startBtn:Landroid/widget/Button;

    .line 53
    iget-object v3, p0, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->startBtn:Landroid/widget/Button;

    const/4 v4, 0x1

    const/high16 v5, 0x41980000    # 19.0f

    invoke-virtual {v3, v4, v5}, Landroid/widget/Button;->setTextSize(IF)V

    .line 54
    iget-object v3, p0, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->startBtn:Landroid/widget/Button;

    iget-object v4, p0, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->mstartButtonOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    :cond_1
    return-object v2

    .line 45
    .restart local v0    # "mImageView_01":Landroid/widget/ImageView;
    .restart local v1    # "mImageView_02":Landroid/widget/ImageView;
    :cond_2
    iget-object v3, p0, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->TAG:Ljava/lang/String;

    const-string v4, "isDownloadable phone"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "wifi_direct_connect_help_01"

    invoke-static {v3, v0, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AllShareCastSmartTv;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "wifi_direct_connect_help_02"

    invoke-static {v3, v1, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0
.end method
