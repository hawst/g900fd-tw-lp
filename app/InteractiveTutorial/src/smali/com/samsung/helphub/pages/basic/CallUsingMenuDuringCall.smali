.class public Lcom/samsung/helphub/pages/basic/CallUsingMenuDuringCall;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "CallUsingMenuDuringCall.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 32
    .local v2, "result":Landroid/view/View;
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isDomesticModel()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 33
    :cond_0
    const v3, 0x7f0d01dc

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 34
    .local v0, "callScreen":Landroid/widget/ImageView;
    const v3, 0x7f0d0034

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 35
    .local v1, "listItem3":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 36
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/basic/CallUsingMenuDuringCall;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "easy_mode_switch"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_2

    .line 37
    const v3, 0x7f02003e

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 38
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 51
    .end local v0    # "callScreen":Landroid/widget/ImageView;
    .end local v1    # "listItem3":Landroid/view/View;
    :cond_1
    :goto_0
    return-object v2

    .line 40
    .restart local v0    # "callScreen":Landroid/widget/ImageView;
    .restart local v1    # "listItem3":Landroid/view/View;
    :cond_2
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isDomesticModel()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 41
    const v3, 0x7f02003f

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 46
    :goto_1
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 44
    :cond_3
    const v3, 0x7f02003c

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 58
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->onResume()V

    .line 60
    return-void
.end method
