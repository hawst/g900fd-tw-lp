.class public Lcom/samsung/helphub/pages/smartscreen/SmartRotation;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "SmartRotation.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/smartscreen/SmartRotation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/smartscreen/SmartRotation;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/smartscreen/SmartRotation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/smartscreen/SmartRotation;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/smartscreen/SmartRotation;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/smartscreen/SmartRotation;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->showEnableSettingDialog(II)V

    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 46
    return-object p0
.end method

.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/samsung/helphub/pages/smartscreen/SmartRotation$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/smartscreen/SmartRotation$1;-><init>(Lcom/samsung/helphub/pages/smartscreen/SmartRotation;)V

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->isClicked:Z

    .line 42
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 3

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "intelligent_rotation_mode"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 36
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->startTutorial()V

    .line 37
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 23
    .local v1, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 24
    const v2, 0x7f0d03df

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 26
    .local v0, "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "tutorial_smart_screen_rotation"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 29
    .end local v0    # "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_0
    return-object v1
.end method

.method protected startTutorial()V
    .locals 3

    .prologue
    .line 72
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.sbrowsertry.GUIDE_SMART_ROTATION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 73
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v1, "fromHelp"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 74
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->startActivity(Landroid/content/Intent;)V

    .line 75
    return-void
.end method
