.class Lcom/samsung/helphub/pages/AirViewPenFingerPage$1;
.super Landroid/content/BroadcastReceiver;
.source "AirViewPenFingerPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/AirViewPenFingerPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/AirViewPenFingerPage;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/AirViewPenFingerPage;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$1;->this$0:Lcom/samsung/helphub/pages/AirViewPenFingerPage;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$1;->this$0:Lcom/samsung/helphub/pages/AirViewPenFingerPage;

    const-string v1, "penInsert"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPeninsert:Z

    .line 74
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$1;->this$0:Lcom/samsung/helphub/pages/AirViewPenFingerPage;

    # getter for: Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mPenDetachDialogFragment:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->access$000(Lcom/samsung/helphub/pages/AirViewPenFingerPage;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$1;->this$0:Lcom/samsung/helphub/pages/AirViewPenFingerPage;

    # getter for: Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mPenDetachDialogFragment:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->access$000(Lcom/samsung/helphub/pages/AirViewPenFingerPage;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 76
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$1;->this$0:Lcom/samsung/helphub/pages/AirViewPenFingerPage;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mPenDetachDialogFragment:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->access$002(Lcom/samsung/helphub/pages/AirViewPenFingerPage;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 78
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$1;->this$0:Lcom/samsung/helphub/pages/AirViewPenFingerPage;

    iget-boolean v0, v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPenMode:Z

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$1;->this$0:Lcom/samsung/helphub/pages/AirViewPenFingerPage;

    iget-boolean v0, v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPeninsert:Z

    if-nez v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$1;->this$0:Lcom/samsung/helphub/pages/AirViewPenFingerPage;

    invoke-virtual {v0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->startTutorial()V

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$1;->this$0:Lcom/samsung/helphub/pages/AirViewPenFingerPage;

    iget-boolean v0, v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPeninsert:Z

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$1;->this$0:Lcom/samsung/helphub/pages/AirViewPenFingerPage;

    invoke-virtual {v0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->startTutorial()V

    goto :goto_0
.end method
