.class public Lcom/samsung/helphub/pages/EnableSettingDialogFragment;
.super Landroid/app/DialogFragment;
.source "EnableSettingDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final BUNDLE_KEY_DIALOG_SUMMARY:Ljava/lang/String; = "summary"

.field private static final BUNDLE_KEY_DIALOG_TITLE:Ljava/lang/String; = "title"


# instance fields
.field private mListener:Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/EnableSettingDialogFragment;)Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/EnableSettingDialogFragment;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->mListener:Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    return-object v0
.end method

.method public static newInstance(II)Lcom/samsung/helphub/pages/EnableSettingDialogFragment;
    .locals 3
    .param p0, "titleID"    # I
    .param p1, "summaryID"    # I

    .prologue
    .line 32
    new-instance v1, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;

    invoke-direct {v1}, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;-><init>()V

    .line 33
    .local v1, "result":Lcom/samsung/helphub/pages/EnableSettingDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 34
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 35
    const-string v2, "summary"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 36
    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 37
    return-object v1
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 107
    packed-switch p2, :pswitch_data_0

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 109
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->mListener:Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->mListener:Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    invoke-interface {v0}, Lcom/samsung/helphub/pages/IEnableSettingDialogListener;->onClickEnableSettingsButtonOK()V

    goto :goto_0

    .line 115
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->mListener:Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->mListener:Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    invoke-interface {v0}, Lcom/samsung/helphub/pages/IEnableSettingDialogListener;->onClickEnableSettingsButtonCancel()V

    goto :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 42
    const/4 v2, 0x0

    .line 44
    .local v2, "result":Landroid/view/View;
    const v4, 0x7f04006e

    invoke-virtual {p1, v4, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 49
    const v4, 0x7f0d0114

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 50
    .local v3, "textView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "summary"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 52
    const v4, 0x7f0d0116

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 53
    .local v0, "button":Landroid/widget/Button;
    if-eqz v0, :cond_0

    .line 54
    new-instance v4, Lcom/samsung/helphub/pages/EnableSettingDialogFragment$1;

    invoke-direct {v4, p0}, Lcom/samsung/helphub/pages/EnableSettingDialogFragment$1;-><init>(Lcom/samsung/helphub/pages/EnableSettingDialogFragment;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    :cond_0
    const v4, 0x7f0d0115

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "button":Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 66
    .restart local v0    # "button":Landroid/widget/Button;
    if-eqz v0, :cond_1

    .line 67
    new-instance v4, Lcom/samsung/helphub/pages/EnableSettingDialogFragment$2;

    invoke-direct {v4, p0}, Lcom/samsung/helphub/pages/EnableSettingDialogFragment$2;-><init>(Lcom/samsung/helphub/pages/EnableSettingDialogFragment;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    :cond_1
    invoke-virtual {p0, v6}, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->setCancelable(Z)V

    .line 81
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    .line 82
    .local v1, "dialog":Landroid/app/Dialog;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "summary"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setTitle(I)V

    .line 83
    new-instance v4, Lcom/samsung/helphub/pages/EnableSettingDialogFragment$3;

    invoke-direct {v4, p0}, Lcom/samsung/helphub/pages/EnableSettingDialogFragment$3;-><init>(Lcom/samsung/helphub/pages/EnableSettingDialogFragment;)V

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 98
    return-object v2
.end method

.method public setSettingDialogButtonClickListener(Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->mListener:Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    .line 103
    return-void
.end method
