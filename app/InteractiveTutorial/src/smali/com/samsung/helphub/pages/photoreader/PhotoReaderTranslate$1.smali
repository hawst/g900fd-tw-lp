.class Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate$1;
.super Ljava/lang/Object;
.source "PhotoReaderTranslate.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;)V
    .locals 0

    .prologue
    .line 13
    iput-object p1, p0, Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate$1;->this$0:Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 17
    iget-object v2, p0, Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate$1;->this$0:Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->getOCRPKGName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 19
    .local v1, "ocrPKGName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate$1;->this$0:Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;

    # getter for: Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;->access$000(Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;)Z

    move-result v2

    if-nez v2, :cond_0

    if-nez v1, :cond_1

    .line 26
    :cond_0
    :goto_0
    return-void

    .line 22
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate$1;->this$0:Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;

    # setter for: Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;->isClicked:Z
    invoke-static {v2, v4}, Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;->access$102(Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;Z)Z

    .line 23
    new-instance v0, Landroid/content/Intent;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".showhelp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 24
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "help_translate"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 25
    iget-object v2, p0, Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate$1;->this$0:Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/photoreader/PhotoReaderTranslate;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
