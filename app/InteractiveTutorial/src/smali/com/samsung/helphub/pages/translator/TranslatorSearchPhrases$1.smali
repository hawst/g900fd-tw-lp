.class Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases$1;
.super Ljava/lang/Object;
.source "TranslatorSearchPhrases.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases$1;->this$0:Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 16
    const-string v1, "com.sec.android.app.translator"

    .line 18
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases$1;->this$0:Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;

    iget-object v3, p0, Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases$1;->this$0:Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;->access$000(Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 19
    iget-object v2, p0, Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases$1;->this$0:Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;

    # getter for: Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;->access$100(Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 30
    :goto_0
    return-void

    .line 21
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases$1;->this$0:Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;

    # setter for: Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;->isClicked:Z
    invoke-static {v2, v4}, Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;->access$202(Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;Z)Z

    .line 22
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 23
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.translator.START_HELP_MODE_PHRASES"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 24
    const-string v2, "helpMode"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 25
    iget-object v2, p0, Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases$1;->this$0:Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 28
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases$1;->this$0:Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;

    # invokes: Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;->access$300(Lcom/samsung/helphub/pages/translator/TranslatorSearchPhrases;Ljava/lang/String;)V

    goto :goto_0
.end method
