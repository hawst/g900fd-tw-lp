.class Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;
.super Ljava/lang/Object;
.source "AirViewInformationPreview.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

.field final synthetic val$MagnificationResult:Z

.field final synthetic val$dialog_content:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    iput-boolean p2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->val$MagnificationResult:Z

    iput-object p3, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->val$dialog_content:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v5, 0x7f0a056b

    .line 103
    const-string v1, "com.android.mms"

    .line 105
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    iget-object v3, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->access$500(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 106
    iget-object v2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    iget-boolean v2, v2, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->isClicked:Z

    if-eqz v2, :cond_0

    .line 125
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->isClicked:Z

    .line 109
    iget-object v2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->checkSettingStatus()Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->access$000(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->val$MagnificationResult:Z

    if-nez v2, :cond_1

    .line 110
    iget-object v2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    iget-object v3, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    const v4, 0x7f0a056e

    invoke-virtual {v3, v4}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->showEMailSettingDialog(ILjava/lang/String;)V
    invoke-static {v2, v5, v3}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->access$600(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;ILjava/lang/String;)V

    goto :goto_0

    .line 112
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->val$MagnificationResult:Z

    if-eqz v2, :cond_3

    .line 113
    :cond_2
    iget-object v2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    iget-object v3, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->val$dialog_content:Ljava/lang/String;

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->showEMailSettingDialog(ILjava/lang/String;)V
    invoke-static {v2, v5, v3}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->access$600(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;ILjava/lang/String;)V

    goto :goto_0

    .line 117
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.android.mms.help.AirViewTutorialReceiver.java"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 118
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.android.mms.help.AirViewMainActivity"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    iget-object v2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 123
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_4
    iget-object v2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$3;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->access$700(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;Ljava/lang/String;)V

    goto :goto_0
.end method
