.class public Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "WritingOnApplications.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->showEnableSettingDialog(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->showEnableSettingDialog(ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 69
    return-object p0
.end method

.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 79
    new-instance v0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications$1;-><init>(Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;)V

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->isClicked:Z

    .line 61
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 3

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/helphub/utility/TackBackUtility;->turnOffTalkBack(Landroid/content/Context;)Z

    .line 41
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accessibility_display_magnification_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 43
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "pen_writing_buddy"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 47
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/WritingOnApplications;->startTutorial()V

    .line 49
    return-void
.end method

.method protected startTutorial()V
    .locals 0

    .prologue
    .line 142
    return-void
.end method
