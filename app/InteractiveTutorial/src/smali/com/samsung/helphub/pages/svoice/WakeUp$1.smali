.class Lcom/samsung/helphub/pages/svoice/WakeUp$1;
.super Ljava/lang/Object;
.source "WakeUp.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/svoice/WakeUp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/svoice/WakeUp;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/svoice/WakeUp;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/samsung/helphub/pages/svoice/WakeUp$1;->this$0:Lcom/samsung/helphub/pages/svoice/WakeUp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 119
    const-string v1, "com.vlingo.midas"

    .line 121
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/WakeUp$1;->this$0:Lcom/samsung/helphub/pages/svoice/WakeUp;

    iget-object v3, p0, Lcom/samsung/helphub/pages/svoice/WakeUp$1;->this$0:Lcom/samsung/helphub/pages/svoice/WakeUp;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/svoice/WakeUp;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/svoice/WakeUp;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/svoice/WakeUp;->access$000(Lcom/samsung/helphub/pages/svoice/WakeUp;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 122
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/WakeUp$1;->this$0:Lcom/samsung/helphub/pages/svoice/WakeUp;

    # getter for: Lcom/samsung/helphub/pages/svoice/WakeUp;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/svoice/WakeUp;->access$100(Lcom/samsung/helphub/pages/svoice/WakeUp;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 144
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/WakeUp$1;->this$0:Lcom/samsung/helphub/pages/svoice/WakeUp;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/svoice/WakeUp;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/svoice/WakeUp;->access$202(Lcom/samsung/helphub/pages/svoice/WakeUp;Z)Z

    .line 126
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WakeUp Model name is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ro.product.name"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->LogD(Ljava/lang/String;)V

    .line 127
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 128
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "ks01lteskt"

    const-string v3, "ro.product.name"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "ks01ltektt"

    const-string v3, "ro.product.name"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "ks01ltelgt"

    const-string v3, "ro.product.name"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 129
    :cond_1
    const-string v2, "com.vlingo.midas"

    const-string v3, "com.vlingo.midas.gui.ConversationActivity"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    :goto_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/WakeUp$1;->this$0:Lcom/samsung/helphub/pages/svoice/WakeUp;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/svoice/WakeUp;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 131
    :cond_2
    const-string v2, "hlte"

    const-string v3, "ro.product.name"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "ha3g"

    const-string v3, "ro.product.name"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 132
    :cond_3
    const-string v2, "com.vlingo.midas"

    const-string v3, "com.samsung.wakeupsetting.CustomWakeupHelpSettingActivity"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 135
    :cond_4
    const-string v2, "com.vlingo.midas"

    const-string v3, "com.vlingo.midas.gui.ConversationHelpActivity"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 141
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_5
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/WakeUp$1;->this$0:Lcom/samsung/helphub/pages/svoice/WakeUp;

    # invokes: Lcom/samsung/helphub/pages/svoice/WakeUp;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/svoice/WakeUp;->access$300(Lcom/samsung/helphub/pages/svoice/WakeUp;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
