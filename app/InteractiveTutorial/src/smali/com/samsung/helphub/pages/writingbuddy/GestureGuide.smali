.class public Lcom/samsung/helphub/pages/writingbuddy/GestureGuide;
.super Landroid/app/Activity;
.source "GestureGuide.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 11
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 12
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/GestureGuide;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "writing_buddy_setting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 13
    .local v0, "isFromWritingBuddy":Z
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/GestureGuide;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 14
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/GestureGuide;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 15
    if-eqz v0, :cond_0

    .line 16
    const v1, 0x7f0401a4

    invoke-virtual {p0, v1}, Lcom/samsung/helphub/pages/writingbuddy/GestureGuide;->setContentView(I)V

    .line 18
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/GestureGuide;->onBackPressed()V

    .line 24
    const/4 v0, 0x1

    return v0
.end method
