.class Lcom/samsung/helphub/pages/groupplay/JoinGroup$1;
.super Ljava/lang/Object;
.source "JoinGroup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/groupplay/JoinGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/groupplay/JoinGroup;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/groupplay/JoinGroup;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/samsung/helphub/pages/groupplay/JoinGroup$1;->this$0:Lcom/samsung/helphub/pages/groupplay/JoinGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 16
    const-string v1, "com.samsung.groupcast"

    .line 18
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/JoinGroup$1;->this$0:Lcom/samsung/helphub/pages/groupplay/JoinGroup;

    iget-object v3, p0, Lcom/samsung/helphub/pages/groupplay/JoinGroup$1;->this$0:Lcom/samsung/helphub/pages/groupplay/JoinGroup;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/groupplay/JoinGroup;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/groupplay/JoinGroup;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/groupplay/JoinGroup;->access$000(Lcom/samsung/helphub/pages/groupplay/JoinGroup;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 19
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/JoinGroup$1;->this$0:Lcom/samsung/helphub/pages/groupplay/JoinGroup;

    # getter for: Lcom/samsung/helphub/pages/groupplay/JoinGroup;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/groupplay/JoinGroup;->access$100(Lcom/samsung/helphub/pages/groupplay/JoinGroup;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 29
    :goto_0
    return-void

    .line 21
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/JoinGroup$1;->this$0:Lcom/samsung/helphub/pages/groupplay/JoinGroup;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/groupplay/JoinGroup;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/groupplay/JoinGroup;->access$202(Lcom/samsung/helphub/pages/groupplay/JoinGroup;Z)Z

    .line 22
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.samsung.groupcast.action.HELP"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 23
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "help_screen"

    const-string v3, "join_group"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 24
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/JoinGroup$1;->this$0:Lcom/samsung/helphub/pages/groupplay/JoinGroup;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/groupplay/JoinGroup;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 27
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/JoinGroup$1;->this$0:Lcom/samsung/helphub/pages/groupplay/JoinGroup;

    # invokes: Lcom/samsung/helphub/pages/groupplay/JoinGroup;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/groupplay/JoinGroup;->access$300(Lcom/samsung/helphub/pages/groupplay/JoinGroup;Ljava/lang/String;)V

    goto :goto_0
.end method
