.class public abstract Lcom/samsung/helphub/pages/AirViewPenFingerPage;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "AirViewPenFingerPage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/pages/AirViewPenFingerPage$10;,
        Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;
    }
.end annotation


# instance fields
.field SpenDetectionReceiver:Landroid/content/BroadcastReceiver;

.field protected isClicked:Z

.field protected isPenMode:Z

.field protected isPeninsert:Z

.field private mEnableSettingDialogFragment:Landroid/app/AlertDialog;

.field private mPenDetachDialogFragment:Landroid/app/AlertDialog;

.field private mPenDetachHandler:Landroid/os/Handler;

.field private mPenDetachRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 27
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isClicked:Z

    .line 62
    iput-boolean v1, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPenMode:Z

    .line 63
    iput-boolean v1, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPeninsert:Z

    .line 64
    iput-object v2, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 65
    iput-object v2, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mPenDetachDialogFragment:Landroid/app/AlertDialog;

    .line 69
    new-instance v0, Lcom/samsung/helphub/pages/AirViewPenFingerPage$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$1;-><init>(Lcom/samsung/helphub/pages/AirViewPenFingerPage;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->SpenDetectionReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/AirViewPenFingerPage;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/AirViewPenFingerPage;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mPenDetachDialogFragment:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/helphub/pages/AirViewPenFingerPage;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/AirViewPenFingerPage;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mPenDetachDialogFragment:Landroid/app/AlertDialog;

    return-object p1
.end method


# virtual methods
.method protected checkAirviewStatus(Ljava/lang/String;)Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;
    .locals 7
    .param p1, "FunctionMode"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 192
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "air_view_master_onoff"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_3

    move v1, v3

    .line 194
    .local v1, "masterStatus":Z
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "air_view_mode"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 196
    .local v2, "penStatus":I
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static {v5, p1, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_4

    move v0, v3

    .line 200
    .local v0, "functionStatus":Z
    :goto_1
    const-string v5, "DCM"

    const-string v6, "ro.csc.sales_code"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "KDI"

    const-string v6, "ro.csc.sales_code"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 202
    :cond_0
    const-string v5, "pen_hovering_information_preview"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 203
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "finger_air_view_information_preview"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_1

    move v4, v3

    :cond_1
    and-int/2addr v0, v4

    .line 212
    :cond_2
    :goto_2
    if-nez v1, :cond_9

    .line 213
    sget-object v3, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->DisabledAirView:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    .line 221
    :goto_3
    return-object v3

    .end local v0    # "functionStatus":Z
    .end local v1    # "masterStatus":Z
    .end local v2    # "penStatus":I
    :cond_3
    move v1, v4

    .line 192
    goto :goto_0

    .restart local v1    # "masterStatus":Z
    .restart local v2    # "penStatus":I
    :cond_4
    move v0, v4

    .line 196
    goto :goto_1

    .line 204
    .restart local v0    # "functionStatus":Z
    :cond_5
    const-string v5, "pen_hovering_progress_preview"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 205
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "finger_air_view_pregress_bar_preview"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_6

    move v4, v3

    :cond_6
    and-int/2addr v0, v4

    goto :goto_2

    .line 206
    :cond_7
    const-string v5, "pen_hovering_speed_dial_preview"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 207
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "finger_air_view_speed_dial_tip"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_8

    move v4, v3

    :cond_8
    and-int/2addr v0, v4

    goto :goto_2

    .line 214
    :cond_9
    iget-boolean v4, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPenMode:Z

    if-eqz v4, :cond_a

    if-ne v2, v3, :cond_a

    .line 215
    sget-object v3, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->DisabledPenMode:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    goto :goto_3

    .line 216
    :cond_a
    iget-boolean v3, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPenMode:Z

    if-nez v3, :cond_b

    if-nez v2, :cond_b

    .line 217
    sget-object v3, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->DisabledFingerMode:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    goto :goto_3

    .line 218
    :cond_b
    if-nez v0, :cond_c

    .line 219
    sget-object v3, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->DisabledFuction:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    goto :goto_3

    .line 221
    :cond_c
    sget-object v3, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->AllEnalbed:Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    goto :goto_3
.end method

.method protected checkFingerModePopup(Ljava/lang/String;I)V
    .locals 8
    .param p1, "fuctionMode"    # Ljava/lang/String;
    .param p2, "functionStringID"    # I

    .prologue
    const v7, 0x7f0a01b3

    const/4 v6, 0x2

    const v5, 0x7f0a01b0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 158
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 160
    .local v0, "res":Landroid/content/res/Resources;
    iput-boolean v4, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPenMode:Z

    .line 162
    sget-object v1, Lcom/samsung/helphub/pages/AirViewPenFingerPage$10;->$SwitchMap$com$samsung$helphub$pages$AirViewPenFingerPage$AirViewStatus:[I

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->checkAirviewStatus(Ljava/lang/String;)Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 184
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->startTutorial()V

    .line 187
    :goto_0
    return-void

    .line 164
    :pswitch_1
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->checkTalkbackAndMagnification()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    const v1, 0x7f0a01b1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 168
    :cond_0
    new-array v1, v6, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v7, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 173
    :pswitch_2
    new-array v1, v6, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v7, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 177
    :pswitch_3
    const v1, 0x7f0a01b6

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 181
    :pswitch_4
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->startTutorial()V

    goto :goto_0

    .line 162
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method protected checkPenFingerModePopup(Ljava/lang/String;I)V
    .locals 8
    .param p1, "fuctionMode"    # Ljava/lang/String;
    .param p2, "functionStringID"    # I

    .prologue
    const v7, 0x7f0a01b4

    const/4 v3, 0x2

    const v6, 0x7f0a01b0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 123
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 125
    .local v0, "res":Landroid/content/res/Resources;
    iput-boolean v4, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPenMode:Z

    .line 126
    sget-object v1, Lcom/samsung/helphub/pages/AirViewPenFingerPage$10;->$SwitchMap$com$samsung$helphub$pages$AirViewPenFingerPage$AirViewStatus:[I

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->checkAirviewStatus(Ljava/lang/String;)Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 152
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->startTutorial()V

    .line 155
    :goto_0
    return-void

    .line 128
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->checkTalkbackAndMagnification()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    const v1, 0x7f0a01b1

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v6, v1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_0
    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v7, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v6, v1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 137
    :pswitch_1
    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v7, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v6, v1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 141
    :pswitch_2
    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v7, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v6, v1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 145
    :pswitch_3
    const v1, 0x7f0a01b7

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v6, v1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 149
    :pswitch_4
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->startTutorial()V

    goto :goto_0

    .line 126
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method protected checkPenModePopup(Ljava/lang/String;I)V
    .locals 8
    .param p1, "fuctionMode"    # Ljava/lang/String;
    .param p2, "functionStringID"    # I

    .prologue
    const v7, 0x7f0a01b2

    const/4 v6, 0x2

    const v5, 0x7f0a01b0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 92
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 94
    .local v0, "res":Landroid/content/res/Resources;
    iput-boolean v3, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPenMode:Z

    .line 95
    sget-object v1, Lcom/samsung/helphub/pages/AirViewPenFingerPage$10;->$SwitchMap$com$samsung$helphub$pages$AirViewPenFingerPage$AirViewStatus:[I

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->checkAirviewStatus(Ljava/lang/String;)Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 117
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->startTutorial()V

    .line 120
    :goto_0
    return-void

    .line 97
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->checkTalkbackAndMagnification()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    const v1, 0x7f0a01b1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 101
    :cond_0
    new-array v1, v6, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v7, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 106
    :pswitch_1
    new-array v1, v6, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v7, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 110
    :pswitch_2
    const v1, 0x7f0a01b5

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v5, v1}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 114
    :pswitch_3
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->startTutorial()V

    goto :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected checkPenStatus()V
    .locals 1

    .prologue
    .line 260
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPenMode:Z

    if-eqz v0, :cond_1

    .line 261
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPeninsert:Z

    if-eqz v0, :cond_0

    .line 262
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showPenDetachDialog()V

    .line 273
    :goto_0
    return-void

    .line 264
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->startTutorial()V

    goto :goto_0

    .line 267
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPeninsert:Z

    if-eqz v0, :cond_2

    .line 268
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->startTutorial()V

    goto :goto_0

    .line 270
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->showPenDetachDialog()V

    goto :goto_0
.end method

.method protected checkTalkbackAndMagnification()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 226
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accessibility_display_magnification_enabled"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method protected abstract getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 277
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 279
    iput-object v1, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mPenDetachDialogFragment:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 282
    iget-object v0, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mPenDetachDialogFragment:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 283
    iput-object v1, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mPenDetachDialogFragment:Landroid/app/AlertDialog;

    .line 285
    :cond_1
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->onDestroy()V

    .line 286
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 48
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->onDestroyView()V

    .line 49
    return-void
.end method

.method protected setEnableSettings(Ljava/lang/String;)V
    .locals 5
    .param p1, "FunctionMode"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 235
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "air_view_master_onoff"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 236
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "air_view_mode"

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 237
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "pen_hovering"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 238
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 239
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, p1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 242
    const-string v1, "DCM"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "KDI"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 244
    :cond_0
    const-string v1, "pen_hovering_information_preview"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 245
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view_information_preview"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 254
    :cond_1
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.gesture.AIR_VIEW_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 255
    .local v0, "air_view_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 256
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 257
    return-void

    .line 246
    .end local v0    # "air_view_changed":Landroid/content/Intent;
    :cond_2
    const-string v1, "pen_hovering_progress_preview"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 247
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view_pregress_bar_preview"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 248
    :cond_3
    const-string v1, "pen_hovering_speed_dial_preview"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 249
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view_speed_dial_tip"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method protected setTalkbackAndMagnification()V
    .locals 3

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/helphub/utility/TackBackUtility;->turnOffTalkBack(Landroid/content/Context;)Z

    .line 231
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accessibility_display_magnification_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 232
    return-void
.end method

.method protected final showEnableSettingDialog(II)V
    .locals 4
    .param p1, "titleID"    # I
    .param p2, "summaryID"    # I

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    move-result-object v0

    .line 335
    .local v0, "dialogListener":Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0064

    new-instance v3, Lcom/samsung/helphub/pages/AirViewPenFingerPage$7;

    invoke-direct {v3, p0, v0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$7;-><init>(Lcom/samsung/helphub/pages/AirViewPenFingerPage;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0063

    new-instance v3, Lcom/samsung/helphub/pages/AirViewPenFingerPage$6;

    invoke-direct {v3, p0, v0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$6;-><init>(Lcom/samsung/helphub/pages/AirViewPenFingerPage;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/helphub/pages/AirViewPenFingerPage$5;

    invoke-direct {v2, p0, v0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$5;-><init>(Lcom/samsung/helphub/pages/AirViewPenFingerPage;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 361
    return-void
.end method

.method protected final showEnableSettingDialog(ILjava/lang/String;)V
    .locals 4
    .param p1, "titleID"    # I
    .param p2, "summaryID"    # Ljava/lang/String;

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    move-result-object v0

    .line 298
    .local v0, "dialogListener":Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0064

    new-instance v3, Lcom/samsung/helphub/pages/AirViewPenFingerPage$4;

    invoke-direct {v3, p0, v0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$4;-><init>(Lcom/samsung/helphub/pages/AirViewPenFingerPage;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0063

    new-instance v3, Lcom/samsung/helphub/pages/AirViewPenFingerPage$3;

    invoke-direct {v3, p0, v0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$3;-><init>(Lcom/samsung/helphub/pages/AirViewPenFingerPage;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/helphub/pages/AirViewPenFingerPage$2;

    invoke-direct {v2, p0, v0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$2;-><init>(Lcom/samsung/helphub/pages/AirViewPenFingerPage;Lcom/samsung/helphub/pages/IEnableSettingDialogListener;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 322
    return-void
.end method

.method protected final showPenDetachDialog()V
    .locals 6

    .prologue
    .line 365
    iget-boolean v1, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->isPenMode:Z

    if-eqz v1, :cond_0

    .line 366
    const v0, 0x7f0a01af

    .line 371
    .local v0, "penDetachMessage":I
    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a056b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/helphub/pages/AirViewPenFingerPage$8;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$8;-><init>(Lcom/samsung/helphub/pages/AirViewPenFingerPage;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mPenDetachDialogFragment:Landroid/app/AlertDialog;

    .line 379
    new-instance v1, Lcom/samsung/helphub/pages/AirViewPenFingerPage$9;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$9;-><init>(Lcom/samsung/helphub/pages/AirViewPenFingerPage;)V

    iput-object v1, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mPenDetachRunnable:Ljava/lang/Runnable;

    .line 390
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mPenDetachHandler:Landroid/os/Handler;

    .line 391
    iget-object v1, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mPenDetachHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->mPenDetachRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 392
    return-void

    .line 368
    .end local v0    # "penDetachMessage":I
    :cond_0
    const v0, 0x7f0a01ae

    .restart local v0    # "penDetachMessage":I
    goto :goto_0
.end method

.method protected abstract startTutorial()V
.end method
