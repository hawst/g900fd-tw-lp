.class public Lcom/samsung/helphub/pages/tethering/USBTetheringVZW;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "USBTetheringVZW.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 22
    .local v0, "result":Landroid/view/View;
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "VZW"

    invoke-static {v3}, Lcom/samsung/helphub/HelpHubCommon;->isCurrentCarrier(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 23
    const/4 v1, 0x0

    .line 24
    .local v1, "tv":Landroid/widget/TextView;
    const v3, 0x7f0d0032

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "tv":Landroid/widget/TextView;
    check-cast v1, Landroid/widget/TextView;

    .line 25
    .restart local v1    # "tv":Landroid/widget/TextView;
    const v3, 0x7f0a0869

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 28
    .end local v1    # "tv":Landroid/widget/TextView;
    :cond_0
    const v3, 0x7f0d0035

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 29
    .local v2, "webview":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/tethering/USBTetheringVZW;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0862

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, " <a href=http://www.android.com/tether#usb>http://www.android.com/tether#usb</a></body></html>"

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 30
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 31
    return-object v0
.end method
