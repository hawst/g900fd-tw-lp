.class public Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "GalleryViewingPictureResizingThumbnailsEasy.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 20
    new-instance v0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy$1;-><init>(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->isClicked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->isClicked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 50
    .local v1, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-object v1

    .line 53
    :cond_1
    const-string v2, "ro.product.name"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "m2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 54
    const v2, 0x7f0d02df

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 55
    .local v0, "mImageView":Landroid/widget/ImageView;
    const v2, 0x7f020092

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
