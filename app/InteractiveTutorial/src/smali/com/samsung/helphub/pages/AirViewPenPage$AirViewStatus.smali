.class public final enum Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;
.super Ljava/lang/Enum;
.source "AirViewPenPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/AirViewPenPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AirViewStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

.field public static final enum AllEnalbed:Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

.field public static final enum DisabledAirView:Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

.field public static final enum DisabledFuction:Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 18
    new-instance v0, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    const-string v1, "DisabledAirView"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;->DisabledAirView:Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    new-instance v0, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    const-string v1, "DisabledFuction"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;->DisabledFuction:Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    new-instance v0, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    const-string v1, "AllEnalbed"

    invoke-direct {v0, v1, v3, v5}, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;->AllEnalbed:Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    .line 17
    new-array v0, v5, [Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    sget-object v1, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;->DisabledAirView:Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;->DisabledFuction:Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;->AllEnalbed:Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;->$VALUES:[Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput p3, p0, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;->value:I

    .line 23
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    return-object v0
.end method

.method public static values()[Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;->$VALUES:[Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    invoke-virtual {v0}, [Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/helphub/pages/AirViewPenPage$AirViewStatus;

    return-object v0
.end method
