.class Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;
.super Landroid/os/Handler;
.source "TutorialDragDownAnimView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->this$0:Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v8, 0xc8

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 129
    const-string v0, "STATUSBAR-DropDownAnimView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mHandler : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 217
    :goto_0
    return-void

    .line 135
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->this$0:Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    # getter for: Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;
    invoke-static {v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->access$000(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)[Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->this$0:Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    # getter for: Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownImageView:[Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->access$100(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)[Landroid/widget/ImageView;

    move-result-object v0

    aget-object v0, v0, v4

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    aput-object v0, v1, v4

    .line 139
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->this$0:Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    # getter for: Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownImageView:[Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->access$100(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)[Landroid/widget/ImageView;

    move-result-object v0

    aget-object v0, v0, v4

    new-instance v1, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1$1;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1$1;-><init>(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 149
    invoke-virtual {p0, v3, v8, v9}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 155
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->this$0:Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    # getter for: Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;
    invoke-static {v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->access$000(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)[Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->this$0:Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    # getter for: Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownImageView:[Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->access$100(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)[Landroid/widget/ImageView;

    move-result-object v0

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    aput-object v0, v1, v3

    .line 159
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->this$0:Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    # getter for: Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownImageView:[Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->access$100(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)[Landroid/widget/ImageView;

    move-result-object v0

    aget-object v0, v0, v3

    new-instance v1, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1$2;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1$2;-><init>(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 169
    invoke-virtual {p0, v4, v8, v9}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 175
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->this$0:Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    # getter for: Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;
    invoke-static {v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->access$000(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)[Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->this$0:Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    # getter for: Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownImageView:[Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->access$100(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)[Landroid/widget/ImageView;

    move-result-object v0

    aget-object v0, v0, v6

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    aput-object v0, v1, v6

    .line 179
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->this$0:Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    # getter for: Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownImageView:[Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->access$100(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)[Landroid/widget/ImageView;

    move-result-object v0

    aget-object v0, v0, v6

    new-instance v1, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1$3;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1$3;-><init>(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 189
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v8, v9}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 195
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->this$0:Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    # getter for: Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownList:[Landroid/graphics/drawable/AnimationDrawable;
    invoke-static {v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->access$000(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)[Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->this$0:Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    # getter for: Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownImageView:[Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->access$100(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)[Landroid/widget/ImageView;

    move-result-object v0

    aget-object v0, v0, v5

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    aput-object v0, v1, v5

    .line 199
    iget-object v0, p0, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;->this$0:Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;

    # getter for: Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->mDragDownImageView:[Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;->access$100(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView;)[Landroid/widget/ImageView;

    move-result-object v0

    aget-object v0, v0, v5

    new-instance v1, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1$4;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1$4;-><init>(Lcom/samsung/helphub/pages/notifications/TutorialDragDownAnimView$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
