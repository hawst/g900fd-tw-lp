.class public abstract Lcom/samsung/helphub/pages/online/WebFragmentBase;
.super Landroid/app/Fragment;
.source "WebFragmentBase.java"

# interfaces
.implements Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;
.implements Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/pages/online/WebFragmentBase$2;
    }
.end annotation


# static fields
.field private static final CONNECTION_DIALOG_TAG:Ljava/lang/String; = "connection_dialog"

.field private static final DO_POP_BACK_STACK_KEY:Ljava/lang/String; = "online_help:DoPopBackStack"

.field private static final TAG:Ljava/lang/String; = "InteractiveTutorial : WebFragmentBase"


# instance fields
.field private mDataConnectionFragment:Landroid/app/DialogFragment;

.field private mIsWiFiOpenedFromDataDialog:Z

.field private mLoadWeb:Z

.field private mPopBackStack:Z

.field protected mTwoPane:Z

.field public mWebView:Landroid/webkit/WebView;

.field private final mWebViewClient:Landroid/webkit/WebViewClient;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 34
    iput-boolean v1, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mTwoPane:Z

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mLoadWeb:Z

    .line 45
    iput-boolean v1, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mIsWiFiOpenedFromDataDialog:Z

    .line 257
    new-instance v0, Lcom/samsung/helphub/pages/online/WebFragmentBase$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase$1;-><init>(Lcom/samsung/helphub/pages/online/WebFragmentBase;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebViewClient:Landroid/webkit/WebViewClient;

    return-void
.end method


# virtual methods
.method protected abstract getUrl()Ljava/lang/String;
.end method

.method protected loadData()V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 214
    return-void
.end method

.method public onClickCancel()V
    .locals 3

    .prologue
    .line 218
    iget-object v1, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    if-eqz v1, :cond_0

    .line 219
    iget-object v1, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    invoke-virtual {v1}, Landroid/app/DialogFragment;->dismiss()V

    .line 223
    :cond_0
    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mPopBackStack:Z

    if-eqz v1, :cond_1

    .line 224
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "HelpHubSecondDepthActivity"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 225
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 234
    :cond_1
    :goto_0
    return-void

    .line 228
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->popBackStack()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 231
    :catch_0
    move-exception v0

    .line 232
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "InteractiveTutorial : WebFragmentBase"

    const-string v2, "NullpointException in onClickCancel()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onClickMobileConnection()V
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 252
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->loadData()V

    .line 253
    return-void
.end method

.method public onClickWiFi()V
    .locals 3

    .prologue
    .line 240
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 241
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.Settings$WifiSettingsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    const-string v1, "settings:guide_mode"

    const-string v2, "wifi"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 243
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 244
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->startActivity(Landroid/content/Intent;)V

    .line 246
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mIsWiFiOpenedFromDataDialog:Z

    .line 247
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0d0087

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mTwoPane:Z

    .line 72
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 50
    const v2, 0x7f04009b

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 51
    .local v1, "result":Landroid/view/View;
    const v2, 0x7f0d0098

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    iput-object v2, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    .line 52
    iget-object v2, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 53
    iget-object v2, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 54
    iget-object v2, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    new-instance v3, Landroid/webkit/WebViewClient;

    invoke-direct {v3}, Landroid/webkit/WebViewClient;-><init>()V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 57
    const v2, 0x7f0d0097

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 59
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 60
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 61
    const-string v2, "online_help:DoPopBackStack"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mPopBackStack:Z

    .line 63
    :cond_0
    return-object v1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 307
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 294
    const/4 v0, 0x0

    .line 297
    .local v0, "result":Z
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    .line 298
    iget-object v1, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 299
    iget-object v1, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->goBack()V

    .line 300
    const/4 v0, 0x1

    .line 303
    :cond_0
    return v0
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 199
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/helphub/HelpHubCommon;->isNetworkConnected(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/samsung/helphub/HelpHubCommon;->isNetworkConnected(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    :cond_0
    iput-boolean v2, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mLoadWeb:Z

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    if-eqz v0, :cond_2

    .line 204
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    .line 208
    :cond_2
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 209
    return-void
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 76
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 77
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    if-eqz v4, :cond_0

    .line 78
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    invoke-virtual {v4}, Landroid/app/DialogFragment;->dismiss()V

    .line 79
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, v7}, Lcom/samsung/helphub/HelpHubCommon;->isNetworkConnected(Landroid/content/Context;I)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const/4 v5, 0x7

    invoke-static {v4, v5}, Lcom/samsung/helphub/HelpHubCommon;->isNetworkConnected(Landroid/content/Context;I)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const/16 v5, 0x9

    invoke-static {v4, v5}, Lcom/samsung/helphub/HelpHubCommon;->isNetworkConnected(Landroid/content/Context;I)Z

    move-result v4

    if-nez v4, :cond_8

    iget-boolean v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mLoadWeb:Z

    if-eqz v4, :cond_8

    .line 89
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, v6}, Lcom/samsung/helphub/HelpHubCommon;->isNetworkConnected(Landroid/content/Context;I)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 91
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/helphub/utility/PreferenceHelper;->getDataPreference(Landroid/content/Context;)Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;

    move-result-object v2

    .line 94
    .local v2, "dataPreference":Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;
    if-eqz v2, :cond_3

    sget-object v4, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->NONE:Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;

    if-eq v4, v2, :cond_3

    .line 97
    sget-object v4, Lcom/samsung/helphub/pages/online/WebFragmentBase$2;->$SwitchMap$com$samsung$helphub$utility$PreferenceHelper$DataSelectionPreferences:[I

    invoke-virtual {v2}, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 195
    .end local v2    # "dataPreference":Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;
    :cond_1
    :goto_0
    return-void

    .line 99
    .restart local v2    # "dataPreference":Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->onClickMobileConnection()V

    goto :goto_0

    .line 103
    :pswitch_1
    iget-boolean v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mIsWiFiOpenedFromDataDialog:Z

    if-nez v4, :cond_2

    .line 104
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->onClickWiFi()V

    goto :goto_0

    .line 106
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->onClickCancel()V

    goto :goto_0

    .line 114
    :cond_3
    const-string v4, "ro.csc.sales_code"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 115
    .local v3, "salesCode":Ljava/lang/String;
    const-string v4, "LGT"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 117
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->NONE:Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;

    invoke-static {v4, v5}, Lcom/samsung/helphub/utility/PreferenceHelper;->setDataPreference(Landroid/content/Context;Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;)V

    .line 119
    iput-boolean v6, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mIsWiFiOpenedFromDataDialog:Z

    .line 121
    new-instance v4, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;

    invoke-direct {v4}, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;-><init>()V

    iput-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    .line 122
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    check-cast v4, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;

    invoke-virtual {v4, p0}, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;->setDialogListener(Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;)V

    .line 123
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "connection_dialog"

    invoke-virtual {v4, v5, v6}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 126
    :cond_4
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 127
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->NONE:Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;

    invoke-static {v4, v5}, Lcom/samsung/helphub/utility/PreferenceHelper;->setDataPreference(Landroid/content/Context;Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;)V

    .line 129
    iput-boolean v6, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mIsWiFiOpenedFromDataDialog:Z

    .line 130
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "MOBILE_CHECKED"

    invoke-static {v4, v5}, Lcom/samsung/helphub/utility/PreferenceHelper;->getCheckedPreference(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    move-result-object v1

    .line 132
    .local v1, "checkedPreference":Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;
    sget-object v4, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->NONE_CHECKED:Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    if-ne v1, v4, :cond_5

    .line 133
    new-instance v4, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;

    invoke-direct {v4}, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;-><init>()V

    iput-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    .line 134
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    check-cast v4, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;

    invoke-virtual {v4, p0}, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->setDialogListener(Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;)V

    .line 136
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "connection_dialog"

    invoke-virtual {v4, v5, v6}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 140
    :cond_5
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    if-eqz v4, :cond_6

    .line 141
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4, v6}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 142
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->loadData()V

    .line 147
    .end local v1    # "checkedPreference":Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;
    :cond_6
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    if-eqz v4, :cond_1

    .line 148
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4, v6}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 149
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->loadData()V

    .line 150
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v4

    invoke-virtual {v4}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v0

    .line 151
    .local v0, "UA":Ljava/lang/String;
    const-string v4, "InteractiveTutorial : WebFragmentBase"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UA:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 155
    .end local v0    # "UA":Ljava/lang/String;
    .end local v2    # "dataPreference":Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;
    .end local v3    # "salesCode":Ljava/lang/String;
    :cond_7
    new-instance v4, Lcom/samsung/helphub/pages/online/WifiDialogFragment;

    invoke-direct {v4}, Lcom/samsung/helphub/pages/online/WifiDialogFragment;-><init>()V

    iput-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    .line 156
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    check-cast v4, Lcom/samsung/helphub/pages/online/WifiDialogFragment;

    invoke-virtual {v4, p0}, Lcom/samsung/helphub/pages/online/WifiDialogFragment;->setDialogListener(Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;)V

    .line 157
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "connection_dialog"

    invoke-virtual {v4, v5, v6}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 160
    :cond_8
    iget-boolean v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mLoadWeb:Z

    if-eqz v4, :cond_b

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, v7}, Lcom/samsung/helphub/HelpHubCommon;->isNetworkConnected(Landroid/content/Context;I)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 162
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 163
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->NONE:Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;

    invoke-static {v4, v5}, Lcom/samsung/helphub/utility/PreferenceHelper;->setDataPreference(Landroid/content/Context;Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;)V

    .line 165
    iput-boolean v6, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mIsWiFiOpenedFromDataDialog:Z

    .line 166
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "WLAN_CHECKED"

    invoke-static {v4, v5}, Lcom/samsung/helphub/utility/PreferenceHelper;->getCheckedPreference(Landroid/content/Context;Ljava/lang/String;)Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    move-result-object v1

    .line 168
    .restart local v1    # "checkedPreference":Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;
    sget-object v4, Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;->NONE_CHECKED:Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;

    if-ne v1, v4, :cond_9

    .line 169
    new-instance v4, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;

    invoke-direct {v4}, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;-><init>()V

    iput-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    .line 170
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    check-cast v4, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;

    invoke-virtual {v4, p0}, Lcom/samsung/helphub/pages/online/ConnectionDialogFragment;->setDialogListener(Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;)V

    .line 172
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mDataConnectionFragment:Landroid/app/DialogFragment;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "connection_dialog"

    invoke-virtual {v4, v5, v6}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 175
    :cond_9
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mLoadWeb:Z

    if-eqz v4, :cond_1

    .line 176
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4, v6}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 177
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->loadData()V

    goto/16 :goto_0

    .line 182
    .end local v1    # "checkedPreference":Lcom/samsung/helphub/utility/PreferenceHelper$CheckedPreferences;
    :cond_a
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mLoadWeb:Z

    if-eqz v4, :cond_1

    .line 183
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4, v6}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 184
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->loadData()V

    goto/16 :goto_0

    .line 189
    :cond_b
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mLoadWeb:Z

    if-eqz v4, :cond_1

    .line 190
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/WebFragmentBase;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v4, v6}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 191
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;->loadData()V

    goto/16 :goto_0

    .line 97
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
