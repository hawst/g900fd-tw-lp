.class Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate$1;
.super Ljava/lang/Object;
.source "MultiWindowBezelTemplate.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate$1;->this$0:Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 41
    iget-object v1, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate$1;->this$0:Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate;

    iget-object v1, v1, Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate;->mMultiWindowHelpUtil:Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

    iget-object v2, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate$1;->this$0:Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;->checkTalkbackEnable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    iget-object v1, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate$1;->this$0:Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate;

    iget-object v1, v1, Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate;->mMultiWindowHelpUtil:Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

    const v2, 0x7f0a00e4

    const v3, 0x7f0a00e5

    const-string v4, "com.sec.android.app.flashBarService.action.TryMultiWindowBezelTemplate"

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;->showTalkBackDisablePopup(IILjava/lang/String;)V

    .line 51
    :goto_0
    return-void

    .line 47
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.flashBarService.action.TryMultiWindowBezelTemplate"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 49
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate$1;->this$0:Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowBezelTemplate;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
