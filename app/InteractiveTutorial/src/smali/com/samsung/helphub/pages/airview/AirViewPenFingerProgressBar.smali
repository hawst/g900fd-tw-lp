.class public Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;
.super Lcom/samsung/helphub/pages/AirViewPenFingerPage;
.source "AirViewPenFingerProgressBar.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# instance fields
.field final functionID:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;-><init>()V

    .line 23
    const v0, 0x7f0a01a3

    iput v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->functionID:I

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;
    .param p1, "x1"    # Z

    .prologue
    .line 21
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 21
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->checkPenFingerModePopup(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 100
    return-object p0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->isClicked:Z

    .line 77
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->checkTalkbackAndMagnification()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->setTalkbackAndMagnification()V

    .line 68
    :cond_0
    const-string v0, "pen_hovering_progress_preview"

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->setEnableSettings(Ljava/lang/String;)V

    .line 69
    const-string v0, "finger_air_view_pregress_bar_preview"

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->setEnableSettings(Ljava/lang/String;)V

    .line 71
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->startTutorial()V

    .line 72
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 28
    .local v2, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 29
    const v3, 0x7f0d0238

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 31
    .local v1, "mImageViewer_pen":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "tutorial_air_view_progress_preview_pen"

    invoke-static {v3, v1, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 36
    .end local v1    # "mImageViewer_pen":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_0
    const v3, 0x7f0d0081

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 37
    .local v0, "button":Landroid/widget/Button;
    new-instance v3, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar$1;

    invoke-direct {v3, p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar$1;-><init>(Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    return-object v2
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->onResume()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->isClicked:Z

    .line 59
    return-void
.end method

.method protected startTutorial()V
    .locals 6

    .prologue
    .line 81
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.START_HELP_VIDEO_PROGRESS_BAR_PREVIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 82
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 83
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "video_help"

    const-string v4, "raw"

    const-string v5, "com.samsung.helpplugin"

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/helphub/HelpHubCommon;->getResourceId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 84
    .local v1, "resId":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 85
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "android.resource://com.samsung.helpplugin/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "video/mp4"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    const-string v2, "type"

    const-string v3, "help"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->startActivity(Landroid/content/Intent;)V

    .line 96
    .end local v1    # "resId":I
    :goto_0
    return-void

    .line 89
    .restart local v1    # "resId":I
    :cond_0
    const-string v2, "The Video resource not downloaded yet"

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->LogD(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    .end local v1    # "resId":I
    :cond_1
    const-string v2, "file:///system/media/video/video_help.mp4"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "video/mp4"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    const-string v2, "type"

    const-string v3, "help"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerProgressBar;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
