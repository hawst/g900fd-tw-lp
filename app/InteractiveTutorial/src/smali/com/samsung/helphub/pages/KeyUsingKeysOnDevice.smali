.class public Lcom/samsung/helphub/pages/KeyUsingKeysOnDevice;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "KeyUsingKeysOnDevice.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 14
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super/range {p0 .. p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v10

    .line 23
    .local v10, "result":Landroid/view/View;
    const-string v12, "ro.product.name"

    invoke-static {v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 25
    .local v9, "productName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/KeyUsingKeysOnDevice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-static {v12}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 26
    const v12, 0x7f0d0020

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 27
    .local v2, "mImageView1":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/KeyUsingKeysOnDevice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    const-string v13, "help_key_power_key"

    invoke-static {v12, v2, v13}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 28
    const v12, 0x7f0d0021

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 29
    .local v3, "mImageView2":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/KeyUsingKeysOnDevice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    const-string v13, "help_key_volume_key"

    invoke-static {v12, v3, v13}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 30
    const v12, 0x7f0d0022

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 31
    .local v4, "mImageView3":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/KeyUsingKeysOnDevice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    const-string v13, "help_key_shutter_key"

    invoke-static {v12, v4, v13}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 32
    const v12, 0x7f0d0023

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 33
    .local v5, "mImageView4":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/KeyUsingKeysOnDevice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    const-string v13, "help_key_recent_key"

    invoke-static {v12, v5, v13}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 34
    const v12, 0x7f0d0025

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 35
    .local v6, "mImageView6":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/KeyUsingKeysOnDevice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    const-string v13, "help_key_home_key"

    invoke-static {v12, v6, v13}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 36
    const v12, 0x7f0d0026

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 37
    .local v7, "mImageView7":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/KeyUsingKeysOnDevice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    const-string v13, "help_key_back_key"

    invoke-static {v12, v7, v13}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 38
    const v12, 0x7f0d0027

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 39
    .local v8, "mImageView8":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/KeyUsingKeysOnDevice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    const-string v13, "help_key_more_option_button"

    invoke-static {v12, v8, v13}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 42
    .end local v2    # "mImageView1":Landroid/widget/ImageView;
    .end local v3    # "mImageView2":Landroid/widget/ImageView;
    .end local v4    # "mImageView3":Landroid/widget/ImageView;
    .end local v5    # "mImageView4":Landroid/widget/ImageView;
    .end local v6    # "mImageView6":Landroid/widget/ImageView;
    .end local v7    # "mImageView7":Landroid/widget/ImageView;
    .end local v8    # "mImageView8":Landroid/widget/ImageView;
    :cond_0
    const-string v12, "VZW"

    invoke-static {v12}, Lcom/samsung/helphub/HelpHubCommon;->isCurrentCarrier(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 43
    const v12, 0x7f0d005b

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 44
    .local v0, "layout_RecentKey1":Landroid/widget/RelativeLayout;
    if-eqz v0, :cond_1

    .line 45
    const/16 v12, 0x8

    invoke-virtual {v0, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 47
    :cond_1
    const v12, 0x7f0d005c

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 48
    .local v1, "layout_RecentKey2":Landroid/widget/RelativeLayout;
    if-eqz v1, :cond_2

    .line 49
    const/4 v12, 0x0

    invoke-virtual {v1, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 53
    .end local v0    # "layout_RecentKey1":Landroid/widget/RelativeLayout;
    .end local v1    # "layout_RecentKey2":Landroid/widget/RelativeLayout;
    :cond_2
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCMCCModel()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 54
    const v12, 0x7f0d003e

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 55
    .local v11, "textView":Landroid/widget/TextView;
    if-eqz v11, :cond_3

    .line 56
    const v12, 0x7f0a007a

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    .line 59
    .end local v11    # "textView":Landroid/widget/TextView;
    :cond_3
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isSReminderPreloaded()Z

    move-result v12

    if-eqz v12, :cond_4

    .line 60
    const v12, 0x7f0d003e

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 61
    .restart local v11    # "textView":Landroid/widget/TextView;
    if-eqz v11, :cond_4

    .line 62
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCMCCModel()Z

    move-result v12

    if-eqz v12, :cond_7

    .line 63
    const v12, 0x7f0a007b

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    .line 69
    .end local v11    # "textView":Landroid/widget/TextView;
    :cond_4
    :goto_0
    const-string v12, "ro.product.name"

    invoke-static {v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "tblte"

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_5

    const-string v12, "ro.product.name"

    invoke-static {v12}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "tbelte"

    invoke-virtual {v12, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_5

    const-string v12, "SCL24"

    invoke-virtual {v9, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_5

    const-string v12, "SC-01G"

    invoke-virtual {v9, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 71
    :cond_5
    const v12, 0x7f0d0020

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 72
    .restart local v2    # "mImageView1":Landroid/widget/ImageView;
    const v12, 0x7f020159

    invoke-virtual {v2, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 73
    const v12, 0x7f0d0021

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 74
    .restart local v3    # "mImageView2":Landroid/widget/ImageView;
    const v12, 0x7f02015f

    invoke-virtual {v3, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 75
    const v12, 0x7f0d0023

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 76
    .restart local v5    # "mImageView4":Landroid/widget/ImageView;
    const v12, 0x7f02015c

    invoke-virtual {v5, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 77
    const v12, 0x7f0d0025

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 78
    .restart local v6    # "mImageView6":Landroid/widget/ImageView;
    const v12, 0x7f020155

    invoke-virtual {v6, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 79
    const v12, 0x7f0d0026

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 80
    .restart local v7    # "mImageView7":Landroid/widget/ImageView;
    const v12, 0x7f020152

    invoke-virtual {v7, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 81
    const v12, 0x7f0d0027

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 82
    .restart local v8    # "mImageView8":Landroid/widget/ImageView;
    const v12, 0x7f020157

    invoke-virtual {v8, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 85
    .end local v2    # "mImageView1":Landroid/widget/ImageView;
    .end local v3    # "mImageView2":Landroid/widget/ImageView;
    .end local v5    # "mImageView4":Landroid/widget/ImageView;
    .end local v6    # "mImageView6":Landroid/widget/ImageView;
    .end local v7    # "mImageView7":Landroid/widget/ImageView;
    .end local v8    # "mImageView8":Landroid/widget/ImageView;
    :cond_6
    return-object v10

    .line 65
    .restart local v11    # "textView":Landroid/widget/TextView;
    :cond_7
    const v12, 0x7f0a0099

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0
.end method
