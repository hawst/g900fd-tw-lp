.class Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW$1;
.super Ljava/lang/Object;
.source "PortableHotspotVZW.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW;)V
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW$1;->this$0:Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 23
    iget-object v1, p0, Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW$1;->this$0:Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW;

    # getter for: Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW;->isClicked:Z
    invoke-static {v1}, Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW;->access$000(Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 33
    :goto_0
    return-void

    .line 25
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW$1;->this$0:Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW;

    # setter for: Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW;->isClicked:Z
    invoke-static {v1, v3}, Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW;->access$102(Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW;Z)Z

    .line 26
    const-string v1, "PortableHotspotPage"

    const-string v2, "onClick() is called"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.ACTION_MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 28
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 29
    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.wifi.mobileap.WifiApSettings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 30
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 31
    const-string v1, "fromHelp"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 32
    iget-object v1, p0, Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW$1;->this$0:Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/tethering/PortableHotspotVZW;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
