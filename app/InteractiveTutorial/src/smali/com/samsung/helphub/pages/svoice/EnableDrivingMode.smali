.class public Lcom/samsung/helphub/pages/svoice/EnableDrivingMode;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "EnableDrivingMode.java"


# instance fields
.field private mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private view1:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 107
    new-instance v0, Lcom/samsung/helphub/pages/svoice/EnableDrivingMode$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/svoice/EnableDrivingMode$1;-><init>(Lcom/samsung/helphub/pages/svoice/EnableDrivingMode;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/svoice/EnableDrivingMode;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/helphub/pages/svoice/EnableDrivingMode;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 33
    .local v0, "result":Landroid/view/View;
    const v1, 0x7f0d0032

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/helphub/pages/svoice/EnableDrivingMode;->view1:Landroid/widget/TextView;

    .line 35
    return-object v0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 40
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onResume()V

    .line 41
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/EnableDrivingMode;->sethands_freetext()V

    .line 42
    return-void
.end method

.method public sethands_freetext()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 45
    new-instance v10, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-direct {v10}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;-><init>()V

    iput-object v10, p0, Lcom/samsung/helphub/pages/svoice/EnableDrivingMode;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    .line 46
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/EnableDrivingMode;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 47
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/EnableDrivingMode;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 48
    .local v5, "resources":Landroid/content/res/Resources;
    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 50
    .local v0, "configuration":Landroid/content/res/Configuration;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    .line 52
    .local v4, "previousLocale":Ljava/util/Locale;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "voicetalk_language"

    invoke-static {v10, v11}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 55
    .local v8, "voiceLocale":Ljava/lang/String;
    if-nez v8, :cond_7

    .line 56
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 57
    const-string v8, "zh-CN"

    .line 65
    :cond_0
    :goto_0
    const-string v10, "en-gb_GB"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 66
    const-string v8, "en_GB"

    .line 68
    :cond_1
    new-instance v9, Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v8, v10, v11}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .local v9, "voicetalkLocale":Ljava/util/Locale;
    const-string v10, "pt-BR"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 72
    const-string v10, "-"

    invoke-virtual {v8, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 73
    .local v6, "splitVoiceLocale":[Ljava/lang/String;
    new-instance v9, Ljava/util/Locale;

    .end local v9    # "voicetalkLocale":Ljava/util/Locale;
    aget-object v10, v6, v12

    aget-object v11, v6, v13

    invoke-direct {v9, v10, v11}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .end local v6    # "splitVoiceLocale":[Ljava/lang/String;
    .restart local v9    # "voicetalkLocale":Ljava/util/Locale;
    :cond_2
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v10

    if-eqz v10, :cond_3

    const-string v10, "zh-CN"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 78
    sget-object v9, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    .line 80
    :cond_3
    iget-object v10, p0, Lcom/samsung/helphub/pages/svoice/EnableDrivingMode;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v10}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->isUseChineseModel()Z

    move-result v10

    if-nez v10, :cond_4

    invoke-virtual {v9}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "zh_CN"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 82
    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 87
    :cond_4
    iput-object v9, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 88
    invoke-virtual {v5, v0, v14}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 90
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/EnableDrivingMode;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a0448

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 93
    .local v3, "handsfreecommand":Ljava/lang/String;
    iput-object v4, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 94
    invoke-virtual {v5, v0, v14}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 96
    const v10, 0x7f0a0445

    invoke-virtual {p0, v10}, Lcom/samsung/helphub/pages/svoice/EnableDrivingMode;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 99
    .local v7, "tempForCall":Ljava/lang/String;
    new-array v10, v13, [Ljava/lang/Object;

    aput-object v3, v10, v12

    invoke-static {v7, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 101
    .local v2, "guideCallString":Ljava/lang/String;
    iget-object v10, p0, Lcom/samsung/helphub/pages/svoice/EnableDrivingMode;->view1:Landroid/widget/TextView;

    if-eqz v10, :cond_5

    .line 102
    iget-object v10, p0, Lcom/samsung/helphub/pages/svoice/EnableDrivingMode;->view1:Landroid/widget/TextView;

    invoke-virtual {v10, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    :cond_5
    return-void

    .line 59
    .end local v2    # "guideCallString":Ljava/lang/String;
    .end local v3    # "handsfreecommand":Ljava/lang/String;
    .end local v7    # "tempForCall":Ljava/lang/String;
    .end local v9    # "voicetalkLocale":Ljava/util/Locale;
    :cond_6
    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 61
    :cond_7
    const-string v10, "v-es-LA"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 62
    const-string v8, "es"

    goto/16 :goto_0
.end method
