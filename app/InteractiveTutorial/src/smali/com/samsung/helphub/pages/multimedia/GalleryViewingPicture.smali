.class public Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "GalleryViewingPicture.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 12
    new-instance v0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture$1;-><init>(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;

    .prologue
    .line 10
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;->isClicked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;
    .param p1, "x1"    # Z

    .prologue
    .line 10
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;->isClicked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPicture;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method
