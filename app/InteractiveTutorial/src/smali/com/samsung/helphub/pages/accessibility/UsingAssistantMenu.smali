.class public Lcom/samsung/helphub/pages/accessibility/UsingAssistantMenu;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "UsingAssistantMenu.java"


# instance fields
.field private mSupportHoverZoom:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/accessibility/UsingAssistantMenu;->mSupportHoverZoom:Z

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 23
    .local v2, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/accessibility/UsingAssistantMenu;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.sec.feature.overlaymagnifier"

    invoke-static {v3, v4}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/samsung/helphub/pages/accessibility/UsingAssistantMenu;->mSupportHoverZoom:Z

    .line 24
    const v3, 0x7f0d043a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 25
    .local v0, "mImageView":Landroid/widget/ImageView;
    const v3, 0x7f0d043b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 27
    .local v1, "mMagnifyInfoLayout":Landroid/widget/LinearLayout;
    const-string v3, "UsingAssistantMenu"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SupportHoverZoom is : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/helphub/pages/accessibility/UsingAssistantMenu;->mSupportHoverZoom:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    iget-boolean v3, p0, Lcom/samsung/helphub/pages/accessibility/UsingAssistantMenu;->mSupportHoverZoom:Z

    if-nez v3, :cond_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/accessibility/UsingAssistantMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020061

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 32
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/accessibility/UsingAssistantMenu;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 36
    iget-boolean v3, p0, Lcom/samsung/helphub/pages/accessibility/UsingAssistantMenu;->mSupportHoverZoom:Z

    if-nez v3, :cond_2

    .line 37
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/accessibility/UsingAssistantMenu;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "cursor_image_no_magnify"

    invoke-static {v3, v0, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 42
    :cond_1
    :goto_0
    return-object v2

    .line 39
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/accessibility/UsingAssistantMenu;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "cursor_image"

    invoke-static {v3, v0, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0
.end method
