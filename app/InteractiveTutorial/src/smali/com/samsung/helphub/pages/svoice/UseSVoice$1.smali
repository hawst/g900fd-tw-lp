.class Lcom/samsung/helphub/pages/svoice/UseSVoice$1;
.super Ljava/lang/Object;
.source "UseSVoice.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/svoice/UseSVoice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/svoice/UseSVoice;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/svoice/UseSVoice;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/helphub/pages/svoice/UseSVoice$1;->this$0:Lcom/samsung/helphub/pages/svoice/UseSVoice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 55
    const-string v2, "com.vlingo.midas"

    .line 57
    .local v2, "pkgName":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/helphub/pages/svoice/UseSVoice$1;->this$0:Lcom/samsung/helphub/pages/svoice/UseSVoice;

    iget-object v4, p0, Lcom/samsung/helphub/pages/svoice/UseSVoice$1;->this$0:Lcom/samsung/helphub/pages/svoice/UseSVoice;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->getActivity()Landroid/app/Activity;

    move-result-object v4

    # invokes: Lcom/samsung/helphub/pages/svoice/UseSVoice;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v3, v4, v2}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->access$000(Lcom/samsung/helphub/pages/svoice/UseSVoice;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 58
    iget-object v3, p0, Lcom/samsung/helphub/pages/svoice/UseSVoice$1;->this$0:Lcom/samsung/helphub/pages/svoice/UseSVoice;

    # getter for: Lcom/samsung/helphub/pages/svoice/UseSVoice;->isClicked:Z
    invoke-static {v3}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->access$100(Lcom/samsung/helphub/pages/svoice/UseSVoice;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 72
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v3, p0, Lcom/samsung/helphub/pages/svoice/UseSVoice$1;->this$0:Lcom/samsung/helphub/pages/svoice/UseSVoice;

    const/4 v4, 0x1

    # setter for: Lcom/samsung/helphub/pages/svoice/UseSVoice;->isClicked:Z
    invoke-static {v3, v4}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->access$202(Lcom/samsung/helphub/pages/svoice/UseSVoice;Z)Z

    .line 62
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 63
    .local v1, "mIntent":Landroid/content/Intent;
    const-string v3, "com.vlingo.midas"

    const-string v4, "com.vlingo.midas.gui.ConversationHelpActivity"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    :try_start_0
    iget-object v3, p0, Lcom/samsung/helphub/pages/svoice/UseSVoice$1;->this$0:Lcom/samsung/helphub/pages/svoice/UseSVoice;

    invoke-virtual {v3, v1}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "UseSVoice ActivityNotFound Exception"

    invoke-static {v3}, Lcom/samsung/helphub/HelpHubCommon;->LogW(Ljava/lang/String;)V

    goto :goto_0

    .line 70
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v3, p0, Lcom/samsung/helphub/pages/svoice/UseSVoice$1;->this$0:Lcom/samsung/helphub/pages/svoice/UseSVoice;

    # invokes: Lcom/samsung/helphub/pages/svoice/UseSVoice;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v3, v2}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->access$300(Lcom/samsung/helphub/pages/svoice/UseSVoice;Ljava/lang/String;)V

    goto :goto_0
.end method
