.class public Lcom/samsung/helphub/pages/smartscreen/SmartStay;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "SmartStay.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/smartscreen/SmartStay;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/smartscreen/SmartStay;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/smartscreen/SmartStay;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/smartscreen/SmartStay;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/smartscreen/SmartStay;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/smartscreen/SmartStay;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 22
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->showEnableSettingDialog(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/smartscreen/SmartStay;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/smartscreen/SmartStay;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 22
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->showEnableSettingDialog(II)V

    return-void
.end method

.method public static isInternetHidden(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    const/4 v3, 0x1

    .line 100
    .local v3, "result":Z
    const/4 v1, 0x0

    .line 101
    .local v1, "piBrowser":Landroid/content/pm/PackageInfo;
    const/4 v2, 0x0

    .line 103
    .local v2, "piSBrowser":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.android.browser"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 108
    :goto_0
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.sec.android.app.sbrowser"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 112
    :goto_1
    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    .line 113
    :cond_0
    const/4 v3, 0x0

    .line 115
    :cond_1
    return v3

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 109
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 110
    .restart local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 50
    return-object p0
.end method

.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/samsung/helphub/pages/smartscreen/SmartStay$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/smartscreen/SmartStay$1;-><init>(Lcom/samsung/helphub/pages/smartscreen/SmartStay;)V

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->isClicked:Z

    .line 46
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 3

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/helphub/utility/TackBackUtility;->turnOffTalkBack(Landroid/content/Context;)Z

    .line 37
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "intelligent_sleep_mode"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 39
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->startTutorial()V

    .line 40
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 26
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 27
    .local v1, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 28
    const v2, 0x7f0d03ec

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 29
    .local v0, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "smart_screen_smart_stay_eye"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 31
    .end local v0    # "mImageView":Landroid/widget/ImageView;
    :cond_0
    return-object v1
.end method

.method protected startTutorial()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 82
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->isInternetHidden(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.email.help.SMART_STAY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 84
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v1, "fromHelp"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 85
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->startActivity(Landroid/content/Intent;)V

    .line 91
    :goto_0
    return-void

    .line 87
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.sbrowsertry.GUIDE_SMART_STAY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 88
    .restart local v0    # "mIntent":Landroid/content/Intent;
    const-string v1, "fromHelp"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 89
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
