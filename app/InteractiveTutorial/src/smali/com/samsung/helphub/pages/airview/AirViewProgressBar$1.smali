.class Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;
.super Ljava/lang/Object;
.source "AirViewProgressBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/airview/AirViewProgressBar;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/airview/AirViewProgressBar;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewProgressBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v9, 0x7f0a0570

    const v8, 0x7f0a056d

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 72
    iget-object v6, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewProgressBar;

    invoke-virtual {v6}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "finger_air_view"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_1

    move v2, v4

    .line 73
    .local v2, "masterStatus":Z
    :goto_0
    iget-object v6, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewProgressBar;

    invoke-virtual {v6}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "finger_air_view_pregress_bar_preview"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_2

    move v3, v4

    .line 74
    .local v3, "settingStatus":Z
    :goto_1
    iget-object v6, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewProgressBar;

    invoke-virtual {v6}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "accessibility_display_magnification_enabled"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_3

    move v0, v4

    .line 75
    .local v0, "MagnificationResult":Z
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewProgressBar;

    invoke-virtual {v5, v9}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewProgressBar;

    const v6, 0x7f0a0572

    invoke-virtual {v5, v6}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n- "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewProgressBar;

    const v6, 0x7f0a0573

    invoke-virtual {v5, v6}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n- "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewProgressBar;

    const v6, 0x7f0a0574

    invoke-virtual {v5, v6}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 79
    .local v1, "dialog_content":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-nez v3, :cond_4

    :cond_0
    iget-object v4, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewProgressBar;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_4

    if-nez v0, :cond_4

    .line 80
    iget-object v4, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewProgressBar;

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->showEnableSettingDialog(II)V
    invoke-static {v4, v8, v9}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->access$000(Lcom/samsung/helphub/pages/airview/AirViewProgressBar;II)V

    .line 90
    :goto_3
    return-void

    .end local v0    # "MagnificationResult":Z
    .end local v1    # "dialog_content":Ljava/lang/String;
    .end local v2    # "masterStatus":Z
    .end local v3    # "settingStatus":Z
    :cond_1
    move v2, v5

    .line 72
    goto/16 :goto_0

    .restart local v2    # "masterStatus":Z
    :cond_2
    move v3, v5

    .line 73
    goto :goto_1

    .restart local v3    # "settingStatus":Z
    :cond_3
    move v0, v5

    .line 74
    goto :goto_2

    .line 83
    .restart local v0    # "MagnificationResult":Z
    .restart local v1    # "dialog_content":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewProgressBar;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_5

    if-eqz v0, :cond_6

    .line 84
    :cond_5
    iget-object v4, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewProgressBar;

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->showEnableSettingDialog(ILjava/lang/String;)V
    invoke-static {v4, v8, v1}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->access$100(Lcom/samsung/helphub/pages/airview/AirViewProgressBar;ILjava/lang/String;)V

    goto :goto_3

    .line 88
    :cond_6
    iget-object v4, p0, Lcom/samsung/helphub/pages/airview/AirViewProgressBar$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewProgressBar;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/airview/AirViewProgressBar;->startTutorial()V

    goto :goto_3
.end method
