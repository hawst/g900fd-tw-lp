.class Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$3;
.super Ljava/lang/Object;
.source "UsingFeaturesInAirCommand.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->showTalkBackDisableDialog(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$3;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    iput-object p2, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$3;->val$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v2, 0x0

    .line 103
    iget-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$3;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    # getter for: Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->access$000(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accessibility_display_magnification_enabled"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 105
    iget-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$3;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    # getter for: Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->access$000(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accessibility_magnifier"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 107
    iget-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$3;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    # getter for: Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->access$000(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/helphub/utility/Utils;->turnOffTalkBack(Landroid/content/Context;)Z

    .line 108
    iget-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$3;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    # getter for: Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->access$000(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "air_button_onoff"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 110
    iget-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$3;->val$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 118
    :goto_0
    return-void

    .line 112
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$3;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    iget-object v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$3;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    # getter for: Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mPenableHelpIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->access$200(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 115
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$3;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    iget-object v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$3;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    # getter for: Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mEasyClipHelpIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->access$300(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 110
    :pswitch_data_0
    .packed-switch 0x7f0d0081
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
