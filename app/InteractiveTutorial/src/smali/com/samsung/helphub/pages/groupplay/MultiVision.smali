.class public Lcom/samsung/helphub/pages/groupplay/MultiVision;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "MultiVision.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 42
    new-instance v0, Lcom/samsung/helphub/pages/groupplay/MultiVision$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/groupplay/MultiVision$1;-><init>(Lcom/samsung/helphub/pages/groupplay/MultiVision;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/groupplay/MultiVision;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/groupplay/MultiVision;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/groupplay/MultiVision;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/groupplay/MultiVision;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/groupplay/MultiVision;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/groupplay/MultiVision;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/groupplay/MultiVision;->isClicked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/helphub/pages/groupplay/MultiVision;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/groupplay/MultiVision;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/groupplay/MultiVision;->isClicked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/groupplay/MultiVision;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/groupplay/MultiVision;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/groupplay/MultiVision;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/helphub/pages/groupplay/MultiVision;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 27
    .local v0, "contentView":Landroid/view/View;
    const v4, 0x7f0d02e4

    :try_start_0
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 28
    .local v1, "msg1":Landroid/widget/TextView;
    const v4, 0x7f0d02e5

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 29
    .local v2, "msg2":Landroid/widget/TextView;
    const v4, 0x7f0d02e6

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 31
    .local v3, "msg3":Landroid/widget/TextView;
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 32
    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 33
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    .end local v1    # "msg1":Landroid/widget/TextView;
    .end local v2    # "msg2":Landroid/widget/TextView;
    .end local v3    # "msg3":Landroid/widget/TextView;
    :cond_0
    :goto_0
    return-object v0

    .line 35
    :catch_0
    move-exception v4

    goto :goto_0
.end method
