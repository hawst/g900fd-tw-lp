.class public Lcom/samsung/helphub/pages/multiwindow/MultiWindowTemplate;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "MultiWindowTemplate.java"


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field mMultiWindowHelpUtil:Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 38
    new-instance v0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTemplate$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTemplate$1;-><init>(Lcom/samsung/helphub/pages/multiwindow/MultiWindowTemplate;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTemplate;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTemplate;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    new-instance v2, Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTemplate;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTemplate;->mMultiWindowHelpUtil:Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

    .line 32
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 33
    .local v1, "result":Landroid/view/View;
    const v2, 0x7f0d03a2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 34
    .local v0, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowTemplate;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "multi_window_template"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 35
    return-object v1
.end method
