.class Lcom/samsung/helphub/pages/nfc/TurnOnSBeam$1;
.super Ljava/lang/Object;
.source "TurnOnSBeam.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/nfc/TurnOnSBeam;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/nfc/TurnOnSBeam;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/nfc/TurnOnSBeam;)V
    .locals 0

    .prologue
    .line 13
    iput-object p1, p0, Lcom/samsung/helphub/pages/nfc/TurnOnSBeam$1;->this$0:Lcom/samsung/helphub/pages/nfc/TurnOnSBeam;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 17
    iget-object v1, p0, Lcom/samsung/helphub/pages/nfc/TurnOnSBeam$1;->this$0:Lcom/samsung/helphub/pages/nfc/TurnOnSBeam;

    # getter for: Lcom/samsung/helphub/pages/nfc/TurnOnSBeam;->isClicked:Z
    invoke-static {v1}, Lcom/samsung/helphub/pages/nfc/TurnOnSBeam;->access$000(Lcom/samsung/helphub/pages/nfc/TurnOnSBeam;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 30
    :goto_0
    return-void

    .line 19
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/pages/nfc/TurnOnSBeam$1;->this$0:Lcom/samsung/helphub/pages/nfc/TurnOnSBeam;

    # setter for: Lcom/samsung/helphub/pages/nfc/TurnOnSBeam;->isClicked:Z
    invoke-static {v1, v2}, Lcom/samsung/helphub/pages/nfc/TurnOnSBeam;->access$102(Lcom/samsung/helphub/pages/nfc/TurnOnSBeam;Z)Z

    .line 20
    const/4 v0, 0x0

    .line 21
    .local v0, "mIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 22
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "mIntent":Landroid/content/Intent;
    const-string v1, "android.settings.NFC_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 23
    .restart local v0    # "mIntent":Landroid/content/Intent;
    const-string v1, "fromHelp"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 24
    const-string v1, "fromHelp_Sbeam"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 29
    :goto_1
    iget-object v1, p0, Lcom/samsung/helphub/pages/nfc/TurnOnSBeam$1;->this$0:Lcom/samsung/helphub/pages/nfc/TurnOnSBeam;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/nfc/TurnOnSBeam;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 26
    :cond_1
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "mIntent":Landroid/content/Intent;
    const-string v1, "android.settings.SBEAM_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 27
    .restart local v0    # "mIntent":Landroid/content/Intent;
    const-string v1, "fromHelp"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1
.end method
