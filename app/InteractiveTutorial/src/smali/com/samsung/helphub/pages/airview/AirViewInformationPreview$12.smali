.class Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$12;
.super Ljava/lang/Object;
.source "AirViewInformationPreview.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->showEMailSettingDialog(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$12;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 210
    iget-object v1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$12;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->turnOnRelevantSetting()V
    invoke-static {v1}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->access$800(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)V

    .line 212
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.mms.help.AirViewTutorialReceiver.java"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 213
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.mms.help.AirViewMainActivity"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    iget-object v1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$12;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    invoke-virtual {v1}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 215
    return-void
.end method
