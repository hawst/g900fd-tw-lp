.class public Lcom/samsung/helphub/pages/LearnAboutNFC;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "LearnAboutNFC.java"


# instance fields
.field TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    .line 23
    const-string v0, "LearnAboutNFC"

    iput-object v0, p0, Lcom/samsung/helphub/pages/LearnAboutNFC;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 30
    .local v2, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/LearnAboutNFC;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 31
    const v3, 0x7f0d024b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 33
    .local v0, "mImageView_01":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/LearnAboutNFC;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "imageview_allshare_learn_nfc_01"

    invoke-static {v3, v0, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 34
    const v3, 0x7f0d0250

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 36
    .local v1, "mImageView_03":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/LearnAboutNFC;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "imageview_allshare_learn_nfc_03"

    invoke-static {v3, v1, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 38
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 39
    iget-object v3, p0, Lcom/samsung/helphub/pages/LearnAboutNFC;->TAG:Ljava/lang/String;

    const-string v4, "isDownloadable Tablet"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/LearnAboutNFC;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "tablet_help_case_1_01"

    invoke-static {v3, v0, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/LearnAboutNFC;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "tablet_help_case_1_03"

    invoke-static {v3, v1, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 49
    .end local v0    # "mImageView_01":Landroid/widget/ImageView;
    .end local v1    # "mImageView_03":Landroid/widget/ImageView;
    :cond_0
    :goto_0
    return-object v2

    .line 44
    .restart local v0    # "mImageView_01":Landroid/widget/ImageView;
    .restart local v1    # "mImageView_03":Landroid/widget/ImageView;
    :cond_1
    iget-object v3, p0, Lcom/samsung/helphub/pages/LearnAboutNFC;->TAG:Ljava/lang/String;

    const-string v4, "isDownloadable phone"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/LearnAboutNFC;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "wifi_direct_connect_help_06"

    invoke-static {v3, v0, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/LearnAboutNFC;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "wifi_direct_connect_help_05"

    invoke-static {v3, v1, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0
.end method
