.class Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;
.super Ljava/lang/Object;
.source "AirViewInformationPreview.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

.field final synthetic val$MagnificationResult:Z

.field final synthetic val$dialog_content:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    iput-boolean p2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->val$MagnificationResult:Z

    iput-object p3, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->val$dialog_content:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v4, 0x7f0a056b

    .line 51
    iget-object v1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    iget-boolean v1, v1, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->isClicked:Z

    if-eqz v1, :cond_0

    .line 65
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->isClicked:Z

    .line 54
    iget-object v1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->checkSettingStatus()Z
    invoke-static {v1}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->access$000(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    invoke-virtual {v1}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->val$MagnificationResult:Z

    if-nez v1, :cond_1

    .line 55
    iget-object v1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    iget-object v2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    const v3, 0x7f0a056e

    invoke-virtual {v2, v3}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getString(I)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->showSPlannerSettingDialog(ILjava/lang/String;)V
    invoke-static {v1, v4, v2}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->access$100(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;ILjava/lang/String;)V

    goto :goto_0

    .line 57
    :cond_1
    iget-object v1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    invoke-virtual {v1}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->val$MagnificationResult:Z

    if-eqz v1, :cond_3

    .line 58
    :cond_2
    iget-object v1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    iget-object v2, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->val$dialog_content:Ljava/lang/String;

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->showSPlannerSettingDialog(ILjava/lang/String;)V
    invoke-static {v1, v4, v2}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->access$100(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;ILjava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.intent.calendar.help"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 63
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$1;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
