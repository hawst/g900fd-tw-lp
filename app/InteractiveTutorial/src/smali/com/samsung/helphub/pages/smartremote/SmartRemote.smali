.class public Lcom/samsung/helphub/pages/smartremote/SmartRemote;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "SmartRemote.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method

.method private isYosemiteExist()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 33
    :try_start_0
    invoke-static {}, Landroid/util/GeneralUtil;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartremote/SmartRemote;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.yosemite.tab"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 42
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 36
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartremote/SmartRemote;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.yosemite.phone"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 37
    :catch_0
    move-exception v0

    .line 38
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 22
    .local v1, "result":Landroid/view/View;
    invoke-direct {p0}, Lcom/samsung/helphub/pages/smartremote/SmartRemote;->isYosemiteExist()Z

    move-result v3

    if-nez v3, :cond_0

    .line 23
    const v3, 0x7f0d0032

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 24
    .local v0, "helpHubTextView":Lcom/samsung/helphub/widget/HelpHubTextView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartremote/SmartRemote;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0145

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 25
    .local v2, "showDisplayString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartremote/SmartRemote;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0418

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    .end local v0    # "helpHubTextView":Lcom/samsung/helphub/widget/HelpHubTextView;
    .end local v2    # "showDisplayString":Ljava/lang/String;
    :cond_0
    return-object v1
.end method
