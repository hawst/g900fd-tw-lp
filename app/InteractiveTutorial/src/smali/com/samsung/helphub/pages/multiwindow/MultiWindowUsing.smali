.class public Lcom/samsung/helphub/pages/multiwindow/MultiWindowUsing;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "MultiWindowUsing.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 38
    new-instance v0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowUsing$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowUsing$1;-><init>(Lcom/samsung/helphub/pages/multiwindow/MultiWindowUsing;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowUsing;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowUsing;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 33
    .local v1, "result":Landroid/view/View;
    const v2, 0x7f0d03a2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 34
    .local v0, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowUsing;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "multi_window_using"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 35
    return-object v1
.end method
