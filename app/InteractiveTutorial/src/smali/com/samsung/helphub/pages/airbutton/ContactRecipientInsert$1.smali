.class Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert$1;
.super Ljava/lang/Object;
.source "ContactRecipientInsert.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;)V
    .locals 0

    .prologue
    .line 15
    iput-object p1, p0, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert$1;->this$0:Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 18
    iget-object v4, p0, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert$1;->this$0:Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->getActivity()Landroid/app/Activity;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 19
    iget-object v4, p0, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert$1;->this$0:Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;

    # getter for: Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->isClicked:Z
    invoke-static {v4}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->access$000(Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 34
    :cond_0
    :goto_0
    return-void

    .line 22
    :cond_1
    iget-object v4, p0, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert$1;->this$0:Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "air_view_master_onoff"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_3

    move v1, v2

    .line 23
    .local v1, "isAirViewEnabled":Z
    :goto_1
    iget-object v4, p0, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert$1;->this$0:Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "air_button_onoff"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_4

    move v0, v2

    .line 24
    .local v0, "isAirButtonEnabled":Z
    :goto_2
    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert$1;->this$0:Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;

    # setter for: Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->isClicked:Z
    invoke-static {v3, v2}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->access$102(Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;Z)Z

    .line 26
    if-eqz v1, :cond_2

    if-nez v0, :cond_5

    .line 27
    :cond_2
    iget-object v2, p0, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert$1;->this$0:Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;

    const v3, 0x7f0a0788

    const v4, 0x7f0a0789

    # invokes: Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->showEnableSettingDialog(II)V
    invoke-static {v2, v3, v4}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->access$200(Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;II)V

    goto :goto_0

    .end local v0    # "isAirButtonEnabled":Z
    .end local v1    # "isAirViewEnabled":Z
    :cond_3
    move v1, v3

    .line 22
    goto :goto_1

    .restart local v1    # "isAirViewEnabled":Z
    :cond_4
    move v0, v3

    .line 23
    goto :goto_2

    .line 29
    .restart local v0    # "isAirButtonEnabled":Z
    :cond_5
    iget-object v2, p0, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert$1;->this$0:Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/airbutton/ContactRecipientInsert;->startTutorial()V

    goto :goto_0
.end method
