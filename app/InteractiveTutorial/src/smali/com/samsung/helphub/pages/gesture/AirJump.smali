.class public Lcom/samsung/helphub/pages/gesture/AirJump;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "AirJump.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/gesture/AirJump;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/gesture/AirJump;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/gesture/AirJump;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/gesture/AirJump;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/gesture/AirJump;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/gesture/AirJump;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/gesture/AirJump;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/gesture/AirJump;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/gesture/AirJump;->showEnableSettingDialog(II)V

    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 65
    return-object p0
.end method

.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lcom/samsung/helphub/pages/gesture/AirJump$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/gesture/AirJump$1;-><init>(Lcom/samsung/helphub/pages/gesture/AirJump;)V

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/gesture/AirJump;->isClicked:Z

    .line 61
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 45
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/gesture/AirJump;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "air_motion_engine"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 46
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/gesture/AirJump;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "air_motion_scroll"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 47
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.gesture.AIR_MOTION_SETTINGS_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 48
    .local v0, "master_changed":Landroid/content/Intent;
    const-string v2, "isEnable"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 49
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/gesture/AirJump;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 50
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.gesture.AIR_SCROLL_SETTINGS_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 51
    .local v1, "setting_changed":Landroid/content/Intent;
    const-string v2, "isEnable"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 52
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/gesture/AirJump;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 54
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/gesture/AirJump;->startTutorial()V

    .line 55
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v8, 0x8

    .line 24
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v5

    .line 25
    .local v5, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/gesture/AirJump;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 26
    const v6, 0x7f0d021e

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 28
    .local v1, "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/gesture/AirJump;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-string v7, "tutorial_gesture_jump"

    invoke-static {v6, v1, v7}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 31
    .end local v1    # "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_0
    const v6, 0x7f0d0112

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 32
    .local v2, "mImageview":Landroid/widget/ImageView;
    const v6, 0x7f0d0081

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 33
    .local v0, "mButton":Landroid/widget/Button;
    const v6, 0x7f0d005d

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 34
    .local v3, "mTextview1":Landroid/widget/TextView;
    const v6, 0x7f0d0032

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 35
    .local v4, "mTextview2":Landroid/widget/TextView;
    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 36
    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 37
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 38
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 40
    return-object v5
.end method

.method protected startTutorial()V
    .locals 2

    .prologue
    .line 92
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.AirScrollTry"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 93
    .local v0, "mIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/gesture/AirJump;->startActivity(Landroid/content/Intent;)V

    .line 94
    return-void
.end method
