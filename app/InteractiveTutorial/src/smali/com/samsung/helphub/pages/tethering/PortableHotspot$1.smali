.class Lcom/samsung/helphub/pages/tethering/PortableHotspot$1;
.super Ljava/lang/Object;
.source "PortableHotspot.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/tethering/PortableHotspot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/tethering/PortableHotspot;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/tethering/PortableHotspot;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/helphub/pages/tethering/PortableHotspot$1;->this$0:Lcom/samsung/helphub/pages/tethering/PortableHotspot;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 41
    iget-object v1, p0, Lcom/samsung/helphub/pages/tethering/PortableHotspot$1;->this$0:Lcom/samsung/helphub/pages/tethering/PortableHotspot;

    # getter for: Lcom/samsung/helphub/pages/tethering/PortableHotspot;->isClicked:Z
    invoke-static {v1}, Lcom/samsung/helphub/pages/tethering/PortableHotspot;->access$000(Lcom/samsung/helphub/pages/tethering/PortableHotspot;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    :goto_0
    return-void

    .line 43
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/pages/tethering/PortableHotspot$1;->this$0:Lcom/samsung/helphub/pages/tethering/PortableHotspot;

    # setter for: Lcom/samsung/helphub/pages/tethering/PortableHotspot;->isClicked:Z
    invoke-static {v1, v3}, Lcom/samsung/helphub/pages/tethering/PortableHotspot;->access$102(Lcom/samsung/helphub/pages/tethering/PortableHotspot;Z)Z

    .line 44
    const-string v1, "PortableHotspot"

    const-string v2, "onClick() is called"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.ACTION.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 46
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.TetherSettings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 53
    const-string v1, "fromHelp"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 55
    iget-object v1, p0, Lcom/samsung/helphub/pages/tethering/PortableHotspot$1;->this$0:Lcom/samsung/helphub/pages/tethering/PortableHotspot;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/tethering/PortableHotspot;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
