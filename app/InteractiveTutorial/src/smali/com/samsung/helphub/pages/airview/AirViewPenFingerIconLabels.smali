.class public Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;
.super Lcom/samsung/helphub/pages/AirViewPenFingerPage;
.source "AirViewPenFingerIconLabels.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels$2;
    }
.end annotation


# instance fields
.field functionID:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;-><init>()V

    .line 27
    const v0, 0x7f0a01a5

    iput v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->functionID:I

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->isClicked:Z

    return p1
.end method


# virtual methods
.method protected checkPenModePopup(Ljava/lang/String;I)V
    .locals 7
    .param p1, "fuctionMode"    # Ljava/lang/String;
    .param p2, "functionStringID"    # I

    .prologue
    const v6, 0x7f0a01b8

    const/4 v5, 0x0

    const/4 v3, 0x1

    const v4, 0x7f0a01b0

    .line 53
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 55
    .local v0, "res":Landroid/content/res/Resources;
    iput-boolean v3, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->isPenMode:Z

    .line 56
    sget-object v1, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels$2;->$SwitchMap$com$samsung$helphub$pages$AirViewPenFingerPage$AirViewStatus:[I

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->checkAirviewStatus(Ljava/lang/String;)Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 78
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->startTutorial()V

    .line 81
    :goto_0
    return-void

    .line 58
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->checkTalkbackAndMagnification()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    const v1, 0x7f0a01b1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4, v1}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_0
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4, v1}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 67
    :pswitch_1
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4, v1}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 71
    :pswitch_2
    const v1, 0x7f0a01b5

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4, v1}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 75
    :pswitch_3
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->startTutorial()V

    goto :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 118
    return-object p0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->isClicked:Z

    .line 105
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->checkTalkbackAndMagnification()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->setTalkbackAndMagnification()V

    .line 97
    :cond_0
    const-string v0, "pen_hovering_icon_label"

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->setEnableSettings(Ljava/lang/String;)V

    .line 99
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->startTutorial()V

    .line 100
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 32
    .local v2, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 33
    const v3, 0x7f0d0232

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 35
    .local v1, "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "tutorial_air_view_icon_labels"

    invoke-static {v3, v1, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 38
    .end local v1    # "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_0
    const v3, 0x7f0d0081

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 39
    .local v0, "button":Landroid/widget/Button;
    new-instance v3, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels$1;

    invoke-direct {v3, p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels$1;-><init>(Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    return-object v2
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->onResume()V

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->isClicked:Z

    .line 88
    return-void
.end method

.method protected startTutorial()V
    .locals 3

    .prologue
    .line 109
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 110
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.gallery3d"

    const-string v2, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    const-string v1, "IsHelpMode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 112
    const-string v1, "HelpMode"

    const-string v2, "ICON_LABELS"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerIconLabels;->startActivity(Landroid/content/Intent;)V

    .line 114
    return-void
.end method
