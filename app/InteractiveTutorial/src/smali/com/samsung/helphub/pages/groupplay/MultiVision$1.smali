.class Lcom/samsung/helphub/pages/groupplay/MultiVision$1;
.super Ljava/lang/Object;
.source "MultiVision.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/groupplay/MultiVision;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/groupplay/MultiVision;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/groupplay/MultiVision;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/helphub/pages/groupplay/MultiVision$1;->this$0:Lcom/samsung/helphub/pages/groupplay/MultiVision;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 47
    const-string v1, "com.samsung.groupcast"

    .line 49
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/MultiVision$1;->this$0:Lcom/samsung/helphub/pages/groupplay/MultiVision;

    iget-object v3, p0, Lcom/samsung/helphub/pages/groupplay/MultiVision$1;->this$0:Lcom/samsung/helphub/pages/groupplay/MultiVision;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/groupplay/MultiVision;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/groupplay/MultiVision;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/groupplay/MultiVision;->access$000(Lcom/samsung/helphub/pages/groupplay/MultiVision;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 51
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/MultiVision$1;->this$0:Lcom/samsung/helphub/pages/groupplay/MultiVision;

    # getter for: Lcom/samsung/helphub/pages/groupplay/MultiVision;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/groupplay/MultiVision;->access$100(Lcom/samsung/helphub/pages/groupplay/MultiVision;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 65
    :goto_0
    return-void

    .line 54
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/MultiVision$1;->this$0:Lcom/samsung/helphub/pages/groupplay/MultiVision;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/groupplay/MultiVision;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/groupplay/MultiVision;->access$202(Lcom/samsung/helphub/pages/groupplay/MultiVision;Z)Z

    .line 56
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.samsung.groupcast.action.HELP"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 57
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "help_screen"

    const-string v3, "multi_vision"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    const/high16 v2, 0x20000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 59
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/MultiVision$1;->this$0:Lcom/samsung/helphub/pages/groupplay/MultiVision;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/groupplay/MultiVision;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 63
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/MultiVision$1;->this$0:Lcom/samsung/helphub/pages/groupplay/MultiVision;

    # invokes: Lcom/samsung/helphub/pages/groupplay/MultiVision;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/groupplay/MultiVision;->access$300(Lcom/samsung/helphub/pages/groupplay/MultiVision;Ljava/lang/String;)V

    goto :goto_0
.end method
