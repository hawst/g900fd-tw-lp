.class public Lcom/samsung/helphub/pages/actionmemo/LinkActionMemo;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "LinkActionMemo.java"


# static fields
.field public static final APP_PACKAGE_NAME_BAIDU_MAP:Ljava/lang/String; = "com.baidu.BaiduMap.samsung"

.field public static final APP_PACKAGE_NAME_CONTACT:Ljava/lang/String; = "com.android.contacts"

.field public static final APP_PACKAGE_NAME_EMAIL:Ljava/lang/String; = "com.android.email"

.field public static final APP_PACKAGE_NAME_GMAIL:Ljava/lang/String; = "com.google.android.gm"

.field public static final APP_PACKAGE_NAME_GOOGLESEARCH:Ljava/lang/String; = "com.google.android.googlequicksearchbox"

.field public static final APP_PACKAGE_NAME_MAP:Ljava/lang/String; = "com.google.android.apps.maps"

.field public static final APP_PACKAGE_NAME_MMS:Ljava/lang/String; = "com.android.mms"

.field public static final APP_PACKAGE_NAME_PHONE:Ljava/lang/String; = "com.android.phone"

.field public static final APP_PACKAGE_NAME_SBROWSER:Ljava/lang/String; = "com.sec.android.app.sbrowser"

.field public static final APP_PACKAGE_NAME_TASK:Ljava/lang/String; = "com.android.calendar"

.field private static final REMOVE_MAP:I = 0x100

.field private static final REMOVE_MESSAGE:I = 0x10

.field private static final REMOVE_MESSAGE_MAP:I = 0x110

.field private static final REMOVE_PHONE:I = 0x1

.field private static final REMOVE_PHONE_MAP:I = 0x101

.field private static final REMOVE_PHONE_MESSAGE:I = 0x11

.field private static final REMOVE_PHONE_MESSAGE_MAP:I = 0x111


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method

.method private static getappListItem(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 113
    const/4 v0, 0x0

    .line 115
    .local v0, "action_memo_type":I
    invoke-static {p0}, Lcom/samsung/helphub/pages/actionmemo/LinkActionMemo;->isAvailableGoogleMaps(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 116
    const-string v1, "CN"

    const-string v2, "ro.csc.countryiso_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 117
    invoke-static {p0}, Lcom/samsung/helphub/pages/actionmemo/LinkActionMemo;->isAvailableBaiduMaps(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/samsung/helphub/pages/actionmemo/LinkActionMemo;->isAvailableMaps(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 118
    or-int/lit16 v0, v0, 0x100

    .line 125
    :cond_0
    :goto_0
    invoke-static {p0}, Lcom/samsung/helphub/pages/actionmemo/LinkActionMemo;->isAvailablePhone(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 126
    or-int/lit8 v0, v0, 0x1

    .line 129
    :cond_1
    invoke-static {p0}, Lcom/samsung/helphub/pages/actionmemo/LinkActionMemo;->isAvailableMessage(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 130
    or-int/lit8 v0, v0, 0x10

    .line 133
    :cond_2
    return v0

    .line 121
    :cond_3
    or-int/lit16 v0, v0, 0x100

    goto :goto_0
.end method

.method private static isAvailableBaiduMaps(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 146
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.baidu.BaiduMap.samsung"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    const/4 v1, 0x1

    .line 149
    :goto_0
    return v1

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method private static isAvailableGoogleMaps(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 137
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.google.android.apps.maps"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    const/4 v1, 0x1

    .line 140
    :goto_0
    return v1

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method private static isAvailableMaps(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 154
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "geo:0,0?q="

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 155
    .local v0, "mapIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 156
    const/4 v1, 0x1

    .line 158
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isAvailableMessage(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 168
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.android.mms"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    const/4 v1, 0x1

    .line 171
    :goto_0
    return v1

    .line 170
    :catch_0
    move-exception v0

    .line 171
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method private static isAvailablePhone(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 163
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "config_voice_capable"

    const-string v3, "bool"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 16
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-super/range {p0 .. p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v14

    .line 51
    .local v14, "result":Landroid/view/View;
    const v15, 0x7f0d01ed

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 52
    .local v5, "mlayout_mphone":Landroid/widget/LinearLayout;
    const v15, 0x7f0d01ef

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 53
    .local v1, "mlayout_contact":Landroid/widget/LinearLayout;
    const v15, 0x7f0d01f1

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 54
    .local v4, "mlayout_message":Landroid/widget/LinearLayout;
    const v15, 0x7f0d01f3

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 55
    .local v2, "mlayout_email":Landroid/widget/LinearLayout;
    const v15, 0x7f0d01f5

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 56
    .local v0, "mlayout_browser":Landroid/widget/LinearLayout;
    const v15, 0x7f0d01f7

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 57
    .local v3, "mlayout_map":Landroid/widget/LinearLayout;
    const v15, 0x7f0d01f9

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 59
    .local v6, "mlayout_task":Landroid/widget/LinearLayout;
    const v15, 0x7f0d01ee

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 60
    .local v12, "mtextview_phone":Landroid/widget/TextView;
    const v15, 0x7f0d01f0

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 61
    .local v8, "mtextview_contact":Landroid/widget/TextView;
    const v15, 0x7f0d01f2

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 62
    .local v11, "mtextview_message":Landroid/widget/TextView;
    const v15, 0x7f0d01f4

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 63
    .local v9, "mtextview_email":Landroid/widget/TextView;
    const v15, 0x7f0d01f6

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 64
    .local v7, "mtextview_browser":Landroid/widget/TextView;
    const v15, 0x7f0d01f8

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 65
    .local v10, "mtextview_map":Landroid/widget/TextView;
    const v15, 0x7f0d01fa

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 67
    .local v13, "mtextview_task":Landroid/widget/TextView;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/helphub/pages/actionmemo/LinkActionMemo;->getActivity()Landroid/app/Activity;

    move-result-object v15

    invoke-static {v15}, Lcom/samsung/helphub/pages/actionmemo/LinkActionMemo;->getappListItem(Landroid/content/Context;)I

    move-result v15

    sparse-switch v15, :sswitch_data_0

    .line 109
    :goto_0
    return-object v14

    .line 69
    :sswitch_0
    const/16 v15, 0x8

    invoke-virtual {v5, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 70
    const/16 v15, 0x8

    invoke-virtual {v4, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 71
    const/16 v15, 0x8

    invoke-virtual {v3, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 72
    const v15, 0x7f0a0021

    invoke-virtual {v8, v15}, Landroid/widget/TextView;->setText(I)V

    .line 73
    const v15, 0x7f0a0022

    invoke-virtual {v9, v15}, Landroid/widget/TextView;->setText(I)V

    .line 74
    const v15, 0x7f0a0023

    invoke-virtual {v7, v15}, Landroid/widget/TextView;->setText(I)V

    .line 75
    const v15, 0x7f0a0024

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 78
    :sswitch_1
    const/16 v15, 0x8

    invoke-virtual {v4, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 79
    const/16 v15, 0x8

    invoke-virtual {v3, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 80
    const v15, 0x7f0a0023

    invoke-virtual {v9, v15}, Landroid/widget/TextView;->setText(I)V

    .line 81
    const v15, 0x7f0a0024

    invoke-virtual {v7, v15}, Landroid/widget/TextView;->setText(I)V

    .line 82
    const v15, 0x7f0a0025

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 85
    :sswitch_2
    const/16 v15, 0x8

    invoke-virtual {v5, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 86
    const/16 v15, 0x8

    invoke-virtual {v4, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 87
    const v15, 0x7f0a0021

    invoke-virtual {v8, v15}, Landroid/widget/TextView;->setText(I)V

    .line 88
    const v15, 0x7f0a0022

    invoke-virtual {v9, v15}, Landroid/widget/TextView;->setText(I)V

    .line 89
    const v15, 0x7f0a0023

    invoke-virtual {v7, v15}, Landroid/widget/TextView;->setText(I)V

    .line 90
    const v15, 0x7f0a0024

    invoke-virtual {v10, v15}, Landroid/widget/TextView;->setText(I)V

    .line 91
    const v15, 0x7f0a0025

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 94
    :sswitch_3
    const/16 v15, 0x8

    invoke-virtual {v4, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 95
    const v15, 0x7f0a0023

    invoke-virtual {v9, v15}, Landroid/widget/TextView;->setText(I)V

    .line 96
    const v15, 0x7f0a0024

    invoke-virtual {v7, v15}, Landroid/widget/TextView;->setText(I)V

    .line 97
    const v15, 0x7f0a0025

    invoke-virtual {v10, v15}, Landroid/widget/TextView;->setText(I)V

    .line 98
    const v15, 0x7f0a0026

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 101
    :sswitch_4
    const/16 v15, 0x8

    invoke-virtual {v3, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 102
    const v15, 0x7f0a0026

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 67
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_3
        0x11 -> :sswitch_2
        0x100 -> :sswitch_4
        0x110 -> :sswitch_1
        0x111 -> :sswitch_0
    .end sparse-switch
.end method
