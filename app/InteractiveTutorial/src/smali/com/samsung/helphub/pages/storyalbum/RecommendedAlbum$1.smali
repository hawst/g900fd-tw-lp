.class Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum$1;
.super Ljava/lang/Object;
.source "RecommendedAlbum.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;)V
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum$1;->this$0:Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 18
    const-string v2, "com.samsung.android.app.episodes"

    .line 20
    .local v2, "pkgName":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum$1;->this$0:Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;

    iget-object v4, p0, Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum$1;->this$0:Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;->getActivity()Landroid/app/Activity;

    move-result-object v4

    # invokes: Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v3, v4, v2}, Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;->access$000(Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 21
    iget-object v3, p0, Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum$1;->this$0:Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;

    # getter for: Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;->isClicked:Z
    invoke-static {v3}, Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;->access$100(Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 40
    :goto_0
    return-void

    .line 23
    :cond_0
    iget-object v3, p0, Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum$1;->this$0:Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;

    const/4 v4, 0x1

    # setter for: Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;->isClicked:Z
    invoke-static {v3, v4}, Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;->access$202(Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;Z)Z

    .line 26
    :try_start_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "com.samsung.android.app.episodes"

    const-string v5, "com.samsung.android.app.episodes.ui.timeline.TimelineActivity"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 29
    .local v1, "mIntent":Landroid/content/Intent;
    const-string v3, "HelpMode"

    const-string v4, "recommended"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 30
    iget-object v3, p0, Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum$1;->this$0:Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;

    invoke-virtual {v3, v1}, Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 32
    .end local v1    # "mIntent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 33
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "Help_StoryAlbum"

    const-string v4, "call com.samsung.android.app.episodes.ui.timeline.TimelineActivity. recommended"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 38
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_1
    iget-object v3, p0, Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum$1;->this$0:Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;

    # invokes: Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v3, v2}, Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;->access$300(Lcom/samsung/helphub/pages/storyalbum/RecommendedAlbum;Ljava/lang/String;)V

    goto :goto_0
.end method
