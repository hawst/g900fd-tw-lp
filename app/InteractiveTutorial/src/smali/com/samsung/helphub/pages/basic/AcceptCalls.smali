.class public Lcom/samsung/helphub/pages/basic/AcceptCalls;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "AcceptCalls.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "AcceptCalls"


# instance fields
.field protected isClicked:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/basic/AcceptCalls;->isClicked:Z

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/high16 v7, 0x24000000

    const/4 v6, 0x1

    .line 59
    const-string v3, "AcceptCalls"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onclick v "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 62
    :sswitch_0
    const-string v3, "AcceptCalls"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onclick button_start_try_it isClicked : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/helphub/pages/basic/AcceptCalls;->isClicked:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iget-boolean v3, p0, Lcom/samsung/helphub/pages/basic/AcceptCalls;->isClicked:Z

    if-nez v3, :cond_0

    .line 65
    iput-boolean v6, p0, Lcom/samsung/helphub/pages/basic/AcceptCalls;->isClicked:Z

    .line 66
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 67
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v3, "com.android.phone"

    const-string v4, "com.android.phone.IncomingCallTutorialIncallScreen"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    invoke-virtual {v0, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 69
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/basic/AcceptCalls;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 73
    .end local v0    # "mIntent":Landroid/content/Intent;
    :sswitch_1
    const-string v3, "AcceptCalls"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onclick phone_button_start_try_it_2 isClicked : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/helphub/pages/basic/AcceptCalls;->isClicked:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-boolean v3, p0, Lcom/samsung/helphub/pages/basic/AcceptCalls;->isClicked:Z

    if-nez v3, :cond_0

    .line 76
    iput-boolean v6, p0, Lcom/samsung/helphub/pages/basic/AcceptCalls;->isClicked:Z

    .line 77
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 78
    .local v1, "mIntent1":Landroid/content/Intent;
    const-string v3, "com.android.phone"

    const-string v4, "com.android.phone.RejectwithMessageTutorialIncallScreen"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    invoke-virtual {v1, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 80
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/pages/basic/AcceptCalls;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 84
    .end local v1    # "mIntent1":Landroid/content/Intent;
    :sswitch_2
    const-string v3, "AcceptCalls"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onclick phone_button_start_try_it_3 isClicked : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/helphub/pages/basic/AcceptCalls;->isClicked:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    iget-boolean v3, p0, Lcom/samsung/helphub/pages/basic/AcceptCalls;->isClicked:Z

    if-nez v3, :cond_0

    .line 87
    iput-boolean v6, p0, Lcom/samsung/helphub/pages/basic/AcceptCalls;->isClicked:Z

    .line 88
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 89
    .local v2, "mIntent2":Landroid/content/Intent;
    const-string v3, "com.android.phone"

    const-string v4, "com.android.phone.IncomingCallWhileUsingApp"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    invoke-virtual {v2, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 91
    invoke-virtual {p0, v2}, Lcom/samsung/helphub/pages/basic/AcceptCalls;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 60
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0d0081 -> :sswitch_0
        0x7f0d01da -> :sswitch_1
        0x7f0d01db -> :sswitch_2
    .end sparse-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    .line 26
    const-string v4, "AcceptCalls"

    const-string v5, "onCreateView"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v3

    .line 29
    .local v3, "result":Landroid/view/View;
    const v4, 0x7f0d0081

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 30
    .local v0, "button":Landroid/view/View;
    const/4 v1, 0x0

    .line 31
    .local v1, "button2":Landroid/view/View;
    const/4 v2, 0x0

    .line 32
    .local v2, "button3":Landroid/view/View;
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v4

    if-eq v4, v6, :cond_0

    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isAmericano()Z

    move-result v4

    if-ne v4, v6, :cond_2

    .line 33
    :cond_0
    const v4, 0x7f0d01da

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 34
    const v4, 0x7f0d01db

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 35
    if-eqz v1, :cond_1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    :cond_2
    const-string v4, "AcceptCalls"

    const-string v5, "onCreateView2"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    if-eqz v0, :cond_3

    .line 41
    const-string v4, "AcceptCalls"

    const-string v5, "button1"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    :cond_3
    return-object v3
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->onResume()V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/basic/AcceptCalls;->isClicked:Z

    .line 52
    return-void
.end method
