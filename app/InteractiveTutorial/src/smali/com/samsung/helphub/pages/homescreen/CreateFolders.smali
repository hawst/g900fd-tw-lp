.class public Lcom/samsung/helphub/pages/homescreen/CreateFolders;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "CreateFolders.java"

# interfaces
.implements Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;


# static fields
.field private static final NOTIFICATION_ID:I = 0x64


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 43
    new-instance v0, Lcom/samsung/helphub/pages/homescreen/CreateFolders$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/homescreen/CreateFolders$1;-><init>(Lcom/samsung/helphub/pages/homescreen/CreateFolders;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/homescreen/CreateFolders;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/homescreen/CreateFolders;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/homescreen/CreateFolders;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/homescreen/CreateFolders;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/homescreen/CreateFolders;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/homescreen/CreateFolders;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/homescreen/CreateFolders;->isClicked:Z

    return p1
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/helphub/pages/homescreen/CreateFolders;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 36
    .local v1, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/homescreen/CreateFolders;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    const v2, 0x7f0d001f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 38
    .local v0, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/homescreen/CreateFolders;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "adding_applications"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 40
    .end local v0    # "mImageView":Landroid/widget/ImageView;
    :cond_0
    return-object v1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 104
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/homescreen/CreateFolders;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExternalHelpActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/homescreen/CreateFolders;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/ExternalHelpActivity;

    sget-boolean v0, Lcom/samsung/helphub/ExternalHelpActivity;->sIsReturnFromLauncher:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/homescreen/CreateFolders;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/ExternalHelpActivity;

    sget-object v0, Lcom/samsung/helphub/ExternalHelpActivity;->sItemOpenReq:Ljava/lang/String;

    const-string v1, "homescreen_create_folder"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/homescreen/CreateFolders;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/ExternalHelpActivity;

    invoke-virtual {v0}, Lcom/samsung/helphub/ExternalHelpActivity;->startHomeScreenSection()V

    .line 108
    const/4 v0, 0x1

    .line 110
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
