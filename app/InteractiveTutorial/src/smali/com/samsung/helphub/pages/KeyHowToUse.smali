.class public Lcom/samsung/helphub/pages/KeyHowToUse;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "KeyHowToUse.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v6

    .line 22
    .local v6, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/KeyHowToUse;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 23
    const v7, 0x7f0d035a

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 24
    .local v2, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/KeyHowToUse;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const-string v8, "help_expanded_hardkey"

    invoke-static {v7, v2, v8}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 27
    .end local v2    # "mImageView":Landroid/widget/ImageView;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/KeyHowToUse;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v7

    if-nez v7, :cond_5

    .line 28
    const v7, 0x7f0d0034

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 29
    .local v1, "help_list_item_3":Landroid/widget/TextView;
    if-eqz v1, :cond_1

    .line 30
    const v7, 0x7f0a007f

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(I)V

    .line 32
    :cond_1
    const v7, 0x7f0d035d

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 33
    .local v3, "physical_key_child_text_3":Landroid/widget/TextView;
    if-eqz v3, :cond_3

    .line 34
    const-string v7, "VZW"

    const-string v8, "ro.csc.sales_code"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "VZW"

    const-string v8, "ril.sales_code"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 35
    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const v8, 0x7f0a0081

    invoke-virtual {p0, v8}, Lcom/samsung/helphub/pages/KeyHowToUse;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f0a0082

    invoke-virtual {p0, v8}, Lcom/samsung/helphub/pages/KeyHowToUse;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "description":Ljava/lang/String;
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    .end local v0    # "description":Ljava/lang/String;
    :cond_3
    :goto_0
    const v7, 0x7f0d035e

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 42
    .local v4, "physical_key_child_text_3_search":Landroid/widget/TextView;
    if-eqz v4, :cond_4

    .line 43
    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 45
    :cond_4
    const v7, 0x7f0d035f

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 46
    .local v5, "physical_key_child_text_4":Landroid/widget/TextView;
    if-eqz v5, :cond_5

    .line 47
    const v7, 0x7f0a007a

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(I)V

    .line 50
    .end local v1    # "help_list_item_3":Landroid/widget/TextView;
    .end local v3    # "physical_key_child_text_3":Landroid/widget/TextView;
    .end local v4    # "physical_key_child_text_3_search":Landroid/widget/TextView;
    .end local v5    # "physical_key_child_text_4":Landroid/widget/TextView;
    :cond_5
    return-object v6

    .line 38
    .restart local v1    # "help_list_item_3":Landroid/widget/TextView;
    .restart local v3    # "physical_key_child_text_3":Landroid/widget/TextView;
    :cond_6
    const v7, 0x7f0a0080

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method
