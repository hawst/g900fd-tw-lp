.class Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand$1;
.super Ljava/lang/Object;
.source "SetWakeUpCommand.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;)V
    .locals 0

    .prologue
    .line 16
    iput-object p1, p0, Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand$1;->this$0:Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 20
    const-string v1, "com.vlingo.midas"

    .line 21
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand$1;->this$0:Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;

    iget-object v3, p0, Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand$1;->this$0:Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;->access$000(Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 23
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand$1;->this$0:Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;

    # getter for: Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;->access$100(Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 35
    :goto_0
    return-void

    .line 26
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand$1;->this$0:Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;->access$202(Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;Z)Z

    .line 29
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 30
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "android.intent.action.VOICE_SETTING_SET_WAKEUP_COMMAND_HELP"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand$1;->this$0:Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 33
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand$1;->this$0:Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;

    # invokes: Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;->access$300(Lcom/samsung/helphub/pages/svoice/SetWakeUpCommand;Ljava/lang/String;)V

    goto :goto_0
.end method
