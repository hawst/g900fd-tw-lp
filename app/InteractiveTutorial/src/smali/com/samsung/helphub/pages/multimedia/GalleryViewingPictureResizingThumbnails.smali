.class public Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "GalleryViewingPictureResizingThumbnails.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 19
    new-instance v0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails$1;-><init>(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->isClicked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->isClicked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v4, 0x7f0d02df

    .line 48
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 49
    .local v1, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 50
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 51
    .local v0, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "gallery_help_resizing_thumbnails_01"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 56
    .end local v0    # "mImageView":Landroid/widget/ImageView;
    :cond_0
    :goto_0
    return-object v1

    .line 52
    :cond_1
    const-string v2, "ro.product.name"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "m2"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 53
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 54
    .restart local v0    # "mImageView":Landroid/widget/ImageView;
    const v2, 0x7f020092

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
