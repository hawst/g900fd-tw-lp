.class Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid$1;
.super Ljava/lang/Object;
.source "EditWhatYouSaid.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid$1;->this$0:Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 33
    const-string v1, "com.vlingo.midas"

    .line 35
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid$1;->this$0:Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;

    iget-object v3, p0, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid$1;->this$0:Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->access$000(Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 36
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid$1;->this$0:Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;

    # getter for: Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->access$100(Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid$1;->this$0:Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->access$202(Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;Z)Z

    .line 40
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EditWhatYouSaid Model name is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ro.product.name"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->LogD(Ljava/lang/String;)V

    .line 41
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 42
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "com.vlingo.midas"

    const-string v3, "com.vlingo.midas.gui.EditWhatYouSaidHelpActivity"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid$1;->this$0:Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 46
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid$1;->this$0:Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;

    # invokes: Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;->access$300(Lcom/samsung/helphub/pages/svoice/EditWhatYouSaid;Ljava/lang/String;)V

    goto :goto_0
.end method
