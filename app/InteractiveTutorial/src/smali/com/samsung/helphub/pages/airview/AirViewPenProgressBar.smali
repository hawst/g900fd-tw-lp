.class public Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;
.super Lcom/samsung/helphub/pages/AirViewPenPage;
.source "AirViewPenProgressBar.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# instance fields
.field final functionID:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;-><init>()V

    .line 24
    const v0, 0x7f0a01a3

    iput v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->functionID:I

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 22
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->checkPenModePopup(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 104
    return-object p0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->isClicked:Z

    .line 81
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->checkTalkbackAndMagnification()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->setTalkbackAndMagnification()V

    .line 73
    :cond_0
    const-string v0, "pen_hovering_progress_preview"

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->setEnableSettings(Ljava/lang/String;)V

    .line 75
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->startTutorial()V

    .line 76
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/AirViewPenPage;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 30
    .local v2, "result":Landroid/view/View;
    const v4, 0x7f0d0032

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 31
    .local v3, "text":Landroid/widget/TextView;
    const v4, 0x7f0a01a9

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 33
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 34
    const v4, 0x7f0d0238

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 36
    .local v1, "mImageViewer_pen":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "tutorial_air_view_progress_preview_pen"

    invoke-static {v4, v1, v5}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 41
    .end local v1    # "mImageViewer_pen":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_0
    const v4, 0x7f0d0081

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 42
    .local v0, "button":Landroid/widget/Button;
    new-instance v4, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar$1;

    invoke-direct {v4, p0}, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar$1;-><init>(Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    return-object v2
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Lcom/samsung/helphub/pages/AirViewPenPage;->onResume()V

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->isClicked:Z

    .line 64
    return-void
.end method

.method protected startTutorial()V
    .locals 6

    .prologue
    .line 85
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.START_HELP_VIDEO_PROGRESS_BAR_PREVIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 86
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 87
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "video_help"

    const-string v4, "raw"

    const-string v5, "com.samsung.helpplugin"

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/helphub/HelpHubCommon;->getResourceId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 88
    .local v1, "resId":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 89
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "android.resource://com.samsung.helpplugin/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "video/mp4"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    const-string v2, "type"

    const-string v3, "pen"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->startActivity(Landroid/content/Intent;)V

    .line 100
    .end local v1    # "resId":I
    :goto_0
    return-void

    .line 93
    .restart local v1    # "resId":I
    :cond_0
    const-string v2, "The Video resource not downloaded yet"

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->LogD(Ljava/lang/String;)V

    goto :goto_0

    .line 96
    .end local v1    # "resId":I
    :cond_1
    const-string v2, "file:///system/media/video/video_help.mp4"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "video/mp4"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    const-string v2, "type"

    const-string v3, "pen"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenProgressBar;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
