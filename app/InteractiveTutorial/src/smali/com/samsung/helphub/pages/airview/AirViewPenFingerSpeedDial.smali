.class public Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;
.super Lcom/samsung/helphub/pages/AirViewPenFingerPage;
.source "AirViewPenFingerSpeedDial.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# instance fields
.field final functionID:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;-><init>()V

    .line 24
    const v0, 0x7f0a01a4

    iput v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;->functionID:I

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 22
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;->checkPenFingerModePopup(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 94
    return-object p0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;->isClicked:Z

    .line 77
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;->checkTalkbackAndMagnification()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;->setTalkbackAndMagnification()V

    .line 67
    :cond_0
    const-string v0, "pen_hovering_speed_dial_preview"

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;->setEnableSettings(Ljava/lang/String;)V

    .line 68
    const-string v0, "finger_air_view_speed_dial_tip"

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;->setEnableSettings(Ljava/lang/String;)V

    .line 71
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;->startTutorial()V

    .line 72
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 29
    .local v2, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 30
    const v3, 0x7f0d0239

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 32
    .local v1, "mImageViewer_pen":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "tutorial_air_view_speed_dial_pen"

    invoke-static {v3, v1, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 37
    .end local v1    # "mImageViewer_pen":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_0
    const v3, 0x7f0d0081

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 38
    .local v0, "button":Landroid/widget/Button;
    new-instance v3, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial$1;

    invoke-direct {v3, p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial$1;-><init>(Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    return-object v2
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->onResume()V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;->isClicked:Z

    .line 58
    return-void
.end method

.method protected startTutorial()V
    .locals 3

    .prologue
    .line 86
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.contacts.action.DialerHelpActivity"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 88
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v1, "DialerGuideMode"

    const-string v2, "SPEED_DIAL_TIP_TUTORIAL"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerSpeedDial;->startActivity(Landroid/content/Intent;)V

    .line 90
    return-void
.end method
