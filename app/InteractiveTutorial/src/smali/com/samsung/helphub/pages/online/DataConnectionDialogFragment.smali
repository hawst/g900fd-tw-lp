.class public Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;
.super Landroid/app/DialogFragment;
.source "DataConnectionDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;)Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;->mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 82
    const/4 v1, 0x0

    .line 83
    .local v1, "rememberSelection":Z
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 84
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_2

    .line 85
    sget-object v3, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->NONE:Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;

    .line 87
    .local v3, "selection":Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;
    const v4, 0x7f0d02ba

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 89
    .local v2, "rememberSelectionBox":Landroid/widget/CheckBox;
    if-eqz v2, :cond_0

    .line 90
    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 93
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 117
    sget-object v3, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->NONE:Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;

    .line 121
    :cond_1
    :goto_0
    invoke-virtual {v0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/samsung/helphub/utility/PreferenceHelper;->setDataPreference(Landroid/content/Context;Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;)V

    .line 123
    .end local v2    # "rememberSelectionBox":Landroid/widget/CheckBox;
    .end local v3    # "selection":Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;
    :cond_2
    return-void

    .line 95
    .restart local v2    # "rememberSelectionBox":Landroid/widget/CheckBox;
    .restart local v3    # "selection":Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;
    :pswitch_0
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;->mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    invoke-interface {v4}, Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;->onClickMobileConnection()V

    .line 97
    if-eqz v1, :cond_1

    .line 98
    sget-object v3, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->MOBILE_DATA:Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;

    goto :goto_0

    .line 103
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;->mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    invoke-interface {v4}, Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;->onClickWiFi()V

    .line 105
    if-eqz v1, :cond_1

    .line 106
    sget-object v3, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->WIFI:Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;

    goto :goto_0

    .line 111
    :pswitch_2
    iget-object v4, p0, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;->mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    invoke-interface {v4}, Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;->onClickCancel()V

    .line 112
    sget-object v3, Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;->NONE:Lcom/samsung/helphub/utility/PreferenceHelper$DataSelectionPreferences;

    .line 113
    goto :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 26
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const/4 v0, 0x1

    const v1, 0x7f0e003a

    invoke-virtual {p0, v0, v1}, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;->setStyle(II)V

    .line 28
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 35
    const v2, 0x7f0a0593

    .line 40
    .local v2, "setUpWiFiButton":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f040051

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 42
    .local v1, "dialogLayout":Landroid/view/View;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0a0587

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0a0596

    invoke-virtual {v3, v4, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0a0597

    invoke-virtual {v3, v4, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment$1;

    invoke-direct {v4, p0}, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment$1;-><init>(Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 65
    .local v0, "dialog":Landroid/app/Dialog;
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;->setCancelable(Z)V

    .line 67
    return-object v0

    .line 37
    .end local v0    # "dialog":Landroid/app/Dialog;
    .end local v1    # "dialogLayout":Landroid/view/View;
    .end local v2    # "setUpWiFiButton":I
    :cond_0
    const v2, 0x7f0a0594

    .restart local v2    # "setUpWiFiButton":I
    goto :goto_0
.end method

.method public setDialogListener(Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/samsung/helphub/pages/online/DataConnectionDialogFragment;->mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    .line 77
    return-void
.end method
