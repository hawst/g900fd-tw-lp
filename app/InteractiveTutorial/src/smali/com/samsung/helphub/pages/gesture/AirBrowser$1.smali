.class Lcom/samsung/helphub/pages/gesture/AirBrowser$1;
.super Ljava/lang/Object;
.source "AirBrowser.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/gesture/AirBrowser;->getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/gesture/AirBrowser;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/gesture/AirBrowser;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/helphub/pages/gesture/AirBrowser$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirBrowser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 62
    const-string v1, "com.sec.android.gallery3d"

    .line 64
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/helphub/pages/gesture/AirBrowser$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirBrowser;

    iget-object v6, p0, Lcom/samsung/helphub/pages/gesture/AirBrowser$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirBrowser;

    invoke-virtual {v6}, Lcom/samsung/helphub/pages/gesture/AirBrowser;->getActivity()Landroid/app/Activity;

    move-result-object v6

    # invokes: Lcom/samsung/helphub/pages/gesture/AirBrowser;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v5, v6, v1}, Lcom/samsung/helphub/pages/gesture/AirBrowser;->access$000(Lcom/samsung/helphub/pages/gesture/AirBrowser;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 65
    iget-object v5, p0, Lcom/samsung/helphub/pages/gesture/AirBrowser$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirBrowser;

    invoke-virtual {v5}, Lcom/samsung/helphub/pages/gesture/AirBrowser;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "air_motion_engine"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_0

    move v0, v3

    .line 66
    .local v0, "masterStatus":Z
    :goto_0
    iget-object v5, p0, Lcom/samsung/helphub/pages/gesture/AirBrowser$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirBrowser;

    invoke-virtual {v5}, Lcom/samsung/helphub/pages/gesture/AirBrowser;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "air_motion_turn"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    if-eqz v5, :cond_1

    move v2, v3

    .line 67
    .local v2, "settingStatus":Z
    :goto_1
    iget-object v4, p0, Lcom/samsung/helphub/pages/gesture/AirBrowser$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirBrowser;

    # getter for: Lcom/samsung/helphub/pages/gesture/AirBrowser;->isClicked:Z
    invoke-static {v4}, Lcom/samsung/helphub/pages/gesture/AirBrowser;->access$100(Lcom/samsung/helphub/pages/gesture/AirBrowser;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 81
    .end local v0    # "masterStatus":Z
    .end local v2    # "settingStatus":Z
    :goto_2
    return-void

    :cond_0
    move v0, v4

    .line 65
    goto :goto_0

    .restart local v0    # "masterStatus":Z
    :cond_1
    move v2, v4

    .line 66
    goto :goto_1

    .line 69
    .restart local v2    # "settingStatus":Z
    :cond_2
    iget-object v4, p0, Lcom/samsung/helphub/pages/gesture/AirBrowser$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirBrowser;

    # setter for: Lcom/samsung/helphub/pages/gesture/AirBrowser;->isClicked:Z
    invoke-static {v4, v3}, Lcom/samsung/helphub/pages/gesture/AirBrowser;->access$202(Lcom/samsung/helphub/pages/gesture/AirBrowser;Z)Z

    .line 70
    if-eqz v0, :cond_3

    if-nez v2, :cond_4

    .line 71
    :cond_3
    iget-object v3, p0, Lcom/samsung/helphub/pages/gesture/AirBrowser$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirBrowser;

    const v4, 0x7f0a0564

    const v5, 0x7f0a0567

    # invokes: Lcom/samsung/helphub/pages/gesture/AirBrowser;->showEnableSettingDialog(II)V
    invoke-static {v3, v4, v5}, Lcom/samsung/helphub/pages/gesture/AirBrowser;->access$300(Lcom/samsung/helphub/pages/gesture/AirBrowser;II)V

    goto :goto_2

    .line 75
    :cond_4
    iget-object v3, p0, Lcom/samsung/helphub/pages/gesture/AirBrowser$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirBrowser;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/gesture/AirBrowser;->startTutorial()V

    goto :goto_2

    .line 79
    .end local v0    # "masterStatus":Z
    .end local v2    # "settingStatus":Z
    :cond_5
    iget-object v3, p0, Lcom/samsung/helphub/pages/gesture/AirBrowser$1;->this$0:Lcom/samsung/helphub/pages/gesture/AirBrowser;

    # invokes: Lcom/samsung/helphub/pages/gesture/AirBrowser;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v3, v1}, Lcom/samsung/helphub/pages/gesture/AirBrowser;->access$400(Lcom/samsung/helphub/pages/gesture/AirBrowser;Ljava/lang/String;)V

    goto :goto_2
.end method
