.class public Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "CreateAlbumFromGallery.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 14
    new-instance v0, Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery$1;-><init>(Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;

    .prologue
    .line 12
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;->isClicked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;->isClicked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/helphub/pages/storyalbum/CreateAlbumFromGallery;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method
