.class public Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "HowToUseSConnect.java"


# instance fields
.field private mAnimateImage:Landroid/widget/ImageView;

.field private mAnimation:Landroid/graphics/drawable/AnimationDrawable;

.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;->mAnimateImage:Landroid/widget/ImageView;

    .line 18
    iput-object v0, p0, Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;->mAnimation:Landroid/graphics/drawable/AnimationDrawable;

    .line 43
    new-instance v0, Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect$1;-><init>(Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;
    .param p1, "x1"    # Z

    .prologue
    .line 16
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;->isClicked:Z

    return p1
.end method


# virtual methods
.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 25
    .local v0, "result":Landroid/view/View;
    const v1, 0x7f0d03c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;->mAnimateImage:Landroid/widget/ImageView;

    .line 26
    iget-object v1, p0, Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;->mAnimateImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v1, p0, Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;->mAnimation:Landroid/graphics/drawable/AnimationDrawable;

    .line 28
    return-object v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onPause()V

    .line 40
    iget-object v0, p0, Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;->mAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 41
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 33
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onResume()V

    .line 34
    iget-object v0, p0, Lcom/samsung/helphub/pages/sconnect/HowToUseSConnect;->mAnimation:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 35
    return-void
.end method
