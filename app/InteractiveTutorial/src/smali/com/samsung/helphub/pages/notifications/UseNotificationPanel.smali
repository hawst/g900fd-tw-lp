.class public Lcom/samsung/helphub/pages/notifications/UseNotificationPanel;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "UseNotificationPanel.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v3

    .line 19
    .local v3, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/notifications/UseNotificationPanel;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 20
    const v4, 0x7f0d03aa

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 21
    .local v0, "mImageView_01":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/notifications/UseNotificationPanel;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "notification_use_the_notification_panel_01"

    invoke-static {v4, v0, v5}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 22
    const v4, 0x7f0d03ac

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 23
    .local v1, "mImageView_02":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/notifications/UseNotificationPanel;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "notification_use_the_notification_panel_02"

    invoke-static {v4, v1, v5}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 24
    const v4, 0x7f0d03a9

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 25
    .local v2, "mImageView_03":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/notifications/UseNotificationPanel;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "notification_use_the_notification_panel_03"

    invoke-static {v4, v2, v5}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 27
    .end local v0    # "mImageView_01":Landroid/widget/ImageView;
    .end local v1    # "mImageView_02":Landroid/widget/ImageView;
    .end local v2    # "mImageView_03":Landroid/widget/ImageView;
    :cond_0
    return-object v3
.end method
