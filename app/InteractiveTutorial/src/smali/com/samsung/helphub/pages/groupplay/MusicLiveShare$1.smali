.class Lcom/samsung/helphub/pages/groupplay/MusicLiveShare$1;
.super Ljava/lang/Object;
.source "MusicLiveShare.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/samsung/helphub/pages/groupplay/MusicLiveShare$1;->this$0:Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 16
    const-string v1, "com.samsung.groupcast"

    .line 18
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/MusicLiveShare$1;->this$0:Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;

    iget-object v3, p0, Lcom/samsung/helphub/pages/groupplay/MusicLiveShare$1;->this$0:Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;->access$000(Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 19
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/MusicLiveShare$1;->this$0:Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;

    # getter for: Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;->access$100(Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    :goto_0
    return-void

    .line 21
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/MusicLiveShare$1;->this$0:Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;->access$202(Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;Z)Z

    .line 22
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.samsung.groupcast.action.HELP"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 23
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "help_screen"

    const-string v3, "music_live_share"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25
    const/high16 v2, 0x20000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 26
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/MusicLiveShare$1;->this$0:Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 29
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/groupplay/MusicLiveShare$1;->this$0:Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;

    # invokes: Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;->access$300(Lcom/samsung/helphub/pages/groupplay/MusicLiveShare;Ljava/lang/String;)V

    goto :goto_0
.end method
