.class public Lcom/samsung/helphub/pages/groupplay/SharingContents;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "SharingContents.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 21
    new-instance v0, Lcom/samsung/helphub/pages/groupplay/SharingContents$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/groupplay/SharingContents$1;-><init>(Lcom/samsung/helphub/pages/groupplay/SharingContents;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/groupplay/SharingContents;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/groupplay/SharingContents;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/groupplay/SharingContents;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/groupplay/SharingContents;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/groupplay/SharingContents;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/groupplay/SharingContents;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/groupplay/SharingContents;->isClicked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/helphub/pages/groupplay/SharingContents;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/groupplay/SharingContents;
    .param p1, "x1"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/groupplay/SharingContents;->isClicked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/groupplay/SharingContents;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/groupplay/SharingContents;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/groupplay/SharingContents;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/helphub/pages/groupplay/SharingContents;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v5, 0x8

    .line 49
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v3

    .line 51
    .local v3, "view":Landroid/view/View;
    if-eqz v3, :cond_1

    .line 52
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/groupplay/SharingContents;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->isMusicLiveShareHeaderHidden(Landroid/content/Context;)Z

    move-result v0

    .line 53
    .local v0, "musicHidden":Z
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/groupplay/SharingContents;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->isMultiVisionHeaderHidden(Landroid/content/Context;)Z

    move-result v2

    .line 55
    .local v2, "videoHidden":Z
    if-eqz v0, :cond_0

    .line 57
    const v4, 0x7f0d0032

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 58
    .local v1, "textView":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 59
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 63
    .end local v1    # "textView":Landroid/widget/TextView;
    :cond_0
    if-eqz v2, :cond_1

    .line 65
    const v4, 0x7f0d0034

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 66
    .restart local v1    # "textView":Landroid/widget/TextView;
    if-eqz v1, :cond_1

    .line 67
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 71
    .end local v0    # "musicHidden":Z
    .end local v1    # "textView":Landroid/widget/TextView;
    .end local v2    # "videoHidden":Z
    :cond_1
    return-object v3
.end method
