.class Lcom/samsung/helphub/pages/snote/SnotePanningPen$1;
.super Ljava/lang/Object;
.source "SnotePanningPen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/snote/SnotePanningPen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/snote/SnotePanningPen;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/snote/SnotePanningPen;)V
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Lcom/samsung/helphub/pages/snote/SnotePanningPen$1;->this$0:Lcom/samsung/helphub/pages/snote/SnotePanningPen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 15
    const-string v1, "com.samsung.android.snote"

    .line 16
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnotePanningPen$1;->this$0:Lcom/samsung/helphub/pages/snote/SnotePanningPen;

    iget-object v3, p0, Lcom/samsung/helphub/pages/snote/SnotePanningPen$1;->this$0:Lcom/samsung/helphub/pages/snote/SnotePanningPen;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/snote/SnotePanningPen;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/snote/SnotePanningPen;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/snote/SnotePanningPen;->access$000(Lcom/samsung/helphub/pages/snote/SnotePanningPen;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 17
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnotePanningPen$1;->this$0:Lcom/samsung/helphub/pages/snote/SnotePanningPen;

    # getter for: Lcom/samsung/helphub/pages/snote/SnotePanningPen;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/snote/SnotePanningPen;->access$100(Lcom/samsung/helphub/pages/snote/SnotePanningPen;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 32
    :goto_0
    return-void

    .line 21
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnotePanningPen$1;->this$0:Lcom/samsung/helphub/pages/snote/SnotePanningPen;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/snote/SnotePanningPen;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/snote/SnotePanningPen;->access$202(Lcom/samsung/helphub/pages/snote/SnotePanningPen;Z)Z

    .line 23
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.samsung.android.snote"

    const-string v4, "com.samsung.android.snote.helper.HelperActivity"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 25
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "snote:section"

    const-string v3, "pan_pen"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 27
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnotePanningPen$1;->this$0:Lcom/samsung/helphub/pages/snote/SnotePanningPen;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/snote/SnotePanningPen;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 30
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnotePanningPen$1;->this$0:Lcom/samsung/helphub/pages/snote/SnotePanningPen;

    # invokes: Lcom/samsung/helphub/pages/snote/SnotePanningPen;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/snote/SnotePanningPen;->access$300(Lcom/samsung/helphub/pages/snote/SnotePanningPen;Ljava/lang/String;)V

    goto :goto_0
.end method
