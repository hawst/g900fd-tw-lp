.class public Lcom/samsung/helphub/pages/smartscreen/SmartScroll;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "SmartScroll.java"


# instance fields
.field private mEnableSettingDialogFragment:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    return-void
.end method


# virtual methods
.method protected ClickEnableSettings()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 142
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/helphub/utility/TackBackUtility;->turnOffTalkBack(Landroid/content/Context;)Z

    .line 143
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "smart_scroll"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 144
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.SMART_SCROLL_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 145
    .local v0, "setting_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 146
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 148
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v3

    .line 29
    .local v3, "result":Landroid/view/View;
    const v4, 0x7f0d03e4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 30
    .local v0, "button":Landroid/widget/Button;
    new-instance v4, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$1;

    invoke-direct {v4, p0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$1;-><init>(Lcom/samsung/helphub/pages/smartscreen/SmartScroll;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    const v4, 0x7f0d03e9

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "button":Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 53
    .restart local v0    # "button":Landroid/widget/Button;
    new-instance v4, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$2;

    invoke-direct {v4, p0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$2;-><init>(Lcom/samsung/helphub/pages/smartscreen/SmartScroll;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 75
    const v4, 0x7f0d03e1

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 77
    .local v1, "mImageViewer_face_orientation":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "tutorial_smart_screen_scroll_face"

    invoke-static {v4, v1, v5}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 79
    const v4, 0x7f0d03e6

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 81
    .local v2, "mImageViewer_tilting_device":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "tutorial_smart_screen_scroll_tilt"

    invoke-static {v4, v2, v5}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 84
    .end local v1    # "mImageViewer_face_orientation":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    .end local v2    # "mImageViewer_tilting_device":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_0
    return-object v3
.end method

.method protected showEnableFaceScrollSettingDialog(II)V
    .locals 3
    .param p1, "titleID"    # I
    .param p2, "summaryID"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 91
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0064

    new-instance v2, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$4;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$4;-><init>(Lcom/samsung/helphub/pages/smartscreen/SmartScroll;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0063

    new-instance v2, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$3;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$3;-><init>(Lcom/samsung/helphub/pages/smartscreen/SmartScroll;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 113
    return-void
.end method

.method protected showEnableTiltScrollSettingDialog(II)V
    .locals 3
    .param p1, "titleID"    # I
    .param p2, "summaryID"    # I

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 119
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0064

    new-instance v2, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$6;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$6;-><init>(Lcom/samsung/helphub/pages/smartscreen/SmartScroll;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0063

    new-instance v2, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$5;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$5;-><init>(Lcom/samsung/helphub/pages/smartscreen/SmartScroll;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->mEnableSettingDialogFragment:Landroid/app/AlertDialog;

    .line 139
    return-void
.end method
