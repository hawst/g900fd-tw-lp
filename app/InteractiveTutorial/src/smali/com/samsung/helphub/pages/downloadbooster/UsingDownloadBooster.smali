.class public Lcom/samsung/helphub/pages/downloadbooster/UsingDownloadBooster;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "UsingDownloadBooster.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v9, 0x7f0d0042

    const v8, 0x7f0d0040

    .line 18
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 19
    .local v2, "result":Landroid/view/View;
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "VZW"

    invoke-static {v4}, Lcom/samsung/helphub/HelpHubCommon;->isCurrentCarrier(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 20
    const/4 v3, 0x0

    .line 21
    .local v3, "tv":Landroid/widget/TextView;
    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "tv":Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 22
    .restart local v3    # "tv":Landroid/widget/TextView;
    const v4, 0x7f0a089e

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 25
    .end local v3    # "tv":Landroid/widget/TextView;
    :cond_0
    const v4, 0x7f0a08a2

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x1e

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/samsung/helphub/pages/downloadbooster/UsingDownloadBooster;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 26
    .local v0, "donwload_booster_text_4":Ljava/lang/String;
    const/4 v1, 0x0

    .line 27
    .local v1, "download_booster_text":Lcom/samsung/helphub/widget/HelpHubTextView;
    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "download_booster_text":Lcom/samsung/helphub/widget/HelpHubTextView;
    check-cast v1, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 28
    .restart local v1    # "download_booster_text":Lcom/samsung/helphub/widget/HelpHubTextView;
    invoke-virtual {v1, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(Ljava/lang/CharSequence;)V

    .line 30
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 31
    const/4 v3, 0x0

    .line 32
    .restart local v3    # "tv":Landroid/widget/TextView;
    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "tv":Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 33
    .restart local v3    # "tv":Landroid/widget/TextView;
    const v4, 0x7f0a089f

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 34
    const v4, 0x7f0d0041

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "tv":Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 35
    .restart local v3    # "tv":Landroid/widget/TextView;
    const v4, 0x7f0a08a1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 36
    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "tv":Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 37
    .restart local v3    # "tv":Landroid/widget/TextView;
    const v4, 0x7f0a08a3

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 39
    .end local v3    # "tv":Landroid/widget/TextView;
    :cond_1
    return-object v2
.end method
