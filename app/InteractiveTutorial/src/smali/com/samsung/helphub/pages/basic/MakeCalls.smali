.class public Lcom/samsung/helphub/pages/basic/MakeCalls;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "MakeCalls.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 41
    new-instance v0, Lcom/samsung/helphub/pages/basic/MakeCalls$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/basic/MakeCalls$1;-><init>(Lcom/samsung/helphub/pages/basic/MakeCalls;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/basic/MakeCalls;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/basic/MakeCalls;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/basic/MakeCalls;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/basic/MakeCalls;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/basic/MakeCalls;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/basic/MakeCalls;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/basic/MakeCalls;->isClicked:Z

    return p1
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/helphub/pages/basic/MakeCalls;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 35
    .local v1, "result":Landroid/view/View;
    const v3, 0x7f0d0033

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 36
    .local v0, "helpHubTextView":Lcom/samsung/helphub/widget/HelpHubTextView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/basic/MakeCalls;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0119

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 37
    .local v2, "showDisplayString":Ljava/lang/String;
    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "BMC"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 38
    invoke-virtual {v0, v2}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    :cond_0
    return-object v1
.end method
