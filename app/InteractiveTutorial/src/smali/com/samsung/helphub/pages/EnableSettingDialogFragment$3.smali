.class Lcom/samsung/helphub/pages/EnableSettingDialogFragment$3;
.super Ljava/lang/Object;
.source "EnableSettingDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/EnableSettingDialogFragment;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/EnableSettingDialogFragment;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/samsung/helphub/pages/EnableSettingDialogFragment$3;->this$0:Lcom/samsung/helphub/pages/EnableSettingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 87
    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/samsung/helphub/pages/EnableSettingDialogFragment$3;->this$0:Lcom/samsung/helphub/pages/EnableSettingDialogFragment;

    invoke-virtual {v0}, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->dismiss()V

    .line 89
    iget-object v0, p0, Lcom/samsung/helphub/pages/EnableSettingDialogFragment$3;->this$0:Lcom/samsung/helphub/pages/EnableSettingDialogFragment;

    # getter for: Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->mListener:Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    invoke-static {v0}, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->access$000(Lcom/samsung/helphub/pages/EnableSettingDialogFragment;)Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/samsung/helphub/pages/EnableSettingDialogFragment$3;->this$0:Lcom/samsung/helphub/pages/EnableSettingDialogFragment;

    # getter for: Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->mListener:Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    invoke-static {v0}, Lcom/samsung/helphub/pages/EnableSettingDialogFragment;->access$000(Lcom/samsung/helphub/pages/EnableSettingDialogFragment;)Lcom/samsung/helphub/pages/IEnableSettingDialogListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/helphub/pages/IEnableSettingDialogListener;->onClickEnableSettingsButtonCancel()V

    .line 92
    :cond_0
    const/4 v0, 0x1

    .line 94
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
