.class Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview$2;
.super Ljava/lang/Object;
.source "AirViewPenInformationPreview.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview$2;->this$0:Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview$2;->this$0:Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;

    # getter for: Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;->isClicked:Z
    invoke-static {v0}, Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;->access$300(Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview$2;->this$0:Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;->isClicked:Z
    invoke-static {v0, v1}, Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;->access$402(Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;Z)Z

    .line 56
    iget-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview$2;->this$0:Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;->isSplanner:Z

    .line 58
    iget-object v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview$2;->this$0:Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;

    const-string v1, "pen_hovering_information_preview"

    const v2, 0x7f0a019f

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;->checkPenModePopup(Ljava/lang/String;I)V
    invoke-static {v0, v1, v2}, Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;->access$500(Lcom/samsung/helphub/pages/airview/AirViewPenInformationPreview;Ljava/lang/String;I)V

    goto :goto_0
.end method
