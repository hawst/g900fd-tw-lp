.class public Lcom/samsung/helphub/pages/svoice/UseSVoice;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "UseSVoice.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 51
    new-instance v0, Lcom/samsung/helphub/pages/svoice/UseSVoice$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/svoice/UseSVoice$1;-><init>(Lcom/samsung/helphub/pages/svoice/UseSVoice;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/svoice/UseSVoice;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 19
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/svoice/UseSVoice;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/svoice/UseSVoice;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/svoice/UseSVoice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/svoice/UseSVoice;

    .prologue
    .line 15
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/svoice/UseSVoice;->isClicked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/helphub/pages/svoice/UseSVoice;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/svoice/UseSVoice;
    .param p1, "x1"    # Z

    .prologue
    .line 15
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/svoice/UseSVoice;->isClicked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/svoice/UseSVoice;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/svoice/UseSVoice;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/helphub/pages/svoice/UseSVoice;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 14
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-super/range {p0 .. p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v9

    .line 24
    .local v9, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-static {v12}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 28
    const v12, 0x7f0d0020

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 29
    .local v2, "mImageView1":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    const-string v13, "svoice_use_svoice_1"

    invoke-static {v12, v2, v13}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 30
    const v12, 0x7f0d0021

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 31
    .local v3, "mImageView2":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    const-string v13, "svoice_use_svoice_2"

    invoke-static {v12, v3, v13}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 32
    const v12, 0x7f0d0022

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 33
    .local v4, "mImageView3":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    const-string v13, "svoice_use_svoice_3"

    invoke-static {v12, v4, v13}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 36
    .end local v2    # "mImageView1":Landroid/widget/ImageView;
    .end local v3    # "mImageView2":Landroid/widget/ImageView;
    .end local v4    # "mImageView3":Landroid/widget/ImageView;
    :cond_0
    const v12, 0x7f0d0034

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 37
    .local v10, "textview1":Lcom/samsung/helphub/widget/HelpHubTextView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-virtual {v12}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a0a09

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 38
    .local v5, "original1":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-virtual {v12}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a0a0b

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 39
    .local v7, "placeholder1":Ljava/lang/String;
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v7, v12, v13

    invoke-static {v5, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "guideString1":Ljava/lang/String;
    invoke-virtual {v10, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 42
    const v12, 0x7f0d0035

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 43
    .local v11, "textview2":Lcom/samsung/helphub/widget/HelpHubTextView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-virtual {v12}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a0a0a

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 44
    .local v6, "original2":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/UseSVoice;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-virtual {v12}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a0a0c

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 45
    .local v8, "placeholder2":Ljava/lang/String;
    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v8, v12, v13

    invoke-static {v6, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 46
    .local v1, "guideString2":Ljava/lang/String;
    invoke-virtual {v11, v1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 48
    return-object v9
.end method
