.class Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;
.super Ljava/lang/Object;
.source "UsingOtherAirCommandFeatures.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v9, 0x7f0a076c

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 53
    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 54
    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    # getter for: Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->isClicked:Z
    invoke-static {v3}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->access$000(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "air_button_onoff"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    .line 58
    .local v0, "isAirButtonEnabled":Z
    :goto_1
    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    # setter for: Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->isClicked:Z
    invoke-static {v3, v1}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->access$102(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;Z)Z

    .line 61
    if-nez v0, :cond_3

    .line 62
    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    iput v1, v3, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->clickedbutton:I

    .line 63
    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    iget-object v4, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    const v5, 0x7f0a07c6

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    invoke-virtual {v7, v9}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v6}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    const v6, 0x7f0a078a

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    invoke-virtual {v8, v9}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    iget-object v2, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    const v8, 0x7f0a0a27

    invoke-virtual {v2, v8}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v1

    invoke-virtual {v5, v6, v7}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->showEnableSettingDialog(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v3, v4, v1}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->access$200(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "isAirButtonEnabled":Z
    :cond_2
    move v0, v2

    .line 57
    goto :goto_1

    .line 66
    .restart local v0    # "isAirButtonEnabled":Z
    :cond_3
    iget-object v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;->this$0:Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    invoke-virtual {v1}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->startTutorialInsertingContent()V

    goto :goto_0
.end method
