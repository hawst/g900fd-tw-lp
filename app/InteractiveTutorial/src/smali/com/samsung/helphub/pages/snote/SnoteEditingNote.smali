.class public Lcom/samsung/helphub/pages/snote/SnoteEditingNote;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "SnoteEditingNote.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method

.method private IsJapanTBModel()Z
    .locals 3

    .prologue
    .line 44
    const/4 v0, 0x0

    .line 45
    .local v0, "JapanModel":Z
    const-string v1, "ro.product.name"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "SC-01G"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ro.product.name"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "SCL24"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 46
    :cond_0
    const/4 v0, 0x1

    .line 49
    :cond_1
    return v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v6, 0x8

    .line 24
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 25
    .local v1, "result":Landroid/view/View;
    const-string v4, "ro.product.name"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "tblte"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "ro.product.name"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "tbelte"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0}, Lcom/samsung/helphub/pages/snote/SnoteEditingNote;->IsJapanTBModel()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 27
    :cond_0
    const v4, 0x7f0d0020

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 28
    .local v0, "mImageView1":Landroid/widget/ImageView;
    const v4, 0x7f02033b

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 29
    const v4, 0x7f0d001b

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 30
    .local v2, "textView1":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 31
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 33
    :cond_1
    const v4, 0x7f0d0036

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 34
    .local v3, "textView2":Landroid/widget/TextView;
    if-eqz v3, :cond_2

    .line 35
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 39
    .end local v0    # "mImageView1":Landroid/widget/ImageView;
    .end local v2    # "textView1":Landroid/widget/TextView;
    .end local v3    # "textView2":Landroid/widget/TextView;
    :cond_2
    return-object v1
.end method
