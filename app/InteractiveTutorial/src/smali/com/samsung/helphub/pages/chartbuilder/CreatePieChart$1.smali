.class Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart$1;
.super Ljava/lang/Object;
.source "CreatePieChart.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;)V
    .locals 0

    .prologue
    .line 10
    iput-object p1, p0, Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart$1;->this$0:Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 13
    const-string v1, "com.samsung.android.chartbuilder"

    .line 14
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart$1;->this$0:Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;

    iget-object v3, p0, Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart$1;->this$0:Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;->access$000(Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 15
    iget-object v2, p0, Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart$1;->this$0:Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;

    # getter for: Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;->access$100(Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 30
    :goto_0
    return-void

    .line 19
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart$1;->this$0:Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;->access$202(Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;Z)Z

    .line 21
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.samsung.android.chartbuilder"

    const-string v4, "com.samsung.android.chartbuilder.help.HelperActivity"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 23
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "chartbuilder:section"

    const-string v3, "create_pie_chart"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25
    iget-object v2, p0, Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart$1;->this$0:Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 28
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart$1;->this$0:Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;

    # invokes: Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;->access$300(Lcom/samsung/helphub/pages/chartbuilder/CreatePieChart;Ljava/lang/String;)V

    goto :goto_0
.end method
