.class Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings$1;
.super Ljava/lang/Object;
.source "SmemoAccountSettings.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings$1;->this$0:Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 17
    const-string v1, "com.sec.android.widgetapp.diotek.smemo"

    .line 19
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings$1;->this$0:Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;

    iget-object v3, p0, Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings$1;->this$0:Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;->access$000(Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 21
    iget-object v2, p0, Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings$1;->this$0:Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;

    # getter for: Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;->access$100(Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 36
    :goto_0
    return-void

    .line 25
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings$1;->this$0:Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;->access$202(Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;Z)Z

    .line 27
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.smemo.InteractiveHelp"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 29
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "smemo:section"

    const-string v3, "account_settings"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    iget-object v2, p0, Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings$1;->this$0:Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 34
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings$1;->this$0:Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;

    # invokes: Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;->access$300(Lcom/samsung/helphub/pages/smemo/SmemoAccountSettings;Ljava/lang/String;)V

    goto :goto_0
.end method
