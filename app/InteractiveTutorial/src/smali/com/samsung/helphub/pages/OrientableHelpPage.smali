.class public Lcom/samsung/helphub/pages/OrientableHelpPage;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "OrientableHelpPage.java"


# instance fields
.field private mPages:[Landroid/view/View;

.field private mSize:[Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    .line 25
    return-void
.end method

.method private isRtlLanguage()Z
    .locals 2

    .prologue
    .line 109
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 110
    .local v0, "curLanguage":Ljava/lang/String;
    const-string v1, "ar"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "fa"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ur"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "he"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112
    :cond_0
    const/4 v1, 0x1

    .line 114
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 7
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/16 v6, 0x122

    const/16 v5, 0x50

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29
    invoke-super {p0, p1}, Lcom/samsung/helphub/pages/HelpPageFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 30
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 31
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v0, v0, v3

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 33
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v0, v0, v3

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/OrientableHelpPage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/HelpHubActivityBase;

    invoke-virtual {v0}, Lcom/samsung/helphub/HelpHubActivityBase;->isTwoPane()Z

    move-result v0

    if-nez v0, :cond_1

    .line 36
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mSize:[Landroid/view/View;

    aget-object v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mSize:[Landroid/view/View;

    aget-object v0, v0, v3

    if-eqz v0, :cond_1

    .line 37
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mSize:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iput v6, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 38
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mSize:[Landroid/view/View;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iput v6, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 54
    :cond_1
    :goto_0
    return-void

    .line 43
    :cond_2
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v0, v0, v2

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v0, v0, v3

    if-eqz v0, :cond_3

    .line 44
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 45
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v0, v0, v3

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 47
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/OrientableHelpPage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/HelpHubActivityBase;

    invoke-virtual {v0}, Lcom/samsung/helphub/HelpHubActivityBase;->isTwoPane()Z

    move-result v0

    if-nez v0, :cond_1

    .line 48
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mSize:[Landroid/view/View;

    aget-object v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mSize:[Landroid/view/View;

    aget-object v0, v0, v3

    if-eqz v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mSize:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 50
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mSize:[Landroid/view/View;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v10, 0x8

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 62
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v8

    .line 63
    .local v8, "result":Landroid/view/View;
    new-array v2, v6, [Landroid/view/View;

    iput-object v2, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    .line 64
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/OrientableHelpPage;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/HelpHubActivityBase;

    invoke-virtual {v2}, Lcom/samsung/helphub/HelpHubActivityBase;->isTwoPane()Z

    move-result v2

    if-nez v2, :cond_0

    .line 65
    new-array v2, v6, [Landroid/view/View;

    iput-object v2, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mSize:[Landroid/view/View;

    .line 67
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    const v3, 0x7f0d0117

    invoke-virtual {v8, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v1

    .line 68
    iget-object v2, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    const v3, 0x7f0d0118

    invoke-virtual {v8, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v4

    .line 69
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/OrientableHelpPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v6, :cond_3

    .line 70
    iget-object v2, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v2, v2, v1

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v2, v2, v4

    if-eqz v2, :cond_1

    .line 71
    iget-object v2, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v2, v2, v1

    invoke-virtual {v2, v10}, Landroid/view/View;->setVisibility(I)V

    .line 72
    iget-object v2, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v2, v2, v4

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 81
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/samsung/helphub/pages/OrientableHelpPage;->isRtlLanguage()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 82
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/OrientableHelpPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020212

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 84
    .local v0, "originalArrow":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 85
    .local v5, "sideInversion":Landroid/graphics/Matrix;
    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v5, v2, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 86
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 88
    .local v9, "sideInversionArrow":Landroid/graphics/Bitmap;
    const v1, 0x7f0d02fb

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 90
    .local v7, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 92
    .end local v0    # "originalArrow":Landroid/graphics/Bitmap;
    .end local v5    # "sideInversion":Landroid/graphics/Matrix;
    .end local v7    # "mImageView":Landroid/widget/ImageView;
    .end local v9    # "sideInversionArrow":Landroid/graphics/Bitmap;
    :cond_2
    return-object v8

    .line 76
    :cond_3
    iget-object v2, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v2, v2, v1

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v2, v2, v4

    if-eqz v2, :cond_1

    .line 77
    iget-object v2, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v2, v2, v1

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 78
    iget-object v2, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mPages:[Landroid/view/View;

    aget-object v2, v2, v4

    invoke-virtual {v2, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/16 v4, 0x122

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 97
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->onResume()V

    .line 98
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/OrientableHelpPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/OrientableHelpPage;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/HelpHubActivityBase;

    invoke-virtual {v0}, Lcom/samsung/helphub/HelpHubActivityBase;->isTwoPane()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mSize:[Landroid/view/View;

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mSize:[Landroid/view/View;

    aget-object v0, v0, v3

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mSize:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iput v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 102
    iget-object v0, p0, Lcom/samsung/helphub/pages/OrientableHelpPage;->mSize:[Landroid/view/View;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iput v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 106
    :cond_0
    return-void
.end method
