.class public Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "WBOnApplications.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;
    .param p1, "x1"    # Z

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;->showEnableSettingDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;->showEnableSettingDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 64
    return-object p0
.end method

.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications$1;-><init>(Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;)V

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;->isClicked:Z

    .line 60
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 45
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/helphub/utility/TackBackUtility;->turnOffTalkBack(Landroid/content/Context;)Z

    .line 46
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accessibility_display_magnification_enabled"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 47
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "com.sec.feature.overlaymagnifier"

    invoke-static {v0, v1}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accessibility_magnifier"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 51
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "pen_writing_buddy"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 53
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;->startTutorial()V

    .line 54
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 35
    .local v1, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 36
    const v2, 0x7f0d0249

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 38
    .local v0, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "writing_buddy_writing_on_applications"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 40
    .end local v0    # "mImageView":Landroid/widget/ImageView;
    :cond_0
    return-object v1
.end method

.method protected startTutorial()V
    .locals 3

    .prologue
    .line 108
    const/4 v0, 0x0

    .line 109
    .local v0, "mIntent":Landroid/content/Intent;
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "mIntent":Landroid/content/Intent;
    const-string v1, "com.samsung.sec.android.clockpackage.alarm_ALARM_WB_TUTORIAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 119
    .restart local v0    # "mIntent":Landroid/content/Intent;
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/writingbuddy/WBOnApplications;->startActivity(Landroid/content/Intent;)V

    .line 120
    return-void

    .line 113
    :cond_0
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "mIntent":Landroid/content/Intent;
    const-string v1, "com.samsung.contacts.action.DialerHelpActivity"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 115
    .restart local v0    # "mIntent":Landroid/content/Intent;
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "mIntent":Landroid/content/Intent;
    const-string v1, "com.android.contacts.action.DialerHelpActivity"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 117
    .restart local v0    # "mIntent":Landroid/content/Intent;
    const-string v1, "DialerGuideMode"

    const-string v2, "WRITING_BUDDY_TUTORIAL"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method
