.class Lcom/samsung/helphub/pages/multimedia/CameraModes$1;
.super Ljava/lang/Object;
.source "CameraModes.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/multimedia/CameraModes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/multimedia/CameraModes;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/multimedia/CameraModes;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/samsung/helphub/pages/multimedia/CameraModes$1;->this$0:Lcom/samsung/helphub/pages/multimedia/CameraModes;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 16
    iget-object v1, p0, Lcom/samsung/helphub/pages/multimedia/CameraModes$1;->this$0:Lcom/samsung/helphub/pages/multimedia/CameraModes;

    # getter for: Lcom/samsung/helphub/pages/multimedia/CameraModes;->isClicked:Z
    invoke-static {v1}, Lcom/samsung/helphub/pages/multimedia/CameraModes;->access$000(Lcom/samsung/helphub/pages/multimedia/CameraModes;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 23
    :goto_0
    return-void

    .line 18
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/pages/multimedia/CameraModes$1;->this$0:Lcom/samsung/helphub/pages/multimedia/CameraModes;

    # setter for: Lcom/samsung/helphub/pages/multimedia/CameraModes;->isClicked:Z
    invoke-static {v1, v3}, Lcom/samsung/helphub/pages/multimedia/CameraModes;->access$102(Lcom/samsung/helphub/pages/multimedia/CameraModes;Z)Z

    .line 19
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 20
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.camera"

    const-string v2, "com.sec.android.app.camera.Camera"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 21
    const-string v1, "help_camera_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 22
    iget-object v1, p0, Lcom/samsung/helphub/pages/multimedia/CameraModes$1;->this$0:Lcom/samsung/helphub/pages/multimedia/CameraModes;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/multimedia/CameraModes;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
