.class public Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "KeyboardSamsungKeyboard.java"


# static fields
.field private static final ACTION_REQUEST_IME_INFO:Ljava/lang/String; = "com.sec.android.inputmethod.RequestImeInfo"

.field private static final SAMSUNG_INFO_KEY_STRING:Ljava/lang/String; = "*#0*#"

.field private static final SAMSUNG_KEYPAD_STRING:Ljava/lang/String; = "com.sec.android.inputmethod/.SamsungKeypad"


# instance fields
.field private mTutorialEditText:Landroid/widget/EditText;

.field private result:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->result:Landroid/view/View;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->mTutorialEditText:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public getResourceId(Ljava/lang/String;)I
    .locals 3
    .param p1, "Name"    # Ljava/lang/String;

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getStringId(Ljava/lang/String;)I
    .locals 3
    .param p1, "Name"    # Ljava/lang/String;

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "string"

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v10, 0x8

    .line 45
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->result:Landroid/view/View;

    .line 46
    iget-object v8, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->result:Landroid/view/View;

    invoke-virtual {p0, v8}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->setupUI(Landroid/view/View;)V

    .line 47
    iget-object v8, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->result:Landroid/view/View;

    const-string v9, "imageview_keyboard_samung_keyboard_open_keyboard"

    invoke-virtual {p0, v9}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getResourceId(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    iput-object v8, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->mTutorialEditText:Landroid/widget/EditText;

    .line 49
    iget-object v8, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->mTutorialEditText:Landroid/widget/EditText;

    if-eqz v8, :cond_0

    .line 50
    iget-object v8, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->mTutorialEditText:Landroid/widget/EditText;

    new-instance v9, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard$1;

    invoke-direct {v9, p0}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard$1;-><init>(Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;)V

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 84
    iget-object v8, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->mTutorialEditText:Landroid/widget/EditText;

    new-instance v9, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard$2;

    invoke-direct {v9, p0}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard$2;-><init>(Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;)V

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 127
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/helphub/HelpHubCommon;->getOCRPKGName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 129
    .local v6, "ocrPKGName":Ljava/lang/String;
    if-nez v6, :cond_4

    .line 131
    iget-object v8, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->result:Landroid/view/View;

    const-string v9, "keyboard_text_recognition_image"

    invoke-virtual {p0, v9}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getResourceId(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 133
    .local v0, "mMultiModeIconImage":Landroid/widget/ImageView;
    if-eqz v0, :cond_1

    .line 134
    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 137
    :cond_1
    iget-object v8, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->result:Landroid/view/View;

    const-string v9, "keyboard_samung_keyboard_text_recognition"

    invoke-virtual {p0, v9}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getResourceId(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 139
    .local v4, "mOcrTitleText":Landroid/widget/TextView;
    if-eqz v4, :cond_2

    .line 140
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 143
    :cond_2
    iget-object v8, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->result:Landroid/view/View;

    const-string v9, "keyboard_samung_keyboard_tap_text_recognition_icon"

    invoke-virtual {p0, v9}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getResourceId(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 145
    .local v3, "mOcrText":Landroid/widget/TextView;
    if-eqz v3, :cond_3

    .line 146
    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 149
    :cond_3
    iget-object v8, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->result:Landroid/view/View;

    const-string v9, "imageview_keyboard_samung_keyboard_text_recognition"

    invoke-virtual {p0, v9}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getResourceId(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 151
    .local v2, "mOcrIconImage":Landroid/widget/ImageView;
    if-eqz v2, :cond_4

    .line 152
    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 159
    .end local v0    # "mMultiModeIconImage":Landroid/widget/ImageView;
    .end local v2    # "mOcrIconImage":Landroid/widget/ImageView;
    .end local v3    # "mOcrText":Landroid/widget/TextView;
    .end local v4    # "mOcrTitleText":Landroid/widget/TextView;
    :cond_4
    iget-object v8, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->result:Landroid/view/View;

    const-string v9, "keyboard_samung_keyboard_tap_change_keyboard_type_icon"

    invoke-virtual {p0, v9}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getResourceId(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 161
    .local v5, "mTextView":Landroid/widget/TextView;
    if-eqz v5, :cond_5

    .line 162
    const-string v8, "keyboard_samung_keyboard_tap_change_keyboard_type_icon_one_hand"

    invoke-virtual {p0, v8}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getStringId(Ljava/lang/String;)I

    move-result v7

    .line 163
    .local v7, "strId":I
    if-lez v7, :cond_5

    .line 164
    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(I)V

    .line 214
    .end local v7    # "strId":I
    :cond_5
    iget-object v8, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->result:Landroid/view/View;

    const-string v9, "keyboard_samung_keyboard_hold_multi_mode_icon"

    invoke-virtual {p0, v9}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getResourceId(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 216
    .local v1, "mMultiModeText":Landroid/widget/TextView;
    const/4 v7, 0x0

    .line 217
    .restart local v7    # "strId":I
    if-eqz v1, :cond_6

    .line 219
    if-nez v6, :cond_8

    .line 221
    const/4 v8, 0x0

    invoke-static {v8}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 222
    const-string v8, "keyboard_samung_keyboard_hold_multi_mode_icon_emoji_hanja_no_ocr_tablet"

    invoke-virtual {p0, v8}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getStringId(Ljava/lang/String;)I

    move-result v7

    .line 244
    :goto_0
    if-lez v7, :cond_6

    .line 245
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(I)V

    .line 248
    :cond_6
    iget-object v8, p0, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->result:Landroid/view/View;

    return-object v8

    .line 224
    :cond_7
    const-string v8, "keyboard_samung_keyboard_hold_multi_mode_icon_emoji_hanja_no_ocr"

    invoke-virtual {p0, v8}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getStringId(Ljava/lang/String;)I

    move-result v7

    goto :goto_0

    .line 230
    :cond_8
    const-string v8, "keyboard_samung_keyboard_hold_multi_mode_icon_emoji_ocr"

    invoke-virtual {p0, v8}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getStringId(Ljava/lang/String;)I

    move-result v7

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 306
    .local v0, "focusView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 307
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 311
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_0

    .line 312
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 315
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->onDestroy()V

    .line 316
    return-void
.end method

.method public setupUI(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 277
    instance-of v2, p1, Landroid/widget/EditText;

    if-nez v2, :cond_0

    .line 278
    new-instance v2, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard$3;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard$3;-><init>(Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 295
    :cond_0
    instance-of v2, p1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    .line 296
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    move-object v2, p1

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    move-object v2, p1

    .line 297
    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 298
    .local v1, "innerView":Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/pages/basic/KeyboardSamsungKeyboard;->setupUI(Landroid/view/View;)V

    .line 296
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 301
    .end local v0    # "i":I
    .end local v1    # "innerView":Landroid/view/View;
    :cond_1
    return-void
.end method
