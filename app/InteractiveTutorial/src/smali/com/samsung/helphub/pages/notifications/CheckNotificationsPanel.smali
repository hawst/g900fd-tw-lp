.class public Lcom/samsung/helphub/pages/notifications/CheckNotificationsPanel;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "CheckNotificationsPanel.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 18
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 19
    .local v2, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/notifications/CheckNotificationsPanel;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 20
    const v3, 0x7f0d03a6

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 22
    .local v0, "mImageView_01":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/notifications/CheckNotificationsPanel;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "notification_use_the_notification_panel_03"

    invoke-static {v3, v0, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 24
    const v3, 0x7f0d03a9

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 26
    .local v1, "mImageView_02":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/notifications/CheckNotificationsPanel;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "notification_use_expandable_notification"

    invoke-static {v3, v1, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 29
    .end local v0    # "mImageView_01":Landroid/widget/ImageView;
    .end local v1    # "mImageView_02":Landroid/widget/ImageView;
    :cond_0
    return-object v2
.end method
