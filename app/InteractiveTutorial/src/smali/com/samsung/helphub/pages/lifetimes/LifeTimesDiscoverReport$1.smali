.class Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport$1;
.super Ljava/lang/Object;
.source "LifeTimesDiscoverReport.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport$1;->this$0:Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 16
    const-string v1, "com.samsung.android.app.lifetimes"

    .line 18
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport$1;->this$0:Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;

    iget-object v3, p0, Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport$1;->this$0:Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;->access$000(Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 19
    iget-object v2, p0, Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport$1;->this$0:Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;

    # getter for: Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;->access$100(Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 27
    :goto_0
    return-void

    .line 21
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport$1;->this$0:Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;->access$202(Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;Z)Z

    .line 22
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.samsung.android.app.lifetimes.TRY_DISCOVER_REPORT"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 23
    .local v0, "mIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport$1;->this$0:Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 25
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport$1;->this$0:Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;

    # invokes: Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;->access$300(Lcom/samsung/helphub/pages/lifetimes/LifeTimesDiscoverReport;Ljava/lang/String;)V

    goto :goto_0
.end method
