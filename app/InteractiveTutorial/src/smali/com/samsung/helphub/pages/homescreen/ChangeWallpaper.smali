.class public Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "ChangeWallpaper.java"

# interfaces
.implements Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;


# static fields
.field private static final NOTIFICATION_ID:I = 0x64


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 25
    new-instance v0, Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper$1;-><init>(Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper;

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper;
    .param p1, "x1"    # Z

    .prologue
    .line 21
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper;->isClicked:Z

    return p1
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 99
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 86
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ExternalHelpActivity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/ExternalHelpActivity;

    sget-boolean v0, Lcom/samsung/helphub/ExternalHelpActivity;->sIsReturnFromLauncher:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/ExternalHelpActivity;

    sget-object v0, Lcom/samsung/helphub/ExternalHelpActivity;->sItemOpenReq:Ljava/lang/String;

    const-string v1, "homescreen_change_wallpaper"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/homescreen/ChangeWallpaper;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/ExternalHelpActivity;

    invoke-virtual {v0}, Lcom/samsung/helphub/ExternalHelpActivity;->startHomeScreenSection()V

    .line 90
    const/4 v0, 0x1

    .line 92
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
