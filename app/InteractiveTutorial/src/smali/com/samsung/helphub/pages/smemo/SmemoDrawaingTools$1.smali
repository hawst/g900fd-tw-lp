.class Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools$1;
.super Ljava/lang/Object;
.source "SmemoDrawaingTools.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools$1;->this$0:Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 18
    const-string v1, "com.sec.android.widgetapp.diotek.smemo"

    .line 20
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools$1;->this$0:Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;

    iget-object v3, p0, Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools$1;->this$0:Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;->access$000(Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 22
    iget-object v2, p0, Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools$1;->this$0:Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;

    # getter for: Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;->access$100(Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    :goto_0
    return-void

    .line 26
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools$1;->this$0:Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;->access$202(Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;Z)Z

    .line 28
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.smemo.InteractiveHelp"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 30
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "smemo:section"

    const-string v3, "drawing_tools"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 32
    iget-object v2, p0, Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools$1;->this$0:Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 36
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools$1;->this$0:Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;

    # invokes: Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;->access$300(Lcom/samsung/helphub/pages/smemo/SmemoDrawaingTools;Ljava/lang/String;)V

    goto :goto_0
.end method
