.class Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting$1;
.super Ljava/lang/Object;
.source "SnoteUsePenSetting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;)V
    .locals 0

    .prologue
    .line 10
    iput-object p1, p0, Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting$1;->this$0:Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 14
    const-string v1, "com.samsung.android.snote"

    .line 15
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting$1;->this$0:Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;

    iget-object v3, p0, Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting$1;->this$0:Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;->access$000(Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 16
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting$1;->this$0:Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;

    # getter for: Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;->access$100(Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    :goto_0
    return-void

    .line 20
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting$1;->this$0:Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;->access$202(Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;Z)Z

    .line 22
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.samsung.android.snote"

    const-string v4, "com.samsung.android.snote.helper.HelperActivity"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 24
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "snote:section"

    const-string v3, "use_pen_setting"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 26
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting$1;->this$0:Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 29
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting$1;->this$0:Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;

    # invokes: Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;->access$300(Lcom/samsung/helphub/pages/snote/SnoteUsePenSetting;Ljava/lang/String;)V

    goto :goto_0
.end method
