.class Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$9;
.super Ljava/lang/Object;
.source "AirViewInformationPreview.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->showGallerySettingDialog(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$9;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 174
    iget-object v1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$9;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    # invokes: Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->turnOnRelevantSetting()V
    invoke-static {v1}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->access$800(Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;)V

    .line 176
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 177
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "image/jpg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    const-string v1, "com.sec.android.gallery3d"

    const-string v2, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    const-string v1, "IsHelpMode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 180
    const-string v1, "HelpMode"

    const-string v2, "INFORMATION_PREVIEW"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    iget-object v1, p0, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview$9;->this$0:Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/airview/AirViewInformationPreview;->startActivity(Landroid/content/Intent;)V

    .line 182
    return-void
.end method
