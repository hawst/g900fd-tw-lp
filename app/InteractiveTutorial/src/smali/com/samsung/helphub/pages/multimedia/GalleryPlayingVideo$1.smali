.class Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo$1;
.super Ljava/lang/Object;
.source "GalleryPlayingVideo.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 16
    const-string v1, "com.sec.android.gallery3d"

    .line 18
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;

    iget-object v3, p0, Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;->access$000(Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 19
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;

    # getter for: Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;->access$100(Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    :goto_0
    return-void

    .line 21
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;->access$202(Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;Z)Z

    .line 22
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 23
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "video/3gpp"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 24
    const-string v2, "com.sec.android.gallery3d"

    const-string v3, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 25
    const-string v2, "Help"

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 26
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 29
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;

    # invokes: Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;->access$300(Lcom/samsung/helphub/pages/multimedia/GalleryPlayingVideo;Ljava/lang/String;)V

    goto :goto_0
.end method
