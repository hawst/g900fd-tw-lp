.class public Lcom/samsung/helphub/pages/powersaving/UsingPowerSavingMode;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "UsingPowerSavingMode.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v11, 0x8

    .line 25
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v4

    .line 26
    .local v4, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/powersaving/UsingPowerSavingMode;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 27
    const v9, 0x7f0d043e

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 29
    .local v0, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/powersaving/UsingPowerSavingMode;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const-string v10, "help_power_saving_mode_grayscale"

    invoke-static {v9, v0, v10}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 35
    .end local v0    # "mImageView":Landroid/widget/ImageView;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/powersaving/UsingPowerSavingMode;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/helphub/utility/Utils;->isWifiOnly(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 37
    const v9, 0x7f0d0017

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 38
    .local v8, "titleText1":Landroid/widget/TextView;
    if-eqz v8, :cond_1

    .line 39
    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 41
    :cond_1
    const v9, 0x7f0d005d

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 42
    .local v1, "numberItem1":Landroid/widget/TextView;
    if-eqz v1, :cond_2

    .line 43
    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 44
    :cond_2
    const v9, 0x7f0d0032

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 45
    .local v5, "textItem1":Lcom/samsung/helphub/widget/HelpHubTextView;
    if-eqz v5, :cond_3

    .line 46
    invoke-virtual {v5, v11}, Lcom/samsung/helphub/widget/HelpHubTextView;->setVisibility(I)V

    .line 48
    :cond_3
    const v9, 0x7f0d005e

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 49
    .local v2, "numberItem2":Landroid/widget/TextView;
    if-eqz v2, :cond_4

    .line 50
    invoke-virtual {v2, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 51
    :cond_4
    const v9, 0x7f0d0033

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 52
    .local v6, "textItem2":Landroid/widget/TextView;
    if-eqz v6, :cond_5

    .line 53
    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 55
    :cond_5
    const v9, 0x7f0d005f

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 56
    .local v3, "numberItem3":Landroid/widget/TextView;
    if-eqz v3, :cond_6

    .line 57
    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 58
    :cond_6
    const v9, 0x7f0d0034

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/samsung/helphub/widget/HelpHubTextView;

    .line 59
    .local v7, "textItem3":Lcom/samsung/helphub/widget/HelpHubTextView;
    if-eqz v7, :cond_7

    .line 60
    invoke-virtual {v7, v11}, Lcom/samsung/helphub/widget/HelpHubTextView;->setVisibility(I)V

    .line 62
    .end local v1    # "numberItem1":Landroid/widget/TextView;
    .end local v2    # "numberItem2":Landroid/widget/TextView;
    .end local v3    # "numberItem3":Landroid/widget/TextView;
    .end local v5    # "textItem1":Lcom/samsung/helphub/widget/HelpHubTextView;
    .end local v6    # "textItem2":Landroid/widget/TextView;
    .end local v7    # "textItem3":Lcom/samsung/helphub/widget/HelpHubTextView;
    .end local v8    # "titleText1":Landroid/widget/TextView;
    :cond_7
    return-object v4
.end method
