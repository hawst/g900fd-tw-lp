.class public Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;
.super Lcom/samsung/helphub/pages/HelpPageFragment;
.source "CameraIntroduction.java"


# instance fields
.field mPages:[Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;-><init>()V

    .line 20
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    invoke-super {p0, p1}, Lcom/samsung/helphub/pages/HelpPageFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 26
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 27
    iget-object v0, p0, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->mPages:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 28
    iget-object v0, p0, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->mPages:[Landroid/view/View;

    aget-object v0, v0, v3

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 33
    :goto_0
    return-void

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->mPages:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 31
    iget-object v0, p0, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->mPages:[Landroid/view/View;

    aget-object v0, v0, v3

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v9, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 40
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v4

    .line 42
    .local v4, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "easy_mode_switch"

    invoke-static {v7, v8, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-nez v7, :cond_1

    move v3, v5

    .line 43
    .local v3, "mState":Z
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 45
    if-eqz v3, :cond_2

    .line 46
    const-string v0, "camera_introduction_easymode"

    .line 50
    .local v0, "imageName":Ljava/lang/String;
    :goto_1
    const v7, 0x7f0d0284

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 52
    .local v1, "mImageView_01":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7, v1, v0}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 53
    const v7, 0x7f0d0281

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 55
    .local v2, "mImageView_02":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7, v2, v0}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 58
    .end local v0    # "imageName":Ljava/lang/String;
    .end local v1    # "mImageView_01":Landroid/widget/ImageView;
    .end local v2    # "mImageView_02":Landroid/widget/ImageView;
    :cond_0
    const/4 v7, 0x3

    new-array v7, v7, [Landroid/view/View;

    iput-object v7, p0, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->mPages:[Landroid/view/View;

    .line 59
    iget-object v7, p0, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->mPages:[Landroid/view/View;

    const v8, 0x7f0d0283

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    aput-object v8, v7, v6

    .line 60
    iget-object v7, p0, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->mPages:[Landroid/view/View;

    const v8, 0x7f0d0280

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    aput-object v8, v7, v5

    .line 61
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_3

    .line 62
    iget-object v7, p0, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->mPages:[Landroid/view/View;

    aget-object v7, v7, v6

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    .line 63
    iget-object v7, p0, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->mPages:[Landroid/view/View;

    aget-object v5, v7, v5

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 68
    :goto_2
    return-object v4

    .end local v3    # "mState":Z
    :cond_1
    move v3, v6

    .line 42
    goto :goto_0

    .line 48
    .restart local v3    # "mState":Z
    :cond_2
    const-string v0, "camera_introduction"

    .restart local v0    # "imageName":Ljava/lang/String;
    goto :goto_1

    .line 65
    .end local v0    # "imageName":Ljava/lang/String;
    :cond_3
    iget-object v7, p0, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->mPages:[Landroid/view/View;

    aget-object v7, v7, v6

    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    .line 66
    iget-object v6, p0, Lcom/samsung/helphub/pages/multimedia/CameraIntroduction;->mPages:[Landroid/view/View;

    aget-object v5, v6, v5

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageFragment;->onResume()V

    .line 74
    return-void
.end method
