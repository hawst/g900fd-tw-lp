.class public Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "SnoteHowSetLock.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 11
    new-instance v0, Lcom/samsung/helphub/pages/snote/SnoteHowSetLock$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/snote/SnoteHowSetLock$1;-><init>(Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 9
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;

    .prologue
    .line 9
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;->isClicked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;
    .param p1, "x1"    # Z

    .prologue
    .line 9
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;->isClicked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/helphub/pages/snote/SnoteHowSetLock;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method
