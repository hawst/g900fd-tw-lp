.class Lcom/samsung/helphub/pages/smartscreen/SmartScroll$2;
.super Ljava/lang/Object;
.source "SmartScroll.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/smartscreen/SmartScroll;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/smartscreen/SmartScroll;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$2;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartScroll;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 56
    iget-object v3, p0, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$2;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartScroll;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "smart_scroll"

    invoke-static {v3, v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 57
    .local v1, "settingStatus":Z
    :cond_0
    if-nez v1, :cond_2

    .line 58
    iget-object v2, p0, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$2;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartScroll;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 59
    iget-object v2, p0, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$2;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartScroll;

    const v3, 0x7f0a057e

    const v4, 0x7f0a057f

    invoke-virtual {v2, v3, v4}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->showEnableTiltScrollSettingDialog(II)V

    .line 72
    :goto_0
    return-void

    .line 63
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$2;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartScroll;

    const v3, 0x7f0a057c

    const v4, 0x7f0a057d

    invoke-virtual {v2, v3, v4}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->showEnableTiltScrollSettingDialog(II)V

    goto :goto_0

    .line 68
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.sbrowsertry.GUIDE_SMART_SCROLL_TILT"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 69
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v3, "fromHelp"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 70
    iget-object v2, p0, Lcom/samsung/helphub/pages/smartscreen/SmartScroll$2;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartScroll;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/smartscreen/SmartScroll;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
