.class Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails$1;
.super Ljava/lang/Object;
.source "GalleryViewingPictureResizingThumbnails.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;)V
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 23
    const-string v1, "com.sec.android.gallery3d"

    .line 25
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;

    iget-object v3, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->access$000(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 26
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;

    # getter for: Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->access$100(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    :goto_0
    return-void

    .line 28
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;

    # setter for: Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->isClicked:Z
    invoke-static {v2, v4}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->access$202(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;Z)Z

    .line 29
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 30
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "image/jpg"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    const-string v2, "com.sec.android.gallery3d"

    const-string v3, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 32
    const-string v2, "IsHelpMode"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 33
    const-string v2, "HelpMode"

    const-string v3, "VIEWING_PICTURES"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 34
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 37
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;

    # invokes: Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;->access$300(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnails;Ljava/lang/String;)V

    goto :goto_0
.end method
