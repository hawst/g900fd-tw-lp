.class public Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;
.super Lcom/samsung/helphub/pages/AirViewPenFingerPage;
.source "AirViewPenFingerListScrolling.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling$2;
    }
.end annotation


# instance fields
.field functionID:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;-><init>()V

    .line 23
    const v0, 0x7f0a01a6

    iput v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->functionID:I

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->isClicked:Z

    return p1
.end method


# virtual methods
.method protected checkPenModePopup(Ljava/lang/String;I)V
    .locals 7
    .param p1, "fuctionMode"    # Ljava/lang/String;
    .param p2, "functionStringID"    # I

    .prologue
    const v6, 0x7f0a01b9

    const/4 v5, 0x0

    const/4 v3, 0x1

    const v4, 0x7f0a01b0

    .line 49
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 51
    .local v0, "res":Landroid/content/res/Resources;
    iput-boolean v3, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->isPenMode:Z

    .line 52
    sget-object v1, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling$2;->$SwitchMap$com$samsung$helphub$pages$AirViewPenFingerPage$AirViewStatus:[I

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->checkAirviewStatus(Ljava/lang/String;)Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/AirViewPenFingerPage$AirViewStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 74
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->startTutorial()V

    .line 77
    :goto_0
    return-void

    .line 54
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->checkTalkbackAndMagnification()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    const v1, 0x7f0a01b1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4, v1}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 58
    :cond_0
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4, v1}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 63
    :pswitch_1
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4, v1}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 67
    :pswitch_2
    const v1, 0x7f0a01b5

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4, v1}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->showEnableSettingDialog(ILjava/lang/String;)V

    goto :goto_0

    .line 71
    :pswitch_3
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->startTutorial()V

    goto :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 116
    return-object p0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->isClicked:Z

    .line 102
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->checkTalkbackAndMagnification()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->setTalkbackAndMagnification()V

    .line 92
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->isPenMode:Z

    if-eqz v0, :cond_1

    .line 93
    const-string v0, "pen_hovering_list_scroll"

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->setEnableSettings(Ljava/lang/String;)V

    .line 96
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->startTutorial()V

    .line 97
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 28
    .local v2, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 29
    const v3, 0x7f0d0237

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 31
    .local v1, "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "tutorial_air_view_list_scrolling"

    invoke-static {v3, v1, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 34
    .end local v1    # "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_0
    const v3, 0x7f0d0081

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 35
    .local v0, "button":Landroid/widget/Button;
    new-instance v3, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling$1;

    invoke-direct {v3, p0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling$1;-><init>(Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    return-object v2
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0}, Lcom/samsung/helphub/pages/AirViewPenFingerPage;->onResume()V

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->isClicked:Z

    .line 84
    return-void
.end method

.method protected startTutorial()V
    .locals 3

    .prologue
    .line 106
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 107
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "image/jpg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    const-string v1, "com.sec.android.gallery3d"

    const-string v2, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    const-string v1, "IsHelpMode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 110
    const-string v1, "HelpMode"

    const-string v2, "AIR_VIEW_LIST_SCROLLING"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airview/AirViewPenFingerListScrolling;->startActivity(Landroid/content/Intent;)V

    .line 112
    return-void
.end method
