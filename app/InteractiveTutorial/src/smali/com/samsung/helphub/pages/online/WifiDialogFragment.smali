.class public Lcom/samsung/helphub/pages/online/WifiDialogFragment;
.super Landroid/app/DialogFragment;
.source "WifiDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 75
    packed-switch p2, :pswitch_data_0

    .line 86
    :goto_0
    return-void

    .line 77
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/helphub/pages/online/WifiDialogFragment;->mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    invoke-interface {v0}, Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;->onClickWiFi()V

    goto :goto_0

    .line 80
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/helphub/pages/online/WifiDialogFragment;->mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    invoke-interface {v0}, Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;->onClickCancel()V

    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 25
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const/4 v0, 0x1

    const v1, 0x7f0e003a

    invoke-virtual {p0, v0, v1}, Lcom/samsung/helphub/pages/online/WifiDialogFragment;->setStyle(II)V

    .line 27
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 35
    const v1, 0x7f0a0593

    .line 39
    .local v1, "setUpWiFiButton":I
    :goto_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/WifiDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a058e

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0590

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0597

    invoke-virtual {v2, v3, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/samsung/helphub/pages/online/WifiDialogFragment$1;

    invoke-direct {v3, p0}, Lcom/samsung/helphub/pages/online/WifiDialogFragment$1;-><init>(Lcom/samsung/helphub/pages/online/WifiDialogFragment;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 58
    .local v0, "dialog":Landroid/app/Dialog;
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/pages/online/WifiDialogFragment;->setCancelable(Z)V

    .line 59
    return-object v0

    .line 37
    .end local v0    # "dialog":Landroid/app/Dialog;
    .end local v1    # "setUpWiFiButton":I
    :cond_0
    const v1, 0x7f0a0594

    .restart local v1    # "setUpWiFiButton":I
    goto :goto_0
.end method

.method public setDialogListener(Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/helphub/pages/online/WifiDialogFragment;->mListener:Lcom/samsung/helphub/pages/online/DialogMobileConnectionListener;

    .line 70
    return-void
.end method
