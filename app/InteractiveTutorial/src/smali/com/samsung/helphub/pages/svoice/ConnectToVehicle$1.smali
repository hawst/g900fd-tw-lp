.class Lcom/samsung/helphub/pages/svoice/ConnectToVehicle$1;
.super Ljava/lang/Object;
.source "ConnectToVehicle.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;)V
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle$1;->this$0:Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    .line 21
    const-string v3, "com.vlingo.midas"

    .line 23
    .local v3, "pkgName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle$1;->this$0:Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;

    iget-object v5, p0, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle$1;->this$0:Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;

    invoke-virtual {v5}, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;->getActivity()Landroid/app/Activity;

    move-result-object v5

    # invokes: Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v4, v5, v3}, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;->access$000(Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 24
    iget-object v4, p0, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle$1;->this$0:Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;

    # getter for: Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;->isClicked:Z
    invoke-static {v4}, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;->access$100(Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 46
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    iget-object v4, p0, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle$1;->this$0:Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;

    # setter for: Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;->isClicked:Z
    invoke-static {v4, v6}, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;->access$202(Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;Z)Z

    .line 30
    iget-object v4, p0, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle$1;->this$0:Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 32
    .local v0, "activity":Landroid/app/Activity;
    const/4 v1, 0x1

    .line 34
    .local v1, "isHelpSupported":Z
    if-eqz v0, :cond_0

    .line 35
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 36
    .local v2, "mIntent":Landroid/content/Intent;
    const-string v4, "android.settings.MY_PLACE_PROFILE_SETTINGS"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    const-string v4, "fromHelp"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 38
    const-string v4, "edit_mode"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 39
    const-string v4, "id"

    const/4 v5, 0x2

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 40
    iget-object v4, p0, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle$1;->this$0:Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;

    invoke-virtual {v4, v2}, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 44
    .end local v0    # "activity":Landroid/app/Activity;
    .end local v1    # "isHelpSupported":Z
    .end local v2    # "mIntent":Landroid/content/Intent;
    :cond_2
    iget-object v4, p0, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle$1;->this$0:Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;

    # invokes: Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v4, v3}, Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;->access$300(Lcom/samsung/helphub/pages/svoice/ConnectToVehicle;Ljava/lang/String;)V

    goto :goto_0
.end method
