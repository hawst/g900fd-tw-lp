.class Lcom/samsung/helphub/pages/settings/BluetoothSetup$1;
.super Ljava/lang/Object;
.source "BluetoothSetup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/settings/BluetoothSetup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field final synthetic this$0:Lcom/samsung/helphub/pages/settings/BluetoothSetup;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/settings/BluetoothSetup;)V
    .locals 1

    .prologue
    .line 20
    iput-object p1, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup$1;->this$0:Lcom/samsung/helphub/pages/settings/BluetoothSetup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup$1;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 26
    iget-object v2, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup$1;->this$0:Lcom/samsung/helphub/pages/settings/BluetoothSetup;

    # getter for: Lcom/samsung/helphub/pages/settings/BluetoothSetup;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/settings/BluetoothSetup;->access$000(Lcom/samsung/helphub/pages/settings/BluetoothSetup;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup$1;->this$0:Lcom/samsung/helphub/pages/settings/BluetoothSetup;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/settings/BluetoothSetup;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/settings/BluetoothSetup;->access$102(Lcom/samsung/helphub/pages/settings/BluetoothSetup;Z)Z

    .line 29
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup$1;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 30
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup$1;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 31
    iget-object v2, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup$1;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v2, :cond_0

    .line 35
    :goto_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup$1;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v2

    const/16 v3, 0xd

    if-ne v2, v3, :cond_2

    .line 36
    const-string v2, "BluetoothSetup"

    const-string v3, "Bluetooth is in TURNING_OFF state. Wait for 300ms to TURN_OFF"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    const-wide/16 v2, 0x12c

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 39
    :catch_0
    move-exception v0

    .line 40
    .local v0, "ignored":Ljava/lang/InterruptedException;
    const-string v2, "BluetoothSetup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "InterruptedException"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 44
    .end local v0    # "ignored":Ljava/lang/InterruptedException;
    :cond_2
    iget-object v2, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup$1;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_3

    .line 45
    iget-object v2, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup$1;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    .line 49
    :cond_3
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 50
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.Settings$BluetoothSettingsActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    const-string v2, "settings:guide_mode"

    const-string v3, "bt"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const v2, 0x8000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 53
    iget-object v2, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup$1;->this$0:Lcom/samsung/helphub/pages/settings/BluetoothSetup;

    invoke-virtual {v2, v1}, Lcom/samsung/helphub/pages/settings/BluetoothSetup;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
