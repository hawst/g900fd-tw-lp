.class public Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "AirViewShowStatusBar.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;
    .param p1, "x1"    # Z

    .prologue
    .line 14
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;->showEnableSettingDialog(II)V

    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 39
    return-object p0
.end method

.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar$1;-><init>(Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;)V

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;->isClicked:Z

    .line 35
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 20
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 21
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view_highlight"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 22
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view_show_up_indicator"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 24
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.gesture.FINGER_AIR_VIEW_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 25
    .local v0, "finger_air_view_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 26
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 28
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airview/AirViewShowStatusBar;->startTutorial()V

    .line 29
    return-void
.end method

.method protected startTutorial()V
    .locals 0

    .prologue
    .line 68
    return-void
.end method
