.class Lcom/samsung/helphub/pages/smartscreen/SmartStay$1;
.super Ljava/lang/Object;
.source "SmartStay.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/smartscreen/SmartStay;->getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/smartscreen/SmartStay;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/smartscreen/SmartStay;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/samsung/helphub/pages/smartscreen/SmartStay$1;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartStay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 59
    iget-object v2, p0, Lcom/samsung/helphub/pages/smartscreen/SmartStay$1;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartStay;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "intelligent_sleep_mode"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 60
    .local v0, "settingStatus":Z
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/smartscreen/SmartStay$1;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartStay;

    # getter for: Lcom/samsung/helphub/pages/smartscreen/SmartStay;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->access$000(Lcom/samsung/helphub/pages/smartscreen/SmartStay;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 76
    :goto_0
    return-void

    .line 62
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/smartscreen/SmartStay$1;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartStay;

    # setter for: Lcom/samsung/helphub/pages/smartscreen/SmartStay;->isClicked:Z
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->access$102(Lcom/samsung/helphub/pages/smartscreen/SmartStay;Z)Z

    .line 63
    if-nez v0, :cond_3

    .line 64
    iget-object v1, p0, Lcom/samsung/helphub/pages/smartscreen/SmartStay$1;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartStay;

    invoke-virtual {v1}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/helphub/utility/TackBackUtility;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 65
    iget-object v1, p0, Lcom/samsung/helphub/pages/smartscreen/SmartStay$1;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartStay;

    const v2, 0x7f0a0580

    const v3, 0x7f0a0581

    # invokes: Lcom/samsung/helphub/pages/smartscreen/SmartStay;->showEnableSettingDialog(II)V
    invoke-static {v1, v2, v3}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->access$200(Lcom/samsung/helphub/pages/smartscreen/SmartStay;II)V

    goto :goto_0

    .line 69
    :cond_2
    iget-object v1, p0, Lcom/samsung/helphub/pages/smartscreen/SmartStay$1;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartStay;

    const v2, 0x7f0a0577

    const v3, 0x7f0a0578

    # invokes: Lcom/samsung/helphub/pages/smartscreen/SmartStay;->showEnableSettingDialog(II)V
    invoke-static {v1, v2, v3}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->access$300(Lcom/samsung/helphub/pages/smartscreen/SmartStay;II)V

    goto :goto_0

    .line 74
    :cond_3
    iget-object v1, p0, Lcom/samsung/helphub/pages/smartscreen/SmartStay$1;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartStay;

    invoke-virtual {v1}, Lcom/samsung/helphub/pages/smartscreen/SmartStay;->startTutorial()V

    goto :goto_0
.end method
