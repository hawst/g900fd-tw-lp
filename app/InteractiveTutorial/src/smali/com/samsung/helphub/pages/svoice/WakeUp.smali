.class public Lcom/samsung/helphub/pages/svoice/WakeUp;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "WakeUp.java"


# instance fields
.field private mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private view1:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 115
    new-instance v0, Lcom/samsung/helphub/pages/svoice/WakeUp$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/svoice/WakeUp$1;-><init>(Lcom/samsung/helphub/pages/svoice/WakeUp;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/svoice/WakeUp;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/svoice/WakeUp;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/svoice/WakeUp;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/svoice/WakeUp;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/svoice/WakeUp;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/svoice/WakeUp;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/svoice/WakeUp;->isClicked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/helphub/pages/svoice/WakeUp;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/svoice/WakeUp;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/svoice/WakeUp;->isClicked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/svoice/WakeUp;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/svoice/WakeUp;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/svoice/WakeUp;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/helphub/pages/svoice/WakeUp;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public isH_Device()Z
    .locals 2

    .prologue
    .line 105
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 106
    .local v0, "build_model":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "SM-N900"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ASH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Madrid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "JS01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SC-02F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SM-G910"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 110
    :cond_0
    const/4 v1, 0x1

    .line 113
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 31
    .local v0, "result":Landroid/view/View;
    const v1, 0x7f0d0032

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/helphub/pages/svoice/WakeUp;->view1:Landroid/widget/TextView;

    .line 33
    return-object v0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 37
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onResume()V

    .line 38
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/WakeUp;->setwakeuptext()V

    .line 39
    return-void
.end method

.method public setwakeuptext()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 42
    new-instance v10, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-direct {v10}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;-><init>()V

    iput-object v10, p0, Lcom/samsung/helphub/pages/svoice/WakeUp;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    .line 43
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/WakeUp;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 44
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/WakeUp;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 45
    .local v4, "resources":Landroid/content/res/Resources;
    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 47
    .local v0, "configuration":Landroid/content/res/Configuration;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    .line 49
    .local v3, "previousLocale":Ljava/util/Locale;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "voicetalk_language"

    invoke-static {v10, v11}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 51
    .local v7, "voiceLocale":Ljava/lang/String;
    if-nez v7, :cond_7

    .line 53
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 54
    const-string v7, "zh-CN"

    .line 64
    :cond_0
    :goto_0
    const-string v10, "en-gb_GB"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 65
    const-string v7, "en_GB"

    .line 67
    :cond_1
    new-instance v8, Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v8, v7, v10, v11}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    .local v8, "voicetalkLocale":Ljava/util/Locale;
    const-string v10, "pt-BR"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 70
    const-string v10, "-"

    invoke-virtual {v7, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 71
    .local v5, "splitVoiceLocale":[Ljava/lang/String;
    new-instance v8, Ljava/util/Locale;

    .end local v8    # "voicetalkLocale":Ljava/util/Locale;
    aget-object v10, v5, v12

    aget-object v11, v5, v13

    invoke-direct {v8, v10, v11}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .end local v5    # "splitVoiceLocale":[Ljava/lang/String;
    .restart local v8    # "voicetalkLocale":Ljava/util/Locale;
    :cond_2
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v10

    if-eqz v10, :cond_3

    const-string v10, "zh-CN"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 75
    sget-object v8, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    .line 77
    :cond_3
    iget-object v10, p0, Lcom/samsung/helphub/pages/svoice/WakeUp;->mBargeInRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    invoke-virtual {v10}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->isUseChineseModel()Z

    move-result v10

    if-nez v10, :cond_4

    invoke-virtual {v8}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "zh_CN"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 78
    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    .line 81
    :cond_4
    iput-object v8, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 82
    invoke-virtual {v4, v0, v14}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 84
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/WakeUp;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f070002

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    .line 88
    .local v9, "wakeupCommands":[Ljava/lang/String;
    iput-object v3, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 89
    invoke-virtual {v4, v0, v14}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 91
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/svoice/WakeUp;->isH_Device()Z

    move-result v10

    if-eqz v10, :cond_8

    .line 92
    const v10, 0x7f0a088e

    invoke-virtual {p0, v10}, Lcom/samsung/helphub/pages/svoice/WakeUp;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 96
    .local v6, "tempForCall":Ljava/lang/String;
    :goto_1
    new-array v10, v13, [Ljava/lang/Object;

    aget-object v11, v9, v12

    aput-object v11, v10, v12

    invoke-static {v6, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 99
    .local v2, "guideCallString":Ljava/lang/String;
    iget-object v10, p0, Lcom/samsung/helphub/pages/svoice/WakeUp;->view1:Landroid/widget/TextView;

    if-eqz v10, :cond_5

    .line 100
    iget-object v10, p0, Lcom/samsung/helphub/pages/svoice/WakeUp;->view1:Landroid/widget/TextView;

    invoke-virtual {v10, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    :cond_5
    return-void

    .line 56
    .end local v2    # "guideCallString":Ljava/lang/String;
    .end local v6    # "tempForCall":Ljava/lang/String;
    .end local v8    # "voicetalkLocale":Ljava/util/Locale;
    .end local v9    # "wakeupCommands":[Ljava/lang/String;
    :cond_6
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 59
    :cond_7
    const-string v10, "v-es-LA"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 60
    const-string v7, "es"

    goto/16 :goto_0

    .line 94
    .restart local v8    # "voicetalkLocale":Ljava/util/Locale;
    .restart local v9    # "wakeupCommands":[Ljava/lang/String;
    :cond_8
    const v10, 0x7f0a088f

    invoke-virtual {p0, v10}, Lcom/samsung/helphub/pages/svoice/WakeUp;->getString(I)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "tempForCall":Ljava/lang/String;
    goto :goto_1
.end method
