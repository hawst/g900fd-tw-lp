.class public Lcom/samsung/helphub/pages/settings/BluetoothSetup;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "BluetoothSetup.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BluetoothSetup"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 20
    new-instance v0, Lcom/samsung/helphub/pages/settings/BluetoothSetup$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/settings/BluetoothSetup$1;-><init>(Lcom/samsung/helphub/pages/settings/BluetoothSetup;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/settings/BluetoothSetup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/settings/BluetoothSetup;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/settings/BluetoothSetup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/settings/BluetoothSetup;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup;->isClicked:Z

    return p1
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/helphub/pages/settings/BluetoothSetup;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onResume()V

    .line 65
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/settings/BluetoothSetup;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0a02ab

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 68
    :cond_0
    return-void
.end method
