.class Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage$1;
.super Ljava/lang/Object;
.source "DownloadBoosterPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage;)V
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage$1;->this$0:Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 18
    iget-object v1, p0, Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage$1;->this$0:Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage;

    # getter for: Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage;->isClicked:Z
    invoke-static {v1}, Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage;->access$000(Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 28
    :goto_0
    return-void

    .line 20
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage$1;->this$0:Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage;

    # setter for: Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage;->isClicked:Z
    invoke-static {v1, v3}, Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage;->access$102(Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage;Z)Z

    .line 21
    const-string v1, "DownloadBoosterPage"

    const-string v2, "onClick() is called"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 23
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.Settings$SmartBondingSettingsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 24
    const-string v1, "fromHelp"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 26
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 27
    iget-object v1, p0, Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage$1;->this$0:Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/downloadbooster/DownloadBoosterPage;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
