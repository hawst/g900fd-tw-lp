.class Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$3;
.super Ljava/lang/Object;
.source "PalmMotionToCaptureScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->makeTalkBackDisablePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$3;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x1

    .line 109
    iget-object v0, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$3;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;

    invoke-virtual {v0}, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/helphub/utility/TackBackUtility;->turnOffTalkBack(Landroid/content/Context;)Z

    .line 110
    iget-object v0, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$3;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;

    invoke-virtual {v0}, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v2, "SEC_FLOATING_FEATURE_SETTINGS_MOTION_CONCEPT_2013"

    invoke-virtual {v0, v2}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "surface_motion_engine"

    :goto_0
    invoke-static {v1, v0, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 111
    iget-object v0, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$3;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;

    invoke-virtual {v0}, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "surface_palm_swipe"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 112
    iget-object v0, p0, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen$3;->this$0:Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;

    invoke-virtual {v0}, Lcom/samsung/helphub/pages/motion/PalmMotionToCaptureScreen;->startTutorial()V

    .line 113
    return-void

    .line 110
    :cond_0
    const-string v0, "master_motion"

    goto :goto_0
.end method
