.class public Lcom/samsung/helphub/pages/multiwindow/MultiWindowOpeningByRecents;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "MultiWindowOpeningByRecents.java"


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field mMultiWindowHelpUtil:Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 50
    new-instance v0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowOpeningByRecents$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowOpeningByRecents$1;-><init>(Lcom/samsung/helphub/pages/multiwindow/MultiWindowOpeningByRecents;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowOpeningByRecents;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowOpeningByRecents;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    new-instance v3, Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowOpeningByRecents;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowOpeningByRecents;->mMultiWindowHelpUtil:Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

    .line 38
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 39
    .local v2, "result":Landroid/view/View;
    const v3, 0x7f0d03a2

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 40
    .local v0, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowOpeningByRecents;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "multi_window_opening_by_recents"

    invoke-static {v3, v0, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 42
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 43
    const v3, 0x7f0d039e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 44
    .local v1, "mImageView2":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 45
    const v3, 0x7f0202aa

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 47
    .end local v1    # "mImageView2":Landroid/widget/ImageView;
    :cond_0
    return-object v2
.end method
