.class public Lcom/samsung/helphub/pages/multiwindow/MultiWindowView;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "MultiWindowView.java"


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field mMultiWindowHelpUtil:Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 70
    new-instance v0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowView$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowView$1;-><init>(Lcom/samsung/helphub/pages/multiwindow/MultiWindowView;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowView;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private static checkFunctionStateDisable(Ljava/util/List;Ljava/lang/String;)Z
    .locals 1
    .param p1, "disableFunction"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 123
    .local p0, "disableFunctionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    const/4 v0, 0x1

    .line 126
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isMultiWindowCSCFeatureDisableCheck(Ljava/lang/String;)Z
    .locals 9
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 101
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 102
    .local v3, "mTrayBarEnabledFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 103
    .local v2, "mTrayBarDisabledFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v7

    const-string v8, "CscFeature_Framework_ConfigMultiWindowTrayBarFunction"

    invoke-virtual {v7, v8}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 104
    .local v4, "traybarFunction":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 105
    new-instance v5, Ljava/util/ArrayList;

    const-string v7, "\\,"

    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 106
    .local v5, "traybarFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 107
    .local v0, "function":Ljava/lang/String;
    const-string v7, "+"

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 108
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109
    :cond_1
    const-string v7, "-"

    invoke-virtual {v0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 110
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 115
    .end local v0    # "function":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v5    # "traybarFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    invoke-static {v2, p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowView;->checkFunctionStateDisable(Ljava/util/List;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 118
    :goto_1
    return v6

    :cond_3
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private isSupportDragAndDrop()Z
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowView;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v8, 0x7f0d03a2

    const/16 v7, 0x8

    .line 41
    new-instance v5, Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowView;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/samsung/helphub/pages/multiwindow/MultiWindowView;->mMultiWindowHelpUtil:Lcom/samsung/helphub/pages/multiwindow/MultiWindowHelpUtil;

    .line 42
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v4

    .line 43
    .local v4, "result":Landroid/view/View;
    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 44
    .local v1, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowView;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "multi_window_view"

    invoke-static {v5, v1, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 46
    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 47
    .local v2, "mImageViewTitle":Landroid/widget/ImageView;
    const v5, 0x7f0d038b

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 48
    .local v3, "mSwitchAppLayout":Landroid/widget/RelativeLayout;
    const v5, 0x7f0d0390

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 50
    .local v0, "mDragLayout":Landroid/widget/RelativeLayout;
    const-string v5, "AppSwitching"

    invoke-static {v5}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowView;->isMultiWindowCSCFeatureDisableCheck(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 51
    invoke-direct {p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowView;->isSupportDragAndDrop()Z

    move-result v5

    if-nez v5, :cond_0

    .line 52
    const v5, 0x7f0202a0

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 53
    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 54
    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 67
    :goto_0
    return-object v4

    .line 56
    :cond_0
    const v5, 0x7f02029f

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 57
    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 60
    :cond_1
    invoke-direct {p0}, Lcom/samsung/helphub/pages/multiwindow/MultiWindowView;->isSupportDragAndDrop()Z

    move-result v5

    if-nez v5, :cond_2

    .line 61
    const v5, 0x7f0202a3

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 62
    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 64
    :cond_2
    const v5, 0x7f02029e

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
