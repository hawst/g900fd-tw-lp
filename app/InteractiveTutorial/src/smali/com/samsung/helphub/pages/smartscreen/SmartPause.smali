.class public Lcom/samsung/helphub/pages/smartscreen/SmartPause;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "SmartPause.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# instance fields
.field public mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    .line 127
    new-instance v0, Lcom/samsung/helphub/pages/smartscreen/SmartPause$2;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause$2;-><init>(Lcom/samsung/helphub/pages/smartscreen/SmartPause;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/smartscreen/SmartPause;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/smartscreen/SmartPause;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/smartscreen/SmartPause;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/smartscreen/SmartPause;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/smartscreen/SmartPause;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/smartscreen/SmartPause;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 25
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->showEnableSettingDialog(II)V

    return-void
.end method

.method private sendBroadcastForNetworkErrorPopup(I)V
    .locals 3
    .param p1, "status"    # I

    .prologue
    .line 120
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 121
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.popupuireceiver"

    const-string v2, "com.sec.android.app.popupuireceiver.popupNetworkError"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 123
    const-string v1, "network_err_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 124
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->startActivityForResult(Landroid/content/Intent;I)V

    .line 125
    return-void
.end method


# virtual methods
.method public checkNetworkState()V
    .locals 6

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 99
    new-instance v1, Lcom/samsung/helphub/UpdateCheckThread;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->mHandler:Landroid/os/Handler;

    const-string v5, "com.samsung.helpplugin"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/samsung/helphub/UpdateCheckThread;-><init>(Landroid/content/Context;ZLandroid/os/Handler;Ljava/lang/String;)V

    .line 101
    .local v1, "thread":Lcom/samsung/helphub/UpdateCheckThread;
    invoke-virtual {v1}, Lcom/samsung/helphub/UpdateCheckThread;->start()V

    .line 117
    .end local v1    # "thread":Lcom/samsung/helphub/UpdateCheckThread;
    :goto_0
    return-void

    .line 103
    :cond_0
    const/4 v0, -0x1

    .line 104
    .local v0, "networkStatus":I
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isFligtMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 105
    const/4 v0, 0x0

    .line 115
    :goto_1
    invoke-direct {p0, v0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->sendBroadcastForNetworkErrorPopup(I)V

    goto :goto_0

    .line 106
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isMobileDataOff(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 107
    const/4 v0, 0x1

    goto :goto_1

    .line 108
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isRoamingOff(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 109
    const/4 v0, 0x2

    goto :goto_1

    .line 110
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isReachToDataLimit(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 111
    const/4 v0, 0x3

    goto :goto_1

    .line 113
    :cond_4
    const/4 v0, 0x4

    goto :goto_1
.end method

.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 56
    return-object p0
.end method

.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/samsung/helphub/pages/smartscreen/SmartPause$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause$1;-><init>(Lcom/samsung/helphub/pages/smartscreen/SmartPause;)V

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->isClicked:Z

    .line 52
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 41
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "smart_pause"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 42
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.SMART_PAUSE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 43
    .local v0, "setting_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 44
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 45
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->startTutorial()V

    .line 47
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 29
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 30
    .local v1, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    const v2, 0x7f0d03de

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 33
    .local v0, "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "tutorial_smart_screen_pause"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 36
    .end local v0    # "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_0
    return-object v1
.end method

.method protected startTutorial()V
    .locals 6

    .prologue
    .line 81
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.START_HELP_VIDEO_SMART_PAUSE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 82
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 83
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "video_help"

    const-string v4, "raw"

    const-string v5, "com.samsung.helpplugin"

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/helphub/HelpHubCommon;->getResourceId(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 84
    .local v1, "resId":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 85
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "android.resource://com.samsung.helpplugin/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "video/mp4"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->startActivity(Landroid/content/Intent;)V

    .line 95
    .end local v1    # "resId":I
    :goto_0
    return-void

    .line 88
    .restart local v1    # "resId":I
    :cond_0
    const-string v2, "The Video resource not downloaded yet"

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->LogD(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    .end local v1    # "resId":I
    :cond_1
    const-string v2, "file:///system/media/video/video_help.mp4"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "video/mp4"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/smartscreen/SmartPause;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
