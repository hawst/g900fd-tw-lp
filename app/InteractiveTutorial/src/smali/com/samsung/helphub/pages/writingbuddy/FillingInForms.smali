.class public Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "FillingInForms.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;->showEnableSettingDialog(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;->showEnableSettingDialog(ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 70
    return-object p0
.end method

.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/samsung/helphub/pages/writingbuddy/FillingInForms$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/writingbuddy/FillingInForms$1;-><init>(Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;)V

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;->isClicked:Z

    .line 62
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 3

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/helphub/utility/TackBackUtility;->turnOffTalkBack(Landroid/content/Context;)Z

    .line 42
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accessibility_display_magnification_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 44
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "pen_writing_buddy"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 48
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/writingbuddy/FillingInForms;->startTutorial()V

    .line 50
    return-void
.end method

.method protected startTutorial()V
    .locals 0

    .prologue
    .line 143
    return-void
.end method
