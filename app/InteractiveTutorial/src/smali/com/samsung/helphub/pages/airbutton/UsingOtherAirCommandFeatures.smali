.class public Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "UsingOtherAirCommandFeatures.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# instance fields
.field clickedbutton:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->clickedbutton:I

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->isClicked:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->showEnableSettingDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->isClicked:Z

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->isClicked:Z

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->showEnableSettingDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->isClicked:Z

    return v0
.end method

.method static synthetic access$802(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->isClicked:Z

    return p1
.end method

.method static synthetic access$900(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->showEnableSettingDialog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private turnOnPenInfoPreview()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 132
    const-string v6, "DCM"

    const-string v7, "ro.csc.sales_code"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "KDI"

    const-string v7, "ro.csc.sales_code"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "XJP"

    const-string v7, "ro.csc.sales_code"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 133
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "air_view_master_onoff"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_5

    move v1, v4

    .line 134
    .local v1, "airViewState":Z
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "air_view_mode"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 135
    .local v0, "airViewMode":I
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "pen_hovering"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_6

    move v2, v4

    .line 136
    .local v2, "penHover":Z
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "pen_hovering_information_preview"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_7

    move v3, v4

    .line 137
    .local v3, "penInforPreview":Z
    :goto_2
    if-nez v1, :cond_1

    .line 139
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "air_view_master_onoff"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 141
    :cond_1
    if-ne v0, v4, :cond_2

    .line 142
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "air_view_mode"

    const/4 v7, 0x2

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 143
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "pen_hovering"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 144
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "finger_air_view"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 146
    :cond_2
    if-nez v2, :cond_3

    .line 147
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "pen_hovering"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 149
    :cond_3
    if-nez v3, :cond_4

    .line 150
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "pen_hovering_information_preview"

    invoke-static {v5, v6, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 153
    .end local v0    # "airViewMode":I
    .end local v1    # "airViewState":Z
    .end local v2    # "penHover":Z
    .end local v3    # "penInforPreview":Z
    :cond_4
    return-void

    :cond_5
    move v1, v5

    .line 133
    goto/16 :goto_0

    .restart local v0    # "airViewMode":I
    .restart local v1    # "airViewState":Z
    :cond_6
    move v2, v5

    .line 135
    goto :goto_1

    .restart local v2    # "penHover":Z
    :cond_7
    move v3, v5

    .line 136
    goto :goto_2
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 196
    return-object p0
.end method

.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$3;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$3;-><init>(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;)V

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->isClicked:Z

    .line 192
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 160
    const-string v1, "DCM"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "KDI"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "XJP"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 161
    :cond_0
    invoke-direct {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->turnOnPenInfoPreview()V

    .line 165
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "air_button_onoff"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 167
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.gesture.AIR_VIEW_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 168
    .local v0, "air_view_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 169
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 170
    iget v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->clickedbutton:I

    packed-switch v1, :pswitch_data_0

    .line 185
    :goto_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->clickedbutton:I

    .line 187
    return-void

    .line 173
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->startTutorial()V

    goto :goto_0

    .line 177
    :pswitch_1
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->startTutorialInsertingContent()V

    goto :goto_0

    .line 181
    :pswitch_2
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->startTutorialAddingRecipients()V

    goto :goto_0

    .line 170
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v7, 0x8

    .line 29
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v4

    .line 31
    .local v4, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 32
    const v5, 0x7f0d01fb

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 34
    .local v1, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "air_button_infopreview_introduction_2"

    invoke-static {v5, v1, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 36
    const v5, 0x7f0d0217

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 38
    .local v2, "mImageView2":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "air_button_infopreview_introduction_3"

    invoke-static {v5, v2, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 40
    const v5, 0x7f0d0219

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 42
    .local v3, "mImageView3":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "air_button_infopreview_introduction_4"

    invoke-static {v5, v3, v6}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 45
    .end local v1    # "mImageView":Landroid/widget/ImageView;
    .end local v2    # "mImageView2":Landroid/widget/ImageView;
    .end local v3    # "mImageView3":Landroid/widget/ImageView;
    :cond_0
    const v5, 0x7f0d0216

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 46
    .local v0, "button":Landroid/widget/Button;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->isMessageHeaderHidden(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->isDCMModel()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 47
    :cond_1
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 49
    :cond_2
    new-instance v5, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;

    invoke-direct {v5, p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$1;-><init>(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;)V

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    const v5, 0x7f0d0218

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "button":Landroid/widget/Button;
    check-cast v0, Landroid/widget/Button;

    .line 74
    .restart local v0    # "button":Landroid/widget/Button;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->isMessageHeaderHidden(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 75
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 77
    :cond_3
    new-instance v5, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$2;

    invoke-direct {v5, p0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures$2;-><init>(Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;)V

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    return-object v4
.end method

.method protected startTutorial()V
    .locals 3

    .prologue
    .line 213
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 214
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "image/jpg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    const-string v1, "com.sec.android.gallery3d"

    const-string v2, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 216
    const-string v1, "IsHelpMode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 217
    const-string v1, "HelpMode"

    const-string v2, "AIR_BUTTON"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->startActivity(Landroid/content/Intent;)V

    .line 219
    return-void
.end method

.method protected startTutorialAddingRecipients()V
    .locals 3

    .prologue
    .line 206
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.mms.help.AirButtonMainActivity"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 207
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "helpContactTutorial"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 208
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->startActivity(Landroid/content/Intent;)V

    .line 209
    return-void
.end method

.method protected startTutorialInsertingContent()V
    .locals 3

    .prologue
    .line 200
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.mms.help.AirButtonMainActivity"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 201
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "helpAttachTutorial"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 202
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/airbutton/UsingOtherAirCommandFeatures;->startActivity(Landroid/content/Intent;)V

    .line 203
    return-void
.end method
