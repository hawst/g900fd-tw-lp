.class public Lcom/samsung/helphub/pages/motion/TurnOverToMute;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "TurnOverToMute.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# instance fields
.field mUseRingDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/motion/TurnOverToMute;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/motion/TurnOverToMute;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/motion/TurnOverToMute;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/motion/TurnOverToMute;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->isClicked:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/motion/TurnOverToMute;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/motion/TurnOverToMute;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->showEnableSettingDialog(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/motion/TurnOverToMute;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/motion/TurnOverToMute;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->startTutorial(Z)V

    return-void
.end method

.method private startTutorial(Z)V
    .locals 3
    .param p1, "useRing"    # Z

    .prologue
    .line 84
    iget-object v1, p0, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->mUseRingDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->mUseRingDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 87
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 88
    .local v0, "mIntent":Landroid/content/Intent;
    if-eqz p1, :cond_1

    .line 89
    const-string v1, "UseRing"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 91
    :cond_1
    const-string v1, "com.android.phone"

    const-string v2, "com.android.phone.callsettings.OverturnTutorialIncallScreen"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->startActivity(Landroid/content/Intent;)V

    .line 93
    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 59
    return-object p0
.end method

.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcom/samsung/helphub/pages/motion/TurnOverToMute$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute$1;-><init>(Lcom/samsung/helphub/pages/motion/TurnOverToMute;)V

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->isClicked:Z

    .line 55
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 40
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "master_motion"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 42
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "motion_engine"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 43
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "motion_overturn"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 44
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.motions.MOTIONS_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 45
    .local v0, "motion_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 46
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 48
    .end local v0    # "motion_changed":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->startTutorial()V

    .line 49
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 29
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 30
    .local v1, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    const v2, 0x7f0d0386

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 33
    .local v0, "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "motion_turnover"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 35
    .end local v0    # "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_0
    return-object v1
.end method

.method protected startTutorial()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 97
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 98
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v1

    if-ne v1, v3, :cond_0

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a02da

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a02d8

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0064

    new-instance v3, Lcom/samsung/helphub/pages/motion/TurnOverToMute$4;

    invoke-direct {v3, p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute$4;-><init>(Lcom/samsung/helphub/pages/motion/TurnOverToMute;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a02d9

    new-instance v3, Lcom/samsung/helphub/pages/motion/TurnOverToMute$3;

    invoke-direct {v3, p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute$3;-><init>(Lcom/samsung/helphub/pages/motion/TurnOverToMute;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/helphub/pages/motion/TurnOverToMute$2;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/pages/motion/TurnOverToMute$2;-><init>(Lcom/samsung/helphub/pages/motion/TurnOverToMute;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->mUseRingDialog:Landroid/app/AlertDialog;

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/helphub/pages/motion/TurnOverToMute;->startTutorial(Z)V

    goto :goto_0
.end method
