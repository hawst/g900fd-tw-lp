.class public Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "UsingFeaturesInAirCommand.java"


# static fields
.field private static final FEATURE_OVERLAYMAGNIFIER:Ljava/lang/String; = "com.sec.feature.overlaymagnifier"

.field private static final URI_AIR_BUTTON_ONOFF:Ljava/lang/String; = "air_button_onoff"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mEasyClipHelpIntent:Landroid/content/Intent;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mPenableHelpIntent:Landroid/content/Intent;

.field private mTalkbackDisableDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mTalkbackDisableDialog:Landroid/app/AlertDialog;

    .line 51
    new-instance v0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$1;-><init>(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->showTalkBackDisableDialog(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mPenableHelpIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mEasyClipHelpIntent:Landroid/content/Intent;

    return-object v0
.end method

.method private dismissAllDialog()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mTalkbackDisableDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mTalkbackDisableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mTalkbackDisableDialog:Landroid/app/AlertDialog;

    .line 140
    :cond_0
    return-void
.end method

.method private showTalkBackDisableDialog(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->dismissAllDialog()V

    .line 85
    move-object v2, p1

    .line 86
    .local v2, "view":Landroid/view/View;
    const v3, 0x7f0a07a6

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const v6, 0x7f0a076c

    invoke-virtual {p0, v6}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, "title":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0a07a7

    invoke-virtual {p0, v4}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "message":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v3

    if-nez v3, :cond_0

    .line 90
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0a0573

    invoke-virtual {p0, v4}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 92
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0a0574

    invoke-virtual {p0, v4}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 93
    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mActivity:Landroid/app/Activity;

    const-string v4, "com.sec.feature.overlaymagnifier"

    invoke-static {v3, v4}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 94
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n- "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0a0575

    invoke-virtual {p0, v4}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 97
    :cond_1
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mActivity:Landroid/app/Activity;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x104000a

    new-instance v5, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$3;

    invoke-direct {v5, p0, v2}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$3;-><init>(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;Landroid/view/View;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/high16 v4, 0x1040000

    new-instance v5, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$2;

    invoke-direct {v5, p0}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$2;-><init>(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mTalkbackDisableDialog:Landroid/app/AlertDialog;

    .line 126
    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mTalkbackDisableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 127
    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mTalkbackDisableDialog:Landroid/app/AlertDialog;

    new-instance v4, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$4;

    invoke-direct {v4, p0}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand$4;-><init>(Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 133
    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v4, 0x10808000

    .line 34
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 36
    .local v0, "result":Landroid/view/View;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mPenableHelpIntent:Landroid/content/Intent;

    .line 37
    iget-object v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mPenableHelpIntent:Landroid/content/Intent;

    const-string v2, "com.sec.android.app.SmartClipService"

    const-string v3, "com.sec.android.app.SmartClipService.help.PenAbleTutorialActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    iget-object v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mPenableHelpIntent:Landroid/content/Intent;

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 41
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mEasyClipHelpIntent:Landroid/content/Intent;

    .line 42
    iget-object v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mEasyClipHelpIntent:Landroid/content/Intent;

    const-string v2, "com.sec.android.app.SmartClipService"

    const-string v3, "com.sec.android.app.SmartClipService.help.EasyClipTutorialActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    iget-object v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mEasyClipHelpIntent:Landroid/content/Intent;

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 46
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/pages/airbutton/UsingFeaturesInAirCommand;->mActivity:Landroid/app/Activity;

    .line 48
    return-object v0
.end method
