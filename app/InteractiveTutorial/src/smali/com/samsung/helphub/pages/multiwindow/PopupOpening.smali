.class public Lcom/samsung/helphub/pages/multiwindow/PopupOpening;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "PopupOpening.java"


# static fields
.field public static final mProductName:Ljava/lang/String;


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/helphub/pages/multiwindow/PopupOpening;->mProductName:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 51
    new-instance v0, Lcom/samsung/helphub/pages/multiwindow/PopupOpening$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/multiwindow/PopupOpening$1;-><init>(Lcom/samsung/helphub/pages/multiwindow/PopupOpening;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/multiwindow/PopupOpening;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/helphub/pages/multiwindow/PopupOpening;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 36
    .local v1, "result":Landroid/view/View;
    const v3, 0x7f0d03a2

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 37
    .local v0, "mImageView":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/multiwindow/PopupOpening;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "popup_opening"

    invoke-static {v3, v0, v4}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 39
    sget-object v3, Lcom/samsung/helphub/pages/multiwindow/PopupOpening;->mProductName:Ljava/lang/String;

    if-eqz v3, :cond_1

    sget-object v3, Lcom/samsung/helphub/pages/multiwindow/PopupOpening;->mProductName:Ljava/lang/String;

    const-string v4, "tblte"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/samsung/helphub/pages/multiwindow/PopupOpening;->mProductName:Ljava/lang/String;

    const-string v4, "tbelte"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/samsung/helphub/pages/multiwindow/PopupOpening;->mProductName:Ljava/lang/String;

    const-string v4, "tbhplte"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/samsung/helphub/pages/multiwindow/PopupOpening;->mProductName:Ljava/lang/String;

    const-string v4, "tb3g"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/samsung/helphub/pages/multiwindow/PopupOpening;->mProductName:Ljava/lang/String;

    const-string v4, "tbwifi"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/samsung/helphub/pages/multiwindow/PopupOpening;->mProductName:Ljava/lang/String;

    const-string v4, "tbewifi"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 42
    :cond_0
    const/4 v2, 0x0

    .line 43
    .local v2, "tv":Landroid/widget/TextView;
    const v3, 0x7f0d003e

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "tv":Landroid/widget/TextView;
    check-cast v2, Landroid/widget/TextView;

    .line 44
    .restart local v2    # "tv":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 45
    const v3, 0x7f0a00de

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 48
    .end local v2    # "tv":Landroid/widget/TextView;
    :cond_1
    return-object v1
.end method
