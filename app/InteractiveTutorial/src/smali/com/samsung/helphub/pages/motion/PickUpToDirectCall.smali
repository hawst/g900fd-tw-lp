.class public Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;
.super Lcom/samsung/helphub/pages/HelpPageCheckSetting;
.source "PickUpToDirectCall.java"

# interfaces
.implements Lcom/samsung/helphub/pages/IEnableSettingDialogListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;->isClicked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;->isClicked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;->showEnableSettingDialog(II)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;->makeDisablePopup(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected getSettingDialogButtonListener()Lcom/samsung/helphub/pages/IEnableSettingDialogListener;
    .locals 0

    .prologue
    .line 53
    return-object p0
.end method

.method protected getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall$1;-><init>(Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;)V

    return-object v0
.end method

.method public onClickEnableSettingsButtonCancel()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;->isClicked:Z

    .line 49
    return-void
.end method

.method public onClickEnableSettingsButtonOK()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 35
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "master_motion"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 36
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "motion_engine"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 37
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "motion_pick_up_to_call_out"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 38
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.motions.MOTIONS_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 39
    .local v0, "motion_changed":Landroid/content/Intent;
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 40
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 42
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;->startTutorial()V

    .line 43
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageCheckSetting;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 24
    .local v1, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 25
    const v2, 0x7f0d0382

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .line 27
    .local v0, "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "motion_pick_up_direct_call"

    invoke-static {v2, v0, v3}, Lcom/samsung/helphub/HelpHubCommon;->setImageViewer(Landroid/content/Context;Lcom/samsung/helphub/widget/HelpHubImageViewer;Ljava/lang/String;)V

    .line 29
    .end local v0    # "mImageViewer":Lcom/samsung/helphub/widget/HelpHubImageViewer;
    :cond_0
    return-object v1
.end method

.method protected startTutorial()V
    .locals 2

    .prologue
    .line 84
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 85
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.mms.ui.DirectCallTutorial"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/pages/motion/PickUpToDirectCall;->startActivity(Landroid/content/Intent;)V

    .line 87
    return-void
.end method
