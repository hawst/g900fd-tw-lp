.class Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy$1;
.super Ljava/lang/Object;
.source "GalleryViewingPictureResizingThumbnailsEasy.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 24
    const-string v1, "com.sec.android.gallery3d"

    .line 26
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;

    iget-object v3, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->access$000(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 27
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;

    # getter for: Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->access$100(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 40
    :goto_0
    return-void

    .line 29
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;

    # setter for: Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->isClicked:Z
    invoke-static {v2, v4}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->access$202(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;Z)Z

    .line 30
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 31
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "image/jpg"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 32
    const-string v2, "com.sec.android.gallery3d"

    const-string v3, "com.sec.android.gallery3d.app.Gallery"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 33
    const-string v2, "IsHelpMode"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 34
    const-string v2, "HelpMode"

    const-string v3, "VIEWING_PICTURES"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 38
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy$1;->this$0:Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;

    # invokes: Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;->access$300(Lcom/samsung/helphub/pages/multimedia/GalleryViewingPictureResizingThumbnailsEasy;Ljava/lang/String;)V

    goto :goto_0
.end method
