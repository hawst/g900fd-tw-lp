.class public Lcom/samsung/helphub/pages/online/OnlineHelpPage;
.super Lcom/samsung/helphub/pages/online/WebFragmentBase;
.source "OnlineHelpPage.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/samsung/helphub/pages/online/WebFragmentBase;-><init>()V

    return-void
.end method

.method public static readSalesCode()Ljava/lang/String;
    .locals 4

    .prologue
    .line 45
    const-string v1, ""

    .line 47
    .local v1, "sales_code":Ljava/lang/String;
    :try_start_0
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 48
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 49
    const-string v2, "ril.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 54
    :cond_0
    :goto_0
    return-object v1

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "OnlineHelpPage"

    const-string v3, "readSalesCode failed"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected getUrl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 17
    iget-boolean v2, p0, Lcom/samsung/helphub/pages/online/OnlineHelpPage;->mTwoPane:Z

    if-nez v2, :cond_0

    .line 18
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/OnlineHelpPage;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const v3, 0x7f0a05b4

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(I)V

    .line 22
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/pages/online/OnlineHelpPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a06b1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 24
    .local v0, "outUrl":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/helphub/pages/online/OnlineHelpPage;->readSalesCode()Ljava/lang/String;

    move-result-object v1

    .line 25
    .local v1, "sales_code":Ljava/lang/String;
    const-string v2, "CHC"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 26
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "http://www.samsung.com/m-manual/chc"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 37
    :cond_1
    :goto_0
    return-object v0

    .line 27
    :cond_2
    const-string v2, "CHM"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 28
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "http://www.samsung.com/m-manual/cmcc"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 29
    :cond_3
    const-string v2, "CHU"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 30
    const-string v0, "http://www.samsung.com/m-manual/chinaunicom"

    goto :goto_0

    .line 31
    :cond_4
    const-string v2, "CHN"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 32
    const-string v0, "http://www.samsung.com/m-manual/chn"

    goto :goto_0

    .line 33
    :cond_5
    const-string v2, "CTC"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 34
    const-string v0, "http://www.samsung.com/m-manual/ctc"

    goto :goto_0
.end method
