.class Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;
.super Ljava/lang/Object;
.source "DisplayFunctionalMenu.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;)V
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;->this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v10, 0x7f0a076c

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 20
    const-string v1, "com.sec.android.gallery3d"

    .line 21
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;->this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    iget-object v5, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;->this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    invoke-virtual {v5}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->getActivity()Landroid/app/Activity;

    move-result-object v5

    # invokes: Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v4, v5, v1}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->access$000(Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 22
    iget-object v4, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;->this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    invoke-virtual {v4}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "air_button_onoff"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_0

    move v0, v2

    .line 23
    .local v0, "isAirButtonEnabled":Z
    :goto_0
    iget-object v4, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;->this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    # getter for: Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->isClicked:Z
    invoke-static {v4}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->access$100(Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 37
    .end local v0    # "isAirButtonEnabled":Z
    :goto_1
    return-void

    :cond_0
    move v0, v3

    .line 22
    goto :goto_0

    .line 25
    .restart local v0    # "isAirButtonEnabled":Z
    :cond_1
    iget-object v4, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;->this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    # setter for: Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->isClicked:Z
    invoke-static {v4, v2}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->access$202(Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;Z)Z

    .line 26
    if-nez v0, :cond_2

    .line 27
    iget-object v4, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;->this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    iget-object v5, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;->this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    const v6, 0x7f0a07c6

    new-array v7, v2, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;->this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    invoke-virtual {v8, v10}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-virtual {v5, v6, v7}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;->this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    const v7, 0x7f0a078a

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;->this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    invoke-virtual {v9, v10}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v3

    iget-object v3, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;->this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    const v9, 0x7f0a0771

    invoke-virtual {v3, v9}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v8, v2

    invoke-virtual {v6, v7, v8}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->showEnableSettingDialog(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v4, v5, v2}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->access$300(Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 31
    :cond_2
    iget-object v2, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;->this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->startTutorial()V

    goto :goto_1

    .line 35
    .end local v0    # "isAirButtonEnabled":Z
    :cond_3
    iget-object v2, p0, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu$1;->this$0:Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;

    # invokes: Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;->access$400(Lcom/samsung/helphub/pages/airbutton/DisplayFunctionalMenu;Ljava/lang/String;)V

    goto :goto_1
.end method
