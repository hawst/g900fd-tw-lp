.class Lcom/samsung/helphub/pages/multimedia/CameraChangingMode$1;
.super Ljava/lang/Object;
.source "CameraChangingMode.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/multimedia/CameraChangingMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/multimedia/CameraChangingMode;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/multimedia/CameraChangingMode;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/samsung/helphub/pages/multimedia/CameraChangingMode$1;->this$0:Lcom/samsung/helphub/pages/multimedia/CameraChangingMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 51
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 53
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 67
    :goto_0
    return-void

    .line 55
    :pswitch_0
    const-string v1, "com.sec.android.app.camera"

    const-string v2, "com.sec.android.app.camera.Camera"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    const-string v1, "help_select_camera_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 57
    iget-object v1, p0, Lcom/samsung/helphub/pages/multimedia/CameraChangingMode$1;->this$0:Lcom/samsung/helphub/pages/multimedia/CameraChangingMode;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/multimedia/CameraChangingMode;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 60
    :pswitch_1
    const-string v1, "com.sec.android.app.camera"

    const-string v2, "com.sec.android.app.camera.Camera"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    const-string v1, "help_learn_camera_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 62
    iget-object v1, p0, Lcom/samsung/helphub/pages/multimedia/CameraChangingMode$1;->this$0:Lcom/samsung/helphub/pages/multimedia/CameraChangingMode;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/pages/multimedia/CameraChangingMode;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 53
    :pswitch_data_0
    .packed-switch 0x7f0d027e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
