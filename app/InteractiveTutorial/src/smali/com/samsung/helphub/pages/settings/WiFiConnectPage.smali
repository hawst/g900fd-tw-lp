.class public Lcom/samsung/helphub/pages/settings/WiFiConnectPage;
.super Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;
.source "WiFiConnectPage.java"


# instance fields
.field private mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;-><init>()V

    .line 32
    new-instance v0, Lcom/samsung/helphub/pages/settings/WiFiConnectPage$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/pages/settings/WiFiConnectPage$1;-><init>(Lcom/samsung/helphub/pages/settings/WiFiConnectPage;)V

    iput-object v0, p0, Lcom/samsung/helphub/pages/settings/WiFiConnectPage;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/pages/settings/WiFiConnectPage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/pages/settings/WiFiConnectPage;

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/samsung/helphub/pages/settings/WiFiConnectPage;->isClicked:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/pages/settings/WiFiConnectPage;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/pages/settings/WiFiConnectPage;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/samsung/helphub/pages/settings/WiFiConnectPage;->isClicked:Z

    return p1
.end method


# virtual methods
.method public final getTryItOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/helphub/pages/settings/WiFiConnectPage;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/pages/HelpPageStartNowTryIt;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 24
    .local v0, "result":Landroid/view/View;
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 26
    const v2, 0x7f0d008d

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 27
    .local v1, "tv":Landroid/widget/TextView;
    const v2, 0x7f0a00ed

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 29
    .end local v1    # "tv":Landroid/widget/TextView;
    :cond_0
    return-object v0
.end method
