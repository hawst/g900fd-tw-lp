.class Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart$1;
.super Ljava/lang/Object;
.source "CreateBarChart.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;)V
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart$1;->this$0:Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 14
    const-string v1, "com.samsung.android.chartbuilder"

    .line 15
    .local v1, "pkgName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart$1;->this$0:Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;

    iget-object v3, p0, Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart$1;->this$0:Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;

    invoke-virtual {v3}, Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    invoke-static {v2, v3, v1}, Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;->access$000(Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 16
    iget-object v2, p0, Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart$1;->this$0:Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;

    # getter for: Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;->access$100(Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    :goto_0
    return-void

    .line 20
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart$1;->this$0:Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;->isClicked:Z
    invoke-static {v2, v3}, Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;->access$202(Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;Z)Z

    .line 22
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.samsung.android.chartbuilder"

    const-string v4, "com.samsung.android.chartbuilder.help.HelperActivity"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 24
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v2, "chartbuilder:section"

    const-string v3, "create_bar_chart"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 26
    iget-object v2, p0, Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart$1;->this$0:Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 29
    .end local v0    # "mIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart$1;->this$0:Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;

    # invokes: Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;->makeDisablePopup(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;->access$300(Lcom/samsung/helphub/pages/chartbuilder/CreateBarChart;Ljava/lang/String;)V

    goto :goto_0
.end method
