.class Lcom/samsung/helphub/pages/smartscreen/SmartRotation$1;
.super Ljava/lang/Object;
.source "SmartRotation.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->getTryItOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/pages/smartscreen/SmartRotation;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/pages/smartscreen/SmartRotation;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/helphub/pages/smartscreen/SmartRotation$1;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartRotation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 55
    iget-object v2, p0, Lcom/samsung/helphub/pages/smartscreen/SmartRotation$1;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartRotation;

    invoke-virtual {v2}, Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "intelligent_rotation_mode"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 56
    .local v0, "settingStatus":Z
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/pages/smartscreen/SmartRotation$1;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartRotation;

    # getter for: Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->isClicked:Z
    invoke-static {v2}, Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->access$000(Lcom/samsung/helphub/pages/smartscreen/SmartRotation;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 66
    :goto_0
    return-void

    .line 58
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/pages/smartscreen/SmartRotation$1;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartRotation;

    # setter for: Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->isClicked:Z
    invoke-static {v2, v1}, Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->access$102(Lcom/samsung/helphub/pages/smartscreen/SmartRotation;Z)Z

    .line 59
    if-nez v0, :cond_2

    .line 60
    iget-object v1, p0, Lcom/samsung/helphub/pages/smartscreen/SmartRotation$1;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartRotation;

    const v2, 0x7f0a0579

    const v3, 0x7f0a057a

    # invokes: Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->showEnableSettingDialog(II)V
    invoke-static {v1, v2, v3}, Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->access$200(Lcom/samsung/helphub/pages/smartscreen/SmartRotation;II)V

    goto :goto_0

    .line 64
    :cond_2
    iget-object v1, p0, Lcom/samsung/helphub/pages/smartscreen/SmartRotation$1;->this$0:Lcom/samsung/helphub/pages/smartscreen/SmartRotation;

    invoke-virtual {v1}, Lcom/samsung/helphub/pages/smartscreen/SmartRotation;->startTutorial()V

    goto :goto_0
.end method
