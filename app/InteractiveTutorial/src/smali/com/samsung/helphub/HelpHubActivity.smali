.class public Lcom/samsung/helphub/HelpHubActivity;
.super Lcom/samsung/helphub/HelpHubActivityBase;
.source "HelpHubActivity.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/helphub/HelpHubActivityBase;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final BUNDLE_KEY_TARGET_ID:Ljava/lang/String; = "Search:target_id"

.field private static final HEADER_LOADER_ID:I

.field private static actionbarType:I


# instance fields
.field protected mBackStackListener:Landroid/app/FragmentManager$OnBackStackChangedListener;

.field private mHeaderList:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

.field private mItemNum:I

.field private mNewIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/samsung/helphub/HelpHubActivityBase;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/helphub/HelpHubActivity;->mItemNum:I

    .line 50
    new-instance v0, Lcom/samsung/helphub/HelpHubActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/HelpHubActivity$1;-><init>(Lcom/samsung/helphub/HelpHubActivity;)V

    iput-object v0, p0, Lcom/samsung/helphub/HelpHubActivity;->mBackStackListener:Landroid/app/FragmentManager$OnBackStackChangedListener;

    return-void
.end method

.method public static getAionbarType()I
    .locals 1

    .prologue
    .line 249
    sget v0, Lcom/samsung/helphub/HelpHubActivity;->actionbarType:I

    return v0
.end method

.method private handleSearchRequest(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 203
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/helphub/HelpHubActivity;->queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v1

    .line 204
    .local v1, "header":Lcom/samsung/helphub/headers/HelpHeader;
    const/4 v3, 0x0

    .line 205
    .local v3, "type":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    const/4 v2, 0x0

    .line 207
    .local v2, "targetID":I
    iput v6, p0, Lcom/samsung/helphub/HelpHubActivity;->mItemNum:I

    .line 209
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v4

    sget-object v5, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v4, v5, :cond_3

    .line 210
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v4

    const v5, 0x7f0d00a3

    if-ne v4, v5, :cond_1

    .line 211
    sget-object v3, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 212
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v2

    .line 233
    :cond_0
    :goto_0
    if-eqz v3, :cond_7

    .line 236
    new-instance v0, Landroid/os/Bundle;

    const/4 v4, 0x2

    invoke-direct {v0, v4}, Landroid/os/Bundle;-><init>(I)V

    .line 237
    .local v0, "args":Landroid/os/Bundle;
    const-string v4, "Search:target_id"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 238
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/app/LoaderManager;->getLoader(I)Landroid/content/Loader;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 239
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v4

    invoke-virtual {v4, v6, v0, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 246
    .end local v0    # "args":Landroid/os/Bundle;
    :goto_1
    return-void

    .line 213
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v4

    invoke-static {v4}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourCategory(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 214
    sget-object v3, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 215
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v2

    goto :goto_0

    .line 217
    :cond_2
    sget-object v3, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 218
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v4

    add-int/lit8 v2, v4, 0x1

    goto :goto_0

    .line 220
    :cond_3
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v4

    sget-object v5, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v4, v5, :cond_4

    .line 221
    sget-object v3, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 222
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v2

    goto :goto_0

    .line 223
    :cond_4
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v4

    sget-object v5, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v4, v5, :cond_0

    .line 224
    sget-object v3, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 225
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->isVideoPath()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 226
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v2

    goto :goto_0

    .line 228
    :cond_5
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getOwnerResId()I

    move-result v2

    .line 229
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v4

    sub-int/2addr v4, v2

    iput v4, p0, Lcom/samsung/helphub/HelpHubActivity;->mItemNum:I

    goto :goto_0

    .line 241
    .restart local v0    # "args":Landroid/os/Bundle;
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v4

    invoke-virtual {v4, v6, v0, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_1

    .line 244
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->finish()V

    goto :goto_1
.end method

.method public static setAionbarType(I)V
    .locals 0
    .param p0, "type"    # I

    .prologue
    .line 253
    sput p0, Lcom/samsung/helphub/HelpHubActivity;->actionbarType:I

    .line 254
    return-void
.end method


# virtual methods
.method public checkCreateByIntent()Z
    .locals 2

    .prologue
    .line 311
    iget-boolean v0, p0, Lcom/samsung/helphub/HelpHubActivity;->mTwoPane:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/HelpHubActivity;->mNewIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const-string v0, "android.intent.action.VIEW"

    iget-object v1, p0, Lcom/samsung/helphub/HelpHubActivity;->mNewIntent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    const/4 v0, 0x1

    .line 314
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemNum()I
    .locals 2

    .prologue
    .line 303
    iget v0, p0, Lcom/samsung/helphub/HelpHubActivity;->mItemNum:I

    .line 304
    .local v0, "i":I
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/helphub/HelpHubActivity;->mItemNum:I

    .line 306
    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 327
    invoke-super {p0, p1}, Lcom/samsung/helphub/HelpHubActivityBase;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 328
    iget-object v0, p0, Lcom/samsung/helphub/HelpHubActivity;->mHeaderList:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/samsung/helphub/HelpHubActivity;->mHeaderList:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    invoke-virtual {v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->scrollHeaderList()V

    .line 331
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0x9

    const/16 v6, 0x400

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 64
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/helphub/HelpHubActivity;->mNewIntent:Landroid/content/Intent;

    .line 65
    const/4 v1, 0x0

    .line 66
    .local v1, "actionbar":Z
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v10}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 67
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 68
    .local v3, "intent":Landroid/content/Intent;
    const-string v5, "actionbar"

    invoke-virtual {v3, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 69
    if-nez v1, :cond_0

    .line 70
    sput v9, Lcom/samsung/helphub/HelpHubActivity;->actionbarType:I

    .line 72
    :cond_0
    if-nez v1, :cond_3

    sget v5, Lcom/samsung/helphub/HelpHubActivity;->actionbarType:I

    if-eq v5, v8, :cond_3

    .line 73
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/Window;->requestFeature(I)Z

    .line 74
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 77
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 78
    .local v0, "HelpActionBar":Landroid/app/ActionBar;
    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 85
    sput v9, Lcom/samsung/helphub/HelpHubActivity;->actionbarType:I

    .line 100
    .end local v0    # "HelpActionBar":Landroid/app/ActionBar;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/helphub/HelpHubActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 101
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/helphub/HelpHubActivity;->mBackStackListener:Landroid/app/FragmentManager$OnBackStackChangedListener;

    invoke-virtual {v5, v6}, Landroid/app/FragmentManager;->addOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 104
    iget-boolean v5, p0, Lcom/samsung/helphub/HelpHubActivity;->mTwoPane:Z

    if-nez v5, :cond_8

    .line 105
    if-nez p1, :cond_7

    .line 107
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v5

    if-nez v5, :cond_6

    invoke-static {v10}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 108
    sget v5, Lcom/samsung/helphub/HelpHubActivity;->actionbarType:I

    if-eq v5, v8, :cond_5

    new-instance v2, Lcom/samsung/helphub/fragments/CategoryHeaderList;

    invoke-direct {v2}, Lcom/samsung/helphub/fragments/CategoryHeaderList;-><init>()V

    .line 111
    .local v2, "fragment":Landroid/app/Fragment;
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 112
    .local v4, "transaction":Landroid/app/FragmentTransaction;
    invoke-virtual {v4, v7}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 113
    const v5, 0x7f0d0004

    invoke-virtual {v4, v5, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commit()I

    .line 140
    .end local v2    # "fragment":Landroid/app/Fragment;
    .end local v4    # "transaction":Landroid/app/FragmentTransaction;
    :cond_2
    :goto_2
    return-void

    .line 87
    .restart local v3    # "intent":Landroid/content/Intent;
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/Window;->requestFeature(I)Z

    .line 88
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 91
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 92
    .restart local v0    # "HelpActionBar":Landroid/app/ActionBar;
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 93
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {v10}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 94
    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 96
    :cond_4
    sput v8, Lcom/samsung/helphub/HelpHubActivity;->actionbarType:I

    goto :goto_0

    .line 108
    .end local v0    # "HelpActionBar":Landroid/app/ActionBar;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_5
    new-instance v2, Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-direct {v2}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;-><init>()V

    goto :goto_1

    .line 110
    :cond_6
    new-instance v2, Lcom/samsung/helphub/fragments/CategoryHeaderList;

    invoke-direct {v2}, Lcom/samsung/helphub/fragments/CategoryHeaderList;-><init>()V

    .restart local v2    # "fragment":Landroid/app/Fragment;
    goto :goto_1

    .line 116
    .end local v2    # "fragment":Landroid/app/Fragment;
    :cond_7
    iget-object v5, p0, Lcom/samsung/helphub/HelpHubActivity;->mBackStackListener:Landroid/app/FragmentManager$OnBackStackChangedListener;

    invoke-interface {v5}, Landroid/app/FragmentManager$OnBackStackChangedListener;->onBackStackChanged()V

    goto :goto_2

    .line 119
    :cond_8
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isLatte()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 123
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 124
    .restart local v0    # "HelpActionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 127
    .end local v0    # "HelpActionBar":Landroid/app/ActionBar;
    :cond_9
    if-nez p1, :cond_2

    .line 129
    new-instance v2, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    invoke-direct {v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;-><init>()V

    .restart local v2    # "fragment":Landroid/app/Fragment;
    move-object v5, v2

    .line 130
    check-cast v5, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    iput-object v5, p0, Lcom/samsung/helphub/HelpHubActivity;->mHeaderList:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    .line 131
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    .line 132
    .restart local v4    # "transaction":Landroid/app/FragmentTransaction;
    invoke-virtual {v4, v7}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 133
    const v5, 0x7f0d0004

    const-string v6, "contents"

    invoke-virtual {v4, v5, v2, v6}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_2
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 8
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 258
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v0, "content://com.samsung.helphub.provider.search"

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 259
    .local v7, "builder":Ljava/lang/StringBuilder;
    const-string v0, "/header/"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Search:target_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 261
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/samsung/helphub/HelpHubActivity;->mTwoPane:Z

    if-nez v0, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/helphub/HelpHubActivity;->mBackStackListener:Landroid/app/FragmentManager$OnBackStackChangedListener;

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->removeOnBackStackChangedListener(Landroid/app/FragmentManager$OnBackStackChangedListener;)V

    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/HelpHubActivity;->mBackStackListener:Landroid/app/FragmentManager$OnBackStackChangedListener;

    .line 156
    :cond_0
    invoke-super {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->onDestroy()V

    .line 157
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 5
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 267
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v4

    if-nez v4, :cond_2

    .line 268
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_3

    .line 269
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->finish()V

    .line 290
    :cond_1
    :goto_0
    if-eqz p2, :cond_2

    .line 291
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 294
    :cond_2
    return-void

    .line 271
    :cond_3
    invoke-static {p2}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v2

    .line 272
    .local v2, "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->isTwoPane()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 274
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    .line 275
    .local v3, "manager":Landroid/app/FragmentManager;
    const-string v4, "contents"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    .line 277
    .local v0, "contentsList":Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;
    if-eqz v0, :cond_4

    .line 278
    invoke-virtual {v0, v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->doActiveSectionItem(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 279
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->isVideoPath()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 280
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p0}, Lcom/samsung/helphub/HelpHubCommon;->startVideo(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 283
    :cond_4
    const-string v4, "sort"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;

    .line 285
    .local v1, "contentsSortingList":Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;
    if-eqz v1, :cond_1

    .line 286
    invoke-virtual {v1, v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->doActiveSectionItem(Lcom/samsung/helphub/headers/HelpHeader;)V

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 39
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/HelpHubActivity;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 299
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/samsung/helphub/HelpHubActivityBase;->onNewIntent(Landroid/content/Intent;)V

    .line 146
    iget-boolean v0, p0, Lcom/samsung/helphub/HelpHubActivity;->mTwoPane:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 147
    invoke-direct {p0, p1}, Lcom/samsung/helphub/HelpHubActivity;->handleSearchRequest(Landroid/content/Intent;)V

    .line 148
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const v4, 0x7f0a001e

    const/4 v2, 0x1

    .line 161
    const/4 v1, 0x0

    .line 163
    .local v1, "retval":Z
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 187
    :goto_0
    if-nez v1, :cond_0

    invoke-super {p0, p1}, Lcom/samsung/helphub/HelpHubActivityBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    :goto_1
    return v2

    .line 166
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v3

    if-nez v3, :cond_1

    .line 167
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 168
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setTitle(I)V

    goto :goto_0

    .line 170
    :cond_1
    iget-boolean v3, p0, Lcom/samsung/helphub/HelpHubActivity;->mTwoPane:Z

    if-eqz v3, :cond_2

    .line 171
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 172
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setTitle(I)V

    .line 175
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->popBackStack()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    :goto_2
    const/4 v1, 0x1

    .line 181
    goto :goto_0

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_2

    .line 187
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 163
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 198
    invoke-super {p0}, Lcom/samsung/helphub/HelpHubActivityBase;->onResume()V

    .line 199
    invoke-virtual {p0}, Lcom/samsung/helphub/HelpHubActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HELP"

    invoke-static {p0, v0, v1}, Lcom/samsung/helphub/HelpHubCommon;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x0

    return v0
.end method

.method public processIntent()V
    .locals 2

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/samsung/helphub/HelpHubActivity;->mTwoPane:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/HelpHubActivity;->mNewIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const-string v0, "android.intent.action.VIEW"

    iget-object v1, p0, Lcom/samsung/helphub/HelpHubActivity;->mNewIntent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/samsung/helphub/HelpHubActivity;->mNewIntent:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/samsung/helphub/HelpHubActivity;->handleSearchRequest(Landroid/content/Intent;)V

    .line 321
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/HelpHubActivity;->mNewIntent:Landroid/content/Intent;

    .line 323
    :cond_0
    return-void
.end method
