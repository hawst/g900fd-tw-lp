.class public final Lcom/samsung/helphub/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accept_reject_tip1:I = 0x7f0d025c

.field public static final accept_reject_tip_2:I = 0x7f0d025f

.field public static final access_notification_panel:I = 0x7f0d040a

.field public static final access_notification_panel_layout:I = 0x7f0d0403

.field public static final access_notification_panel_layout_holder:I = 0x7f0d0402

.field public static final access_quick_panel:I = 0x7f0d041d

.field public static final access_quick_panel_layout:I = 0x7f0d0412

.field public static final add_application_image:I = 0x7f0d037a

.field public static final add_contact_image:I = 0x7f0d037c

.field public static final air_button_infopreview_introduction_att_1:I = 0x7f0d020a

.field public static final air_button_infopreview_introduction_att_2:I = 0x7f0d020e

.field public static final air_button_introduction_action_memo_description:I = 0x7f0d01ff

.field public static final air_button_introduction_additional_features_description:I = 0x7f0d020d

.field public static final air_button_introduction_easy_clip_description:I = 0x7f0d0205

.field public static final air_button_introduction_pen_window_description:I = 0x7f0d020c

.field public static final air_button_introduction_scrap_booker_description:I = 0x7f0d0202

.field public static final air_button_introduction_screen_write_description:I = 0x7f0d0208

.field public static final air_button_introduction_sfinder_description:I = 0x7f0d020b

.field public static final air_button_introduction_to_use_air_command:I = 0x7f0d0209

.field public static final air_view_informatioin_pointer:I = 0x7f0d023a

.field public static final air_view_informatioin_preview:I = 0x7f0d022f

.field public static final air_view_pen_introduction_to_air_view_pen:I = 0x7f0d0231

.field public static final allshare_howto_cast_dongle_01:I = 0x7f0d023d

.field public static final allshare_howto_cast_dongle_02:I = 0x7f0d0240

.field public static final allshare_howto_cast_dongle_03:I = 0x7f0d0243

.field public static final allshare_howto_cast_dongle_04:I = 0x7f0d0246

.field public static final allshare_smarttv_tip_description_id:I = 0x7f0d025a

.field public static final body_categorising_content:I = 0x7f0d03d6

.field public static final body_highlight_text:I = 0x7f0d03d8

.field public static final body_using_spen:I = 0x7f0d03d0

.field public static final bottom:I = 0x7f0d01e7

.field public static final button_online_search:I = 0x7f0d0002

.field public static final button_search_no_result:I = 0x7f0d0372

.field public static final button_search_result:I = 0x7f0d0374

.field public static final button_shealth_caregiver_1:I = 0x7f0d01a0

.field public static final button_shealth_caregiver_2:I = 0x7f0d01a1

.field public static final button_shealth_community_1:I = 0x7f0d019e

.field public static final button_shealth_community_2:I = 0x7f0d019f

.field public static final button_shealth_exercise_1:I = 0x7f0d01a2

.field public static final button_shealth_exercise_2:I = 0x7f0d01a3

.field public static final button_shealth_exercise_pro_1:I = 0x7f0d01a4

.field public static final button_shealth_exercise_pro_2:I = 0x7f0d01a5

.field public static final button_shealth_home_1:I = 0x7f0d019c

.field public static final button_shealth_home_2:I = 0x7f0d019d

.field public static final button_start_guide_mode_face_orientation:I = 0x7f0d03e4

.field public static final button_start_guide_mode_gallery:I = 0x7f0d0228

.field public static final button_start_guide_mode_messaging:I = 0x7f0d022d

.field public static final button_start_guide_mode_splanner:I = 0x7f0d0224

.field public static final button_start_guide_mode_tilting_device:I = 0x7f0d03e9

.field public static final button_start_try_it:I = 0x7f0d0081

.field public static final button_start_try_it_1:I = 0x7f0d0082

.field public static final button_try_it_adding_recipients:I = 0x7f0d0218

.field public static final button_try_it_gallery:I = 0x7f0d0236

.field public static final button_try_it_inserting_content:I = 0x7f0d0216

.field public static final button_try_it_splanner:I = 0x7f0d0234

.field public static final button_try_motion:I = 0x7f0d0108

.field public static final camera_auto:I = 0x7f0d0265

.field public static final camera_beauty:I = 0x7f0d0267

.field public static final camera_dual:I = 0x7f0d0272

.field public static final camera_gifmaker:I = 0x7f0d027c

.field public static final camera_help_changing_setting:I = 0x7f0d0288

.field public static final camera_learn_more_about_mode_auto:I = 0x7f0d0266

.field public static final camera_learn_more_about_mode_beauty:I = 0x7f0d0268

.field public static final camera_learn_more_about_mode_bestphoto:I = 0x7f0d026a

.field public static final camera_learn_more_about_mode_drama:I = 0x7f0d026b

.field public static final camera_learn_more_about_mode_dual:I = 0x7f0d0273

.field public static final camera_learn_more_about_mode_eraser:I = 0x7f0d026c

.field public static final camera_learn_more_about_mode_gifmaker:I = 0x7f0d027d

.field public static final camera_learn_more_about_mode_panningshot:I = 0x7f0d026d

.field public static final camera_learn_more_about_mode_panorama:I = 0x7f0d026f

.field public static final camera_learn_more_about_mode_selectivefocus:I = 0x7f0d0277

.field public static final camera_learn_more_about_mode_selfie:I = 0x7f0d0279

.field public static final camera_learn_more_about_mode_selfiealarm:I = 0x7f0d0275

.field public static final camera_learn_more_about_mode_virtualtour:I = 0x7f0d0271

.field public static final camera_learn_more_about_mode_wideselfie:I = 0x7f0d027b

.field public static final camera_panorama:I = 0x7f0d026e

.field public static final camera_selecting_camera_mode_tip_1:I = 0x7f0d0263

.field public static final camera_selecting_camera_mode_tip_2:I = 0x7f0d0264

.field public static final camera_selectivefocus:I = 0x7f0d0276

.field public static final camera_selfie:I = 0x7f0d0278

.field public static final camera_selfiealarm:I = 0x7f0d0274

.field public static final camera_shotandmore:I = 0x7f0d0269

.field public static final camera_virtualtour:I = 0x7f0d0270

.field public static final camera_wideselfie:I = 0x7f0d027a

.field public static final car_mode_ans_rej_call_1:I = 0x7f0d02a2

.field public static final car_mode_ans_rej_call_2:I = 0x7f0d02a4

.field public static final car_mode_ans_rej_call_number_1:I = 0x7f0d02a1

.field public static final car_mode_ans_rej_call_number_2:I = 0x7f0d02a3

.field public static final car_mode_ans_rej_call_tip_box_1:I = 0x7f0d02a5

.field public static final car_mode_ans_rej_call_title:I = 0x7f0d02a0

.field public static final car_mode_enabling_car_mode_1:I = 0x7f0d028a

.field public static final car_mode_enabling_car_mode_2:I = 0x7f0d028c

.field public static final car_mode_enabling_car_mode_number_1:I = 0x7f0d0289

.field public static final car_mode_enabling_car_mode_number_2:I = 0x7f0d028b

.field public static final car_mode_enabling_car_mode_tip_box:I = 0x7f0d028d

.field public static final car_mode_help_separator_1:I = 0x7f0d029f

.field public static final car_mode_help_separator_2:I = 0x7f0d02a6

.field public static final car_mode_help_separator_3:I = 0x7f0d02b1

.field public static final car_mode_making_call_1:I = 0x7f0d029b

.field public static final car_mode_making_call_2:I = 0x7f0d029d

.field public static final car_mode_making_call_number_1:I = 0x7f0d029a

.field public static final car_mode_making_call_number_2:I = 0x7f0d029c

.field public static final car_mode_making_call_tip_box_1:I = 0x7f0d029e

.field public static final car_mode_making_call_title:I = 0x7f0d0299

.field public static final car_mode_registering_cars_1:I = 0x7f0d028f

.field public static final car_mode_registering_cars_2:I = 0x7f0d0291

.field public static final car_mode_registering_cars_3:I = 0x7f0d0293

.field public static final car_mode_registering_cars_4:I = 0x7f0d0295

.field public static final car_mode_registering_cars_5:I = 0x7f0d0297

.field public static final car_mode_registering_cars_number_1:I = 0x7f0d028e

.field public static final car_mode_registering_cars_number_2:I = 0x7f0d0290

.field public static final car_mode_registering_cars_number_3:I = 0x7f0d0292

.field public static final car_mode_registering_cars_number_4:I = 0x7f0d0294

.field public static final car_mode_registering_cars_number_5:I = 0x7f0d0296

.field public static final car_mode_registering_cars_tip_box:I = 0x7f0d0298

.field public static final car_mode_sending_msg_1:I = 0x7f0d02a9

.field public static final car_mode_sending_msg_2:I = 0x7f0d02ab

.field public static final car_mode_sending_msg_3:I = 0x7f0d02ad

.field public static final car_mode_sending_msg_4:I = 0x7f0d02af

.field public static final car_mode_sending_msg_number_1:I = 0x7f0d02a8

.field public static final car_mode_sending_msg_number_2:I = 0x7f0d02aa

.field public static final car_mode_sending_msg_number_3:I = 0x7f0d02ac

.field public static final car_mode_sending_msg_number_4:I = 0x7f0d02ae

.field public static final car_mode_sending_msg_tip_box_1:I = 0x7f0d02b0

.field public static final car_mode_sending_msg_title:I = 0x7f0d02a7

.field public static final car_mode_setting_dest_1:I = 0x7f0d02b4

.field public static final car_mode_setting_dest_2:I = 0x7f0d02b6

.field public static final car_mode_setting_dest_number_1:I = 0x7f0d02b3

.field public static final car_mode_setting_dest_number_2:I = 0x7f0d02b5

.field public static final car_mode_setting_dest_tip_box_1:I = 0x7f0d02b7

.field public static final car_mode_setting_dest_title:I = 0x7f0d02b2

.field public static final category_available_accessories:I = 0x7f0d00a4

.field public static final category_basic_apps:I = 0x7f0d00a0

.field public static final category_basic_apps_easy_mode:I = 0x7f0d00ae

.field public static final category_device:I = 0x7f0d009e

.field public static final category_getting_started:I = 0x7f0d009d

.field public static final category_getting_started_easy_mode:I = 0x7f0d00ad

.field public static final category_icon_glossary:I = 0x7f0d009a

.field public static final category_life_log:I = 0x7f0d00a1

.field public static final category_multimedia:I = 0x7f0d00a2

.field public static final category_new_features:I = 0x7f0d009b

.field public static final category_new_features_easy_mode:I = 0x7f0d00ab

.field public static final category_new_features_list:I = 0x7f0d00aa

.field public static final category_new_features_list_easy_mode:I = 0x7f0d00ac

.field public static final category_new_features_message:I = 0x7f0d00b1

.field public static final category_new_features_summary:I = 0x7f0d009c

.field public static final category_new_features_view_more:I = 0x7f0d036e

.field public static final category_online_help:I = 0x7f0d00a3

.field public static final category_settings:I = 0x7f0d009f

.field public static final category_settings_easy_mode:I = 0x7f0d00af

.field public static final category_spen_apps:I = 0x7f0d00b0

.field public static final category_vzw_how_to_videos:I = 0x7f0d00a5

.field public static final category_vzw_my_verizon:I = 0x7f0d00a6

.field public static final category_vzw_useful_tips:I = 0x7f0d00a7

.field public static final category_vzw_user_manual:I = 0x7f0d00a8

.field public static final center:I = 0x7f0d01e8

.field public static final delete_application_image:I = 0x7f0d037b

.field public static final delete_contact_image:I = 0x7f0d037d

.field public static final description:I = 0x7f0d03f7

.field public static final detail_activity_container:I = 0x7f0d02bb

.field public static final dialog_button_cancel:I = 0x7f0d0105

.field public static final dialog_button_mobile:I = 0x7f0d0107

.field public static final dialog_button_wifi:I = 0x7f0d0106

.field public static final drop_down_anim:I = 0x7f0d0405

.field public static final drop_down_anim_1:I = 0x7f0d0417

.field public static final drop_down_anim_2:I = 0x7f0d0418

.field public static final drop_down_image1:I = 0x7f0d0423

.field public static final drop_down_image2:I = 0x7f0d0424

.field public static final drop_down_image3:I = 0x7f0d0425

.field public static final drop_down_image4:I = 0x7f0d0426

.field public static final emptyview_air_view_information_preview_button:I = 0x7f0d021a

.field public static final emptyview_air_view_information_preview_gallery:I = 0x7f0d0226

.field public static final emptyview_air_view_information_preview_mesaging:I = 0x7f0d022b

.field public static final emptyview_air_view_information_preview_messaging_button:I = 0x7f0d022e

.field public static final emptyview_air_view_information_preview_splanner:I = 0x7f0d0222

.field public static final emptyview_smart_screen_smart_scroll_face_orientation:I = 0x7f0d03e2

.field public static final emptyview_smart_screen_smart_scroll_information:I = 0x7f0d03e0

.field public static final emptyview_smart_screen_smart_scroll_tilting_device:I = 0x7f0d03e7

.field public static final emptyview_start_guide_mode_face_orientation_button:I = 0x7f0d03e5

.field public static final emptyview_start_guide_mode_tilting_device_button:I = 0x7f0d03ea

.field public static final emptyview_start_guide_mode_tilting_device_settings:I = 0x7f0d03eb

.field public static final enable_setting_button_cancel:I = 0x7f0d0116

.field public static final enable_setting_button_ok:I = 0x7f0d0115

.field public static final enable_setting_dialog_summary:I = 0x7f0d0114

.field public static final enable_setting_dialog_title:I = 0x7f0d0113

.field public static final enabling_multi_window_text_1:I = 0x7f0d039b

.field public static final enabling_multi_window_text_4:I = 0x7f0d039d

.field public static final expand_notification_close:I = 0x7f0d0428

.field public static final expand_notification_open:I = 0x7f0d042e

.field public static final expand_notification_open_layout:I = 0x7f0d042d

.field public static final expand_notification_panel_layout:I = 0x7f0d0427

.field public static final expandable_list:I = 0x7f0d02ca

.field public static final expandable_section_child:I = 0x7f0d010a

.field public static final expandable_section_child_divider:I = 0x7f0d010b

.field public static final expandable_section_child_divider_top:I = 0x7f0d0377

.field public static final expandable_section_group:I = 0x7f0d0109

.field public static final expandable_the_notification_panel_01:I = 0x7f0d03a6

.field public static final fill:I = 0x7f0d01e9

.field public static final filtering_picture_info2:I = 0x7f0d02dc

.field public static final filtering_pictures_info1:I = 0x7f0d02db

.field public static final gallery_more_info:I = 0x7f0d03d9

.field public static final gallery_viewing_a_picture_tip1:I = 0x7f0d02e0

.field public static final gallery_viewing_a_picture_tip2:I = 0x7f0d02e1

.field public static final gallery_viewing_a_picture_tip3:I = 0x7f0d03da

.field public static final gesture_delete_character_layout:I = 0x7f0d044a

.field public static final gesture_delete_one:I = 0x7f0d044b

.field public static final gesture_delete_space:I = 0x7f0d0452

.field public static final gesture_delete_space_layout:I = 0x7f0d0451

.field public static final gesture_delete_word:I = 0x7f0d0447

.field public static final gesture_delete_word_layout:I = 0x7f0d0446

.field public static final gesture_edit:I = 0x7f0d0444

.field public static final gesture_edit_layout:I = 0x7f0d0443

.field public static final gesture_insert_01:I = 0x7f0d044e

.field public static final gesture_insert_layout:I = 0x7f0d044d

.field public static final gloablairbutton_item1:I = 0x7f0d01fe

.field public static final gloablairbutton_item3:I = 0x7f0d0201

.field public static final gloablairbutton_item4:I = 0x7f0d0204

.field public static final gloablairbutton_item5:I = 0x7f0d0207

.field public static final grayscale_layout:I = 0x7f0d043d

.field public static final group_play_introduction:I = 0x7f0d02e2

.field public static final group_play_introduction_text:I = 0x7f0d02e3

.field public static final group_play_share_video_introduction_1:I = 0x7f0d02e4

.field public static final group_play_share_video_introduction_2:I = 0x7f0d02e5

.field public static final group_play_share_video_introduction_3:I = 0x7f0d02e6

.field public static final header_devider:I = 0x7f0d0007

.field public static final header_icon:I = 0x7f0d010f

.field public static final header_image:I = 0x7f0d010e

.field public static final header_layout:I = 0x7f0d0005

.field public static final header_layout_item:I = 0x7f0d0006

.field public static final header_list:I = 0x7f0d00a9

.field public static final header_sumarry:I = 0x7f0d0376

.field public static final header_summary:I = 0x7f0d010d

.field public static final header_title:I = 0x7f0d010c

.field public static final help_anim_view:I = 0x7f0d0416

.field public static final help_delete_a_picture_img:I = 0x7f0d02d9

.field public static final help_delete_multiple_pictures_img:I = 0x7f0d02da

.field public static final help_empty_item_1:I = 0x7f0d03ab

.field public static final help_empty_item_2:I = 0x7f0d03ad

.field public static final help_im_checking_event_notifications:I = 0x7f0d0433

.field public static final help_image_magnified_note_image_1:I = 0x7f0d03ef

.field public static final help_list_detail:I = 0x7f0d03af

.field public static final help_list_image_3:I = 0x7f0d0262

.field public static final help_list_item_1:I = 0x7f0d0032

.field public static final help_list_item_10:I = 0x7f0d003b

.field public static final help_list_item_11:I = 0x7f0d003c

.field public static final help_list_item_12:I = 0x7f0d003d

.field public static final help_list_item_1_1:I = 0x7f0d003e

.field public static final help_list_item_1_10:I = 0x7f0d0047

.field public static final help_list_item_1_11:I = 0x7f0d0048

.field public static final help_list_item_1_12:I = 0x7f0d0049

.field public static final help_list_item_1_2:I = 0x7f0d003f

.field public static final help_list_item_1_3:I = 0x7f0d0040

.field public static final help_list_item_1_4:I = 0x7f0d0041

.field public static final help_list_item_1_5:I = 0x7f0d0042

.field public static final help_list_item_1_6:I = 0x7f0d0043

.field public static final help_list_item_1_7:I = 0x7f0d0044

.field public static final help_list_item_1_8:I = 0x7f0d0045

.field public static final help_list_item_1_9:I = 0x7f0d0046

.field public static final help_list_item_1_usa:I = 0x7f0d01e2

.field public static final help_list_item_2:I = 0x7f0d0033

.field public static final help_list_item_2_1:I = 0x7f0d004a

.field public static final help_list_item_2_10:I = 0x7f0d0053

.field public static final help_list_item_2_11:I = 0x7f0d0054

.field public static final help_list_item_2_12:I = 0x7f0d0055

.field public static final help_list_item_2_2:I = 0x7f0d004b

.field public static final help_list_item_2_3:I = 0x7f0d004c

.field public static final help_list_item_2_4:I = 0x7f0d004d

.field public static final help_list_item_2_5:I = 0x7f0d004e

.field public static final help_list_item_2_6:I = 0x7f0d004f

.field public static final help_list_item_2_7:I = 0x7f0d0050

.field public static final help_list_item_2_8:I = 0x7f0d0051

.field public static final help_list_item_2_9:I = 0x7f0d0052

.field public static final help_list_item_3:I = 0x7f0d0034

.field public static final help_list_item_3_1:I = 0x7f0d0056

.field public static final help_list_item_3_2:I = 0x7f0d0057

.field public static final help_list_item_3_3:I = 0x7f0d0058

.field public static final help_list_item_3_4:I = 0x7f0d0059

.field public static final help_list_item_3_5:I = 0x7f0d005a

.field public static final help_list_item_4:I = 0x7f0d0035

.field public static final help_list_item_4_1:I = 0x7f0d005b

.field public static final help_list_item_4_2:I = 0x7f0d005c

.field public static final help_list_item_5:I = 0x7f0d0036

.field public static final help_list_item_6:I = 0x7f0d0037

.field public static final help_list_item_7:I = 0x7f0d0038

.field public static final help_list_item_8:I = 0x7f0d0039

.field public static final help_list_item_9:I = 0x7f0d003a

.field public static final help_list_item_dot:I = 0x7f0d0080

.field public static final help_list_item_festival_1:I = 0x7f0d0092

.field public static final help_list_item_festival_2:I = 0x7f0d0093

.field public static final help_list_item_festival_3:I = 0x7f0d0094

.field public static final help_list_item_image_1:I = 0x7f0d02c1

.field public static final help_list_item_image_2:I = 0x7f0d02c9

.field public static final help_list_item_image_4:I = 0x7f0d02c5

.field public static final help_list_item_image_5:I = 0x7f0d02c6

.field public static final help_list_item_image_text_1:I = 0x7f0d02c7

.field public static final help_list_item_image_text_2:I = 0x7f0d02c8

.field public static final help_list_item_number_1:I = 0x7f0d005d

.field public static final help_list_item_number_10:I = 0x7f0d0066

.field public static final help_list_item_number_1_1:I = 0x7f0d0067

.field public static final help_list_item_number_1_10:I = 0x7f0d0070

.field public static final help_list_item_number_1_2:I = 0x7f0d0068

.field public static final help_list_item_number_1_3:I = 0x7f0d0069

.field public static final help_list_item_number_1_4:I = 0x7f0d006a

.field public static final help_list_item_number_1_5:I = 0x7f0d006b

.field public static final help_list_item_number_1_6:I = 0x7f0d006c

.field public static final help_list_item_number_1_7:I = 0x7f0d006d

.field public static final help_list_item_number_1_8:I = 0x7f0d006e

.field public static final help_list_item_number_1_9:I = 0x7f0d006f

.field public static final help_list_item_number_1_text:I = 0x7f0d0388

.field public static final help_list_item_number_2:I = 0x7f0d005e

.field public static final help_list_item_number_2_1:I = 0x7f0d0071

.field public static final help_list_item_number_2_10:I = 0x7f0d007a

.field public static final help_list_item_number_2_2:I = 0x7f0d0072

.field public static final help_list_item_number_2_3:I = 0x7f0d0073

.field public static final help_list_item_number_2_4:I = 0x7f0d0074

.field public static final help_list_item_number_2_5:I = 0x7f0d0075

.field public static final help_list_item_number_2_6:I = 0x7f0d0076

.field public static final help_list_item_number_2_7:I = 0x7f0d0077

.field public static final help_list_item_number_2_8:I = 0x7f0d0078

.field public static final help_list_item_number_2_9:I = 0x7f0d0079

.field public static final help_list_item_number_2_text:I = 0x7f0d0389

.field public static final help_list_item_number_3:I = 0x7f0d005f

.field public static final help_list_item_number_3_1:I = 0x7f0d007b

.field public static final help_list_item_number_3_2:I = 0x7f0d007c

.field public static final help_list_item_number_3_3:I = 0x7f0d007d

.field public static final help_list_item_number_3_4:I = 0x7f0d007e

.field public static final help_list_item_number_3_5:I = 0x7f0d007f

.field public static final help_list_item_number_4:I = 0x7f0d0060

.field public static final help_list_item_number_5:I = 0x7f0d0061

.field public static final help_list_item_number_6:I = 0x7f0d0062

.field public static final help_list_item_number_7:I = 0x7f0d0063

.field public static final help_list_item_number_8:I = 0x7f0d0064

.field public static final help_list_item_number_9:I = 0x7f0d0065

.field public static final help_list_item_number_festival_1:I = 0x7f0d008f

.field public static final help_list_item_number_festival_2:I = 0x7f0d0090

.field public static final help_list_item_number_festival_3:I = 0x7f0d0091

.field public static final help_list_item_tip_1:I = 0x7f0d02be

.field public static final help_list_item_tip_2:I = 0x7f0d02bf

.field public static final help_list_item_tip_3:I = 0x7f0d02c0

.field public static final help_list_item_title_1:I = 0x7f0d02c2

.field public static final help_list_item_title_2:I = 0x7f0d02c3

.field public static final help_list_item_title_3:I = 0x7f0d02c4

.field public static final help_list_sub_title:I = 0x7f0d03c6

.field public static final help_list_sub_title_2:I = 0x7f0d03c7

.field public static final help_log_press:I = 0x7f0d0365

.field public static final help_main_new_features_image_default:I = 0x7f0d036b

.field public static final help_main_new_features_play:I = 0x7f0d0379

.field public static final help_moment_ll:I = 0x7f0d0366

.field public static final help_moment_scroll_log:I = 0x7f0d0367

.field public static final help_page_breadcrumbs:I = 0x7f0d0119

.field public static final help_page_breadcrumbs_separator:I = 0x7f0d02e8

.field public static final help_page_container:I = 0x7f0d0001

.field public static final help_page_details_container:I = 0x7f0d0089

.field public static final help_page_group_item_fold:I = 0x7f0d0086

.field public static final help_page_group_item_title:I = 0x7f0d0085

.field public static final help_page_image:I = 0x7f0d03b6

.field public static final help_page_imageview:I = 0x7f0d001f

.field public static final help_page_imageview_1:I = 0x7f0d0020

.field public static final help_page_imageview_2:I = 0x7f0d0021

.field public static final help_page_imageview_3:I = 0x7f0d0022

.field public static final help_page_imageview_4:I = 0x7f0d0023

.field public static final help_page_imageview_5:I = 0x7f0d0024

.field public static final help_page_imageview_6:I = 0x7f0d0025

.field public static final help_page_imageview_7:I = 0x7f0d0026

.field public static final help_page_imageview_8:I = 0x7f0d0027

.field public static final help_page_imageview_9:I = 0x7f0d0028

.field public static final help_page_layout_1:I = 0x7f0d0029

.field public static final help_page_layout_2:I = 0x7f0d002a

.field public static final help_page_layout_3:I = 0x7f0d002b

.field public static final help_page_layout_4:I = 0x7f0d002c

.field public static final help_page_layout_5:I = 0x7f0d002d

.field public static final help_page_layout_6:I = 0x7f0d002e

.field public static final help_page_layout_7:I = 0x7f0d002f

.field public static final help_page_layout_8:I = 0x7f0d0030

.field public static final help_page_layout_9:I = 0x7f0d0031

.field public static final help_page_sub_title:I = 0x7f0d03c5

.field public static final help_page_textview_1:I = 0x7f0d02eb

.field public static final help_page_textview_10:I = 0x7f0d02f4

.field public static final help_page_textview_11:I = 0x7f0d02f5

.field public static final help_page_textview_12:I = 0x7f0d02f6

.field public static final help_page_textview_2:I = 0x7f0d02ec

.field public static final help_page_textview_3:I = 0x7f0d02ed

.field public static final help_page_textview_4:I = 0x7f0d02ee

.field public static final help_page_textview_5:I = 0x7f0d02ef

.field public static final help_page_textview_6:I = 0x7f0d02f0

.field public static final help_page_textview_7:I = 0x7f0d02f1

.field public static final help_page_textview_8:I = 0x7f0d02f2

.field public static final help_page_textview_9:I = 0x7f0d02f3

.field public static final help_page_title:I = 0x7f0d0016

.field public static final help_page_title1:I = 0x7f0d0261

.field public static final help_page_title_1:I = 0x7f0d0017

.field public static final help_page_title_2:I = 0x7f0d0018

.field public static final help_page_title_3:I = 0x7f0d0019

.field public static final help_page_title_4:I = 0x7f0d001a

.field public static final help_page_title_5:I = 0x7f0d001b

.field public static final help_page_title_6:I = 0x7f0d001c

.field public static final help_page_title_7:I = 0x7f0d001d

.field public static final help_page_title_8:I = 0x7f0d001e

.field public static final help_page_title_festival:I = 0x7f0d008e

.field public static final help_page_view_container:I = 0x7f0d0088

.field public static final help_quick_panel_complete_tip:I = 0x7f0d0420

.field public static final help_quick_panel_tip:I = 0x7f0d041b

.field public static final help_report_scroll_log:I = 0x7f0d0368

.field public static final help_report_tap_log:I = 0x7f0d0369

.field public static final help_settings:I = 0x7f0d0013

.field public static final help_settings_button_text:I = 0x7f0d0014

.field public static final help_sharing_a_picture_img:I = 0x7f0d02dd

.field public static final help_sharing_multiple_pictures_img:I = 0x7f0d02de

.field public static final help_status_bar_view:I = 0x7f0d0414

.field public static final help_supporting2_predictive_text:I = 0x7f0d032b

.field public static final help_supporting3_predictive_text:I = 0x7f0d032c

.field public static final help_supporting_predictive_text:I = 0x7f0d032a

.field public static final help_tap_and_pay_desc_6:I = 0x7f0d01e3

.field public static final help_tap_and_pay_desc_6_usa:I = 0x7f0d01e4

.field public static final help_tip_access_notification:I = 0x7f0d0406

.field public static final help_tip_access_notification_complete:I = 0x7f0d040d

.field public static final help_tip_access_notification_complete_msg_text:I = 0x7f0d040e

.field public static final help_tip_access_notification_complete_text:I = 0x7f0d040f

.field public static final help_tip_access_notification_msg_tail:I = 0x7f0d0409

.field public static final help_tip_access_notification_msg_text:I = 0x7f0d0407

.field public static final help_tip_access_notification_text:I = 0x7f0d0408

.field public static final help_tip_access_quick_panel:I = 0x7f0d0419

.field public static final help_tip_access_quick_panel_complete:I = 0x7f0d041e

.field public static final help_tip_access_quick_panel_complete_msg_text:I = 0x7f0d041f

.field public static final help_tip_access_quick_panel_msg_tail:I = 0x7f0d041c

.field public static final help_tip_access_quick_panel_msg_text:I = 0x7f0d041a

.field public static final help_tip_expand_noti_msg_tail:I = 0x7f0d042c

.field public static final help_tip_expand_notification:I = 0x7f0d0429

.field public static final help_tip_expand_notification_close_text:I = 0x7f0d042b

.field public static final help_tip_expand_notification_complete:I = 0x7f0d042f

.field public static final help_tip_expand_notification_complete_text:I = 0x7f0d0430

.field public static final help_tip_expand_notification_msg_text:I = 0x7f0d042a

.field public static final help_view_anim:I = 0x7f0d0413

.field public static final help_viewing_album_img:I = 0x7f0d02df

.field public static final help_widget:I = 0x7f0d02ea

.field public static final helphub_title_text:I = 0x7f0d02e9

.field public static final helppage_tipbox_text1:I = 0x7f0d03d2

.field public static final helppage_tipbox_text2:I = 0x7f0d03d4

.field public static final helppage_tipbox_text3:I = 0x7f0d03db

.field public static final imageView:I = 0x7f0d03f6

.field public static final imageView1:I = 0x7f0d0249

.field public static final image_face_orientation:I = 0x7f0d03e1

.field public static final image_tilting_device:I = 0x7f0d03e6

.field public static final image_use_fingerprint_1:I = 0x7f0d02cb

.field public static final image_use_fingerprint_2:I = 0x7f0d02d0

.field public static final image_use_fingerprint_3:I = 0x7f0d02d1

.field public static final imageview_allshare_learn_nfc_01:I = 0x7f0d024b

.field public static final imageview_allshare_learn_nfc_03:I = 0x7f0d0250

.field public static final imageview_contextual_tag:I = 0x7f0d02d2

.field public static final imageview_cursor_image:I = 0x7f0d043a

.field public static final imageview_enable_driving:I = 0x7f0d03fc

.field public static final imageview_gesture_delete_one:I = 0x7f0d044c

.field public static final imageview_gesture_delete_space:I = 0x7f0d0453

.field public static final imageview_gesture_delete_word_01:I = 0x7f0d0448

.field public static final imageview_gesture_delete_word_02:I = 0x7f0d0449

.field public static final imageview_gesture_edit:I = 0x7f0d0445

.field public static final imageview_gesture_insert_01:I = 0x7f0d044f

.field public static final imageview_gesture_insert_02:I = 0x7f0d0450

.field public static final imageview_healthcare_diary_blood_glucose:I = 0x7f0d0198

.field public static final imageview_healthcare_diary_blood_pressure:I = 0x7f0d0199

.field public static final imageview_healthcare_diary_weight:I = 0x7f0d019a

.field public static final imageview_introduction_of_camera:I = 0x7f0d0284

.field public static final imageview_introduction_of_camera2:I = 0x7f0d0281

.field public static final imageview_keyboard_continuous_input_insert_key:I = 0x7f0d02f8

.field public static final imageview_keyboard_gesture_01:I = 0x7f0d0316

.field public static final imageview_keyboard_gesture_02:I = 0x7f0d0319

.field public static final imageview_keyboard_gesture_03:I = 0x7f0d031e

.field public static final imageview_keyboard_gesture_04:I = 0x7f0d031a

.field public static final imageview_keyboard_gesture_05:I = 0x7f0d0321

.field public static final imageview_keyboard_gesture_06:I = 0x7f0d0324

.field public static final imageview_keyboard_gesture_07:I = 0x7f0d0328

.field public static final imageview_keyboard_gesture_09:I = 0x7f0d031b

.field public static final imageview_keyboard_handwriting_not_overwrite_left:I = 0x7f0d0306

.field public static final imageview_keyboard_handwriting_not_overwrite_o:I = 0x7f0d0307

.field public static final imageview_keyboard_handwriting_not_overwrite_right:I = 0x7f0d0309

.field public static final imageview_keyboard_handwriting_not_overwrite_x:I = 0x7f0d030a

.field public static final imageview_keyboard_handwriting_support_word_left:I = 0x7f0d0310

.field public static final imageview_keyboard_handwriting_support_word_right:I = 0x7f0d0313

.field public static final imageview_keyboard_handwriting_write_next_arrow:I = 0x7f0d02fb

.field public static final imageview_keyboard_handwriting_write_next_left:I = 0x7f0d02fa

.field public static final imageview_keyboard_handwriting_write_next_right:I = 0x7f0d02fc

.field public static final imageview_keyboard_handwriting_write_parallel_left:I = 0x7f0d02ff

.field public static final imageview_keyboard_handwriting_write_parallel_o:I = 0x7f0d0300

.field public static final imageview_keyboard_handwriting_write_parallel_right:I = 0x7f0d0302

.field public static final imageview_keyboard_handwriting_write_parallel_x:I = 0x7f0d0303

.field public static final imageview_keyboard_samsung_chineseime_toolbar_mode:I = 0x7f0d0347

.field public static final imageview_keyboard_samung_keyboard_close_keyboard:I = 0x7f0d0332

.field public static final imageview_keyboard_samung_keyboard_open_keyboard:I = 0x7f0d032f

.field public static final imageview_multi_drag_and_drop_content:I = 0x7f0d0398

.field public static final imageview_multi_window_edit_items:I = 0x7f0d039a

.field public static final imageview_multi_window_opening:I = 0x7f0d039e

.field public static final imageview_multi_window_popup:I = 0x7f0d03b0

.field public static final imageview_multi_window_popup_1:I = 0x7f0d03b1

.field public static final imageview_multi_window_show_hide_tray:I = 0x7f0d03a0

.field public static final imageview_multi_window_tray:I = 0x7f0d03a1

.field public static final imageview_multi_window_view:I = 0x7f0d03a2

.field public static final imageview_navigate_home_screen:I = 0x7f0d02bc

.field public static final imageview_smart_stay_eye:I = 0x7f0d03ec

.field public static final imageview_svoice_edit_what_you_said:I = 0x7f0d03fd

.field public static final imageview_svoice_use_svoice_1:I = 0x7f0d03fe

.field public static final imageview_svoice_use_svoice_2:I = 0x7f0d03ff

.field public static final imageview_svoice_use_svoice_3:I = 0x7f0d0401

.field public static final imageview_view_all_apps:I = 0x7f0d02bd

.field public static final imageview_view_using_menu:I = 0x7f0d01dc

.field public static final imageview_walk_mate:I = 0x7f0d019b

.field public static final imageviewer_Spen_Gesture_capture_screen:I = 0x7f0d03f5

.field public static final imageviewer_air_button_indicator:I = 0x7f0d01fb

.field public static final imageviewer_air_button_indicator_1:I = 0x7f0d020f

.field public static final imageviewer_air_button_indicator_2:I = 0x7f0d0213

.field public static final imageviewer_air_command_adding_recipients:I = 0x7f0d0217

.field public static final imageviewer_air_command_displaying_available_actions:I = 0x7f0d0219

.field public static final imageviewer_air_gesture_browse:I = 0x7f0d021b

.field public static final imageviewer_air_gesture_call_accept:I = 0x7f0d021c

.field public static final imageviewer_air_gesture_indicator:I = 0x7f0d021d

.field public static final imageviewer_air_gesture_jump:I = 0x7f0d021e

.field public static final imageviewer_air_gesture_move:I = 0x7f0d021f

.field public static final imageviewer_air_gesture_quick_glance:I = 0x7f0d0220

.field public static final imageviewer_air_view_icon_labels:I = 0x7f0d0232

.field public static final imageviewer_air_view_informatioin_preview_progress_bar:I = 0x7f0d023b

.field public static final imageviewer_air_view_informatioin_preview_speed_dial:I = 0x7f0d023c

.field public static final imageviewer_air_view_list_scrolling:I = 0x7f0d0237

.field public static final imageviewer_air_view_magnifier:I = 0x7f0d0230

.field public static final imageviewer_air_view_pen_gallery:I = 0x7f0d0235

.field public static final imageviewer_air_view_pen_spanner:I = 0x7f0d0233

.field public static final imageviewer_air_view_progress_preview_pen:I = 0x7f0d0238

.field public static final imageviewer_air_view_speed_dial_pen:I = 0x7f0d0239

.field public static final imageviewer_gloablairbutton_app:I = 0x7f0d01fc

.field public static final imageviewer_motion_palm_swipe_capture:I = 0x7f0d037e

.field public static final imageviewer_motion_palm_swipe_mute:I = 0x7f0d037f

.field public static final imageviewer_motion_pan_to_browse:I = 0x7f0d0380

.field public static final imageviewer_motion_pan_to_move_icon:I = 0x7f0d0381

.field public static final imageviewer_motion_pick_up_direct_call:I = 0x7f0d0382

.field public static final imageviewer_motion_pick_up_smart_alert:I = 0x7f0d0383

.field public static final imageviewer_motion_shake:I = 0x7f0d0384

.field public static final imageviewer_motion_tile_to_zoom:I = 0x7f0d0385

.field public static final imageviewer_motion_turnover:I = 0x7f0d0386

.field public static final imageviewer_smart_clip_pen_crop:I = 0x7f0d03dc

.field public static final imageviewer_smart_screen_introduction:I = 0x7f0d03dd

.field public static final imageviewer_smart_screen_pause:I = 0x7f0d03de

.field public static final imageviewer_smart_screen_rotation:I = 0x7f0d03df

.field public static final imageviewer_snote_action_memo_1:I = 0x7f0d03ed

.field public static final imageviewer_snote_action_memo_2:I = 0x7f0d03ee

.field public static final imageviewer_snote_pen_preset_2:I = 0x7f0d03f0

.field public static final imageviewer_snote_pen_preset_3:I = 0x7f0d03f1

.field public static final imageviewer_snote_snap_note_1:I = 0x7f0d03f2

.field public static final imageviewer_snote_snap_note_2:I = 0x7f0d03f3

.field public static final imageviewer_snote_using_snote:I = 0x7f0d03f4

.field public static final img_allshare_learn_smarttv_01:I = 0x7f0d0254

.field public static final img_allshare_learn_smarttv_02:I = 0x7f0d0257

.field public static final introduction_of_camera:I = 0x7f0d0285

.field public static final introduction_of_camera2:I = 0x7f0d0282

.field public static final introduction_of_camera_show1:I = 0x7f0d0283

.field public static final introduction_of_camera_show2:I = 0x7f0d0280

.field public static final introduction_of_camera_show22:I = 0x7f0d0287

.field public static final introduction_of_camera_title:I = 0x7f0d0286

.field public static final item1_quickconnect_description_1:I = 0x7f0d03b4

.field public static final item1_quickconnect_tip_1:I = 0x7f0d03b5

.field public static final item_actionmemo_help_item_action_link:I = 0x7f0d0180

.field public static final item_air_command_add_recipients:I = 0x7f0d0167

.field public static final item_air_command_attach_image:I = 0x7f0d0166

.field public static final item_air_command_introduction:I = 0x7f0d0169

.field public static final item_air_command_introduction_att:I = 0x7f0d016a

.field public static final item_air_gesture_browse:I = 0x7f0d0193

.field public static final item_air_gesture_call_accept:I = 0x7f0d0194

.field public static final item_air_gesture_indicator:I = 0x7f0d0192

.field public static final item_air_gesture_quick_glance:I = 0x7f0d0161

.field public static final item_air_view_pen_finger_speed_dial_preview:I = 0x7f0d0168

.field public static final item_allshare_learn_nfc:I = 0x7f0d0141

.field public static final item_basic_definition_apps:I = 0x7f0d0186

.field public static final item_basic_definition_bluetooth:I = 0x7f0d0187

.field public static final item_basic_definition_data:I = 0x7f0d0188

.field public static final item_basic_definition_flight_mode:I = 0x7f0d0185

.field public static final item_basic_definition_gps:I = 0x7f0d0189

.field public static final item_basic_definition_mobile_hotspot:I = 0x7f0d018a

.field public static final item_basic_definition_nfc:I = 0x7f0d018b

.field public static final item_basic_definition_software_update:I = 0x7f0d018c

.field public static final item_basic_definition_sync:I = 0x7f0d018d

.field public static final item_basic_definition_visual_voice_mail:I = 0x7f0d018e

.field public static final item_basic_definition_widget:I = 0x7f0d0190

.field public static final item_basic_definition_wifi:I = 0x7f0d018f

.field public static final item_bluetooth_tethering:I = 0x7f0d0165

.field public static final item_camera_introduction:I = 0x7f0d01e0

.field public static final item_camera_introduction_2nd_lcd:I = 0x7f0d01e1

.field public static final item_camera_take_dual_picture:I = 0x7f0d0181

.field public static final item_contacts_setup_speed_dial:I = 0x7f0d0140

.field public static final item_editing_homescreen:I = 0x7f0d01d1

.field public static final item_galaxyfinder_how_to_improve_searching:I = 0x7f0d017d

.field public static final item_galaxyfinder_how_to_start_searching:I = 0x7f0d017c

.field public static final item_galaxyfinder_search_by_handwriting:I = 0x7f0d017e

.field public static final item_galaxyfinder_sort_search_results:I = 0x7f0d017f

.field public static final item_gallery_contextual_tag:I = 0x7f0d0152

.field public static final item_gallery_contextual_tag_k:I = 0x7f0d0153

.field public static final item_gallery_deleting_picture:I = 0x7f0d015b

.field public static final item_gallery_deleting_picture_easy:I = 0x7f0d015d

.field public static final item_gallery_face_tagging:I = 0x7f0d0154

.field public static final item_gallery_filtering_picture:I = 0x7f0d0159

.field public static final item_gallery_howto_collage:I = 0x7f0d014a

.field public static final item_gallery_howto_copy:I = 0x7f0d014c

.field public static final item_gallery_howto_copy_onmenu:I = 0x7f0d0150

.field public static final item_gallery_howto_edit:I = 0x7f0d0149

.field public static final item_gallery_howto_edit_onmenu:I = 0x7f0d014e

.field public static final item_gallery_howto_entertainer:I = 0x7f0d014d

.field public static final item_gallery_howto_entertainer_onmenu:I = 0x7f0d0151

.field public static final item_gallery_howto_selection_mode:I = 0x7f0d014b

.field public static final item_gallery_howto_selection_mode_onmenu:I = 0x7f0d014f

.field public static final item_gallery_sharing_picture:I = 0x7f0d015a

.field public static final item_gallery_sharing_picture_easy:I = 0x7f0d015c

.field public static final item_gallery_viewing_a_picture_and_resizing_thumbnails:I = 0x7f0d0155

.field public static final item_gallery_viewing_a_picture_and_resizing_thumbnails_easy:I = 0x7f0d0156

.field public static final item_gallery_viewing_album:I = 0x7f0d0157

.field public static final item_gallery_viewing_album_easy:I = 0x7f0d0158

.field public static final item_gallery_viewing_picture:I = 0x7f0d0148

.field public static final item_gesture_move:I = 0x7f0d0160

.field public static final item_group_play_music_sharing:I = 0x7f0d0142

.field public static final item_group_play_video_sharing:I = 0x7f0d0143

.field public static final item_guest_lounge_access_home:I = 0x7f0d0455

.field public static final item_guest_lounge_introduction:I = 0x7f0d0454

.field public static final item_hide_shealth_sleep:I = 0x7f0d0135

.field public static final item_hs_id_using_flipboard_briefing:I = 0x7f0d01e6

.field public static final item_hs_id_using_mymagazine:I = 0x7f0d01e5

.field public static final item_keyboard_handwriting:I = 0x7f0d015e

.field public static final item_keyboard_handwriting_gesture:I = 0x7f0d015f

.field public static final item_lock_screen_changing_wallpaper:I = 0x7f0d0095

.field public static final item_lock_screen_changing_wallpaper_festival:I = 0x7f0d0096

.field public static final item_lock_screen_checking_event_notifications:I = 0x7f0d01cc

.field public static final item_lock_screen_opening_camera:I = 0x7f0d01cb

.field public static final item_magazine_home_adding_widget:I = 0x7f0d01b1

.field public static final item_magazine_home_change_layout:I = 0x7f0d01b5

.field public static final item_magazine_home_move_widget:I = 0x7f0d01b2

.field public static final item_magazine_home_navigatting:I = 0x7f0d01b0

.field public static final item_magazine_home_remove_widget:I = 0x7f0d01b3

.field public static final item_magazine_home_resize_widget:I = 0x7f0d01b4

.field public static final item_managing_applications:I = 0x7f0d01d0

.field public static final item_managing_contacts:I = 0x7f0d01cf

.field public static final item_messaging_add_priority_sender:I = 0x7f0d0184

.field public static final item_messaging_send_multimedia:I = 0x7f0d0195

.field public static final item_mobile_hotspot:I = 0x7f0d0164

.field public static final item_music_playback:I = 0x7f0d0125

.field public static final item_music_rocking_playback:I = 0x7f0d0126

.field public static final item_mw_bezel_template:I = 0x7f0d0174

.field public static final item_mw_drag_and_drop_content:I = 0x7f0d0172

.field public static final item_mw_edit_items:I = 0x7f0d0176

.field public static final item_mw_open_the_same_app:I = 0x7f0d0175

.field public static final item_mw_opening:I = 0x7f0d0178

.field public static final item_mw_opening_by_recents:I = 0x7f0d016e

.field public static final item_mw_opening_by_tray:I = 0x7f0d0179

.field public static final item_mw_recent_apps:I = 0x7f0d0177

.field public static final item_mw_show_hide_tray:I = 0x7f0d016f

.field public static final item_mw_template:I = 0x7f0d0173

.field public static final item_mw_tray:I = 0x7f0d0170

.field public static final item_mw_using:I = 0x7f0d017a

.field public static final item_mw_view:I = 0x7f0d0171

.field public static final item_nfc_payment:I = 0x7f0d013a

.field public static final item_nfc_tap_and_pay:I = 0x7f0d013b

.field public static final item_nfc_turn_on_nfc:I = 0x7f0d013c

.field public static final item_nfc_turn_on_nfc_att:I = 0x7f0d013d

.field public static final item_nfc_turn_on_sbeam:I = 0x7f0d013e

.field public static final item_notifications_check_id:I = 0x7f0d01c4

.field public static final item_notifications_enabledisable_function_id:I = 0x7f0d01c5

.field public static final item_notifications_panel_access_id:I = 0x7f0d01c3

.field public static final item_one_hand_size_key:I = 0x7f0d01df

.field public static final item_one_hand_size_menu:I = 0x7f0d01de

.field public static final item_onehand_reduce_screen:I = 0x7f0d01dd

.field public static final item_palm_to_capture_screen:I = 0x7f0d011b

.field public static final item_palm_to_mute:I = 0x7f0d011a

.field public static final item_pan_to_browse_image:I = 0x7f0d011f

.field public static final item_pan_to_move_icon:I = 0x7f0d011e

.field public static final item_phone_call_drag_and_drop:I = 0x7f0d01c9

.field public static final item_pick_up_to_direct_call:I = 0x7f0d013f

.field public static final item_pick_up_to_smart_alert:I = 0x7f0d011c

.field public static final item_pin_board_pin_it_pinboard:I = 0x7f0d0183

.field public static final item_pin_board_pin_mode:I = 0x7f0d0182

.field public static final item_popup_opening:I = 0x7f0d017b

.field public static final item_quick_glance:I = 0x7f0d0124

.field public static final item_ripple_effect:I = 0x7f0d0123

.field public static final item_sconnect_how_to_use_sconnect:I = 0x7f0d0191

.field public static final item_sending_help_message:I = 0x7f0d01d6

.field public static final item_shealth_blood_glucose:I = 0x7f0d012d

.field public static final item_shealth_blood_pressure:I = 0x7f0d012e

.field public static final item_shealth_caregiver:I = 0x7f0d0131

.field public static final item_shealth_coach:I = 0x7f0d0139

.field public static final item_shealth_comfort_level:I = 0x7f0d0138

.field public static final item_shealth_community:I = 0x7f0d0130

.field public static final item_shealth_ecg:I = 0x7f0d012f

.field public static final item_shealth_heartrate:I = 0x7f0d0132

.field public static final item_shealth_sleep_monitor:I = 0x7f0d0134

.field public static final item_shealth_spo2:I = 0x7f0d0133

.field public static final item_shealth_stress:I = 0x7f0d0136

.field public static final item_shealth_stress_monitor:I = 0x7f0d0137

.field public static final item_shealth_weight:I = 0x7f0d012c

.field public static final item_smart_remote:I = 0x7f0d0128

.field public static final item_smart_rotation:I = 0x7f0d0127

.field public static final item_smart_screen_smart_pause:I = 0x7f0d012a

.field public static final item_smart_screen_smart_scroll:I = 0x7f0d012b

.field public static final item_smart_screen_smart_stay:I = 0x7f0d0129

.field public static final item_svoice_how_to_use_voice_commands:I = 0x7f0d0146

.field public static final item_svoice_use_voice_control:I = 0x7f0d0145

.field public static final item_svoice_voice_control:I = 0x7f0d0144

.field public static final item_svoice_wakeup_command:I = 0x7f0d0147

.field public static final item_sync_google_account:I = 0x7f0d0197

.field public static final item_sync_samsung_account:I = 0x7f0d0196

.field public static final item_tethering:I = 0x7f0d0162

.field public static final item_tilt_to_zoom:I = 0x7f0d011d

.field public static final item_turn_over_to_mute:I = 0x7f0d0120

.field public static final item_unlock_set_screen_lock:I = 0x7f0d01ca

.field public static final item_usb_tethering:I = 0x7f0d0163

.field public static final item_using_assistant_menu:I = 0x7f0d01c0

.field public static final item_using_babycrying_detector:I = 0x7f0d01bf

.field public static final item_using_direct_access:I = 0x7f0d01c1

.field public static final item_using_download_booster:I = 0x7f0d01c8

.field public static final item_using_easy_mode:I = 0x7f0d01ce

.field public static final item_using_emergency_mode:I = 0x7f0d01d5

.field public static final item_using_features_in_air_command:I = 0x7f0d016b

.field public static final item_using_features_in_air_command_image_clip:I = 0x7f0d016d

.field public static final item_using_features_in_air_command_smart_select:I = 0x7f0d016c

.field public static final item_using_geo_life:I = 0x7f0d01d4

.field public static final item_using_geo_news:I = 0x7f0d01d3

.field public static final item_using_multiwindow_pop_up_view:I = 0x7f0d0396

.field public static final item_using_multiwindow_view:I = 0x7f0d0397

.field public static final item_using_sound_detectors:I = 0x7f0d01be

.field public static final item_using_talkback:I = 0x7f0d01bd

.field public static final item_video_chapter_list:I = 0x7f0d0122

.field public static final item_view_albums_list:I = 0x7f0d0121

.field public static final item_vzw_my_basic_device_calling_and_messaging:I = 0x7f0d01a9

.field public static final item_vzw_my_basic_device_easy_mode:I = 0x7f0d01ad

.field public static final item_vzw_my_basic_device_email_setup:I = 0x7f0d01ab

.field public static final item_vzw_my_basic_device_navigation:I = 0x7f0d01a8

.field public static final item_vzw_my_basic_device_verizon_messages:I = 0x7f0d01aa

.field public static final item_vzw_my_basic_device_wifi_and_bt_setup:I = 0x7f0d01ac

.field public static final item_vzw_wifi_enhanced:I = 0x7f0d01ae

.field public static final key_applications_header:I = 0x7f0d000e

.field public static final keyboard_continuous_input_insert_key:I = 0x7f0d02f7

.field public static final keyboard_handwriting_gesture_delete_one:I = 0x7f0d031d

.field public static final keyboard_handwriting_gesture_delete_space:I = 0x7f0d0323

.field public static final keyboard_handwriting_gesture_delete_space_layout:I = 0x7f0d0322

.field public static final keyboard_handwriting_gesture_delete_word:I = 0x7f0d0318

.field public static final keyboard_handwriting_gesture_edit:I = 0x7f0d0315

.field public static final keyboard_handwriting_gesture_edit_layout:I = 0x7f0d0314

.field public static final keyboard_handwriting_gesture_enter_stroke:I = 0x7f0d0326

.field public static final keyboard_handwriting_gesture_text_insert:I = 0x7f0d0320

.field public static final keyboard_handwriting_gesture_text_insert_layout:I = 0x7f0d031f

.field public static final keyboard_handwriting_recognition_each_char:I = 0x7f0d030f

.field public static final keyboard_handwriting_recognition_word:I = 0x7f0d0312

.field public static final keyboard_handwriting_support_handwriting:I = 0x7f0d030b

.field public static final keyboard_handwriting_support_words:I = 0x7f0d030c

.field public static final keyboard_samsung_chineseime_change_language_description:I = 0x7f0d0344

.field public static final keyboard_samsung_chineseime_changing_input_mode:I = 0x7f0d034c

.field public static final keyboard_samsung_chineseime_changing_input_mode_description:I = 0x7f0d034d

.field public static final keyboard_samsung_chineseime_changing_keyboard_theme:I = 0x7f0d0358

.field public static final keyboard_samsung_chineseime_changing_keyboard_theme_description:I = 0x7f0d0359

.field public static final keyboard_samsung_chineseime_changing_keyboard_type:I = 0x7f0d0356

.field public static final keyboard_samsung_chineseime_changing_keyboard_type_description:I = 0x7f0d0357

.field public static final keyboard_samsung_chineseime_inserting_emoticons:I = 0x7f0d0350

.field public static final keyboard_samsung_chineseime_inserting_emoticons_description:I = 0x7f0d0351

.field public static final keyboard_samsung_chineseime_number_mode:I = 0x7f0d0342

.field public static final keyboard_samsung_chineseime_number_mode_description:I = 0x7f0d0343

.field public static final keyboard_samsung_chineseime_open_keyboard_settings:I = 0x7f0d0348

.field public static final keyboard_samsung_chineseime_open_keyboard_settings_description:I = 0x7f0d0349

.field public static final keyboard_samsung_chineseime_opening_clipboard:I = 0x7f0d034a

.field public static final keyboard_samsung_chineseime_opening_clipboard_description:I = 0x7f0d034b

.field public static final keyboard_samsung_chineseime_opening_text_editing_panel:I = 0x7f0d0352

.field public static final keyboard_samsung_chineseime_opening_text_editing_panel_description:I = 0x7f0d0353

.field public static final keyboard_samsung_chineseime_symbol_mode:I = 0x7f0d0340

.field public static final keyboard_samsung_chineseime_symbol_mode_description:I = 0x7f0d0341

.field public static final keyboard_samsung_chineseime_toolbar_icon:I = 0x7f0d0346

.field public static final keyboard_samsung_chineseime_toolbar_mode:I = 0x7f0d0345

.field public static final keyboard_samsung_chineseime_using_handwriting_temporarily:I = 0x7f0d0354

.field public static final keyboard_samsung_chineseime_using_handwriting_temporarily_description:I = 0x7f0d0355

.field public static final keyboard_samsung_chineseime_using_voice_input:I = 0x7f0d034e

.field public static final keyboard_samsung_chineseime_using_voide_input_description:I = 0x7f0d034f

.field public static final keyboard_samung_chineseime_tap_back:I = 0x7f0d033f

.field public static final keyboard_samung_keyboard_change_language:I = 0x7f0d0335

.field public static final keyboard_samung_keyboard_change_language_description:I = 0x7f0d0336

.field public static final keyboard_samung_keyboard_change_language_tip:I = 0x7f0d0338

.field public static final keyboard_samung_keyboard_change_language_tipbox:I = 0x7f0d0337

.field public static final keyboard_samung_keyboard_change_mode:I = 0x7f0d0333

.field public static final keyboard_samung_keyboard_change_mode_description:I = 0x7f0d0334

.field public static final keyboard_samung_keyboard_close_keyboard:I = 0x7f0d0330

.field public static final keyboard_samung_keyboard_move_cursor:I = 0x7f0d033b

.field public static final keyboard_samung_keyboard_move_cursor_description:I = 0x7f0d033c

.field public static final keyboard_samung_keyboard_move_cursor_tip:I = 0x7f0d033e

.field public static final keyboard_samung_keyboard_move_cursor_tipbox:I = 0x7f0d033d

.field public static final keyboard_samung_keyboard_multi_mode_icon:I = 0x7f0d0339

.field public static final keyboard_samung_keyboard_multi_mode_icon_description:I = 0x7f0d033a

.field public static final keyboard_samung_keyboard_open_keyboard:I = 0x7f0d032d

.field public static final keyboard_samung_keyboard_tap_back:I = 0x7f0d0331

.field public static final keyboard_samung_keyboard_tap_text_field:I = 0x7f0d032e

.field public static final layout_keyboard_handwriting_gesture_delete_one_layout:I = 0x7f0d031c

.field public static final layout_keyboard_handwriting_gesture_delete_word_layout:I = 0x7f0d0317

.field public static final layout_keyboard_handwriting_gesture_enter_stroke_2:I = 0x7f0d0327

.field public static final layout_keyboard_handwriting_gesture_enter_stroke_layout:I = 0x7f0d0325

.field public static final layout_keyboard_handwriting_gesture_enter_stroke_text:I = 0x7f0d0329

.field public static final layout_keyboard_handwriting_not_overwrite:I = 0x7f0d0304

.field public static final layout_keyboard_handwriting_not_overwrite_left:I = 0x7f0d0305

.field public static final layout_keyboard_handwriting_not_overwrite_right:I = 0x7f0d0308

.field public static final layout_keyboard_handwriting_support_word:I = 0x7f0d030d

.field public static final layout_keyboard_handwriting_support_word_left:I = 0x7f0d030e

.field public static final layout_keyboard_handwriting_support_word_right:I = 0x7f0d0311

.field public static final layout_keyboard_handwriting_write_next:I = 0x7f0d02f9

.field public static final layout_keyboard_handwriting_write_parallel:I = 0x7f0d02fd

.field public static final layout_keyboard_handwriting_write_parallel_left:I = 0x7f0d02fe

.field public static final layout_keyboard_handwriting_write_parallel_right:I = 0x7f0d0301

.field public static final layout_title_text_1:I = 0x7f0d0221

.field public static final layout_title_text_2:I = 0x7f0d0225

.field public static final layout_title_text_3:I = 0x7f0d022a

.field public static final learn_camera_mode_try_it_button:I = 0x7f0d027f

.field public static final left:I = 0x7f0d01ea

.field public static final linearLayout1:I = 0x7f0d02b8

.field public static final linearlayout_multi_window_accessing_items_text1:I = 0x7f0d0387

.field public static final linearlayout_multi_window_button_info_text:I = 0x7f0d03a3

.field public static final linearlayout_multi_window_edit_items_text1:I = 0x7f0d0399

.field public static final linearlayout_multi_window_opening_text1:I = 0x7f0d039c

.field public static final linearlayout_multi_window_opening_text2:I = 0x7f0d039f

.field public static final linearlayout_multi_window_view_button_info:I = 0x7f0d038a

.field public static final linearlayout_smart_select_more_info_text_1_1:I = 0x7f0d0210

.field public static final linearlayout_smart_select_more_info_text_1_2:I = 0x7f0d0211

.field public static final linearlayout_smart_select_more_info_text_1_3:I = 0x7f0d0212

.field public static final linearlayout_smart_select_more_info_text_2_1:I = 0x7f0d0214

.field public static final linearlayout_smart_select_more_info_text_2_2:I = 0x7f0d0215

.field public static final list:I = 0x7f0d0000

.field public static final list_category_new_features_play_image:I = 0x7f0d036f

.field public static final list_category_new_features_title_1st_line:I = 0x7f0d036d

.field public static final list_category_new_features_title_sort_and_search:I = 0x7f0d036c

.field public static final list_category_scrollable_container:I = 0x7f0d036a

.field public static final list_item_summary:I = 0x7f0d008b

.field public static final list_item_text:I = 0x7f0d008c

.field public static final list_item_title:I = 0x7f0d008a

.field public static final list_video_bubble_item:I = 0x7f0d0111

.field public static final list_video_bubble_item_icon:I = 0x7f0d0110

.field public static final list_video_image:I = 0x7f0d0378

.field public static final lock_screen_edit_lock_screen_land:I = 0x7f0d0435

.field public static final lock_screen_edit_lock_screen_ver:I = 0x7f0d0434

.field public static final lock_screen_navigate_lock_screen_imageview:I = 0x7f0d0436

.field public static final lock_screen_unlock_device_land:I = 0x7f0d0439

.field public static final lock_screen_unlock_device_ver:I = 0x7f0d0438

.field public static final magnify_more_info:I = 0x7f0d043b

.field public static final main_activity_carrier_header:I = 0x7f0d0015

.field public static final main_activity_carrier_list:I = 0x7f0d0012

.field public static final main_activity_guided_tours_header:I = 0x7f0d000c

.field public static final main_activity_guided_tours_list:I = 0x7f0d000d

.field public static final main_activity_key_apps_list:I = 0x7f0d0011

.field public static final main_activity_learn_list:I = 0x7f0d000f

.field public static final main_activity_settings_list:I = 0x7f0d0010

.field public static final main_layout_fragment_container:I = 0x7f0d0004

.field public static final main_layout_help_page:I = 0x7f0d02e7

.field public static final main_layout_help_page_container:I = 0x7f0d0087

.field public static final menu_actionbar_sort:I = 0x7f0d0456

.field public static final menu_search:I = 0x7f0d0457

.field public static final message:I = 0x7f0d02b9

.field public static final message_preview_layout:I = 0x7f0d0229

.field public static final more_info_image_1:I = 0x7f0d02cc

.field public static final more_info_image_2:I = 0x7f0d02ce

.field public static final more_info_text_1:I = 0x7f0d02cd

.field public static final more_info_text_2:I = 0x7f0d02cf

.field public static final my_magazine_introduction_01:I = 0x7f0d03a4

.field public static final no_data_text_view:I = 0x7f0d0003

.field public static final notification_access_the_notification_panel_01:I = 0x7f0d03a7

.field public static final notification_access_the_notification_panel_02:I = 0x7f0d03a8

.field public static final notification_bottom_line:I = 0x7f0d0431

.field public static final notification_panel_handler:I = 0x7f0d0410

.field public static final notification_panel_handler_grey:I = 0x7f0d0411

.field public static final notification_panel_status_bar:I = 0x7f0d0404

.field public static final notification_use_the_notification_panel_01:I = 0x7f0d03aa

.field public static final notification_use_the_notification_panel_02:I = 0x7f0d03ac

.field public static final notification_use_the_notification_panel_03:I = 0x7f0d03a9

.field public static final onehand_reduce_screen_help_image:I = 0x7f0d03ae

.field public static final opening_camera:I = 0x7f0d0437

.field public static final page_landscape_view:I = 0x7f0d0118

.field public static final page_portrait_view:I = 0x7f0d0117

.field public static final phone_button_start_try_it_2:I = 0x7f0d01da

.field public static final phone_button_start_try_it_3:I = 0x7f0d01db

.field public static final physical_key_child_text_1:I = 0x7f0d035b

.field public static final physical_key_child_text_2:I = 0x7f0d035c

.field public static final physical_key_child_text_3:I = 0x7f0d035d

.field public static final physical_key_child_text_3_search:I = 0x7f0d035e

.field public static final physical_key_child_text_4:I = 0x7f0d035f

.field public static final physical_key_child_text_5:I = 0x7f0d0360

.field public static final physical_key_child_text_5_multi_window:I = 0x7f0d0361

.field public static final physicalkey_image1:I = 0x7f0d035a

.field public static final preview_category:I = 0x7f0d02d8

.field public static final preview_date:I = 0x7f0d02d3

.field public static final preview_location:I = 0x7f0d02d5

.field public static final preview_person:I = 0x7f0d02d4

.field public static final preview_user_tag:I = 0x7f0d02d7

.field public static final preview_weather:I = 0x7f0d02d6

.field public static final privatemode_setting_add:I = 0x7f0d03b2

.field public static final privatemode_setting_remove:I = 0x7f0d03b3

.field public static final quick_panel_handler:I = 0x7f0d0421

.field public static final quick_panel_handler_grey:I = 0x7f0d0422

.field public static final quick_panel_status_bar:I = 0x7f0d0415

.field public static final quickmemo_help_browser:I = 0x7f0d01f5

.field public static final quickmemo_help_browser_num:I = 0x7f0d01f6

.field public static final quickmemo_help_contact:I = 0x7f0d01ef

.field public static final quickmemo_help_contact_num:I = 0x7f0d01f0

.field public static final quickmemo_help_email:I = 0x7f0d01f3

.field public static final quickmemo_help_email_num:I = 0x7f0d01f4

.field public static final quickmemo_help_map:I = 0x7f0d01f7

.field public static final quickmemo_help_map_num:I = 0x7f0d01f8

.field public static final quickmemo_help_message:I = 0x7f0d01f1

.field public static final quickmemo_help_message_num:I = 0x7f0d01f2

.field public static final quickmemo_help_phone:I = 0x7f0d01ed

.field public static final quickmemo_help_phone_num:I = 0x7f0d01ee

.field public static final quickmemo_help_task:I = 0x7f0d01f9

.field public static final quickmemo_help_task_num:I = 0x7f0d01fa

.field public static final remember_selection_checkbox:I = 0x7f0d02ba

.field public static final right:I = 0x7f0d01eb

.field public static final safety_geonews_dot_1:I = 0x7f0d03b7

.field public static final safety_geonews_dot_2:I = 0x7f0d03b9

.field public static final safety_geonews_dot_3:I = 0x7f0d03bb

.field public static final safety_geonews_dot_4:I = 0x7f0d03bd

.field public static final safety_geonews_dot_5:I = 0x7f0d03bf

.field public static final safety_help_list_item_1:I = 0x7f0d01d8

.field public static final safety_help_list_item_1_green:I = 0x7f0d01d9

.field public static final safety_more_info_text_1:I = 0x7f0d03b8

.field public static final safety_more_info_text_2:I = 0x7f0d03ba

.field public static final safety_more_info_text_3:I = 0x7f0d03bc

.field public static final safety_more_info_text_4:I = 0x7f0d03be

.field public static final safety_more_info_text_5:I = 0x7f0d03c0

.field public static final safety_sendhelp_dot_1:I = 0x7f0d03c1

.field public static final safety_sendhelp_dot_2:I = 0x7f0d03c2

.field public static final safety_sendhelp_dot_3:I = 0x7f0d03c3

.field public static final sconnect_page_how_to_use_sconnect_image_1:I = 0x7f0d03c9

.field public static final sconnect_page_how_to_use_sconnect_image_2:I = 0x7f0d03cc

.field public static final sconnect_page_how_to_use_sconnect_text_1:I = 0x7f0d03ca

.field public static final sconnect_page_how_to_use_sconnect_text_2:I = 0x7f0d03cd

.field public static final sconnect_page_how_to_use_sconnect_title_1:I = 0x7f0d03c8

.field public static final sconnect_page_how_to_use_sconnect_title_2:I = 0x7f0d03cb

.field public static final search_icon:I = 0x7f0d01a7

.field public static final search_list_container:I = 0x7f0d0009

.field public static final search_no_result_found:I = 0x7f0d0370

.field public static final search_no_result_found_text:I = 0x7f0d0371

.field public static final search_result:I = 0x7f0d0373

.field public static final search_result_list_filler:I = 0x7f0d0375

.field public static final search_view:I = 0x7f0d0008

.field public static final section_accessibility:I = 0x7f0d01bc

.field public static final section_actionmemo:I = 0x7f0d00e7

.field public static final section_actionmemo_summary:I = 0x7f0d00e8

.field public static final section_air_button:I = 0x7f0d00e6

.field public static final section_air_gesture:I = 0x7f0d00de

.field public static final section_air_view:I = 0x7f0d00dd

.field public static final section_air_view_pen:I = 0x7f0d00e5

.field public static final section_basic_definition:I = 0x7f0d00b2

.field public static final section_camera:I = 0x7f0d00bc

.field public static final section_camera_2nd_lcd:I = 0x7f0d00be

.field public static final section_camera_2nd_lcd_chn:I = 0x7f0d00bf

.field public static final section_camera_chn:I = 0x7f0d00bd

.field public static final section_camera_video:I = 0x7f0d00c8

.field public static final section_car_mode:I = 0x7f0d00f8

.field public static final section_carrier_open_link:I = 0x7f0d0099

.field public static final section_chart_builder:I = 0x7f0d00eb

.field public static final section_cocktail_bar_service:I = 0x7f0d00ff

.field public static final section_data_usage_vzw_spr:I = 0x7f0d01c2

.field public static final section_download_booster:I = 0x7f0d01c6

.field public static final section_download_booster_chn:I = 0x7f0d01c7

.field public static final section_easy_mode:I = 0x7f0d01cd

.field public static final section_edit_quick_settings:I = 0x7f0d0101

.field public static final section_email:I = 0x7f0d00e1

.field public static final section_emeeting:I = 0x7f0d00ee

.field public static final section_fingerprints:I = 0x7f0d00e4

.field public static final section_galaxyfinder:I = 0x7f0d00e3

.field public static final section_gallery:I = 0x7f0d00f5

.field public static final section_group_play:I = 0x7f0d00cf

.field public static final section_group_play_video:I = 0x7f0d00c9

.field public static final section_guest_lounge:I = 0x7f0d00f4

.field public static final section_icon_alarm_and_calendar:I = 0x7f0d00b8

.field public static final section_icon_battery:I = 0x7f0d00b9

.field public static final section_icon_calendar:I = 0x7f0d00c6

.field public static final section_icon_call:I = 0x7f0d00b3

.field public static final section_icon_connectivity:I = 0x7f0d00b5

.field public static final section_icon_email:I = 0x7f0d00c2

.field public static final section_icon_email_and_messaging:I = 0x7f0d00b6

.field public static final section_icon_messages:I = 0x7f0d00b7

.field public static final section_icon_messaging:I = 0x7f0d00c5

.field public static final section_icon_other_icons:I = 0x7f0d00ba

.field public static final section_icon_phone:I = 0x7f0d00c1

.field public static final section_icon_sound:I = 0x7f0d00b4

.field public static final section_icon_system_status:I = 0x7f0d00c0

.field public static final section_keyboard:I = 0x7f0d00df

.field public static final section_keyboard_china:I = 0x7f0d00e0

.field public static final section_life_times:I = 0x7f0d01bb

.field public static final section_lockscreen:I = 0x7f0d00fa

.field public static final section_magazine_home:I = 0x7f0d01af

.field public static final section_message:I = 0x7f0d00d4

.field public static final section_motion:I = 0x7f0d00dc

.field public static final section_motion_video:I = 0x7f0d00c7

.field public static final section_multi_window:I = 0x7f0d00da

.field public static final section_mymagazine:I = 0x7f0d00ce

.field public static final section_nfc:I = 0x7f0d00d0

.field public static final section_onehand:I = 0x7f0d00f7

.field public static final section_phone:I = 0x7f0d00d1

.field public static final section_phone_2nd_lcd:I = 0x7f0d00d2

.field public static final section_phone_only_contacts:I = 0x7f0d00d3

.field public static final section_photo_reader:I = 0x7f0d00bb

.field public static final section_pin_board:I = 0x7f0d00e9

.field public static final section_power_saving_mode:I = 0x7f0d00d6

.field public static final section_private_mode:I = 0x7f0d00f9

.field public static final section_ringtone:I = 0x7f0d00d5

.field public static final section_s_voice:I = 0x7f0d00d8

.field public static final section_safety_assistance:I = 0x7f0d01d2

.field public static final section_samsunglink:I = 0x7f0d00ec

.field public static final section_sbrowser:I = 0x7f0d00f3

.field public static final section_sconnect:I = 0x7f0d00f6

.field public static final section_scrapbook:I = 0x7f0d00fb

.field public static final section_screen_mirroring:I = 0x7f0d00c3

.field public static final section_screen_mirroring_chn:I = 0x7f0d00fe

.field public static final section_shealth:I = 0x7f0d00cb

.field public static final section_smart_clip:I = 0x7f0d00ea

.field public static final section_smart_remote:I = 0x7f0d00ca

.field public static final section_smart_screen:I = 0x7f0d00db

.field public static final section_smemo:I = 0x7f0d00d9

.field public static final section_snote:I = 0x7f0d00e2

.field public static final section_spen_gesture:I = 0x7f0d00f2

.field public static final section_story_album:I = 0x7f0d00cc

.field public static final section_story_album_summary:I = 0x7f0d00cd

.field public static final section_tethering:I = 0x7f0d00ef

.field public static final section_tethering_tmo:I = 0x7f0d00f0

.field public static final section_tethering_vzw:I = 0x7f0d00f1

.field public static final section_translator:I = 0x7f0d00d7

.field public static final section_upgrade:I = 0x7f0d0100

.field public static final section_voice_control:I = 0x7f0d00c4

.field public static final section_vzw_additional_videos:I = 0x7f0d0103

.field public static final section_vzw_basic_setup_videos:I = 0x7f0d0104

.field public static final section_vzw_my_verizon_videos:I = 0x7f0d0102

.field public static final section_wifi:I = 0x7f0d00fc

.field public static final section_wifi_chn:I = 0x7f0d00fd

.field public static final section_writing_buddy:I = 0x7f0d00ed

.field public static final select_camera_mode_try_it_button:I = 0x7f0d027e

.field public static final select_device_to_view:I = 0x7f0d000b

.field public static final send_help_msg:I = 0x7f0d03c4

.field public static final send_help_msg_desc:I = 0x7f0d01d7

.field public static final sort_icon:I = 0x7f0d01a6

.field public static final split_bar:I = 0x7f0d01b7

.field public static final split_bar_hover:I = 0x7f0d01ba

.field public static final split_view_container:I = 0x7f0d01b6

.field public static final split_view_left_panel:I = 0x7f0d01b8

.field public static final split_view_right_panel:I = 0x7f0d01b9

.field public static final step_goal:I = 0x7f0d000a

.field public static final story_album_edit_memo:I = 0x7f0d03f8

.field public static final story_album_introduction_01:I = 0x7f0d03f9

.field public static final story_album_introduction_02:I = 0x7f0d03fa

.field public static final story_album_similial_pic:I = 0x7f0d03fb

.field public static final svoice_help_tip_layout:I = 0x7f0d0400

.field public static final text_number_1:I = 0x7f0d01fd

.field public static final text_number_3:I = 0x7f0d0200

.field public static final text_number_4:I = 0x7f0d0203

.field public static final text_number_5:I = 0x7f0d0206

.field public static final thumbnail:I = 0x7f0d03cf

.field public static final tip_box_1:I = 0x7f0d0083

.field public static final tip_box_2:I = 0x7f0d0084

.field public static final title_categorising_content:I = 0x7f0d03d5

.field public static final title_highlight_text:I = 0x7f0d03d7

.field public static final title_saving_content:I = 0x7f0d03d3

.field public static final title_saving_selected_content:I = 0x7f0d03d1

.field public static final title_separator_line:I = 0x7f0d0112

.field public static final title_separator_line_1:I = 0x7f0d025d

.field public static final title_separator_line_2:I = 0x7f0d025e

.field public static final title_separator_line_3:I = 0x7f0d0260

.field public static final title_separator_line_4:I = 0x7f0d0362

.field public static final title_separator_line_5:I = 0x7f0d0363

.field public static final title_separator_line_6:I = 0x7f0d0364

.field public static final title_separator_line_face_orientation:I = 0x7f0d03e3

.field public static final title_separator_line_gallery:I = 0x7f0d0227

.field public static final title_separator_line_messaging:I = 0x7f0d022c

.field public static final title_separator_line_splanner:I = 0x7f0d0223

.field public static final title_separator_line_tilting_device:I = 0x7f0d03e8

.field public static final title_using_spen:I = 0x7f0d03ce

.field public static final top:I = 0x7f0d01ec

.field public static final try_help_close:I = 0x7f0d0395

.field public static final try_help_close_layout:I = 0x7f0d0394

.field public static final try_help_drag_drops:I = 0x7f0d0391

.field public static final try_help_drag_drops_layout:I = 0x7f0d0390

.field public static final try_help_full_view:I = 0x7f0d0393

.field public static final try_help_full_view_layout:I = 0x7f0d0392

.field public static final try_help_switch:I = 0x7f0d038f

.field public static final try_help_switch_application:I = 0x7f0d038c

.field public static final try_help_switch_application_layout:I = 0x7f0d038b

.field public static final try_help_switch_application_text:I = 0x7f0d038d

.field public static final try_help_switch_window_layout:I = 0x7f0d038e

.field public static final tutorial_noti_empty:I = 0x7f0d040b

.field public static final tutorial_noti_open:I = 0x7f0d040c

.field public static final ultra_power_saving_more_info_greyscale:I = 0x7f0d0441

.field public static final ultra_power_saving_more_info_mobile_data:I = 0x7f0d0442

.field public static final unlock_expand_widget:I = 0x7f0d0432

.field public static final unlock_unlock_the_device:I = 0x7f0d03a5

.field public static final using_darkscreen:I = 0x7f0d043f

.field public static final using_direct_access_text_1_4:I = 0x7f0d043c

.field public static final using_power_saving_mode_greyscale_mode:I = 0x7f0d043e

.field public static final using_ultra_power_saving_mode:I = 0x7f0d0440

.field public static final view_no_connection:I = 0x7f0d0097

.field public static final web_view:I = 0x7f0d0098

.field public static final wfd_dongle_help_start_btn:I = 0x7f0d024a

.field public static final wfd_dongle_help_text_01:I = 0x7f0d023f

.field public static final wfd_dongle_help_text_02:I = 0x7f0d0242

.field public static final wfd_dongle_help_text_03:I = 0x7f0d0245

.field public static final wfd_dongle_help_text_04:I = 0x7f0d0248

.field public static final wfd_dongle_item_number_1:I = 0x7f0d023e

.field public static final wfd_dongle_item_number_2:I = 0x7f0d0241

.field public static final wfd_dongle_item_number_3:I = 0x7f0d0244

.field public static final wfd_dongle_item_number_4:I = 0x7f0d0247

.field public static final wfd_item_number_1:I = 0x7f0d024c

.field public static final wfd_nfc_help_text_01:I = 0x7f0d024d

.field public static final wfd_nfc_help_text_02:I = 0x7f0d024f

.field public static final wfd_nfc_help_text_03:I = 0x7f0d0252

.field public static final wfd_nfc_item_number_2:I = 0x7f0d024e

.field public static final wfd_nfc_item_number_3:I = 0x7f0d0251

.field public static final wfd_nfc_more_info_id:I = 0x7f0d0253

.field public static final wfd_smarttv_help_start_btn:I = 0x7f0d025b

.field public static final wfd_smarttv_help_text_01:I = 0x7f0d0256

.field public static final wfd_smarttv_help_text_02:I = 0x7f0d0259

.field public static final wfd_smarttv_item_number_1:I = 0x7f0d0255

.field public static final wfd_smarttv_item_number_2:I = 0x7f0d0258

.field public static final wifi_tip_description_id:I = 0x7f0d008d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
