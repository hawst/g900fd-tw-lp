.class Lcom/samsung/helphub/fragments/tablet/HelpPagesList$1;
.super Ljava/lang/Object;
.source "HelpPagesList.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/tablet/HelpPagesList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/tablet/HelpPagesList;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/tablet/HelpPagesList;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPagesList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 8
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "id"    # J

    .prologue
    .line 111
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPagesList;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;
    invoke-static {v4}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->access$000(Lcom/samsung/helphub/fragments/tablet/HelpPagesList;)Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->getGroupCount()I

    move-result v0

    .line 112
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_5

    .line 113
    if-ne v2, p3, :cond_4

    .line 114
    invoke-virtual {p1, v2}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 115
    invoke-virtual {p1}, Landroid/widget/ExpandableListView;->findFocus()Landroid/view/View;

    move-result-object v3

    .line 118
    .local v3, "view":Landroid/view/View;
    if-eqz v3, :cond_0

    instance-of v4, v3, Landroid/widget/EditText;

    if-eqz v4, :cond_0

    .line 119
    invoke-virtual {v3}, Landroid/view/View;->clearFocus()V

    .line 121
    :cond_0
    invoke-virtual {p1, v2}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 112
    .end local v3    # "view":Landroid/view/View;
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 123
    :cond_2
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPagesList;

    # setter for: Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mExpandedGroupPosition:I
    invoke-static {v4, p3}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->access$102(Lcom/samsung/helphub/fragments/tablet/HelpPagesList;I)I

    .line 124
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPagesList;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;
    invoke-static {v4}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->access$000(Lcom/samsung/helphub/fragments/tablet/HelpPagesList;)Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;

    move-result-object v4

    invoke-virtual {v4, p3}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/headers/HelpHeader;

    .line 125
    .local v1, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v1, :cond_1

    .line 126
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->isVideoPath()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 127
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPagesList;

    invoke-virtual {v5}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/helphub/HelpHubCommon;->startVideo(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_1

    .line 129
    :cond_3
    invoke-virtual {p1, v2}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    goto :goto_1

    .line 134
    .end local v1    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_4
    invoke-virtual {p1, v2}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    goto :goto_1

    .line 139
    :cond_5
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/widget/ExpandableListView;->playSoundEffect(I)V

    .line 141
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPagesList;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;
    invoke-static {v4}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->access$000(Lcom/samsung/helphub/fragments/tablet/HelpPagesList;)Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->notifyDataSetChanged()V

    .line 142
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPagesList;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;
    invoke-static {v4}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->access$200(Lcom/samsung/helphub/fragments/tablet/HelpPagesList;)Landroid/widget/ExpandableListView;

    move-result-object v4

    new-instance v5, Lcom/samsung/helphub/fragments/tablet/HelpPagesList$MoveExpandingItemRunnable;

    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPagesList;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;
    invoke-static {v6}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->access$200(Lcom/samsung/helphub/fragments/tablet/HelpPagesList;)Landroid/widget/ExpandableListView;

    move-result-object v6

    invoke-direct {v5, v6, p3}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList$MoveExpandingItemRunnable;-><init>(Landroid/widget/ListView;I)V

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/widget/ExpandableListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 144
    const/4 v4, 0x1

    return v4
.end method
