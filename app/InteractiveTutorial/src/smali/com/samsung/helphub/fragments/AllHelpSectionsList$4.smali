.class Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;
.super Ljava/lang/Object;
.source "AllHelpSectionsList.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/AllHelpSectionsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 19
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "id"    # J

    .prologue
    .line 328
    const/16 v16, 0x0

    .line 329
    .local v16, "result":Z
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v11

    .line 331
    .local v11, "expanded":Z
    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1, v3}, Landroid/widget/ExpandableListView;->setSelectionFromTop(II)V

    .line 332
    if-eqz p2, :cond_0

    .line 333
    const v3, 0x7f0d0086

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 334
    .local v12, "foldIcon":Landroid/widget/ImageView;
    if-eqz v12, :cond_0

    .line 335
    if-eqz v11, :cond_2

    .line 336
    new-instance v2, Landroid/view/animation/RotateAnimation;

    const/high16 v3, 0x43340000    # 180.0f

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v2 .. v8}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 339
    .local v2, "anim":Landroid/view/animation/RotateAnimation;
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 340
    const-wide/16 v4, 0x2bc

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 341
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V

    .line 342
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 343
    invoke-virtual {v12, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 357
    .end local v2    # "anim":Landroid/view/animation/RotateAnimation;
    .end local v12    # "foldIcon":Landroid/widget/ImageView;
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    # getter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$000(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/helphub/headers/HelpHeader;

    .line 358
    .local v13, "header":Lcom/samsung/helphub/headers/HelpHeader;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    # getter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$000(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/helphub/headers/HelpHeader;

    # setter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->clickedSectionHeader:Lcom/samsung/helphub/headers/HelpHeader;
    invoke-static {v4, v3}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$302(Lcom/samsung/helphub/fragments/AllHelpSectionsList;Lcom/samsung/helphub/headers/HelpHeader;)Lcom/samsung/helphub/headers/HelpHeader;

    .line 359
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    move/from16 v0, p3

    # setter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->clickedSectionIndex:I
    invoke-static {v3, v0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$402(Lcom/samsung/helphub/fragments/AllHelpSectionsList;I)I

    .line 360
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v3}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->savedLanguage:Ljava/lang/String;

    .line 362
    invoke-virtual {v13}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d009a

    if-eq v3, v4, :cond_1

    invoke-virtual {v13}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d00a5

    if-eq v3, v4, :cond_1

    invoke-virtual {v13}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d00a6

    if-eq v3, v4, :cond_1

    invoke-virtual {v13}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d00a7

    if-eq v3, v4, :cond_1

    invoke-virtual {v13}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d00a8

    if-ne v3, v4, :cond_5

    .line 367
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v3, v13}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 370
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    # getter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$000(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroupCount()I

    move-result v9

    .line 371
    .local v9, "count":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    if-ge v14, v9, :cond_3

    .line 372
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    # getter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHeadersListView:Landroid/widget/ExpandableListView;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$100(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)Landroid/widget/ExpandableListView;

    move-result-object v3

    invoke-virtual {v3, v14}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 371
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 346
    .end local v9    # "count":I
    .end local v13    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v14    # "i":I
    .restart local v12    # "foldIcon":Landroid/widget/ImageView;
    :cond_2
    new-instance v2, Landroid/view/animation/RotateAnimation;

    const/high16 v3, 0x43340000    # 180.0f

    const/high16 v4, 0x43b40000    # 360.0f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v2 .. v8}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 349
    .restart local v2    # "anim":Landroid/view/animation/RotateAnimation;
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 350
    const-wide/16 v4, 0x2bc

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 351
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V

    .line 352
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 353
    invoke-virtual {v12, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 374
    .end local v2    # "anim":Landroid/view/animation/RotateAnimation;
    .end local v12    # "foldIcon":Landroid/widget/ImageView;
    .restart local v9    # "count":I
    .restart local v13    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .restart local v14    # "i":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    const/4 v4, 0x0

    # setter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mExpandedGroup:I
    invoke-static {v3, v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$202(Lcom/samsung/helphub/fragments/AllHelpSectionsList;I)I

    .line 375
    const/16 v16, 0x1

    .line 494
    .end local v9    # "count":I
    .end local v14    # "i":I
    :cond_4
    :goto_2
    const/4 v3, 0x0

    return v3

    .line 376
    :cond_5
    invoke-virtual {v13}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d00c7

    if-ne v3, v4, :cond_6

    .line 377
    const/4 v3, 0x3

    new-array v0, v3, [J

    move-object/from16 v17, v0

    fill-array-data v17, :array_0

    .line 378
    .local v17, "timeStemp":[J
    const/4 v3, 0x3

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a06ac

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a06ad

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a06ae

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    .line 383
    .local v10, "description":[Ljava/lang/String;
    const/4 v3, 0x3

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a01a3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v18, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a01db

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v18, v3

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a019d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v18, v3

    .line 389
    .local v18, "title":[Ljava/lang/String;
    new-instance v15, Landroid/content/Intent;

    const-string v3, "android.intent.action.START_HELP_CLIP"

    invoke-direct {v15, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 408
    .local v15, "mIntent":Landroid/content/Intent;
    const-string v3, "file:///system/media/video/motion.mp4"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "video/mp4"

    invoke-virtual {v15, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 409
    const-string v3, "HELP_TIME_STEMP"

    move-object/from16 v0, v17

    invoke-virtual {v15, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 410
    const-string v3, "HELP_DESCRIPTION"

    invoke-virtual {v15, v3, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 411
    const-string v3, "HELP_TITLE"

    move-object/from16 v0, v18

    invoke-virtual {v15, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 412
    const-string v3, "HELP_START_CLIP"

    const/4 v4, 0x0

    invoke-virtual {v15, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 413
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v3, v15}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->startActivity(Landroid/content/Intent;)V

    .line 415
    const/16 v16, 0x1

    .line 416
    goto/16 :goto_2

    .end local v10    # "description":[Ljava/lang/String;
    .end local v15    # "mIntent":Landroid/content/Intent;
    .end local v17    # "timeStemp":[J
    .end local v18    # "title":[Ljava/lang/String;
    :cond_6
    invoke-virtual {v13}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d00c8

    if-ne v3, v4, :cond_7

    .line 417
    const/4 v3, 0x2

    new-array v0, v3, [J

    move-object/from16 v17, v0

    fill-array-data v17, :array_1

    .line 418
    .restart local v17    # "timeStemp":[J
    const/4 v3, 0x2

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a013c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a06b0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    .line 422
    .restart local v10    # "description":[Ljava/lang/String;
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a013b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v18, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a06af

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v18, v3

    .line 427
    .restart local v18    # "title":[Ljava/lang/String;
    new-instance v15, Landroid/content/Intent;

    const-string v3, "android.intent.action.START_HELP_CLIP"

    invoke-direct {v15, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 446
    .restart local v15    # "mIntent":Landroid/content/Intent;
    const-string v3, "file:///system/media/video/camera.mp4"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "video/mp4"

    invoke-virtual {v15, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 447
    const-string v3, "HELP_TIME_STEMP"

    move-object/from16 v0, v17

    invoke-virtual {v15, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 448
    const-string v3, "HELP_DESCRIPTION"

    invoke-virtual {v15, v3, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 449
    const-string v3, "HELP_TITLE"

    move-object/from16 v0, v18

    invoke-virtual {v15, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 450
    const-string v3, "HELP_START_CLIP"

    const/4 v4, 0x0

    invoke-virtual {v15, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 451
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v3, v15}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->startActivity(Landroid/content/Intent;)V

    .line 453
    const/16 v16, 0x1

    .line 454
    goto/16 :goto_2

    .end local v10    # "description":[Ljava/lang/String;
    .end local v15    # "mIntent":Landroid/content/Intent;
    .end local v17    # "timeStemp":[J
    .end local v18    # "title":[Ljava/lang/String;
    :cond_7
    invoke-virtual {v13}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d00c9

    if-ne v3, v4, :cond_4

    .line 455
    const/4 v3, 0x2

    new-array v0, v3, [J

    move-object/from16 v17, v0

    fill-array-data v17, :array_2

    .line 456
    .restart local v17    # "timeStemp":[J
    const/4 v3, 0x2

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a03fe

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0402

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    .line 460
    .restart local v10    # "description":[Ljava/lang/String;
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a03fa

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v18, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a03fb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v18, v3

    .line 465
    .restart local v18    # "title":[Ljava/lang/String;
    new-instance v15, Landroid/content/Intent;

    const-string v3, "android.intent.action.START_HELP_CLIP"

    invoke-direct {v15, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 484
    .restart local v15    # "mIntent":Landroid/content/Intent;
    const-string v3, "file:///system/media/video/group_play.mp4"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "video/mp4"

    invoke-virtual {v15, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 485
    const-string v3, "HELP_TIME_STEMP"

    move-object/from16 v0, v17

    invoke-virtual {v15, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 486
    const-string v3, "HELP_DESCRIPTION"

    invoke-virtual {v15, v3, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 487
    const-string v3, "HELP_TITLE"

    move-object/from16 v0, v18

    invoke-virtual {v15, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 488
    const-string v3, "HELP_START_CLIP"

    const/4 v4, 0x0

    invoke-virtual {v15, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 489
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v3, v15}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->startActivity(Landroid/content/Intent;)V

    .line 491
    const/16 v16, 0x1

    goto/16 :goto_2

    .line 377
    :array_0
    .array-data 8
        0x0
        0x4268
        0x7d00
    .end array-data

    .line 417
    :array_1
    .array-data 8
        0x0
        0x6d60
    .end array-data

    .line 455
    :array_2
    .array-data 8
        0x0
        0x7148
    .end array-data
.end method
