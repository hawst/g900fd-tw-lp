.class Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$5;
.super Ljava/lang/Object;
.source "HeaderContentsSearchList.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;)V
    .locals 0

    .prologue
    .line 403
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$5;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 406
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$5;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->access$300(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;)Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/helphub/headers/HelpHeader;

    .line 408
    .local v7, "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourCategory(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$5;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;

    invoke-virtual {v0, v7}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->openGuideTour(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 441
    :goto_0
    return-void

    .line 410
    :cond_0
    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    const v2, 0x7f0d00a3

    if-ne v0, v2, :cond_1

    .line 411
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$5;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;

    invoke-virtual {v0, v7}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->openOnlineHelp(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 412
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$5;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->access$300(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;)Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->setActiveItem(I)V

    goto :goto_0

    .line 413
    :cond_1
    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->isVideoPath()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 414
    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$5;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;

    invoke-virtual {v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/helphub/HelpHubCommon;->startVideo(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 417
    :cond_2
    const/4 v9, 0x0

    .line 418
    .local v9, "owner":Lcom/samsung/helphub/headers/HelpHeader;
    const/4 v6, 0x0

    .line 420
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->getOwnerResId()I

    move-result v0

    if-lez v0, :cond_3

    .line 421
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "content://com.samsung.helphub.provider.search/header/owner/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->getOwnerResId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 422
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$5;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;

    invoke-virtual {v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 424
    if-eqz v6, :cond_3

    .line 425
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 426
    invoke-static {v6}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 430
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_3
    if-eqz v6, :cond_4

    .line 431
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 434
    :cond_4
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$5;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->access$300(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;)Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->setActiveItem(I)V

    .line 435
    const/4 v8, 0x1

    .line 436
    .local v8, "isPage":Z
    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v0

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-eq v0, v2, :cond_5

    .line 437
    const/4 v8, 0x0

    .line 439
    :cond_5
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$5;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;

    invoke-virtual {v0, v9, v7, v8}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->openHelpPage(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Z)V

    goto/16 :goto_0

    .line 430
    .end local v8    # "isPage":Z
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_6

    .line 431
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0
.end method
