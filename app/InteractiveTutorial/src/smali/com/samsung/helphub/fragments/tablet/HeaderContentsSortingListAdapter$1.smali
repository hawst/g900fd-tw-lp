.class Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$1;
.super Ljava/lang/Object;
.source "HeaderContentsSortingListAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->getSortHedarList(Landroid/database/Cursor;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/samsung/helphub/headers/HelpHeader;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;)I
    .locals 10
    .param p1, "header1"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p2, "header2"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    const v9, 0x7f0d00c9

    const v8, 0x7f0d00c8

    const v7, 0x7f0d00c7

    .line 182
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v2

    .line 183
    .local v2, "mCollator":Ljava/text/Collator;
    const/4 v0, 0x0

    .line 184
    .local v0, "isHeader1Video":Z
    const/4 v1, 0x0

    .line 186
    .local v1, "isHeader2Video":Z
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v6

    if-eq v6, v8, :cond_0

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v6

    if-eq v6, v7, :cond_0

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v6

    if-ne v6, v9, :cond_1

    .line 189
    :cond_0
    const/4 v0, 0x1

    .line 191
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v6

    if-eq v6, v8, :cond_2

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v6

    if-eq v6, v7, :cond_2

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v6

    if-ne v6, v9, :cond_3

    .line 194
    :cond_2
    const/4 v1, 0x1

    .line 196
    :cond_3
    const/4 v3, 0x0

    .line 197
    .local v3, "result":I
    xor-int v6, v0, v1

    if-eqz v6, :cond_5

    .line 198
    if-eqz v0, :cond_4

    .line 199
    const/4 v3, -0x1

    .line 226
    :goto_0
    return v3

    .line 201
    :cond_4
    const/4 v3, 0x1

    goto :goto_0

    .line 203
    :cond_5
    if-eqz v0, :cond_8

    if-eqz v1, :cond_8

    .line 204
    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 205
    .local v4, "s1":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 206
    .local v5, "s2":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v6

    if-nez v6, :cond_6

    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isHKTWModel()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 207
    :cond_6
    invoke-virtual {v2, v4, v5}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    .line 209
    :cond_7
    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    .line 211
    .end local v4    # "s1":Ljava/lang/String;
    .end local v5    # "s2":Ljava/lang/String;
    :cond_8
    if-eqz v0, :cond_a

    if-eqz v1, :cond_a

    .line 212
    if-eqz v0, :cond_9

    .line 213
    const/4 v3, -0x1

    goto :goto_0

    .line 215
    :cond_9
    const/4 v3, 0x1

    goto :goto_0

    .line 218
    :cond_a
    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 219
    .restart local v4    # "s1":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 220
    .restart local v5    # "s2":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v6

    if-nez v6, :cond_b

    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isHKTWModel()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 221
    :cond_b
    invoke-virtual {v2, v4, v5}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    .line 223
    :cond_c
    invoke-virtual {v4, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 179
    check-cast p1, Lcom/samsung/helphub/headers/HelpHeader;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/samsung/helphub/headers/HelpHeader;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$1;->compare(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;)I

    move-result v0

    return v0
.end method
