.class public Lcom/samsung/helphub/fragments/SearchableHeaderList;
.super Lcom/samsung/helphub/fragments/HelpHeaderList;
.source "SearchableHeaderList.java"

# interfaces
.implements Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;


# static fields
.field private static final BUNDLE_KEY_IS_DIALOG_SORT_MODE_OPENED:Ljava/lang/String; = "category_help:is_sort_dialog_opened"

.field private static final BUNDLE_KEY_SEARCH_VIEW_IS_OPEN:Ljava/lang/String; = "category_help:search_view_is_open"

.field private static final BUNDLE_KEY_SEARCH_VIEW_STRING:Ljava/lang/String; = "category_help:search_view_string"


# instance fields
.field private mActionBar:Landroid/app/ActionBar;

.field private mIsSearchViewOpen:Z

.field private mMenuItemSearch:Landroid/view/MenuItem;

.field private mMenuItemSort:Landroid/view/MenuItem;

.field private mSearchString:Ljava/lang/String;

.field private mSearchView:Landroid/widget/SearchView;

.field private mSortModeDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mIsSearchViewOpen:Z

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/fragments/SearchableHeaderList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/SearchableHeaderList;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->hideSearchView()V

    return-void
.end method

.method private getSelectSortMode()V
    .locals 5

    .prologue
    .line 352
    const/4 v0, 0x0

    .line 353
    .local v0, "destination":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 355
    const-class v3, Lcom/samsung/helphub/fragments/CategoryHeaderList;

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    .line 360
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v0}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 361
    .local v1, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 364
    .local v2, "transaction":Landroid/app/FragmentTransaction;
    const/16 v3, 0x1001

    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 365
    const v3, 0x7f0d0004

    invoke-virtual {v2, v3, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 366
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 367
    return-void

    .line 358
    .end local v1    # "fragment":Landroid/app/Fragment;
    .end local v2    # "transaction":Landroid/app/FragmentTransaction;
    :cond_0
    const-class v3, Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private hideSearchView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 277
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 278
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    const-string v1, ""

    invoke-virtual {v0, v1, v3}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 279
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 281
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 283
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 288
    :goto_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 290
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSearch:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 291
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSort:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 292
    return-void

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto :goto_0
.end method

.method private hidesort()V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSort:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 297
    return-void
.end method

.method private showSearchView()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 310
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSearch:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 311
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 313
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 314
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 317
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 319
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 320
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSort:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 321
    return-void
.end method

.method private showsort()V
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSort:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 302
    return-void
.end method


# virtual methods
.method protected getHeaderXmlID()I
    .locals 3

    .prologue
    .line 371
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "easy_mode_switch"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 373
    const v0, 0x7f060001

    .line 375
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x7f060000

    goto :goto_0
.end method

.method protected getSelectSearchIcon()V
    .locals 3

    .prologue
    .line 324
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/samsung/helphub/HelpHubSearchActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 325
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->startActivity(Landroid/content/Intent;)V

    .line 326
    return-void
.end method

.method protected getSelectSortIcon()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 329
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 330
    .local v0, "currentActivity":Landroid/app/Activity;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/samsung/helphub/HelpHubActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 332
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/samsung/helphub/HelpHubActivity;->getAionbarType()I

    move-result v2

    if-ne v2, v4, :cond_0

    .line 333
    const-string v2, "actionbar"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 334
    const/16 v2, 0x9

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubActivity;->setAionbarType(I)V

    .line 336
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 337
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->startActivity(Landroid/content/Intent;)V

    .line 345
    :goto_0
    return-void

    .line 340
    :cond_0
    const-string v2, "actionbar"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 341
    invoke-static {v4}, Lcom/samsung/helphub/HelpHubActivity;->setAionbarType(I)V

    .line 342
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 343
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const/high16 v2, 0x7f0f0000

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 120
    const v1, 0x7f0d0457

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSearch:Landroid/view/MenuItem;

    .line 121
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mActionBar:Landroid/app/ActionBar;

    const v2, 0x7f040002

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 122
    const v1, 0x7f0d0456

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSort:Landroid/view/MenuItem;

    .line 124
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 125
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSort:Landroid/view/MenuItem;

    const v2, 0x7f0a0135

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 126
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSort:Landroid/view/MenuItem;

    const v2, 0x7f0201d4

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 133
    :goto_0
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SearchView;

    iput-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    .line 134
    iget-boolean v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mIsSearchViewOpen:Z

    if-eqz v1, :cond_0

    .line 135
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->showSearchView()V

    .line 136
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchString:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 139
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "search"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 142
    .local v0, "searchManager":Landroid/app/SearchManager;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/samsung/helphub/HelpHubSearchActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 146
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    new-instance v2, Lcom/samsung/helphub/fragments/SearchableHeaderList$1;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList$1;-><init>(Lcom/samsung/helphub/fragments/SearchableHeaderList;)V

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 161
    invoke-super {p0, p1, p2}, Lcom/samsung/helphub/fragments/HelpHeaderList;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 162
    return-void

    .line 128
    .end local v0    # "searchManager":Landroid/app/SearchManager;
    :cond_1
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSort:Landroid/view/MenuItem;

    const v2, 0x7f0a0136

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 129
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSort:Landroid/view/MenuItem;

    const v2, 0x7f0201d2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/fragments/HelpHeaderList;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 53
    .local v0, "result":Landroid/view/View;
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->setHasOptionsMenu(Z)V

    .line 55
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mActionBar:Landroid/app/ActionBar;

    .line 57
    if-eqz p3, :cond_1

    .line 58
    const-string v1, "category_help:search_view_is_open"

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mIsSearchViewOpen:Z

    .line 60
    iget-boolean v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mIsSearchViewOpen:Z

    if-eqz v1, :cond_0

    .line 61
    const-string v1, "category_help:search_view_string"

    const-string v2, ""

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchString:Ljava/lang/String;

    .line 68
    :cond_0
    :goto_0
    return-object v0

    .line 64
    :cond_1
    iput-boolean v2, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mIsSearchViewOpen:Z

    .line 65
    const-string v1, ""

    iput-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchString:Ljava/lang/String;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSortModeDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSortModeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSortModeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 383
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSortModeDialog:Landroid/app/AlertDialog;

    .line 386
    :cond_0
    invoke-super {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->onDestroy()V

    .line 387
    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 84
    .local v0, "focusView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 89
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v2, :cond_0

    .line 90
    iget-object v2, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v2}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 94
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v2, :cond_1

    .line 95
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->hideSearchView()V

    .line 97
    :cond_1
    invoke-super {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->onDestroy()V

    .line 98
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 255
    const/4 v0, 0x0

    .line 256
    .local v0, "result":Z
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 257
    const/16 v1, 0x52

    if-ne p1, v1, :cond_2

    .line 258
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 259
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v1, :cond_1

    .line 260
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSearch:Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    .line 261
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->showSearchView()V

    .line 263
    :cond_0
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->hidesort()V

    .line 265
    :cond_1
    const/4 v0, 0x1

    .line 269
    :cond_2
    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 224
    const/4 v0, 0x0

    .line 226
    .local v0, "result":Z
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v1, :cond_5

    .line 227
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->isIconified()Z

    move-result v1

    if-nez v1, :cond_2

    .line 228
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSort:Landroid/view/MenuItem;

    if-eqz v1, :cond_1

    .line 229
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSearch:Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    .line 230
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->hideSearchView()V

    .line 232
    :cond_0
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->showsort()V

    .line 234
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    iget v2, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mTitleResId:I

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(I)V

    .line 235
    const/4 v0, 0x1

    .line 237
    :cond_2
    const/16 v1, 0x54

    if-ne p1, v1, :cond_5

    .line 238
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSort:Landroid/view/MenuItem;

    if-eqz v1, :cond_4

    .line 239
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSearch:Landroid/view/MenuItem;

    if-eqz v1, :cond_3

    .line 240
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 241
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->showSearchView()V

    .line 246
    :cond_3
    :goto_0
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->hidesort()V

    .line 248
    :cond_4
    const/4 v0, 0x1

    .line 251
    :cond_5
    return v0

    .line 243
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getSelectSearchIcon()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 166
    const/4 v2, 0x0

    .line 168
    .local v2, "result":Z
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x7f0d0457

    if-ne v3, v4, :cond_1

    .line 184
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getSelectSearchIcon()V

    .line 185
    const/4 v2, 0x1

    .line 216
    :cond_0
    :goto_0
    return v2

    .line 186
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x7f0d0456

    if-ne v3, v4, :cond_3

    .line 187
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 188
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getSelectSortIcon()V

    goto :goto_0

    .line 190
    :cond_2
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getSelectSortMode()V

    goto :goto_0

    .line 191
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x102002c

    if-ne v3, v4, :cond_7

    .line 192
    iget-object v3, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v3}, Landroid/widget/SearchView;->isIconified()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 193
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 194
    .local v0, "focusView":Landroid/view/View;
    iget-object v3, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSort:Landroid/view/MenuItem;

    if-eqz v3, :cond_6

    .line 195
    iget-object v3, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mMenuItemSearch:Landroid/view/MenuItem;

    if-eqz v3, :cond_5

    .line 196
    if-eqz v0, :cond_4

    .line 197
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "input_method"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 202
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_4

    .line 203
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 206
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_4
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->hideSearchView()V

    .line 208
    :cond_5
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->showsort()V

    .line 210
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    iget v4, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mTitleResId:I

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setTitle(I)V

    goto :goto_0

    .line 213
    .end local v0    # "focusView":Landroid/view/View;
    :cond_7
    invoke-super {p0, p1}, Lcom/samsung/helphub/fragments/HelpHeaderList;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->onResume()V

    .line 75
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    check-cast v0, Lcom/samsung/helphub/widget/HelpHubSearchView;

    invoke-virtual {v0}, Lcom/samsung/helphub/widget/HelpHubSearchView;->forceSuggestion()V

    .line 78
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 102
    invoke-super {p0, p1}, Lcom/samsung/helphub/fragments/HelpHeaderList;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 105
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->isIconified()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    const-string v1, "category_help:search_view_string"

    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v0, "category_help:search_view_is_open"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSortModeDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchableHeaderList;->mSortModeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    const-string v0, "category_help:is_sort_dialog_opened"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 114
    :cond_1
    return-void
.end method
