.class Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$MoveExpandingItemRunnable;
.super Ljava/lang/Object;
.source "HeaderContentsSearchList.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MoveExpandingItemRunnable"
.end annotation


# instance fields
.field private mMovingListView:Landroid/widget/ListView;

.field private mPosition:I


# direct methods
.method constructor <init>(Landroid/widget/ListView;I)V
    .locals 0
    .param p1, "listView"    # Landroid/widget/ListView;
    .param p2, "position"    # I

    .prologue
    .line 449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$MoveExpandingItemRunnable;->mMovingListView:Landroid/widget/ListView;

    .line 451
    iput p2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$MoveExpandingItemRunnable;->mPosition:I

    .line 452
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 456
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$MoveExpandingItemRunnable;->mMovingListView:Landroid/widget/ListView;

    iget v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$MoveExpandingItemRunnable;->mPosition:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    .line 457
    return-void
.end method
