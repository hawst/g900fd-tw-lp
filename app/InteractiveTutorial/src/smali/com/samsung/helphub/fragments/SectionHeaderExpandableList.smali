.class public Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;
.super Landroid/app/Fragment;
.source "SectionHeaderExpandableList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$Collapser;
    }
.end annotation


# static fields
.field public static final ARG_KEY_EXPANDED_SECTION_NAME:Ljava/lang/String; = "section_list:expanded_section_name"

.field private static final BUNDLE_KEY_EXPANDED_GROUP_POSITION:Ljava/lang/String; = "section_list:expanded_group"


# instance fields
.field private mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

.field private mExpandedGroupPosition:I

.field public mHandler:Landroid/os/Handler;

.field private mHeaderXmlID:I

.field private mHeadersListView:Landroid/widget/ExpandableListView;

.field private mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

.field mOnGroupExpandListener:Landroid/widget/ExpandableListView$OnGroupExpandListener;

.field private mTitleResId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 53
    const v0, 0x7f0a001e

    iput v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mTitleResId:I

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mExpandedGroupPosition:I

    .line 198
    new-instance v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$2;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$2;-><init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mOnGroupExpandListener:Landroid/widget/ExpandableListView$OnGroupExpandListener;

    .line 234
    new-instance v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;-><init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

    .line 451
    new-instance v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$4;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$4;-><init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mExpandedGroupPosition:I

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)Landroid/widget/ExpandableListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeadersListView:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method private sendBroadcastForNetworkErrorPopup(I)V
    .locals 3
    .param p1, "status"    # I

    .prologue
    .line 444
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 445
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.popupuireceiver"

    const-string v2, "com.sec.android.app.popupuireceiver.popupNetworkError"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 447
    const-string v1, "network_err_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 448
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->startActivityForResult(Landroid/content/Intent;I)V

    .line 449
    return-void
.end method


# virtual methods
.method public checkNetworkState()V
    .locals 6

    .prologue
    .line 422
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 423
    new-instance v1, Lcom/samsung/helphub/UpdateCheckThread;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHandler:Landroid/os/Handler;

    const-string v5, "com.samsung.helpplugin"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/samsung/helphub/UpdateCheckThread;-><init>(Landroid/content/Context;ZLandroid/os/Handler;Ljava/lang/String;)V

    .line 425
    .local v1, "thread":Lcom/samsung/helphub/UpdateCheckThread;
    invoke-virtual {v1}, Lcom/samsung/helphub/UpdateCheckThread;->start()V

    .line 441
    .end local v1    # "thread":Lcom/samsung/helphub/UpdateCheckThread;
    :goto_0
    return-void

    .line 427
    :cond_0
    const/4 v0, -0x1

    .line 428
    .local v0, "networkStatus":I
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isFligtMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 429
    const/4 v0, 0x0

    .line 439
    :goto_1
    invoke-direct {p0, v0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->sendBroadcastForNetworkErrorPopup(I)V

    goto :goto_0

    .line 430
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isMobileDataOff(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 431
    const/4 v0, 0x1

    goto :goto_1

    .line 432
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isRoamingOff(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 433
    const/4 v0, 0x2

    goto :goto_1

    .line 434
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isReachToDataLimit(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 435
    const/4 v0, 0x3

    goto :goto_1

    .line 437
    :cond_4
    const/4 v0, 0x4

    goto :goto_1
.end method

.method protected isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 405
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v0

    .line 407
    .local v0, "enable":I
    const/4 v3, 0x2

    if-eq v3, v0, :cond_0

    const/4 v3, 0x3

    if-ne v3, v0, :cond_1

    .line 409
    :cond_0
    const-string v3, "Help_TryIt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is diabled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    .end local v0    # "enable":I
    :goto_0
    return v2

    .line 412
    .restart local v0    # "enable":I
    :cond_1
    const-string v3, "Help_TryIt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is enabled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 413
    const/4 v2, 0x1

    goto :goto_0

    .line 415
    .end local v0    # "enable":I
    :catch_0
    move-exception v1

    .line 416
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    const-string v3, "Help_TryIt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not installed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;I)V
    .locals 5
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p2, "titleId"    # I

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 179
    new-instance v0, Landroid/os/Bundle;

    const/4 v3, 0x2

    invoke-direct {v0, v3}, Landroid/os/Bundle;-><init>(I)V

    .line 180
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "helpub:header"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 181
    const-string v3, "page_fragment_argument:activity_title"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 183
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v1

    .line 185
    .local v1, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 188
    .local v2, "transaction":Landroid/app/FragmentTransaction;
    const/16 v3, 0x1001

    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 189
    const v3, 0x7f0d0004

    invoke-virtual {v2, v3, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 190
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 191
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 193
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "fragment":Landroid/app/Fragment;
    .end local v2    # "transaction":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, -0x1

    .line 60
    const/4 v1, 0x0

    .line 61
    .local v1, "expandedSectionName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 64
    .local v2, "externalHeader":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "easy_mode_switch"

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-nez v6, :cond_6

    .line 66
    const v6, 0x7f060001

    iput v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeaderXmlID:I

    .line 71
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 72
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_3

    .line 73
    const-string v6, "helpub:header"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 74
    const-string v6, "helpub:header"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/samsung/helphub/headers/HelpHeader;

    .line 75
    .local v3, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v3, :cond_2

    .line 76
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getSubHeadersArrayId()I

    move-result v6

    if-eq v6, v9, :cond_0

    .line 77
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getSubHeadersArrayId()I

    move-result v6

    iput v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeaderXmlID:I

    .line 81
    :cond_0
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->isExternal()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getActivityTitleId()I

    move-result v6

    if-eq v6, v9, :cond_1

    .line 83
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getActivityTitleId()I

    move-result v6

    iput v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mTitleResId:I

    .line 86
    :cond_1
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->isExternal()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 87
    move-object v2, v3

    .line 91
    .end local v3    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_2
    const-string v6, "section_list:expanded_section_name"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 92
    const-string v6, "section_list:expanded_section_name"

    invoke-virtual {v0, v6, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    :cond_3
    const v6, 0x7f0400bd

    invoke-virtual {p1, v6, p2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 97
    .local v5, "result":Landroid/view/View;
    if-eqz v2, :cond_8

    .line 98
    if-eqz v1, :cond_7

    .line 100
    new-instance v6, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    iget v7, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeaderXmlID:I

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v6, v7, v1, v8}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;-><init>(ILjava/lang/String;Landroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    .line 105
    :goto_1
    const/4 v4, 0x1

    .local v4, "isExternal":Z
    :goto_2
    move-object v6, v5

    .line 111
    check-cast v6, Landroid/widget/ExpandableListView;

    iput-object v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeadersListView:Landroid/widget/ExpandableListView;

    .line 112
    iget-object v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeadersListView:Landroid/widget/ExpandableListView;

    iget-object v7, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    invoke-virtual {v6, v7}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 113
    iget-object v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeadersListView:Landroid/widget/ExpandableListView;

    iget-object v7, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

    invoke-virtual {v6, v7}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 114
    iget-object v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeadersListView:Landroid/widget/ExpandableListView;

    new-instance v7, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$1;

    invoke-direct {v7, p0, v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$1;-><init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;Z)V

    invoke-virtual {v6, v7}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 138
    iget-object v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeadersListView:Landroid/widget/ExpandableListView;

    iget-object v7, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mOnGroupExpandListener:Landroid/widget/ExpandableListView$OnGroupExpandListener;

    invoke-virtual {v6, v7}, Landroid/widget/ExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 140
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 141
    iget-object v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    invoke-virtual {v6, v1}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getSectionPositionForName(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mExpandedGroupPosition:I

    .line 142
    if-nez v4, :cond_4

    .line 143
    const-string v6, "section_list:expanded_section_name"

    invoke-virtual {v0, v6, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_4
    if-eqz p3, :cond_5

    .line 147
    const-string v6, "section_list:expanded_group"

    invoke-virtual {p3, v6, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mExpandedGroupPosition:I

    .line 150
    :cond_5
    return-object v5

    .line 68
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v4    # "isExternal":Z
    .end local v5    # "result":Landroid/view/View;
    :cond_6
    const/high16 v6, 0x7f060000

    iput v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeaderXmlID:I

    goto/16 :goto_0

    .line 103
    .restart local v0    # "args":Landroid/os/Bundle;
    .restart local v5    # "result":Landroid/view/View;
    :cond_7
    new-instance v6, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    iget v7, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeaderXmlID:I

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;-><init>(ILandroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    goto :goto_1

    .line 107
    :cond_8
    new-instance v6, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    iget v7, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeaderXmlID:I

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;-><init>(ILandroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    .line 108
    const/4 v4, 0x0

    .restart local v4    # "isExternal":Z
    goto :goto_2
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 155
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 157
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/HelpHubActivityBase;

    invoke-virtual {v0}, Lcom/samsung/helphub/HelpHubActivityBase;->isTwoPane()Z

    move-result v0

    if-nez v0, :cond_0

    .line 158
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mTitleResId:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeadersListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->requestFocus()Z

    .line 164
    iget v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mExpandedGroupPosition:I

    if-ltz v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeadersListView:Landroid/widget/ExpandableListView;

    iget v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mExpandedGroupPosition:I

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setSelectedGroup(I)V

    .line 166
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeadersListView:Landroid/widget/ExpandableListView;

    iget v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mExpandedGroupPosition:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ExpandableListView;->expandGroup(IZ)Z

    .line 168
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 230
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 231
    const-string v0, "section_list:expanded_group"

    iget v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mExpandedGroupPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 232
    return-void
.end method
