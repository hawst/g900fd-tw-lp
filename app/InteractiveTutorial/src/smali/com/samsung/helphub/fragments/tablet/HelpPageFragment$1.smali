.class Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;
.super Ljava/lang/Object;
.source "HelpPageFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->showBreadCrumbs(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

.field final synthetic val$isFullTitle:Z

.field final synthetic val$isPage:Z

.field final synthetic val$shortTitle:Ljava/lang/CharSequence;

.field final synthetic val$title:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    iput-object p2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$title:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$shortTitle:Ljava/lang/CharSequence;

    iput-boolean p4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$isPage:Z

    iput-boolean p5, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$isFullTitle:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 154
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$000(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/app/FragmentBreadCrumbs;

    move-result-object v0

    invoke-virtual {v0, v3, v3, v3}, Landroid/app/FragmentBreadCrumbs;->setParentTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 155
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$000(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/app/FragmentBreadCrumbs;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/app/FragmentBreadCrumbs;->setTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 157
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$title:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$000(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/app/FragmentBreadCrumbs;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$shortTitle:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$shortTitle:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentBreadCrumbs;->setParentTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 160
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$100(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$100(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$title:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$isPage:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$isFullTitle:Z

    if-nez v0, :cond_2

    .line 165
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$000(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/app/FragmentBreadCrumbs;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$shortTitle:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$shortTitle:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentBreadCrumbs;->setParentTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    .line 166
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$100(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$100(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 170
    :cond_2
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    invoke-virtual {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/helphub/ExternalHelpActivity;

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 171
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$000(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/app/FragmentBreadCrumbs;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$shortTitle:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$shortTitle:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentBreadCrumbs;->setTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 179
    :goto_1
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$100(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$100(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 173
    :cond_3
    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v0

    if-nez v0, :cond_4

    .line 174
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$000(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/app/FragmentBreadCrumbs;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$shortTitle:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$shortTitle:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentBreadCrumbs;->setTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 176
    :cond_4
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$000(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/app/FragmentBreadCrumbs;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$title:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;->val$title:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentBreadCrumbs;->setParentTitle(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method
