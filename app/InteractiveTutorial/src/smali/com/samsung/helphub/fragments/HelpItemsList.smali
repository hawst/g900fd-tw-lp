.class public Lcom/samsung/helphub/fragments/HelpItemsList;
.super Landroid/app/Fragment;
.source "HelpItemsList.java"

# interfaces
.implements Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/HelpItemsList$MoveExpandingItemRunnable;
    }
.end annotation


# static fields
.field private static final SAVE_BUNDLE_KEY_OPENED_ITEM:Ljava/lang/String; = "item_list:opened_item"


# instance fields
.field private mHeaderXmlId:I

.field private mItemClickListner:Landroid/widget/AdapterView$OnItemClickListener;

.field private mListAdapter:Lcom/samsung/helphub/fragments/HelpItemsListAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mTitleResId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 22
    const v0, 0x7f0a001e

    iput v0, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mTitleResId:I

    .line 92
    new-instance v0, Lcom/samsung/helphub/fragments/HelpItemsList$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/HelpItemsList$1;-><init>(Lcom/samsung/helphub/fragments/HelpItemsList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mItemClickListner:Landroid/widget/AdapterView$OnItemClickListener;

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/fragments/HelpItemsList;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/HelpItemsList;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListView:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    const/4 v7, -0x1

    .line 34
    const/4 v3, 0x0

    .line 35
    .local v3, "result":Landroid/view/View;
    const v4, 0x7f0400c4

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 37
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpItemsList;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "easy_mode_switch"

    invoke-static {v4, v5, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-nez v4, :cond_3

    .line 39
    const v4, 0x7f060001

    iput v4, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mHeaderXmlId:I

    .line 44
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpItemsList;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 45
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    const-string v4, "helpub:header"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 46
    const-string v4, "helpub:header"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/headers/HelpHeader;

    .line 47
    .local v1, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v1, :cond_1

    .line 49
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getActivityTitleId()I

    move-result v4

    if-eq v4, v7, :cond_0

    .line 50
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getActivityTitleId()I

    move-result v4

    iput v4, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mTitleResId:I

    .line 52
    :cond_0
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getSubHeadersArrayId()I

    move-result v4

    iput v4, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mHeaderXmlId:I

    .line 56
    .end local v1    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_1
    new-instance v4, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpItemsList;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iget v6, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mHeaderXmlId:I

    invoke-direct {v4, v5, v6, p0}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;-><init>(Landroid/app/Activity;ILandroid/app/Fragment;)V

    iput-object v4, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListAdapter:Lcom/samsung/helphub/fragments/HelpItemsListAdapter;

    .line 57
    iget-object v4, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListAdapter:Lcom/samsung/helphub/fragments/HelpItemsListAdapter;

    invoke-virtual {v4, p0}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->setOnHelpItemViewClickListener(Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;)V

    .line 58
    const v4, 0x102000a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListView:Landroid/widget/ListView;

    .line 59
    iget-object v4, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListAdapter:Lcom/samsung/helphub/fragments/HelpItemsListAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 60
    iget-object v4, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mItemClickListner:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 62
    if-eqz p3, :cond_2

    .line 63
    const-string v4, "item_list:opened_item"

    invoke-virtual {p3, v4, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 64
    .local v2, "position":I
    if-ltz v2, :cond_2

    .line 65
    iget-object v4, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListAdapter:Lcom/samsung/helphub/fragments/HelpItemsListAdapter;

    invoke-virtual {v4, v2, v8}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->toggle(IZ)V

    .line 67
    iget-object v4, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListView:Landroid/widget/ListView;

    new-instance v5, Lcom/samsung/helphub/fragments/HelpItemsList$MoveExpandingItemRunnable;

    iget-object v6, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListView:Landroid/widget/ListView;

    invoke-direct {v5, v6, v2}, Lcom/samsung/helphub/fragments/HelpItemsList$MoveExpandingItemRunnable;-><init>(Landroid/widget/ListView;I)V

    const-wide/16 v6, 0x64

    invoke-virtual {v4, v5, v6, v7}, Landroid/widget/ListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 70
    .end local v2    # "position":I
    :cond_2
    return-object v3

    .line 41
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_3
    const/high16 v4, 0x7f060000

    iput v4, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mHeaderXmlId:I

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListAdapter:Lcom/samsung/helphub/fragments/HelpItemsListAdapter;

    invoke-virtual {v0}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->destroy()V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListAdapter:Lcom/samsung/helphub/fragments/HelpItemsListAdapter;

    .line 127
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 128
    return-void
.end method

.method public onHelpItemClick(Landroid/view/View;I)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;

    invoke-virtual {v0, p2}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->toggle(I)V

    .line 103
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/helphub/fragments/HelpItemsList$MoveExpandingItemRunnable;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListView:Landroid/widget/ListView;

    invoke-direct {v1, v2, p2}, Lcom/samsung/helphub/fragments/HelpItemsList$MoveExpandingItemRunnable;-><init>(Landroid/widget/ListView;I)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 104
    const/4 v0, 0x1

    return v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 75
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 77
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpItemsList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/HelpHubActivityBase;

    invoke-virtual {v0}, Lcom/samsung/helphub/HelpHubActivityBase;->isTwoPane()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpItemsList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget v1, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mTitleResId:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 80
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 87
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListAdapter:Lcom/samsung/helphub/fragments/HelpItemsListAdapter;

    if-eqz v0, :cond_0

    .line 88
    const-string v0, "item_list:opened_item"

    iget-object v1, p0, Lcom/samsung/helphub/fragments/HelpItemsList;->mListAdapter:Lcom/samsung/helphub/fragments/HelpItemsListAdapter;

    invoke-virtual {v1}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->getOpenedItem()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 90
    :cond_0
    return-void
.end method
