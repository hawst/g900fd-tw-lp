.class public Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;
.super Landroid/app/Fragment;
.source "HeaderContentsSearchableList.java"

# interfaces
.implements Lcom/samsung/helphub/fragments/HelpFragmentKeyListener;


# static fields
.field private static final BUNDLE_KEY_IS_DIALOG_SORT_MODE_OPENED:Ljava/lang/String; = "category_help:is_sort_dialog_opened"

.field private static final BUNDLE_KEY_SEARCH_VIEW_IS_OPEN:Ljava/lang/String; = "category_help:search_view_is_open"

.field private static final BUNDLE_KEY_SEARCH_VIEW_STRING:Ljava/lang/String; = "category_help:search_view_string"

.field public static final TAG_HELP_PAGE_FRAGMENT:Ljava/lang/String; = "tag:help_page_fragment"


# instance fields
.field private mActionBar:Landroid/app/ActionBar;

.field protected mHelpPageContainer:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

.field private mIsSearchViewOpen:Z

.field private mMenuItemSearch:Landroid/view/MenuItem;

.field private mMenuItemSort:Landroid/view/MenuItem;

.field private mSearchString:Ljava/lang/String;

.field protected mSearchView:Landroid/widget/SearchView;

.field private mSortModeDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mIsSearchViewOpen:Z

    return-void
.end method

.method private getSelectSortMode()V
    .locals 6

    .prologue
    .line 349
    const/4 v0, 0x0

    .line 350
    .local v0, "destination":Ljava/lang/String;
    const-string v2, "contents"

    .line 351
    .local v2, "tag":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 353
    const-class v4, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    .line 358
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, v0}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 359
    .local v1, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 361
    .local v3, "transaction":Landroid/app/FragmentTransaction;
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 362
    const v4, 0x7f0d0004

    invoke-virtual {v3, v4, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 363
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 364
    return-void

    .line 355
    .end local v1    # "fragment":Landroid/app/Fragment;
    .end local v3    # "transaction":Landroid/app/FragmentTransaction;
    :cond_0
    const-class v4, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    .line 356
    const-string v2, "sort"

    goto :goto_0
.end method

.method private hideSearchView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 294
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setVisibility(I)V

    .line 295
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setFocusable(Z)V

    .line 297
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    const-string v1, ""

    invoke-virtual {v0, v1, v3}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 298
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 299
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 301
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 302
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 303
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 305
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSearch:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 306
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSort:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 307
    return-void
.end method

.method private hidesort()V
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSort:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 312
    return-void
.end method

.method private showsort()V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSort:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 317
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 53
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->setHasOptionsMenu(Z)V

    .line 55
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mActionBar:Landroid/app/ActionBar;

    .line 57
    if-eqz p1, :cond_2

    .line 58
    const-string v2, "category_help:search_view_is_open"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mIsSearchViewOpen:Z

    .line 60
    iget-boolean v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mIsSearchViewOpen:Z

    if-eqz v2, :cond_0

    .line 61
    const-string v2, "category_help:search_view_string"

    const-string v3, ""

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchString:Ljava/lang/String;

    .line 68
    :cond_0
    :goto_0
    new-instance v2, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    invoke-direct {v2}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;-><init>()V

    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mHelpPageContainer:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    .line 70
    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v2

    if-nez v2, :cond_1

    .line 71
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 72
    .local v1, "transaction":Landroid/app/FragmentTransaction;
    const v2, 0x7f0d0087

    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mHelpPageContainer:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    const-string v4, "tag:help_page_fragment"

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 74
    const/16 v2, 0x1001

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 76
    :try_start_0
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    .end local v1    # "transaction":Landroid/app/FragmentTransaction;
    :cond_1
    :goto_1
    return-void

    .line 64
    :cond_2
    iput-boolean v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mIsSearchViewOpen:Z

    .line 65
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchString:Ljava/lang/String;

    goto :goto_0

    .line 77
    .restart local v1    # "transaction":Landroid/app/FragmentTransaction;
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    const/4 v3, 0x0

    .line 137
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const/high16 v2, 0x7f0f0000

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 138
    const v1, 0x7f0d0457

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSearch:Landroid/view/MenuItem;

    .line 139
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mActionBar:Landroid/app/ActionBar;

    const v2, 0x7f040002

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 140
    const v1, 0x7f0d0456

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSort:Landroid/view/MenuItem;

    .line 142
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 144
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSort:Landroid/view/MenuItem;

    const v2, 0x7f0a0135

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 145
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSort:Landroid/view/MenuItem;

    const v2, 0x7f0201d4

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 152
    :goto_0
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d0008

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SearchView;

    iput-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    .line 153
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setVisibility(I)V

    .line 154
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 155
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mActionBar:Landroid/app/ActionBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 157
    iget-boolean v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mIsSearchViewOpen:Z

    if-eqz v1, :cond_0

    .line 158
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->showSearchView()V

    .line 159
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchString:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 162
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "search"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 165
    .local v0, "searchManager":Landroid/app/SearchManager;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/samsung/helphub/HelpHubActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 168
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    new-instance v2, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList$1;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList$1;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;)V

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 192
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 193
    return-void

    .line 147
    .end local v0    # "searchManager":Landroid/app/SearchManager;
    :cond_1
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSort:Landroid/view/MenuItem;

    const v2, 0x7f0a0136

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 148
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSort:Landroid/view/MenuItem;

    const v2, 0x7f0201d2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSortModeDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSortModeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSortModeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 371
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSortModeDialog:Landroid/app/AlertDialog;

    .line 375
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 376
    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 87
    .local v0, "focusView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 92
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v2, :cond_0

    .line 93
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v2}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 97
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v2, :cond_1

    .line 98
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->hideSearchView()V

    .line 100
    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 101
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 197
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 247
    const/4 v1, 0x0

    .line 249
    .local v1, "result":Z
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v2, :cond_4

    .line 250
    const/4 v2, 0x4

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v2}, Landroid/widget/SearchView;->isIconified()Z

    move-result v2

    if-nez v2, :cond_1

    .line 259
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v2

    if-nez v2, :cond_5

    .line 260
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSearch:Landroid/view/MenuItem;

    if-eqz v2, :cond_0

    .line 261
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->hideSearchView()V

    .line 263
    :cond_0
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->showsort()V

    .line 271
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 272
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const v3, 0x7f0a001e

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(I)V

    .line 273
    const/4 v1, 0x1

    .line 276
    :cond_1
    const/16 v2, 0x54

    if-ne p1, v2, :cond_4

    .line 277
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSort:Landroid/view/MenuItem;

    if-eqz v2, :cond_3

    .line 278
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSearch:Landroid/view/MenuItem;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mActionBar:Landroid/app/ActionBar;

    if-eqz v2, :cond_2

    .line 279
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->showSearchView()V

    .line 281
    :cond_2
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->hidesort()V

    .line 283
    :cond_3
    const/4 v1, 0x1

    .line 286
    :cond_4
    return v1

    .line 266
    :cond_5
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->popBackStack()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 267
    :catch_0
    move-exception v0

    .line 268
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 202
    const/4 v2, 0x0

    .line 204
    .local v2, "result":Z
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x7f0d0457

    if-ne v3, v4, :cond_3

    .line 205
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSort:Landroid/view/MenuItem;

    if-eqz v3, :cond_1

    .line 206
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSearch:Landroid/view/MenuItem;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mActionBar:Landroid/app/ActionBar;

    if-eqz v3, :cond_0

    .line 207
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->showSearchView()V

    .line 209
    :cond_0
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->hidesort()V

    .line 211
    :cond_1
    const/4 v2, 0x1

    .line 239
    :cond_2
    :goto_0
    return v2

    .line 212
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x7f0d0456

    if-ne v3, v4, :cond_4

    .line 214
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getSelectSortMode()V

    goto :goto_0

    .line 215
    :cond_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    const v4, 0x102002c

    if-ne v3, v4, :cond_7

    .line 216
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v3}, Landroid/widget/SearchView;->isIconified()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 218
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 219
    .local v0, "focusView":Landroid/view/View;
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSort:Landroid/view/MenuItem;

    if-eqz v3, :cond_2

    .line 220
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSearch:Landroid/view/MenuItem;

    if-eqz v3, :cond_6

    .line 221
    if-eqz v0, :cond_5

    .line 222
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "input_method"

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 226
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_5

    .line 227
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 230
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_5
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->hideSearchView()V

    .line 232
    :cond_6
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->showsort()V

    goto :goto_0

    .line 236
    .end local v0    # "focusView":Landroid/view/View;
    :cond_7
    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_0
.end method

.method public onPause()V
    .locals 4

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 107
    .local v0, "focusView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 112
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v2, :cond_0

    .line 113
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v2}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 116
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 117
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 121
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 124
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->isIconified()Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    const-string v0, "category_help:search_view_string"

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v0, "category_help:search_view_is_open"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSortModeDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSortModeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 130
    const-string v0, "category_help:is_sort_dialog_opened"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 132
    :cond_1
    return-void
.end method

.method protected openGuideTour(Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 8
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    const v7, 0x7f0d0087

    const/16 v6, 0x1001

    const/4 v5, 0x1

    .line 458
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d009a

    if-eq v3, v4, :cond_0

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d00a8

    if-eq v3, v4, :cond_0

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d00a7

    if-eq v3, v4, :cond_0

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d00a6

    if-ne v3, v4, :cond_4

    .line 462
    :cond_0
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mHelpPageContainer:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    invoke-virtual {v3}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->isAdded()Z

    move-result v3

    if-nez v3, :cond_3

    .line 463
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mHelpPageContainer:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    invoke-virtual {v3, p1, p1, v5}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->setHeaders(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Z)V

    .line 464
    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 465
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 467
    .local v2, "transaction":Landroid/app/FragmentTransaction;
    const v3, 0x7f0d0004

    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mHelpPageContainer:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    const-string v5, "tag:help_page_fragment"

    invoke-virtual {v2, v3, v4, v5}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 468
    invoke-virtual {v2, v6}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 469
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 470
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 494
    .end local v2    # "transaction":Landroid/app/FragmentTransaction;
    :cond_1
    :goto_0
    return-void

    .line 472
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 473
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 474
    .restart local v2    # "transaction":Landroid/app/FragmentTransaction;
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mHelpPageContainer:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    const-string v4, "tag:help_page_fragment"

    invoke-virtual {v2, v7, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 476
    invoke-virtual {v2, v6}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 477
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0

    .line 481
    .end local v2    # "transaction":Landroid/app/FragmentTransaction;
    :cond_3
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mHelpPageContainer:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    invoke-virtual {v3, p1, p1, v5}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->switchToHelpPage(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Z)V

    goto :goto_0

    .line 484
    :cond_4
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 485
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 486
    .restart local v2    # "transaction":Landroid/app/FragmentTransaction;
    const-string v3, "helpub:header"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 487
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v1

    .line 489
    .local v1, "fragment":Landroid/app/Fragment;
    const-string v3, "help_page:guide_tours"

    invoke-virtual {v2, v7, v1, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 491
    invoke-virtual {v2, v6}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 492
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method protected openHelpPage(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 3
    .param p1, "owner"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p2, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 385
    const/4 v0, 0x0

    .line 387
    .local v0, "isPage":Z
    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v1

    const v2, 0x7f0d0103

    if-eq v1, v2, :cond_0

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v1

    const v2, 0x7f0d0102

    if-eq v1, v2, :cond_0

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v1

    const v2, 0x7f0d0104

    if-ne v1, v2, :cond_1

    .line 390
    :cond_0
    const/4 v0, 0x1

    .line 392
    :cond_1
    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->openHelpPage(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Z)V

    .line 393
    return-void
.end method

.method protected openHelpPage(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Z)V
    .locals 6
    .param p1, "owner"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p2, "header"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p3, "isPage"    # Z

    .prologue
    const/16 v5, 0x1001

    .line 403
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 404
    .local v0, "manager":Landroid/app/FragmentManager;
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 434
    :cond_0
    :goto_0
    return-void

    .line 407
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/samsung/helphub/HelpHubCommon;->isResourceAvailable(Lcom/samsung/helphub/headers/HelpHeader;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 410
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mHelpPageContainer:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    invoke-virtual {v2}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_3

    .line 411
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mHelpPageContainer:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    invoke-virtual {v2, p1, p2, p3}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->setHeaders(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Z)V

    .line 412
    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 413
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 414
    .local v1, "transaction":Landroid/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->setBreadCrumbTitle(Ljava/lang/CharSequence;)Landroid/app/FragmentTransaction;

    .line 415
    const v2, 0x7f0d0004

    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mHelpPageContainer:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    const-string v4, "tag:help_page_fragment"

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 416
    invoke-virtual {v1, v5}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 417
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 418
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0

    .line 420
    .end local v1    # "transaction":Landroid/app/FragmentTransaction;
    :cond_2
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 421
    .restart local v1    # "transaction":Landroid/app/FragmentTransaction;
    const v2, 0x7f0d0087

    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mHelpPageContainer:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    const-string v4, "tag:help_page_fragment"

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 423
    invoke-virtual {v1, v5}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 424
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0

    .line 428
    .end local v1    # "transaction":Landroid/app/FragmentTransaction;
    :cond_3
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mHelpPageContainer:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    invoke-virtual {v2, p1, p2, p3}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->switchToHelpPage(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Z)V

    goto :goto_0

    .line 432
    :cond_4
    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getPluginAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/helphub/HelpHubCommon;->requestDownloadingResource(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected openOnlineHelp(Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 6
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 448
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a06b1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 449
    .local v2, "outUrl":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 451
    .local v1, "mIntent":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 455
    :goto_0
    return-void

    .line 452
    :catch_0
    move-exception v0

    .line 453
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a06b3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected showSearchView()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 325
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSearch:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSearch:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSort:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 329
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mMenuItemSort:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 332
    :cond_1
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 335
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 336
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 337
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setVisibility(I)V

    .line 340
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 341
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setFocusable(Z)V

    .line 342
    return-void
.end method
