.class public Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;
.super Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;
.source "HeaderContentsSearchList.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$MoveExpandingItemRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field public static final ARGUMENT_KEY_CATEGORY_HEADER:Ljava/lang/String; = "search_list_arg:category_header"

.field public static final ARGUMENT_KEY_QUERY:Ljava/lang/String; = "search_list_arg:query"

.field private static final BUNDLE_KEY_CATEGORY_HEADER:Ljava/lang/String; = "search_list:category_header"

.field protected static final BUNDLE_KEY_QUERY:Ljava/lang/String; = "search_list:query"

.field private static final ONLINE_HELP_LOADER_ID:I = 0x1

.field private static final SEARCH_CATEGORY_LOADER_ID:I = 0x2

.field private static final SEARCH_LOADER_ID:I


# instance fields
.field private activeItemPosition:I

.field private mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

.field private mCategory:Lcom/samsung/helphub/headers/HelpHeader;

.field private mContainer:Landroid/view/ViewGroup;

.field protected mListFiller:Landroid/widget/ImageView;

.field protected mListView:Landroid/widget/ListView;

.field private mNoDataTextView:Landroid/widget/TextView;

.field private mOnClickOnlineSearchButton:Landroid/view/View$OnClickListener;

.field private mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mQuery:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mQuery:Ljava/lang/String;

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->activeItemPosition:I

    .line 403
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$5;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$5;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 463
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$6;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$6;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mOnClickOnlineSearchButton:Landroid/view/View$OnClickListener;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->makeupInitialView()V

    return-void
.end method

.method static synthetic access$102(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;
    .param p1, "x1"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->activeItemPosition:I

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->loadData()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;)Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    return-object v0
.end method

.method private loadData()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 332
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mQuery:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 333
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 337
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 338
    return-void

    .line 334
    :cond_1
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    if-eqz v0, :cond_0

    .line 335
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0
.end method

.method private makeupInitialView()V
    .locals 12

    .prologue
    const v11, 0x7f0d00a3

    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 344
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    invoke-virtual {v0}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_8

    .line 345
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v9, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 346
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    iget v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->activeItemPosition:I

    invoke-virtual {v0, v2}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/helphub/headers/HelpHeader;

    .line 347
    .local v7, "header":Lcom/samsung/helphub/headers/HelpHeader;
    const/4 v8, 0x0

    .line 348
    .local v8, "owner":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    if-eq v0, v11, :cond_1

    .line 349
    const/4 v6, 0x0

    .line 351
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->getOwnerResId()I

    move-result v0

    if-lez v0, :cond_0

    .line 352
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "content://com.samsung.helphub.provider.search/header/owner/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->getOwnerResId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 354
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 356
    if-eqz v6, :cond_0

    .line 357
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 358
    invoke-static {v6}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 362
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    if-eqz v6, :cond_1

    .line 363
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 367
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setVisibility(I)V

    .line 368
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mListFiller:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 369
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mListFiller:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 371
    :cond_2
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mNoDataTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 373
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    iget v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->activeItemPosition:I

    invoke-virtual {v0, v2}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->setActiveItem(I)V

    .line 374
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mListView:Landroid/widget/ListView;

    new-instance v2, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$MoveExpandingItemRunnable;

    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mListView:Landroid/widget/ListView;

    iget v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->activeItemPosition:I

    invoke-direct {v2, v3, v4}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$MoveExpandingItemRunnable;-><init>(Landroid/widget/ListView;I)V

    const-wide/16 v4, 0x1

    invoke-virtual {v0, v2, v4, v5}, Landroid/widget/ListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 379
    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourCategory(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 380
    invoke-virtual {p0, v7}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->openGuideTour(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 401
    .end local v7    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v8    # "owner":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_3
    :goto_0
    return-void

    .line 362
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v7    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .restart local v8    # "owner":Lcom/samsung/helphub/headers/HelpHeader;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 363
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 381
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_5
    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    if-ne v0, v11, :cond_6

    .line 382
    invoke-virtual {p0, v7}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->openOnlineHelp(Lcom/samsung/helphub/headers/HelpHeader;)V

    goto :goto_0

    .line 383
    :cond_6
    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "file"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v7}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 388
    :cond_7
    const/4 v0, 0x1

    invoke-virtual {p0, v8, v7, v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->openHelpPage(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Z)V

    goto :goto_0

    .line 393
    .end local v7    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v8    # "owner":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_8
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v10}, Landroid/widget/ListView;->setVisibility(I)V

    .line 394
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mListFiller:Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    .line 395
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mListFiller:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 397
    :cond_9
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mNoDataTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 399
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v2, -0x1

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v3, 0x7f0d00a3

    const/4 v2, 0x0

    .line 69
    invoke-super {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->onActivityCreated(Landroid/os/Bundle;)V

    .line 71
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    .line 72
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 73
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0d0004

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mContainer:Landroid/view/ViewGroup;

    .line 74
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 76
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mQuery:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 77
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourCategory(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->addHeaderItem(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 80
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$1;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$1;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 86
    :cond_2
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    if-eq v0, v3, :cond_3

    .line 87
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0

    .line 88
    :cond_3
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 89
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->addHeaderItem(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 90
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$2;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$2;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 158
    const/4 v2, 0x0

    .line 159
    .local v2, "uri":Landroid/net/Uri;
    const/4 v5, 0x0

    .line 161
    .local v5, "query":[Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 183
    :goto_0
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v3, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 163
    :pswitch_0
    const-string v0, "content://com.samsung.helphub.provider.search/header/search"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 164
    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    .end local v5    # "query":[Ljava/lang/String;
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mQuery:Ljava/lang/String;

    aput-object v1, v5, v0

    .line 165
    .restart local v5    # "query":[Ljava/lang/String;
    goto :goto_0

    .line 168
    :pswitch_1
    const-string v0, "content://com.samsung.helphub.provider.search/header/category/online_help"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 169
    goto :goto_0

    .line 172
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    const v1, 0x7f0d00a3

    if-ne v0, v1, :cond_0

    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://com.samsung.helphub.provider.search/header/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0

    .line 175
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://com.samsung.helphub.provider.search/header/owner/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 177
    goto :goto_0

    .line 161
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 283
    invoke-super {p0, p1, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 284
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->showSearchView()V

    .line 285
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mSearchView:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mQuery:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 286
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    .line 287
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mSearchView:Landroid/widget/SearchView;

    new-instance v1, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$3;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$3;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 303
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mSearchView:Landroid/widget/SearchView;

    new-instance v1, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$4;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList$4;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnSuggestionListener(Landroid/widget/SearchView$OnSuggestionListener;)V

    .line 328
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 102
    const v2, 0x7f0400bb

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 103
    .local v1, "result":Landroid/view/View;
    const v2, 0x7f0d0003

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mNoDataTextView:Landroid/widget/TextView;

    .line 104
    const v2, 0x7f0d0375

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mListFiller:Landroid/widget/ImageView;

    .line 105
    const v2, 0x102000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mListView:Landroid/widget/ListView;

    .line 106
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 108
    if-eqz p3, :cond_1

    .line 109
    const-string v2, "search_list:query"

    invoke-virtual {p3, v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mQuery:Ljava/lang/String;

    .line 110
    const-string v2, "search_list:category_header"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    .line 118
    :cond_0
    :goto_0
    return-object v1

    .line 112
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 113
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 114
    const-string v2, "search_list_arg:query"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mQuery:Ljava/lang/String;

    .line 115
    const-string v2, "search_list_arg:category_header"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 141
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 143
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 144
    .local v0, "focusView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 149
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v2, :cond_0

    .line 150
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v2}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 153
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    invoke-super {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->onDestroy()V

    .line 154
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 7
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const/16 v6, 0x8

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 188
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 190
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    invoke-virtual {v2, p2, v5}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->addItems(Landroid/database/Cursor;Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 194
    const/4 v1, 0x0

    .line 195
    .local v1, "isCategoryFound":Z
    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 197
    :cond_1
    const/4 v2, 0x5

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    sget-object v3, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_6

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const v3, 0x7f0d00a3

    if-eq v2, v3, :cond_6

    .line 202
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourCategory(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 218
    :cond_2
    :goto_1
    if-eqz p2, :cond_3

    .line 219
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 224
    :cond_3
    if-nez v1, :cond_0

    .line 225
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->makeupInitialView()V

    goto :goto_0

    .line 206
    :cond_4
    const/4 v1, 0x1

    .line 207
    invoke-static {p2}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    .line 208
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/LoaderManager;->getLoader(I)Landroid/content/Loader;

    move-result-object v2

    if-nez v2, :cond_5

    .line 209
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v2

    invoke-virtual {v2, v4, v5, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_1

    .line 211
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v2

    invoke-virtual {v2, v4, v5, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_1

    .line 216
    :cond_6
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    .line 230
    .end local v1    # "isCategoryFound":Z
    :pswitch_1
    if-eqz p2, :cond_7

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 233
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0d0002

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 234
    .local v0, "button":Landroid/widget/Button;
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mOnClickOnlineSearchButton:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    .end local v0    # "button":Landroid/widget/Button;
    :cond_7
    if-eqz p2, :cond_0

    .line 237
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 242
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v2, p2, v3}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->addItems(Landroid/database/Cursor;Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 243
    if-eqz p2, :cond_0

    .line 248
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->makeupInitialView()V

    .line 250
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 188
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 39
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 279
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->onResume()V

    .line 124
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0a0020

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 125
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mQuery:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 130
    const-string v0, "search_list:query"

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    if-eqz v0, :cond_1

    .line 133
    const-string v0, "search_list:category_header"

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 135
    :cond_1
    invoke-super {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 136
    return-void
.end method

.method public setQuery(Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 3
    .param p1, "category"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    const/4 v2, 0x0

    .line 272
    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mQuery:Ljava/lang/String;

    .line 273
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    .line 274
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    invoke-virtual {v0}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->reset()V

    .line 275
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 276
    return-void
.end method

.method public setQuery(Ljava/lang/String;)V
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 265
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mQuery:Ljava/lang/String;

    .line 266
    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mCategory:Lcom/samsung/helphub/headers/HelpHeader;

    .line 267
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;

    invoke-virtual {v0}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->reset()V

    .line 268
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 269
    return-void
.end method
