.class Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$2;
.super Ljava/lang/Object;
.source "SectionHeaderExpandableList.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnGroupExpandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$2;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupExpand(I)V
    .locals 4
    .param p1, "groupPosition"    # I

    .prologue
    .line 202
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$2;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    # setter for: Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mExpandedGroupPosition:I
    invoke-static {v0, p1}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->access$102(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;I)I

    .line 203
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$2;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    # getter for: Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeadersListView:Landroid/widget/ExpandableListView;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->access$200(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)Landroid/widget/ExpandableListView;

    move-result-object v0

    new-instance v1, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$Collapser;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$2;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-direct {v1, v2, p1}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$Collapser;-><init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;I)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ExpandableListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 204
    return-void
.end method
