.class Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "HeaderContentsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$1;,
        Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildViewHolder;,
        Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;,
        Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;
    }
.end annotation


# instance fields
.field private mActiveChildPosition:I

.field private mActiveGroupPosition:I

.field private mChildAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;

.field private mChildCursor:Landroid/database/Cursor;

.field private mContext:Landroid/content/Context;

.field private mGroupCursor:Landroid/database/Cursor;

.field private mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 39
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 40
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;

    invoke-direct {v0, p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;

    .line 41
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mContext:Landroid/content/Context;

    .line 42
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method private getHeaderType(I)Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .locals 1
    .param p1, "ordinal"    # I

    .prologue
    .line 451
    invoke-static {}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->values()[Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method private makePSText(Landroid/content/Context;III)Ljava/lang/String;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textID"    # I
    .param p3, "TextPs1Id"    # I
    .param p4, "TextPs2Id"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 215
    const/4 v0, 0x0

    .line 218
    .local v0, "TextPsCount":I
    if-eq p3, v2, :cond_0

    .line 219
    add-int/lit8 v0, v0, 0x1

    .line 220
    :cond_0
    if-eq p4, v2, :cond_1

    .line 221
    add-int/lit8 v0, v0, 0x1

    .line 223
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 231
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 234
    .local v1, "result":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 225
    .end local v1    # "result":Ljava/lang/String;
    :pswitch_0
    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 226
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0

    .line 228
    .end local v1    # "result":Ljava/lang/String;
    :pswitch_1
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 229
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0

    .line 223
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public changeChildCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 303
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->swapChildCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 304
    .local v0, "old":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 305
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 307
    :cond_0
    return-void
.end method

.method public changeGroupCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 264
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->swapGroupCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 265
    .local v0, "old":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 266
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 268
    :cond_0
    return-void
.end method

.method public getChild(II)Ljava/lang/Object;
    .locals 4
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 46
    const/4 v0, 0x0

    .line 47
    .local v0, "result":Ljava/lang/Object;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;

    if-eqz v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getGroupIntID(I)I

    move-result v2

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getChildrenCount(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->setOwnerID(II)V

    .line 49
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;

    invoke-virtual {v1, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 51
    .end local v0    # "result":Ljava/lang/Object;
    :cond_0
    return-object v0
.end method

.method public getChildId(II)J
    .locals 5
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 56
    const-wide/16 v0, -0x1

    .line 57
    .local v0, "result":J
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;

    if-eqz v2, :cond_0

    .line 58
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getGroupIntID(I)I

    move-result v3

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getChildrenCount(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->setOwnerID(II)V

    .line 59
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;

    invoke-virtual {v2, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->getItemId(I)J

    move-result-wide v0

    .line 61
    :cond_0
    return-wide v0
.end method

.method public getChildPosition(Lcom/samsung/helphub/headers/HelpHeader;I)I
    .locals 5
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p2, "groupPosition"    # I

    .prologue
    .line 455
    const/4 v2, -0x1

    .line 456
    .local v2, "result":I
    invoke-virtual {p0, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getChildrenCount(I)I

    move-result v0

    .line 457
    .local v0, "count":I
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;

    invoke-virtual {p0, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getGroupIntID(I)I

    move-result v4

    invoke-virtual {v3, v4, v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->setOwnerID(II)V

    .line 458
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 459
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;

    invoke-virtual {v3, v1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->getItemIntID(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 460
    move v2, v1

    .line 458
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 464
    :cond_1
    return v2
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 67
    const/4 v0, 0x0

    .line 68
    .local v0, "result":Landroid/view/View;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;

    if-eqz v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getGroupIntID(I)I

    move-result v2

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getChildrenCount(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->setOwnerID(II)V

    .line 70
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;

    invoke-virtual {v1, p2, p4, p5}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 71
    if-eqz v0, :cond_0

    .line 72
    iget v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mActiveGroupPosition:I

    if-ne p1, v1, :cond_1

    iget v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mActiveChildPosition:I

    if-ne p2, v1, :cond_1

    .line 73
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    .line 80
    :cond_0
    :goto_0
    return-object v0

    .line 75
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    goto :goto_0
.end method

.method public getChildrenCount(I)I
    .locals 3
    .param p1, "groupPosition"    # I

    .prologue
    .line 85
    const/4 v0, 0x0

    .line 86
    .local v0, "result":I
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    const/16 v2, 0xf

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 90
    :cond_0
    return v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 95
    const/4 v0, 0x0

    .line 96
    .local v0, "result":Lcom/samsung/helphub/headers/HelpHeader;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 98
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-static {v1}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v0

    .line 100
    :cond_0
    return-object v0
.end method

.method public getGroupCount()I
    .locals 2

    .prologue
    .line 105
    const/4 v0, 0x0

    .line 106
    .local v0, "result":I
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 109
    :cond_0
    return v0
.end method

.method public getGroupId(I)J
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 114
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getGroupIntID(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final getGroupIntID(I)I
    .locals 3
    .param p1, "groupPosition"    # I

    .prologue
    .line 120
    const/4 v0, -0x1

    .line 121
    .local v0, "result":I
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 126
    :cond_0
    return v0
.end method

.method public getGroupPosition(Lcom/samsung/helphub/headers/HelpHeader;)I
    .locals 5
    .param p1, "section"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 468
    const/4 v2, 0x0

    .line 469
    .local v2, "result":I
    const/4 v1, 0x0

    .line 470
    .local v1, "owner_id":I
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v3

    sget-object v4, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v3, v4, :cond_3

    .line 471
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v1

    .line 475
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 477
    :cond_1
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 478
    .local v0, "id":I
    if-ne v0, v1, :cond_4

    .line 483
    :goto_1
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lt v2, v3, :cond_2

    .line 484
    const/4 v2, -0x1

    .line 486
    :cond_2
    return v2

    .line 472
    .end local v0    # "id":I
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v3

    sget-object v4, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v3, v4, :cond_0

    .line 473
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getOwnerResId()I

    move-result v1

    goto :goto_0

    .line 481
    .restart local v0    # "id":I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 482
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v10, 0xc

    const/16 v7, 0xa

    const/4 v9, 0x4

    const/4 v6, 0x2

    const/4 v8, 0x0

    .line 133
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    if-nez v4, :cond_0

    .line 134
    const/4 v1, 0x0

    .line 211
    :goto_0
    return-object v1

    .line 137
    :cond_0
    move-object v1, p3

    .line 139
    .local v1, "result":Landroid/view/View;
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v4, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 140
    const/4 v0, 0x0

    .line 142
    .local v0, "holder":Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;
    if-nez v1, :cond_1

    .line 143
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f0400bf

    invoke-virtual {v4, v5, p4, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 146
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;

    .end local v0    # "holder":Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;
    const/4 v4, 0x0

    invoke-direct {v0, v4}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$1;)V

    .line 147
    .restart local v0    # "holder":Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;
    const v4, 0x7f0d010c

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->title:Landroid/widget/TextView;

    .line 148
    const v4, 0x7f0d0086

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->foldIndicator:Landroid/widget/ImageView;

    .line 150
    const v4, 0x7f0d010d

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->summary:Landroid/widget/TextView;

    .line 151
    const v4, 0x7f0d010e

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->headerImage:Landroid/widget/ImageView;

    .line 152
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 159
    :goto_1
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    const/4 v5, 0x5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getHeaderType(I)Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v4

    sget-object v5, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v4, v5, :cond_6

    .line 160
    iget-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->title:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 162
    iget-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->headerImage:Landroid/widget/ImageView;

    if-eqz v4, :cond_2

    .line 163
    iget-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->headerImage:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 170
    :goto_2
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getChildrenCount(I)I

    move-result v4

    if-nez v4, :cond_4

    .line 171
    iget-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->foldIndicator:Landroid/widget/ImageView;

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 173
    iget v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mActiveGroupPosition:I

    if-ne p1, v4, :cond_3

    .line 174
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/view/View;->setActivated(Z)V

    goto/16 :goto_0

    .line 154
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "holder":Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;
    check-cast v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;

    .restart local v0    # "holder":Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;
    goto :goto_1

    .line 165
    :cond_2
    iget-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->title:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5, v8, v8, v8}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_2

    .line 176
    :cond_3
    invoke-virtual {v1, v8}, Landroid/view/View;->setActivated(Z)V

    goto/16 :goto_0

    .line 179
    :cond_4
    iget-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->foldIndicator:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 181
    if-eqz p2, :cond_5

    .line 183
    iget-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->foldIndicator:Landroid/widget/ImageView;

    const v5, 0x7f020081

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 186
    :cond_5
    iget-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->foldIndicator:Landroid/widget/ImageView;

    const v5, 0x7f020080

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 191
    :cond_6
    if-eqz p2, :cond_7

    .line 192
    iget-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->foldIndicator:Landroid/widget/ImageView;

    const v5, 0x7f020081

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 193
    const v4, 0x7f020314

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 199
    :goto_3
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    const/4 v7, 0x3

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    iget-object v7, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->makePSText(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v3

    .line 200
    .local v3, "title":Ljava/lang/String;
    iget-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v4, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-lez v4, :cond_8

    .line 203
    iget-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    invoke-interface {v5, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    const/16 v7, 0xd

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    iget-object v7, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    const/16 v8, 0xe

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->makePSText(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v2

    .line 205
    .local v2, "summary":Ljava/lang/String;
    iget-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->summary:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 195
    .end local v2    # "summary":Ljava/lang/String;
    .end local v3    # "title":Ljava/lang/String;
    :cond_7
    iget-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->foldIndicator:Landroid/widget/ImageView;

    const v5, 0x7f020080

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 196
    const v4, 0x7f020313

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_3

    .line 207
    .restart local v3    # "title":Ljava/lang/String;
    :cond_8
    iget-object v4, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$GroupViewHolder;->summary:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x1

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 254
    const/4 v0, 0x1

    return v0
.end method

.method public setActiveItem(II)V
    .locals 0
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 342
    iput p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mActiveGroupPosition:I

    .line 343
    iput p2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mActiveChildPosition:I

    .line 344
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->notifyDataSetChanged()V

    .line 345
    return-void
.end method

.method public swapChildCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 320
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;

    if-ne p1, v1, :cond_0

    .line 321
    const/4 v0, 0x0

    .line 332
    :goto_0
    return-object v0

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;

    .line 324
    .local v0, "oldCursor":Landroid/database/Cursor;
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;

    .line 325
    if-eqz p1, :cond_1

    .line 327
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 330
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->notifyDataSetInvalidated()V

    goto :goto_0
.end method

.method public swapGroupCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 281
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    if-ne p1, v1, :cond_0

    .line 282
    const/4 v0, 0x0

    .line 293
    :goto_0
    return-object v0

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    .line 285
    .local v0, "oldCursor":Landroid/database/Cursor;
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mGroupCursor:Landroid/database/Cursor;

    .line 286
    if-eqz p1, :cond_1

    .line 288
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 291
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->notifyDataSetInvalidated()V

    goto :goto_0
.end method
