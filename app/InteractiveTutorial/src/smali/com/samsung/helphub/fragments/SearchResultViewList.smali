.class public Lcom/samsung/helphub/fragments/SearchResultViewList;
.super Lcom/samsung/helphub/fragments/HelpSearchList;
.source "SearchResultViewList.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchResultViewList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 33
    const v0, 0x7f0d0457

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mMenuItemSearch:Landroid/view/MenuItem;

    .line 34
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mActionBar:Landroid/app/ActionBar;

    const v1, 0x7f040002

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 35
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mSearchView:Landroid/widget/SearchView;

    .line 37
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mSearchView:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mQuery:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 38
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    .line 39
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 41
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchResultViewList;->showSearchView()V

    .line 42
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 3
    .param p1, "mQuery"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x8

    .line 47
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 48
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/SearchResultViewList;->search(Ljava/lang/String;)V

    .line 58
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 50
    :cond_1
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mSearchResult:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 51
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mNoResultFoundRelativeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 52
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchResultViewList;->findOnlineHelp()Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mNoResultFoundTextView:Landroid/widget/TextView;

    const v1, 0x7f0a06b4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 54
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mNoResultFoundButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 4
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 65
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchResultViewList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 70
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 71
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 74
    :cond_0
    return v3
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 20
    invoke-super {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->onResume()V

    .line 22
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchResultViewList;->mSearchView:Landroid/widget/SearchView;

    check-cast v0, Lcom/samsung/helphub/widget/HelpHubSearchView;

    invoke-virtual {v0}, Lcom/samsung/helphub/widget/HelpHubSearchView;->forceSuggestion()V

    .line 25
    :cond_0
    return-void
.end method
