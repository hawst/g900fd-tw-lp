.class Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$4;
.super Landroid/os/Handler;
.source "SectionHeaderExpandableList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)V
    .locals 0

    .prologue
    .line 451
    iput-object p1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$4;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v3, 0x7f0a0586

    const v2, 0x104000a

    .line 454
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$4;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 480
    :cond_0
    :goto_0
    return-void

    .line 457
    :cond_1
    const/4 v0, 0x1

    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_2

    .line 458
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$4;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v1}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0585

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$4$1;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$4$1;-><init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$4;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 468
    :cond_2
    const/4 v0, 0x2

    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_0

    .line 469
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$4;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v1}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0584

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$4$2;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$4$2;-><init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$4;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method
