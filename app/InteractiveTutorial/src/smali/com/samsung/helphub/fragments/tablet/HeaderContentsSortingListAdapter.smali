.class Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;
.super Landroid/widget/BaseAdapter;
.source "HeaderContentsSortingListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$Holder;
    }
.end annotation


# instance fields
.field private mActiveItemPosition:I

.field private mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mIsSorted:Z

.field private mSortingList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isSorted"    # Z

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mContext:Landroid/content/Context;

    .line 47
    iput-boolean p2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mIsSorted:Z

    .line 48
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 50
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getSortHedarList(Landroid/database/Cursor;)Ljava/util/List;
    .locals 13
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    new-instance v12, Ljava/util/ArrayList;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {v12, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 170
    .local v12, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 172
    :cond_0
    invoke-static {p1}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v9

    .line 173
    .local v9, "header":Lcom/samsung/helphub/headers/HelpHeader;
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v9, v0}, Lcom/samsung/helphub/HelpHubCommon;->isResourceAvailable(Lcom/samsung/helphub/headers/HelpHeader;Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v9}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourSection(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 174
    invoke-interface {v12, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    iget-boolean v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mIsSorted:Z

    if-eqz v0, :cond_2

    .line 179
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$1;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;)V

    invoke-static {v12, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 231
    :cond_2
    const/4 v7, 0x0

    .line 233
    .local v7, "categoryCursor":Landroid/database/Cursor;
    :try_start_0
    const-string v0, "content://com.samsung.helphub.provider.search/header/category"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 234
    .local v1, "uri":Landroid/net/Uri;
    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v0, 0xa

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 236
    .local v6, "builder":Ljava/lang/StringBuilder;
    const-string v0, "header_res_id"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " IN ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0d009a

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0d00a7

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0d00a8

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x29

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 242
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 245
    if-eqz v7, :cond_4

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 246
    const/4 v11, 0x0

    .line 248
    .local v11, "pos":I
    :cond_3
    invoke-static {v7}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v0

    invoke-interface {v12, v11, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 249
    add-int/lit8 v11, v11, 0x1

    .line 250
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    .line 253
    .end local v11    # "pos":I
    :cond_4
    if-eqz v7, :cond_5

    .line 254
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 257
    :cond_5
    const/4 v8, 0x0

    .line 259
    .local v8, "guidedSectionCursor":Landroid/database/Cursor;
    :try_start_1
    const-string v0, "content://com.samsung.helphub.provider.search/header/section"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 260
    new-instance v6, Ljava/lang/StringBuilder;

    .end local v6    # "builder":Ljava/lang/StringBuilder;
    const/16 v0, 0xa

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 262
    .restart local v6    # "builder":Ljava/lang/StringBuilder;
    const-string v0, "header_res_id"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " IN ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0d0102

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0d0103

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0d0104

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x29

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 267
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 270
    if-eqz v8, :cond_7

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 271
    const/4 v11, 0x0

    .line 273
    .restart local v11    # "pos":I
    :cond_6
    invoke-static {v8}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v0

    invoke-interface {v12, v11, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 274
    add-int/lit8 v11, v11, 0x1

    .line 275
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_6

    .line 278
    .end local v11    # "pos":I
    :cond_7
    if-eqz v8, :cond_8

    .line 279
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 284
    :cond_8
    const/4 v10, 0x0

    .line 286
    .local v10, "onlineHelpCursor":Landroid/database/Cursor;
    :try_start_2
    const-string v0, "content://com.samsung.helphub.provider.search/header/category"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 287
    new-instance v6, Ljava/lang/StringBuilder;

    .end local v6    # "builder":Ljava/lang/StringBuilder;
    const/16 v0, 0xa

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 289
    .restart local v6    # "builder":Ljava/lang/StringBuilder;
    const-string v0, "header_res_id"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " IN ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0d00a3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x29

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 292
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 295
    if-eqz v10, :cond_a

    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 297
    :cond_9
    invoke-static {v10}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v0

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v0

    if-nez v0, :cond_9

    .line 301
    :cond_a
    if-eqz v10, :cond_b

    .line 302
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 305
    :cond_b
    return-object v12

    .line 253
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "builder":Ljava/lang/StringBuilder;
    .end local v8    # "guidedSectionCursor":Landroid/database/Cursor;
    .end local v10    # "onlineHelpCursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_c

    .line 254
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v0

    .line 278
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v8    # "guidedSectionCursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v0

    if-eqz v8, :cond_d

    .line 279
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_d
    throw v0

    .line 301
    .restart local v10    # "onlineHelpCursor":Landroid/database/Cursor;
    :catchall_2
    move-exception v0

    if-eqz v10, :cond_e

    .line 302
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_e
    throw v0
.end method


# virtual methods
.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 162
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 163
    .local v0, "old":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 164
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 166
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 54
    const/4 v0, 0x0

    .line 55
    .local v0, "result":I
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mSortingList:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mSortingList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 58
    :cond_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 63
    const/4 v0, 0x0

    .line 64
    .local v0, "result":Ljava/lang/Object;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mSortingList:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mSortingList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 67
    .end local v0    # "result":Ljava/lang/Object;
    :cond_0
    return-object v0
.end method

.method public getItemId(I)J
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 72
    const-wide/16 v0, 0x0

    .line 73
    .local v0, "result":J
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mSortingList:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 74
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mSortingList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v2

    int-to-long v0, v2

    .line 76
    :cond_0
    return-wide v0
.end method

.method public getItemPosition(Lcom/samsung/helphub/headers/HelpHeader;)I
    .locals 5
    .param p1, "section"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 80
    const/4 v3, 0x0

    .line 81
    .local v3, "result":I
    const/4 v1, 0x0

    .line 82
    .local v1, "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v0

    .line 83
    .local v0, "Record_id":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->getCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 84
    invoke-virtual {p0, v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    check-cast v1, Lcom/samsung/helphub/headers/HelpHeader;

    .line 85
    .restart local v1    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v4

    if-ne v0, v4, :cond_1

    .line 87
    move v3, v2

    .line 91
    :cond_0
    return v3

    .line 83
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 95
    const/4 v2, 0x0

    .line 96
    .local v2, "result":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    .line 97
    .local v0, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v0, :cond_0

    .line 103
    new-instance v1, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$Holder;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$Holder;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$1;)V

    .line 104
    .local v1, "holder":Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$Holder;
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f0400be

    invoke-virtual {v3, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 106
    const v3, 0x7f0d010c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$Holder;->Title:Landroid/widget/TextView;

    .line 107
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 113
    iget-object v3, v1, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$Holder;->Title:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 115
    iget v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mActiveItemPosition:I

    if-ne p1, v3, :cond_1

    .line 116
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setActivated(Z)V

    .line 121
    .end local v1    # "holder":Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$Holder;
    :cond_0
    :goto_0
    return-object v2

    .line 118
    .restart local v1    # "holder":Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter$Holder;
    :cond_1
    invoke-virtual {v2, v5}, Landroid/view/View;->setActivated(Z)V

    goto :goto_0
.end method

.method public setActiveItem(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 309
    iput p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mActiveItemPosition:I

    .line 310
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->notifyDataSetChanged()V

    .line 311
    return-void
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 139
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mCursor:Landroid/database/Cursor;

    if-ne p1, v1, :cond_0

    .line 140
    const/4 v0, 0x0

    .line 152
    :goto_0
    return-object v0

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mCursor:Landroid/database/Cursor;

    .line 143
    .local v0, "oldCursor":Landroid/database/Cursor;
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mCursor:Landroid/database/Cursor;

    .line 144
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 145
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mCursor:Landroid/database/Cursor;

    invoke-direct {p0, v1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->getSortHedarList(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->mSortingList:Ljava/util/List;

    .line 147
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 150
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->notifyDataSetInvalidated()V

    goto :goto_0
.end method
