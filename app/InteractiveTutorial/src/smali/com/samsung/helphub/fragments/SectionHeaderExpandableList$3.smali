.class Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;
.super Ljava/lang/Object;
.source "SectionHeaderExpandableList.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 19
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "id"    # J

    .prologue
    .line 238
    const/16 v16, 0x0

    .line 239
    .local v16, "result":Z
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v12

    .line 240
    .local v12, "expanded":Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v3}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    .line 241
    .local v9, "currentActivityName":Ljava/lang/String;
    if-eqz v9, :cond_0

    const-string v3, "ExternalHelpActivity"

    invoke-virtual {v9, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    # getter for: Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->access$000(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroupCount()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 242
    const/4 v3, 0x1

    .line 399
    :goto_0
    return v3

    .line 245
    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1, v3}, Landroid/widget/ExpandableListView;->setSelectionFromTop(II)V

    .line 246
    if-eqz p2, :cond_1

    .line 247
    const v3, 0x7f0d0086

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    .line 248
    .local v13, "foldIcon":Landroid/widget/ImageView;
    if-eqz v13, :cond_1

    .line 249
    if-eqz v12, :cond_3

    .line 250
    new-instance v2, Landroid/view/animation/RotateAnimation;

    const/high16 v3, 0x43340000    # 180.0f

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v2 .. v8}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 253
    .local v2, "anim":Landroid/view/animation/RotateAnimation;
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 254
    const-wide/16 v4, 0x2bc

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 255
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V

    .line 256
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 257
    invoke-virtual {v13, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 271
    .end local v2    # "anim":Landroid/view/animation/RotateAnimation;
    .end local v13    # "foldIcon":Landroid/widget/ImageView;
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    # getter for: Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->access$000(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/helphub/headers/HelpHeader;

    .line 272
    .local v14, "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {v14}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d00c7

    if-ne v3, v4, :cond_4

    .line 273
    const/4 v3, 0x3

    new-array v0, v3, [J

    move-object/from16 v17, v0

    fill-array-data v17, :array_0

    .line 274
    .local v17, "timeStemp":[J
    const/4 v3, 0x3

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a06ac

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a06ad

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a06ae

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    .line 279
    .local v10, "description":[Ljava/lang/String;
    const/4 v3, 0x3

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a01a3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v18, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a01db

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v18, v3

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a019d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v18, v3

    .line 285
    .local v18, "title":[Ljava/lang/String;
    new-instance v15, Landroid/content/Intent;

    const-string v3, "android.intent.action.START_HELP_CLIP"

    invoke-direct {v15, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 304
    .local v15, "mIntent":Landroid/content/Intent;
    const-string v3, "file:///system/media/video/motion.mp4"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "video/mp4"

    invoke-virtual {v15, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 305
    const-string v3, "HELP_TIME_STEMP"

    move-object/from16 v0, v17

    invoke-virtual {v15, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 306
    const-string v3, "HELP_DESCRIPTION"

    invoke-virtual {v15, v3, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 307
    const-string v3, "HELP_TITLE"

    move-object/from16 v0, v18

    invoke-virtual {v15, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 308
    const-string v3, "HELP_START_CLIP"

    const/4 v4, 0x0

    invoke-virtual {v15, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 309
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v3, v15}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->startActivity(Landroid/content/Intent;)V

    .line 311
    const/16 v16, 0x1

    .line 399
    .end local v10    # "description":[Ljava/lang/String;
    .end local v15    # "mIntent":Landroid/content/Intent;
    .end local v17    # "timeStemp":[J
    .end local v18    # "title":[Ljava/lang/String;
    :cond_2
    :goto_2
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 260
    .end local v14    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .restart local v13    # "foldIcon":Landroid/widget/ImageView;
    :cond_3
    new-instance v2, Landroid/view/animation/RotateAnimation;

    const/high16 v3, 0x43340000    # 180.0f

    const/high16 v4, 0x43b40000    # 360.0f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v2 .. v8}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 263
    .restart local v2    # "anim":Landroid/view/animation/RotateAnimation;
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 264
    const-wide/16 v4, 0x2bc

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 265
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V

    .line 266
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 267
    invoke-virtual {v13, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_1

    .line 312
    .end local v2    # "anim":Landroid/view/animation/RotateAnimation;
    .end local v13    # "foldIcon":Landroid/widget/ImageView;
    .restart local v14    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_4
    invoke-virtual {v14}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d00c8

    if-ne v3, v4, :cond_5

    .line 313
    const/4 v3, 0x2

    new-array v0, v3, [J

    move-object/from16 v17, v0

    fill-array-data v17, :array_1

    .line 314
    .restart local v17    # "timeStemp":[J
    const/4 v3, 0x2

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a013c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a06b0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    .line 318
    .restart local v10    # "description":[Ljava/lang/String;
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a013b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v18, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a06af

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v18, v3

    .line 323
    .restart local v18    # "title":[Ljava/lang/String;
    new-instance v15, Landroid/content/Intent;

    const-string v3, "android.intent.action.START_HELP_CLIP"

    invoke-direct {v15, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 342
    .restart local v15    # "mIntent":Landroid/content/Intent;
    const-string v3, "file:///system/media/video/camera.mp4"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "video/mp4"

    invoke-virtual {v15, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 343
    const-string v3, "HELP_TIME_STEMP"

    move-object/from16 v0, v17

    invoke-virtual {v15, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 344
    const-string v3, "HELP_DESCRIPTION"

    invoke-virtual {v15, v3, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    const-string v3, "HELP_TITLE"

    move-object/from16 v0, v18

    invoke-virtual {v15, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 346
    const-string v3, "HELP_START_CLIP"

    const/4 v4, 0x0

    invoke-virtual {v15, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 347
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v3, v15}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->startActivity(Landroid/content/Intent;)V

    .line 349
    const/16 v16, 0x1

    .line 350
    goto/16 :goto_2

    .end local v10    # "description":[Ljava/lang/String;
    .end local v15    # "mIntent":Landroid/content/Intent;
    .end local v17    # "timeStemp":[J
    .end local v18    # "title":[Ljava/lang/String;
    :cond_5
    invoke-virtual {v14}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    const v4, 0x7f0d00c9

    if-ne v3, v4, :cond_6

    .line 351
    const/4 v3, 0x2

    new-array v0, v3, [J

    move-object/from16 v17, v0

    fill-array-data v17, :array_2

    .line 352
    .restart local v17    # "timeStemp":[J
    const/4 v3, 0x2

    new-array v10, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a03fe

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0402

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    .line 356
    .restart local v10    # "description":[Ljava/lang/String;
    const/4 v3, 0x2

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a03fa

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v18, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a03fb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v18, v3

    .line 361
    .restart local v18    # "title":[Ljava/lang/String;
    new-instance v15, Landroid/content/Intent;

    const-string v3, "android.intent.action.START_HELP_CLIP"

    invoke-direct {v15, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 380
    .restart local v15    # "mIntent":Landroid/content/Intent;
    const-string v3, "file:///system/media/video/group_play.mp4"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "video/mp4"

    invoke-virtual {v15, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 381
    const-string v3, "HELP_TIME_STEMP"

    move-object/from16 v0, v17

    invoke-virtual {v15, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 382
    const-string v3, "HELP_DESCRIPTION"

    invoke-virtual {v15, v3, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 383
    const-string v3, "HELP_TITLE"

    move-object/from16 v0, v18

    invoke-virtual {v15, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 384
    const-string v3, "HELP_START_CLIP"

    const/4 v4, 0x0

    invoke-virtual {v15, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 385
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v3, v15}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->startActivity(Landroid/content/Intent;)V

    .line 387
    const/16 v16, 0x1

    .line 388
    goto/16 :goto_2

    .end local v10    # "description":[Ljava/lang/String;
    .end local v15    # "mIntent":Landroid/content/Intent;
    .end local v17    # "timeStemp":[J
    .end local v18    # "title":[Ljava/lang/String;
    :cond_6
    const-string v3, "email"

    invoke-virtual {v14}, Lcom/samsung/helphub/headers/HelpHeader;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 389
    const-string v11, "com.android.email"

    .line 390
    .local v11, "email":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v3, v4, v11}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 391
    new-instance v15, Landroid/content/Intent;

    invoke-direct {v15}, Landroid/content/Intent;-><init>()V

    .line 392
    .restart local v15    # "mIntent":Landroid/content/Intent;
    const-string v3, "com.sec.android.app.popupuireceiver"

    const-string v4, "com.sec.android.app.popupuireceiver.DisableApp"

    invoke-virtual {v15, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 394
    const-string v3, "app_package_name"

    invoke-virtual {v15, v3, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 395
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$3;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    const/4 v4, 0x0

    invoke-virtual {v3, v15, v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->startActivityForResult(Landroid/content/Intent;I)V

    .line 396
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 273
    nop

    :array_0
    .array-data 8
        0x0
        0x4268
        0x7d00
    .end array-data

    .line 313
    :array_1
    .array-data 8
        0x0
        0x6d60
    .end array-data

    .line 351
    :array_2
    .array-data 8
        0x0
        0x80e8
    .end array-data
.end method
