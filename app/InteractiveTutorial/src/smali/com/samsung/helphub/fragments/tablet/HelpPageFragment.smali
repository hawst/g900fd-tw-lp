.class public Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;
.super Landroid/app/Fragment;
.source "HelpPageFragment.java"


# instance fields
.field private mActivityNull:Z

.field private mContext:Landroid/content/Context;

.field private mFragment:Landroid/app/Fragment;

.field private mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

.field private mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;

.field private mHeader:Lcom/samsung/helphub/headers/HelpHeader;

.field private mIsPage:Z

.field private mOnBreadCrumbClickListener:Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;

.field private mOwner:Lcom/samsung/helphub/headers/HelpHeader;

.field private mSummaryTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mActivityNull:Z

    .line 187
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$2;-><init>(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mOnBreadCrumbClickListener:Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/app/FragmentBreadCrumbs;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;Landroid/content/Context;III)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->makePSText(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mSummaryTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method private drawSummary(Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 4
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 202
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 221
    :goto_0
    return-void

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mSummaryTextView:Landroid/widget/TextView;

    new-instance v1, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$3;

    invoke-direct {v1, p0, p1}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$3;-><init>(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;Lcom/samsung/helphub/headers/HelpHeader;)V

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private makePSText(Landroid/content/Context;III)Ljava/lang/String;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textID"    # I
    .param p3, "TextPs1Id"    # I
    .param p4, "TextPs2Id"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 223
    const/4 v0, 0x0

    .line 226
    .local v0, "TextPsCount":I
    if-eq p3, v2, :cond_0

    .line 227
    add-int/lit8 v0, v0, 0x1

    .line 228
    :cond_0
    if-eq p4, v2, :cond_1

    .line 229
    add-int/lit8 v0, v0, 0x1

    .line 231
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 239
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 242
    .local v1, "result":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 233
    .end local v1    # "result":Ljava/lang/String;
    :pswitch_0
    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 234
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0

    .line 236
    .end local v1    # "result":Ljava/lang/String;
    :pswitch_1
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 237
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0

    .line 231
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private showBreadCrumbs(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V
    .locals 7
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "shortTitle"    # Ljava/lang/CharSequence;
    .param p3, "isPage"    # Z
    .param p4, "isFullTitle"    # Z

    .prologue
    .line 147
    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    new-instance v0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$1;-><init>(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V

    const-wide/16 v2, 0xa

    invoke-virtual {v6, v0, v2, v3}, Landroid/app/FragmentBreadCrumbs;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 185
    return-void
.end method


# virtual methods
.method public getListFragment()Landroid/app/Fragment;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragment:Landroid/app/Fragment;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 54
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 55
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/app/FragmentBreadCrumbs;->setMaxVisible(I)V

    .line 56
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentBreadCrumbs;->setActivity(Landroid/app/Activity;)V

    .line 57
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mContext:Landroid/content/Context;

    .line 58
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mSummaryTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 59
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mSummaryTextView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 60
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v3, 0x9

    invoke-direct {v2, v5, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 62
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;

    const/4 v2, 0x6

    invoke-virtual {v1, v4, v2, v4, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 70
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mActivityNull:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragment:Landroid/app/Fragment;

    if-eqz v1, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 72
    .local v0, "transaction":Landroid/app/FragmentTransaction;
    invoke-virtual {v0, v4}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 73
    const v1, 0x7f0d0089

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 74
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 75
    iput-boolean v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mActivityNull:Z

    .line 78
    .end local v0    # "transaction":Landroid/app/FragmentTransaction;
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mOwner:Lcom/samsung/helphub/headers/HelpHeader;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mHeader:Lcom/samsung/helphub/headers/HelpHeader;

    iget-boolean v3, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mIsPage:Z

    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->switchToHelpPage(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Z)V

    .line 79
    return-void

    .line 64
    :cond_1
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mSummaryTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 65
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, 0x3

    invoke-direct {v2, v5, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;

    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    const v1, 0x7f040099

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 45
    .local v0, "result":Landroid/view/View;
    const v1, 0x7f0d010d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mSummaryTextView:Landroid/widget/TextView;

    .line 46
    const v1, 0x7f0d0119

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/app/FragmentBreadCrumbs;

    iput-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    .line 47
    const v1, 0x7f0d02e8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;

    .line 48
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbs:Landroid/app/FragmentBreadCrumbs;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mOnBreadCrumbClickListener:Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;

    invoke-virtual {v1, v2}, Landroid/app/FragmentBreadCrumbs;->setOnBreadCrumbClickListener(Landroid/app/FragmentBreadCrumbs$OnBreadCrumbClickListener;)V

    .line 49
    return-object v0
.end method

.method public setHeaders(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Z)V
    .locals 0
    .param p1, "owner"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p2, "header"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p3, "isPage"    # Z

    .prologue
    .line 250
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mOwner:Lcom/samsung/helphub/headers/HelpHeader;

    .line 251
    iput-object p2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mHeader:Lcom/samsung/helphub/headers/HelpHeader;

    .line 252
    iput-boolean p3, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mIsPage:Z

    .line 253
    return-void
.end method

.method public switchToHelpPage(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;Z)V
    .locals 9
    .param p1, "owner"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p2, "header"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p3, "isPage"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v8, -0x1

    const/4 v7, 0x0

    .line 92
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    if-eqz v4, :cond_0

    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 96
    .local v0, "args":Landroid/os/Bundle;
    const-string v4, "helpub:header"

    invoke-virtual {v0, v4, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 98
    if-eqz p3, :cond_5

    .line 99
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragment:Landroid/app/Fragment;

    .line 104
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 106
    .local v2, "tempActivity":Landroid/app/Activity;
    if-eqz v2, :cond_6

    .line 107
    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v4

    if-nez v4, :cond_2

    .line 108
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 109
    .local v3, "transaction":Landroid/app/FragmentTransaction;
    invoke-virtual {v3, v7}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 110
    const v4, 0x7f0d0089

    iget-object v5, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v3, v4, v5}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 111
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 112
    iput-boolean v7, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mActivityNull:Z

    .line 118
    .end local v3    # "transaction":Landroid/app/FragmentTransaction;
    :cond_2
    :goto_2
    const/4 v1, 0x0

    .line 119
    .local v1, "isFullTitle":Z
    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v4

    const v5, 0x7f0d0103

    if-eq v4, v5, :cond_3

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v4

    const v5, 0x7f0d0102

    if-eq v4, v5, :cond_3

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v4

    const v5, 0x7f0d0104

    if-ne v4, v5, :cond_4

    .line 122
    :cond_3
    const/4 v1, 0x1

    .line 125
    :cond_4
    if-eqz p1, :cond_7

    .line 126
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5, p3, v1}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->showBreadCrumbs(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V

    .line 130
    :goto_3
    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryId()I

    move-result v4

    if-eq v4, v8, :cond_8

    .line 131
    invoke-direct {p0, p2}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->drawSummary(Lcom/samsung/helphub/headers/HelpHeader;)V

    goto :goto_0

    .line 101
    .end local v1    # "isFullTitle":Z
    .end local v2    # "tempActivity":Landroid/app/Activity;
    :cond_5
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mContext:Landroid/content/Context;

    const-class v5, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;

    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragment:Landroid/app/Fragment;

    goto :goto_1

    .line 115
    .restart local v2    # "tempActivity":Landroid/app/Activity;
    :cond_6
    iput-boolean v6, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mActivityNull:Z

    goto :goto_2

    .line 128
    .restart local v1    # "isFullTitle":Z
    :cond_7
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5, p3, v1}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->showBreadCrumbs(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)V

    goto :goto_3

    .line 133
    :cond_8
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mSummaryTextView:Landroid/widget/TextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mSummaryTextView:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 135
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v6, 0x9

    invoke-direct {v5, v8, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 137
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;

    const/4 v5, 0x6

    invoke-virtual {v4, v7, v5, v7, v7}, Landroid/widget/ImageView;->setPadding(IIII)V

    goto/16 :goto_0
.end method
