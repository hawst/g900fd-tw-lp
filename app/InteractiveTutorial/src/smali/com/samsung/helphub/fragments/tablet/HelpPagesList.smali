.class public Lcom/samsung/helphub/fragments/tablet/HelpPagesList;
.super Landroid/app/Fragment;
.source "HelpPagesList.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/tablet/HelpPagesList$MoveExpandingItemRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/Fragment;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final HEADER_LOADER_ID:I = 0x0

.field private static final SAVE_BUNDLE_KEY_OPENED_ITEM:Ljava/lang/String; = "item_list:opened_item"


# instance fields
.field private mAdapter:Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;

.field private mCurrentSection:I

.field private mExpandedGroupPosition:I

.field private mListView:Landroid/widget/ExpandableListView;

.field private mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

.field private mPageListLoadListener:Lcom/samsung/helphub/fragments/tablet/PageListLoadListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 34
    iput v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mCurrentSection:I

    .line 40
    iput v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mExpandedGroupPosition:I

    .line 106
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList$1;-><init>(Lcom/samsung/helphub/fragments/tablet/HelpPagesList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/fragments/tablet/HelpPagesList;)Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/tablet/HelpPagesList;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/fragments/tablet/HelpPagesList;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/tablet/HelpPagesList;
    .param p1, "x1"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mExpandedGroupPosition:I

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/helphub/fragments/tablet/HelpPagesList;)Landroid/widget/ExpandableListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/tablet/HelpPagesList;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;

    return-object v0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 51
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 52
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;-><init>(Landroid/app/Activity;Landroid/app/Fragment;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;

    .line 53
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 54
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 166
    const/4 v2, 0x0

    .line 167
    .local v2, "uri":Landroid/net/Uri;
    packed-switch p1, :pswitch_data_0

    .line 176
    :goto_0
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v3, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 169
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://com.samsung.helphub.provider.search/header/owner/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mCurrentSection:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 170
    goto :goto_0

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    const/4 v2, 0x0

    .line 59
    .local v2, "result":Landroid/view/View;
    const v3, 0x7f0400b9

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 62
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 63
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    const-string v3, "helpub:header"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 64
    const-string v3, "helpub:header"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/headers/HelpHeader;

    .line 65
    .local v1, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v1, :cond_0

    .line 66
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v3

    iput v3, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mCurrentSection:I

    .line 70
    .end local v1    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_0
    const v3, 0x102000a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ExpandableListView;

    iput-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;

    .line 72
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/ExpandableListView;->setItemsCanFocus(Z)V

    .line 73
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;

    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 75
    if-eqz p3, :cond_1

    .line 76
    const-string v3, "item_list:opened_item"

    const/4 v4, -0x1

    invoke-virtual {p3, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mExpandedGroupPosition:I

    .line 82
    :cond_1
    return-object v2
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 182
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v2

    if-nez v2, :cond_1

    .line 183
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;

    invoke-virtual {v2, p2}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 185
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 186
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 189
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/HelpHubActivityBase;

    .line 190
    .local v0, "activity":Lcom/samsung/helphub/HelpHubActivityBase;
    invoke-virtual {v0}, Lcom/samsung/helphub/HelpHubActivityBase;->getItemNum()I

    move-result v1

    .line 191
    .local v1, "itemNum":I
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->selectItem(I)V

    .line 192
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mPageListLoadListener:Lcom/samsung/helphub/fragments/tablet/PageListLoadListener;

    if-eqz v2, :cond_1

    .line 193
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mPageListLoadListener:Lcom/samsung/helphub/fragments/tablet/PageListLoadListener;

    invoke-interface {v2}, Lcom/samsung/helphub/fragments/tablet/PageListLoadListener;->pageListLoaded()V

    .line 195
    .end local v0    # "activity":Lcom/samsung/helphub/HelpHubActivityBase;
    .end local v1    # "itemNum":I
    :cond_1
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 31
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 199
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    if-nez v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 202
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 87
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 92
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 96
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 99
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;

    if-eqz v0, :cond_0

    .line 102
    const-string v0, "item_list:opened_item"

    iget v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mExpandedGroupPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 104
    :cond_0
    return-void
.end method

.method public openPage(Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 6
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 220
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;

    if-eqz v2, :cond_0

    .line 221
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;

    invoke-virtual {v2, p1}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->getGroupPosition(Lcom/samsung/helphub/headers/HelpHeader;)I

    move-result v1

    .line 222
    .local v1, "groupPos":I
    if-ltz v1, :cond_0

    .line 224
    :try_start_0
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 226
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;

    new-instance v3, Lcom/samsung/helphub/fragments/tablet/HelpPagesList$MoveExpandingItemRunnable;

    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;

    invoke-direct {v3, v4, v1}, Lcom/samsung/helphub/fragments/tablet/HelpPagesList$MoveExpandingItemRunnable;-><init>(Landroid/widget/ListView;I)V

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/widget/ExpandableListView;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    .end local v1    # "groupPos":I
    :cond_0
    :goto_0
    return-void

    .line 227
    .restart local v1    # "groupPos":I
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method public selectItem(I)V
    .locals 3
    .param p1, "itemNum"    # I

    .prologue
    .line 205
    add-int/lit8 v1, p1, -0x1

    .line 206
    .local v1, "index":I
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2}, Landroid/widget/ExpandableListView;->getCount()I

    move-result v2

    if-lt v2, p1, :cond_1

    if-lez p1, :cond_1

    .line 207
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2}, Landroid/widget/ExpandableListView;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 208
    if-ne v0, v1, :cond_0

    .line 209
    iput v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mExpandedGroupPosition:I

    .line 210
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v0}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 207
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 212
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mListView:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v0}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    goto :goto_1

    .line 214
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method public setPageListLoadListener(Lcom/samsung/helphub/fragments/tablet/PageListLoadListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/helphub/fragments/tablet/PageListLoadListener;

    .prologue
    .line 216
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesList;->mPageListLoadListener:Lcom/samsung/helphub/fragments/tablet/PageListLoadListener;

    .line 217
    return-void
.end method
