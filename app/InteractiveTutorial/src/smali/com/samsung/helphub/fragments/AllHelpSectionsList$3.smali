.class Lcom/samsung/helphub/fragments/AllHelpSectionsList$3;
.super Ljava/lang/Object;
.source "AllHelpSectionsList.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnGroupExpandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/AllHelpSectionsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$3;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupExpand(I)V
    .locals 4
    .param p1, "groupPosition"    # I

    .prologue
    .line 286
    iget-object v0, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$3;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    # getter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$000(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    const v1, 0x7f0d00a5

    if-ne v0, v1, :cond_0

    .line 287
    iget-object v0, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$3;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    # getter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHeadersListView:Landroid/widget/ExpandableListView;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$100(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)Landroid/widget/ExpandableListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 292
    :goto_0
    return-void

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$3;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    # setter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mExpandedGroup:I
    invoke-static {v0, p1}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$202(Lcom/samsung/helphub/fragments/AllHelpSectionsList;I)I

    .line 290
    iget-object v0, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$3;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    # getter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHeadersListView:Landroid/widget/ExpandableListView;
    invoke-static {v0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$100(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)Landroid/widget/ExpandableListView;

    move-result-object v0

    new-instance v1, Lcom/samsung/helphub/fragments/AllHelpSectionsList$Collapser;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$3;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-direct {v1, v2, p1}, Lcom/samsung/helphub/fragments/AllHelpSectionsList$Collapser;-><init>(Lcom/samsung/helphub/fragments/AllHelpSectionsList;I)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ExpandableListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
