.class public Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;
.super Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;
.source "ExternalHeaderContentsList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;
    }
.end annotation


# instance fields
.field private mState:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

.field private mTargetSection:Lcom/samsung/helphub/headers/HelpHeader;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;-><init>()V

    .line 27
    sget-object v0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;->NOT_INITIALIZED:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mState:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    .line 19
    return-void
.end method

.method private doOpenCategory()V
    .locals 3

    .prologue
    .line 112
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mTargetSection:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourCategory(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mTargetSection:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {p0, v1}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->openGuideTour(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mTargetSection:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getGroupPosition(Lcom/samsung/helphub/headers/HelpHeader;)I

    move-result v0

    .line 116
    .local v0, "groupPosition":I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v1, v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getChildrenCount(I)I

    move-result v1

    if-lez v1, :cond_0

    .line 117
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 118
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->doActiveSectionItem(II)V

    goto :goto_0
.end method

.method private doOpenSection()V
    .locals 8

    .prologue
    .line 76
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mTargetSection:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourCategory(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 77
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mTargetSection:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {p0, v3}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->openGuideTour(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mTargetSection:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v3, v4}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getGroupPosition(Lcom/samsung/helphub/headers/HelpHeader;)I

    move-result v2

    .line 81
    .local v2, "groupPosition":I
    if-ltz v2, :cond_0

    .line 82
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mTargetSection:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v3, v4, v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getChildPosition(Lcom/samsung/helphub/headers/HelpHeader;I)I

    move-result v0

    .line 84
    .local v0, "childPosition":I
    if-ltz v2, :cond_0

    if-ltz v0, :cond_0

    .line 85
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, v2}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 86
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/ExternalHelpActivity;

    .line 87
    .local v1, "externalAcivity":Lcom/samsung/helphub/ExternalHelpActivity;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/samsung/helphub/ExternalHelpActivity;->isUseDelay()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 88
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    new-instance v4, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$1;

    invoke-direct {v4, p0, v2, v0}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$1;-><init>(Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;II)V

    const-wide/16 v6, 0xa

    invoke-virtual {v3, v4, v6, v7}, Landroid/widget/ExpandableListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 95
    :cond_2
    invoke-virtual {p0, v2, v0}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->doActiveSectionItem(II)V

    goto :goto_0
.end method


# virtual methods
.method protected makeupIinitialView()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->onActivityCreated(Landroid/os/Bundle;)V

    .line 32
    sget-object v0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;->LOADING:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mState:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    .line 33
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-super {p0, p1, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 49
    iget v0, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mLoadFinishCount:I

    if-nez v0, :cond_0

    .line 50
    sget-object v0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;->STANDBY:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mState:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    .line 51
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mTargetSection:Lcom/samsung/helphub/headers/HelpHeader;

    if-eqz v0, :cond_0

    .line 52
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->doOpenSection()V

    .line 55
    :cond_0
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 15
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x0

    .line 37
    const v2, 0x7f0d0457

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 38
    .local v0, "mMenuItemSearch":Landroid/view/MenuItem;
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 40
    const v2, 0x7f0d0456

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 41
    .local v1, "mMenuItemSort":Landroid/view/MenuItem;
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 43
    invoke-super {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 44
    return-void
.end method

.method public openCategory(Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 2
    .param p1, "section"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mTargetSection:Lcom/samsung/helphub/headers/HelpHeader;

    .line 106
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mState:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    sget-object v1, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;->STANDBY:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    if-ne v0, v1, :cond_0

    .line 107
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->doOpenCategory()V

    .line 109
    :cond_0
    return-void
.end method

.method public openSection(Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 2
    .param p1, "section"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mTargetSection:Lcom/samsung/helphub/headers/HelpHeader;

    .line 70
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->mState:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    sget-object v1, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;->STANDBY:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    if-ne v0, v1, :cond_0

    .line 71
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;->doOpenSection()V

    .line 73
    :cond_0
    return-void
.end method
