.class Lcom/samsung/helphub/fragments/AllHelpSectionsList$1;
.super Ljava/lang/Object;
.source "AllHelpSectionsList.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnChildClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/fragments/AllHelpSectionsList;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$1;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 3
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I
    .param p5, "id"    # J

    .prologue
    .line 82
    iget-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$1;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    # getter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    invoke-static {v2}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$000(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v1

    .line 83
    .local v1, "titleId":I
    iget-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$1;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    # getter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    invoke-static {v2}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$000(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    .line 84
    .local v0, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v0, :cond_0

    .line 85
    iget-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$1;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-virtual {v2, v0, v1}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;I)V

    .line 87
    :cond_0
    const/4 v2, 0x1

    return v2
.end method
