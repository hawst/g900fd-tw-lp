.class Lcom/samsung/helphub/fragments/CategoryHeaderList$3;
.super Ljava/lang/Object;
.source "CategoryHeaderList.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/fragments/CategoryHeaderList;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/CategoryHeaderList;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/CategoryHeaderList;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList$3;->this$0:Lcom/samsung/helphub/fragments/CategoryHeaderList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 105
    iget-object v3, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList$3;->this$0:Lcom/samsung/helphub/fragments/CategoryHeaderList;

    invoke-virtual {v3}, Lcom/samsung/helphub/fragments/CategoryHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList$3;->this$0:Lcom/samsung/helphub/fragments/CategoryHeaderList;

    # getter for: Lcom/samsung/helphub/fragments/CategoryHeaderList;->mButtonSort:Landroid/widget/ImageButton;
    invoke-static {v4}, Lcom/samsung/helphub/fragments/CategoryHeaderList;->access$000(Lcom/samsung/helphub/fragments/CategoryHeaderList;)Landroid/widget/ImageButton;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 108
    .local v0, "DescriptionToast":Landroid/widget/Toast;
    iget-object v3, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList$3;->this$0:Lcom/samsung/helphub/fragments/CategoryHeaderList;

    # getter for: Lcom/samsung/helphub/fragments/CategoryHeaderList;->mButtonSort:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/CategoryHeaderList;->access$000(Lcom/samsung/helphub/fragments/CategoryHeaderList;)Landroid/widget/ImageButton;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    div-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList$3;->this$0:Lcom/samsung/helphub/fragments/CategoryHeaderList;

    # getter for: Lcom/samsung/helphub/fragments/CategoryHeaderList;->mButtonSearch:Landroid/widget/ImageButton;
    invoke-static {v4}, Lcom/samsung/helphub/fragments/CategoryHeaderList;->access$100(Lcom/samsung/helphub/fragments/CategoryHeaderList;)Landroid/widget/ImageButton;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    add-int v1, v3, v4

    .line 109
    .local v1, "offsetX":I
    iget-object v3, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList$3;->this$0:Lcom/samsung/helphub/fragments/CategoryHeaderList;

    # getter for: Lcom/samsung/helphub/fragments/CategoryHeaderList;->mButtonSort:Landroid/widget/ImageButton;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/CategoryHeaderList;->access$000(Lcom/samsung/helphub/fragments/CategoryHeaderList;)Landroid/widget/ImageButton;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 111
    .local v2, "offsetY":I
    const/16 v3, 0x35

    invoke-virtual {v0, v3, v1, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 113
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 114
    const/4 v3, 0x1

    return v3
.end method
