.class public Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;
.super Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;
.source "HeaderContentsList.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field protected static final CHILD_LOADER_ID:I = 0xb

.field protected static final GROUP_LOADER_ID:I = 0xa

.field private static mChildItemSelected:I

.field private static mGroupItemSelected:I


# instance fields
.field protected mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

.field protected mList:Landroid/widget/ExpandableListView;

.field protected mLoadFinishCount:I

.field private mOnChildClickListener:Landroid/widget/ExpandableListView$OnChildClickListener;

.field private mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mLoadFinishCount:I

    .line 170
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$3;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$3;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mOnChildClickListener:Landroid/widget/ExpandableListView$OnChildClickListener;

    .line 253
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 31
    sget v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mGroupItemSelected:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 31
    sput p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mGroupItemSelected:I

    return p0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 31
    sget v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mChildItemSelected:I

    return v0
.end method

.method static synthetic access$102(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 31
    sput p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mChildItemSelected:I

    return p0
.end method

.method private doInitializeLoader()V
    .locals 4

    .prologue
    const/16 v3, 0xb

    const/16 v2, 0xa

    const/4 v1, 0x0

    .line 50
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/LoaderManager;->getLoader(I)Landroid/content/Loader;

    move-result-object v0

    if-nez v0, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v1, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 55
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/LoaderManager;->getLoader(I)Landroid/content/Loader;

    move-result-object v0

    if-nez v0, :cond_1

    .line 56
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v1, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 60
    :goto_1
    return-void

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v1, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0

    .line 58
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v1, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_1
.end method


# virtual methods
.method protected doActiveSectionItem(II)V
    .locals 4
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 206
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const v3, 0x7f0a001e

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(I)V

    .line 207
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v2, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/headers/HelpHeader;

    .line 208
    .local v1, "owner":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v1, :cond_1

    .line 209
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourCategory(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 210
    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v2

    if-nez v2, :cond_1

    .line 211
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->openGuideTour(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 223
    :cond_1
    :goto_0
    return-void

    .line 213
    :cond_2
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v2, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getChildrenCount(I)I

    move-result v2

    if-lez v2, :cond_1

    .line 214
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v2, p1, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    .line 215
    .local v0, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v0, :cond_1

    .line 216
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v2, p1, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->setActiveItem(II)V

    .line 218
    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v2

    if-nez v2, :cond_1

    .line 219
    invoke-virtual {p0, v1, v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->openHelpPage(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;)V

    goto :goto_0
.end method

.method public doActiveSectionItem(Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 6
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 226
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v4, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getGroupPosition(Lcom/samsung/helphub/headers/HelpHeader;)I

    move-result v2

    .line 228
    .local v2, "groupPosition":I
    if-ltz v2, :cond_0

    .line 229
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v4

    const v5, 0x7f0d00a3

    if-ne v4, v5, :cond_1

    .line 231
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->openOnlineHelp(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v4

    invoke-static {v4}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourCategory(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 234
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->setActiveItem(II)V

    .line 235
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->openGuideTour(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 237
    :cond_2
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v4, p1, v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getChildPosition(Lcom/samsung/helphub/headers/HelpHeader;I)I

    move-result v0

    .line 239
    .local v0, "childPosition":I
    if-ltz v2, :cond_0

    if-ltz v0, :cond_0

    .line 240
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v4}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getGroupCount()I

    move-result v1

    .line 241
    .local v1, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v1, :cond_4

    .line 242
    if-ne v3, v2, :cond_3

    .line 243
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v4, v3}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 241
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 245
    :cond_3
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v4, v3}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    goto :goto_2

    .line 248
    :cond_4
    invoke-virtual {p0, v2, v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->doActiveSectionItem(II)V

    goto :goto_0
.end method

.method protected launchFragment(II)V
    .locals 3
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 309
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v2, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/headers/HelpHeader;

    .line 310
    .local v1, "owner":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v1, :cond_0

    .line 311
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourCategory(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 312
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->openGuideTour(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v2, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getChildrenCount(I)I

    move-result v2

    if-lez v2, :cond_0

    .line 314
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v2, p1, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    .line 315
    .local v0, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v0, :cond_0

    .line 316
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v2, p1, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->setActiveItem(II)V

    .line 317
    invoke-virtual {p0, v1, v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->openHelpPage(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;)V

    goto :goto_0
.end method

.method protected makeupIinitialView()V
    .locals 8

    .prologue
    const-wide/16 v6, 0xa

    .line 126
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/HelpHubActivityBase;

    .line 127
    .local v0, "activity":Lcom/samsung/helphub/HelpHubActivityBase;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/helphub/HelpHubActivityBase;->checkCreateByIntent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 129
    invoke-virtual {v0}, Lcom/samsung/helphub/HelpHubActivityBase;->processIntent()V

    .line 168
    :goto_0
    return-void

    .line 132
    :cond_0
    const/4 v2, 0x0

    .line 140
    .local v2, "i":I
    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 141
    sget v1, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mGroupItemSelected:I

    .line 142
    .local v1, "group":I
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, v1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 146
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    new-instance v4, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$1;

    invoke-direct {v4, p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$1;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;)V

    invoke-virtual {v3, v4, v6, v7}, Landroid/widget/ExpandableListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 154
    .end local v1    # "group":I
    :cond_1
    move v1, v2

    .line 155
    .restart local v1    # "group":I
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v3, v1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 159
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    new-instance v4, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$2;

    invoke-direct {v4, p0, v1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$2;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;I)V

    invoke-virtual {v3, v4, v6, v7}, Landroid/widget/ExpandableListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->onActivityCreated(Landroid/os/Bundle;)V

    .line 46
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->doInitializeLoader()V

    .line 47
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 78
    const/4 v2, 0x0

    .line 79
    .local v2, "uri":Landroid/net/Uri;
    packed-switch p1, :pswitch_data_0

    .line 92
    :goto_0
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v3, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 81
    :pswitch_0
    const-string v0, "content://com.samsung.helphub.provider.search/header/category"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 82
    goto :goto_0

    .line 85
    :pswitch_1
    const-string v0, "content://com.samsung.helphub.provider.search/header/section"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 86
    goto :goto_0

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 66
    const v1, 0x7f0400b6

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 67
    .local v0, "result":Landroid/view/View;
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ExpandableListView;

    iput-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    .line 68
    new-instance v1, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    .line 69
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 70
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mOnChildClickListener:Landroid/widget/ExpandableListView$OnChildClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 71
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 73
    return-object v0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_2

    .line 99
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v0, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->changeGroupCursor(Landroid/database/Cursor;)V

    .line 103
    :cond_0
    :goto_0
    iget v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mLoadFinishCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mLoadFinishCount:I

    .line 106
    iget v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mLoadFinishCount:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 107
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->makeupIinitialView()V

    .line 108
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mLoadFinishCount:I

    .line 110
    :cond_1
    return-void

    .line 100
    :cond_2
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 101
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v0, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->changeChildCursor(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 31
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const/4 v2, 0x0

    .line 114
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 115
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v0, v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->changeGroupCursor(Landroid/database/Cursor;)V

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 117
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v0, v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->changeChildCursor(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public scrollHeaderList()V
    .locals 6

    .prologue
    .line 324
    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 325
    sget v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mGroupItemSelected:I

    .line 326
    .local v0, "group":I
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 327
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mList:Landroid/widget/ExpandableListView;

    new-instance v2, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$5;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$5;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;)V

    const-wide/16 v4, 0xa

    invoke-virtual {v1, v2, v4, v5}, Landroid/widget/ExpandableListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 335
    .end local v0    # "group":I
    :cond_0
    return-void
.end method
