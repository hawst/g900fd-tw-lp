.class public Lcom/samsung/helphub/fragments/HelpSearchList;
.super Landroid/app/Fragment;
.source "HelpSearchList.java"


# static fields
.field public static final ARGUMENT_KEY_QUERY:Ljava/lang/String; = "search_list_arg:query"

.field protected static final BUNDLE_KEY_QUERY:Ljava/lang/String; = "search_list:query"


# instance fields
.field protected mActionBar:Landroid/app/ActionBar;

.field protected mAdapter:Lcom/samsung/helphub/fragments/SearchListAdapter;

.field protected mListView:Landroid/widget/ListView;

.field protected mMenuItemSearch:Landroid/view/MenuItem;

.field protected mNoResultFoundButton:Landroid/widget/Button;

.field protected mNoResultFoundRelativeLayout:Landroid/widget/LinearLayout;

.field protected mNoResultFoundTextView:Landroid/widget/TextView;

.field protected mQuery:Ljava/lang/String;

.field protected mResultFoundButton:Landroid/widget/Button;

.field protected mSearchResult:Landroid/widget/LinearLayout;

.field protected mSearchView:Landroid/widget/SearchView;

.field onlineSearchClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mQuery:Ljava/lang/String;

    .line 132
    new-instance v0, Lcom/samsung/helphub/fragments/HelpSearchList$2;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/HelpSearchList$2;-><init>(Lcom/samsung/helphub/fragments/HelpSearchList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->onlineSearchClickListener:Landroid/view/View$OnClickListener;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/fragments/HelpSearchList;Lcom/samsung/helphub/headers/HelpHeader;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/HelpSearchList;
    .param p1, "x1"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p2, "x2"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/samsung/helphub/fragments/HelpSearchList;->launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;Z)V

    return-void
.end method

.method private excludedExpandableHeader(Lcom/samsung/helphub/headers/HelpHeader;)Z
    .locals 2
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 341
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    const v1, 0x7f0d0103

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    const v1, 0x7f0d0102

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    const v1, 0x7f0d0104

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;Z)V
    .locals 6
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p2, "addToBackStack"    # Z

    .prologue
    .line 211
    new-instance v0, Landroid/os/Bundle;

    const/4 v4, 0x2

    invoke-direct {v0, v4}, Landroid/os/Bundle;-><init>(I)V

    .line 212
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/samsung/helphub/HelpHubCommon;->isResourceAvailable(Lcom/samsung/helphub/headers/HelpHeader;Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 215
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v4

    sget-object v5, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v4, v5, :cond_2

    invoke-direct {p0, p1}, Lcom/samsung/helphub/fragments/HelpSearchList;->excludedExpandableHeader(Lcom/samsung/helphub/headers/HelpHeader;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 217
    const-string v4, "section_list:expanded_section_name"

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://com.samsung.helphub.provider.search/header/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getOwnerResId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/helphub/fragments/HelpSearchList;->queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object p1

    .line 243
    :cond_0
    :goto_0
    const-string v4, "helpub:header"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 244
    if-eqz p1, :cond_1

    .line 245
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 246
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->isVideoPath()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 247
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/helphub/HelpHubCommon;->startVideo(Ljava/lang/String;Landroid/content/Context;)V

    .line 264
    :cond_1
    :goto_1
    return-void

    .line 223
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v4

    sget-object v5, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v4, v5, :cond_0

    .line 225
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/samsung/helphub/HelpHubCommon;->isResourceAvailable(Lcom/samsung/helphub/headers/HelpHeader;Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 226
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://com.samsung.helphub.provider.search/header/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getOwnerResId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/helphub/fragments/HelpSearchList;->queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v2

    .line 230
    .local v2, "owner":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v2, :cond_0

    .line 231
    const/4 v4, 0x1

    invoke-direct {p0, v2, v4}, Lcom/samsung/helphub/fragments/HelpSearchList;->launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;Z)V

    .line 232
    const-string v4, "page_fragment_argument:activity_title"

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 237
    .end local v2    # "owner":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/helphub/HelpHubCommon;->requestDownloadingResource(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_1

    .line 250
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v1

    .line 251
    .local v1, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 252
    .local v3, "transaction":Landroid/app/FragmentTransaction;
    const/16 v4, 0x1001

    invoke-virtual {v3, v4}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 253
    const v4, 0x7f0d0004

    invoke-virtual {v3, v4, v1}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 254
    if-eqz p2, :cond_5

    .line 255
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 257
    :cond_5
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_1

    .line 262
    .end local v1    # "fragment":Landroid/app/Fragment;
    .end local v3    # "transaction":Landroid/app/FragmentTransaction;
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getPluginAppId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/helphub/HelpHubCommon;->requestDownloadingResource(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_1
.end method


# virtual methods
.method protected findOnlineHelp()Z
    .locals 4

    .prologue
    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "content://com.samsung.helphub.provider.search"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 193
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v2, "/header/category/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "online_help"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/fragments/HelpSearchList;->queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v1

    .line 196
    .local v1, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-nez v1, :cond_0

    .line 197
    const/4 v2, 0x0

    .line 200
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method protected hideSearchView()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 367
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 368
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mAdapter:Lcom/samsung/helphub/fragments/SearchListAdapter;

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mAdapter:Lcom/samsung/helphub/fragments/SearchListAdapter;

    invoke-virtual {v0, v1, v1}, Lcom/samsung/helphub/fragments/SearchListAdapter;->changeCursor(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchView:Landroid/widget/SearchView;

    const-string v1, ""

    invoke-virtual {v0, v1, v3}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 372
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 374
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 376
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mMenuItemSearch:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 377
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mMenuItemSearch:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 378
    :cond_1
    return-void
.end method

.method public hideSoftKeyboard()V
    .locals 3

    .prologue
    .line 399
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 400
    .local v0, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 401
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 402
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 74
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 302
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const v2, 0x7f0f0001

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 303
    const v1, 0x7f0d0457

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mMenuItemSearch:Landroid/view/MenuItem;

    .line 304
    iget-object v1, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mActionBar:Landroid/app/ActionBar;

    const v2, 0x7f040002

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 307
    iget-object v1, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SearchView;

    iput-object v1, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchView:Landroid/widget/SearchView;

    .line 308
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->showSearchView()V

    .line 309
    iget-object v1, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchView:Landroid/widget/SearchView;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mQuery:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 310
    iget-object v1, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->clearFocus()V

    .line 312
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "search"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 315
    .local v0, "searchManager":Landroid/app/SearchManager;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchView:Landroid/widget/SearchView;

    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/samsung/helphub/HelpHubSearchActivity;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 319
    iget-object v1, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchView:Landroid/widget/SearchView;

    new-instance v2, Lcom/samsung/helphub/fragments/HelpSearchList$3;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/fragments/HelpSearchList$3;-><init>(Lcom/samsung/helphub/fragments/HelpSearchList;)V

    invoke-virtual {v1, v2}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 334
    invoke-super {p0, p1, p2}, Landroid/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 335
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 79
    const v2, 0x7f0400bb

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 80
    .local v1, "result":Landroid/view/View;
    const v2, 0x102000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mListView:Landroid/widget/ListView;

    .line 81
    const v2, 0x7f0d0373

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchResult:Landroid/widget/LinearLayout;

    .line 82
    const v2, 0x7f0d0370

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mNoResultFoundRelativeLayout:Landroid/widget/LinearLayout;

    .line 84
    const v2, 0x7f0d0371

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mNoResultFoundTextView:Landroid/widget/TextView;

    .line 85
    const v2, 0x7f0d0372

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mNoResultFoundButton:Landroid/widget/Button;

    .line 86
    const v2, 0x7f0d0374

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mResultFoundButton:Landroid/widget/Button;

    .line 87
    const v2, 0x7f0d0009

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/fragments/HelpSearchList;->setupUI(Landroid/view/View;)V

    .line 88
    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mListView:Landroid/widget/ListView;

    new-instance v3, Lcom/samsung/helphub/fragments/HelpSearchList$1;

    invoke-direct {v3, p0}, Lcom/samsung/helphub/fragments/HelpSearchList$1;-><init>(Lcom/samsung/helphub/fragments/HelpSearchList;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 102
    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mNoResultFoundButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->onlineSearchClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mResultFoundButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->onlineSearchClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    if-eqz p3, :cond_2

    .line 106
    const-string v2, "search_list:query"

    invoke-virtual {p3, v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mQuery:Ljava/lang/String;

    .line 113
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mQuery:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 114
    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mQuery:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/fragments/HelpSearchList;->search(Ljava/lang/String;)V

    .line 126
    :cond_1
    :goto_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/fragments/HelpSearchList;->setHasOptionsMenu(Z)V

    .line 127
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mActionBar:Landroid/app/ActionBar;

    .line 129
    return-object v1

    .line 108
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 109
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 110
    const-string v2, "search_list_arg:query"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mQuery:Ljava/lang/String;

    goto :goto_0

    .line 117
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_3
    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchResult:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 118
    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mNoResultFoundRelativeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 119
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->findOnlineHelp()Z

    move-result v2

    if-nez v2, :cond_1

    .line 120
    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mNoResultFoundTextView:Landroid/widget/TextView;

    const v3, 0x7f0a06b4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 121
    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mNoResultFoundButton:Landroid/widget/Button;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 408
    .local v0, "focusView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 409
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 413
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v2, :cond_0

    .line 414
    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v2}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 418
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v2, :cond_1

    .line 419
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->hideSearchView()V

    .line 421
    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 422
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 144
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 145
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0a0020

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 146
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 269
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 270
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->isIconified()Z

    move-result v0

    if-nez v0, :cond_0

    .line 271
    const-string v1, "search_list:query"

    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :cond_0
    return-void
.end method

.method protected queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 283
    const/4 v6, 0x0

    .line 284
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 286
    .local v7, "result":Lcom/samsung/helphub/headers/HelpHeader;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 288
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    invoke-static {v6}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 292
    :cond_0
    if-eqz v6, :cond_1

    .line 293
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 296
    :cond_1
    return-object v7

    .line 292
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 293
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method protected search(Ljava/lang/String;)V
    .locals 13
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/16 v12, 0x8

    const/4 v11, 0x0

    .line 149
    const-string v0, "%"

    const-string v2, ""

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "_"

    const-string v4, ""

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 150
    const-string v0, "content://com.samsung.helphub.provider.search/header/search"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 151
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v11

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 155
    .local v6, "cursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mAdapter:Lcom/samsung/helphub/fragments/SearchListAdapter;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mAdapter:Lcom/samsung/helphub/fragments/SearchListAdapter;

    invoke-virtual {v0, v6, p1}, Lcom/samsung/helphub/fragments/SearchListAdapter;->changeCursor(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 161
    :goto_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mAdapter:Lcom/samsung/helphub/fragments/SearchListAdapter;

    invoke-virtual {v0}, Lcom/samsung/helphub/fragments/SearchListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 164
    const/4 v10, 0x0

    .line 165
    .local v10, "totalHeight":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mAdapter:Lcom/samsung/helphub/fragments/SearchListAdapter;

    invoke-virtual {v0}, Lcom/samsung/helphub/fragments/SearchListAdapter;->getCount()I

    move-result v0

    if-ge v7, v0, :cond_1

    .line 166
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mAdapter:Lcom/samsung/helphub/fragments/SearchListAdapter;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v7, v3, v2}, Lcom/samsung/helphub/fragments/SearchListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 167
    .local v8, "listItem":Landroid/view/View;
    invoke-virtual {v8, v11, v11}, Landroid/view/View;->measure(II)V

    .line 168
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v10, v0

    .line 165
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 158
    .end local v7    # "i":I
    .end local v8    # "listItem":Landroid/view/View;
    .end local v10    # "totalHeight":I
    :cond_0
    new-instance v0, Lcom/samsung/helphub/fragments/SearchListAdapter;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2, v6, p1}, Lcom/samsung/helphub/fragments/SearchListAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mAdapter:Lcom/samsung/helphub/fragments/SearchListAdapter;

    goto :goto_0

    .line 170
    .restart local v7    # "i":I
    .restart local v10    # "totalHeight":I
    :cond_1
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .line 171
    .local v9, "params":Landroid/view/ViewGroup$LayoutParams;
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v0

    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v2

    mul-int/2addr v0, v2

    add-int/2addr v0, v10

    iput v0, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 172
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchResult:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mNoResultFoundRelativeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 177
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->findOnlineHelp()Z

    move-result v0

    if-nez v0, :cond_2

    .line 178
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mResultFoundButton:Landroid/widget/Button;

    invoke-virtual {v0, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 188
    .end local v7    # "i":I
    .end local v9    # "params":Landroid/view/ViewGroup$LayoutParams;
    .end local v10    # "totalHeight":I
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mAdapter:Lcom/samsung/helphub/fragments/SearchListAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 189
    return-void

    .line 181
    :cond_3
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchResult:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 182
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mNoResultFoundRelativeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 183
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpSearchList;->findOnlineHelp()Z

    move-result v0

    if-nez v0, :cond_2

    .line 184
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mNoResultFoundTextView:Landroid/widget/TextView;

    const v2, 0x7f0a06b4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 185
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mNoResultFoundButton:Landroid/widget/Button;

    invoke-virtual {v0, v12}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2
.end method

.method public setupUI(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 382
    instance-of v2, p1, Landroid/widget/EditText;

    if-nez v2, :cond_0

    .line 383
    new-instance v2, Lcom/samsung/helphub/fragments/HelpSearchList$4;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/fragments/HelpSearchList$4;-><init>(Lcom/samsung/helphub/fragments/HelpSearchList;)V

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 391
    :cond_0
    instance-of v2, p1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    .line 392
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    move-object v2, p1

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    move-object v2, p1

    .line 393
    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 394
    .local v1, "innerView":Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/fragments/HelpSearchList;->setupUI(Landroid/view/View;)V

    .line 392
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 397
    .end local v0    # "i":I
    .end local v1    # "innerView":Landroid/view/View;
    :cond_1
    return-void
.end method

.method protected showSearchView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 353
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mMenuItemSearch:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 354
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 357
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 359
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpSearchList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 360
    return-void
.end method
