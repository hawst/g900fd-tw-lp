.class Lcom/samsung/helphub/fragments/HelpHeaderList$1;
.super Ljava/lang/Object;
.source "HelpHeaderList.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/HelpHeaderList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/HelpHeaderList;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/HelpHeaderList;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lcom/samsung/helphub/fragments/HelpHeaderList$1;->this$0:Lcom/samsung/helphub/fragments/HelpHeaderList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 206
    const/4 v2, 0x0

    .line 208
    .local v2, "position":I
    iget-object v3, p0, Lcom/samsung/helphub/fragments/HelpHeaderList$1;->this$0:Lcom/samsung/helphub/fragments/HelpHeaderList;

    iget-object v1, v3, Lcom/samsung/helphub/fragments/HelpHeaderList;->headerLayout:Landroid/widget/LinearLayout;

    .line 209
    .local v1, "parent":Landroid/view/ViewGroup;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 210
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    if-ne v3, v4, :cond_0

    .line 211
    move v2, v0

    .line 209
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 213
    :cond_1
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 214
    iget-object v4, p0, Lcom/samsung/helphub/fragments/HelpHeaderList$1;->this$0:Lcom/samsung/helphub/fragments/HelpHeaderList;

    iget-object v3, p0, Lcom/samsung/helphub/fragments/HelpHeaderList$1;->this$0:Lcom/samsung/helphub/fragments/HelpHeaderList;

    # getter for: Lcom/samsung/helphub/fragments/HelpHeaderList;->mHeaders:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/HelpHeaderList;->access$000(Lcom/samsung/helphub/fragments/HelpHeaderList;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v4, v3}, Lcom/samsung/helphub/fragments/HelpHeaderList;->launchToSecondDepthActivity(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 218
    :goto_1
    return-void

    .line 216
    :cond_2
    iget-object v4, p0, Lcom/samsung/helphub/fragments/HelpHeaderList$1;->this$0:Lcom/samsung/helphub/fragments/HelpHeaderList;

    iget-object v3, p0, Lcom/samsung/helphub/fragments/HelpHeaderList$1;->this$0:Lcom/samsung/helphub/fragments/HelpHeaderList;

    # getter for: Lcom/samsung/helphub/fragments/HelpHeaderList;->mHeaders:Ljava/util/List;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/HelpHeaderList;->access$000(Lcom/samsung/helphub/fragments/HelpHeaderList;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v4, v3}, Lcom/samsung/helphub/fragments/HelpHeaderList;->launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;)V

    goto :goto_1
.end method
