.class public Lcom/samsung/helphub/fragments/HelpItemsListAdapter;
.super Landroid/widget/BaseAdapter;
.source "HelpItemsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mExpanded:[Z

.field private mInflater:Landroid/view/LayoutInflater;

.field private mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation
.end field

.field private mOnHelpItemViewClickListener:Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;

.field private mParentFragment:Landroid/app/Fragment;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILandroid/app/Fragment;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "resXmlID"    # I
    .param p3, "parentFragment"    # Landroid/app/Fragment;

    .prologue
    const/4 v2, 0x1

    .line 39
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 44
    invoke-static {v2}, Landroid/app/FragmentManager;->enableDebugLogging(Z)V

    .line 46
    iput-object p1, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mActivity:Landroid/app/Activity;

    .line 47
    iput-object p3, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mParentFragment:Landroid/app/Fragment;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mItems:Ljava/util/List;

    .line 49
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mItems:Ljava/util/List;

    invoke-static {v0, p2, v1}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->loadHeadersFromResource(Landroid/content/Context;ILjava/util/List;)V

    .line 51
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->getCount()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mExpanded:[Z

    .line 52
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->getCount()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 54
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mExpanded:[Z

    const/4 v1, 0x0

    aput-boolean v2, v0, v1

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mActivity:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 57
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mParentFragment:Landroid/app/Fragment;

    .line 201
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 65
    const/4 v0, 0x0

    .line 66
    .local v0, "result":I
    iget-object v1, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mItems:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 69
    :cond_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 81
    add-int/lit8 v0, p1, 0x1

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 192
    return p1
.end method

.method public getOpenedItem()I
    .locals 3

    .prologue
    .line 175
    const/4 v1, -0x1

    .line 176
    .local v1, "result":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mExpanded:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 177
    iget-object v2, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mExpanded:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_1

    .line 178
    move v1, v0

    .line 182
    :cond_0
    return v1

    .line 176
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 87
    const/4 v4, 0x0

    .line 88
    .local v4, "result":Lcom/samsung/helphub/widget/HelpItemExpView;
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    .line 90
    .local v2, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-nez p2, :cond_3

    .line 93
    iget-object v6, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f0400b8

    const/4 v8, 0x0

    invoke-virtual {v6, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .end local v4    # "result":Lcom/samsung/helphub/widget/HelpItemExpView;
    check-cast v4, Lcom/samsung/helphub/widget/HelpItemExpView;

    .line 96
    .restart local v4    # "result":Lcom/samsung/helphub/widget/HelpItemExpView;
    new-instance v3, Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;

    invoke-direct {v3}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;-><init>()V

    .line 97
    .local v3, "holder":Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;
    const v6, 0x7f0d008a

    invoke-virtual {v4, v6}, Lcom/samsung/helphub/widget/HelpItemExpView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v3, Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;->title:Landroid/widget/TextView;

    .line 98
    const v6, 0x7f0d0088

    invoke-virtual {v4, v6}, Lcom/samsung/helphub/widget/HelpItemExpView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    iput-object v6, v3, Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    .line 100
    iget-object v6, v3, Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->getItemId(I)J

    move-result-wide v8

    long-to-int v7, v8

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->setId(I)V

    .line 102
    invoke-virtual {v4, v3}, Lcom/samsung/helphub/widget/HelpItemExpView;->setTag(Ljava/lang/Object;)V

    .line 103
    iget-object v6, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mOnHelpItemViewClickListener:Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;

    invoke-virtual {v4, v6}, Lcom/samsung/helphub/widget/HelpItemExpView;->setOnHelpItemViewClickListener(Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;)V

    .line 104
    invoke-virtual {v4, p1}, Lcom/samsung/helphub/widget/HelpItemExpView;->setListPosition(I)V

    .line 112
    :goto_0
    iget-object v6, v3, Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getActivityTitleId()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 115
    new-instance v0, Landroid/os/Bundle;

    const/4 v6, 0x1

    invoke-direct {v0, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 116
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v6, "helpub:header"

    invoke-virtual {v0, v6, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 117
    iget-object v6, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mExpanded:[Z

    aget-boolean v6, v6, p1

    if-eqz v6, :cond_2

    .line 118
    iget-object v6, v3, Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;->pageFragment:Ljava/lang/ref/WeakReference;

    if-eqz v6, :cond_0

    iget-object v6, v3, Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;->pageFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 122
    :cond_0
    iget-object v6, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v1

    .line 124
    .local v1, "fragment":Landroid/app/Fragment;
    new-instance v6, Ljava/lang/ref/WeakReference;

    invoke-direct {v6, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v6, v3, Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;->pageFragment:Ljava/lang/ref/WeakReference;

    .line 127
    .end local v1    # "fragment":Landroid/app/Fragment;
    :cond_1
    iget-object v6, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mActivity:Landroid/app/Activity;

    iget-object v7, v3, Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getId()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mParentFragment:Landroid/app/Fragment;

    if-eqz v6, :cond_2

    .line 128
    iget-object v6, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mParentFragment:Landroid/app/Fragment;

    invoke-virtual {v6}, Landroid/app/Fragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v5

    .line 130
    .local v5, "transaction":Landroid/app/FragmentTransaction;
    iget-object v6, v3, Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getId()I

    move-result v7

    iget-object v6, v3, Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;->pageFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/Fragment;

    invoke-virtual {v5, v7, v6}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 131
    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 136
    .end local v5    # "transaction":Landroid/app/FragmentTransaction;
    :cond_2
    iget-object v6, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mExpanded:[Z

    aget-boolean v6, v6, p1

    invoke-virtual {v4, v6}, Lcom/samsung/helphub/widget/HelpItemExpView;->expand(Z)V

    .line 139
    return-object v4

    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "holder":Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;
    :cond_3
    move-object v4, p2

    .line 108
    check-cast v4, Lcom/samsung/helphub/widget/HelpItemExpView;

    .line 109
    invoke-virtual {v4}, Lcom/samsung/helphub/widget/HelpItemExpView;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;

    .restart local v3    # "holder":Lcom/samsung/helphub/fragments/HelpItemsListAdapter$ItemViewHolder;
    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public isToggle(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 196
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mExpanded:[Z

    aget-boolean v0, v0, p1

    return v0
.end method

.method public setOnHelpItemViewClickListener(Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mOnHelpItemViewClickListener:Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;

    .line 61
    return-void
.end method

.method public toggle(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 148
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->isToggle(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->toggle(IZ)V

    .line 149
    return-void

    .line 148
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toggle(IZ)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "isExpanded"    # Z

    .prologue
    .line 158
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mExpanded:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 159
    if-ne v0, p1, :cond_0

    .line 160
    iget-object v1, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mExpanded:[Z

    aput-boolean p2, v1, v0

    .line 158
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 162
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->mExpanded:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    goto :goto_1

    .line 165
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpItemsListAdapter;->notifyDataSetChanged()V

    .line 166
    return-void
.end method
