.class public Lcom/samsung/helphub/fragments/AllHelpSectionsList;
.super Lcom/samsung/helphub/fragments/SearchableHeaderList;
.source "AllHelpSectionsList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/AllHelpSectionsList$Collapser;
    }
.end annotation


# static fields
.field private static final BUNDLE_KEY_EXPANDED_GROUP_POSITION:Ljava/lang/String; = "section_list:expanded_group"

.field public static savedLanguage:Ljava/lang/String;


# instance fields
.field private clickedSectionHeader:Lcom/samsung/helphub/headers/HelpHeader;

.field private clickedSectionIndex:I

.field private mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

.field private mExpandedGroup:I

.field public mHandler:Landroid/os/Handler;

.field private mHeadersListView:Landroid/widget/ExpandableListView;

.field private mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

.field mOnGroupExpandListener:Landroid/widget/ExpandableListView$OnGroupExpandListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;-><init>()V

    .line 54
    iput v0, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mExpandedGroup:I

    .line 58
    iput v0, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->clickedSectionIndex:I

    .line 282
    new-instance v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$3;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList$3;-><init>(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mOnGroupExpandListener:Landroid/widget/ExpandableListView$OnGroupExpandListener;

    .line 324
    new-instance v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList$4;-><init>(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

    .line 528
    new-instance v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$5;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList$5;-><init>(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)Landroid/widget/ExpandableListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHeadersListView:Landroid/widget/ExpandableListView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/helphub/fragments/AllHelpSectionsList;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/AllHelpSectionsList;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mExpandedGroup:I

    return p1
.end method

.method static synthetic access$302(Lcom/samsung/helphub/fragments/AllHelpSectionsList;Lcom/samsung/helphub/headers/HelpHeader;)Lcom/samsung/helphub/headers/HelpHeader;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/AllHelpSectionsList;
    .param p1, "x1"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->clickedSectionHeader:Lcom/samsung/helphub/headers/HelpHeader;

    return-object p1
.end method

.method static synthetic access$402(Lcom/samsung/helphub/fragments/AllHelpSectionsList;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/AllHelpSectionsList;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->clickedSectionIndex:I

    return p1
.end method

.method private filterHeaderList(Ljava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    move-object v7, p1

    .line 192
    .local v7, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 193
    .local v1, "count":I
    add-int/lit8 v3, v1, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_5

    .line 194
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    .line 195
    .local v2, "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v8

    const v9, 0x7f0d00c0

    if-eq v8, v9, :cond_0

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v8

    const v9, 0x7f0d00c1

    if-eq v8, v9, :cond_0

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v8

    const v9, 0x7f0d00c2

    if-eq v8, v9, :cond_0

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v8

    const v9, 0x7f0d00c5

    if-eq v8, v9, :cond_0

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v8

    const v9, 0x7f0d00c6

    if-ne v8, v9, :cond_2

    .line 200
    :cond_0
    invoke-interface {p1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 193
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 203
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->getHiddenSectionHeaderId(Landroid/content/Context;)[I

    move-result-object v0

    .local v0, "arr$":[I
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_2
    if-ge v4, v6, :cond_4

    aget v5, v0, v4

    .line 204
    .local v5, "item":I
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v8

    if-ne v8, v5, :cond_3

    .line 205
    invoke-interface {p1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 203
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 208
    .end local v5    # "item":I
    :cond_4
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v8

    invoke-static {v8}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourSection(I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 209
    invoke-interface {p1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 213
    .end local v0    # "arr$":[I
    .end local v2    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    :cond_5
    return-object v7
.end method

.method private sendBroadcastForNetworkErrorPopup(I)V
    .locals 3
    .param p1, "status"    # I

    .prologue
    .line 521
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 522
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.popupuireceiver"

    const-string v2, "com.sec.android.app.popupuireceiver.popupNetworkError"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 524
    const-string v1, "network_err_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 525
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->startActivityForResult(Landroid/content/Intent;I)V

    .line 526
    return-void
.end method


# virtual methods
.method public checkNetworkState()V
    .locals 6

    .prologue
    .line 499
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 500
    new-instance v1, Lcom/samsung/helphub/UpdateCheckThread;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHandler:Landroid/os/Handler;

    const-string v5, "com.samsung.helpplugin"

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/samsung/helphub/UpdateCheckThread;-><init>(Landroid/content/Context;ZLandroid/os/Handler;Ljava/lang/String;)V

    .line 502
    .local v1, "thread":Lcom/samsung/helphub/UpdateCheckThread;
    invoke-virtual {v1}, Lcom/samsung/helphub/UpdateCheckThread;->start()V

    .line 518
    .end local v1    # "thread":Lcom/samsung/helphub/UpdateCheckThread;
    :goto_0
    return-void

    .line 504
    :cond_0
    const/4 v0, -0x1

    .line 505
    .local v0, "networkStatus":I
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isFligtMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 506
    const/4 v0, 0x0

    .line 516
    :goto_1
    invoke-direct {p0, v0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->sendBroadcastForNetworkErrorPopup(I)V

    goto :goto_0

    .line 507
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isMobileDataOff(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 508
    const/4 v0, 0x1

    goto :goto_1

    .line 509
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isRoamingOff(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 510
    const/4 v0, 0x2

    goto :goto_1

    .line 511
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isReachToDataLimit(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 512
    const/4 v0, 0x3

    goto :goto_1

    .line 514
    :cond_4
    const/4 v0, 0x4

    goto :goto_1
.end method

.method protected getHeaders()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->getSectionHeaders(Landroid/content/Context;)Ljava/util/List;

    move-result-object v9

    .line 106
    .local v9, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-direct {p0, v9}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->filterHeaderList(Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    .line 108
    new-instance v0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$2;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList$2;-><init>(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)V

    invoke-static {v9, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 154
    const/4 v7, 0x0

    .line 156
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v0, "content://com.samsung.helphub.provider.search/header/category"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 157
    .local v1, "uri":Landroid/net/Uri;
    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v0, 0xa

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 159
    .local v6, "builder":Ljava/lang/StringBuilder;
    const-string v0, "header_res_id"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " IN ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0d00a5

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0d009a

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0d00a7

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0d00a8

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const v2, 0x7f0d00a6

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x29

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 166
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 168
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    const/4 v8, 0x0

    .line 171
    .local v8, "pos":I
    :cond_0
    invoke-static {v7}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v0

    invoke-interface {v9, v8, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 172
    add-int/lit8 v8, v8, 0x1

    .line 173
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 176
    .end local v8    # "pos":I
    :cond_1
    if-eqz v7, :cond_2

    .line 177
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 180
    :cond_2
    return-object v9

    .line 176
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "builder":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_3

    .line 177
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method protected launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;I)V
    .locals 6
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;
    .param p2, "titleId"    # I

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 250
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 252
    invoke-static {p1, v0}, Lcom/samsung/helphub/HelpHubCommon;->isResourceAvailable(Lcom/samsung/helphub/headers/HelpHeader;Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 253
    new-instance v1, Landroid/os/Bundle;

    const/4 v4, 0x2

    invoke-direct {v1, v4}, Landroid/os/Bundle;-><init>(I)V

    .line 255
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v4, "helpub:header"

    invoke-virtual {v1, v4, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 256
    const-string v4, "page_fragment_argument:activity_title"

    invoke-virtual {v1, v4, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 257
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 258
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->isVideoPath()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 259
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/helphub/HelpHubCommon;->startVideo(Ljava/lang/String;Landroid/content/Context;)V

    .line 277
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 261
    .restart local v1    # "bundle":Landroid/os/Bundle;
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4, v1}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v2

    .line 263
    .local v2, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 266
    .local v3, "transaction":Landroid/app/FragmentTransaction;
    const/16 v4, 0x1001

    invoke-virtual {v3, v4}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 267
    const v4, 0x7f0d0004

    invoke-virtual {v3, v4, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 268
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 269
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0

    .line 274
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "fragment":Landroid/app/Fragment;
    .end local v3    # "transaction":Landroid/app/FragmentTransaction;
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/helphub/HelpHubCommon;->requestDownloadingResource(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 62
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 64
    const/4 v0, 0x0

    .line 66
    .local v0, "expandedSectionName":Ljava/lang/String;
    if-eqz p3, :cond_2

    .line 67
    const-string v2, "lang"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->savedLanguage:Ljava/lang/String;

    .line 68
    const-string v2, "mySection"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    iput-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->clickedSectionHeader:Lcom/samsung/helphub/headers/HelpHeader;

    .line 72
    :goto_0
    const v2, 0x7f0400bd

    invoke-virtual {p1, v2, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 73
    .local v1, "result":Landroid/view/View;
    new-instance v2, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getHeaders()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;-><init>(Ljava/util/List;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    move-object v2, v1

    .line 75
    check-cast v2, Landroid/widget/ExpandableListView;

    iput-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHeadersListView:Landroid/widget/ExpandableListView;

    .line 76
    iget-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHeadersListView:Landroid/widget/ExpandableListView;

    iget-object v3, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 77
    iget-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHeadersListView:Landroid/widget/ExpandableListView;

    new-instance v3, Lcom/samsung/helphub/fragments/AllHelpSectionsList$1;

    invoke-direct {v3, p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList$1;-><init>(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)V

    invoke-virtual {v2, v3}, Landroid/widget/ExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 90
    iget-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHeadersListView:Landroid/widget/ExpandableListView;

    iget-object v3, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mOnGroupClickListener:Landroid/widget/ExpandableListView$OnGroupClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 91
    iget-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHeadersListView:Landroid/widget/ExpandableListView;

    iget-object v3, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mOnGroupExpandListener:Landroid/widget/ExpandableListView$OnGroupExpandListener;

    invoke-virtual {v2, v3}, Landroid/widget/ExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 93
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 94
    iget-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    invoke-virtual {v2, v0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getSectionPositionForName(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mExpandedGroup:I

    .line 96
    :cond_0
    if-eqz p3, :cond_1

    .line 97
    const-string v2, "section_list:expanded_group"

    invoke-virtual {p3, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mExpandedGroup:I

    .line 99
    :cond_1
    return-object v1

    .line 70
    .end local v1    # "result":Landroid/view/View;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->savedLanguage:Ljava/lang/String;

    goto :goto_0
.end method

.method public onResume()V
    .locals 6

    .prologue
    .line 218
    invoke-super {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->onResume()V

    .line 219
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 221
    .local v1, "currentLanguage":Ljava/lang/String;
    sget-object v3, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->savedLanguage:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 223
    iget-object v3, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->clickedSectionHeader:Lcom/samsung/helphub/headers/HelpHeader;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->clickedSectionHeader:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 224
    iget-object v3, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    iget-object v4, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->clickedSectionHeader:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v3, v4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getSectionPositionForHeader(Lcom/samsung/helphub/headers/HelpHeader;)I

    move-result v0

    .line 229
    .local v0, "clickedSectionIndexForNewLanguage":I
    :goto_0
    iput v0, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mExpandedGroup:I

    .line 230
    sput-object v1, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->savedLanguage:Ljava/lang/String;

    .line 233
    .end local v0    # "clickedSectionIndexForNewLanguage":I
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHeadersListView:Landroid/widget/ExpandableListView;

    iget v4, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mExpandedGroup:I

    invoke-virtual {v3, v4}, Landroid/widget/ExpandableListView;->setSelectedGroup(I)V

    .line 234
    iget-object v3, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHeadersListView:Landroid/widget/ExpandableListView;

    iget v4, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mExpandedGroup:I

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/widget/ExpandableListView;->expandGroup(IZ)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    :goto_1
    return-void

    .line 227
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "clickedSectionIndexForNewLanguage":I
    goto :goto_0

    .line 235
    .end local v0    # "clickedSectionIndexForNewLanguage":I
    :catch_0
    move-exception v2

    .line 236
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 318
    const-string v0, "section_list:expanded_group"

    iget v1, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mExpandedGroup:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 319
    const-string v0, "mySection"

    iget-object v1, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->clickedSectionHeader:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 320
    const-string v0, "lang"

    sget-object v1, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->savedLanguage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    invoke-super {p0, p1}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 322
    return-void
.end method
