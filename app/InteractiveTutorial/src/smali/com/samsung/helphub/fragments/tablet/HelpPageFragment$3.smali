.class Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$3;
.super Ljava/lang/Object;
.source "HelpPageFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->drawSummary(Lcom/samsung/helphub/headers/HelpHeader;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

.field final synthetic val$header:Lcom/samsung/helphub/headers/HelpHeader;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$3;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    iput-object p2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$3;->val$header:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 212
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$3;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$3;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$200(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$3;->val$header:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryId()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$3;->val$header:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v4}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryPs1Id()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$3;->val$header:Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v5}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryPs2Id()I

    move-result v5

    # invokes: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->makePSText(Landroid/content/Context;III)Ljava/lang/String;
    invoke-static {v1, v2, v3, v4, v5}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$300(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v0

    .line 214
    .local v0, "summary":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$3;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mSummaryTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$400(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 215
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$3;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$100(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/widget/ImageView;

    move-result-object v1

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 217
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$3;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mFragmentBreadCrumbsSeparator:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$100(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v6, v6, v6, v6}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 218
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment$3;->this$0:Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->mSummaryTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;->access$400(Lcom/samsung/helphub/fragments/tablet/HelpPageFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    return-void
.end method
