.class Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "HelpPageExpandableListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$1;,
        Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;,
        Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$ItemViewHolder;
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCursor:Landroid/database/Cursor;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mParentFragment:Landroid/app/Fragment;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/app/Fragment;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "parentFragment"    # Landroid/app/Fragment;

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 40
    const/4 v0, 0x1

    invoke-static {v0}, Landroid/app/FragmentManager;->enableDebugLogging(Z)V

    .line 42
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mActivity:Landroid/app/Activity;

    .line 43
    iput-object p2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mParentFragment:Landroid/app/Fragment;

    .line 44
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mActivity:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 45
    return-void
.end method

.method private makePSText(Landroid/content/Context;III)Ljava/lang/String;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textID"    # I
    .param p3, "TextPs1Id"    # I
    .param p4, "TextPs2Id"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 200
    const/4 v0, 0x0

    .line 203
    .local v0, "TextPsCount":I
    if-eq p3, v2, :cond_0

    .line 204
    add-int/lit8 v0, v0, 0x1

    .line 205
    :cond_0
    if-eq p4, v2, :cond_1

    .line 206
    add-int/lit8 v0, v0, 0x1

    .line 208
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 216
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 219
    .local v1, "result":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 210
    .end local v1    # "result":Ljava/lang/String;
    :pswitch_0
    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 211
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0

    .line 213
    .end local v1    # "result":Ljava/lang/String;
    :pswitch_1
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 214
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0

    .line 208
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 265
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 266
    .local v0, "old":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 267
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 269
    :cond_0
    return-void
.end method

.method public getChild(II)Ljava/lang/Object;
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 49
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 54
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 61
    const/4 v6, 0x0

    .line 63
    .local v6, "result":Landroid/view/View;
    if-eqz p4, :cond_2

    .line 64
    move-object/from16 v6, p4

    .line 65
    invoke-virtual {v6}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$ItemViewHolder;

    .line 73
    .local v5, "holder":Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$ItemViewHolder;
    iget-object v9, v5, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v9}, Landroid/view/ViewGroup;->getId()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->getGroupId(I)J

    move-result-wide v12

    cmp-long v9, v10, v12

    if-nez v9, :cond_0

    move-object v7, v6

    .line 110
    .end local v6    # "result":Landroid/view/View;
    .local v7, "result":Landroid/view/View;
    :goto_0
    return-object v7

    .line 76
    .end local v7    # "result":Landroid/view/View;
    .restart local v6    # "result":Landroid/view/View;
    :cond_0
    iget-object v9, v5, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->getGroupId(I)J

    move-result-wide v10

    long-to-int v10, v10

    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->setId(I)V

    .line 85
    :goto_1
    iget-object v9, v5, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v9}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 87
    iget-object v9, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mActivity:Landroid/app/Activity;

    iget-object v10, v5, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v10}, Landroid/view/ViewGroup;->getId()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-nez v9, :cond_1

    iget-object v9, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mParentFragment:Landroid/app/Fragment;

    if-eqz v9, :cond_1

    .line 89
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/helphub/headers/HelpHeader;

    .line 90
    .local v4, "header":Lcom/samsung/helphub/headers/HelpHeader;
    new-instance v2, Landroid/os/Bundle;

    const/4 v9, 0x1

    invoke-direct {v2, v9}, Landroid/os/Bundle;-><init>(I)V

    .line 91
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v9, "helpub:header"

    invoke-virtual {v2, v9, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 95
    if-eqz v4, :cond_1

    .line 96
    iget-object v9, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v2}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v3

    .line 99
    .local v3, "fragment":Landroid/app/Fragment;
    iget-object v9, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mParentFragment:Landroid/app/Fragment;

    invoke-virtual {v9}, Landroid/app/Fragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v8

    .line 102
    .local v8, "transaction":Landroid/app/FragmentTransaction;
    iget-object v9, v5, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v9}, Landroid/view/ViewGroup;->getId()I

    move-result v9

    invoke-virtual {v8, v9, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 103
    invoke-virtual {v8}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v3    # "fragment":Landroid/app/Fragment;
    .end local v4    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v8    # "transaction":Landroid/app/FragmentTransaction;
    :cond_1
    move-object v7, v6

    .line 110
    .end local v6    # "result":Landroid/view/View;
    .restart local v7    # "result":Landroid/view/View;
    goto :goto_0

    .line 78
    .end local v5    # "holder":Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$ItemViewHolder;
    .end local v7    # "result":Landroid/view/View;
    .restart local v6    # "result":Landroid/view/View;
    :cond_2
    iget-object v9, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v10, 0x7f040098

    const/4 v11, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v9, v10, v0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 79
    new-instance v5, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$ItemViewHolder;

    const/4 v9, 0x0

    invoke-direct {v5, v9}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$ItemViewHolder;-><init>(Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$1;)V

    .line 80
    .restart local v5    # "holder":Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$ItemViewHolder;
    invoke-virtual {v6, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 81
    const v9, 0x7f0d0088

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    iput-object v9, v5, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    .line 82
    iget-object v9, v5, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->getGroupId(I)J

    move-result-wide v10

    long-to-int v10, v10

    invoke-virtual {v9, v10}, Landroid/view/ViewGroup;->setId(I)V

    goto :goto_1
.end method

.method public getChildrenCount(I)I
    .locals 1
    .param p1, "groupPosition"    # I

    .prologue
    .line 115
    const/4 v0, 0x1

    return v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 120
    const/4 v0, 0x0

    .line 121
    .local v0, "result":Ljava/lang/Object;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 123
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    invoke-static {v1}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v0

    .line 125
    .end local v0    # "result":Ljava/lang/Object;
    :cond_0
    return-object v0
.end method

.method public getGroupCount()I
    .locals 2

    .prologue
    .line 130
    const/4 v0, 0x0

    .line 131
    .local v0, "result":I
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 132
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 134
    :cond_0
    return v0
.end method

.method public getGroupId(I)J
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 141
    add-int/lit8 v0, p1, 0x1

    int-to-long v0, v0

    return-wide v0
.end method

.method public getGroupPosition(Lcom/samsung/helphub/headers/HelpHeader;)I
    .locals 12
    .param p1, "section"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 286
    const/4 v10, 0x0

    .line 287
    .local v10, "result":I
    const/4 v9, 0x0

    .line 288
    .local v9, "owner_id":I
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v0

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v0, v2, :cond_2

    .line 289
    new-instance v6, Ljava/lang/StringBuilder;

    const/4 v0, 0x3

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 290
    .local v6, "builder":Ljava/lang/StringBuilder;
    const-string v0, "content://com.samsung.helphub.provider.search"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/header/item/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 293
    .local v1, "uri":Landroid/net/Uri;
    const/4 v8, 0x0

    .line 294
    .local v8, "ownerHeader":Lcom/samsung/helphub/headers/HelpHeader;
    const/4 v11, 0x0

    .line 296
    .local v11, "tempCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 297
    if-eqz v11, :cond_0

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    invoke-static {v11}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 301
    :cond_0
    if-eqz v11, :cond_1

    .line 302
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 306
    :cond_1
    if-eqz v8, :cond_2

    .line 307
    invoke-virtual {v8}, Lcom/samsung/helphub/headers/HelpHeader;->getRecordId()I

    move-result v9

    .line 310
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "builder":Ljava/lang/StringBuilder;
    .end local v8    # "ownerHeader":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v11    # "tempCursor":Landroid/database/Cursor;
    :cond_2
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 312
    :cond_3
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 313
    .local v7, "id":I
    if-ne v7, v9, :cond_6

    .line 318
    :goto_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt v10, v0, :cond_4

    .line 319
    const/4 v10, -0x1

    .line 321
    :cond_4
    return v10

    .line 301
    .end local v7    # "id":I
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v6    # "builder":Ljava/lang/StringBuilder;
    .restart local v8    # "ownerHeader":Lcom/samsung/helphub/headers/HelpHeader;
    .restart local v11    # "tempCursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    if-eqz v11, :cond_5

    .line 302
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 316
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "builder":Ljava/lang/StringBuilder;
    .end local v8    # "ownerHeader":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v11    # "tempCursor":Landroid/database/Cursor;
    .restart local v7    # "id":I
    :cond_6
    add-int/lit8 v10, v10, 0x1

    .line 317
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 147
    const/4 v3, 0x0

    .line 148
    .local v3, "result":Landroid/view/View;
    iget-object v5, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v5, :cond_1

    .line 149
    iget-object v5, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v5, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 150
    const/4 v1, 0x0

    .line 152
    .local v1, "holder":Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;
    if-nez p3, :cond_2

    .line 153
    new-instance v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;

    .end local v1    # "holder":Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;
    const/4 v5, 0x0

    invoke-direct {v1, v5}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;-><init>(Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$1;)V

    .line 154
    .restart local v1    # "holder":Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;
    iget-object v5, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f0400ba

    invoke-virtual {v5, v6, p4, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 156
    const v5, 0x7f0d008a

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;->Title:Landroid/widget/TextView;

    .line 157
    const v5, 0x7f0d0086

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;->FoldIndicator:Landroid/widget/ImageView;

    .line 159
    const v5, 0x7f0d0112

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;->Diveder:Landroid/widget/ImageView;

    .line 160
    invoke-virtual {v3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 166
    :goto_0
    if-eqz p2, :cond_3

    .line 167
    iget-object v5, v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;->FoldIndicator:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 168
    iget-object v5, v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;->FoldIndicator:Landroid/widget/ImageView;

    const v6, 0x7f020084

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 170
    iget-object v5, v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;->Title:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09003d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 171
    iget-object v5, v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;->Diveder:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 179
    :goto_1
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    .line 180
    .local v0, "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v2

    .line 181
    .local v2, "id":I
    const v5, 0x7f0d01a8

    if-eq v2, v5, :cond_0

    const v5, 0x7f0d01a9

    if-eq v2, v5, :cond_0

    const v5, 0x7f0d01ab

    if-eq v2, v5, :cond_0

    const v5, 0x7f0d01ac

    if-ne v2, v5, :cond_4

    .line 185
    :cond_0
    iget-object v5, v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;->FoldIndicator:Landroid/widget/ImageView;

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 189
    :goto_2
    iget-object v5, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v7, 0x2

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    iget-object v7, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v8, 0x3

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iget-object v8, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v9, 0x4

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-direct {p0, v5, v6, v7, v8}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->makePSText(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v4

    .line 193
    .local v4, "title":Ljava/lang/String;
    iget-object v5, v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;->Title:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    .end local v0    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v1    # "holder":Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;
    .end local v2    # "id":I
    .end local v4    # "title":Ljava/lang/String;
    :cond_1
    return-object v3

    .line 162
    .restart local v1    # "holder":Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;
    :cond_2
    move-object v3, p3

    .line 163
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "holder":Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;
    check-cast v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;

    .restart local v1    # "holder":Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;
    goto :goto_0

    .line 173
    :cond_3
    iget-object v5, v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;->FoldIndicator:Landroid/widget/ImageView;

    const v6, 0x7f020083

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 175
    iget-object v5, v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;->Title:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09003b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 176
    iget-object v5, v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;->Diveder:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 187
    .restart local v0    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .restart local v2    # "id":I
    :cond_4
    iget-object v5, v1, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter$GroupViewHolder;->FoldIndicator:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 229
    const/4 v0, 0x1

    return v0
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 243
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    if-ne p1, v1, :cond_0

    .line 244
    const/4 v0, 0x0

    .line 255
    :goto_0
    return-object v0

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    .line 247
    .local v0, "oldCursor":Landroid/database/Cursor;
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->mCursor:Landroid/database/Cursor;

    .line 248
    if-eqz p1, :cond_1

    .line 250
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 253
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->notifyDataSetInvalidated()V

    goto :goto_0
.end method

.method public toggle(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPageExpandableListAdapter;->notifyDataSetChanged()V

    .line 273
    return-void
.end method
