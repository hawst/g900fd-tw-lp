.class Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$Collapser;
.super Ljava/lang/Object;
.source "SectionHeaderExpandableList.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Collapser"
.end annotation


# instance fields
.field private mOpenedPosition:I

.field final synthetic this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;


# direct methods
.method public constructor <init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;I)V
    .locals 1
    .param p2, "openedPosition"    # I

    .prologue
    .line 211
    iput-object p1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$Collapser;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$Collapser;->mOpenedPosition:I

    .line 212
    iput p2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$Collapser;->mOpenedPosition:I

    .line 213
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 217
    iget-object v2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$Collapser;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    # getter for: Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    invoke-static {v2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->access$000(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroupCount()I

    move-result v0

    .line 218
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 219
    iget v2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$Collapser;->mOpenedPosition:I

    if-eq v1, v2, :cond_0

    .line 220
    iget-object v2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$Collapser;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    # getter for: Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeadersListView:Landroid/widget/ExpandableListView;
    invoke-static {v2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->access$200(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)Landroid/widget/ExpandableListView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 218
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 223
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$Collapser;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    # getter for: Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mHeadersListView:Landroid/widget/ExpandableListView;
    invoke-static {v2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->access$200(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)Landroid/widget/ExpandableListView;

    move-result-object v2

    iget v3, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$Collapser;->mOpenedPosition:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/widget/ExpandableListView;->smoothScrollToPositionFromTop(II)V

    .line 224
    return-void
.end method
