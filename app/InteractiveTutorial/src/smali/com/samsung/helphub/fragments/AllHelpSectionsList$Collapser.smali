.class Lcom/samsung/helphub/fragments/AllHelpSectionsList$Collapser;
.super Ljava/lang/Object;
.source "AllHelpSectionsList.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/AllHelpSectionsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Collapser"
.end annotation


# instance fields
.field private mOpenedPosition:I

.field final synthetic this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;


# direct methods
.method public constructor <init>(Lcom/samsung/helphub/fragments/AllHelpSectionsList;I)V
    .locals 1
    .param p2, "openedPosition"    # I

    .prologue
    .line 299
    iput-object p1, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$Collapser;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$Collapser;->mOpenedPosition:I

    .line 300
    iput p2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$Collapser;->mOpenedPosition:I

    .line 301
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 305
    iget-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$Collapser;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    # getter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    invoke-static {v2}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$000(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroupCount()I

    move-result v0

    .line 306
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 307
    iget v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$Collapser;->mOpenedPosition:I

    if-eq v1, v2, :cond_0

    .line 308
    iget-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$Collapser;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    # getter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHeadersListView:Landroid/widget/ExpandableListView;
    invoke-static {v2}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$100(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)Landroid/widget/ExpandableListView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 306
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 311
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$Collapser;->this$0:Lcom/samsung/helphub/fragments/AllHelpSectionsList;

    # getter for: Lcom/samsung/helphub/fragments/AllHelpSectionsList;->mHeadersListView:Landroid/widget/ExpandableListView;
    invoke-static {v2}, Lcom/samsung/helphub/fragments/AllHelpSectionsList;->access$100(Lcom/samsung/helphub/fragments/AllHelpSectionsList;)Landroid/widget/ExpandableListView;

    move-result-object v2

    iget v3, p0, Lcom/samsung/helphub/fragments/AllHelpSectionsList$Collapser;->mOpenedPosition:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/widget/ExpandableListView;->smoothScrollToPositionFromTop(II)V

    .line 312
    return-void
.end method
