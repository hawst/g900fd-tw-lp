.class Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;
.super Landroid/widget/BaseAdapter;
.source "SectionHeaderExpandableListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChildAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentXmlId:I

.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;


# direct methods
.method public constructor <init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 375
    iput-object p1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 370
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mCurrentXmlId:I

    .line 371
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mCurrentHeaders:Ljava/util/List;

    .line 376
    iput-object p2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mContext:Landroid/content/Context;

    .line 377
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 378
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 398
    const/4 v0, 0x0

    .line 399
    .local v0, "result":I
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mCurrentHeaders:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 400
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mCurrentHeaders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 402
    :cond_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 407
    const/4 v0, 0x0

    .line 408
    .local v0, "result":Ljava/lang/Object;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mCurrentHeaders:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 409
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mCurrentHeaders:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 411
    .end local v0    # "result":Ljava/lang/Object;
    :cond_0
    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 416
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 421
    const/4 v6, 0x0

    .line 422
    .local v6, "result":Landroid/view/View;
    const/4 v3, 0x0

    .line 423
    .local v3, "holder":Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    .line 425
    .local v2, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v7

    const v8, 0x7f0d010a

    if-eq v7, v8, :cond_4

    .line 426
    :cond_0
    new-instance v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;

    .end local v3    # "holder":Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;
    const/4 v7, 0x0

    invoke-direct {v3, p0, v7}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;-><init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$1;)V

    .line 427
    .restart local v3    # "holder":Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;
    iget-object v7, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v8, 0x7f0400be

    const/4 v9, 0x0

    invoke-virtual {v7, v8, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 429
    const v7, 0x7f0d010c

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;->Title:Landroid/widget/TextView;

    .line 430
    const v7, 0x7f0d0377

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;->DeviderTop:Landroid/widget/ImageView;

    .line 432
    const v7, 0x7f0d010b

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;->Devider:Landroid/widget/ImageView;

    .line 434
    invoke-virtual {v6, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 439
    :goto_0
    if-eqz v2, :cond_1

    .line 440
    iget-object v7, v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;->Title:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 443
    iget-object v7, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    iget-object v8, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v9

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitlePs1Id()I

    move-result v10

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitlePs2Id()I

    move-result v11

    # invokes: Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->makePSText(Landroid/content/Context;III)Ljava/lang/String;
    invoke-static {v7, v8, v9, v10, v11}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->access$300(Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v0

    .line 444
    .local v0, "Title":Ljava/lang/String;
    iget-object v7, v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;->Title:Landroid/widget/TextView;

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 448
    .end local v0    # "Title":Ljava/lang/String;
    :cond_1
    iget-object v7, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 449
    .local v5, "res":Landroid/content/res/Resources;
    iget-object v7, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/samsung/helphub/HelpHubActivityBase;

    invoke-virtual {v7}, Lcom/samsung/helphub/HelpHubActivityBase;->isTwoPane()Z

    move-result v7

    if-nez v7, :cond_3

    .line 450
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 451
    if-nez p1, :cond_5

    .line 455
    iget-object v7, v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;->DeviderTop:Landroid/widget/ImageView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 465
    :goto_1
    const v7, 0x7f0d0006

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 466
    .local v4, "ll":Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->getCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ne p1, v7, :cond_6

    .line 468
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v7

    if-nez v7, :cond_2

    .line 469
    const v7, 0x7f02030d

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 471
    :cond_2
    iget-object v7, v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;->Devider:Landroid/widget/ImageView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 516
    .end local v4    # "ll":Landroid/widget/LinearLayout;
    :cond_3
    :goto_2
    return-object v6

    .line 436
    .end local v5    # "res":Landroid/content/res/Resources;
    :cond_4
    move-object v6, p2

    .line 437
    invoke-virtual {v6}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "holder":Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;
    check-cast v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;

    .restart local v3    # "holder":Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;
    goto :goto_0

    .line 460
    .restart local v5    # "res":Landroid/content/res/Resources;
    :cond_5
    iget-object v7, v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;->DeviderTop:Landroid/widget/ImageView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 473
    .restart local v4    # "ll":Landroid/widget/LinearLayout;
    :cond_6
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v7

    if-nez v7, :cond_7

    .line 474
    const v7, 0x7f02030e

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 476
    :cond_7
    iget-object v7, v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;->Devider:Landroid/widget/ImageView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 479
    .end local v4    # "ll":Landroid/widget/LinearLayout;
    :cond_8
    iget-object v7, v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;->DeviderTop:Landroid/widget/ImageView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 481
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v7

    if-nez v7, :cond_9

    .line 482
    const v7, 0x7f0d0006

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 499
    .restart local v4    # "ll":Landroid/widget/LinearLayout;
    const v7, 0x7f02030f

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_2

    .line 501
    .end local v4    # "ll":Landroid/widget/LinearLayout;
    :cond_9
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->getCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ne p1, v7, :cond_b

    .line 502
    iget-object v7, v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;->Devider:Landroid/widget/ImageView;

    const v8, 0x7f0203b9

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 504
    iget-object v7, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 505
    .local v1, "currentActivityName":Ljava/lang/String;
    if-eqz v1, :cond_a

    const-string v7, "ExternalHelpActivity"

    invoke-virtual {v1, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    invoke-virtual {v7}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroupCount()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_a

    .line 506
    iget-object v7, v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;->Devider:Landroid/widget/ImageView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 508
    :cond_a
    iget-object v7, v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;->Devider:Landroid/widget/ImageView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 511
    .end local v1    # "currentActivityName":Ljava/lang/String;
    :cond_b
    iget-object v7, v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter$ChildViewHolder;->Devider:Landroid/widget/ImageView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method

.method public setHeaderXml(I)V
    .locals 3
    .param p1, "xmlId"    # I

    .prologue
    .line 381
    iget v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mCurrentXmlId:I

    if-ne v0, p1, :cond_0

    .line 394
    :goto_0
    return-void

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mCurrentHeaders:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 385
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mCurrentHeaders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 387
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mCurrentHeaders:Ljava/util/List;

    .line 389
    if-lez p1, :cond_2

    .line 390
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mCurrentHeaders:Ljava/util/List;

    invoke-static {v0, p1, v1}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->loadHeadersFromResource(Landroid/content/Context;ILjava/util/List;)V

    .line 392
    :cond_2
    iput p1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mCurrentXmlId:I

    .line 393
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    iget-object v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mCurrentHeaders:Ljava/util/List;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->mContext:Landroid/content/Context;

    # invokes: Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->filterItemList(Ljava/util/List;Landroid/content/Context;)Ljava/util/List;
    invoke-static {v0, v1, v2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->access$100(Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;Ljava/util/List;Landroid/content/Context;)Ljava/util/List;

    goto :goto_0
.end method
