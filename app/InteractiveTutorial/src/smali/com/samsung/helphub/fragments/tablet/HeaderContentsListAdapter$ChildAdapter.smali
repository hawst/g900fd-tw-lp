.class Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;
.super Landroid/widget/BaseAdapter;
.source "HeaderContentsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChildAdapter"
.end annotation


# instance fields
.field private mChildPositionsInDB:[I

.field private mCurrentOwnerID:I

.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;


# direct methods
.method public constructor <init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 356
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 354
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mCurrentOwnerID:I

    .line 357
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 358
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 389
    const/4 v0, 0x0

    .line 390
    .local v0, "result":I
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mChildPositionsInDB:[I

    if-eqz v1, :cond_0

    .line 391
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mChildPositionsInDB:[I

    array-length v0, v1

    .line 393
    :cond_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 398
    const/4 v0, 0x0

    .line 399
    .local v0, "result":Lcom/samsung/helphub/headers/HelpHeader;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mChildPositionsInDB:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 400
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mChildPositionsInDB:[I

    aget v2, v2, p1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 401
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v0

    .line 403
    :cond_0
    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 408
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->getItemIntID(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemIntID(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 412
    const/4 v0, -0x1

    .line 413
    .local v0, "result":I
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mChildPositionsInDB:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mChildPositionsInDB:[I

    array-length v1, v1

    if-le v1, p1, :cond_0

    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 415
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mChildPositionsInDB:[I

    aget v2, v2, p1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 416
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;
    invoke-static {v1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 418
    :cond_0
    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 423
    const/4 v1, 0x0

    .line 424
    .local v1, "result":Landroid/view/View;
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mChildPositionsInDB:[I

    if-eqz v2, :cond_0

    .line 425
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;
    invoke-static {v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mChildPositionsInDB:[I

    aget v3, v3, p1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 428
    move-object v1, p2

    .line 430
    if-nez v1, :cond_1

    .line 431
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f0400be

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 433
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildViewHolder;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildViewHolder;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$1;)V

    .line 434
    .local v0, "holder":Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildViewHolder;
    const v2, 0x7f0d010c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildViewHolder;->title:Landroid/widget/TextView;

    .line 435
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 440
    :goto_0
    iget-object v2, v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildViewHolder;->title:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 442
    .end local v0    # "holder":Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildViewHolder;
    :cond_0
    return-object v1

    .line 437
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildViewHolder;

    .restart local v0    # "holder":Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildViewHolder;
    goto :goto_0
.end method

.method public setOwnerID(II)V
    .locals 5
    .param p1, "ownerID"    # I
    .param p2, "childCount"    # I

    .prologue
    .line 368
    iget v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mCurrentOwnerID:I

    if-eq v3, p1, :cond_2

    .line 369
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mChildPositionsInDB:[I

    .line 370
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 371
    new-array v3, p2, [I

    iput-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mChildPositionsInDB:[I

    .line 372
    const/4 v2, 0x0

    .line 373
    .local v2, "pos":I
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 374
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 375
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;

    move-result-object v3

    const/16 v4, 0xb

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 376
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mChildPositionsInDB:[I

    aput v1, v3, v2

    .line 377
    add-int/lit8 v2, v2, 0x1

    .line 380
    :cond_0
    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->mChildCursor:Landroid/database/Cursor;
    invoke-static {v3}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->access$100(Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;)Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    .line 374
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 383
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "pos":I
    :cond_1
    iput p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter$ChildAdapter;->mCurrentOwnerID:I

    .line 385
    :cond_2
    return-void
.end method
