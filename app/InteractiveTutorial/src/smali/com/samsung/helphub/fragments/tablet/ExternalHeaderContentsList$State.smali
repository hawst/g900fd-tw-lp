.class final enum Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;
.super Ljava/lang/Enum;
.source "ExternalHeaderContentsList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

.field public static final enum LOADING:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

.field public static final enum NOT_INITIALIZED:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

.field public static final enum STANDBY:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    const-string v1, "NOT_INITIALIZED"

    invoke-direct {v0, v1, v2}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;->NOT_INITIALIZED:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    .line 23
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;->LOADING:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    .line 24
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    const-string v1, "STANDBY"

    invoke-direct {v0, v1, v4}, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;->STANDBY:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    .line 21
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    sget-object v1, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;->NOT_INITIALIZED:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;->LOADING:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;->STANDBY:Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;->$VALUES:[Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    return-object v0
.end method

.method public static values()[Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;->$VALUES:[Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    invoke-virtual {v0}, [Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/helphub/fragments/tablet/ExternalHeaderContentsList$State;

    return-object v0
.end method
