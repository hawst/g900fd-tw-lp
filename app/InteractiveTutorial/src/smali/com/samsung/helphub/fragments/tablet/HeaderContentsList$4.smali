.class Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;
.super Ljava/lang/Object;
.source "HeaderContentsList.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 9
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 258
    iget-object v7, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    invoke-virtual {v7}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    .line 259
    .local v1, "focusView":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 260
    iget-object v7, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    invoke-virtual {v7}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const-string v8, "input_method"

    invoke-virtual {v7, v8}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/inputmethod/InputMethodManager;

    .line 264
    .local v4, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v4, :cond_0

    iget-object v7, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    iget-object v7, v7, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v7, :cond_0

    .line 265
    iget-object v7, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    iget-object v7, v7, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v7}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v7

    invoke-virtual {v4, v7, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 268
    .end local v4    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    iget-object v7, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    iget-object v7, v7, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v7, p3}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    .line 269
    .local v2, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-nez v2, :cond_1

    .line 303
    :goto_0
    return v5

    .line 272
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v7

    const v8, 0x7f0d00a3

    if-ne v7, v8, :cond_2

    .line 274
    iget-object v7, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    invoke-virtual {v7, v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->openOnlineHelp(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 275
    invoke-virtual {p1, v5}, Landroid/widget/ExpandableListView;->playSoundEffect(I)V

    move v5, v6

    .line 276
    goto :goto_0

    .line 279
    :cond_2
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v7

    invoke-static {v7}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourCategory(I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 280
    iget-object v7, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    iget-object v7, v7, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v7, p3, v5}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->setActiveItem(II)V

    .line 281
    iget-object v7, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    invoke-virtual {v7, v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->openGuideTour(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 283
    :cond_3
    iget-object v7, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    invoke-virtual {v7}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/samsung/helphub/HelpHubCommon;->isResourceAvailable(Lcom/samsung/helphub/headers/HelpHeader;Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 285
    iget-object v7, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    iget-object v7, v7, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;

    invoke-virtual {v7}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsListAdapter;->getGroupCount()I

    move-result v0

    .line 286
    .local v0, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v0, :cond_7

    .line 287
    if-ne v3, p3, :cond_5

    .line 288
    invoke-virtual {p1, v3}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 289
    invoke-virtual {p1, v3}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 286
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 291
    :cond_4
    invoke-virtual {p1, v3}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    goto :goto_2

    .line 294
    :cond_5
    invoke-virtual {p1, v3}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    goto :goto_2

    .line 298
    .end local v0    # "count":I
    .end local v3    # "i":I
    :cond_6
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getPluginAppId()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$4;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    invoke-virtual {v8}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/helphub/HelpHubCommon;->requestDownloadingResource(Ljava/lang/String;Landroid/content/Context;)V

    .line 301
    :cond_7
    invoke-virtual {p1, v5}, Landroid/widget/ExpandableListView;->playSoundEffect(I)V

    move v5, v6

    .line 303
    goto :goto_0
.end method
