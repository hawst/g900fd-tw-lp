.class Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;
.super Landroid/widget/BaseAdapter;
.source "SearchListCursorsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter$1;,
        Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter$Holder;
    }
.end annotation


# instance fields
.field private mActiveItemPosition:I

.field private mContext:Landroid/content/Context;

.field private mExtendedTitles:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 29
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mActiveItemPosition:I

    .line 36
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mContext:Landroid/content/Context;

    .line 37
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mHeaders:Ljava/util/List;

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mExtendedTitles:Ljava/util/HashMap;

    .line 40
    return-void
.end method


# virtual methods
.method public addHeaderItem(Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 1
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->notifyDataSetChanged()V

    .line 80
    return-void
.end method

.method public addItems(Landroid/database/Cursor;Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "owner"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    const/16 v6, 0x8

    .line 50
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 51
    const/4 v0, 0x0

    .line 52
    .local v0, "extendedTitle":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 53
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 58
    :cond_0
    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    sget-object v3, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_3

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const v3, 0x7f0d00a3

    if-eq v2, v3, :cond_3

    .line 62
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourCategory(I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 72
    :cond_1
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->notifyDataSetChanged()V

    .line 75
    .end local v0    # "extendedTitle":Ljava/lang/String;
    :cond_2
    return-void

    .line 67
    .restart local v0    # "extendedTitle":Ljava/lang/String;
    :cond_3
    invoke-static {p1}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v1

    .line 68
    .local v1, "header":Lcom/samsung/helphub/headers/HelpHeader;
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    if-eqz v0, :cond_1

    .line 70
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mExtendedTitles:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 94
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 99
    const/4 v2, 0x0

    .line 101
    .local v2, "result":Landroid/view/View;
    if-eqz p2, :cond_0

    .line 102
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter$Holder;

    .line 103
    .local v1, "holder":Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter$Holder;
    move-object v2, p2

    .line 110
    :goto_0
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    .line 112
    .local v0, "header":Lcom/samsung/helphub/headers/HelpHeader;
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mExtendedTitles:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 113
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mExtendedTitles:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 117
    .local v3, "title":Ljava/lang/String;
    :goto_1
    iget-object v4, v1, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter$Holder;->Title:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget v4, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mActiveItemPosition:I

    if-ne v4, p1, :cond_2

    const/4 v4, 0x1

    :goto_2
    invoke-virtual {v2, v4}, Landroid/view/View;->setActivated(Z)V

    .line 119
    return-object v2

    .line 105
    .end local v0    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v1    # "holder":Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter$Holder;
    .end local v3    # "title":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f0400be

    invoke-virtual {v4, v6, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 106
    new-instance v1, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter$Holder;

    const/4 v4, 0x0

    invoke-direct {v1, v4}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter$Holder;-><init>(Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter$1;)V

    .line 107
    .restart local v1    # "holder":Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter$Holder;
    const v4, 0x7f0d010c

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter$Holder;->Title:Landroid/widget/TextView;

    .line 108
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    .line 115
    .restart local v0    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_1
    iget-object v4, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "title":Ljava/lang/String;
    goto :goto_1

    :cond_2
    move v4, v5

    .line 118
    goto :goto_2
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 132
    return-void
.end method

.method public setActiveItem(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 126
    iput p1, p0, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->mActiveItemPosition:I

    .line 127
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/SearchListCursorsAdapter;->notifyDataSetChanged()V

    .line 128
    return-void
.end method
