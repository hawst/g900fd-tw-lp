.class Lcom/samsung/helphub/fragments/HelpItemsList$MoveExpandingItemRunnable;
.super Ljava/lang/Object;
.source "HelpItemsList.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/HelpItemsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MoveExpandingItemRunnable"
.end annotation


# instance fields
.field private mMovingListView:Landroid/widget/ListView;

.field private mPosition:I


# direct methods
.method constructor <init>(Landroid/widget/ListView;I)V
    .locals 0
    .param p1, "listView"    # Landroid/widget/ListView;
    .param p2, "position"    # I

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p1, p0, Lcom/samsung/helphub/fragments/HelpItemsList$MoveExpandingItemRunnable;->mMovingListView:Landroid/widget/ListView;

    .line 114
    iput p2, p0, Lcom/samsung/helphub/fragments/HelpItemsList$MoveExpandingItemRunnable;->mPosition:I

    .line 115
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpItemsList$MoveExpandingItemRunnable;->mMovingListView:Landroid/widget/ListView;

    iget v1, p0, Lcom/samsung/helphub/fragments/HelpItemsList$MoveExpandingItemRunnable;->mPosition:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 120
    return-void
.end method
