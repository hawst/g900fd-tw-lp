.class public Lcom/samsung/helphub/fragments/HelpHeaderList;
.super Landroid/app/Fragment;
.source "HelpHeaderList.java"


# instance fields
.field headerClickListener:Landroid/view/View$OnClickListener;

.field headerLayout:Landroid/widget/LinearLayout;

.field protected mAdapter:Lcom/samsung/helphub/headers/HelpHeaderAdapter;

.field private mHeaderXmlID:I

.field private mHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation
.end field

.field protected mHeadersListView:Landroid/widget/ListView;

.field protected mTitleResId:I

.field result:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 45
    const v0, 0x7f0a001e

    iput v0, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->mTitleResId:I

    .line 203
    new-instance v0, Lcom/samsung/helphub/fragments/HelpHeaderList$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/HelpHeaderList$1;-><init>(Lcom/samsung/helphub/fragments/HelpHeaderList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->headerClickListener:Landroid/view/View$OnClickListener;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/fragments/HelpHeaderList;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/HelpHeaderList;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->mHeaders:Ljava/util/List;

    return-object v0
.end method

.method private findNewFeatures()Z
    .locals 4

    .prologue
    .line 280
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "content://com.samsung.helphub.provider.search"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 281
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v2, "/header/category/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "new_feature"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/fragments/HelpHeaderList;->queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v1

    .line 284
    .local v1, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-nez v1, :cond_0

    .line 285
    const/4 v2, 0x0

    .line 288
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private getTotalHeightofHeaderLayout()I
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 249
    const/4 v2, 0x0

    .line 250
    .local v2, "mAdditionalHeight":I
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    .line 251
    .local v5, "metrics":Landroid/util/DisplayMetrics;
    iget v7, v5, Landroid/util/DisplayMetrics;->scaledDensity:F

    iget v8, v5, Landroid/util/DisplayMetrics;->density:F

    sub-float v0, v7, v8

    .line 252
    .local v0, "densityDelta":F
    const/4 v7, 0x0

    cmpl-float v7, v0, v7

    if-lez v7, :cond_0

    .line 253
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0041

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 256
    :cond_0
    const/4 v3, 0x0

    .line 257
    .local v3, "mHeight":I
    iget-object v6, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->headerLayout:Landroid/widget/LinearLayout;

    .line 258
    .local v6, "parent":Landroid/view/ViewGroup;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v7

    if-ge v1, v7, :cond_4

    .line 259
    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 260
    .local v4, "mView":Landroid/view/View;
    if-eqz v4, :cond_1

    .line 261
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v4, v7, v8}, Landroid/view/View;->measure(II)V

    .line 264
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 265
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v3, v7

    .line 272
    :cond_1
    :goto_1
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 273
    add-int/2addr v3, v2

    .line 258
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 267
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0032

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    add-int/2addr v3, v7

    goto :goto_1

    .line 276
    .end local v4    # "mView":Landroid/view/View;
    :cond_4
    return v3
.end method

.method private setCategoryLayout()V
    .locals 7

    .prologue
    .line 191
    new-instance v3, Lcom/samsung/helphub/headers/HelpHeaderLayout;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->mHeaders:Ljava/util/List;

    invoke-direct {v3, v5, v6}, Lcom/samsung/helphub/headers/HelpHeaderLayout;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 192
    .local v3, "mHelpHeaderLayout":Lcom/samsung/helphub/headers/HelpHeaderLayout;
    const/4 v4, 0x0

    .local v4, "position":I
    :goto_0
    iget-object v5, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->mHeaders:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 193
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v1, v5, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 194
    .local v1, "lparams":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 195
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {v3, v4}, Lcom/samsung/helphub/headers/HelpHeaderLayout;->getAppropiateResource(I)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 196
    .local v2, "mHeaderView":Landroid/view/View;
    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 197
    invoke-virtual {v3, v2, v4}, Lcom/samsung/helphub/headers/HelpHeaderLayout;->setHolderTag(Landroid/view/View;I)V

    .line 198
    iget-object v5, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->headerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 199
    iget-object v5, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->headerClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 201
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v1    # "lparams":Landroid/view/ViewGroup$LayoutParams;
    .end local v2    # "mHeaderView":Landroid/view/View;
    :cond_0
    return-void
.end method


# virtual methods
.method protected getHeaderXmlID()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->mHeaderXmlID:I

    return v0
.end method

.method protected getHeaders()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 176
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 177
    .local v7, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getHeaderXmlID()I

    move-result v9

    invoke-static {v8, v9, v7}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->loadHeadersFromResource(Landroid/content/Context;ILjava/util/List;)V

    .line 178
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    .line 179
    .local v1, "count":I
    add-int/lit8 v3, v1, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_2

    .line 180
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    .line 181
    .local v2, "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v8}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->getHiddenCategoryHeaderId(Landroid/content/Context;)[I

    move-result-object v0

    .local v0, "arr$":[I
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v6, :cond_1

    aget v5, v0, v4

    .line 182
    .local v5, "item":I
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v8

    if-ne v8, v5, :cond_0

    .line 183
    invoke-interface {v7, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 181
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 179
    .end local v5    # "item":I
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 187
    .end local v0    # "arr$":[I
    .end local v2    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    :cond_2
    return-object v7
.end method

.method protected getLayoutID()I
    .locals 1

    .prologue
    .line 166
    const v0, 0x7f0400b6

    return v0
.end method

.method protected launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 6
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 141
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 142
    invoke-static {p1, v0}, Lcom/samsung/helphub/HelpHubCommon;->isResourceAvailable(Lcom/samsung/helphub/headers/HelpHeader;Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 143
    new-instance v1, Landroid/os/Bundle;

    const/4 v4, 0x1

    invoke-direct {v1, v4}, Landroid/os/Bundle;-><init>(I)V

    .line 144
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v4, "helpub:header"

    invoke-virtual {v1, v4, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 146
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 147
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4, v1}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v2

    .line 149
    .local v2, "fragment":Landroid/app/Fragment;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 152
    .local v3, "transaction":Landroid/app/FragmentTransaction;
    const/16 v4, 0x1001

    invoke-virtual {v3, v4}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 153
    const v4, 0x7f0d0004

    invoke-virtual {v3, v4, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 154
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 155
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 162
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "fragment":Landroid/app/Fragment;
    .end local v3    # "transaction":Landroid/app/FragmentTransaction;
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/helphub/HelpHubCommon;->requestDownloadingResource(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected launchToSecondDepthActivity(Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 6
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    const/4 v5, 0x1

    .line 223
    if-eqz p1, :cond_0

    .line 224
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/samsung/helphub/HelpHubCommon;->isResourceAvailable(Lcom/samsung/helphub/headers/HelpHeader;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 225
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 226
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v1, 0x0

    .line 228
    .local v1, "fragmentName":Ljava/lang/String;
    const-string v3, "helpub:header"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 229
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v1

    .line 231
    if-eqz v1, :cond_1

    .line 233
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/samsung/helphub/HelpHubSecondDepthActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 234
    .local v2, "intent":Landroid/content/Intent;
    const-string v3, "fragment_name"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 236
    invoke-virtual {p0, v2}, Lcom/samsung/helphub/fragments/HelpHeaderList;->startActivity(Landroid/content/Intent;)V

    .line 246
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "fragmentName":Ljava/lang/String;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 239
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v1    # "fragmentName":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "This function will not be provided before PRA"

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 243
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "fragmentName":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/helphub/HelpHubCommon;->requestDownloadingResource(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 61
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, -0x1

    .line 66
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getLayoutID()I

    move-result v3

    .line 68
    .local v3, "layout":I
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "easy_mode_switch"

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-nez v7, :cond_5

    .line 70
    const v7, 0x7f060001

    iput v7, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->mHeaderXmlID:I

    .line 75
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 76
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    const-string v7, "helpub:header"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 77
    const-string v7, "helpub:header"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    .line 78
    .local v2, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v2, :cond_1

    .line 79
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getSubHeadersArrayId()I

    move-result v7

    if-eq v7, v10, :cond_0

    .line 81
    const v3, 0x7f0400c0

    .line 82
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getSubHeadersArrayId()I

    move-result v7

    iput v7, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->mHeaderXmlID:I

    .line 86
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getActivityTitleId()I

    move-result v7

    if-eq v7, v10, :cond_1

    .line 87
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getActivityTitleId()I

    move-result v7

    iput v7, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->mTitleResId:I

    .line 92
    .end local v2    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_1
    const/4 v7, 0x0

    invoke-virtual {p1, v3, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->result:Landroid/view/View;

    .line 93
    iget-object v7, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->result:Landroid/view/View;

    const v8, 0x7f0d009b

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 94
    .local v4, "mCategory_new_features":Landroid/view/View;
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->findNewFeatures()Z

    move-result v7

    if-nez v7, :cond_2

    .line 95
    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 97
    :cond_2
    if-eqz v4, :cond_3

    .line 98
    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    .line 102
    :cond_3
    iget-object v7, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->result:Landroid/view/View;

    const v8, 0x7f0d0005

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->headerLayout:Landroid/widget/LinearLayout;

    .line 103
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getHeaders()Ljava/util/List;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->mHeaders:Ljava/util/List;

    .line 104
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->setCategoryLayout()V

    .line 114
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    .line 115
    .local v5, "metrics":Landroid/util/DisplayMetrics;
    iget v7, v5, Landroid/util/DisplayMetrics;->scaledDensity:F

    iget v8, v5, Landroid/util/DisplayMetrics;->density:F

    sub-float v1, v7, v8

    .line 116
    .local v1, "densityDelta":F
    iget-object v7, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->headerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    .line 118
    .local v6, "param":Landroid/view/ViewGroup$LayoutParams;
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 119
    const/4 v7, 0x0

    cmpl-float v7, v1, v7

    if-lez v7, :cond_4

    .line 120
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0034

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    invoke-direct {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getTotalHeightofHeaderLayout()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 123
    :cond_4
    iget-object v7, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->headerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v6}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    iget-object v7, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->result:Landroid/view/View;

    return-object v7

    .line 72
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "densityDelta":F
    .end local v4    # "mCategory_new_features":Landroid/view/View;
    .end local v5    # "metrics":Landroid/util/DisplayMetrics;
    .end local v6    # "param":Landroid/view/ViewGroup$LayoutParams;
    :cond_5
    const/high16 v7, 0x7f060000

    iput v7, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->mHeaderXmlID:I

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 130
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 131
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget v1, p0, Lcom/samsung/helphub/fragments/HelpHeaderList;->mTitleResId:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 132
    return-void
.end method

.method protected queryHeaderById(Landroid/net/Uri;)Lcom/samsung/helphub/headers/HelpHeader;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 298
    const/4 v6, 0x0

    .line 299
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 301
    .local v7, "result":Lcom/samsung/helphub/headers/HelpHeader;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/HelpHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 303
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    invoke-static {v6}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 307
    :cond_0
    if-eqz v6, :cond_1

    .line 308
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 311
    :cond_1
    return-object v7

    .line 307
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 308
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method
