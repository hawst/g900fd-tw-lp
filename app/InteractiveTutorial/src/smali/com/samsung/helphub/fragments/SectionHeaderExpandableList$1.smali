.class Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$1;
.super Ljava/lang/Object;
.source "SectionHeaderExpandableList.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnChildClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

.field final synthetic val$isExternal:Z


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;Z)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$1;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    iput-boolean p2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$1;->val$isExternal:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 5
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I
    .param p5, "id"    # J

    .prologue
    const/4 v4, 0x1

    .line 120
    iget-object v2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$1;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    # getter for: Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    invoke-static {v2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->access$000(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    .line 122
    .local v0, "launchHeader":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v0, :cond_1

    .line 123
    iget-object v2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$1;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/helphub/HelpHubCommon;->isResourceAvailable(Lcom/samsung/helphub/headers/HelpHeader;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 124
    iget-boolean v2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$1;->val$isExternal:Z

    if-eqz v2, :cond_0

    .line 125
    invoke-virtual {v0, v4}, Lcom/samsung/helphub/headers/HelpHeader;->setIsExtrenal(Z)V

    .line 127
    :cond_0
    iget-object v2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$1;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    # getter for: Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->mAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    invoke-static {v2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->access$000(Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;)Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v1

    .line 128
    .local v1, "titleId":I
    iget-object v2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$1;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v2, v0, v1}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->launchToFragment(Lcom/samsung/helphub/headers/HelpHeader;I)V

    .line 134
    .end local v1    # "titleId":I
    :cond_1
    :goto_0
    return v4

    .line 130
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList$1;->this$0:Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;

    invoke-virtual {v3}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableList;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/helphub/HelpHubCommon;->requestDownloadingResource(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method
