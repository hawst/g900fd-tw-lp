.class public Lcom/samsung/helphub/fragments/SearchListAdapter;
.super Landroid/widget/CursorAdapter;
.source "SearchListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/SearchListAdapter$1;,
        Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;
    }
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field mParent:Landroid/view/ViewGroup;

.field mQuery:Ljava/lang/String;

.field result:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "query"    # Ljava/lang/String;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 34
    iput-object p1, p0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    .line 35
    iput-object p3, p0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mQuery:Ljava/lang/String;

    .line 36
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 37
    return-void
.end method

.method private static makeStringWithHighLight(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 8
    .param p0, "mTextView"    # Landroid/widget/TextView;
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "highlightstr"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 198
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 199
    :cond_0
    const-string v6, "Utils"

    const-string v7, "makeStringWithHighLight - str or highlightstr is null"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v5

    .line 225
    :goto_0
    return-object v4

    .line 203
    :cond_1
    const/4 v2, -0x1

    .line 204
    .local v2, "indexOf":I
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    .line 205
    .local v0, "highlightStrLength":I
    invoke-virtual {p0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v6

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    invoke-static {v6, p1, v7}, Landroid/text/TextUtils;->getPrefixCharForIndian(Landroid/text/TextPaint;Ljava/lang/CharSequence;[C)[C

    move-result-object v1

    .line 207
    .local v1, "iQueryForIndian":[C
    if-eqz v1, :cond_2

    .line 208
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([C)V

    .line 209
    .local v3, "s":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 210
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    .line 216
    .end local v3    # "s":Ljava/lang/String;
    :goto_1
    new-instance v4, Landroid/text/SpannableString;

    invoke-direct {v4, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 218
    .local v4, "ss":Landroid/text/SpannableString;
    const/4 v6, -0x1

    if-eq v2, v6, :cond_3

    .line 219
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    const v6, -0xff852e

    invoke-direct {v5, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int v6, v2, v0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v2, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 212
    .end local v4    # "ss":Landroid/text/SpannableString;
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 221
    .restart local v4    # "ss":Landroid/text/SpannableString;
    :cond_3
    const-string v6, "Utils"

    const-string v7, "makeStringWithHighLight - no matching word in this section"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v5

    .line 222
    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 41
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;

    .line 43
    .local v0, "holder":Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;
    return-void
.end method

.method public changeCursor(Landroid/database/Cursor;Ljava/lang/String;)V
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 244
    iput-object p2, p0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mQuery:Ljava/lang/String;

    .line 245
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/SearchListAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 246
    return-void
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SearchListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 232
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 233
    invoke-static {v0}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v1

    .line 234
    .local v1, "result":Lcom/samsung/helphub/headers/HelpHeader;
    return-object v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 26
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v22, v0

    const v23, 0x7f0400bc

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v23

    move-object/from16 v2, p3

    move/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    .line 73
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v17

    .line 74
    .local v17, "metrics":Landroid/util/DisplayMetrics;
    move-object/from16 v0, v17

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    move/from16 v22, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    move/from16 v23, v0

    sub-float v14, v22, v23

    .line 76
    .local v14, "densityDelta":F
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v22

    if-eqz v22, :cond_0

    .line 77
    const/16 v22, 0x0

    cmpl-float v22, v14, v22

    if-lez v22, :cond_8

    .line 78
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    .line 79
    .local v18, "param":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0b0033

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v18

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 81
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 89
    .end local v18    # "param":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    :goto_0
    new-instance v16, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;

    const/16 v22, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;-><init>(Lcom/samsung/helphub/fragments/SearchListAdapter$1;)V

    .line 90
    .local v16, "holder":Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    const v23, 0x7f0d010c

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;->title:Landroid/widget/TextView;

    .line 92
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    const v23, 0x7f0d0376

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;->summary:Landroid/widget/TextView;

    .line 93
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    const v23, 0x7f0d0007

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/ImageView;

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;->devider:Landroid/widget/ImageView;

    .line 94
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 99
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/helphub/fragments/SearchListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v5

    .line 100
    .local v5, "SearchTextId":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/helphub/fragments/SearchListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/helphub/headers/HelpHeader;->getTitlePs1Id()I

    move-result v11

    .line 101
    .local v11, "TextPs1Id":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/helphub/fragments/SearchListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/helphub/headers/HelpHeader;->getTitlePs2Id()I

    move-result v12

    .line 102
    .local v12, "TextPs2Id":I
    const/4 v13, 0x0

    .line 104
    .local v13, "TextPsCount":I
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v11, v0, :cond_1

    .line 105
    add-int/lit8 v13, v13, 0x1

    .line 106
    :cond_1
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v12, v0, :cond_2

    .line 107
    add-int/lit8 v13, v13, 0x1

    .line 109
    :cond_2
    packed-switch v13, :pswitch_data_0

    .line 117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 120
    .local v6, "Searchtext":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mQuery:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v6, v1}, Lcom/samsung/helphub/fragments/SearchListAdapter;->makeStringWithHighLight(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v20

    .line 121
    .local v20, "sTextTitle":Landroid/text/SpannableString;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v22, v0

    if-eqz v20, :cond_9

    .end local v20    # "sTextTitle":Landroid/text/SpannableString;
    :goto_2
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/helphub/fragments/SearchListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryId()I

    move-result v8

    .line 125
    .local v8, "SummaryId":I
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v22

    if-eqz v22, :cond_3

    .line 126
    const v22, 0x7f0a0895

    move/from16 v0, v22

    if-ne v8, v0, :cond_3

    .line 127
    const v8, 0x7f0a0896

    .line 129
    :cond_3
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v8, v0, :cond_b

    .line 132
    const/4 v7, 0x0

    .line 133
    .local v7, "SummaryCount":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/helphub/fragments/SearchListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryPs1Id()I

    move-result v9

    .line 134
    .local v9, "SummaryPs1Id":I
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/helphub/fragments/SearchListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryPs2Id()I

    move-result v10

    .line 136
    .local v10, "SummaryPs2Id":I
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v9, v0, :cond_4

    .line 137
    add-int/lit8 v7, v7, 0x1

    .line 138
    :cond_4
    const/16 v22, -0x1

    move/from16 v0, v22

    if-eq v10, v0, :cond_5

    .line 139
    add-int/lit8 v7, v7, 0x1

    .line 141
    :cond_5
    packed-switch v7, :pswitch_data_1

    .line 149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 153
    .local v4, "SearchSummaryText":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;->summary:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mQuery:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v4, v1}, Lcom/samsung/helphub/fragments/SearchListAdapter;->makeStringWithHighLight(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v19

    .line 154
    .local v19, "sTextSummary":Landroid/text/SpannableString;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;->summary:Landroid/widget/TextView;

    move-object/from16 v22, v0

    if-eqz v19, :cond_a

    .end local v19    # "sTextSummary":Landroid/text/SpannableString;
    :goto_4
    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    .end local v4    # "SearchSummaryText":Ljava/lang/String;
    .end local v7    # "SummaryCount":I
    .end local v9    # "SummaryPs1Id":I
    .end local v10    # "SummaryPs2Id":I
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/helphub/fragments/SearchListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v21

    .line 161
    .local v21, "size":I
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v22

    if-eqz v22, :cond_10

    .line 162
    if-nez p1, :cond_d

    .line 163
    const/16 v22, 0x0

    cmpl-float v22, v14, v22

    if-lez v22, :cond_6

    .line 164
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    .line 165
    .restart local v18    # "param":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-static/range {v23 .. v24}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v23

    const/16 v24, 0x0

    const/16 v25, 0x0

    invoke-static/range {v24 .. v25}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v24

    invoke-virtual/range {v22 .. v24}, Landroid/view/View;->measure(II)V

    .line 167
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    .line 168
    .local v15, "height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0b0038

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    add-int v22, v22, v15

    move/from16 v0, v22

    move-object/from16 v1, v18

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 171
    .end local v15    # "height":I
    .end local v18    # "param":Landroid/view/ViewGroup$LayoutParams;
    :cond_6
    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_c

    .line 172
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    const v23, 0x7f02030c

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setBackgroundResource(I)V

    .line 180
    :goto_6
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;->devider:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    if-eqz v22, :cond_7

    .line 182
    add-int/lit8 v22, v21, -0x1

    move/from16 v0, p1

    move/from16 v1, v22

    if-ne v0, v1, :cond_f

    .line 183
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;->devider:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 193
    :cond_7
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    return-object v22

    .line 83
    .end local v5    # "SearchTextId":I
    .end local v6    # "Searchtext":Ljava/lang/String;
    .end local v8    # "SummaryId":I
    .end local v11    # "TextPs1Id":I
    .end local v12    # "TextPs2Id":I
    .end local v13    # "TextPsCount":I
    .end local v16    # "holder":Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;
    .end local v21    # "size":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    .line 84
    .restart local v18    # "param":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0b0032

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, v18

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 86
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 111
    .end local v18    # "param":Landroid/view/ViewGroup$LayoutParams;
    .restart local v5    # "SearchTextId":I
    .restart local v11    # "TextPs1Id":I
    .restart local v12    # "TextPs2Id":I
    .restart local v13    # "TextPsCount":I
    .restart local v16    # "holder":Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v5, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 112
    .restart local v6    # "Searchtext":Ljava/lang/String;
    goto/16 :goto_1

    .line 114
    .end local v6    # "Searchtext":Ljava/lang/String;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v5, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 115
    .restart local v6    # "Searchtext":Ljava/lang/String;
    goto/16 :goto_1

    .restart local v20    # "sTextTitle":Landroid/text/SpannableString;
    :cond_9
    move-object/from16 v20, v6

    .line 121
    goto/16 :goto_2

    .line 143
    .end local v20    # "sTextTitle":Landroid/text/SpannableString;
    .restart local v7    # "SummaryCount":I
    .restart local v8    # "SummaryId":I
    .restart local v9    # "SummaryPs1Id":I
    .restart local v10    # "SummaryPs2Id":I
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v8, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 144
    .restart local v4    # "SearchSummaryText":Ljava/lang/String;
    goto/16 :goto_3

    .line 146
    .end local v4    # "SearchSummaryText":Ljava/lang/String;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v8, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 147
    .restart local v4    # "SearchSummaryText":Ljava/lang/String;
    goto/16 :goto_3

    .restart local v19    # "sTextSummary":Landroid/text/SpannableString;
    :cond_a
    move-object/from16 v19, v4

    .line 154
    goto/16 :goto_4

    .line 156
    .end local v4    # "SearchSummaryText":Ljava/lang/String;
    .end local v7    # "SummaryCount":I
    .end local v9    # "SummaryPs1Id":I
    .end local v10    # "SummaryPs2Id":I
    .end local v19    # "sTextSummary":Landroid/text/SpannableString;
    :cond_b
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;->summary:Landroid/widget/TextView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 174
    .restart local v21    # "size":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    const v23, 0x7f020052

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_6

    .line 175
    :cond_d
    add-int/lit8 v22, v21, -0x1

    move/from16 v0, p1

    move/from16 v1, v22

    if-ne v0, v1, :cond_e

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    const v23, 0x7f020050

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_6

    .line 178
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    const v23, 0x7f020051

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_6

    .line 185
    :cond_f
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;->devider:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_7

    .line 189
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    move-object/from16 v22, v0

    const v23, 0x7f02030c

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->setBackgroundResource(I)V

    .line 190
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/samsung/helphub/fragments/SearchListAdapter$HeaderViewHolder;->devider:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_7

    .line 109
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 141
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SearchListAdapter;->result:Landroid/view/View;

    return-object v0
.end method
