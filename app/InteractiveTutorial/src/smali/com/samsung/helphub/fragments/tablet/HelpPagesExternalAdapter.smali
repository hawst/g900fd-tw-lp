.class public Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;
.super Landroid/widget/BaseAdapter;
.source "HelpPagesExternalAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;
    }
.end annotation


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mCursor:Landroid/database/Cursor;

.field private mExpanded:[Z

.field private mInflater:Landroid/view/LayoutInflater;

.field private mOnHelpItemViewClickListener:Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 42
    const/4 v0, 0x1

    invoke-static {v0}, Landroid/app/FragmentManager;->enableDebugLogging(Z)V

    .line 44
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mActivity:Landroid/app/Activity;

    .line 45
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mActivity:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 46
    return-void
.end method


# virtual methods
.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 241
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 242
    .local v0, "old":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 243
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 245
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 54
    const/4 v0, 0x0

    .line 55
    .local v0, "result":I
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 58
    :cond_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 63
    const/4 v0, 0x0

    .line 64
    .local v0, "result":Ljava/lang/Object;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 66
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mCursor:Landroid/database/Cursor;

    invoke-static {v1}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v0

    .line 68
    .end local v0    # "result":Ljava/lang/Object;
    :cond_0
    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 75
    add-int/lit8 v0, p1, 0x1

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 194
    return p1
.end method

.method public getOpenedItem()I
    .locals 3

    .prologue
    .line 173
    const/4 v1, -0x1

    .line 174
    .local v1, "result":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mExpanded:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 175
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mExpanded:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_1

    .line 176
    move v1, v0

    .line 180
    :cond_0
    return v1

    .line 174
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 81
    const/4 v4, 0x0

    .line 82
    .local v4, "result":Lcom/samsung/helphub/widget/HelpItemExpView;
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    .line 84
    .local v2, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-nez p2, :cond_3

    .line 87
    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f0400b8

    const/4 v8, 0x0

    invoke-virtual {v6, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .end local v4    # "result":Lcom/samsung/helphub/widget/HelpItemExpView;
    check-cast v4, Lcom/samsung/helphub/widget/HelpItemExpView;

    .line 90
    .restart local v4    # "result":Lcom/samsung/helphub/widget/HelpItemExpView;
    new-instance v3, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;

    invoke-direct {v3}, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;-><init>()V

    .line 91
    .local v3, "holder":Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;
    const v6, 0x7f0d008a

    invoke-virtual {v4, v6}, Lcom/samsung/helphub/widget/HelpItemExpView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, v3, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;->title:Landroid/widget/TextView;

    .line 92
    const v6, 0x7f0d0088

    invoke-virtual {v4, v6}, Lcom/samsung/helphub/widget/HelpItemExpView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    iput-object v6, v3, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    .line 93
    iget-object v6, v3, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->getItemId(I)J

    move-result-wide v8

    long-to-int v7, v8

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->setId(I)V

    .line 95
    invoke-virtual {v4, v3}, Lcom/samsung/helphub/widget/HelpItemExpView;->setTag(Ljava/lang/Object;)V

    .line 96
    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mOnHelpItemViewClickListener:Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;

    invoke-virtual {v4, v6}, Lcom/samsung/helphub/widget/HelpItemExpView;->setOnHelpItemViewClickListener(Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;)V

    .line 97
    invoke-virtual {v4, p1}, Lcom/samsung/helphub/widget/HelpItemExpView;->setListPosition(I)V

    .line 105
    :goto_0
    iget-object v6, v3, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 107
    :try_start_0
    iget-object v6, v3, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getActivityTitleId()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :goto_1
    new-instance v0, Landroid/os/Bundle;

    const/4 v6, 0x1

    invoke-direct {v0, v6}, Landroid/os/Bundle;-><init>(I)V

    .line 114
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v6, "helpub:header"

    invoke-virtual {v0, v6, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 115
    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mExpanded:[Z

    aget-boolean v6, v6, p1

    if-eqz v6, :cond_2

    .line 116
    iget-object v6, v3, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;->pageFragment:Ljava/lang/ref/WeakReference;

    if-eqz v6, :cond_0

    iget-object v6, v3, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;->pageFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 120
    :cond_0
    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getFragmentName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/Fragment;

    move-result-object v1

    .line 122
    .local v1, "fragment":Landroid/app/Fragment;
    new-instance v6, Ljava/lang/ref/WeakReference;

    invoke-direct {v6, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v6, v3, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;->pageFragment:Ljava/lang/ref/WeakReference;

    .line 125
    .end local v1    # "fragment":Landroid/app/Fragment;
    :cond_1
    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mActivity:Landroid/app/Activity;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mActivity:Landroid/app/Activity;

    iget-object v7, v3, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getId()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-nez v6, :cond_2

    .line 126
    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v5

    .line 127
    .local v5, "transaction":Landroid/app/FragmentTransaction;
    iget-object v6, v3, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;->pageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getId()I

    move-result v7

    iget-object v6, v3, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;->pageFragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/Fragment;

    invoke-virtual {v5, v7, v6}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 128
    invoke-virtual {v5}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 134
    .end local v5    # "transaction":Landroid/app/FragmentTransaction;
    :cond_2
    iget-object v6, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mExpanded:[Z

    aget-boolean v6, v6, p1

    invoke-virtual {v4, v6}, Lcom/samsung/helphub/widget/HelpItemExpView;->expand(Z)V

    .line 137
    return-object v4

    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v3    # "holder":Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;
    :cond_3
    move-object v4, p2

    .line 102
    check-cast v4, Lcom/samsung/helphub/widget/HelpItemExpView;

    .line 103
    invoke-virtual {v4}, Lcom/samsung/helphub/widget/HelpItemExpView;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;

    .restart local v3    # "holder":Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter$ItemViewHolder;
    goto :goto_0

    .line 108
    :catch_0
    move-exception v6

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 2

    .prologue
    .line 185
    const/4 v0, 0x1

    .line 186
    .local v0, "result":I
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->getCount()I

    move-result v0

    .line 189
    :cond_0
    return v0
.end method

.method public isToggle(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mExpanded:[Z

    aget-boolean v0, v0, p1

    return v0
.end method

.method public setOnHelpItemViewClickListener(Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mOnHelpItemViewClickListener:Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;

    .line 50
    return-void
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 5
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, 0x1

    .line 212
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mCursor:Landroid/database/Cursor;

    if-ne p1, v2, :cond_0

    .line 213
    const/4 v1, 0x0

    .line 231
    :goto_0
    return-object v1

    .line 215
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mCursor:Landroid/database/Cursor;

    .line 216
    .local v1, "oldCursor":Landroid/database/Cursor;
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mCursor:Landroid/database/Cursor;

    .line 217
    if-eqz p1, :cond_2

    .line 218
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 219
    .local v0, "count":I
    new-array v2, v0, [Z

    iput-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mExpanded:[Z

    .line 220
    if-ne v0, v4, :cond_1

    .line 222
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mExpanded:[Z

    const/4 v3, 0x0

    aput-boolean v4, v2, v3

    .line 226
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 229
    .end local v0    # "count":I
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->notifyDataSetInvalidated()V

    goto :goto_0
.end method

.method public toggle(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 146
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->isToggle(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->toggle(IZ)V

    .line 147
    return-void

    .line 146
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toggle(IZ)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "isExpanded"    # Z

    .prologue
    .line 156
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mExpanded:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 157
    if-ne v0, p1, :cond_0

    .line 158
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mExpanded:[Z

    aput-boolean p2, v1, v0

    .line 156
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 160
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->mExpanded:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    goto :goto_1

    .line 163
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HelpPagesExternalAdapter;->notifyDataSetChanged()V

    .line 164
    return-void
.end method
