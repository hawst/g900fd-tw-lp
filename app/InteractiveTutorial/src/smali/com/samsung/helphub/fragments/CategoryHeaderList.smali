.class public Lcom/samsung/helphub/fragments/CategoryHeaderList;
.super Lcom/samsung/helphub/fragments/SearchableHeaderList;
.source "CategoryHeaderList.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/helphub/fragments/SearchableHeaderList;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final BUNDLE_KEY_SCROLLER_POSITION_X:Ljava/lang/String; = "category_help:scroller_position_x"

.field private static final BUNDLE_KEY_SCROLLER_POSITION_Y:Ljava/lang/String; = "category_help:scroller_position_y"

.field static loaderID:I


# instance fields
.field private mBubbleNewFeature:Landroid/view/View;

.field private mBubbleNewFeatureViewMore:Landroid/view/View;

.field private mButtonSearch:Landroid/widget/ImageButton;

.field private mButtonSort:Landroid/widget/ImageButton;

.field private mCursor:Landroid/database/Cursor;

.field private mHeaderNewFeature:Lcom/samsung/helphub/headers/HelpHeader;

.field private mImageView:Landroid/widget/ImageView;

.field mScroller:Landroid/widget/ScrollView;

.field private mTitleTextView:Landroid/widget/TextView;

.field private mWelcomeTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    sput v0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->loaderID:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/SearchableHeaderList;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mHeaderNewFeature:Lcom/samsung/helphub/headers/HelpHeader;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/fragments/CategoryHeaderList;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/CategoryHeaderList;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mButtonSort:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/fragments/CategoryHeaderList;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/CategoryHeaderList;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mButtonSearch:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/helphub/fragments/CategoryHeaderList;)Lcom/samsung/helphub/headers/HelpHeader;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/CategoryHeaderList;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mHeaderNewFeature:Lcom/samsung/helphub/headers/HelpHeader;

    return-object v0
.end method


# virtual methods
.method protected getHeaderXmlID()I
    .locals 3

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/CategoryHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "easy_mode_switch"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 308
    const v0, 0x7f060001

    .line 310
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x7f060000

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 12
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v11, 0x7f02025b

    const/4 v10, 0x2

    const/4 v9, -0x2

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 205
    const/16 v1, 0x15

    .line 206
    .local v1, "padding_in_dp_p":I
    const/16 v0, 0xa

    .line 207
    .local v0, "padding_in_dp_l":I
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/CategoryHeaderList;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v5, v6, Landroid/util/DisplayMetrics;->density:F

    .line 208
    .local v5, "scale":F
    int-to-float v6, v1

    mul-float/2addr v6, v5

    float-to-int v3, v6

    .line 209
    .local v3, "padding_in_px_p":I
    int-to-float v6, v0

    mul-float/2addr v6, v5

    float-to-int v2, v6

    .line 211
    .local v2, "padding_in_px_l":I
    iget v6, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v6, v10, :cond_3

    .line 213
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {v8}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 214
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mImageView:Landroid/widget/ImageView;

    const v7, 0x7f02025c

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 215
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 216
    .local v4, "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v6, 0x42600000    # 56.0f

    mul-float/2addr v6, v5

    float-to-int v6, v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 217
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 218
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mWelcomeTextView:Landroid/widget/TextView;

    if-eqz v6, :cond_0

    .line 219
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mWelcomeTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setLines(I)V

    .line 220
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mWelcomeTextView:Landroid/widget/TextView;

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 263
    .end local v4    # "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 264
    return-void

    .line 223
    :cond_1
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {v8}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 224
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mImageView:Landroid/widget/ImageView;

    const v7, 0x7f02025c

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 225
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 226
    .restart local v4    # "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v6, 0x41a00000    # 20.0f

    mul-float/2addr v6, v5

    float-to-int v6, v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 227
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 231
    .end local v4    # "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mBubbleNewFeatureViewMore:Landroid/view/View;

    invoke-virtual {v6, v7, v7, v7, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 234
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mImageView:Landroid/widget/ImageView;

    const v7, 0x7f020259

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 238
    :cond_3
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-static {v8}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 239
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v6, v11}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 240
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 241
    .restart local v4    # "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v6, v5

    float-to-int v6, v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 242
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 250
    .end local v4    # "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v6

    if-nez v6, :cond_5

    invoke-static {v8}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 251
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v6, v11}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 252
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 253
    .restart local v4    # "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v6, 0x42740000    # 61.0f

    mul-float/2addr v6, v5

    float-to-int v6, v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 254
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 257
    .end local v4    # "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_5
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mBubbleNewFeatureViewMore:Landroid/view/View;

    invoke-virtual {v6, v7, v7, v7, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 260
    iget-object v6, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mImageView:Landroid/widget/ImageView;

    const v7, 0x7f02025a

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 280
    const-string v0, "content://com.samsung.helphub.provider.search/header/category/new_feature"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 281
    .local v2, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/CategoryHeaderList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v3, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 15
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 54
    invoke-super/range {p0 .. p3}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v7

    .line 55
    .local v7, "result":Landroid/view/View;
    const v11, 0x7f0d009b

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mBubbleNewFeature:Landroid/view/View;

    .line 56
    const v11, 0x7f0d036e

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mBubbleNewFeatureViewMore:Landroid/view/View;

    .line 59
    if-eqz p3, :cond_5

    .line 60
    const-string v11, "category_help:scroller_position_x"

    const/4 v12, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    .line 61
    .local v9, "scrollPositionX":I
    const-string v11, "category_help:scroller_position_y"

    const/4 v12, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v10

    .line 66
    .local v10, "scrollPositionY":I
    :goto_0
    const v11, 0x7f0d036a

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ScrollView;

    iput-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mScroller:Landroid/widget/ScrollView;

    .line 67
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mScroller:Landroid/widget/ScrollView;

    new-instance v12, Lcom/samsung/helphub/fragments/CategoryHeaderList$1;

    invoke-direct {v12, p0, v9, v10}, Lcom/samsung/helphub/fragments/CategoryHeaderList$1;-><init>(Lcom/samsung/helphub/fragments/CategoryHeaderList;II)V

    invoke-virtual {v11, v12}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 79
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/CategoryHeaderList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    move-result-object v1

    .line 80
    .local v1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {v1}, Landroid/content/Loader;->isReset()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/CategoryHeaderList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 85
    :cond_0
    const v11, 0x7f0d036b

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    iput-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mImageView:Landroid/widget/ImageView;

    .line 86
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v11

    if-eqz v11, :cond_1

    const/4 v11, 0x0

    invoke-static {v11}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 87
    const v11, 0x7f0d00b1

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mWelcomeTextView:Landroid/widget/TextView;

    .line 89
    :cond_1
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v11

    if-nez v11, :cond_3

    const/4 v11, 0x0

    invoke-static {v11}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 90
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mBubbleNewFeature:Landroid/view/View;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 91
    const v11, 0x7f0d036d

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    .line 92
    const v11, 0x7f0d01a6

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageButton;

    iput-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mButtonSort:Landroid/widget/ImageButton;

    .line 93
    const v11, 0x7f0d01a7

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageButton;

    iput-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mButtonSearch:Landroid/widget/ImageButton;

    .line 95
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mButtonSort:Landroid/widget/ImageButton;

    if-eqz v11, :cond_2

    .line 96
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mButtonSort:Landroid/widget/ImageButton;

    new-instance v12, Lcom/samsung/helphub/fragments/CategoryHeaderList$2;

    invoke-direct {v12, p0}, Lcom/samsung/helphub/fragments/CategoryHeaderList$2;-><init>(Lcom/samsung/helphub/fragments/CategoryHeaderList;)V

    invoke-virtual {v11, v12}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mButtonSort:Landroid/widget/ImageButton;

    new-instance v12, Lcom/samsung/helphub/fragments/CategoryHeaderList$3;

    invoke-direct {v12, p0}, Lcom/samsung/helphub/fragments/CategoryHeaderList$3;-><init>(Lcom/samsung/helphub/fragments/CategoryHeaderList;)V

    invoke-virtual {v11, v12}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 119
    :cond_2
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mButtonSearch:Landroid/widget/ImageButton;

    if-eqz v11, :cond_3

    .line 120
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mButtonSearch:Landroid/widget/ImageButton;

    new-instance v12, Lcom/samsung/helphub/fragments/CategoryHeaderList$4;

    invoke-direct {v12, p0}, Lcom/samsung/helphub/fragments/CategoryHeaderList$4;-><init>(Lcom/samsung/helphub/fragments/CategoryHeaderList;)V

    invoke-virtual {v11, v12}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mButtonSearch:Landroid/widget/ImageButton;

    new-instance v12, Lcom/samsung/helphub/fragments/CategoryHeaderList$5;

    invoke-direct {v12, p0}, Lcom/samsung/helphub/fragments/CategoryHeaderList$5;-><init>(Lcom/samsung/helphub/fragments/CategoryHeaderList;)V

    invoke-virtual {v11, v12}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 144
    :cond_3
    const/16 v3, 0x15

    .line 145
    .local v3, "padding_in_dp_p":I
    const/16 v2, 0xa

    .line 146
    .local v2, "padding_in_dp_l":I
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/CategoryHeaderList;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v11

    iget v8, v11, Landroid/util/DisplayMetrics;->density:F

    .line 147
    .local v8, "scale":F
    int-to-float v11, v3

    mul-float/2addr v11, v8

    const/high16 v12, 0x3f000000    # 0.5f

    add-float/2addr v11, v12

    float-to-int v5, v11

    .line 148
    .local v5, "padding_in_px_p":I
    int-to-float v11, v2

    mul-float/2addr v11, v8

    const/high16 v12, 0x3f000000    # 0.5f

    add-float/2addr v11, v12

    float-to-int v4, v11

    .line 150
    .local v4, "padding_in_px_l":I
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/CategoryHeaderList;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    iget v11, v11, Landroid/content/res/Configuration;->orientation:I

    const/4 v12, 0x2

    if-ne v11, v12, :cond_8

    .line 152
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v11

    if-eqz v11, :cond_6

    const/4 v11, 0x0

    invoke-static {v11}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 153
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mImageView:Landroid/widget/ImageView;

    const v12, 0x7f02025c

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 154
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 155
    .local v6, "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v11, 0x42600000    # 56.0f

    mul-float/2addr v11, v8

    float-to-int v11, v11

    iput v11, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 156
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v11, v6}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 157
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mWelcomeTextView:Landroid/widget/TextView;

    if-eqz v11, :cond_4

    .line 158
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mWelcomeTextView:Landroid/widget/TextView;

    const/4 v12, 0x2

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setLines(I)V

    .line 159
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mWelcomeTextView:Landroid/widget/TextView;

    new-instance v12, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v13, -0x2

    const/4 v14, -0x2

    invoke-direct {v12, v13, v14}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 200
    .end local v6    # "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    :goto_1
    return-object v7

    .line 63
    .end local v1    # "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    .end local v2    # "padding_in_dp_l":I
    .end local v3    # "padding_in_dp_p":I
    .end local v4    # "padding_in_px_l":I
    .end local v5    # "padding_in_px_p":I
    .end local v8    # "scale":F
    .end local v9    # "scrollPositionX":I
    .end local v10    # "scrollPositionY":I
    :cond_5
    const/4 v9, 0x0

    .line 64
    .restart local v9    # "scrollPositionX":I
    const/4 v10, 0x0

    .restart local v10    # "scrollPositionY":I
    goto/16 :goto_0

    .line 162
    .restart local v1    # "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    .restart local v2    # "padding_in_dp_l":I
    .restart local v3    # "padding_in_dp_p":I
    .restart local v4    # "padding_in_px_l":I
    .restart local v5    # "padding_in_px_p":I
    .restart local v8    # "scale":F
    :cond_6
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v11

    if-nez v11, :cond_7

    const/4 v11, 0x0

    invoke-static {v11}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v11

    if-nez v11, :cond_7

    .line 163
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mImageView:Landroid/widget/ImageView;

    const v12, 0x7f02025c

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 164
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 165
    .restart local v6    # "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v11, 0x41a00000    # 20.0f

    mul-float/2addr v11, v8

    float-to-int v11, v11

    iput v11, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 166
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v11, v6}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 169
    .end local v6    # "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_7
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mBubbleNewFeatureViewMore:Landroid/view/View;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v11, v12, v13, v14, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 172
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mImageView:Landroid/widget/ImageView;

    const v12, 0x7f020259

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 175
    :cond_8
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v11

    if-eqz v11, :cond_9

    const/4 v11, 0x0

    invoke-static {v11}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v11

    if-nez v11, :cond_9

    .line 176
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mImageView:Landroid/widget/ImageView;

    const v12, 0x7f02025b

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 177
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 178
    .restart local v6    # "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v11, 0x42c80000    # 100.0f

    mul-float/2addr v11, v8

    float-to-int v11, v11

    iput v11, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 179
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v11, v6}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 187
    .end local v6    # "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_9
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v11

    if-nez v11, :cond_a

    const/4 v11, 0x0

    invoke-static {v11}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v11

    if-nez v11, :cond_a

    .line 188
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mImageView:Landroid/widget/ImageView;

    const v12, 0x7f02025b

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 189
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 190
    .restart local v6    # "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    const/high16 v11, 0x42740000    # 61.0f

    mul-float/2addr v11, v8

    float-to-int v11, v11

    iput v11, v6, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 191
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v11, v6}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 194
    .end local v6    # "plControl":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_a
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mBubbleNewFeatureViewMore:Landroid/view/View;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v11, v12, v13, v14, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 197
    iget-object v11, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mImageView:Landroid/widget/ImageView;

    const v12, 0x7f02025a

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_1
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 286
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    invoke-static {p2}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mHeaderNewFeature:Lcom/samsung/helphub/headers/HelpHeader;

    .line 288
    iget-object v0, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mBubbleNewFeature:Landroid/view/View;

    new-instance v1, Lcom/samsung/helphub/fragments/CategoryHeaderList$6;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/fragments/CategoryHeaderList$6;-><init>(Lcom/samsung/helphub/fragments/CategoryHeaderList;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    :cond_0
    iput-object p2, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mCursor:Landroid/database/Cursor;

    .line 297
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/fragments/CategoryHeaderList;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 300
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 303
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 268
    invoke-super {p0, p1}, Lcom/samsung/helphub/fragments/SearchableHeaderList;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 270
    iget-object v0, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mScroller:Landroid/widget/ScrollView;

    if-eqz v0, :cond_0

    .line 271
    const-string v0, "category_help:scroller_position_x"

    iget-object v1, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mScroller:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 272
    const-string v0, "category_help:scroller_position_y"

    iget-object v1, p0, Lcom/samsung/helphub/fragments/CategoryHeaderList;->mScroller:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 274
    :cond_0
    return-void
.end method
