.class Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$3;
.super Ljava/lang/Object;
.source "HeaderContentsList.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnChildClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$3;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .locals 5
    .param p1, "parent"    # Landroid/widget/ExpandableListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "groupPosition"    # I
    .param p4, "childPosition"    # I
    .param p5, "id"    # J

    .prologue
    .line 175
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$3;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    invoke-virtual {v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 176
    .local v0, "focusView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 177
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$3;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    invoke-virtual {v2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 181
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$3;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    iget-object v2, v2, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v2, :cond_0

    .line 182
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$3;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    iget-object v2, v2, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v2}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 186
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 187
    # setter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mGroupItemSelected:I
    invoke-static {p3}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->access$002(I)I

    .line 188
    # setter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mChildItemSelected:I
    invoke-static {p4}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->access$102(I)I

    .line 189
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$3;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mGroupItemSelected:I
    invoke-static {}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->access$000()I

    move-result v3

    # getter for: Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->mChildItemSelected:I
    invoke-static {}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->access$100()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->launchFragment(II)V

    .line 194
    :goto_0
    const/4 v2, 0x1

    return v2

    .line 192
    :cond_1
    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList$3;->this$0:Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;

    invoke-virtual {v2, p3, p4}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsList;->doActiveSectionItem(II)V

    goto :goto_0
.end method
