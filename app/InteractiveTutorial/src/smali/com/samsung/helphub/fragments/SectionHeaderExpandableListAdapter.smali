.class public Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "SectionHeaderExpandableListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$1;,
        Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;,
        Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;
    }
.end annotation


# instance fields
.field private mChildAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;

.field private mContext:Landroid/content/Context;

.field private mGroupList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(ILandroid/content/Context;)V
    .locals 1
    .param p1, "resXmlId"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 43
    iput-object p2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mContext:Landroid/content/Context;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mGroupList:Ljava/util/List;

    .line 46
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mGroupList:Ljava/util/List;

    invoke-static {p2, p1, v0}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->loadHeadersFromResource(Landroid/content/Context;ILjava/util/List;)V

    .line 48
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mGroupList:Ljava/util/List;

    invoke-direct {p0, v0, p2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->filterHeaderList(Ljava/util/List;Landroid/content/Context;)Ljava/util/List;

    .line 49
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 50
    new-instance v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;

    invoke-direct {v0, p0, p2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;-><init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;

    .line 51
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Landroid/content/Context;)V
    .locals 5
    .param p1, "resXmlId"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 62
    iput-object p3, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mContext:Landroid/content/Context;

    .line 63
    new-instance v3, Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mGroupList:Ljava/util/List;

    .line 64
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .local v2, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-static {p3, p1, v2}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->loadHeadersFromResource(Landroid/content/Context;ILjava/util/List;)V

    .line 66
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/headers/HelpHeader;

    .line 67
    .local v1, "item":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 68
    iget-object v3, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mGroupList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    .end local v1    # "item":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_1
    const-string v3, "layout_inflater"

    invoke-virtual {p3, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    iput-object v3, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 73
    new-instance v3, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;

    invoke-direct {v3, p0, p3}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;-><init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;

    .line 74
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "groupList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 77
    iput-object p2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mContext:Landroid/content/Context;

    .line 78
    iput-object p1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mGroupList:Ljava/util/List;

    .line 79
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 80
    new-instance v0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;

    invoke-direct {v0, p0, p2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;-><init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;

    .line 81
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;Ljava/util/List;Landroid/content/Context;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->filterItemList(Ljava/util/List;Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;Landroid/content/Context;III)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->makePSText(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private filterHeaderList(Ljava/util/List;Landroid/content/Context;)Ljava/util/List;
    .locals 10
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 535
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    move-object v8, p1

    .line 536
    .local v8, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 537
    .local v2, "count":I
    add-int/lit8 v4, v2, -0x1

    .local v4, "i":I
    :goto_0
    if-ltz v4, :cond_2

    .line 538
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/helphub/headers/HelpHeader;

    .line 539
    .local v3, "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-static {p2}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->getHiddenSectionHeaderId(Landroid/content/Context;)[I

    move-result-object v1

    .line 540
    .local v1, "conditions":[I
    move-object v0, v1

    .local v0, "arr$":[I
    array-length v7, v0

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v7, :cond_1

    aget v6, v0, v5

    .line 541
    .local v6, "item":I
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v9

    if-ne v9, v6, :cond_0

    .line 542
    invoke-interface {p1, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 540
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 537
    .end local v6    # "item":I
    :cond_1
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    .line 547
    .end local v0    # "arr$":[I
    .end local v1    # "conditions":[I
    .end local v3    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v5    # "i$":I
    .end local v7    # "len$":I
    :cond_2
    return-object v8
.end method

.method private filterItemList(Ljava/util/List;Landroid/content/Context;)Ljava/util/List;
    .locals 10
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 551
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    move-object v8, p1

    .line 552
    .local v8, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 553
    .local v2, "count":I
    add-int/lit8 v4, v2, -0x1

    .local v4, "i":I
    :goto_0
    if-ltz v4, :cond_2

    .line 554
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/helphub/headers/HelpHeader;

    .line 555
    .local v3, "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-static {p2}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->getHiddenItemHeaderId(Landroid/content/Context;)[I

    move-result-object v1

    .line 556
    .local v1, "conditions":[I
    move-object v0, v1

    .local v0, "arr$":[I
    array-length v7, v0

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v7, :cond_1

    aget v6, v0, v5

    .line 557
    .local v6, "item":I
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v9

    if-ne v9, v6, :cond_0

    .line 558
    invoke-interface {p1, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 556
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 553
    .end local v6    # "item":I
    :cond_1
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    .line 562
    .end local v0    # "arr$":[I
    .end local v1    # "conditions":[I
    .end local v3    # "header":Lcom/samsung/helphub/headers/HelpHeader;
    .end local v5    # "i$":I
    .end local v7    # "len$":I
    :cond_2
    return-object v8
.end method

.method private makePSText(Landroid/content/Context;III)Ljava/lang/String;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textID"    # I
    .param p3, "TextPs1Id"    # I
    .param p4, "TextPs2Id"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 290
    const/4 v0, 0x0

    .line 293
    .local v0, "TextPsCount":I
    if-eq p3, v2, :cond_0

    .line 294
    add-int/lit8 v0, v0, 0x1

    .line 295
    :cond_0
    if-eq p4, v2, :cond_1

    .line 296
    add-int/lit8 v0, v0, 0x1

    .line 298
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 306
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 309
    .local v1, "result":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 300
    .end local v1    # "result":Ljava/lang/String;
    :pswitch_0
    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 301
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0

    .line 303
    .end local v1    # "result":Ljava/lang/String;
    :pswitch_1
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p1, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 304
    .restart local v1    # "result":Ljava/lang/String;
    goto :goto_0

    .line 298
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getChild(II)Ljava/lang/Object;
    .locals 3
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    .line 86
    .local v0, "groupItem":Lcom/samsung/helphub/headers/HelpHeader;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getSubHeadersArrayId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->setHeaderXml(I)V

    .line 87
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;

    invoke-virtual {v1, p2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method public getChildId(II)J
    .locals 2
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 92
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    .line 99
    .local v0, "groupItem":Lcom/samsung/helphub/headers/HelpHeader;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getSubHeadersArrayId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->setHeaderXml(I)V

    .line 100
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;

    invoke-virtual {v1, p2, p4, p5}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public getChildrenCount(I)I
    .locals 3
    .param p1, "groupPosition"    # I

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    .line 106
    .local v0, "groupItem":Lcom/samsung/helphub/headers/HelpHeader;
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getSubHeadersArrayId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->setHeaderXml(I)V

    .line 107
    iget-object v1, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mChildAdapter:Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;

    invoke-virtual {v1}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$ChildAdapter;->getCount()I

    move-result v1

    return v1
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 4
    .param p1, "groupPosition"    # I

    .prologue
    .line 114
    :try_start_0
    iget-object v2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mGroupList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 119
    .local v1, "object":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 115
    .end local v1    # "object":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    iget-object v2, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mGroupList:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .restart local v1    # "object":Ljava/lang/Object;
    goto :goto_0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mGroupList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 129
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 14
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CutPasteId"
        }
    .end annotation

    .prologue
    .line 136
    const/4 v6, 0x0

    .line 137
    .local v6, "result":Landroid/view/View;
    const/4 v4, 0x0

    .line 138
    .local v4, "holder":Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/helphub/headers/HelpHeader;

    .line 142
    .local v3, "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v10

    sget-object v11, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    if-ne v10, v11, :cond_0

    .line 169
    iget-object v10, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v11, 0x7f0400b7

    const/4 v12, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v10, v11, v0, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 170
    const v10, 0x7f0d010c

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    .line 171
    const v10, 0x7f0d010f

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getIconId()I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object v7, v6

    .line 286
    .end local v6    # "result":Landroid/view/View;
    .local v7, "result":Landroid/view/View;
    :goto_0
    return-object v7

    .line 181
    .end local v7    # "result":Landroid/view/View;
    .restart local v6    # "result":Landroid/view/View;
    :cond_0
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 182
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryId()I

    move-result v10

    const v11, 0x7f0a0895

    if-ne v10, v11, :cond_1

    .line 183
    const v10, 0x7f0a0896

    invoke-virtual {v3, v10}, Lcom/samsung/helphub/headers/HelpHeader;->setSummaryId(I)V

    .line 185
    :cond_1
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v10

    const v11, 0x7f0d00c8

    if-eq v10, v11, :cond_2

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v10

    const v11, 0x7f0d00c7

    if-eq v10, v11, :cond_2

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v10

    const v11, 0x7f0d00c9

    if-ne v10, v11, :cond_7

    .line 188
    :cond_2
    iget-object v10, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v11, 0x7f0400c3

    const/4 v12, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v10, v11, v0, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 190
    const v10, 0x7f0d010c

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 191
    .local v9, "title":Landroid/widget/TextView;
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 192
    const v10, 0x7f02030c

    invoke-virtual {v6, v10}, Landroid/view/View;->setBackgroundResource(I)V

    .line 193
    const/4 v8, 0x0

    .line 194
    .local v8, "summary":Landroid/widget/TextView;
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryId()I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_3

    .line 195
    const v10, 0x7f0d010d

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "summary":Landroid/widget/TextView;
    check-cast v8, Landroid/widget/TextView;

    .line 196
    .restart local v8    # "summary":Landroid/widget/TextView;
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 197
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryId()I

    move-result v10

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(I)V

    .line 199
    :cond_3
    const v10, 0x7f0d0110

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 200
    .local v5, "icon":Landroid/widget/ImageView;
    if-eqz v5, :cond_4

    .line 201
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getIconId()I

    move-result v10

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 204
    :cond_4
    const v10, 0x7f0d0378

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 205
    .local v1, "background":Landroid/widget/ImageView;
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 217
    :goto_1
    iget-object v10, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v10, v10, Landroid/content/res/Configuration;->orientation:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_6

    .line 218
    if-eqz v8, :cond_5

    .line 219
    invoke-virtual {v8}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    const/16 v11, 0x35a

    iput v11, v10, Landroid/view/ViewGroup$LayoutParams;->width:I

    :cond_5
    :goto_2
    move-object v7, v6

    .line 230
    .end local v6    # "result":Landroid/view/View;
    .restart local v7    # "result":Landroid/view/View;
    goto/16 :goto_0

    .line 207
    .end local v7    # "result":Landroid/view/View;
    .restart local v6    # "result":Landroid/view/View;
    :pswitch_0
    const v10, 0x7f0202b8

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 210
    :pswitch_1
    const v10, 0x7f0202b9

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 213
    :pswitch_2
    const v10, 0x7f0202ba

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 224
    :cond_6
    if-eqz v8, :cond_5

    .line 225
    invoke-virtual {v8}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    const/16 v11, 0x204

    iput v11, v10, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_2

    .line 233
    .end local v1    # "background":Landroid/widget/ImageView;
    .end local v5    # "icon":Landroid/widget/ImageView;
    .end local v8    # "summary":Landroid/widget/TextView;
    .end local v9    # "title":Landroid/widget/TextView;
    :cond_7
    if-eqz p3, :cond_8

    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_8

    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getId()I

    move-result v10

    const v11, 0x7f0d0109

    if-eq v10, v11, :cond_b

    .line 235
    :cond_8
    new-instance v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;

    .end local v4    # "holder":Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;
    const/4 v10, 0x0

    invoke-direct {v4, v10}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;-><init>(Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$1;)V

    .line 236
    .restart local v4    # "holder":Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;
    iget-object v10, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v11, 0x7f0400bf

    const/4 v12, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v10, v11, v0, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 237
    const v10, 0x7f0d010c

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->Title:Landroid/widget/TextView;

    .line 238
    const v10, 0x7f0d0086

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    iput-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->FoldIndicator:Landroid/widget/ImageView;

    .line 239
    const v10, 0x7f0d010d

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->Summary:Landroid/widget/TextView;

    .line 240
    const v10, 0x7f0d0007

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    iput-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->Devider:Landroid/widget/ImageView;

    .line 241
    invoke-virtual {v6, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 247
    :goto_3
    if-eqz p2, :cond_d

    .line 248
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->Title:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f090017

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 249
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->FoldIndicator:Landroid/widget/ImageView;

    const v11, 0x7f020081

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 250
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->FoldIndicator:Landroid/widget/ImageView;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 251
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->FoldIndicator:Landroid/widget/ImageView;

    iget-object v11, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a001d

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->Devider:Landroid/widget/ImageView;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 253
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v10

    if-nez v10, :cond_c

    const/4 v10, 0x0

    invoke-static {v10}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v10

    if-nez v10, :cond_c

    .line 254
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v10

    if-nez v10, :cond_9

    .line 255
    const v10, 0x7f02030c

    invoke-virtual {v6, v10}, Landroid/view/View;->setBackgroundResource(I)V

    .line 268
    :cond_9
    :goto_4
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->Title:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(I)V

    .line 269
    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryId()I

    move-result v10

    if-lez v10, :cond_e

    .line 270
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->Summary:Landroid/widget/TextView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 273
    iget-object v10, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryId()I

    move-result v11

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryPs1Id()I

    move-result v12

    invoke-virtual {v3}, Lcom/samsung/helphub/headers/HelpHeader;->getSummaryPs2Id()I

    move-result v13

    invoke-direct {p0, v10, v11, v12, v13}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->makePSText(Landroid/content/Context;III)Ljava/lang/String;

    move-result-object v8

    .line 274
    .local v8, "summary":Ljava/lang/String;
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->Summary:Landroid/widget/TextView;

    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    .end local v8    # "summary":Ljava/lang/String;
    :goto_5
    iget-object v10, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 282
    .local v2, "currentActivityName":Ljava/lang/String;
    if-eqz v2, :cond_a

    const-string v10, "ExternalHelpActivity"

    invoke-virtual {v2, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_a

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->getGroupCount()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_a

    .line 283
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->FoldIndicator:Landroid/widget/ImageView;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_a
    move-object v7, v6

    .line 286
    .end local v6    # "result":Landroid/view/View;
    .restart local v7    # "result":Landroid/view/View;
    goto/16 :goto_0

    .line 243
    .end local v2    # "currentActivityName":Ljava/lang/String;
    .end local v7    # "result":Landroid/view/View;
    .restart local v6    # "result":Landroid/view/View;
    :cond_b
    move-object/from16 v6, p3

    .line 244
    invoke-virtual {v6}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "holder":Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;
    check-cast v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;

    .restart local v4    # "holder":Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;
    goto/16 :goto_3

    .line 257
    :cond_c
    const v10, 0x7f020314

    invoke-virtual {v6, v10}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_4

    .line 260
    :cond_d
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->Title:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f090016

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setTextColor(I)V

    .line 261
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->FoldIndicator:Landroid/widget/ImageView;

    const v11, 0x7f020080

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 262
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->FoldIndicator:Landroid/widget/ImageView;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 263
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->FoldIndicator:Landroid/widget/ImageView;

    iget-object v11, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a001c

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 264
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->Devider:Landroid/widget/ImageView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 265
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isCapuccino()Z

    move-result v10

    if-nez v10, :cond_9

    .line 266
    const v10, 0x7f02030c

    invoke-virtual {v6, v10}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_4

    .line 277
    :cond_e
    iget-object v10, v4, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter$GroupViewHolder;->Summary:Landroid/widget/TextView;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5

    .line 205
    :pswitch_data_0
    .packed-switch 0x7f0d00c7
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getSectionPositionForHeader(Lcom/samsung/helphub/headers/HelpHeader;)I
    .locals 6
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 352
    const/4 v3, -0x1

    .line 353
    .local v3, "result":I
    iget-object v4, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mGroupList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    .line 354
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 355
    iget-object v4, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mGroupList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    .line 356
    .local v2, "item":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 357
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 358
    move v3, v1

    .line 363
    .end local v2    # "item":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_0
    return v3

    .line 354
    .restart local v2    # "item":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getSectionPositionForName(Ljava/lang/String;)I
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 339
    const/4 v3, -0x1

    .line 340
    .local v3, "result":I
    iget-object v4, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mGroupList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    .line 341
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 342
    iget-object v4, p0, Lcom/samsung/helphub/fragments/SectionHeaderExpandableListAdapter;->mGroupList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/helphub/headers/HelpHeader;

    .line 343
    .local v2, "item":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 344
    move v3, v1

    .line 348
    .end local v2    # "item":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_0
    return v3

    .line 341
    .restart local v2    # "item":Lcom/samsung/helphub/headers/HelpHeader;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x1

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 319
    const/4 v0, 0x1

    return v0
.end method
