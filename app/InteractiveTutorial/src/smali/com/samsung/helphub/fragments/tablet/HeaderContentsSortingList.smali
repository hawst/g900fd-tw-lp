.class public Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;
.super Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;
.source "HeaderContentsSortingList.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final SECTION_HEADER_LOADER_ID:I


# instance fields
.field private mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

.field private mIsLoadedOnce:Z

.field private mList:Landroid/widget/ListView;

.field private mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mIsLoadedOnce:Z

    .line 105
    new-instance v0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList$1;-><init>(Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;)V

    iput-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 38
    return-void
.end method

.method private makeupInitialView()V
    .locals 1

    .prologue
    .line 101
    invoke-static {}, Lcom/samsung/helphub/HelpHubCommon;->isFullView()Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->doActiveSectionItem(I)V

    .line 103
    :cond_0
    return-void
.end method


# virtual methods
.method protected doActiveSectionItem(I)V
    .locals 11
    .param p1, "itemPosition"    # I

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const v2, 0x7f0a001e

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(I)V

    .line 121
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v7

    .line 122
    .local v7, "focusView":Landroid/view/View;
    if-eqz v7, :cond_0

    .line 123
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/inputmethod/InputMethodManager;

    .line 127
    .local v9, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v9, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v9, v0, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 132
    .end local v9    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/helphub/headers/HelpHeader;

    .line 133
    .local v8, "header":Lcom/samsung/helphub/headers/HelpHeader;
    const/4 v10, 0x0

    .line 135
    .local v10, "owner":Lcom/samsung/helphub/headers/HelpHeader;
    if-eqz v8, :cond_1

    .line 136
    invoke-virtual {v8}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/helphub/HelpHubCommon;->isGuidedTourCategory(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->setActiveItem(I)V

    .line 138
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    invoke-virtual {v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->notifyDataSetInvalidated()V

    .line 139
    invoke-virtual {p0, v8}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->openGuideTour(Lcom/samsung/helphub/headers/HelpHeader;)V

    .line 166
    :cond_1
    :goto_0
    return-void

    .line 140
    :cond_2
    invoke-virtual {v8}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v0

    const v2, 0x7f0d00a3

    if-ne v0, v2, :cond_3

    .line 143
    invoke-virtual {p0, v8}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->openOnlineHelp(Lcom/samsung/helphub/headers/HelpHeader;)V

    goto :goto_0

    .line 145
    :cond_3
    const/4 v6, 0x0

    .line 147
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "content://com.samsung.helphub.provider.search/header/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Lcom/samsung/helphub/headers/HelpHeader;->getOwnerResId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 149
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 151
    if-eqz v6, :cond_4

    .line 152
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 153
    invoke-static {v6}, Lcom/samsung/helphub/headers/HelpHeaderRequester;->convertToHelpHeader(Landroid/database/Cursor;)Lcom/samsung/helphub/headers/HelpHeader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 156
    :cond_4
    if-eqz v6, :cond_5

    .line 157
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 161
    :cond_5
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->setActiveItem(I)V

    .line 162
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    invoke-virtual {v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->notifyDataSetInvalidated()V

    .line 163
    invoke-virtual {p0, v10, v8}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->openHelpPage(Lcom/samsung/helphub/headers/HelpHeader;Lcom/samsung/helphub/headers/HelpHeader;)V

    goto :goto_0

    .line 156
    .end local v1    # "uri":Landroid/net/Uri;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_6

    .line 157
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0
.end method

.method public doActiveSectionItem(Lcom/samsung/helphub/headers/HelpHeader;)V
    .locals 3
    .param p1, "header"    # Lcom/samsung/helphub/headers/HelpHeader;

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/samsung/helphub/HelpHubCommon;->isResourceAvailable(Lcom/samsung/helphub/headers/HelpHeader;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 170
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    invoke-virtual {v1, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->getItemPosition(Lcom/samsung/helphub/headers/HelpHeader;)I

    move-result v0

    .line 172
    .local v0, "itemPosition":I
    if-ltz v0, :cond_0

    .line 173
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->doActiveSectionItem(I)V

    .line 174
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mList:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 179
    .end local v0    # "itemPosition":I
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/helphub/headers/HelpHeader;->getPluginAppId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/helphub/HelpHubCommon;->requestDownloadingResource(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 53
    invoke-super {p0, p1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->onActivityCreated(Landroid/os/Bundle;)V

    .line 55
    iget-boolean v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mIsLoadedOnce:Z

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v3, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 62
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v3, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 66
    const/4 v2, 0x0

    .line 67
    .local v2, "uri":Landroid/net/Uri;
    packed-switch p1, :pswitch_data_0

    .line 76
    :goto_0
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v3, Lcom/samsung/helphub/headers/HelpHeaderRequester;->PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 69
    :pswitch_0
    const-string v0, "content://com.samsung.helphub.provider.search/header/section"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 70
    goto :goto_0

    .line 67
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSearchableList;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 43
    const v1, 0x7f0400c2

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 44
    .local v0, "result":Landroid/view/View;
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mList:Landroid/widget/ListView;

    .line 45
    new-instance v1, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    invoke-virtual {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;-><init>(Landroid/content/Context;Z)V

    iput-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    .line 46
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 47
    iget-object v1, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 49
    return-object v0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 83
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    invoke-virtual {v0, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 84
    invoke-direct {p0}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->makeupInitialView()V

    .line 85
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 29
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mAdapter:Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingListAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/helphub/fragments/tablet/HeaderContentsSortingList;->mIsLoadedOnce:Z

    .line 93
    :cond_0
    return-void
.end method
