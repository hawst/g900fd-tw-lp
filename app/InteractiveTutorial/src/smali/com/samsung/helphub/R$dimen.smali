.class public final Lcom/samsung/helphub/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final category_item:I = 0x7f0b0031

.field public static final data_dialog_text_min_height:I = 0x7f0b0028

.field public static final dialog_bottom_separator_height:I = 0x7f0b0021

.field public static final dialog_button_height:I = 0x7f0b0024

.field public static final dialog_button_margin:I = 0x7f0b0023

.field public static final dialog_button_text_size:I = 0x7f0b0022

.field public static final dialog_header_text_height:I = 0x7f0b002b

.field public static final dialog_header_text_size:I = 0x7f0b002a

.field public static final dialog_main_text_top_padding:I = 0x7f0b0026

.field public static final dialog_text_side_padding:I = 0x7f0b0027

.field public static final dialog_text_size:I = 0x7f0b0025

.field public static final directpeninput_filling_in_forms_image_height:I = 0x7f0b0136

.field public static final directpeninput_filling_in_forms_image_width:I = 0x7f0b0135

.field public static final gesture_guide_description_font_size:I = 0x7f0b0142

.field public static final gesture_guide_description_layout_height:I = 0x7f0b013d

.field public static final gesture_guide_description_line_space:I = 0x7f0b0143

.field public static final gesture_guide_description_margin_bottom:I = 0x7f0b013f

.field public static final gesture_guide_description_margin_left:I = 0x7f0b0140

.field public static final gesture_guide_description_margin_right:I = 0x7f0b0141

.field public static final gesture_guide_description_margin_top:I = 0x7f0b013e

.field public static final gesture_guide_list_item_image_margin_bottom:I = 0x7f0b0151

.field public static final gesture_guide_list_item_image_margin_left:I = 0x7f0b0152

.field public static final gesture_guide_list_item_image_margin_right:I = 0x7f0b0153

.field public static final gesture_guide_list_item_image_margin_top:I = 0x7f0b0150

.field public static final gesture_guide_list_item_layout_margin_bottom:I = 0x7f0b0145

.field public static final gesture_guide_list_item_layout_margin_left:I = 0x7f0b0146

.field public static final gesture_guide_list_item_layout_margin_right:I = 0x7f0b0147

.field public static final gesture_guide_list_item_layout_margin_top:I = 0x7f0b0144

.field public static final gesture_guide_list_item_text_font_size:I = 0x7f0b0149

.field public static final gesture_guide_list_item_text_margin_bottom:I = 0x7f0b014b

.field public static final gesture_guide_list_item_text_margin_left:I = 0x7f0b014c

.field public static final gesture_guide_list_item_text_margin_right:I = 0x7f0b014d

.field public static final gesture_guide_list_item_text_margin_top:I = 0x7f0b014a

.field public static final gesture_guide_list_item_text_padding_end:I = 0x7f0b014f

.field public static final gesture_guide_list_item_text_padding_start:I = 0x7f0b014e

.field public static final gesture_guide_list_item_text_width:I = 0x7f0b0148

.field public static final gesture_guide_title_font_size:I = 0x7f0b013c

.field public static final gesture_guide_title_layout_height:I = 0x7f0b013b

.field public static final help_keyboard_list_body_text_size:I = 0x7f0b0104

.field public static final help_keyboard_list_extra_line_spacing:I = 0x7f0b010e

.field public static final help_keyboard_list_img_margin_bottom:I = 0x7f0b010d

.field public static final help_keyboard_list_img_margin_top:I = 0x7f0b010c

.field public static final help_keyboard_list_text_left_align_margin:I = 0x7f0b0115

.field public static final help_keyboard_list_text_margin_bottom:I = 0x7f0b0108

.field public static final help_keyboard_list_text_margin_left:I = 0x7f0b0109

.field public static final help_keyboard_list_text_margin_right:I = 0x7f0b010a

.field public static final help_keyboard_list_text_number_margin_right:I = 0x7f0b010b

.field public static final help_keyboard_list_tip_body_margin_common:I = 0x7f0b0112

.field public static final help_keyboard_list_tip_body_margin_left:I = 0x7f0b0110

.field public static final help_keyboard_list_tip_body_margin_right:I = 0x7f0b0111

.field public static final help_keyboard_list_tip_margin_common:I = 0x7f0b010f

.field public static final help_keyboard_list_tip_title_margin_left:I = 0x7f0b0113

.field public static final help_keyboard_list_tip_title_margin_right:I = 0x7f0b0114

.field public static final help_keyboard_list_tip_title_text_size:I = 0x7f0b0105

.field public static final help_keyboard_list_title_margin_bottom:I = 0x7f0b0107

.field public static final help_keyboard_list_title_margin_top:I = 0x7f0b0106

.field public static final help_keyboard_list_title_text_size:I = 0x7f0b0103

.field public static final help_page_fragment_right_side_padding:I = 0x7f0b012a

.field public static final help_page_fragment_side_padding:I = 0x7f0b0018

.field public static final help_settings_button_height:I = 0x7f0b0016

.field public static final help_use_samsung_keyboard_4th_image_padding:I = 0x7f0b00ed

.field public static final help_use_samsung_keyboard_5th_image_padding:I = 0x7f0b00ee

.field public static final help_use_samsung_keyboard_6th_image_bottom_padding:I = 0x7f0b00f0

.field public static final help_use_samsung_keyboard_6th_image_top_padding:I = 0x7f0b00ef

.field public static final help_use_samsung_keyboard_8th_image_bottom_padding:I = 0x7f0b00f2

.field public static final help_use_samsung_keyboard_8th_image_top_padding:I = 0x7f0b00f1

.field public static final help_use_samsung_keyboard_9th_image_bottom_padding:I = 0x7f0b00f4

.field public static final help_use_samsung_keyboard_9th_image_top_padding:I = 0x7f0b00f3

.field public static final help_use_samsung_keyboard_bold_text_size:I = 0x7f0b00f7

.field public static final help_use_samsung_keyboard_bottom_padding:I = 0x7f0b00f8

.field public static final help_use_samsung_keyboard_first_image_bottom_padding:I = 0x7f0b00ea

.field public static final help_use_samsung_keyboard_first_image_top_padding:I = 0x7f0b00e9

.field public static final help_use_samsung_keyboard_last_image_bottom_padding:I = 0x7f0b00f6

.field public static final help_use_samsung_keyboard_last_image_top_padding:I = 0x7f0b00f5

.field public static final help_use_samsung_keyboard_second_image_bottom_padding:I = 0x7f0b00ec

.field public static final help_use_samsung_keyboard_second_image_top_padding:I = 0x7f0b00eb

.field public static final helphub_content_image_height:I = 0x7f0b0008

.field public static final helphub_content_image_paddingBottom:I = 0x7f0b0005

.field public static final helphub_content_image_width:I = 0x7f0b0009

.field public static final helphub_content_item_height:I = 0x7f0b0007

.field public static final helphub_content_item_width:I = 0x7f0b0006

.field public static final helphub_content_tab_hspace:I = 0x7f0b0003

.field public static final helphub_content_tab_vspace:I = 0x7f0b0004

.field public static final helphub_content_tab_width:I = 0x7f0b0002

.field public static final helphub_detail_list_divider_height:I = 0x7f0b006c

.field public static final helphub_details_sections_margin:I = 0x7f0b0085

.field public static final helphub_expandable_child_divider_bottom_margin:I = 0x7f0b006a

.field public static final helphub_expandable_child_divider_top_margin:I = 0x7f0b006b

.field public static final helphub_expandable_list_child_text_img_height:I = 0x7f0b007f

.field public static final helphub_expandable_list_child_text_img_width:I = 0x7f0b0080

.field public static final helphub_expandable_list_child_text_margin_left:I = 0x7f0b004e

.field public static final helphub_expandable_list_child_text_margin_right:I = 0x7f0b004f

.field public static final helphub_expandable_list_child_text_padding_bottom:I = 0x7f0b0055

.field public static final helphub_expandable_list_child_text_padding_top:I = 0x7f0b0054

.field public static final helphub_expandable_list_divider_height:I = 0x7f0b0062

.field public static final helphub_expandable_list_divider_margin_side:I = 0x7f0b0063

.field public static final helphub_expandable_list_item_img_margin_bottom:I = 0x7f0b0053

.field public static final helphub_expandable_list_item_img_margin_top:I = 0x7f0b0052

.field public static final helphub_expandable_list_item_padding_bottom:I = 0x7f0b0067

.field public static final helphub_expandable_list_item_padding_side:I = 0x7f0b0068

.field public static final helphub_expandable_list_item_padding_top:I = 0x7f0b0066

.field public static final helphub_expandable_list_shadow_divider_height:I = 0x7f0b0064

.field public static final helphub_expandable_list_shadow_divider_margin:I = 0x7f0b0065

.field public static final helphub_expandable_list_title_fold_margin_left:I = 0x7f0b0057

.field public static final helphub_expandable_list_title_fold_margin_right:I = 0x7f0b0058

.field public static final helphub_expandable_list_title_icon_margin_left:I = 0x7f0b001a

.field public static final helphub_expandable_list_title_icon_margin_right:I = 0x7f0b001b

.field public static final helphub_expandable_list_title_icon_padding:I = 0x7f0b0056

.field public static final helphub_expandable_list_title_margin_bottom:I = 0x7f0b0046

.field public static final helphub_expandable_list_title_margin_left:I = 0x7f0b0047

.field public static final helphub_expandable_list_title_margin_right:I = 0x7f0b0048

.field public static final helphub_expandable_list_title_margin_top:I = 0x7f0b0045

.field public static final helphub_expandable_list_title_text_size:I = 0x7f0b0049

.field public static final helphub_glossary_item_height:I = 0x7f0b001e

.field public static final helphub_glossary_item_padding_bottom:I = 0x7f0b001d

.field public static final helphub_glossary_item_padding_top:I = 0x7f0b001c

.field public static final helphub_help_list_item_big_image_y_diff:I = 0x7f0b0083

.field public static final helphub_help_list_item_extra_line_spacing:I = 0x7f0b004d

.field public static final helphub_help_list_item_image_no_scale:I = 0x7f0b0019

.field public static final helphub_help_list_item_image_no_scale_width:I = 0x7f0b0081

.field public static final helphub_help_list_item_image_no_scale_width_Camera:I = 0x7f0b0097

.field public static final helphub_help_list_item_image_no_y_diff:I = 0x7f0b0082

.field public static final helphub_help_list_item_imagebookmark_no_scale_height:I = 0x7f0b00e3

.field public static final helphub_help_list_item_imagebookmark_no_scale_width:I = 0x7f0b00e4

.field public static final helphub_help_list_item_in_gray_text_size:I = 0x7f0b004c

.field public static final helphub_help_list_item_number_margin_left:I = 0x7f0b0050

.field public static final helphub_help_list_item_number_margin_right:I = 0x7f0b0051

.field public static final helphub_help_list_item_text_size:I = 0x7f0b004a

.field public static final helphub_help_list_sub_title_text_size:I = 0x7f0b004b

.field public static final helphub_help_settings_button_text_margin_left:I = 0x7f0b005b

.field public static final helphub_help_settings_button_text_size:I = 0x7f0b0059

.field public static final helphub_homescreen_navigate_homescreen_padding_bottom:I = 0x7f0b00d4

.field public static final helphub_icon_in_text_offset_1_line:I = 0x7f0b00b8

.field public static final helphub_icon_in_text_offset_extra_font_7_step:I = 0x7f0b00b2

.field public static final helphub_icon_in_text_offset_huge_font:I = 0x7f0b0094

.field public static final helphub_icon_in_text_offset_huge_font_7_step:I = 0x7f0b00b1

.field public static final helphub_icon_in_text_offset_large_font_7_step:I = 0x7f0b00b4

.field public static final helphub_icon_in_text_offset_norm_font:I = 0x7f0b0095

.field public static final helphub_icon_in_text_offset_normal_font_7_step:I = 0x7f0b00b5

.field public static final helphub_icon_in_text_offset_small_font_7_step:I = 0x7f0b00b6

.field public static final helphub_icon_in_text_offset_tiny_font:I = 0x7f0b0096

.field public static final helphub_icon_in_text_offset_tiny_font_7_step:I = 0x7f0b00b7

.field public static final helphub_icon_in_text_offset_very_font_7_step:I = 0x7f0b00b3

.field public static final helphub_item_in_highlighted_background_height:I = 0x7f0b0086

.field public static final helphub_learn_item_fold_image_marginRight:I = 0x7f0b000b

.field public static final helphub_learn_item_fold_image_width:I = 0x7f0b000a

.field public static final helphub_main_screen_grid_item_padding_bottom:I = 0x7f0b0014

.field public static final helphub_main_screen_grid_item_padding_top:I = 0x7f0b0015

.field public static final helphub_main_screen_grid_padding_bottom:I = 0x7f0b0013

.field public static final helphub_main_screen_grid_padding_top:I = 0x7f0b0012

.field public static final helphub_main_screen_grid_side_padding:I = 0x7f0b0011

.field public static final helphub_main_screen_group_caption_padding_bottom:I = 0x7f0b005d

.field public static final helphub_main_screen_group_caption_padding_left:I = 0x7f0b005e

.field public static final helphub_main_screen_group_caption_padding_top:I = 0x7f0b005c

.field public static final helphub_main_screen_group_caption_text_size:I = 0x7f0b005f

.field public static final helphub_main_screen_section_height:I = 0x7f0b005a

.field public static final helphub_main_window_item_height:I = 0x7f0b000d

.field public static final helphub_main_window_item_text_size:I = 0x7f0b000c

.field public static final helphub_main_window_list_header_height:I = 0x7f0b0010

.field public static final helphub_main_window_list_top_margin:I = 0x7f0b0123

.field public static final helphub_main_window_more_text_size:I = 0x7f0b000f

.field public static final helphub_main_window_title_text_image_paddingLeft:I = 0x7f0b0001

.field public static final helphub_main_window_title_text_image_paddingTop:I = 0x7f0b0084

.field public static final helphub_main_window_title_text_paddingLeft:I = 0x7f0b0000

.field public static final helphub_main_window_title_text_size:I = 0x7f0b000e

.field public static final helphub_numbered_list_padding_bottom:I = 0x7f0b0061

.field public static final helphub_numbered_list_padding_top:I = 0x7f0b0060

.field public static final helphub_overlays_list_item_checkbox_margin_left:I = 0x7f0b007d

.field public static final helphub_overlays_list_item_checkbox_margin_right:I = 0x7f0b007e

.field public static final helphub_page_margin_top_has_title:I = 0x7f0b0030

.field public static final helphub_page_margin_top_without_title:I = 0x7f0b002f

.field public static final helphub_start_guide_button_height:I = 0x7f0b0073

.field public static final helphub_start_guide_button_height_2014:I = 0x7f0b0075

.field public static final helphub_start_guide_button_margin_bottom:I = 0x7f0b007a

.field public static final helphub_start_guide_button_margin_bottom_2014:I = 0x7f0b007b

.field public static final helphub_start_guide_button_margin_left:I = 0x7f0b0077

.field public static final helphub_start_guide_button_margin_right:I = 0x7f0b0078

.field public static final helphub_start_guide_button_margin_top:I = 0x7f0b0079

.field public static final helphub_start_guide_button_text_size:I = 0x7f0b007c

.field public static final helphub_start_guide_button_width:I = 0x7f0b0074

.field public static final helphub_start_guide_button_width_2014:I = 0x7f0b0076

.field public static final helphub_widget_framelayout_leftpadding:I = 0x7f0b00e8

.field public static final helphub_widget_textview_leftpadding:I = 0x7f0b00e6

.field public static final helphub_widget_textview_textsize:I = 0x7f0b00e5

.field public static final helphub_widget_textview_toppadding:I = 0x7f0b00e7

.field public static final helppage_bullet_margin_left:I = 0x7f0b008c

.field public static final helppage_change_wallpaper_tipbox_textview_margin_top:I = 0x7f0b00a8

.field public static final helppage_divider_layout_margin_bottom:I = 0x7f0b013a

.field public static final helppage_divider_layout_margin_top:I = 0x7f0b0139

.field public static final helppage_handwriting_list_title_margin_bottom:I = 0x7f0b00ff

.field public static final helppage_icon_image_margin_bottom:I = 0x7f0b0090

.field public static final helppage_icon_text_size:I = 0x7f0b0091

.field public static final helppage_icons_layout_width:I = 0x7f0b0087

.field public static final helppage_icons_margin_bottom:I = 0x7f0b008e

.field public static final helppage_icons_margin_right:I = 0x7f0b008f

.field public static final helppage_icons_margin_top:I = 0x7f0b008d

.field public static final helppage_image_margin:I = 0x7f0b012c

.field public static final helppage_image_margin_bottom_pen_select:I = 0x7f0b00d0

.field public static final helppage_image_margin_left:I = 0x7f0b00d2

.field public static final helppage_image_margin_right:I = 0x7f0b00d3

.field public static final helppage_image_margin_top:I = 0x7f0b00d1

.field public static final helppage_image_margin_top_pen_select:I = 0x7f0b00cf

.field public static final helppage_last_line_margin_bottom:I = 0x7f0b012d

.field public static final helppage_layout_margin_bottom:I = 0x7f0b00a1

.field public static final helppage_layout_margin_top:I = 0x7f0b00a0

.field public static final helppage_layout_padding_bottom:I = 0x7f0b009d

.field public static final helppage_layout_padding_left:I = 0x7f0b009e

.field public static final helppage_layout_padding_right:I = 0x7f0b009f

.field public static final helppage_layout_padding_top:I = 0x7f0b009c

.field public static final helppage_list_title_margin_bottom:I = 0x7f0b012b

.field public static final helppage_number_textview_margin_right:I = 0x7f0b009a

.field public static final helppage_textview_margin_left:I = 0x7f0b0098

.field public static final helppage_textview_margin_right:I = 0x7f0b0099

.field public static final helppage_textview_textsize:I = 0x7f0b009b

.field public static final helppage_tipbox_helphub_textview_margin_left:I = 0x7f0b00af

.field public static final helppage_tipbox_helphub_textview_margin_right:I = 0x7f0b00b0

.field public static final helppage_tipbox_inner_layout_margin_left:I = 0x7f0b00ad

.field public static final helppage_tipbox_inner_layout_margin_right:I = 0x7f0b00ae

.field public static final helppage_tipbox_margin_bottom:I = 0x7f0b00a4

.field public static final helppage_tipbox_margin_left:I = 0x7f0b00a2

.field public static final helppage_tipbox_margin_right:I = 0x7f0b00a3

.field public static final helppage_tipbox_padding:I = 0x7f0b00a5

.field public static final helppage_tipbox_textview_margin_bottom:I = 0x7f0b00a7

.field public static final helppage_tipbox_textview_margin_left:I = 0x7f0b00a9

.field public static final helppage_tipbox_textview_margin_top:I = 0x7f0b00a6

.field public static final helppage_tipbox_textview_padding_left:I = 0x7f0b00aa

.field public static final helppage_tipbox_textview_padding_right:I = 0x7f0b00ab

.field public static final helppage_tipbox_textview_textsize:I = 0x7f0b00ac

.field public static final helppage_title_margin_bottom:I = 0x7f0b0089

.field public static final helppage_title_margin_left:I = 0x7f0b008a

.field public static final helppage_title_margin_right:I = 0x7f0b008b

.field public static final helppage_title_margin_top:I = 0x7f0b0088

.field public static final helppage_title_separator_line_margin_top:I = 0x7f0b0093

.field public static final helppage_title_separator_line_margin_top_with_tipbox:I = 0x7f0b0092

.field public static final helppage_writingbuddy_image_margin_bottom:I = 0x7f0b0134

.field public static final helppage_writingbuddy_image_margin_bottom_narrow:I = 0x7f0b0133

.field public static final helppage_writingbuddy_list_title_margin_bottom:I = 0x7f0b0132

.field public static final keyboard_handwriting_arrow_margin:I = 0x7f0b012f

.field public static final keyboard_handwriting_gesture_02_top_margin:I = 0x7f0b00fd

.field public static final keyboard_handwriting_gesture_04_bottom_margin:I = 0x7f0b00fe

.field public static final keyboard_handwriting_gesture_default_bottom_margin:I = 0x7f0b00fc

.field public static final keyboard_handwriting_gesture_default_margin:I = 0x7f0b00fa

.field public static final keyboard_handwriting_gesture_default_top_margin:I = 0x7f0b00fb

.field public static final keyboard_handwriting_gesture_enter_label_size:I = 0x7f0b00d6

.field public static final keyboard_handwriting_gesture_enter_layout_height:I = 0x7f0b0101

.field public static final keyboard_handwriting_gesture_enter_layout_margin_right:I = 0x7f0b012e

.field public static final keyboard_handwriting_gesture_enter_layout_width:I = 0x7f0b0100

.field public static final keyboard_handwriting_gesture_enter_left_padding:I = 0x7f0b00d7

.field public static final keyboard_handwriting_gesture_enter_stroke_gesture_padding_start:I = 0x7f0b00d8

.field public static final keyboard_handwriting_gesture_enter_stroke_text_size:I = 0x7f0b0102

.field public static final keyboard_handwriting_gesture_zero_margin:I = 0x7f0b00f9

.field public static final keyboard_handwriting_image_margin:I = 0x7f0b0130

.field public static final keyboard_handwriting_layout_margin_bottom:I = 0x7f0b00de

.field public static final keyboard_handwriting_support_word_left_margin_end:I = 0x7f0b00dd

.field public static final keyboard_handwriting_support_word_left_width:I = 0x7f0b00dc

.field public static final keyboard_handwriting_textview_width:I = 0x7f0b0131

.field public static final keyboard_handwriting_write_next_arrow_margin_end:I = 0x7f0b00d9

.field public static final keyboard_handwriting_write_next_arrow_margin_start:I = 0x7f0b00da

.field public static final keyboard_handwriting_write_parallel_left_margin_end:I = 0x7f0b00db

.field public static final kg_help_text_add_msg_height_1:I = 0x7f0b0127

.field public static final kg_help_text_margin_top:I = 0x7f0b0125

.field public static final kg_help_text_tail_add_margin_top_1:I = 0x7f0b0124

.field public static final kg_help_text_tail_margin_top:I = 0x7f0b0126

.field public static final kg_help_text_tail_unlock_margin_top_1:I = 0x7f0b0129

.field public static final kg_help_text_unlock_msg_height_1:I = 0x7f0b0128

.field public static final left_panel_width:I = 0x7f0b0116

.field public static final list_category_item_height:I = 0x7f0b0032

.field public static final list_category_item_height_adjust:I = 0x7f0b0034

.field public static final list_category_item_height_huge:I = 0x7f0b0033

.field public static final list_categroy_headers_item_icon_marginLeft:I = 0x7f0b0036

.field public static final list_categroy_headers_item_title:I = 0x7f0b0035

.field public static final list_categroy_headers_item_title_marginLeft:I = 0x7f0b0037

.field public static final list_divider_margin_bottom_adjust:I = 0x7f0b003d

.field public static final list_search_first_item_height_adjust:I = 0x7f0b0038

.field public static final list_search_item_height:I = 0x7f0b0039

.field public static final list_search_item_height_huge:I = 0x7f0b003a

.field public static final list_section_child_headers_item_text_size:I = 0x7f0b0044

.field public static final list_section_divider_height:I = 0x7f0b0041

.field public static final list_section_expandable_child_divider_margin:I = 0x7f0b0069

.field public static final list_section_expandable_child_item_height:I = 0x7f0b003f

.field public static final list_section_headers_item_summary:I = 0x7f0b0043

.field public static final list_section_headers_item_title:I = 0x7f0b0042

.field public static final list_section_item_height:I = 0x7f0b003e

.field public static final list_section_margin_top:I = 0x7f0b0040

.field public static final list_side_margin:I = 0x7f0b002e

.field public static final main_list_item_height:I = 0x7f0b0017

.field public static final multiwindow_app_default_height:I = 0x7f0b011f

.field public static final multiwindow_app_default_width:I = 0x7f0b011e

.field public static final multiwindow_app_minimum_height:I = 0x7f0b0121

.field public static final multiwindow_app_minimum_width:I = 0x7f0b0120

.field public static final multiwindow_splitbar_default_width:I = 0x7f0b0122

.field public static final right_panel_width:I = 0x7f0b0117

.field public static final search_button_margin_top_adjust:I = 0x7f0b003b

.field public static final search_button_margin_top_huge_adjust:I = 0x7f0b003c

.field public static final split_bar_margin_left:I = 0x7f0b011c

.field public static final split_bar_margin_right:I = 0x7f0b011d

.field public static final split_bar_width:I = 0x7f0b011b

.field public static final split_view_left_panel_default_width:I = 0x7f0b0118

.field public static final split_view_left_panel_min_width:I = 0x7f0b0119

.field public static final split_view_right_panel_min_width:I = 0x7f0b011a

.field public static final subtitle_height:I = 0x7f0b006d

.field public static final subtitle_margin_bottom:I = 0x7f0b0071

.field public static final subtitle_margin_side:I = 0x7f0b006f

.field public static final subtitle_margin_top:I = 0x7f0b0070

.field public static final subtitle_padding_left_side:I = 0x7f0b0072

.field public static final subtitle_text_size:I = 0x7f0b006e

.field public static final try_multi_window_view_title_page_image_size:I = 0x7f0b00df

.field public static final try_multi_window_view_title_page_left_margin:I = 0x7f0b00e2

.field public static final try_multi_window_view_title_page_string_leftMargin:I = 0x7f0b00e1

.field public static final try_multi_window_view_title_page_string_size:I = 0x7f0b00e0

.field public static final tutorial_drop_down_anim_image1_top_margin:I = 0x7f0b00cc

.field public static final tutorial_drop_down_anim_image2_top_margin:I = 0x7f0b00cd

.field public static final tutorial_drop_down_anim_image3_top_margin:I = 0x7f0b00ce

.field public static final tutorial_drop_down_anim_left_margin:I = 0x7f0b00c1

.field public static final tutorial_drop_down_anim_right_margin:I = 0x7f0b00c2

.field public static final tutorial_drop_down_anim_top_margin:I = 0x7f0b00bd

.field public static final tutorial_expand_notification_drop_down_anim_layout_top_margin:I = 0x7f0b00c4

.field public static final tutorial_expand_notification_land_drop_down_anim_layout_top_margin:I = 0x7f0b00c7

.field public static final tutorial_expand_notification_land_top_margin:I = 0x7f0b00c6

.field public static final tutorial_expand_notification_top_margin:I = 0x7f0b00c3

.field public static final tutorial_header_height:I = 0x7f0b00b9

.field public static final tutorial_header_plus_handler_height:I = 0x7f0b00ba

.field public static final tutorial_popup_body_and_anim_gap:I = 0x7f0b00bb

.field public static final tutorial_popup_body_and_top_tail_gap:I = 0x7f0b00bc

.field public static final tutorial_status_bar_height:I = 0x7f0b00cb

.field public static final tutorial_tip_expand_notification_land_top_margin:I = 0x7f0b00c8

.field public static final tutorial_tip_expand_notification_msg_tail_top_margin:I = 0x7f0b00ca

.field public static final tutorial_tip_expand_notification_top_margin:I = 0x7f0b00c5

.field public static final tutorial_tip_land_expand_notification_msg_tail_top_margin:I = 0x7f0b00c9

.field public static final tutorial_tip_left_margin:I = 0x7f0b00bf

.field public static final tutorial_tip_right_margin:I = 0x7f0b00be

.field public static final tutorial_tip_top_margin:I = 0x7f0b00c0

.field public static final welcome_text_greeting:I = 0x7f0b002c

.field public static final welcome_text_summary:I = 0x7f0b002d

.field public static final wifi_dialog_min_height:I = 0x7f0b001f

.field public static final wifi_dialog_text_min_height:I = 0x7f0b0029

.field public static final wifi_dialog_width:I = 0x7f0b0020

.field public static final writing_buddy_fill_in_form_image_margin:I = 0x7f0b0138

.field public static final writing_buddy_fill_in_form_image_top_margin:I = 0x7f0b0137

.field public static final writing_buddy_gesture_description_top:I = 0x7f0b00d5


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
