.class Lcom/samsung/helphub/widget/SplitViewContainer$1;
.super Ljava/lang/Object;
.source "SplitViewContainer.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/widget/SplitViewContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/widget/SplitViewContainer;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/widget/SplitViewContainer;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/samsung/helphub/widget/SplitViewContainer$1;->this$0:Lcom/samsung/helphub/widget/SplitViewContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    .line 291
    :try_start_0
    iget-object v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer$1;->this$0:Lcom/samsung/helphub/widget/SplitViewContainer;

    # getter for: Lcom/samsung/helphub/widget/SplitViewContainer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/helphub/widget/SplitViewContainer;->access$000(Lcom/samsung/helphub/widget/SplitViewContainer;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/samsung/helphub/HelpHubCommon;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 292
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_1

    .line 293
    const/4 v1, 0x4

    const/4 v2, -0x1

    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V

    .line 301
    :cond_0
    :goto_0
    return v3

    .line 294
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    .line 295
    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 298
    :catch_0
    move-exception v0

    .line 299
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
