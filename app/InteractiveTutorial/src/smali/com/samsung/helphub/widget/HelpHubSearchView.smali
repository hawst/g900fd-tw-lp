.class public Lcom/samsung/helphub/widget/HelpHubSearchView;
.super Landroid/widget/SearchView;
.source "HelpHubSearchView.java"


# instance fields
.field mAutoCompleteTextView:Landroid/widget/AutoCompleteTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/SearchView;-><init>(Landroid/content/Context;)V

    .line 19
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubSearchView;->init()V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/SearchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubSearchView;->init()V

    .line 25
    return-void
.end method

.method private findAutoCompleteTextView()Landroid/widget/AutoCompleteTextView;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 47
    const/4 v0, 0x0

    .line 48
    .local v0, "result":Landroid/widget/AutoCompleteTextView;
    invoke-virtual {p0, v4}, Lcom/samsung/helphub/widget/HelpHubSearchView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 50
    .local v1, "viewGroup":Landroid/view/ViewGroup;
    if-eqz v1, :cond_0

    .line 51
    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "result":Landroid/widget/AutoCompleteTextView;
    check-cast v0, Landroid/widget/AutoCompleteTextView;

    .line 52
    .restart local v0    # "result":Landroid/widget/AutoCompleteTextView;
    :cond_0
    return-object v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubSearchView;->findAutoCompleteTextView()Landroid/widget/AutoCompleteTextView;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpHubSearchView;->mAutoCompleteTextView:Landroid/widget/AutoCompleteTextView;

    .line 31
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubSearchView;->mAutoCompleteTextView:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubSearchView;->mAutoCompleteTextView:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/samsung/helphub/widget/HelpHubSearchView$1;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/widget/HelpHubSearchView$1;-><init>(Lcom/samsung/helphub/widget/HelpHubSearchView;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 44
    :cond_0
    return-void
.end method


# virtual methods
.method public forceSuggestion()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubSearchView;->mAutoCompleteTextView:Landroid/widget/AutoCompleteTextView;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubSearchView;->mAutoCompleteTextView:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->showDropDown()V

    .line 59
    :cond_0
    return-void
.end method
