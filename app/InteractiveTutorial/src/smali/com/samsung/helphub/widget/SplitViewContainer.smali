.class public Lcom/samsung/helphub/widget/SplitViewContainer;
.super Landroid/widget/LinearLayout;
.source "SplitViewContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/widget/SplitViewContainer$SplitBarStateChangeListener;
    }
.end annotation


# static fields
.field private static final LEFT_PANEL_MIN_WIDTH_RATIO:F = 0.1f

.field private static final LEFT_PANEL_MIN_WIDTH_RATIO_PORTRAIT:F = 0.16f

.field private static final PRESS_RANGE:I = 0x14

.field private static final RIGHT_PANEL_MIN_WIDTH_RATIO:F = 0.34f

.field private static final SCREEN_ROTATION_RATIO:F = 0.75f

.field private static final SPLIT_BAR_PRESS_STATE_CHANGED:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SplitViewContainer"

.field private static sLastLeftPanelRatio:F


# instance fields
.field private mCheckFirstTimeRotation:I

.field private mContext:Landroid/content/Context;

.field private mDragStarted:Z

.field private mLeftPanel:Landroid/view/ViewGroup;

.field private mLeftPanelDefaultWidth:I

.field private mLeftPanelMinWidth:I

.field private mLeftPanelRatio:F

.field private mRightPanelMinWidth:I

.field private mSplitBar:Landroid/view/View;

.field private mSplitBarHoverListener:Landroid/view/View$OnHoverListener;

.field private mSplitBarRect:Landroid/graphics/Rect;

.field private mSplitBarStateChangeListenerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/helphub/widget/SplitViewContainer$SplitBarStateChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mViewWidth:I

.field private modifyX:I

.field private splitBarRatio:F

.field private splitBarRatioLand:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    sput v0, Lcom/samsung/helphub/widget/SplitViewContainer;->sLastLeftPanelRatio:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    iput v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->modifyX:I

    .line 44
    const v0, 0x3ecccccd    # 0.4f

    iput v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelRatio:F

    .line 45
    iput v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mCheckFirstTimeRotation:I

    .line 286
    new-instance v0, Lcom/samsung/helphub/widget/SplitViewContainer$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/widget/SplitViewContainer$1;-><init>(Lcom/samsung/helphub/widget/SplitViewContainer;)V

    iput-object v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarHoverListener:Landroid/view/View$OnHoverListener;

    .line 53
    iput-object p1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mContext:Landroid/content/Context;

    .line 54
    invoke-direct {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->init()V

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/widget/SplitViewContainer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/widget/SplitViewContainer;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private init()V
    .locals 3

    .prologue
    const/high16 v2, 0x42c80000    # 100.0f

    .line 88
    iget-object v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 89
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelRatio:F

    .line 97
    :cond_0
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarStateChangeListenerList:Ljava/util/ArrayList;

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mDragStarted:Z

    .line 99
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarRect:Landroid/graphics/Rect;

    .line 100
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0118

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelDefaultWidth:I

    .line 102
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0119

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelMinWidth:I

    .line 104
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b011a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mRightPanelMinWidth:I

    .line 106
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/helphub/utility/PreferenceHelper;->getLeftPanelRatio(Landroid/content/Context;)F

    move-result v0

    sput v0, Lcom/samsung/helphub/widget/SplitViewContainer;->sLastLeftPanelRatio:F

    .line 107
    sget v0, Lcom/samsung/helphub/widget/SplitViewContainer;->sLastLeftPanelRatio:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 112
    sget v0, Lcom/samsung/helphub/widget/SplitViewContainer;->sLastLeftPanelRatio:F

    iput v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelRatio:F

    .line 115
    :cond_1
    return-void

    .line 90
    :cond_2
    const-string v0, "ro.build.scafe"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "latte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ro.build.scafe.shot"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "half"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 92
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0c0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelRatio:F

    goto/16 :goto_0

    .line 94
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    iput v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelRatio:F

    goto/16 :goto_0
.end method

.method private notifyToListener(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 275
    iget-object v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarStateChangeListenerList:Ljava/util/ArrayList;

    monitor-enter v3

    .line 276
    packed-switch p1, :pswitch_data_0

    .line 283
    :cond_0
    :try_start_0
    monitor-exit v3

    .line 284
    return-void

    .line 278
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarStateChangeListenerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/widget/SplitViewContainer$SplitBarStateChangeListener;

    .line 279
    .local v1, "listener":Lcom/samsung/helphub/widget/SplitViewContainer$SplitBarStateChangeListener;
    iget-boolean v2, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mDragStarted:Z

    invoke-interface {v1, v2}, Lcom/samsung/helphub/widget/SplitViewContainer$SplitBarStateChangeListener;->onSplitBarPressed(Z)V

    goto :goto_0

    .line 283
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/samsung/helphub/widget/SplitViewContainer$SplitBarStateChangeListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 276
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public addSplitBarStateChangeListener(Lcom/samsung/helphub/widget/SplitViewContainer$SplitBarStateChangeListener;)Z
    .locals 4
    .param p1, "l"    # Lcom/samsung/helphub/widget/SplitViewContainer$SplitBarStateChangeListener;

    .prologue
    .line 255
    iget-object v2, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarStateChangeListenerList:Ljava/util/ArrayList;

    monitor-enter v2

    .line 256
    :try_start_0
    iget-object v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarStateChangeListenerList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 257
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Split bar state observer is already added : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/helphub/HelpHubCommon;->LogD(Ljava/lang/String;)V

    .line 258
    const/4 v0, 0x0

    monitor-exit v2

    .line 263
    :goto_0
    return v0

    .line 260
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarStateChangeListenerList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 261
    .local v0, "ret":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Splitbar state change listener( "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ) is added. total size = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarStateChangeListenerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/helphub/HelpHubCommon;->LogD(Ljava/lang/String;)V

    .line 263
    monitor-exit v2

    goto :goto_0

    .line 265
    .end local v0    # "ret":Z
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 175
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    if-ne v7, v5, :cond_1

    .line 181
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v4, v7

    .line 182
    .local v4, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 183
    .local v0, "action":I
    iget-object v7, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v7

    if-nez v7, :cond_1

    iget-object v7, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBar:Landroid/view/View;

    if-eqz v7, :cond_1

    .line 185
    packed-switch v0, :pswitch_data_0

    .line 240
    :cond_0
    iput-boolean v6, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mDragStarted:Z

    .line 247
    .end local v0    # "action":I
    .end local v4    # "x":I
    :cond_1
    :goto_1
    :try_start_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_1
    .catch Ljava/lang/ArithmeticException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    .line 250
    :goto_2
    return v5

    .line 176
    :catch_0
    move-exception v1

    .line 177
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 187
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "action":I
    .restart local v4    # "x":I
    :pswitch_0
    iget-object v7, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    add-int/lit8 v7, v7, -0x14

    if-gt v7, v4, :cond_1

    iget-object v7, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    add-int/lit8 v7, v7, 0x14

    if-gt v4, v7, :cond_1

    .line 189
    iput-boolean v5, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mDragStarted:Z

    .line 193
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 194
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    invoke-interface {v7, v5}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 196
    :cond_2
    iget-object v7, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    sub-int/2addr v7, v4

    iput v7, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->modifyX:I

    .line 197
    invoke-direct {p0, v6}, Lcom/samsung/helphub/widget/SplitViewContainer;->notifyToListener(I)V

    .line 198
    iget-object v6, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBar:Landroid/view/View;

    invoke-virtual {v6, v5}, Landroid/view/View;->setPressed(Z)V

    goto :goto_2

    .line 205
    :pswitch_1
    iget-boolean v5, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mDragStarted:Z

    if-eqz v5, :cond_1

    .line 206
    iget-object v5, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 207
    .local v3, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v5, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelMinWidth:I

    if-lt v4, v5, :cond_4

    iget v5, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mViewWidth:I

    iget v7, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mRightPanelMinWidth:I

    sub-int/2addr v5, v7

    if-gt v4, v5, :cond_4

    .line 208
    iget v5, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->modifyX:I

    add-int/2addr v5, v4

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 209
    iget-object v5, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v5, v3}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 217
    :cond_3
    :goto_3
    iget v5, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    int-to-float v5, v5

    iget v7, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mViewWidth:I

    int-to-float v7, v7

    div-float/2addr v5, v7

    sput v5, Lcom/samsung/helphub/widget/SplitViewContainer;->sLastLeftPanelRatio:F

    .line 218
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v7, Lcom/samsung/helphub/widget/SplitViewContainer;->sLastLeftPanelRatio:F

    invoke-static {v5, v7}, Lcom/samsung/helphub/utility/PreferenceHelper;->setLeftPanelRatio(Landroid/content/Context;F)V

    goto :goto_1

    .line 211
    :cond_4
    iget v5, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelMinWidth:I

    if-ge v4, v5, :cond_3

    .line 212
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0b0119

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 214
    iget-object v5, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v5, v3}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 224
    .end local v3    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :pswitch_2
    iget-boolean v5, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mDragStarted:Z

    if-eqz v5, :cond_0

    .line 225
    iput-boolean v6, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mDragStarted:Z

    .line 229
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 230
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-interface {v5, v6}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 233
    :cond_5
    invoke-direct {p0, v6}, Lcom/samsung/helphub/widget/SplitViewContainer;->notifyToListener(I)V

    .line 234
    iget-object v5, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBar:Landroid/view/View;

    invoke-virtual {v5, v6}, Landroid/view/View;->setPressed(Z)V

    .line 235
    iput v6, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->modifyX:I

    goto/16 :goto_1

    .line 248
    .end local v0    # "action":I
    .end local v4    # "x":I
    :catch_1
    move-exception v2

    .line 249
    .local v2, "ex":Ljava/lang/ArithmeticException;
    invoke-virtual {v2}, Ljava/lang/ArithmeticException;->printStackTrace()V

    move v5, v6

    .line 250
    goto/16 :goto_2

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/high16 v4, 0x42c80000    # 100.0f

    const/4 v3, 0x1

    .line 59
    iget-object v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    .line 60
    const v1, 0x7f0d01b8

    invoke-virtual {p0, v1}, Lcom/samsung/helphub/widget/SplitViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    .line 62
    :cond_0
    sget v1, Lcom/samsung/helphub/widget/SplitViewContainer;->sLastLeftPanelRatio:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 63
    iget-object v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mViewWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelRatio:F

    .line 78
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 80
    iget-object v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 81
    .local v0, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelRatio:F

    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 82
    iget-object v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 84
    iput v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mCheckFirstTimeRotation:I

    .line 86
    return-void

    .line 65
    .end local v0    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    const-string v1, "ro.build.scafe"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "latte"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "ro.build.scafe.shot"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "half"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 66
    iget-object v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v3, :cond_2

    .line 67
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0c0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    iput v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelRatio:F

    goto :goto_0

    .line 69
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    iput v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelRatio:F

    goto :goto_0

    .line 71
    :cond_3
    iget-object v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v3, :cond_4

    .line 72
    iget v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelRatio:F

    const/high16 v2, 0x3f400000    # 0.75f

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelRatio:F

    goto :goto_0

    .line 74
    :cond_4
    const v1, 0x3eae147b    # 0.34f

    iput v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelRatio:F

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const v5, 0x3eae147b    # 0.34f

    .line 121
    :try_start_0
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    if-eqz p1, :cond_6

    .line 128
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getMeasuredWidth()I

    move-result v3

    iput v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mViewWidth:I

    .line 129
    iget v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mViewWidth:I

    if-eqz v3, :cond_0

    .line 130
    iget v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mViewWidth:I

    int-to-float v3, v3

    const v4, 0x3dcccccd    # 0.1f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelMinWidth:I

    .line 131
    iget v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mViewWidth:I

    int-to-float v3, v3

    mul-float/2addr v3, v5

    float-to-int v3, v3

    iput v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mRightPanelMinWidth:I

    .line 133
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    iget v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mViewWidth:I

    if-eqz v3, :cond_1

    .line 134
    iget v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mViewWidth:I

    int-to-float v3, v3

    const v4, 0x3e23d70a    # 0.16f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelMinWidth:I

    .line 135
    iget v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mViewWidth:I

    int-to-float v3, v3

    mul-float/2addr v3, v5

    float-to-int v3, v3

    iput v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mRightPanelMinWidth:I

    .line 138
    :cond_1
    iget v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mViewWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanelRatio:F

    mul-float/2addr v3, v4

    float-to-int v1, v3

    .line 139
    .local v1, "leftPanelWidth":I
    iget v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mViewWidth:I

    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b011f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    if-ge v3, v4, :cond_2

    .line 141
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/SplitViewContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0122

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v1, v3

    .line 144
    :cond_2
    iget-object v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBar:Landroid/view/View;

    if-nez v3, :cond_3

    .line 145
    const v3, 0x7f0d01b7

    invoke-virtual {p0, v3}, Lcom/samsung/helphub/widget/SplitViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBar:Landroid/view/View;

    .line 147
    :cond_3
    iget-object v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBar:Landroid/view/View;

    if-eqz v3, :cond_4

    .line 148
    iget-object v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBar:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 150
    :cond_4
    iget-object v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    if-nez v3, :cond_5

    .line 151
    const v3, 0x7f0d01b8

    invoke-virtual {p0, v3}, Lcom/samsung/helphub/widget/SplitViewContainer;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    .line 153
    :cond_5
    iget-object v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    if-eqz v3, :cond_6

    .line 154
    iget-object v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 155
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 156
    iget-object v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mLeftPanel:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 160
    .end local v1    # "leftPanelWidth":I
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_6
    iget-object v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBar:Landroid/view/View;

    if-eqz v3, :cond_7

    .line 161
    iget-object v3, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBar:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 163
    :cond_7
    :goto_0
    return-void

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public removeSplitBarStateChangeListener(Lcom/samsung/helphub/widget/SplitViewContainer$SplitBarStateChangeListener;)Z
    .locals 2
    .param p1, "l"    # Lcom/samsung/helphub/widget/SplitViewContainer$SplitBarStateChangeListener;

    .prologue
    .line 269
    iget-object v1, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarStateChangeListenerList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 270
    :try_start_0
    iget-object v0, p0, Lcom/samsung/helphub/widget/SplitViewContainer;->mSplitBarStateChangeListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 271
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
