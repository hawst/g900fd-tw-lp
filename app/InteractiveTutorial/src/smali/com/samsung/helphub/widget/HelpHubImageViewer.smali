.class public final Lcom/samsung/helphub/widget/HelpHubImageViewer;
.super Landroid/widget/RelativeLayout;
.source "HelpHubImageViewer.java"


# static fields
.field private static final DEFAULT_DELAY_TIME:I = 0x7d0


# instance fields
.field private final mActContext:Landroid/app/Activity;

.field private mAutoStart:Z

.field private mCurrentIndex:I

.field private mDrawablesTypedArray:Landroid/content/res/TypedArray;

.field private final mImageView:Landroid/widget/ImageView;

.field private mOneShot:Z

.field private mPeriod:I

.field private mShowSlideTime:[I

.field private mTimer:Ljava/util/Timer;

.field private mTimerTask:Ljava/util/TimerTask;

.field private mViewAttached:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 110
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v7, -0x2

    const/4 v6, 0x0

    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    iput v6, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mCurrentIndex:I

    .line 35
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mShowSlideTime:[I

    move-object v4, p1

    .line 57
    check-cast v4, Landroid/app/Activity;

    iput-object v4, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mActContext:Landroid/app/Activity;

    .line 59
    if-eqz p2, :cond_2

    .line 60
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/samsung/helphub/R$styleable;->HelpHubImageViewer:[I

    invoke-virtual {v4, p2, v5, v6, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 63
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v6, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 66
    .local v3, "resID":I
    if-lez v3, :cond_0

    .line 67
    iget-object v4, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mActContext:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mDrawablesTypedArray:Landroid/content/res/TypedArray;

    .line 71
    :cond_0
    const/4 v4, 0x5

    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 72
    if-lez v3, :cond_1

    .line 74
    :try_start_0
    iget-object v4, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mActContext:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mShowSlideTime:[I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :cond_1
    :goto_0
    const/4 v4, 0x4

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mOneShot:Z

    .line 88
    const/4 v4, 0x2

    const/16 v5, 0x7d0

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    iput v4, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mPeriod:I

    .line 89
    const/4 v4, 0x3

    invoke-virtual {v0, v4, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    iput-boolean v4, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mAutoStart:Z

    .line 91
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 94
    .end local v0    # "a":Landroid/content/res/TypedArray;
    .end local v3    # "resID":I
    :cond_2
    new-instance v4, Landroid/widget/ImageView;

    invoke-direct {v4, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mImageView:Landroid/widget/ImageView;

    .line 95
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 97
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v4, 0xd

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 98
    iget-object v4, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 100
    iget-object v4, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {p0, v4}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->addView(Landroid/view/View;)V

    .line 101
    return-void

    .line 75
    .end local v2    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v0    # "a":Landroid/content/res/TypedArray;
    .restart local v3    # "resID":I
    :catch_0
    move-exception v1

    .line 76
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v4, "HelpHubImageViewer"

    const-string v5, "Fail to getIntArray"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/helphub/widget/HelpHubImageViewer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mViewAttached:Z

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/widget/HelpHubImageViewer;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .prologue
    .line 24
    iget v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mCurrentIndex:I

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/helphub/widget/HelpHubImageViewer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpHubImageViewer;
    .param p1, "x1"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mCurrentIndex:I

    return p1
.end method

.method static synthetic access$108(Lcom/samsung/helphub/widget/HelpHubImageViewer;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .prologue
    .line 24
    iget v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mCurrentIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mCurrentIndex:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/helphub/widget/HelpHubImageViewer;)Landroid/content/res/TypedArray;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mDrawablesTypedArray:Landroid/content/res/TypedArray;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/helphub/widget/HelpHubImageViewer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mOneShot:Z

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/helphub/widget/HelpHubImageViewer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mAutoStart:Z

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/helphub/widget/HelpHubImageViewer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->startTimer()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/helphub/widget/HelpHubImageViewer;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mShowSlideTime:[I

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/helphub/widget/HelpHubImageViewer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->stopTimer()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/helphub/widget/HelpHubImageViewer;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpHubImageViewer;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mActContext:Landroid/app/Activity;

    return-object v0
.end method

.method private startTimer()V
    .locals 6

    .prologue
    .line 274
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->stopTimer()V

    .line 275
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mTimer:Ljava/util/Timer;

    .line 276
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->createTimerTask()V

    .line 278
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mShowSlideTime:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mShowSlideTime:[I

    array-length v0, v0

    iget v1, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mCurrentIndex:I

    if-le v0, v1, :cond_0

    .line 279
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mTimerTask:Ljava/util/TimerTask;

    iget-object v2, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mShowSlideTime:[I

    iget v3, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mCurrentIndex:I

    aget v2, v2, v3

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 285
    :goto_0
    return-void

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mTimerTask:Ljava/util/TimerTask;

    iget v2, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mPeriod:I

    int-to-long v2, v2

    iget v4, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mPeriod:I

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_0
.end method

.method private stopTimer()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 307
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mTimerTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mTimerTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 309
    iput-object v1, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mTimerTask:Ljava/util/TimerTask;

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 313
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 314
    iput-object v1, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mTimer:Ljava/util/Timer;

    .line 318
    :cond_1
    return-void
.end method


# virtual methods
.method createTimerTask()V
    .locals 1

    .prologue
    .line 242
    new-instance v0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;-><init>(Lcom/samsung/helphub/widget/HelpHubImageViewer;)V

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mTimerTask:Ljava/util/TimerTask;

    .line 271
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 322
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mViewAttached:Z

    .line 323
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->startDrawablesAnimation()V

    .line 324
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 325
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mViewAttached:Z

    .line 338
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->stopTimer()V

    .line 339
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 340
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 6
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 187
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 188
    .local v0, "parcel":Landroid/os/Parcel;
    const/4 v3, 0x2

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    .line 191
    .local v2, "values":[I
    const/4 v1, 0x0

    .line 193
    .local v1, "playing":I
    const/4 v3, 0x0

    invoke-super {p0, v3}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 195
    invoke-interface {p1, v0, v4}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    .line 196
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->readIntArray([I)V

    .line 198
    array-length v3, v2

    if-eqz v3, :cond_0

    .line 199
    aget v1, v2, v4

    .line 200
    aget v3, v2, v5

    iput v3, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mCurrentIndex:I

    .line 204
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->setSlide()V

    .line 206
    if-ne v1, v5, :cond_1

    .line 208
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->startTimer()V

    .line 211
    :cond_1
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 212
    return-void

    .line 188
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 6

    .prologue
    .line 216
    const/4 v1, 0x0

    .line 217
    .local v1, "playing":I
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 220
    .local v0, "parcel":Landroid/os/Parcel;
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    .line 223
    const/4 v1, 0x1

    .line 226
    const/4 v4, 0x2

    new-array v3, v4, [I

    const/4 v4, 0x0

    aput v1, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mCurrentIndex:I

    aput v5, v3, v4

    .line 229
    .local v3, "values":[I
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 230
    sget-object v4, Lcom/samsung/helphub/widget/HelpHubImageViewerParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Parcelable;

    .line 231
    .local v2, "state":Landroid/os/Parcelable;
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 233
    return-object v2
.end method

.method public onStartTemporaryDetach()V
    .locals 0

    .prologue
    .line 331
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->startTimer()V

    .line 332
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onStartTemporaryDetach()V

    .line 333
    return-void
.end method

.method public setDrawablesArray(I)V
    .locals 1
    .param p1, "drawablesArrayId"    # I

    .prologue
    .line 294
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mActContext:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mDrawablesTypedArray:Landroid/content/res/TypedArray;

    .line 296
    return-void
.end method

.method public setPluginDrawables(Landroid/content/res/TypedArray;)V
    .locals 0
    .param p1, "drawablesTypedArray"    # Landroid/content/res/TypedArray;

    .prologue
    .line 303
    iput-object p1, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mDrawablesTypedArray:Landroid/content/res/TypedArray;

    .line 304
    return-void
.end method

.method protected setSlide()V
    .locals 3

    .prologue
    .line 237
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mDrawablesTypedArray:Landroid/content/res/TypedArray;

    iget v2, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mCurrentIndex:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 239
    return-void
.end method

.method public startDrawablesAnimation()V
    .locals 3

    .prologue
    .line 172
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mDrawablesTypedArray:Landroid/content/res/TypedArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mDrawablesTypedArray:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mDrawablesTypedArray:Landroid/content/res/TypedArray;

    iget v2, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mCurrentIndex:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 175
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mDrawablesTypedArray:Landroid/content/res/TypedArray;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 178
    iget-boolean v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer;->mAutoStart:Z

    if-eqz v0, :cond_0

    .line 179
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->startTimer()V

    .line 183
    :cond_0
    return-void
.end method
