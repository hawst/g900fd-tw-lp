.class Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;
.super Ljava/lang/Object;
.source "HelpHubImageViewer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/helphub/widget/HelpHubImageViewer$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/widget/HelpHubImageViewer$1;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;->this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;->this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    iget-object v0, v0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;->this$0:Lcom/samsung/helphub/widget/HelpHubImageViewer;

    # operator++ for: Lcom/samsung/helphub/widget/HelpHubImageViewer;->mCurrentIndex:I
    invoke-static {v0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->access$108(Lcom/samsung/helphub/widget/HelpHubImageViewer;)I

    .line 251
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;->this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    iget-object v0, v0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;->this$0:Lcom/samsung/helphub/widget/HelpHubImageViewer;

    # getter for: Lcom/samsung/helphub/widget/HelpHubImageViewer;->mCurrentIndex:I
    invoke-static {v0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->access$100(Lcom/samsung/helphub/widget/HelpHubImageViewer;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;->this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    iget-object v1, v1, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;->this$0:Lcom/samsung/helphub/widget/HelpHubImageViewer;

    # getter for: Lcom/samsung/helphub/widget/HelpHubImageViewer;->mDrawablesTypedArray:Landroid/content/res/TypedArray;
    invoke-static {v1}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->access$200(Lcom/samsung/helphub/widget/HelpHubImageViewer;)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->length()I

    move-result v1

    if-lt v0, v1, :cond_3

    .line 252
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;->this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    iget-object v0, v0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;->this$0:Lcom/samsung/helphub/widget/HelpHubImageViewer;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/helphub/widget/HelpHubImageViewer;->mCurrentIndex:I
    invoke-static {v0, v1}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->access$102(Lcom/samsung/helphub/widget/HelpHubImageViewer;I)I

    .line 253
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;->this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    iget-object v0, v0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;->this$0:Lcom/samsung/helphub/widget/HelpHubImageViewer;

    # getter for: Lcom/samsung/helphub/widget/HelpHubImageViewer;->mOneShot:Z
    invoke-static {v0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->access$300(Lcom/samsung/helphub/widget/HelpHubImageViewer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;->this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;->mStopAnimation:Z

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;->this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    iget-object v0, v0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;->this$0:Lcom/samsung/helphub/widget/HelpHubImageViewer;

    # getter for: Lcom/samsung/helphub/widget/HelpHubImageViewer;->mAutoStart:Z
    invoke-static {v0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->access$400(Lcom/samsung/helphub/widget/HelpHubImageViewer;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;->this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    iget-object v0, v0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;->this$0:Lcom/samsung/helphub/widget/HelpHubImageViewer;

    # invokes: Lcom/samsung/helphub/widget/HelpHubImageViewer;->startTimer()V
    invoke-static {v0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->access$500(Lcom/samsung/helphub/widget/HelpHubImageViewer;)V

    .line 261
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;->this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    iget-object v0, v0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;->this$0:Lcom/samsung/helphub/widget/HelpHubImageViewer;

    invoke-virtual {v0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->setSlide()V

    .line 263
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;->this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    iget-boolean v0, v0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;->mStopAnimation:Z

    if-eqz v0, :cond_2

    .line 264
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;->this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    iget-object v0, v0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;->this$0:Lcom/samsung/helphub/widget/HelpHubImageViewer;

    # invokes: Lcom/samsung/helphub/widget/HelpHubImageViewer;->stopTimer()V
    invoke-static {v0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->access$700(Lcom/samsung/helphub/widget/HelpHubImageViewer;)V

    .line 266
    :cond_2
    return-void

    .line 258
    :cond_3
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;->this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    iget-object v0, v0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;->this$0:Lcom/samsung/helphub/widget/HelpHubImageViewer;

    # getter for: Lcom/samsung/helphub/widget/HelpHubImageViewer;->mShowSlideTime:[I
    invoke-static {v0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->access$600(Lcom/samsung/helphub/widget/HelpHubImageViewer;)[I

    move-result-object v0

    if-eqz v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1$1;->this$1:Lcom/samsung/helphub/widget/HelpHubImageViewer$1;

    iget-object v0, v0, Lcom/samsung/helphub/widget/HelpHubImageViewer$1;->this$0:Lcom/samsung/helphub/widget/HelpHubImageViewer;

    # invokes: Lcom/samsung/helphub/widget/HelpHubImageViewer;->startTimer()V
    invoke-static {v0}, Lcom/samsung/helphub/widget/HelpHubImageViewer;->access$500(Lcom/samsung/helphub/widget/HelpHubImageViewer;)V

    goto :goto_0
.end method
