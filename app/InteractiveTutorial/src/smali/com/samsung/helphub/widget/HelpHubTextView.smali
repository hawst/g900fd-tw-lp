.class public final Lcom/samsung/helphub/widget/HelpHubTextView;
.super Landroid/widget/TextView;
.source "HelpHubTextView.java"


# static fields
.field private static final ICON_HTML_END:Ljava/lang/String; = "\"/>"

.field private static final ICON_HTML_START:Ljava/lang/String; = "<img src=\"@drawable/"

.field static bNewLineCheck:Z

.field static bNewLineOrentation:Z

.field static finalString:Ljava/lang/String;

.field static mNewLineString:Ljava/lang/String;

.field static mShowIntegerString:Ljava/lang/String;

.field static mShowString:Ljava/lang/String;


# instance fields
.field currentDrawable:Lcom/samsung/helphub/widget/HelpHubDrawable;

.field private mAddItemIds:[I

.field private final mContext:Landroid/content/Context;

.field private final mDrawables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/helphub/widget/HelpHubDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mImageYDiff:F

.field private final mImgGetter:Landroid/text/Html$ImageGetter;

.field private mInsideImageGravity:I

.field private mInsideImageHeight:F

.field private mInsideImagePadding:Landroid/graphics/Rect;

.field private mInsideImageWidth:F

.field mStorevalue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mImageYDiff:F

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mDrawables:Ljava/util/ArrayList;

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    .line 60
    const/16 v0, 0x11

    iput v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageGravity:I

    .line 71
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mStorevalue:Ljava/lang/String;

    .line 304
    new-instance v0, Lcom/samsung/helphub/widget/HelpHubTextView$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/widget/HelpHubTextView$1;-><init>(Lcom/samsung/helphub/widget/HelpHubTextView;)V

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mImgGetter:Landroid/text/Html$ImageGetter;

    .line 86
    iput-object p1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 93
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 100
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mImageYDiff:F

    .line 51
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mDrawables:Ljava/util/ArrayList;

    .line 55
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    .line 60
    const/16 v1, 0x11

    iput v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageGravity:I

    .line 71
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mStorevalue:Ljava/lang/String;

    .line 304
    new-instance v1, Lcom/samsung/helphub/widget/HelpHubTextView$1;

    invoke-direct {v1, p0}, Lcom/samsung/helphub/widget/HelpHubTextView$1;-><init>(Lcom/samsung/helphub/widget/HelpHubTextView;)V

    iput-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mImgGetter:Landroid/text/Html$ImageGetter;

    .line 101
    iput-object p1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    .line 102
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    .line 105
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/helphub/widget/HelpHubTextView;->bNewLineOrentation:Z

    .line 110
    :goto_0
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isChinaModel()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 112
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 113
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    :cond_0
    :goto_1
    invoke-direct {p0, p2}, Lcom/samsung/helphub/widget/HelpHubTextView;->initSelfResources(Landroid/util/AttributeSet;)V

    .line 121
    invoke-direct {p0, v0, p2}, Lcom/samsung/helphub/widget/HelpHubTextView;->CheckInteger(Ljava/lang/String;Landroid/util/AttributeSet;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    invoke-direct {p0, p2}, Lcom/samsung/helphub/widget/HelpHubTextView;->IntegerinitSelfResources(Landroid/util/AttributeSet;)V

    .line 123
    :cond_1
    invoke-direct {p0, v0, p2}, Lcom/samsung/helphub/widget/HelpHubTextView;->CheckString(Ljava/lang/String;Landroid/util/AttributeSet;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 124
    invoke-direct {p0, p2}, Lcom/samsung/helphub/widget/HelpHubTextView;->StringinitSelfResources(Landroid/util/AttributeSet;)V

    .line 125
    :cond_2
    invoke-direct {p0, v0, p2}, Lcom/samsung/helphub/widget/HelpHubTextView;->CheckNewLine(Ljava/lang/String;Landroid/util/AttributeSet;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 126
    invoke-direct {p0, p2}, Lcom/samsung/helphub/widget/HelpHubTextView;->NewLineinitSelfResources(Landroid/util/AttributeSet;)V

    .line 128
    :cond_3
    return-void

    .line 107
    :cond_4
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/helphub/widget/HelpHubTextView;->bNewLineOrentation:Z

    goto :goto_0

    .line 114
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-virtual {p0, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private Changespecialchartointeger(Ljava/lang/String;Landroid/util/AttributeSet;)V
    .locals 10
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v9, 0x0

    .line 354
    iget-object v7, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    sget-object v8, Lcom/samsung/helphub/R$styleable;->HelpHubTextView:[I

    invoke-virtual {v7, p2, v8, v9, v9}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 359
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v7, 0x9

    invoke-virtual {v0, v7, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 360
    .local v2, "id":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 362
    iget-object v7, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 364
    .local v4, "mStrings":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 365
    .local v3, "index":I
    const-string v5, "$d"

    .line 366
    .local v5, "newTemplate":Ljava/lang/String;
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 367
    .local v6, "pos":I
    :goto_0
    const/4 v7, -0x1

    if-eq v6, v7, :cond_1

    .line 368
    new-instance v1, Ljava/lang/StringBuilder;

    const/4 v7, 0x3

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 369
    .local v1, "builder":Ljava/lang/StringBuilder;
    add-int/lit8 v7, v6, -0x2

    invoke-virtual {p1, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    array-length v7, v4

    if-ge v3, v7, :cond_0

    .line 371
    aget-object v7, v4, v3

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    :cond_0
    add-int/lit8 v7, v6, 0x2

    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 376
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 377
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(Ljava/lang/CharSequence;)V

    .line 378
    add-int/lit8 v3, v3, 0x1

    .line 379
    goto :goto_0

    .line 381
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    :cond_1
    return-void
.end method

.method private Changespecialchartostring(Ljava/lang/String;Landroid/util/AttributeSet;)V
    .locals 10
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v9, 0x0

    .line 410
    iget-object v7, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    sget-object v8, Lcom/samsung/helphub/R$styleable;->HelpHubTextView:[I

    invoke-virtual {v7, p2, v8, v9, v9}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 413
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v7, 0xa

    invoke-virtual {v0, v7, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 414
    .local v2, "id":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 416
    iget-object v7, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 418
    .local v4, "mStrings":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 419
    .local v3, "index":I
    const-string v5, "$s"

    .line 420
    .local v5, "newTemplate":Ljava/lang/String;
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 421
    .local v6, "pos":I
    :goto_0
    const/4 v7, -0x1

    if-eq v6, v7, :cond_1

    .line 422
    new-instance v1, Ljava/lang/StringBuilder;

    const/4 v7, 0x3

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 423
    .local v1, "builder":Ljava/lang/StringBuilder;
    add-int/lit8 v7, v6, -0x2

    invoke-virtual {p1, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 424
    array-length v7, v4

    if-ge v3, v7, :cond_0

    .line 425
    aget-object v7, v4, v3

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 428
    :cond_0
    add-int/lit8 v7, v6, 0x2

    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 430
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 431
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(Ljava/lang/CharSequence;)V

    .line 432
    add-int/lit8 v3, v3, 0x1

    .line 433
    goto :goto_0

    .line 434
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    :cond_1
    return-void
.end method

.method private CheckInteger(Ljava/lang/String;Landroid/util/AttributeSet;)Z
    .locals 5
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 328
    iget-object v3, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/samsung/helphub/R$styleable;->HelpHubTextView:[I

    invoke-virtual {v3, p2, v4, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 330
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v3, 0x9

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 332
    .local v1, "iconsArrayId":I
    if-lez v1, :cond_1

    .line 333
    invoke-direct {p0, p1}, Lcom/samsung/helphub/widget/HelpHubTextView;->checkspecialcharacterforinteger(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 334
    invoke-direct {p0, p1, p2}, Lcom/samsung/helphub/widget/HelpHubTextView;->Changespecialchartointeger(Ljava/lang/String;Landroid/util/AttributeSet;)V

    .line 336
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 337
    const/4 v2, 0x1

    .line 340
    :goto_0
    return v2

    .line 339
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private CheckNewLine(Ljava/lang/String;Landroid/util/AttributeSet;)Z
    .locals 6
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 437
    iget-object v4, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/samsung/helphub/R$styleable;->HelpHubTextView:[I

    invoke-virtual {v4, p2, v5, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 439
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v4, 0xb

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 441
    .local v1, "iconsArrayId":I
    if-lez v1, :cond_0

    .line 442
    sput-boolean v2, Lcom/samsung/helphub/widget/HelpHubTextView;->bNewLineCheck:Z

    .line 443
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 448
    :goto_0
    return v2

    .line 446
    :cond_0
    sput-boolean v3, Lcom/samsung/helphub/widget/HelpHubTextView;->bNewLineCheck:Z

    .line 447
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    move v2, v3

    .line 448
    goto :goto_0
.end method

.method private CheckString(Ljava/lang/String;Landroid/util/AttributeSet;)Z
    .locals 5
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 384
    iget-object v3, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/samsung/helphub/R$styleable;->HelpHubTextView:[I

    invoke-virtual {v3, p2, v4, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 386
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v3, 0xa

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 388
    .local v1, "iconsArrayId":I
    if-lez v1, :cond_1

    .line 389
    invoke-direct {p0, p1}, Lcom/samsung/helphub/widget/HelpHubTextView;->checkspecialcharacterforstring(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 390
    invoke-direct {p0, p1, p2}, Lcom/samsung/helphub/widget/HelpHubTextView;->Changespecialchartostring(Ljava/lang/String;Landroid/util/AttributeSet;)V

    .line 392
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 393
    const/4 v2, 0x1

    .line 396
    :goto_0
    return v2

    .line 395
    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private IntegerHtmlText(Landroid/content/res/TypedArray;)V
    .locals 4
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 482
    invoke-virtual {p1, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 485
    .local v1, "textResourceId":I
    if-eqz v1, :cond_0

    .line 487
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(I)V

    .line 490
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 493
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    if-eqz v2, :cond_1

    .line 494
    invoke-direct {p0, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->applyInteger(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 498
    :cond_1
    invoke-static {v0, v3, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(Ljava/lang/CharSequence;)V

    .line 499
    return-void
.end method

.method private IntegerinitSelfResources(Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v5, 0x9

    const/4 v4, 0x0

    .line 228
    iget-object v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/samsung/helphub/R$styleable;->HelpHubTextView:[I

    invoke-virtual {v2, p1, v3, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 231
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/samsung/helphub/widget/HelpHubTextView;->mShowIntegerString:Ljava/lang/String;

    .line 234
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 236
    .local v1, "iconId":I
    if-lez v1, :cond_0

    .line 237
    const/4 v2, 0x1

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    .line 238
    iget-object v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    aput v1, v2, v4

    .line 242
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->IntegerHtmlText(Landroid/content/res/TypedArray;)V

    .line 244
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 245
    return-void
.end method

.method private NewLineHtmlText(Landroid/content/res/TypedArray;)V
    .locals 4
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 526
    invoke-virtual {p1, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 529
    .local v1, "textResourceId":I
    if-eqz v1, :cond_0

    .line 531
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(I)V

    .line 534
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 537
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    if-eqz v2, :cond_1

    .line 538
    invoke-direct {p0, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->applyNewLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 542
    :cond_1
    invoke-static {v0, v3, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(Ljava/lang/CharSequence;)V

    .line 543
    return-void
.end method

.method private NewLineinitSelfResources(Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v5, 0xb

    const/4 v4, 0x0

    .line 285
    iget-object v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/samsung/helphub/R$styleable;->HelpHubTextView:[I

    invoke-virtual {v2, p1, v3, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 288
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/samsung/helphub/widget/HelpHubTextView;->mNewLineString:Ljava/lang/String;

    .line 291
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 293
    .local v1, "iconId":I
    if-lez v1, :cond_0

    .line 294
    const/4 v2, 0x1

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    .line 295
    iget-object v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    aput v1, v2, v4

    .line 299
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->NewLineHtmlText(Landroid/content/res/TypedArray;)V

    .line 301
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 302
    return-void
.end method

.method private StringHtmlText(Landroid/content/res/TypedArray;)V
    .locals 4
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 504
    invoke-virtual {p1, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 507
    .local v1, "textResourceId":I
    if-eqz v1, :cond_0

    .line 509
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(I)V

    .line 512
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 515
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    if-eqz v2, :cond_1

    .line 516
    invoke-direct {p0, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->applyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 520
    :cond_1
    invoke-static {v0, v3, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(Ljava/lang/CharSequence;)V

    .line 521
    return-void
.end method

.method private StringinitSelfResources(Landroid/util/AttributeSet;)V
    .locals 9
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const v6, 0x7f0a0525

    const/16 v8, 0xa

    const/4 v7, 0x0

    .line 249
    iget-object v4, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 250
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    .line 252
    .local v1, "ai":Landroid/content/pm/ApplicationInfo;
    iget-object v4, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    sget-object v5, Lcom/samsung/helphub/R$styleable;->HelpHubTextView:[I

    invoke-virtual {v4, p1, v5, v7, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 255
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v8}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/samsung/helphub/widget/HelpHubTextView;->mShowString:Ljava/lang/String;

    .line 257
    sget-object v4, Lcom/samsung/helphub/widget/HelpHubTextView;->mShowString:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 258
    sget-object v4, Lcom/samsung/helphub/widget/HelpHubTextView;->mShowString:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 260
    :try_start_0
    const-string v4, "com.android.calendar"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 264
    :goto_0
    if-eqz v1, :cond_0

    .line 265
    sget-object v4, Lcom/samsung/helphub/widget/HelpHubTextView;->mShowString:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/samsung/helphub/widget/HelpHubTextView;->mShowString:Ljava/lang/String;

    .line 270
    :cond_0
    invoke-virtual {v0, v8, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 272
    .local v2, "iconId":I
    if-lez v2, :cond_1

    .line 273
    const/4 v4, 0x1

    new-array v4, v4, [I

    iput-object v4, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    .line 274
    iget-object v4, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    aput v2, v4, v7

    .line 278
    :cond_1
    invoke-direct {p0, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->StringHtmlText(Landroid/content/res/TypedArray;)V

    .line 280
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 281
    return-void

    .line 261
    .end local v2    # "iconId":I
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/helphub/widget/HelpHubTextView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpHubTextView;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/widget/HelpHubTextView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpHubTextView;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mDrawables:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/helphub/widget/HelpHubTextView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpHubTextView;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->invalidateDrawables()V

    return-void
.end method

.method private applyImages(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 552
    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    .line 553
    .local v9, "sb":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .line 555
    .local v2, "index":I
    const-string v10, "%s"

    invoke-static {v10}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 557
    .local v5, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v5, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 558
    .local v3, "m":Ljava/util/regex/Matcher;
    iget-object v10, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 559
    .local v7, "res":Landroid/content/res/Resources;
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v8

    .line 561
    .local v8, "result":Z
    :goto_0
    if-eqz v8, :cond_1

    .line 562
    iget-object v10, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    array-length v10, v10

    if-ge v2, v10, :cond_0

    .line 563
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<img src=\"@drawable/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    aget v11, v11, v2

    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\"/>"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    .line 570
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 571
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v8

    goto :goto_0

    .line 575
    :cond_1
    invoke-virtual {v3, v9}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 576
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    .line 579
    const-string v4, "$s"

    .line 580
    .local v4, "newTemplate":Ljava/lang/String;
    sget-object v10, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    invoke-virtual {v10, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 581
    .local v6, "pos":I
    :goto_1
    const/4 v10, -0x1

    if-eq v6, v10, :cond_2

    .line 582
    sget-object v10, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    add-int/lit8 v11, v6, -0x1

    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v10

    add-int/lit8 v10, v10, -0x30

    add-int/lit8 v1, v10, -0x1

    .line 583
    .local v1, "image":I
    new-instance v0, Ljava/lang/StringBuilder;

    const/4 v10, 0x3

    invoke-direct {v0, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 584
    .local v0, "builder":Ljava/lang/StringBuilder;
    sget-object v10, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    const/4 v11, 0x0

    add-int/lit8 v12, v6, -0x2

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 585
    const-string v10, "<img src=\"@drawable/"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 586
    iget-object v10, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    aget v10, v10, v1

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 587
    const-string v10, "\"/>"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 588
    sget-object v10, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    add-int/lit8 v11, v6, 0x2

    invoke-virtual {v10, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 589
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    .line 590
    sget-object v10, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    invoke-virtual {v10, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 591
    goto :goto_1

    .line 593
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v1    # "image":I
    :cond_2
    sget-object v10, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    return-object v10
.end method

.method private applyInteger(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 597
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 599
    .local v4, "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .line 601
    .local v0, "index":I
    const-string v5, "%d"

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 603
    .local v2, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 605
    .local v1, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    .line 608
    .local v3, "result":Z
    :goto_0
    if-eqz v3, :cond_1

    .line 609
    iget-object v5, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    array-length v5, v5

    if-ge v0, v5, :cond_0

    .line 610
    sget-object v5, Lcom/samsung/helphub/widget/HelpHubTextView;->mShowIntegerString:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    .line 613
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 614
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    goto :goto_0

    .line 619
    :cond_1
    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 620
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    .line 623
    sget-object v5, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    const-string v6, "[\n]"

    const-string v7, "<br>"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    .line 625
    sget-object v5, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    return-object v5
.end method

.method private applyNewLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 661
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 663
    .local v6, "sb":Ljava/lang/StringBuffer;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    .line 665
    .local v2, "locale":Ljava/lang/String;
    new-instance v0, Ljava/lang/String;

    const-string v7, "ko"

    invoke-direct {v0, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 667
    .local v0, "KOREA":Ljava/lang/String;
    const/4 v1, 0x0

    .line 669
    .local v1, "index":I
    const-string v7, " "

    invoke-static {v7}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    .line 671
    .local v4, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v4, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    .line 673
    .local v3, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    .line 676
    .local v5, "result":Z
    :goto_0
    if-eqz v5, :cond_3

    .line 677
    iget-object v7, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    array-length v7, v7

    if-ge v1, v7, :cond_1

    .line 678
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    sget-boolean v7, Lcom/samsung/helphub/widget/HelpHubTextView;->bNewLineOrentation:Z

    if-eqz v7, :cond_2

    .line 679
    :cond_0
    const-string v7, " "

    invoke-virtual {v3, v6, v7}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    .line 686
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 687
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    goto :goto_0

    .line 681
    :cond_2
    const-string v7, "<BR>"

    invoke-virtual {v3, v6, v7}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_1

    .line 692
    :cond_3
    invoke-virtual {v3, v6}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 693
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    sput-object v7, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    .line 698
    sget-object v7, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    return-object v7
.end method

.method private applyString(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 629
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 631
    .local v4, "sb":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .line 633
    .local v0, "index":I
    const-string v5, "%s"

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 635
    .local v2, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 637
    .local v1, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    .line 640
    .local v3, "result":Z
    :goto_0
    if-eqz v3, :cond_1

    .line 641
    iget-object v5, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    array-length v5, v5

    if-ge v0, v5, :cond_0

    .line 642
    sget-object v5, Lcom/samsung/helphub/widget/HelpHubTextView;->mShowString:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    .line 645
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 646
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    goto :goto_0

    .line 651
    :cond_1
    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 652
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    .line 655
    sget-object v5, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    const-string v6, "[\n]"

    const-string v7, "<br>"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    .line 657
    sget-object v5, Lcom/samsung/helphub/widget/HelpHubTextView;->finalString:Ljava/lang/String;

    return-object v5
.end method

.method private checkspecialcharacterforinteger(Ljava/lang/String;)Z
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 344
    const-string v0, "$d"

    .line 345
    .local v0, "newTemplate":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 346
    .local v1, "pos":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 347
    const/4 v2, 0x1

    .line 349
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private checkspecialcharacterforstring(Ljava/lang/String;)Z
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 400
    const-string v0, "$s"

    .line 401
    .local v0, "newTemplate":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 402
    .local v1, "pos":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 403
    const/4 v2, 0x1

    .line 405
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static countOccurrences(Ljava/lang/String;C)I
    .locals 3
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "value"    # C

    .prologue
    .line 943
    const/4 v0, 0x0

    .line 944
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 945
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v2, p1, :cond_0

    .line 946
    add-int/lit8 v0, v0, 0x1

    .line 944
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 949
    :cond_1
    return v0
.end method

.method private initHtmlText(Landroid/content/res/TypedArray;)V
    .locals 4
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v2, 0x0

    .line 458
    invoke-virtual {p1, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 461
    .local v1, "textResourceId":I
    if-eqz v1, :cond_0

    .line 463
    invoke-virtual {p0, v1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(I)V

    .line 466
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 469
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    if-eqz v2, :cond_1

    .line 470
    invoke-direct {p0, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->applyImages(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 474
    :cond_1
    const-string v2, "[\n]"

    const-string v3, "<br>"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 477
    iget-object v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mImgGetter:Landroid/text/Html$ImageGetter;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(Ljava/lang/CharSequence;)V

    .line 478
    return-void
.end method

.method private initImgPadding(Landroid/content/res/TypedArray;)V
    .locals 3
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v2, 0x0

    .line 702
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    .line 705
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v0, v1

    .line 708
    .local v0, "padding":I
    if-eqz v0, :cond_0

    .line 709
    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 712
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v0, v1

    .line 715
    if-eqz v0, :cond_1

    .line 716
    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 719
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v0, v1

    .line 722
    if-eqz v0, :cond_2

    .line 723
    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 725
    :cond_2
    return-void
.end method

.method private initSelfResources(Landroid/util/AttributeSet;)V
    .locals 11
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/high16 v10, -0x40800000    # -1.0f

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 137
    iget-object v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v1, v6, Landroid/util/DisplayMetrics;->density:F

    .line 139
    .local v1, "density":F
    iget-object v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    sget-object v7, Lcom/samsung/helphub/R$styleable;->HelpHubTextView:[I

    invoke-virtual {v6, p1, v7, v8, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 143
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v6, 0x7

    invoke-virtual {v0, v6, v10}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v6

    iput v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageWidth:F

    .line 145
    iget v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageWidth:F

    cmpg-float v6, v6, v9

    if-gez v6, :cond_0

    .line 148
    iget v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageWidth:F

    div-float/2addr v6, v1

    iput v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageWidth:F

    .line 152
    :cond_0
    const/4 v6, 0x6

    invoke-virtual {v0, v6, v10}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v6

    iput v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageHeight:F

    .line 154
    iget v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageHeight:F

    cmpg-float v6, v6, v9

    if-gez v6, :cond_1

    .line 157
    iget v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageHeight:F

    div-float/2addr v6, v1

    iput v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageHeight:F

    .line 160
    :cond_1
    const/16 v6, 0xc

    const/16 v7, 0x11

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v6

    iput v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageGravity:I

    .line 163
    const/16 v6, 0x8

    invoke-virtual {v0, v6, v9}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v6

    iput v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mImageYDiff:F

    .line 165
    const/16 v6, 0xd

    invoke-virtual {v0, v6, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 168
    .local v4, "iconsArrayId":I
    if-lez v4, :cond_4

    .line 169
    iget-object v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 172
    .local v5, "iconsTypedArray":Landroid/content/res/TypedArray;
    if-eqz v5, :cond_3

    .line 173
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 174
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->length()I

    move-result v6

    new-array v6, v6, [I

    iput-object v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    .line 176
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    array-length v6, v6

    if-ge v2, v6, :cond_2

    .line 177
    iget-object v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    invoke-virtual {v5, v2, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    aput v7, v6, v2

    .line 176
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 180
    .end local v2    # "i":I
    :cond_2
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 198
    .end local v5    # "iconsTypedArray":Landroid/content/res/TypedArray;
    :cond_3
    :goto_1
    invoke-direct {p0, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->initImgPadding(Landroid/content/res/TypedArray;)V

    .line 200
    invoke-direct {p0, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->initHtmlText(Landroid/content/res/TypedArray;)V

    .line 202
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 203
    return-void

    .line 183
    :cond_4
    const/16 v6, 0xe

    invoke-virtual {v0, v6, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 185
    .local v3, "iconId":I
    if-lez v3, :cond_3

    .line 186
    const/4 v6, 0x1

    new-array v6, v6, [I

    iput-object v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    .line 187
    iget-object v6, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mAddItemIds:[I

    aput v3, v6, v8

    goto :goto_1
.end method

.method private invalidateDrawables()V
    .locals 7

    .prologue
    const/16 v6, 0x15

    const/4 v5, 0x0

    .line 728
    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mDrawables:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 732
    .local v0, "count":I
    :goto_0
    if-lez v0, :cond_4

    .line 733
    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mDrawables:Ljava/util/ArrayList;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/widget/HelpHubDrawable;

    iput-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->currentDrawable:Lcom/samsung/helphub/widget/HelpHubDrawable;

    .line 735
    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->currentDrawable:Lcom/samsung/helphub/widget/HelpHubDrawable;

    if-eqz v1, :cond_1

    .line 736
    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->currentDrawable:Lcom/samsung/helphub/widget/HelpHubDrawable;

    iget v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageGravity:I

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/widget/HelpHubDrawable;->setGravity(I)V

    .line 738
    iget v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageWidth:F

    cmpl-float v1, v1, v5

    if-ltz v1, :cond_2

    .line 739
    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->currentDrawable:Lcom/samsung/helphub/widget/HelpHubDrawable;

    iget v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageWidth:F

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/widget/HelpHubDrawable;->setWidth(I)V

    .line 746
    :goto_1
    iget v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageHeight:F

    cmpl-float v1, v1, v5

    if-ltz v1, :cond_3

    .line 747
    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->currentDrawable:Lcom/samsung/helphub/widget/HelpHubDrawable;

    iget v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageHeight:F

    float-to-int v2, v2

    neg-int v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/widget/HelpHubDrawable;->setHeight(I)V

    .line 854
    :goto_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v6, :cond_0

    .line 855
    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->currentDrawable:Lcom/samsung/helphub/widget/HelpHubDrawable;

    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getLineHeight()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->currentDrawable:Lcom/samsung/helphub/widget/HelpHubDrawable;

    invoke-virtual {v3}, Lcom/samsung/helphub/widget/HelpHubDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, v3

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getTextSize()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->currentDrawable:Lcom/samsung/helphub/widget/HelpHubDrawable;

    invoke-virtual {v4}, Lcom/samsung/helphub/widget/HelpHubDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mImageYDiff:F

    add-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/widget/HelpHubDrawable;->setBitmapY(F)V

    .line 857
    :cond_0
    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->currentDrawable:Lcom/samsung/helphub/widget/HelpHubDrawable;

    iget-object v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/widget/HelpHubDrawable;->setPadding(Landroid/graphics/Rect;)V

    .line 860
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 742
    :cond_2
    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->currentDrawable:Lcom/samsung/helphub/widget/HelpHubDrawable;

    iget-object v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->currentDrawable:Lcom/samsung/helphub/widget/HelpHubDrawable;

    invoke-virtual {v2}, Lcom/samsung/helphub/widget/HelpHubDrawable;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageWidth:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    neg-int v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/widget/HelpHubDrawable;->setWidth(I)V

    goto :goto_1

    .line 752
    :cond_3
    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->currentDrawable:Lcom/samsung/helphub/widget/HelpHubDrawable;

    iget-object v2, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->currentDrawable:Lcom/samsung/helphub/widget/HelpHubDrawable;

    invoke-virtual {v2}, Lcom/samsung/helphub/widget/HelpHubDrawable;->getIntrinsicHeight()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageHeight:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/helphub/widget/HelpHubDrawable;->setHeight(I)V

    goto :goto_2

    .line 865
    :cond_4
    iget-object v1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mDrawables:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_5

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v6, :cond_5

    .line 866
    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/samsung/helphub/widget/HelpHubTextView$2;

    invoke-direct {v2, p0}, Lcom/samsung/helphub/widget/HelpHubTextView$2;-><init>(Lcom/samsung/helphub/widget/HelpHubTextView;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 940
    :cond_5
    return-void
.end method


# virtual methods
.method public getImgGravity()I
    .locals 1

    .prologue
    .line 1000
    iget v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageGravity:I

    return v0
.end method

.method public getImgHeight()I
    .locals 1

    .prologue
    .line 991
    iget v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageHeight:F

    float-to-int v0, v0

    return v0
.end method

.method public getImgWidth()I
    .locals 1

    .prologue
    .line 982
    iget v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageWidth:F

    float-to-int v0, v0

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x0

    .line 1005
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 1006
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/helphub/widget/HelpHubTextView;->bNewLineOrentation:Z

    .line 1010
    :goto_0
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mStorevalue:Ljava/lang/String;

    .line 1012
    .local v0, "text":Ljava/lang/String;
    sget-boolean v1, Lcom/samsung/helphub/widget/HelpHubTextView;->bNewLineCheck:Z

    if-eqz v1, :cond_0

    .line 1013
    invoke-direct {p0, v0}, Lcom/samsung/helphub/widget/HelpHubTextView;->applyNewLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1014
    invoke-static {v0, v3, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/helphub/widget/HelpHubTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1016
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1017
    return-void

    .line 1008
    .end local v0    # "text":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    sput-boolean v1, Lcom/samsung/helphub/widget/HelpHubTextView;->bNewLineOrentation:Z

    goto :goto_0
.end method

.method public setImgGravity(I)V
    .locals 0
    .param p1, "g"    # I

    .prologue
    .line 995
    iput p1, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageGravity:I

    .line 996
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->invalidateDrawables()V

    .line 997
    return-void
.end method

.method public setImgHeight(I)V
    .locals 1
    .param p1, "height"    # I

    .prologue
    .line 986
    int-to-float v0, p1

    iput v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageHeight:F

    .line 987
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->invalidateDrawables()V

    .line 988
    return-void
.end method

.method public setImgPadding(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "padding"    # Landroid/graphics/Rect;

    .prologue
    .line 952
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 953
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->invalidateDrawables()V

    .line 954
    return-void
.end method

.method public setImgPaddingBottom(I)V
    .locals 1
    .param p1, "bottom"    # I

    .prologue
    .line 972
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->bottom:I

    .line 973
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->invalidateDrawables()V

    .line 974
    return-void
.end method

.method public setImgPaddingLeft(I)V
    .locals 1
    .param p1, "left"    # I

    .prologue
    .line 957
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->left:I

    .line 958
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->invalidateDrawables()V

    .line 959
    return-void
.end method

.method public setImgPaddingRight(I)V
    .locals 1
    .param p1, "right"    # I

    .prologue
    .line 962
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->right:I

    .line 963
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->invalidateDrawables()V

    .line 964
    return-void
.end method

.method public setImgPaddingTop(I)V
    .locals 1
    .param p1, "top"    # I

    .prologue
    .line 967
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImagePadding:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->top:I

    .line 968
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->invalidateDrawables()V

    .line 969
    return-void
.end method

.method public setImgWidth(I)V
    .locals 1
    .param p1, "width"    # I

    .prologue
    .line 977
    int-to-float v0, p1

    iput v0, p0, Lcom/samsung/helphub/widget/HelpHubTextView;->mInsideImageWidth:F

    .line 978
    invoke-direct {p0}, Lcom/samsung/helphub/widget/HelpHubTextView;->invalidateDrawables()V

    .line 979
    return-void
.end method
