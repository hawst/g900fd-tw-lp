.class public final Lcom/samsung/helphub/widget/HelpHubImageViewerParcelable;
.super Ljava/lang/Object;
.source "HelpHubImageViewerParcelable.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/helphub/widget/HelpHubImageViewerParcelable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mData:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Lcom/samsung/helphub/widget/HelpHubImageViewerParcelable$1;

    invoke-direct {v0}, Lcom/samsung/helphub/widget/HelpHubImageViewerParcelable$1;-><init>()V

    sput-object v0, Lcom/samsung/helphub/widget/HelpHubImageViewerParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewerParcelable;->mData:[I

    .line 23
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 24
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewerParcelable;->mData:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readIntArray([I)V

    .line 25
    return-void

    .line 9
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/helphub/widget/HelpHubImageViewerParcelable$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/samsung/helphub/widget/HelpHubImageViewerParcelable$1;

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/samsung/helphub/widget/HelpHubImageViewerParcelable;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpHubImageViewerParcelable;->mData:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 34
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 35
    return-void
.end method
