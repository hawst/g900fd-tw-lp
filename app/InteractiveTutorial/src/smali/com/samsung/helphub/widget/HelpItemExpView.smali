.class public Lcom/samsung/helphub/widget/HelpItemExpView;
.super Landroid/widget/LinearLayout;
.source "HelpItemExpView.java"


# instance fields
.field private mFoldIndicator:Landroid/widget/ImageView;

.field private mHelpItemViewClickLister:Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;

.field private mHelpPageContainer:Landroid/view/ViewGroup;

.field private mIsExpanded:Z

.field mOnClickListener:Landroid/view/View$OnClickListener;

.field private mPosition:I

.field private mSeparatorLine:Landroid/widget/ImageView;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mIsExpanded:Z

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mHelpItemViewClickLister:Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mPosition:I

    .line 92
    new-instance v0, Lcom/samsung/helphub/widget/HelpItemExpView$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/widget/HelpItemExpView$1;-><init>(Lcom/samsung/helphub/widget/HelpItemExpView;)V

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mIsExpanded:Z

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mHelpItemViewClickLister:Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mPosition:I

    .line 92
    new-instance v0, Lcom/samsung/helphub/widget/HelpItemExpView$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/widget/HelpItemExpView$1;-><init>(Lcom/samsung/helphub/widget/HelpItemExpView;)V

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mIsExpanded:Z

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mHelpItemViewClickLister:Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mPosition:I

    .line 92
    new-instance v0, Lcom/samsung/helphub/widget/HelpItemExpView$1;

    invoke-direct {v0, p0}, Lcom/samsung/helphub/widget/HelpItemExpView$1;-><init>(Lcom/samsung/helphub/widget/HelpItemExpView;)V

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/helphub/widget/HelpItemExpView;)Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpItemExpView;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mHelpItemViewClickLister:Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/helphub/widget/HelpItemExpView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/helphub/widget/HelpItemExpView;

    .prologue
    .line 23
    iget v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mPosition:I

    return v0
.end method


# virtual methods
.method public expand(Z)V
    .locals 3
    .param p1, "expanded"    # Z

    .prologue
    const/16 v2, 0x8

    .line 68
    if-eqz p1, :cond_0

    .line 71
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mHelpPageContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 73
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mFoldIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020084

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 74
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpItemExpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 85
    :goto_0
    iput-boolean p1, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mIsExpanded:Z

    .line 86
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mFoldIndicator:Landroid/widget/ImageView;

    const v1, 0x7f020083

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 80
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mSeparatorLine:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mHelpPageContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/helphub/widget/HelpItemExpView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09001b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mPosition:I

    return v0
.end method

.method public isExpanded()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mIsExpanded:Z

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 55
    const v0, 0x7f0d0088

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/widget/HelpItemExpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mHelpPageContainer:Landroid/view/ViewGroup;

    .line 56
    const v0, 0x7f0d0086

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/widget/HelpItemExpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mFoldIndicator:Landroid/widget/ImageView;

    .line 57
    const v0, 0x7f0d0112

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/widget/HelpItemExpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mSeparatorLine:Landroid/widget/ImageView;

    .line 58
    const v0, 0x7f0d008a

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/widget/HelpItemExpView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mTitle:Landroid/widget/TextView;

    .line 59
    iget-object v0, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/samsung/helphub/widget/HelpItemExpView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    return-void
.end method

.method public setListPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mPosition:I

    .line 46
    return-void
.end method

.method public setOnHelpItemViewClickListener(Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/samsung/helphub/widget/HelpItemExpView;->mHelpItemViewClickLister:Lcom/samsung/helphub/fragments/OnHelpItemViewClickListener;

    .line 90
    return-void
.end method
