.class Lcom/samsung/helphub/widget/HelpHubTextView$1;
.super Ljava/lang/Object;
.source "HelpHubTextView.java"

# interfaces
.implements Landroid/text/Html$ImageGetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/widget/HelpHubTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/helphub/widget/HelpHubTextView;


# direct methods
.method constructor <init>(Lcom/samsung/helphub/widget/HelpHubTextView;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/samsung/helphub/widget/HelpHubTextView$1;->this$0:Lcom/samsung/helphub/widget/HelpHubTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 309
    const/4 v0, 0x0

    .line 311
    .local v0, "commonDrawable":Lcom/samsung/helphub/widget/HelpHubDrawable;
    iget-object v3, p0, Lcom/samsung/helphub/widget/HelpHubTextView$1;->this$0:Lcom/samsung/helphub/widget/HelpHubTextView;

    invoke-virtual {v3}, Lcom/samsung/helphub/widget/HelpHubTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "drawable"

    iget-object v5, p0, Lcom/samsung/helphub/widget/HelpHubTextView$1;->this$0:Lcom/samsung/helphub/widget/HelpHubTextView;

    # getter for: Lcom/samsung/helphub/widget/HelpHubTextView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/helphub/widget/HelpHubTextView;->access$000(Lcom/samsung/helphub/widget/HelpHubTextView;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 314
    .local v2, "resID":I
    if-eqz v2, :cond_0

    .line 315
    iget-object v3, p0, Lcom/samsung/helphub/widget/HelpHubTextView$1;->this$0:Lcom/samsung/helphub/widget/HelpHubTextView;

    invoke-virtual {v3}, Lcom/samsung/helphub/widget/HelpHubTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    .line 317
    .local v1, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    new-instance v0, Lcom/samsung/helphub/widget/HelpHubDrawable;

    .end local v0    # "commonDrawable":Lcom/samsung/helphub/widget/HelpHubDrawable;
    invoke-direct {v0, v1}, Lcom/samsung/helphub/widget/HelpHubDrawable;-><init>(Landroid/graphics/drawable/BitmapDrawable;)V

    .line 320
    .end local v1    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    .restart local v0    # "commonDrawable":Lcom/samsung/helphub/widget/HelpHubDrawable;
    :cond_0
    iget-object v3, p0, Lcom/samsung/helphub/widget/HelpHubTextView$1;->this$0:Lcom/samsung/helphub/widget/HelpHubTextView;

    # getter for: Lcom/samsung/helphub/widget/HelpHubTextView;->mDrawables:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/helphub/widget/HelpHubTextView;->access$100(Lcom/samsung/helphub/widget/HelpHubTextView;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    iget-object v3, p0, Lcom/samsung/helphub/widget/HelpHubTextView$1;->this$0:Lcom/samsung/helphub/widget/HelpHubTextView;

    # invokes: Lcom/samsung/helphub/widget/HelpHubTextView;->invalidateDrawables()V
    invoke-static {v3}, Lcom/samsung/helphub/widget/HelpHubTextView;->access$200(Lcom/samsung/helphub/widget/HelpHubTextView;)V

    .line 323
    return-object v0
.end method
