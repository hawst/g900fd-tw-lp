.class public final Lcom/samsung/helphub/R$xml;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "xml"
.end annotation


# static fields
.field public static final help_categories:I = 0x7f060000

.field public static final help_categories_easy_mode:I = 0x7f060001

.field public static final helphub_widget_info:I = 0x7f060002

.field public static final item_cocktail_service:I = 0x7f060003

.field public static final items_accessibility:I = 0x7f060004

.field public static final items_account:I = 0x7f060005

.field public static final items_actionmemo:I = 0x7f060006

.field public static final items_air_button:I = 0x7f060007

.field public static final items_air_gesture:I = 0x7f060008

.field public static final items_air_view:I = 0x7f060009

.field public static final items_air_view_pen:I = 0x7f06000a

.field public static final items_air_view_pen_finger:I = 0x7f06000b

.field public static final items_allshare_cast:I = 0x7f06000c

.field public static final items_battery_conservation:I = 0x7f06000d

.field public static final items_bluetooth:I = 0x7f06000e

.field public static final items_camera:I = 0x7f06000f

.field public static final items_camera_2nd_lcd:I = 0x7f060010

.field public static final items_camera_2nd_lcd_chn:I = 0x7f060011

.field public static final items_camera_chn:I = 0x7f060012

.field public static final items_camera_easy_mode:I = 0x7f060013

.field public static final items_camera_easy_mode_2nd_lcd:I = 0x7f060014

.field public static final items_car_mode:I = 0x7f060015

.field public static final items_chartbuilder:I = 0x7f060016

.field public static final items_chaton_v:I = 0x7f060017

.field public static final items_contacts:I = 0x7f060018

.field public static final items_data_usage:I = 0x7f060019

.field public static final items_download_booster:I = 0x7f06001a

.field public static final items_dual_camera:I = 0x7f06001b

.field public static final items_easy_homescreen:I = 0x7f06001c

.field public static final items_easy_mode:I = 0x7f06001d

.field public static final items_edit_quick_settings:I = 0x7f06001e

.field public static final items_email:I = 0x7f06001f

.field public static final items_email_easy_mode:I = 0x7f060020

.field public static final items_emeeting:I = 0x7f060021

.field public static final items_fingerprints:I = 0x7f060022

.field public static final items_galaxyfinder:I = 0x7f060023

.field public static final items_gallery:I = 0x7f060024

.field public static final items_gallery_easy:I = 0x7f060025

.field public static final items_group_play:I = 0x7f060026

.field public static final items_guest_lounge:I = 0x7f060027

.field public static final items_homescreen:I = 0x7f060028

.field public static final items_keyboard:I = 0x7f060029

.field public static final items_keyboard_china:I = 0x7f06002a

.field public static final items_keys_2014:I = 0x7f06002b

.field public static final items_life_times:I = 0x7f06002c

.field public static final items_magazine_home:I = 0x7f06002d

.field public static final items_messaging:I = 0x7f06002e

.field public static final items_multi_window:I = 0x7f06002f

.field public static final items_my_magazine:I = 0x7f060030

.field public static final items_nfc:I = 0x7f060031

.field public static final items_notifications2:I = 0x7f060032

.field public static final items_onehand:I = 0x7f060033

.field public static final items_phone:I = 0x7f060034

.field public static final items_phone_2nd_lcd:I = 0x7f060035

.field public static final items_phone_easy_mode:I = 0x7f060036

.field public static final items_phone_only_contacts:I = 0x7f060037

.field public static final items_photo_reader:I = 0x7f060038

.field public static final items_power_saving_mode:I = 0x7f060039

.field public static final items_private_mode:I = 0x7f06003a

.field public static final items_ringtone:I = 0x7f06003b

.field public static final items_safety_assistance:I = 0x7f06003c

.field public static final items_sbrowser:I = 0x7f06003d

.field public static final items_sbrowser_easy_mode:I = 0x7f06003e

.field public static final items_sconnect:I = 0x7f06003f

.field public static final items_scrapbook:I = 0x7f060040

.field public static final items_section_photo_reader:I = 0x7f060041

.field public static final items_smart_clip:I = 0x7f060042

.field public static final items_smart_remote:I = 0x7f060043

.field public static final items_smart_screen:I = 0x7f060044

.field public static final items_smemo:I = 0x7f060045

.field public static final items_snote:I = 0x7f060046

.field public static final items_spen_gesture:I = 0x7f060047

.field public static final items_stock_browser:I = 0x7f060048

.field public static final items_story_album:I = 0x7f060049

.field public static final items_svoice:I = 0x7f06004a

.field public static final items_tethering:I = 0x7f06004b

.field public static final items_tethering_tmo:I = 0x7f06004c

.field public static final items_tethering_vzw:I = 0x7f06004d

.field public static final items_translator:I = 0x7f06004e

.field public static final items_unlock:I = 0x7f06004f

.field public static final items_unlock_2014:I = 0x7f060050

.field public static final items_unlock_easy_mode:I = 0x7f060051

.field public static final items_upgrade:I = 0x7f060052

.field public static final items_voice_control:I = 0x7f060053

.field public static final items_wifi:I = 0x7f060054

.field public static final items_wifi_chn:I = 0x7f060055

.field public static final items_writing_buddy:I = 0x7f060056

.field public static final searchable:I = 0x7f060057

.field public static final sections_applications_2014:I = 0x7f060058

.field public static final sections_applications_2014_easy_mode:I = 0x7f060059

.field public static final sections_basic:I = 0x7f06005a

.field public static final sections_basic_easy_mode:I = 0x7f06005b

.field public static final sections_basics_2014:I = 0x7f06005c

.field public static final sections_basics_2014_easy_mode:I = 0x7f06005d

.field public static final sections_new_features_2014:I = 0x7f06005e

.field public static final sections_new_features_2014_easy_mode:I = 0x7f06005f

.field public static final sections_settings_2014:I = 0x7f060060

.field public static final sections_settings_2014_easy_mode:I = 0x7f060061


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
