.class public final Lcom/samsung/helphub/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AppTheme:I = 0x7f0e0000

.field public static final Dialog_Button:I = 0x7f0e003d

.field public static final Dialog_HeaderText:I = 0x7f0e003b

.field public static final Dialog_MainText:I = 0x7f0e003c

.field public static final ExpandableList:I = 0x7f0e000b

.field public static final ExpandableListItemTitle:I = 0x7f0e002e

.field public static final GestureGuideListItemImage:I = 0x7f0e005f

.field public static final GestureGuideListItemLayout:I = 0x7f0e005d

.field public static final GestureGuideListItemText:I = 0x7f0e005e

.field public static final HelpCameraIntroductionListItem:I = 0x7f0e005b

.field public static final HelpCameraIntroductionListItemNumber:I = 0x7f0e005c

.field public static final HelpCameraListItem:I = 0x7f0e0042

.field public static final HelpCameraListItemNumber:I = 0x7f0e0044

.field public static final HelpCameraListItem_HelphubTextView:I = 0x7f0e0043

.field public static final HelpCameraSubPageTitle:I = 0x7f0e004b

.field public static final HelpCameraTipLayout:I = 0x7f0e0045

.field public static final HelpCameraTipListItem:I = 0x7f0e0047

.field public static final HelpCameraTipListItemDot:I = 0x7f0e004a

.field public static final HelpCameraTipListItemNumber:I = 0x7f0e0049

.field public static final HelpCameraTipListItem_HelphubTextView:I = 0x7f0e0048

.field public static final HelpCameraTipTitle:I = 0x7f0e0046

.field public static final HelpEmptyView:I = 0x7f0e000d

.field public static final HelpGrayListItem:I = 0x7f0e003f

.field public static final HelpKeyboardListBodyText:I = 0x7f0e0054

.field public static final HelpKeyboardListBodyText_HelphubTextView:I = 0x7f0e0055

.field public static final HelpKeyboardListTitle:I = 0x7f0e0050

.field public static final HelpKeyboardListTitleText:I = 0x7f0e0051

.field public static final HelpKeyboardListTitleText_HelphubTextView:I = 0x7f0e0052

.field public static final HelpKeyboardListTitleText_Number:I = 0x7f0e0053

.field public static final HelpKeyboardTip:I = 0x7f0e0056

.field public static final HelpKeyboardTipBody:I = 0x7f0e0058

.field public static final HelpKeyboardTipBody_HelphubTextView:I = 0x7f0e0059

.field public static final HelpKeyboardTipTitle:I = 0x7f0e0057

.field public static final HelpListItem:I = 0x7f0e000e

.field public static final HelpListItemButton:I = 0x7f0e002b

.field public static final HelpListItemButtonMulti:I = 0x7f0e002d

.field public static final HelpListItemImage:I = 0x7f0e0022

.field public static final HelpListItemImage_ContinuousImage:I = 0x7f0e0025

.field public static final HelpListItemImage_InnerImage:I = 0x7f0e0023

.field public static final HelpListItemImage_InnerImage_InnerCenter:I = 0x7f0e0027

.field public static final HelpListItemImage_InnerKeyboardImage:I = 0x7f0e0024

.field public static final HelpListItemImage_LastItem:I = 0x7f0e002a

.field public static final HelpListItemImage_NoMarginImage:I = 0x7f0e0026

.field public static final HelpListItemImage_Slides:I = 0x7f0e0030

.field public static final HelpListItemImage_Slides_Autoplay:I = 0x7f0e0031

.field public static final HelpListItemLayout:I = 0x7f0e0028

.field public static final HelpListItemLayout_InnerLayout:I = 0x7f0e0029

.field public static final HelpListItemLeftMarginKeyboard:I = 0x7f0e0018

.field public static final HelpListItemNumber:I = 0x7f0e001f

.field public static final HelpListItemNumber0:I = 0x7f0e0020

.field public static final HelpListItem_HelphubTextView:I = 0x7f0e0019

.field public static final HelpListItem_HelphubTextViewCamera:I = 0x7f0e001b

.field public static final HelpListItem_HelphubTextView_MoreInfo:I = 0x7f0e001a

.field public static final HelpListItem_HelphubTextView_NoLeftMargin:I = 0x7f0e001e

.field public static final HelpListItem_HelphubTextView_NoScale:I = 0x7f0e001c

.field public static final HelpListItem_InGray:I = 0x7f0e0010

.field public static final HelpListItem_InKeyboard:I = 0x7f0e0012

.field public static final HelpListItem_InKeyboardLastLine:I = 0x7f0e0013

.field public static final HelpListItem_InKeyboard_NoLeftMargin:I = 0x7f0e0014

.field public static final HelpListItem_KeyboardSubTitle:I = 0x7f0e000f

.field public static final HelpListItem_NoLeftMargin:I = 0x7f0e0016

.field public static final HelpListItem_NoLeftMargin_NoPaddingBottom:I = 0x7f0e0017

.field public static final HelpListItem_NoRightMargin:I = 0x7f0e0015

.field public static final HelpListItem_Single:I = 0x7f0e001d

.field public static final HelpListItem_SubTitle:I = 0x7f0e0011

.field public static final HelpListSearchOnlineButton:I = 0x7f0e002c

.field public static final HelpMyMagazineTextStyle:I = 0x7f0e005a

.field public static final HelpNotificationsPageLayout:I = 0x7f0e004f

.field public static final HelpNotificationsPanelTip:I = 0x7f0e004d

.field public static final HelpNotificationsPanelTipDesc:I = 0x7f0e004e

.field public static final HelpPageDivider:I = 0x7f0e000c

.field public static final HelpPageLayoutBase:I = 0x7f0e0009

.field public static final HelpPageSubGroupTitle:I = 0x7f0e003e

.field public static final HelpPageTipBoxInnerDot:I = 0x7f0e0039

.field public static final HelpPageTipBoxInnerLayout:I = 0x7f0e0034

.field public static final HelpPageTipBoxInnerText:I = 0x7f0e0035

.field public static final HelpPageTipBoxInnerText_Image:I = 0x7f0e0037

.field public static final HelpPageTipBoxInnerText_Image_NoMargin:I = 0x7f0e0038

.field public static final HelpPageTipBoxInnerText_NoMargin:I = 0x7f0e0036

.field public static final HelpPageTipBoxLayout:I = 0x7f0e0032

.field public static final HelpPageTipBoxTitle:I = 0x7f0e0033

.field public static final HelpPageTitle:I = 0x7f0e0021

.field public static final MainActivityGroupCaption:I = 0x7f0e0003

.field public static final MainActivityListCategoryCaption:I = 0x7f0e0004

.field public static final MainActivitySectionBase:I = 0x7f0e0006

.field public static final MainActivitySectionGrid:I = 0x7f0e0007

.field public static final MainActivitySectionItem:I = 0x7f0e0001

.field public static final MainActivitySectionList:I = 0x7f0e0008

.field public static final MainActivitySectionListItem:I = 0x7f0e0002

.field public static final MainActivityWelcomeTextBase:I = 0x7f0e0005

.field public static final NumberedList:I = 0x7f0e000a

.field public static final Theme_Dialog:I = 0x7f0e003a

.field public static final TipsCheckbox:I = 0x7f0e002f

.field public static final TutorialNotiPopupStyle:I = 0x7f0e0040

.field public static final TutorialPopupStyle:I = 0x7f0e0041

.field public static final TutorialTipStyle:I = 0x7f0e004c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
