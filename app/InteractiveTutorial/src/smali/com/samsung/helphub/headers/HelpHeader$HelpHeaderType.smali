.class public final enum Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
.super Ljava/lang/Enum;
.source "HelpHeader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/helphub/headers/HelpHeader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HelpHeaderType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

.field public static final enum CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

.field public static final enum ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

.field public static final enum SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    const-string v1, "CATEGORY"

    invoke-direct {v0, v1, v2}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 27
    new-instance v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    const-string v1, "SECTION"

    invoke-direct {v0, v1, v3}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 29
    new-instance v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    const-string v1, "ITEM"

    invoke-direct {v0, v1, v4}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 19
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    sget-object v1, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->$VALUES:[Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->$VALUES:[Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    invoke-virtual {v0}, [Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    return-object v0
.end method
