.class public Lcom/samsung/helphub/headers/HelpHeaderLayout;
.super Ljava/lang/Object;
.source "HelpHeaderLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/headers/HelpHeaderLayout$1;,
        Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;
    }
.end annotation


# instance fields
.field private mHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resXmlId"    # I

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 22
    .local v0, "headers":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-static {p1, p2, v0}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->loadHeadersFromResource(Landroid/content/Context;ILjava/util/List;)V

    .line 23
    invoke-direct {p0, p1, v0}, Lcom/samsung/helphub/headers/HelpHeaderLayout;->init(Landroid/content/Context;Ljava/util/List;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p2, "headers":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/samsung/helphub/headers/HelpHeaderLayout;->init(Landroid/content/Context;Ljava/util/List;)V

    .line 28
    return-void
.end method

.method private filterHeader(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    move-object v3, p1

    .line 52
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v4, 0x0

    invoke-static {v4}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-object v3

    .line 54
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 55
    .local v0, "count":I
    add-int/lit8 v2, v0, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_0

    .line 56
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/headers/HelpHeader;

    .line 58
    .local v1, "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v4

    const v5, 0x7f0d009b

    if-ne v4, v5, :cond_2

    .line 59
    invoke-interface {p1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 55
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1
.end method

.method private getHelpHeaderType(I)Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeaderLayout;->mHeaders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v0

    return-object v0
.end method

.method private init(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p2, "headers":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-direct {p0, p2}, Lcom/samsung/helphub/headers/HelpHeaderLayout;->filterHeader(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeaderLayout;->mHeaders:Ljava/util/List;

    .line 32
    return-void
.end method


# virtual methods
.method public getAppropiateResource(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 69
    sget-object v1, Lcom/samsung/helphub/headers/HelpHeaderLayout$1;->$SwitchMap$com$samsung$helphub$headers$HelpHeader$HelpHeaderType:[I

    invoke-direct {p0, p1}, Lcom/samsung/helphub/headers/HelpHeaderLayout;->getHelpHeaderType(I)Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 85
    const/4 v0, 0x0

    .line 87
    .local v0, "mResource":I
    :goto_0
    return v0

    .line 71
    .end local v0    # "mResource":I
    :pswitch_0
    const v0, 0x7f0400b7

    .line 72
    .restart local v0    # "mResource":I
    goto :goto_0

    .line 75
    .end local v0    # "mResource":I
    :pswitch_1
    const v0, 0x7f0400c1

    .line 76
    .restart local v0    # "mResource":I
    goto :goto_0

    .line 79
    .end local v0    # "mResource":I
    :pswitch_2
    const v0, 0x7f0400c1

    .line 80
    .restart local v0    # "mResource":I
    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeaderLayout;->mHeaders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public setHolderTag(Landroid/view/View;I)V
    .locals 7
    .param p1, "mView"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 92
    new-instance v1, Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;

    invoke-direct {v1, v6}, Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;-><init>(Lcom/samsung/helphub/headers/HelpHeaderLayout$1;)V

    .line 93
    .local v1, "holder":Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;
    const v3, 0x7f0d010c

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;->title:Landroid/widget/TextView;

    .line 94
    const v3, 0x7f0d010f

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    .line 95
    const v3, 0x7f0d0007

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;->Devider:Landroid/widget/ImageView;

    .line 96
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 97
    invoke-virtual {p0, p2}, Lcom/samsung/helphub/headers/HelpHeaderLayout;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    .line 98
    .local v0, "header":Lcom/samsung/helphub/headers/HelpHeader;
    iget-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 99
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 100
    const v3, 0x7f0d0006

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 101
    .local v2, "ll":Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getIconId()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 102
    iget-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    .line 103
    iget-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getIconId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 106
    :cond_0
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v6}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 143
    :cond_1
    :goto_0
    return-void

    .line 111
    :cond_2
    if-nez p2, :cond_4

    .line 113
    if-eqz v2, :cond_3

    .line 115
    const v3, 0x7f020052

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 117
    :cond_3
    iget-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;->Devider:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    .line 119
    iget-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;->Devider:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 121
    :cond_4
    iget-object v3, p0, Lcom/samsung/helphub/headers/HelpHeaderLayout;->mHeaders:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne p2, v3, :cond_6

    .line 123
    if-eqz v2, :cond_5

    .line 125
    const v3, 0x7f020050

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 127
    :cond_5
    iget-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;->Devider:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    .line 129
    iget-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;->Devider:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 133
    :cond_6
    if-eqz v2, :cond_7

    .line 135
    const v3, 0x7f020051

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 137
    :cond_7
    iget-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;->Devider:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    .line 139
    iget-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderLayout$HeaderViewHolder;->Devider:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
