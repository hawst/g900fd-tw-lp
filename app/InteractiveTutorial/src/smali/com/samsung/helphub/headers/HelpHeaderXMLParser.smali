.class public final Lcom/samsung/helphub/headers/HelpHeaderXMLParser;
.super Ljava/lang/Object;
.source "HelpHeaderXMLParser.java"


# static fields
.field private static final CATEGORY_TAG:Ljava/lang/String; = "category"

.field private static final HEADERS_TAG:Ljava/lang/String; = "help-headers"

.field private static final ITEM_TAG:Ljava/lang/String; = "item"

.field private static final SECTION_TAG:Ljava/lang/String; = "section"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    return-void
.end method

.method private static fillHeaderItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/HelpHeader;
    .locals 6
    .param p0, "parser"    # Landroid/content/res/XmlResourceParser;
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "type"    # Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 472
    invoke-static {p0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    .line 473
    .local v0, "attrs":Landroid/util/AttributeSet;
    new-instance v1, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-direct {v1, p2}, Lcom/samsung/helphub/headers/HelpHeader;-><init>(Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)V

    .line 475
    .local v1, "header":Lcom/samsung/helphub/headers/HelpHeader;
    sget-object v3, Lcom/samsung/helphub/R$styleable;->Header:[I

    invoke-virtual {p1, v0, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 476
    .local v2, "sa":Landroid/content/res/TypedArray;
    invoke-virtual {v2, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mId:I

    .line 479
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mTitleId:I

    .line 481
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs1Id:I

    .line 483
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs2Id:I

    .line 485
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryId:I

    .line 487
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs1Id:I

    .line 489
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs2Id:I

    .line 491
    const/16 v3, 0xa

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mActivityTitleId:I

    .line 493
    const/4 v3, 0x5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mIconId:I

    .line 495
    const/16 v3, 0xb

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mSubHeadersId:I

    .line 497
    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentName:Ljava/lang/String;

    .line 498
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mItemLayoutID:I

    .line 500
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mName:Ljava/lang/String;

    .line 501
    const/16 v3, 0xd

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mDictionaryId:I

    .line 503
    const/16 v3, 0xe

    invoke-virtual {v2, v3, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mIsPluginRes:Z

    .line 504
    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/samsung/helphub/headers/HelpHeader;->mPluginAppId:Ljava/lang/String;

    .line 505
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 507
    return-object v1
.end method

.method private static fillSearchItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/SearchHeader;
    .locals 6
    .param p0, "parser"    # Landroid/content/res/XmlResourceParser;
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "type"    # Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 561
    invoke-static {p0}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    .line 562
    .local v0, "attrs":Landroid/util/AttributeSet;
    new-instance v1, Lcom/samsung/helphub/headers/SearchHeader;

    invoke-direct {v1}, Lcom/samsung/helphub/headers/SearchHeader;-><init>()V

    .line 563
    .local v1, "result":Lcom/samsung/helphub/headers/SearchHeader;
    iput-object p2, v1, Lcom/samsung/helphub/headers/SearchHeader;->type:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 565
    sget-object v3, Lcom/samsung/helphub/R$styleable;->Header:[I

    invoke-virtual {p1, v0, v3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 566
    .local v2, "sa":Landroid/content/res/TypedArray;
    invoke-virtual {v2, v5, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/SearchHeader;->itemID:I

    .line 567
    const/16 v3, 0xb

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/SearchHeader;->itemsXmlId:I

    .line 569
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/samsung/helphub/headers/SearchHeader;->name:Ljava/lang/String;

    .line 570
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    iput v3, v1, Lcom/samsung/helphub/headers/SearchHeader;->titleID:I

    .line 572
    const/16 v3, 0xe

    invoke-virtual {v2, v3, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, v1, Lcom/samsung/helphub/headers/SearchHeader;->isPluginRes:Z

    .line 573
    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/samsung/helphub/headers/SearchHeader;->pluginAppId:Ljava/lang/String;

    .line 574
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 576
    return-object v1
.end method

.method private static findAllHeaders(Ljava/util/List;ILandroid/content/Context;)Ljava/util/List;
    .locals 12
    .param p1, "entryXmlID"    # I
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;I",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 398
    .local p0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    move v8, p1

    .line 402
    .local v8, "xmlID":I
    if-gtz v8, :cond_0

    .line 403
    invoke-static {p2}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->getMainXmlID(Landroid/content/Context;)I

    move-result v8

    .line 406
    :cond_0
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 409
    .local v7, "res":Landroid/content/res/Resources;
    :try_start_0
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v6

    .line 411
    .local v6, "parser":Landroid/content/res/XmlResourceParser;
    invoke-static {v6}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->moveToStartTag(Landroid/content/res/XmlResourceParser;)V

    .line 413
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 414
    .local v3, "initialNodeName":Ljava/lang/String;
    const-string v9, "help-headers"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 415
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "XML document must start with <guide-sections> tag; found"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " at "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->getPositionDescription()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 446
    .end local v3    # "initialNodeName":Ljava/lang/String;
    .end local v6    # "parser":Landroid/content/res/XmlResourceParser;
    :catch_0
    move-exception v0

    .line 447
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 454
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :goto_0
    return-object p0

    .line 420
    .restart local v3    # "initialNodeName":Ljava/lang/String;
    .restart local v6    # "parser":Landroid/content/res/XmlResourceParser;
    :cond_2
    :try_start_2
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v1

    .line 421
    .local v1, "event":I
    :goto_1
    const/4 v9, 0x1

    if-eq v1, v9, :cond_1

    .line 422
    const/4 v9, 0x3

    if-eq v1, v9, :cond_3

    const/4 v9, 0x4

    if-ne v1, v9, :cond_4

    .line 423
    :cond_3
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v1

    .line 424
    goto :goto_1

    .line 427
    :cond_4
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 428
    .local v5, "nodeName":Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->getHeaderType(Ljava/lang/String;)Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v2

    .line 429
    .local v2, "headerType":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    if-nez v2, :cond_5

    .line 430
    invoke-static {v6}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 443
    :goto_2
    invoke-interface {v6}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v1

    .line 444
    goto :goto_1

    .line 433
    :cond_5
    invoke-static {v6, v7, v2}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->fillSearchItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/SearchHeader;

    move-result-object v4

    .line 434
    .local v4, "item":Lcom/samsung/helphub/headers/SearchHeader;
    if-eqz v4, :cond_6

    .line 435
    iget v9, v4, Lcom/samsung/helphub/headers/SearchHeader;->itemsXmlId:I

    if-lez v9, :cond_6

    .line 436
    iget v9, v4, Lcom/samsung/helphub/headers/SearchHeader;->itemsXmlId:I

    invoke-static {p0, v9, p2}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->findAllHeaders(Ljava/util/List;ILandroid/content/Context;)Ljava/util/List;

    .line 440
    :cond_6
    invoke-static {v6, v7, v2}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->fillHeaderItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v9

    invoke-interface {p0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 448
    .end local v1    # "event":I
    .end local v2    # "headerType":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .end local v3    # "initialNodeName":Ljava/lang/String;
    .end local v4    # "item":Lcom/samsung/helphub/headers/SearchHeader;
    .end local v5    # "nodeName":Ljava/lang/String;
    .end local v6    # "parser":Landroid/content/res/XmlResourceParser;
    :catch_1
    move-exception v0

    .line 449
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_3
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 450
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_0
    move-exception v9

    throw v9
.end method

.method private static findAllSections(Ljava/util/List;ILandroid/content/Context;)Ljava/util/List;
    .locals 21
    .param p1, "entryXmlID"    # I
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;I",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290
    .local p0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    move/from16 v17, p1

    .line 294
    .local v17, "xmlID":I
    if-gtz v17, :cond_0

    .line 295
    invoke-static/range {p2 .. p2}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->getMainXmlID(Landroid/content/Context;)I

    move-result v17

    .line 298
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 301
    .local v16, "res":Landroid/content/res/Resources;
    :try_start_0
    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v15

    .line 303
    .local v15, "parser":Landroid/content/res/XmlResourceParser;
    invoke-static {v15}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->moveToStartTag(Landroid/content/res/XmlResourceParser;)V

    .line 305
    invoke-interface {v15}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v9

    .line 306
    .local v9, "initialNodeName":Ljava/lang/String;
    const-string v18, "help-headers"

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_2

    .line 307
    new-instance v18, Ljava/lang/RuntimeException;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "XML document must start with <guide-sections> tag; found"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " at "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-interface {v15}, Landroid/content/res/XmlResourceParser;->getPositionDescription()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v18
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 360
    .end local v9    # "initialNodeName":Ljava/lang/String;
    .end local v15    # "parser":Landroid/content/res/XmlResourceParser;
    :catch_0
    move-exception v5

    .line 361
    .local v5, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 368
    .end local v5    # "e":Ljava/io/IOException;
    :cond_1
    :goto_0
    return-object p0

    .line 312
    .restart local v9    # "initialNodeName":Ljava/lang/String;
    .restart local v15    # "parser":Landroid/content/res/XmlResourceParser;
    :cond_2
    :try_start_2
    invoke-static/range {p2 .. p2}, Lcom/samsung/helphub/headers/HelpHeaderHideManager;->getHiddenCategoryHeaderId(Landroid/content/Context;)[I

    move-result-object v13

    .line 313
    .local v13, "mHidedCategoryHeaderId":[I
    invoke-interface {v15}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v6

    .line 314
    .local v6, "event":I
    :goto_1
    const/16 v18, 0x1

    move/from16 v0, v18

    if-eq v6, v0, :cond_1

    .line 315
    const/16 v18, 0x3

    move/from16 v0, v18

    if-eq v6, v0, :cond_3

    const/16 v18, 0x4

    move/from16 v0, v18

    if-ne v6, v0, :cond_4

    .line 316
    :cond_3
    invoke-interface {v15}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v6

    .line 317
    goto :goto_1

    .line 320
    :cond_4
    invoke-interface {v15}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v14

    .line 321
    .local v14, "nodeName":Ljava/lang/String;
    invoke-static {v14}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->getHeaderType(Ljava/lang/String;)Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v7

    .line 322
    .local v7, "headerType":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    if-nez v7, :cond_6

    .line 323
    invoke-static {v15}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 357
    :cond_5
    :goto_2
    invoke-interface {v15}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v6

    .line 358
    goto :goto_1

    .line 326
    :cond_6
    move-object/from16 v0, v16

    invoke-static {v15, v0, v7}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->fillSearchItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/SearchHeader;

    move-result-object v11

    .line 327
    .local v11, "item":Lcom/samsung/helphub/headers/SearchHeader;
    if-eqz v11, :cond_5

    .line 328
    iget v0, v11, Lcom/samsung/helphub/headers/SearchHeader;->itemsXmlId:I

    move/from16 v18, v0

    if-lez v18, :cond_a

    .line 329
    const/4 v10, 0x0

    .line 330
    .local v10, "isFiltered":Z
    move-object v3, v13

    .local v3, "arr$":[I
    array-length v12, v3

    .local v12, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_3
    if-ge v8, v12, :cond_7

    aget v4, v3, v8

    .line 331
    .local v4, "category":I
    iget v0, v11, Lcom/samsung/helphub/headers/SearchHeader;->itemID:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ne v0, v4, :cond_8

    .line 332
    const/4 v10, 0x1

    .line 336
    .end local v4    # "category":I
    :cond_7
    if-eqz v10, :cond_9

    .line 337
    invoke-interface {v15}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v6

    .line 338
    goto :goto_1

    .line 330
    .restart local v4    # "category":I
    :cond_8
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 340
    .end local v4    # "category":I
    :cond_9
    iget-object v0, v11, Lcom/samsung/helphub/headers/SearchHeader;->type:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_a

    .line 341
    iget v0, v11, Lcom/samsung/helphub/headers/SearchHeader;->itemsXmlId:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->findAllSections(Ljava/util/List;ILandroid/content/Context;)Ljava/util/List;

    .line 345
    .end local v3    # "arr$":[I
    .end local v8    # "i$":I
    .end local v10    # "isFiltered":Z
    .end local v12    # "len$":I
    :cond_a
    iget-object v0, v11, Lcom/samsung/helphub/headers/SearchHeader;->type:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-object/from16 v18, v0

    sget-object v19, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_5

    .line 346
    invoke-static/range {p2 .. p2}, Lcom/samsung/helphub/HelpHubCommon;->isDownloadable(Landroid/content/Context;)Z

    move-result v18

    if-eqz v18, :cond_b

    iget-boolean v0, v11, Lcom/samsung/helphub/headers/SearchHeader;->isPluginRes:Z

    move/from16 v18, v0

    if-eqz v18, :cond_b

    .line 347
    iget-object v0, v11, Lcom/samsung/helphub/headers/SearchHeader;->pluginAppId:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->isPluginInstalled(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 348
    move-object/from16 v0, v16

    invoke-static {v15, v0, v7}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->fillHeaderItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 362
    .end local v6    # "event":I
    .end local v7    # "headerType":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .end local v9    # "initialNodeName":Ljava/lang/String;
    .end local v11    # "item":Lcom/samsung/helphub/headers/SearchHeader;
    .end local v13    # "mHidedCategoryHeaderId":[I
    .end local v14    # "nodeName":Ljava/lang/String;
    .end local v15    # "parser":Landroid/content/res/XmlResourceParser;
    :catch_1
    move-exception v5

    .line 363
    .local v5, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_3
    invoke-virtual {v5}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 364
    .end local v5    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_0
    move-exception v18

    throw v18

    .line 351
    .restart local v6    # "event":I
    .restart local v7    # "headerType":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .restart local v9    # "initialNodeName":Ljava/lang/String;
    .restart local v11    # "item":Lcom/samsung/helphub/headers/SearchHeader;
    .restart local v13    # "mHidedCategoryHeaderId":[I
    .restart local v14    # "nodeName":Ljava/lang/String;
    .restart local v15    # "parser":Landroid/content/res/XmlResourceParser;
    :cond_b
    :try_start_4
    move-object/from16 v0, v16

    invoke-static {v15, v0, v7}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->fillHeaderItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2
.end method

.method public static findExternalHeader(Ljava/lang/String;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;Landroid/content/Context;)Lcom/samsung/helphub/headers/HelpHeader;
    .locals 3
    .param p0, "requestName"    # Ljava/lang/String;
    .param p1, "requestedType"    # Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 113
    const/4 v1, 0x0

    .line 114
    .local v1, "result":Lcom/samsung/helphub/headers/HelpHeader;
    const/4 v2, -0x1

    invoke-static {p0, p1, v2, p2}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->findHeader(Ljava/lang/String;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;ILandroid/content/Context;)Lcom/samsung/helphub/headers/SearchHeader;

    move-result-object v0

    .line 115
    .local v0, "header":Lcom/samsung/helphub/headers/SearchHeader;
    if-eqz v0, :cond_0

    .line 116
    iget-object v1, v0, Lcom/samsung/helphub/headers/SearchHeader;->helpHeader:Lcom/samsung/helphub/headers/HelpHeader;

    .line 118
    :cond_0
    return-object v1
.end method

.method private static findHeader(Ljava/lang/String;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;ILandroid/content/Context;)Lcom/samsung/helphub/headers/SearchHeader;
    .locals 18
    .param p0, "requestedName"    # Ljava/lang/String;
    .param p1, "requestedType"    # Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .param p2, "entryXmlID"    # I
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 134
    const/4 v12, 0x0

    .line 135
    .local v12, "result":Lcom/samsung/helphub/headers/SearchHeader;
    move/from16 v14, p2

    .line 138
    .local v14, "xmlID":I
    if-gtz v14, :cond_0

    .line 139
    invoke-static/range {p3 .. p3}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->getMainXmlID(Landroid/content/Context;)I

    move-result v14

    .line 142
    :cond_0
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 145
    .local v11, "res":Landroid/content/res/Resources;
    :try_start_0
    invoke-virtual {v11, v14}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v9

    .line 147
    .local v9, "parser":Landroid/content/res/XmlResourceParser;
    invoke-static {v9}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->moveToStartTag(Landroid/content/res/XmlResourceParser;)V

    .line 149
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v6

    .line 150
    .local v6, "initialNodeName":Ljava/lang/String;
    const-string v15, "help-headers"

    invoke-virtual {v15, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 151
    new-instance v15, Ljava/lang/RuntimeException;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "XML document must start with <guide-sections> tag; found"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " at "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getPositionDescription()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v15
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    .end local v6    # "initialNodeName":Ljava/lang/String;
    .end local v9    # "parser":Landroid/content/res/XmlResourceParser;
    :catch_0
    move-exception v3

    .line 193
    .local v3, "e":Ljava/io/IOException;
    :goto_0
    :try_start_1
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    .end local v3    # "e":Ljava/io/IOException;
    :cond_1
    :goto_1
    return-object v12

    .line 156
    .restart local v6    # "initialNodeName":Ljava/lang/String;
    .restart local v9    # "parser":Landroid/content/res/XmlResourceParser;
    :cond_2
    :try_start_2
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v4

    .line 157
    .local v4, "event":I
    :goto_2
    const/4 v15, 0x1

    if-eq v4, v15, :cond_1

    .line 158
    const/4 v15, 0x3

    if-eq v4, v15, :cond_3

    const/4 v15, 0x4

    if-ne v4, v15, :cond_4

    .line 159
    :cond_3
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v4

    .line 160
    goto :goto_2

    .line 163
    :cond_4
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v8

    .line 164
    .local v8, "nodeName":Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->getHeaderType(Ljava/lang/String;)Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v5

    .line 165
    .local v5, "headerType":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    if-nez v5, :cond_6

    .line 166
    invoke-static {v9}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 189
    :cond_5
    invoke-interface {v9}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v4

    .line 190
    goto :goto_2

    .line 169
    :cond_6
    invoke-static {v9, v11, v5}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->fillSearchItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/SearchHeader;

    move-result-object v7

    .line 170
    .local v7, "item":Lcom/samsung/helphub/headers/SearchHeader;
    if-eqz v7, :cond_5

    .line 171
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    iget-object v15, v7, Lcom/samsung/helphub/headers/SearchHeader;->type:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    iget-object v15, v7, Lcom/samsung/helphub/headers/SearchHeader;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 174
    new-instance v13, Lcom/samsung/helphub/headers/SearchHeader;

    invoke-direct {v13}, Lcom/samsung/helphub/headers/SearchHeader;-><init>()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 175
    .end local v12    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    .local v13, "result":Lcom/samsung/helphub/headers/SearchHeader;
    :try_start_3
    iget-object v15, v7, Lcom/samsung/helphub/headers/SearchHeader;->type:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    invoke-static {v9, v11, v15}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->fillHeaderItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v15

    iput-object v15, v13, Lcom/samsung/helphub/headers/SearchHeader;->helpHeader:Lcom/samsung/helphub/headers/HelpHeader;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v12, v13

    .line 176
    .end local v13    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    .restart local v12    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    goto :goto_1

    .line 177
    :cond_7
    :try_start_4
    iget v15, v7, Lcom/samsung/helphub/headers/SearchHeader;->itemsXmlId:I

    if-lez v15, :cond_5

    .line 178
    iget v15, v7, Lcom/samsung/helphub/headers/SearchHeader;->itemsXmlId:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v1, v15, v2}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->findHeader(Ljava/lang/String;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;ILandroid/content/Context;)Lcom/samsung/helphub/headers/SearchHeader;

    move-result-object v10

    .line 180
    .local v10, "recursiveHeader":Lcom/samsung/helphub/headers/SearchHeader;
    if-eqz v10, :cond_5

    iget-object v15, v10, Lcom/samsung/helphub/headers/SearchHeader;->helpHeader:Lcom/samsung/helphub/headers/HelpHeader;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v15, :cond_5

    .line 182
    move-object v12, v10

    .line 183
    goto :goto_1

    .line 194
    .end local v4    # "event":I
    .end local v5    # "headerType":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .end local v6    # "initialNodeName":Ljava/lang/String;
    .end local v7    # "item":Lcom/samsung/helphub/headers/SearchHeader;
    .end local v8    # "nodeName":Ljava/lang/String;
    .end local v9    # "parser":Landroid/content/res/XmlResourceParser;
    .end local v10    # "recursiveHeader":Lcom/samsung/helphub/headers/SearchHeader;
    :catch_1
    move-exception v3

    .line 195
    .local v3, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_3
    :try_start_5
    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 196
    .end local v3    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_0
    move-exception v15

    :goto_4
    throw v15

    .end local v12    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    .restart local v4    # "event":I
    .restart local v5    # "headerType":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .restart local v6    # "initialNodeName":Ljava/lang/String;
    .restart local v7    # "item":Lcom/samsung/helphub/headers/SearchHeader;
    .restart local v8    # "nodeName":Ljava/lang/String;
    .restart local v9    # "parser":Landroid/content/res/XmlResourceParser;
    .restart local v13    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    :catchall_1
    move-exception v15

    move-object v12, v13

    .end local v13    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    .restart local v12    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    goto :goto_4

    .line 194
    .end local v12    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    .restart local v13    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    :catch_2
    move-exception v3

    move-object v12, v13

    .end local v13    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    .restart local v12    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    goto :goto_3

    .line 192
    .end local v12    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    .restart local v13    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    :catch_3
    move-exception v3

    move-object v12, v13

    .end local v13    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    .restart local v12    # "result":Lcom/samsung/helphub/headers/SearchHeader;
    goto :goto_0
.end method

.method private static findHeaderByTitle(Ljava/lang/String;Ljava/util/List;ILandroid/content/Context;)Ljava/util/List;
    .locals 18
    .param p0, "key"    # Ljava/lang/String;
    .param p2, "entryXmlID"    # I
    .param p3, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;I",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    move/from16 v14, p2

    .line 214
    .local v14, "xmlID":I
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v9

    .line 218
    .local v9, "key_length":I
    if-gtz v14, :cond_0

    .line 219
    invoke-static/range {p3 .. p3}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->getMainXmlID(Landroid/content/Context;)I

    move-result v14

    .line 222
    :cond_0
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 225
    .local v12, "res":Landroid/content/res/Resources;
    :try_start_0
    invoke-virtual {v12, v14}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v11

    .line 227
    .local v11, "parser":Landroid/content/res/XmlResourceParser;
    invoke-static {v11}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->moveToStartTag(Landroid/content/res/XmlResourceParser;)V

    .line 229
    invoke-interface {v11}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v7

    .line 230
    .local v7, "initialNodeName":Ljava/lang/String;
    const-string v15, "help-headers"

    invoke-virtual {v15, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 231
    new-instance v15, Ljava/lang/RuntimeException;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "XML document must start with <guide-sections> tag; found"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " at "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-interface {v11}, Landroid/content/res/XmlResourceParser;->getPositionDescription()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v15
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    .end local v7    # "initialNodeName":Ljava/lang/String;
    .end local v11    # "parser":Landroid/content/res/XmlResourceParser;
    :catch_0
    move-exception v3

    .line 278
    .local v3, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 285
    .end local v3    # "e":Ljava/io/IOException;
    :cond_1
    :goto_0
    return-object p1

    .line 236
    .restart local v7    # "initialNodeName":Ljava/lang/String;
    .restart local v11    # "parser":Landroid/content/res/XmlResourceParser;
    :cond_2
    :try_start_2
    invoke-interface {v11}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v4

    .line 237
    .local v4, "event":I
    :goto_1
    const/4 v15, 0x1

    if-eq v4, v15, :cond_1

    .line 238
    const/4 v15, 0x3

    if-eq v4, v15, :cond_3

    const/4 v15, 0x4

    if-ne v4, v15, :cond_4

    .line 239
    :cond_3
    invoke-interface {v11}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v4

    .line 240
    goto :goto_1

    .line 243
    :cond_4
    invoke-interface {v11}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v10

    .line 244
    .local v10, "nodeName":Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->getHeaderType(Ljava/lang/String;)Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v5

    .line 245
    .local v5, "headerType":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    if-nez v5, :cond_6

    .line 246
    invoke-static {v11}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 274
    :cond_5
    :goto_2
    invoke-interface {v11}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v4

    .line 275
    goto :goto_1

    .line 249
    :cond_6
    invoke-static {v11, v12, v5}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->fillSearchItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/SearchHeader;

    move-result-object v8

    .line 250
    .local v8, "item":Lcom/samsung/helphub/headers/SearchHeader;
    if-eqz v8, :cond_7

    .line 251
    iget v15, v8, Lcom/samsung/helphub/headers/SearchHeader;->itemsXmlId:I

    if-lez v15, :cond_7

    iget-object v15, v8, Lcom/samsung/helphub/headers/SearchHeader;->type:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    sget-object v16, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_7

    .line 252
    iget v15, v8, Lcom/samsung/helphub/headers/SearchHeader;->itemsXmlId:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v1, v15, v2}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->findHeaderByTitle(Ljava/lang/String;Ljava/util/List;ILandroid/content/Context;)Ljava/util/List;

    .line 256
    :cond_7
    iget v15, v8, Lcom/samsung/helphub/headers/SearchHeader;->titleID:I

    invoke-virtual {v12, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    .line 257
    .local v13, "title":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 258
    .local v6, "index":I
    const/4 v15, -0x1

    if-le v6, v15, :cond_5

    .line 259
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v15

    if-ne v15, v9, :cond_8

    .line 260
    invoke-static {v11, v12, v5}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->fillHeaderItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 279
    .end local v4    # "event":I
    .end local v5    # "headerType":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .end local v6    # "index":I
    .end local v7    # "initialNodeName":Ljava/lang/String;
    .end local v8    # "item":Lcom/samsung/helphub/headers/SearchHeader;
    .end local v10    # "nodeName":Ljava/lang/String;
    .end local v11    # "parser":Landroid/content/res/XmlResourceParser;
    .end local v13    # "title":Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 280
    .local v3, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_3
    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 281
    .end local v3    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_0
    move-exception v15

    throw v15

    .line 261
    .restart local v4    # "event":I
    .restart local v5    # "headerType":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .restart local v6    # "index":I
    .restart local v7    # "initialNodeName":Ljava/lang/String;
    .restart local v8    # "item":Lcom/samsung/helphub/headers/SearchHeader;
    .restart local v10    # "nodeName":Ljava/lang/String;
    .restart local v11    # "parser":Landroid/content/res/XmlResourceParser;
    .restart local v13    # "title":Ljava/lang/String;
    :cond_8
    if-nez v6, :cond_9

    :try_start_4
    invoke-virtual {v13, v9}, Ljava/lang/String;->charAt(I)C

    move-result v15

    const/16 v16, 0x20

    move/from16 v0, v16

    if-ne v15, v0, :cond_9

    .line 262
    invoke-static {v11, v12, v5}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->fillHeaderItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 263
    :cond_9
    if-eqz v6, :cond_a

    add-int/lit8 v15, v6, -0x1

    invoke-virtual {v13, v15}, Ljava/lang/String;->charAt(I)C

    move-result v15

    const/16 v16, 0x20

    move/from16 v0, v16

    if-ne v15, v0, :cond_a

    add-int v15, v6, v9

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v16

    move/from16 v0, v16

    if-ne v15, v0, :cond_a

    .line 265
    invoke-static {v11, v12, v5}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->fillHeaderItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 266
    :cond_a
    if-eqz v6, :cond_5

    add-int/lit8 v15, v6, -0x1

    invoke-virtual {v13, v15}, Ljava/lang/String;->charAt(I)C

    move-result v15

    const/16 v16, 0x20

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    add-int v15, v6, v9

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v16

    move/from16 v0, v16

    if-ge v15, v0, :cond_5

    add-int/lit8 v15, v6, -0x1

    invoke-virtual {v13, v15}, Ljava/lang/String;->charAt(I)C

    move-result v15

    const/16 v16, 0x20

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 269
    invoke-static {v11, v12, v5}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->fillHeaderItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2
.end method

.method public static findHeaderTitleByKey(Ljava/lang/String;Landroid/content/Context;)Ljava/util/List;
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 206
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    const/4 v1, -0x1

    invoke-static {p0, v0, v1, p1}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->findHeaderByTitle(Ljava/lang/String;Ljava/util/List;ILandroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 208
    return-object v0
.end method

.method private static findSubHeaders(Ljava/util/List;ILandroid/content/Context;)Ljava/util/List;
    .locals 10
    .param p1, "xmlID"    # I
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;I",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 590
    .local p0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 593
    .local v6, "res":Landroid/content/res/Resources;
    :try_start_0
    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v5

    .line 595
    .local v5, "parser":Landroid/content/res/XmlResourceParser;
    invoke-static {v5}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->moveToStartTag(Landroid/content/res/XmlResourceParser;)V

    .line 597
    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 598
    .local v3, "initialNodeName":Ljava/lang/String;
    const-string v7, "help-headers"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 599
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "XML document must start with <guide-sections> tag; found"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " at "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->getPositionDescription()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 622
    .end local v3    # "initialNodeName":Ljava/lang/String;
    .end local v5    # "parser":Landroid/content/res/XmlResourceParser;
    :catch_0
    move-exception v0

    .line 623
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 630
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :goto_0
    return-object p0

    .line 604
    .restart local v3    # "initialNodeName":Ljava/lang/String;
    .restart local v5    # "parser":Landroid/content/res/XmlResourceParser;
    :cond_1
    :try_start_2
    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v1

    .line 605
    .local v1, "event":I
    :goto_1
    const/4 v7, 0x1

    if-eq v1, v7, :cond_0

    .line 606
    const/4 v7, 0x3

    if-eq v1, v7, :cond_2

    const/4 v7, 0x4

    if-ne v1, v7, :cond_3

    .line 607
    :cond_2
    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v1

    .line 608
    goto :goto_1

    .line 611
    :cond_3
    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 612
    .local v4, "nodeName":Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->getHeaderType(Ljava/lang/String;)Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v2

    .line 613
    .local v2, "headerType":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    if-nez v2, :cond_4

    .line 614
    invoke-static {v5}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 619
    :goto_2
    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v1

    .line 620
    goto :goto_1

    .line 616
    :cond_4
    invoke-static {v5, v6, v2}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->fillHeaderItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v7

    invoke-interface {p0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 624
    .end local v1    # "event":I
    .end local v2    # "headerType":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .end local v3    # "initialNodeName":Ljava/lang/String;
    .end local v4    # "nodeName":Ljava/lang/String;
    .end local v5    # "parser":Landroid/content/res/XmlResourceParser;
    :catch_1
    move-exception v0

    .line 625
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_3
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 626
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_0
    move-exception v7

    throw v7
.end method

.method public static getAllHeaders(Landroid/content/Context;)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 389
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 391
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    const/4 v1, -0x1

    invoke-static {v0, v1, p0}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->findAllHeaders(Ljava/util/List;ILandroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 393
    return-object v0
.end method

.method private static getHeaderType(Ljava/lang/String;)Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .locals 2
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 546
    const/4 v0, 0x0

    .line 548
    .local v0, "result":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    const-string v1, "item"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 549
    sget-object v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 556
    :cond_0
    :goto_0
    return-object v0

    .line 550
    :cond_1
    const-string v1, "category"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 551
    sget-object v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    goto :goto_0

    .line 552
    :cond_2
    const-string v1, "section"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 553
    sget-object v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    goto :goto_0
.end method

.method private static getMainXmlID(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 633
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "easy_mode_switch"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 635
    const v0, 0x7f060001

    .line 637
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x7f060000

    goto :goto_0
.end method

.method public static getSectionHeaders(Landroid/content/Context;)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 127
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    const/4 v1, -0x1

    invoke-static {v0, v1, p0}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->findAllSections(Ljava/util/List;ILandroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 129
    return-object v0
.end method

.method public static getSubHeaders(ILandroid/content/Context;)Ljava/util/List;
    .locals 1
    .param p0, "owner"    # I
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 580
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 582
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-static {v0, p0, p1}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->findSubHeaders(Ljava/util/List;ILandroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 584
    return-object v0
.end method

.method private static isPluginInstalled(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 5
    .param p0, "plugin"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 372
    const/4 v2, 0x0

    .line 373
    .local v2, "result":Z
    const/4 v1, 0x0

    .line 375
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, p0, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 379
    :goto_0
    if-eqz v1, :cond_0

    .line 380
    const/4 v2, 0x1

    .line 382
    :cond_0
    return v2

    .line 376
    :catch_0
    move-exception v0

    .line 377
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static loadHeadersFromResource(Landroid/content/Context;ILjava/util/List;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p2, "headers":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    const/4 v4, 0x0

    .line 60
    .local v4, "parser":Landroid/content/res/XmlResourceParser;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 63
    .local v5, "res":Landroid/content/res/Resources;
    :try_start_0
    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v4

    .line 65
    invoke-static {v4}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->moveToStartTag(Landroid/content/res/XmlResourceParser;)V

    .line 67
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 68
    .local v2, "initialNodeName":Ljava/lang/String;
    const-string v7, "help-headers"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 69
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "XML document must start with <guide-sections> tag; found"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " at "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getPositionDescription()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    .end local v2    # "initialNodeName":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_1
    new-instance v7, Ljava/lang/RuntimeException;

    const-string v8, "Error parsing sections"

    invoke-direct {v7, v8, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catchall_0
    move-exception v7

    if-eqz v4, :cond_0

    .line 106
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_0
    throw v7

    .line 74
    .restart local v2    # "initialNodeName":Ljava/lang/String;
    :cond_1
    :try_start_2
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v1

    .line 75
    .local v1, "event":I
    :goto_0
    const/4 v7, 0x1

    if-eq v1, v7, :cond_8

    .line 76
    const/4 v7, 0x3

    if-eq v1, v7, :cond_2

    const/4 v7, 0x4

    if-ne v1, v7, :cond_3

    .line 77
    :cond_2
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v1

    .line 78
    goto :goto_0

    .line 81
    :cond_3
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 82
    .local v3, "nodeName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 83
    .local v6, "type":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    const-string v7, "item"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 84
    sget-object v6, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ITEM:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 91
    :cond_4
    :goto_1
    if-eqz v6, :cond_7

    .line 92
    invoke-static {v4, v5, v6}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->fillHeaderItem(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)Lcom/samsung/helphub/headers/HelpHeader;

    move-result-object v7

    invoke-interface {p2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    :goto_2
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v1

    .line 98
    goto :goto_0

    .line 85
    :cond_5
    const-string v7, "category"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 86
    sget-object v6, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->CATEGORY:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    goto :goto_1

    .line 87
    :cond_6
    const-string v7, "section"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 88
    sget-object v6, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    goto :goto_1

    .line 94
    :cond_7
    invoke-static {v4}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 102
    .end local v1    # "event":I
    .end local v2    # "initialNodeName":Ljava/lang/String;
    .end local v3    # "nodeName":Ljava/lang/String;
    .end local v6    # "type":Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    :catch_1
    move-exception v0

    .line 103
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    new-instance v7, Ljava/lang/RuntimeException;

    const-string v8, "Error parsing sections"

    invoke-direct {v7, v8, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 105
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "event":I
    .restart local v2    # "initialNodeName":Ljava/lang/String;
    :cond_8
    if-eqz v4, :cond_9

    .line 106
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->close()V

    .line 109
    :cond_9
    return-void
.end method

.method private static moveToStartTag(Landroid/content/res/XmlResourceParser;)V
    .locals 2
    .param p0, "parser"    # Landroid/content/res/XmlResourceParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 519
    invoke-interface {p0}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v0

    .line 520
    .local v0, "type":I
    :goto_0
    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 522
    invoke-interface {p0}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v0

    goto :goto_0

    .line 524
    :cond_0
    return-void
.end method

.method private static skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 536
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    .line 537
    .local v0, "outerDepth":I
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .line 539
    .local v1, "type":I
    :goto_0
    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v2

    if-le v2, v0, :cond_1

    .line 541
    :cond_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    goto :goto_0

    .line 543
    :cond_1
    return-void
.end method
