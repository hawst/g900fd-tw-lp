.class public Lcom/samsung/helphub/headers/HelpHeaderAdapter;
.super Landroid/widget/BaseAdapter;
.source "HelpHeaderAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/headers/HelpHeaderAdapter$1;,
        Lcom/samsung/helphub/headers/HelpHeaderAdapter$HeaderViewHolder;
    }
.end annotation


# instance fields
.field private mHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resXmlId"    # I

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 32
    .local v0, "headers":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-static {p1, p2, v0}, Lcom/samsung/helphub/headers/HelpHeaderXMLParser;->loadHeadersFromResource(Landroid/content/Context;ILjava/util/List;)V

    .line 33
    invoke-direct {p0, p1, v0}, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->init(Landroid/content/Context;Ljava/util/List;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p2, "headers":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->init(Landroid/content/Context;Ljava/util/List;)V

    .line 38
    return-void
.end method

.method private filterHeader(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    move-object v3, p1

    .line 135
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-static {}, Lcom/samsung/helphub/utility/Utils;->isJKitKatMR()Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v4, 0x0

    invoke-static {v4}, Lcom/samsung/helphub/utility/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 147
    :cond_0
    :goto_0
    return-object v3

    .line 137
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 138
    .local v0, "count":I
    add-int/lit8 v2, v0, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_0

    .line 139
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/headers/HelpHeader;

    .line 141
    .local v1, "header":Lcom/samsung/helphub/headers/HelpHeader;
    invoke-virtual {v1}, Lcom/samsung/helphub/headers/HelpHeader;->getId()I

    move-result v4

    const v5, 0x7f0d009b

    if-ne v4, v5, :cond_2

    .line 142
    invoke-interface {p1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 138
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1
.end method

.method private getHelpHeaderType(I)Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v0

    return-object v0
.end method

.method private init(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p2, "headers":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/helphub/headers/HelpHeader;>;"
    invoke-direct {p0, p2}, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->filterHeader(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->mHeaders:Ljava/util/List;

    .line 42
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 43
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 94
    const/4 v0, 0x0

    .line 95
    .local v0, "result":I
    iget-object v1, p0, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->mHeaders:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 98
    :cond_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 108
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->mHeaders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v6, 0x7f0400c1

    const/4 v5, 0x0

    .line 48
    const/4 v2, 0x0

    .line 49
    .local v2, "result":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/helphub/headers/HelpHeader;

    .line 51
    .local v0, "header":Lcom/samsung/helphub/headers/HelpHeader;
    if-nez p2, :cond_1

    .line 52
    new-instance v1, Lcom/samsung/helphub/headers/HelpHeaderAdapter$HeaderViewHolder;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lcom/samsung/helphub/headers/HelpHeaderAdapter$HeaderViewHolder;-><init>(Lcom/samsung/helphub/headers/HelpHeaderAdapter$1;)V

    .line 53
    .local v1, "holder":Lcom/samsung/helphub/headers/HelpHeaderAdapter$HeaderViewHolder;
    sget-object v3, Lcom/samsung/helphub/headers/HelpHeaderAdapter$1;->$SwitchMap$com$samsung$helphub$headers$HelpHeader$HelpHeaderType:[I

    invoke-direct {p0, p1}, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->getHelpHeaderType(I)Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 69
    const/4 v2, 0x0

    .line 71
    :goto_0
    const v3, 0x7f0d010c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderAdapter$HeaderViewHolder;->title:Landroid/widget/TextView;

    .line 72
    const v3, 0x7f0d010f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderAdapter$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    .line 73
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 79
    :goto_1
    iget-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderAdapter$HeaderViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getTitleId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 80
    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getIconId()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 81
    iget-object v3, v1, Lcom/samsung/helphub/headers/HelpHeaderAdapter$HeaderViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader;->getIconId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 84
    :cond_0
    return-object v2

    .line 55
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f0400b7

    invoke-virtual {v3, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 56
    goto :goto_0

    .line 59
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v3, v6, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 60
    goto :goto_0

    .line 63
    :pswitch_2
    iget-object v3, p0, Lcom/samsung/helphub/headers/HelpHeaderAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v3, v6, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 64
    goto :goto_0

    .line 75
    .end local v1    # "holder":Lcom/samsung/helphub/headers/HelpHeaderAdapter$HeaderViewHolder;
    :cond_1
    move-object v2, p2

    .line 76
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/helphub/headers/HelpHeaderAdapter$HeaderViewHolder;

    .restart local v1    # "holder":Lcom/samsung/helphub/headers/HelpHeaderAdapter$HeaderViewHolder;
    goto :goto_1

    .line 53
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 113
    invoke-static {}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->values()[Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
