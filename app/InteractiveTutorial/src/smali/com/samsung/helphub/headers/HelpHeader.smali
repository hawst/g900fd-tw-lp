.class public final Lcom/samsung/helphub/headers/HelpHeader;
.super Ljava/lang/Object;
.source "HelpHeader.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    }
.end annotation


# static fields
.field public static final BUNDLE_KEY_HEADER:Ljava/lang/String; = "helpub:header"

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/helphub/headers/HelpHeader;",
            ">;"
        }
    .end annotation
.end field

.field public static final ID_UNDEFINED:I = -0x1


# instance fields
.field mActivityTitleId:I

.field mDictionaryId:I

.field mFragmentArguments:Landroid/os/Bundle;

.field mFragmentName:Ljava/lang/String;

.field mIconId:I

.field mId:I

.field mInclusiveDictionary:I

.field private mIsExternal:Z

.field mIsPluginRes:Z

.field mItemLayoutID:I

.field mItemsFragmentsArrayId:I

.field mName:Ljava/lang/String;

.field mOwnerResId:I

.field mPluginAppId:Ljava/lang/String;

.field mRecordId:I

.field mSubHeadersId:I

.field mSummaryId:I

.field mSummaryPs1Id:I

.field mSummaryPs2Id:I

.field mTitleId:I

.field mTitlePs1Id:I

.field mTitlePs2Id:I

.field mType:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

.field mXmlId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 382
    new-instance v0, Lcom/samsung/helphub/headers/HelpHeader$1;

    invoke-direct {v0}, Lcom/samsung/helphub/headers/HelpHeader$1;-><init>()V

    sput-object v0, Lcom/samsung/helphub/headers/HelpHeader;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mId:I

    .line 40
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mRecordId:I

    .line 42
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitleId:I

    .line 43
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs1Id:I

    .line 44
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs2Id:I

    .line 46
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mActivityTitleId:I

    .line 48
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryId:I

    .line 49
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs1Id:I

    .line 50
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs2Id:I

    .line 52
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIconId:I

    .line 55
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSubHeadersId:I

    .line 58
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mOwnerResId:I

    .line 61
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mItemsFragmentsArrayId:I

    .line 64
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mItemLayoutID:I

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentName:Ljava/lang/String;

    .line 68
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentArguments:Landroid/os/Bundle;

    .line 70
    sget-object v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mType:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 74
    iput-object v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mName:Ljava/lang/String;

    .line 76
    iput-boolean v2, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIsExternal:Z

    .line 79
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mXmlId:I

    .line 82
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mInclusiveDictionary:I

    .line 85
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mDictionaryId:I

    .line 88
    iput-boolean v2, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIsPluginRes:Z

    .line 92
    iput-object v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mPluginAppId:Ljava/lang/String;

    .line 99
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mId:I

    .line 40
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mRecordId:I

    .line 42
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitleId:I

    .line 43
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs1Id:I

    .line 44
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs2Id:I

    .line 46
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mActivityTitleId:I

    .line 48
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryId:I

    .line 49
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs1Id:I

    .line 50
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs2Id:I

    .line 52
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIconId:I

    .line 55
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSubHeadersId:I

    .line 58
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mOwnerResId:I

    .line 61
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mItemsFragmentsArrayId:I

    .line 64
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mItemLayoutID:I

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentName:Ljava/lang/String;

    .line 68
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentArguments:Landroid/os/Bundle;

    .line 70
    sget-object v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mType:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 74
    iput-object v4, p0, Lcom/samsung/helphub/headers/HelpHeader;->mName:Ljava/lang/String;

    .line 76
    iput-boolean v2, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIsExternal:Z

    .line 79
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mXmlId:I

    .line 82
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mInclusiveDictionary:I

    .line 85
    iput v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mDictionaryId:I

    .line 88
    iput-boolean v2, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIsPluginRes:Z

    .line 92
    iput-object v4, p0, Lcom/samsung/helphub/headers/HelpHeader;->mPluginAppId:Ljava/lang/String;

    .line 356
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mId:I

    .line 357
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitleId:I

    .line 358
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs1Id:I

    .line 359
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs2Id:I

    .line 360
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mActivityTitleId:I

    .line 361
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryId:I

    .line 362
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs1Id:I

    .line 363
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs2Id:I

    .line 364
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIconId:I

    .line 365
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mItemsFragmentsArrayId:I

    .line 366
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentName:Ljava/lang/String;

    .line 367
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentArguments:Landroid/os/Bundle;

    .line 368
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->valueOf(Ljava/lang/String;)Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mType:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 369
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSubHeadersId:I

    .line 370
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mItemLayoutID:I

    .line 371
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIsExternal:Z

    .line 372
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mOwnerResId:I

    .line 373
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mDictionaryId:I

    .line 374
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mRecordId:I

    .line 375
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIsPluginRes:Z

    .line 376
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mPluginAppId:Ljava/lang/String;

    .line 377
    return-void

    :cond_0
    move v0, v2

    .line 371
    goto :goto_0

    :cond_1
    move v1, v2

    .line 375
    goto :goto_1
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/helphub/headers/HelpHeader$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/samsung/helphub/headers/HelpHeader$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/samsung/helphub/headers/HelpHeader;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;)V
    .locals 4
    .param p1, "type"    # Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mId:I

    .line 40
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mRecordId:I

    .line 42
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitleId:I

    .line 43
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs1Id:I

    .line 44
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs2Id:I

    .line 46
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mActivityTitleId:I

    .line 48
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryId:I

    .line 49
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs1Id:I

    .line 50
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs2Id:I

    .line 52
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIconId:I

    .line 55
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSubHeadersId:I

    .line 58
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mOwnerResId:I

    .line 61
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mItemsFragmentsArrayId:I

    .line 64
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mItemLayoutID:I

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentName:Ljava/lang/String;

    .line 68
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentArguments:Landroid/os/Bundle;

    .line 70
    sget-object v0, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->SECTION:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    iput-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mType:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 74
    iput-object v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mName:Ljava/lang/String;

    .line 76
    iput-boolean v2, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIsExternal:Z

    .line 79
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mXmlId:I

    .line 82
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mInclusiveDictionary:I

    .line 85
    iput v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mDictionaryId:I

    .line 88
    iput-boolean v2, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIsPluginRes:Z

    .line 92
    iput-object v3, p0, Lcom/samsung/helphub/headers/HelpHeader;->mPluginAppId:Ljava/lang/String;

    .line 103
    iput-object p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mType:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    .line 104
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 298
    const/4 v0, 0x0

    return v0
.end method

.method public getActivityTitleId()I
    .locals 3

    .prologue
    .line 177
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitleId:I

    .line 178
    .local v0, "result":I
    iget v1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mActivityTitleId:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 179
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mActivityTitleId:I

    .line 181
    :cond_0
    return v0
.end method

.method public getDictionaryId()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mDictionaryId:I

    return v0
.end method

.method public getFragmentArguments()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentArguments:Landroid/os/Bundle;

    return-object v0
.end method

.method public getFragmentName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentName:Ljava/lang/String;

    return-object v0
.end method

.method public getIconId()I
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIconId:I

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mId:I

    return v0
.end method

.method public getInclusiveDictionary()I
    .locals 1

    .prologue
    .line 270
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mInclusiveDictionary:I

    return v0
.end method

.method public getIsPluginRes()Z
    .locals 1

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIsPluginRes:Z

    return v0
.end method

.method public getItemLayoutID()I
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mItemLayoutID:I

    return v0
.end method

.method public getItemsFragmentsArrayId()I
    .locals 1

    .prologue
    .line 254
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mItemsFragmentsArrayId:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getOwnerResId()I
    .locals 1

    .prologue
    .line 286
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mOwnerResId:I

    return v0
.end method

.method public getPluginAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mPluginAppId:Ljava/lang/String;

    return-object v0
.end method

.method public getRecordId()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mRecordId:I

    return v0
.end method

.method public getSubHeadersArrayId()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSubHeadersId:I

    return v0
.end method

.method public getSummaryId()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryId:I

    return v0
.end method

.method public getSummaryPs1Id()I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs1Id:I

    return v0
.end method

.method public getSummaryPs2Id()I
    .locals 1

    .prologue
    .line 202
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs2Id:I

    return v0
.end method

.method public getTitleId()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitleId:I

    return v0
.end method

.method public getTitlePs1Id()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs1Id:I

    return v0
.end method

.method public getTitlePs2Id()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs2Id:I

    return v0
.end method

.method public getType()Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mType:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    return-object v0
.end method

.method public isExternal()Z
    .locals 1

    .prologue
    .line 278
    iget-boolean v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIsExternal:Z

    return v0
.end method

.method public isVideoPath()Z
    .locals 2

    .prologue
    .line 321
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentName:Ljava/lang/String;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentName:Ljava/lang/String;

    const-string v1, "http"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDictionaryId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 168
    iput p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mDictionaryId:I

    .line 169
    return-void
.end method

.method public setFragmentName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 247
    iput-object p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentName:Ljava/lang/String;

    .line 248
    return-void
.end method

.method public setIconId(I)V
    .locals 0
    .param p1, "iconId"    # I

    .prologue
    .line 210
    iput p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIconId:I

    .line 211
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 107
    iput p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mId:I

    .line 108
    return-void
.end method

.method public setInclusiveDictionary(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 266
    iput p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mInclusiveDictionary:I

    .line 267
    return-void
.end method

.method public setIsExtrenal(Z)V
    .locals 0
    .param p1, "isExternal"    # Z

    .prologue
    .line 274
    iput-boolean p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIsExternal:Z

    .line 275
    return-void
.end method

.method public setIsPluginRes(Z)V
    .locals 0
    .param p1, "isPluginRes"    # Z

    .prologue
    .line 306
    iput-boolean p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIsPluginRes:Z

    .line 307
    return-void
.end method

.method public setItemLayoutID(I)V
    .locals 0
    .param p1, "layoutId"    # I

    .prologue
    .line 221
    iput p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mItemLayoutID:I

    .line 222
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mName:Ljava/lang/String;

    .line 132
    return-void
.end method

.method public setOwnerResId(I)V
    .locals 0
    .param p1, "resXmlId"    # I

    .prologue
    .line 282
    iput p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mOwnerResId:I

    .line 283
    return-void
.end method

.method public setPluginAppId(Ljava/lang/String;)V
    .locals 0
    .param p1, "pluginAppId"    # Ljava/lang/String;

    .prologue
    .line 314
    iput-object p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mPluginAppId:Ljava/lang/String;

    .line 315
    return-void
.end method

.method public setRecordId(I)V
    .locals 0
    .param p1, "recordId"    # I

    .prologue
    .line 119
    iput p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mRecordId:I

    .line 120
    return-void
.end method

.method public setSubHeadersArrayId(I)V
    .locals 0
    .param p1, "arrayId"    # I

    .prologue
    .line 262
    iput p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSubHeadersId:I

    .line 263
    return-void
.end method

.method public setSummaryId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 192
    iput p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryId:I

    .line 193
    return-void
.end method

.method public setSummaryPs1Id(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 199
    iput p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs1Id:I

    .line 200
    return-void
.end method

.method public setSummaryPs2Id(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 206
    iput p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs2Id:I

    .line 207
    return-void
.end method

.method public setTitleId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitleId:I

    .line 143
    return-void
.end method

.method public setTitlePs1Id(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 150
    iput p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs1Id:I

    .line 151
    return-void
.end method

.method public setTitlePs2Id(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 157
    iput p1, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs2Id:I

    .line 158
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 327
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 328
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitleId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 329
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs1Id:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 330
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mTitlePs2Id:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 331
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mActivityTitleId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 332
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 333
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs1Id:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 334
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSummaryPs2Id:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 335
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIconId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 336
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mItemsFragmentsArrayId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 337
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 338
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mFragmentArguments:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 339
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mType:Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;

    invoke-virtual {v0}, Lcom/samsung/helphub/headers/HelpHeader$HelpHeaderType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 340
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mSubHeadersId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 341
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mItemLayoutID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 342
    iget-boolean v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIsExternal:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 343
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mOwnerResId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 344
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mDictionaryId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 345
    iget v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mRecordId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 346
    iget-boolean v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mIsPluginRes:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 347
    iget-object v0, p0, Lcom/samsung/helphub/headers/HelpHeader;->mPluginAppId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 348
    return-void

    :cond_0
    move v0, v2

    .line 342
    goto :goto_0

    :cond_1
    move v1, v2

    .line 346
    goto :goto_1
.end method
