.class public Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegUtil;
.super Ljava/lang/Object;


# static fields
.field private static final JPEG_QUANT_TABLES:[[B

.field private static final imageIoScaleFactor:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x40

    const/4 v0, 0x2

    new-array v0, v0, [[B

    const/4 v1, 0x0

    new-array v2, v3, [B

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [B

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegUtil;->JPEG_QUANT_TABLES:[[B

    const/16 v0, 0x65

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegUtil;->imageIoScaleFactor:[I

    return-void

    nop

    :array_0
    .array-data 1
        0x10t
        0xbt
        0xct
        0xet
        0xct
        0xat
        0x10t
        0xet
        0xdt
        0xet
        0x12t
        0x11t
        0x10t
        0x13t
        0x18t
        0x28t
        0x1at
        0x18t
        0x16t
        0x16t
        0x18t
        0x31t
        0x23t
        0x25t
        0x1dt
        0x28t
        0x3at
        0x33t
        0x3dt
        0x3ct
        0x39t
        0x33t
        0x38t
        0x37t
        0x40t
        0x48t
        0x5ct
        0x4et
        0x40t
        0x44t
        0x57t
        0x45t
        0x37t
        0x38t
        0x50t
        0x6dt
        0x51t
        0x57t
        0x5ft
        0x62t
        0x67t
        0x68t
        0x67t
        0x3et
        0x4dt
        0x71t
        0x79t
        0x70t
        0x64t
        0x78t
        0x5ct
        0x65t
        0x67t
        0x63t
    .end array-data

    :array_1
    .array-data 1
        0x11t
        0x12t
        0x12t
        0x18t
        0x15t
        0x18t
        0x2ft
        0x1at
        0x1at
        0x2ft
        0x63t
        0x42t
        0x38t
        0x42t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
    .end array-data

    :array_2
    .array-data 4
        -0x1
        0x64000001
        0x32000001
        0x21555581
        0x19000001
        0x14000001
        0x10aaaac1
        0xe492491
        0xc800001
        0xb1c71c1
        0xa000001
        0x91745d1
        0x8555561
        0x7b13b19
        0x7249249
        0x6aaaaa9
        0x6400001
        0x5e1e1e1
        0x58e38e1
        0x5435e51
        0x5000001
        0x4c30c31
        0x48ba2e9
        0x4590b21
        0x42aaab1
        0x4000001
        0x3d89d8d
        0x3b425ed
        0x3924925
        0x372c239
        0x3555555
        0x339ce75
        0x3200001
        0x307c1f1
        0x2f0f0f1
        0x2db6db9
        0x2c71c71
        0x2b3e455
        0x2a1af29
        0x2906909
        0x2800001
        0x27063e9
        0x2618619
        0x253594d
        0x245d175
        0x238e391
        0x22c8591
        0x220ae4d
        0x2155559
        0x20a72f1
        0x2000001
        0x1f5c291
        0x1eb8521
        0x1e147b1
        0x1d70a3d
        0x1cccccd
        0x1c28f5d
        0x1b851ed
        0x1ae147d
        0x1a3d70d
        0x1999999
        0x18f5c29
        0x1851eb9
        0x17ae149
        0x170a3d9
        0x1666669
        0x15c28f5
        0x151eb85
        0x147ae15
        0x13d70a5
        0x1333335
        0x128f5c5
        0x11eb851
        0x1147ae1
        0x10a3d71
        0x1000001
        0xf5c291
        0xeb8521
        0xe147b1
        0xd70a3d
        0xcccccd
        0xc28f5d
        0xb851ed
        0xae147d
        0xa3d70d
        0x999999
        0x8f5c29
        0x851eb9
        0x7ae149
        0x70a3d9
        0x666669
        0x5c28f5
        0x51eb85
        0x47ae15
        0x3d70a5
        0x333335
        0x28f5c5
        0x1eb851
        0x147ae1
        0xa3d71
        0x1
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getQuantTable(III)[B
    .locals 6

    const/16 v5, 0x40

    const-class v0, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegUtil;

    monitor-enter v0

    mul-int/lit16 v1, p0, 0x9a

    mul-int/lit8 v2, p2, 0x4d

    add-int/2addr v1, v2

    const/16 v2, 0x18

    sub-int v2, p1, v2

    add-int/2addr v1, v2

    const/16 v1, 0x40

    :try_start_0
    new-array v1, v1, [B

    sget-object v2, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegUtil;->JPEG_QUANT_TABLES:[[B

    aget-object v2, v2, p0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_0

    aget-byte v4, v2, v3

    and-int/lit16 v4, v4, 0xff

    invoke-static {v4, p1, p2}, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegUtil;->getScaledQuantizationFactor(III)B

    move-result v4

    aput-byte v4, v1, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getScaledQuantizationFactor(III)B
    .locals 6

    const/16 v1, 0x1388

    const/16 v5, 0xff

    const/4 v4, 0x1

    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "qualityAlgorithm"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/16 v0, 0x63

    if-ne p0, v0, :cond_1

    const/16 v0, 0x24

    if-ne p1, v0, :cond_1

    const/16 v0, 0x8a

    :goto_0
    if-ge v0, v4, :cond_3

    move v0, v4

    :cond_0
    :goto_1
    int-to-byte v0, v0

    return v0

    :cond_1
    int-to-long v0, p0

    sget-object v2, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegUtil;->imageIoScaleFactor:[I

    aget v2, v2, p1

    int-to-long v2, v2

    mul-long/2addr v0, v2

    const-wide/32 v2, 0x1000000

    div-long/2addr v0, v2

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x32

    if-ge p1, v0, :cond_2

    div-int v0, v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_2
    mul-int/2addr v0, p0

    add-int/lit8 v0, v0, 0x32

    div-int/lit8 v0, v0, 0x64

    goto :goto_0

    :cond_2
    const/16 v0, 0xc8

    mul-int/lit8 v1, p1, 0x2

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    :cond_3
    if-le v0, v5, :cond_0

    move v0, v5

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static prependStandardHeader([BII[BILcom/google/mobile/googlenav/common/image/compression/jpeg/JpegHeaderParams;)V
    .locals 7

    invoke-virtual {p5}, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegHeaderParams;->getVariant()I

    move-result v2

    invoke-virtual {p5}, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegHeaderParams;->getWidth()I

    move-result v3

    invoke-virtual {p5}, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegHeaderParams;->getHeight()I

    move-result v4

    invoke-virtual {p5}, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegHeaderParams;->getQuality()I

    move-result v5

    invoke-virtual {p5}, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegHeaderParams;->getQualityAlgorithm()I

    move-result v6

    if-eqz v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "variant"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {v2}, Lcom/google/mobile/googlenav/common/image/compression/jpeg/GenerateJpegHeader;->getHeaderLength(I)I

    move-result v0

    add-int/2addr v0, p4

    invoke-static {p0, p1, p3, v0, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, p3

    move v1, p4

    invoke-static/range {v0 .. v6}, Lcom/google/mobile/googlenav/common/image/compression/jpeg/GenerateJpegHeader;->generate([BIIIIII)I

    return-void
.end method

.method public static uncompactJpeg([B)[B
    .locals 2

    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegUtil;->uncompactJpeg([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public static uncompactJpeg([BII)[B
    .locals 10

    const/16 v9, 0xb

    const/4 v8, 0x0

    aget-byte v0, p0, p1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    add-int/lit8 v0, p1, 0x1

    aget-byte v0, p0, v0

    const/16 v1, -0x28

    if-ne v0, v1, :cond_0

    new-array v0, p2, [B

    invoke-static {p0, p1, v0, v8, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_0
    return-object v0

    :cond_0
    aget-byte v0, p0, p1

    const/16 v1, 0x43

    if-ne v0, v1, :cond_1

    add-int/lit8 v0, p1, 0x1

    aget-byte v0, p0, v0

    const/16 v1, 0x4a

    if-ne v0, v1, :cond_1

    add-int/lit8 v0, p1, 0x2

    aget-byte v0, p0, v0

    const/16 v1, 0x50

    if-ne v0, v1, :cond_1

    add-int/lit8 v0, p1, 0x3

    aget-byte v0, p0, v0

    const/16 v1, 0x47

    if-eq v0, v1, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input is not in compact JPEG format"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    add-int/lit8 v0, p1, 0x4

    aget-byte v0, p0, v0

    and-int/lit16 v1, v0, 0xff

    add-int/lit8 v0, p1, 0x5

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v2, p1, 0x6

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v2, v0

    add-int/lit8 v0, p1, 0x7

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v3, p1, 0x8

    aget-byte v3, p0, v3

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v3, v0

    add-int/lit8 v0, p1, 0x9

    aget-byte v0, p0, v0

    and-int/lit16 v4, v0, 0xff

    add-int/lit8 v0, p1, 0xa

    aget-byte v0, p0, v0

    and-int/lit16 v5, v0, 0xff

    :try_start_0
    invoke-static {v1}, Lcom/google/mobile/googlenav/common/image/compression/jpeg/GenerateJpegHeader;->getHeaderLength(I)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    add-int v0, v6, p2

    sub-int/2addr v0, v9

    new-array v7, v0, [B

    new-instance v0, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegHeaderParams;

    invoke-direct/range {v0 .. v6}, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegHeaderParams;-><init>(IIIIII)V

    add-int/lit8 v2, p1, 0xb

    sub-int v3, p2, v9

    move-object v1, p0

    move-object v4, v7

    move v5, v8

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/google/mobile/googlenav/common/image/compression/jpeg/JpegUtil;->prependStandardHeader([BII[BILcom/google/mobile/googlenav/common/image/compression/jpeg/JpegHeaderParams;)V

    move-object v0, v7

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown variant "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
