.class Lcom/google/android/street/Overlay;
.super Ljava/lang/Object;
.source "Overlay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/street/Overlay$Pancake;,
        Lcom/google/android/street/Overlay$HitTester;,
        Lcom/google/android/street/Overlay$Polygon;,
        Lcom/google/android/street/Overlay$FadeAnimation;,
        Lcom/google/android/street/Overlay$Label;
    }
.end annotation


# static fields
.field private static final ARROW_DATA:[F

.field private static final BAR_DATA:[F

.field private static final COS_NO_SHOW:F

.field private static final COS_SHOW_BELOW:F

.field private static final GROUND_PANCAKE_DATA:[F

.field private static final GROUND_PANCAKE_DATA_FILL_INDICES:[B

.field private static final GROUND_PANCAKE_DATA_OUTLINE_INDICES:[B

.field private static final GROUND_PANCAKE_POLYGON:Lcom/google/android/street/Overlay$Polygon;

.field private static final INTERIOR_ARROW_DATA:[F

.field private static final INTERIOR_ARROW_SHADOW_DATA:[F

.field private static final PANCAKE_DATA:[F

.field private static final PANCAKE_DATA_FILL_INDICES:[B

.field private static final PANCAKE_DATA_OUTLINE_INDICES:[B

.field private static final PANCAKE_POLYGON:Lcom/google/android/street/Overlay$Polygon;

.field private static final PYRAMID_DATA:[F

.field private static final PYRAMID_FILL_INDEX:[B

.field private static final PYRAMID_OUTLINE_INDEX:[B

.field private static final STREET_ANCHOR:[F

.field private static final STREET_ANCHOR_DIR_ARROW:[F


# instance fields
.field private m3DLabelMaker:Lcom/google/android/street/LabelMaker;

.field private mAddressBubble:[Landroid/graphics/drawable/Drawable;

.field private final mArrow:Lcom/google/android/street/Overlay$Polygon;

.field private mAspectRatio:F

.field private final mCompassDirectionNames:[Ljava/lang/CharSequence;

.field private mConfig:Lcom/google/android/street/PanoramaConfig;

.field private final mContext:Landroid/content/Context;

.field private final mDirectionPaint:Landroid/graphics/Paint;

.field private mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

.field private final mDisplayDensity:F

.field private mDrawDisabled:Z

.field private mDrawRoadLabels:Z

.field private final mEnablePancake:Z

.field private final mEnablePanoPoints:Z

.field private mFancyStreetLabelIds:[[I

.field private mHighlight:I

.field private final mHitTesterLock:Ljava/lang/Object;

.field private mIncomingLink:Lcom/google/android/street/PanoramaLink;

.field private mIncomingYaw:F

.field private final mInteriorArrow:Lcom/google/android/street/Overlay$Polygon;

.field private final mInteriorArrowShadow:Lcom/google/android/street/Overlay$Polygon;

.field private mIsIndoorScene:Z

.field private final mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

.field private mLabelMaker:Lcom/google/android/street/LabelMaker;

.field private mLabelsComputed:Z

.field private mLabelsInitialized:Z

.field private mLastTrackballTime:J

.field private mLinks:[Lcom/google/android/street/PanoramaLink;

.field private mNextDrawTime:J

.field private mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

.field private mOutgoingLink:Lcom/google/android/street/PanoramaLink;

.field private mOutgoingYaw:F

.field private mPegmanOnPancake:Landroid/graphics/drawable/Drawable;

.field private mPegmanOnPancakeLabelId:I

.field private final mProjector:Lcom/google/android/street/Projector;

.field private mPublicHitTester:Lcom/google/android/street/Overlay$HitTester;

.field private final mRoad:Lcom/google/android/street/Overlay$Polygon;

.field private final mScratch:[F

.field private mSelectedLink:I

.field private final mStreetOutlinePaint:Landroid/graphics/Paint;

.field private final mStreetPaint:Landroid/graphics/Paint;

.field private mStreets:[[Lcom/google/android/street/Overlay$Label;

.field private mTouchUsed:Z

.field private mTrackballUsed:Z

.field private mViewHeight:I

.field private mViewWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/16 v1, 0x12

    const/16 v5, 0xc

    const/4 v4, 0x4

    .line 88
    new-array v0, v5, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/street/Overlay;->BAR_DATA:[F

    .line 102
    const/16 v0, 0x15

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/street/Overlay;->ARROW_DATA:[F

    .line 120
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/street/Overlay;->INTERIOR_ARROW_DATA:[F

    .line 134
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/street/Overlay;->INTERIOR_ARROW_SHADOW_DATA:[F

    .line 154
    new-array v0, v5, [F

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/street/Overlay;->PANCAKE_DATA:[F

    .line 162
    new-array v0, v4, [B

    fill-array-data v0, :array_5

    sput-object v0, Lcom/google/android/street/Overlay;->PANCAKE_DATA_FILL_INDICES:[B

    .line 165
    new-array v0, v4, [B

    fill-array-data v0, :array_6

    sput-object v0, Lcom/google/android/street/Overlay;->PANCAKE_DATA_OUTLINE_INDICES:[B

    .line 167
    new-instance v0, Lcom/google/android/street/Overlay$Polygon;

    sget-object v1, Lcom/google/android/street/Overlay;->PANCAKE_DATA:[F

    sget-object v2, Lcom/google/android/street/Overlay;->PANCAKE_DATA_FILL_INDICES:[B

    sget-object v3, Lcom/google/android/street/Overlay;->PANCAKE_DATA_OUTLINE_INDICES:[B

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/street/Overlay$Polygon;-><init>([F[B[B)V

    sput-object v0, Lcom/google/android/street/Overlay;->PANCAKE_POLYGON:Lcom/google/android/street/Overlay$Polygon;

    .line 178
    const/high16 v0, 0x40400000    # 3.0f

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/street/Overlay;->getCircle(FI)[F

    move-result-object v0

    sput-object v0, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_DATA:[F

    .line 182
    invoke-static {v6}, Lcom/google/android/street/Overlay;->getPolygonStripIndices(I)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_DATA_FILL_INDICES:[B

    .line 186
    invoke-static {v6}, Lcom/google/android/street/Overlay;->getRange(I)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_DATA_OUTLINE_INDICES:[B

    .line 189
    new-instance v0, Lcom/google/android/street/Overlay$Polygon;

    sget-object v1, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_DATA:[F

    sget-object v2, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_DATA_FILL_INDICES:[B

    sget-object v3, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_DATA_OUTLINE_INDICES:[B

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/street/Overlay$Polygon;-><init>([F[B[B)V

    sput-object v0, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_POLYGON:Lcom/google/android/street/Overlay$Polygon;

    .line 194
    const/16 v0, 0xf

    new-array v0, v0, [F

    fill-array-data v0, :array_7

    sput-object v0, Lcom/google/android/street/Overlay;->PYRAMID_DATA:[F

    .line 201
    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_8

    sput-object v0, Lcom/google/android/street/Overlay;->PYRAMID_FILL_INDEX:[B

    .line 204
    new-array v0, v5, [B

    fill-array-data v0, :array_9

    sput-object v0, Lcom/google/android/street/Overlay;->PYRAMID_OUTLINE_INDEX:[B

    .line 222
    new-array v0, v4, [F

    fill-array-data v0, :array_a

    sput-object v0, Lcom/google/android/street/Overlay;->STREET_ANCHOR:[F

    .line 224
    new-array v0, v4, [F

    fill-array-data v0, :array_b

    sput-object v0, Lcom/google/android/street/Overlay;->STREET_ANCHOR_DIR_ARROW:[F

    .line 234
    const/high16 v0, 0x42d20000    # 105.0f

    invoke-static {v0}, Lcom/google/android/street/StreetMath;->degreesToRadians(F)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    sput v0, Lcom/google/android/street/Overlay;->COS_NO_SHOW:F

    .line 239
    const/high16 v0, 0x428c0000    # 70.0f

    invoke-static {v0}, Lcom/google/android/street/StreetMath;->degreesToRadians(F)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    sput v0, Lcom/google/android/street/Overlay;->COS_SHOW_BELOW:F

    return-void

    .line 88
    nop

    :array_0
    .array-data 4
        0x3d6bedfa    # 0.0576f
        0x3e4ccccd    # 0.2f
        0x0
        0x3d6bedfa    # 0.0576f
        0x3e4ccccd    # 0.2f
        0x41a00000    # 20.0f
        -0x42941206    # -0.0576f
        0x3e4ccccd    # 0.2f
        0x41a00000    # 20.0f
        -0x42941206    # -0.0576f
        0x3e4ccccd    # 0.2f
        0x0
    .end array-data

    .line 102
    :array_1
    .array-data 4
        0x0
        0x3e570a3e    # 0.21000001f
        0x3ee1c582    # 0.44096f
        -0x41c538ef    # -0.1824f
        0x3e570a3e    # 0.21000001f
        0x3e394d94    # 0.18096f
        -0x42941206    # -0.0576f
        0x3e570a3e    # 0.21000001f
        0x3e394d94    # 0.18096f
        -0x42941206    # -0.0576f
        0x3e570a3e    # 0.21000001f
        0x0
        0x3d6bedfa    # 0.0576f
        0x3e570a3e    # 0.21000001f
        0x0
        0x3d6bedfa    # 0.0576f
        0x3e570a3e    # 0.21000001f
        0x3e394d94    # 0.18096f
        0x3e3ac711    # 0.1824f
        0x3e570a3e    # 0.21000001f
        0x3e394d94    # 0.18096f
    .end array-data

    .line 120
    :array_2
    .array-data 4
        0x0
        -0x40b33333    # -0.8f
        0x3ed78812    # 0.42096f
        -0x41c538ef    # -0.1824f
        -0x40b33333    # -0.8f
        0x3e9a176e    # 0.30096f
        -0x41c538ef    # -0.1824f
        -0x40b33333    # -0.8f
        0x3e4ccccd    # 0.2f
        0x0
        -0x40b33333    # -0.8f
        0x3ea3d70a    # 0.32f
        0x3e3ac711    # 0.1824f
        -0x40b33333    # -0.8f
        0x3e4ccccd    # 0.2f
        0x3e3ac711    # 0.1824f
        -0x40b33333    # -0.8f
        0x3e9a176e    # 0.30096f
    .end array-data

    .line 134
    :array_3
    .array-data 4
        0x0
        -0x40a8f5c2    # -0.84000003f
        0x3ed78812    # 0.42096f
        -0x41c538ef    # -0.1824f
        -0x40a8f5c2    # -0.84000003f
        0x3e9a176e    # 0.30096f
        -0x41c538ef    # -0.1824f
        -0x40a8f5c2    # -0.84000003f
        0x3e4ccccd    # 0.2f
        0x0
        -0x40a8f5c2    # -0.84000003f
        0x3ea3d70a    # 0.32f
        0x3e3ac711    # 0.1824f
        -0x40a8f5c2    # -0.84000003f
        0x3e4ccccd    # 0.2f
        0x3e3ac711    # 0.1824f
        -0x40a8f5c2    # -0.84000003f
        0x3e9a176e    # 0.30096f
    .end array-data

    .line 154
    :array_4
    .array-data 4
        0x40400000    # 3.0f
        0x40400000    # 3.0f
        0x0
        -0x3fc00000    # -3.0f
        0x40400000    # 3.0f
        0x0
        -0x3fc00000    # -3.0f
        -0x3fc00000    # -3.0f
        0x0
        0x40400000    # 3.0f
        -0x3fc00000    # -3.0f
        0x0
    .end array-data

    .line 162
    :array_5
    .array-data 1
        0x0t
        0x1t
        0x3t
        0x2t
    .end array-data

    .line 165
    :array_6
    .array-data 1
        0x0t
        0x1t
        0x2t
        0x3t
    .end array-data

    .line 194
    :array_7
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x0
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 201
    :array_8
    .array-data 1
        0x0t
        0x1t
        0x2t
        0x3t
        0x4t
        0x0t
    .end array-data

    .line 204
    nop

    :array_9
    .array-data 1
        0x0t
        0x1t
        0x2t
        0x0t
        0x2t
        0x3t
        0x0t
        0x3t
        0x4t
        0x0t
        0x4t
        0x1t
    .end array-data

    .line 222
    :array_a
    .array-data 4
        0x0
        0x3e570a3e    # 0.21000001f
        0x3de1c582    # 0.11024f
        0x3f800000    # 1.0f
    .end array-data

    .line 224
    :array_b
    .array-data 4
        0x0
        0x3e570a3e    # 0.21000001f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/street/Projector;ZZZ)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "projector"    # Lcom/google/android/street/Projector;
    .param p3, "enablePancake"    # Z
    .param p4, "enablePanoPoints"    # Z
    .param p5, "drawRoadLabels"    # Z

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    const/16 v5, 0x60

    const/4 v4, 0x1

    const/16 v3, 0xff

    .line 377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 346
    iput v0, p0, Lcom/google/android/street/Overlay;->mIncomingYaw:F

    .line 347
    iput v0, p0, Lcom/google/android/street/Overlay;->mOutgoingYaw:F

    .line 378
    iput-object p1, p0, Lcom/google/android/street/Overlay;->mContext:Landroid/content/Context;

    .line 379
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/street/Overlay;->mDisplayDensity:F

    .line 380
    new-instance v0, Lcom/google/android/street/Overlay$Polygon;

    sget-object v1, Lcom/google/android/street/Overlay;->BAR_DATA:[F

    invoke-direct {v0, v1}, Lcom/google/android/street/Overlay$Polygon;-><init>([F)V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mRoad:Lcom/google/android/street/Overlay$Polygon;

    .line 381
    new-instance v0, Lcom/google/android/street/Overlay$Polygon;

    sget-object v1, Lcom/google/android/street/Overlay;->ARROW_DATA:[F

    invoke-direct {v0, v1}, Lcom/google/android/street/Overlay$Polygon;-><init>([F)V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mArrow:Lcom/google/android/street/Overlay$Polygon;

    .line 382
    new-instance v0, Lcom/google/android/street/Overlay$Polygon;

    sget-object v1, Lcom/google/android/street/Overlay;->INTERIOR_ARROW_DATA:[F

    invoke-direct {v0, v1}, Lcom/google/android/street/Overlay$Polygon;-><init>([F)V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mInteriorArrow:Lcom/google/android/street/Overlay$Polygon;

    .line 383
    new-instance v0, Lcom/google/android/street/Overlay$Polygon;

    sget-object v1, Lcom/google/android/street/Overlay;->INTERIOR_ARROW_SHADOW_DATA:[F

    invoke-direct {v0, v1}, Lcom/google/android/street/Overlay$Polygon;-><init>([F)V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mInteriorArrowShadow:Lcom/google/android/street/Overlay$Polygon;

    .line 384
    iput-object p2, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    .line 386
    iput-boolean p5, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    .line 387
    invoke-direct {p0}, Lcom/google/android/street/Overlay;->createLabelMakers()V

    .line 389
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mCompassDirectionNames:[Ljava/lang/CharSequence;

    .line 393
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mStreetPaint:Landroid/graphics/Paint;

    .line 394
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreetPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 395
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreetPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x42700000    # 60.0f

    iget v2, p0, Lcom/google/android/street/Overlay;->mDisplayDensity:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 396
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreetPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v5, v5, v5}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 397
    iget-object v1, p0, Lcom/google/android/street/Overlay;->mStreetPaint:Landroid/graphics/Paint;

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 400
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mStreetOutlinePaint:Landroid/graphics/Paint;

    .line 401
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreetOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 402
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreetOutlinePaint:Landroid/graphics/Paint;

    const/16 v1, 0x40

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 403
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreetOutlinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 404
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreetOutlinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    iget v2, p0, Lcom/google/android/street/Overlay;->mDisplayDensity:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 407
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mDirectionPaint:Landroid/graphics/Paint;

    .line 408
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mDirectionPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 409
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mDirectionPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41600000    # 14.0f

    iget v2, p0, Lcom/google/android/street/Overlay;->mDisplayDensity:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 410
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mDirectionPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 412
    new-instance v0, Lcom/google/android/street/Overlay$HitTester;

    invoke-direct {v0}, Lcom/google/android/street/Overlay$HitTester;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    .line 413
    new-instance v0, Lcom/google/android/street/Overlay$HitTester;

    invoke-direct {v0}, Lcom/google/android/street/Overlay$HitTester;-><init>()V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mPublicHitTester:Lcom/google/android/street/Overlay$HitTester;

    .line 414
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mPublicHitTester:Lcom/google/android/street/Overlay$HitTester;

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mHitTesterLock:Ljava/lang/Object;

    .line 415
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mScratch:[F

    .line 416
    new-instance v0, Lcom/google/android/street/Overlay$FadeAnimation;

    invoke-direct {v0, v4}, Lcom/google/android/street/Overlay$FadeAnimation;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

    .line 417
    iput-boolean p3, p0, Lcom/google/android/street/Overlay;->mEnablePancake:Z

    .line 418
    iput-boolean p4, p0, Lcom/google/android/street/Overlay;->mEnablePanoPoints:Z

    .line 419
    return-void
.end method

.method private addLabels(Ljavax/microedition/khronos/opengles/GL10;Z)V
    .locals 12

    .prologue
    .line 1222
    iget-boolean v0, p0, Lcom/google/android/street/Overlay;->mLabelsComputed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    if-nez v0, :cond_1

    .line 1294
    :cond_0
    :goto_0
    return-void

    .line 1228
    :cond_1
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    if-nez v0, :cond_2

    .line 1229
    invoke-direct {p0}, Lcom/google/android/street/Overlay;->createLabelMakers()V

    .line 1232
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/street/Overlay;->mLabelsInitialized:Z

    if-nez v0, :cond_3

    .line 1233
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->initialize(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1234
    iget-object v0, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->initialize(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1235
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mLabelsInitialized:Z

    .line 1239
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    if-eqz v0, :cond_5

    if-nez p2, :cond_5

    .line 1240
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    const/16 v1, 0x200

    const/16 v2, 0x400

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/street/LabelMaker;->beginAdding(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 1241
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v8, v0

    .line 1242
    const/4 v0, 0x3

    filled-new-array {v8, v0}, [I

    move-result-object v0

    const-class v1, Lcom/google/android/street/Overlay$Label;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/google/android/street/Overlay$Label;

    iput-object v0, p0, Lcom/google/android/street/Overlay;->mStreets:[[Lcom/google/android/street/Overlay$Label;

    .line 1243
    const/4 v0, 0x0

    move v9, v0

    :goto_1
    if-ge v9, v8, :cond_6

    .line 1244
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    aget-object v0, v0, v9

    .line 1245
    iget-object v1, v0, Lcom/google/android/street/PanoramaLink;->mLinkText:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/google/android/street/PanoramaLink;->mLinkText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    .line 1246
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v0, Lcom/google/android/street/PanoramaLink;->mLinkText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/google/android/street/Overlay;->getDirectionText(Lcom/google/android/street/PanoramaLink;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1248
    const/4 v0, 0x0

    move v10, v0

    :goto_2
    const/4 v0, 0x3

    if-ge v10, v0, :cond_4

    .line 1249
    new-instance v11, Lcom/google/android/street/Overlay$Label;

    invoke-direct {v11}, Lcom/google/android/street/Overlay$Label;-><init>()V

    .line 1250
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mAddressBubble:[Landroid/graphics/drawable/Drawable;

    aget-object v2, v1, v10

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mDirectionPaint:Landroid/graphics/Paint;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x20

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/street/LabelMaker;->add(Ljavax/microedition/khronos/opengles/GL10;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Paint;II)I

    move-result v0

    .line 1253
    iput v0, v11, Lcom/google/android/street/Overlay$Label;->mLabelID:I

    .line 1254
    iget-object v1, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v1, v0}, Lcom/google/android/street/LabelMaker;->getWidth(I)F

    move-result v1

    iput v1, v11, Lcom/google/android/street/Overlay$Label;->mWidth:F

    .line 1255
    iget-object v1, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v1, v0}, Lcom/google/android/street/LabelMaker;->getHeight(I)F

    move-result v0

    iput v0, v11, Lcom/google/android/street/Overlay$Label;->mHeight:F

    .line 1256
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mStreets:[[Lcom/google/android/street/Overlay$Label;

    aget-object v0, v0, v9

    aput-object v11, v0, v10

    .line 1248
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_2

    .line 1243
    :cond_4
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    .line 1264
    :cond_5
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    const/16 v1, 0x100

    const/16 v2, 0x100

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/street/LabelMaker;->beginAdding(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 1268
    :cond_6
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mPegmanOnPancake:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/street/LabelMaker;->add(Ljavax/microedition/khronos/opengles/GL10;Landroid/graphics/drawable/Drawable;)I

    move-result v0

    iput v0, p0, Lcom/google/android/street/Overlay;->mPegmanOnPancakeLabelId:I

    .line 1270
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->endAdding(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1273
    iget-boolean v0, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    if-eqz v0, :cond_a

    if-eqz p2, :cond_a

    .line 1274
    iget-object v0, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->beginAdding(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1275
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v0, v0

    .line 1276
    const/4 v1, 0x3

    filled-new-array {v0, v1}, [I

    move-result-object v1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [[I

    iput-object p2, p0, Lcom/google/android/street/Overlay;->mFancyStreetLabelIds:[[I

    .line 1277
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v0, :cond_9

    .line 1278
    iget-object v2, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    aget-object v2, v2, v1

    .line 1279
    iget-object v3, v2, Lcom/google/android/street/PanoramaLink;->mLinkText:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, v2, Lcom/google/android/street/PanoramaLink;->mLinkText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_7

    .line 1280
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v2, Lcom/google/android/street/PanoramaLink;->mLinkText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v2}, Lcom/google/android/street/Overlay;->getDirectionText(Lcom/google/android/street/PanoramaLink;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1282
    const/4 v3, 0x0

    :goto_4
    const/4 v4, 0x3

    if-ge v3, v4, :cond_8

    .line 1283
    iget-object v4, p0, Lcom/google/android/street/Overlay;->mFancyStreetLabelIds:[[I

    aget-object v4, v4, v1

    iget-object v5, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mStreetPaint:Landroid/graphics/Paint;

    iget-object v7, p0, Lcom/google/android/street/Overlay;->mStreetOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v5, p1, v2, v6, v7}, Lcom/google/android/street/LabelMaker;->add(Ljavax/microedition/khronos/opengles/GL10;Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Paint;)I

    move-result v5

    aput v5, v4, v3

    .line 1282
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1287
    :cond_7
    iget-object v2, p0, Lcom/google/android/street/Overlay;->mFancyStreetLabelIds:[[I

    aget-object v2, v2, v1

    const/4 v3, 0x0

    const/4 v4, -0x1

    aput v4, v2, v3

    .line 1277
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1290
    :cond_9
    iget-object v0, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->endAdding(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1293
    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mLabelsComputed:Z

    goto/16 :goto_0
.end method

.method private beginLabelOpacity(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4

    .prologue
    .line 1340
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

    invoke-virtual {v0}, Lcom/google/android/street/Overlay$FadeAnimation;->getOpacity()I

    move-result v0

    .line 1341
    const/high16 v1, 0x10000

    if-eq v0, v1, :cond_0

    .line 1342
    const/16 v1, 0x2300

    const/16 v2, 0x2200

    const/high16 v3, 0x46040000    # 8448.0f

    invoke-interface {p1, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvf(IIF)V

    .line 1344
    invoke-interface {p1, v0, v0, v0, v0}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 1346
    :cond_0
    return-void
.end method

.method private clearLabelMakers(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 2
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 470
    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mLabelsComputed:Z

    .line 471
    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mLabelsInitialized:Z

    .line 472
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->shutdown(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 474
    iput-object v1, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    .line 476
    :cond_0
    iget-object v0, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    if-eqz v0, :cond_1

    .line 477
    iget-object v0, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->shutdown(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 478
    iput-object v1, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    .line 480
    :cond_1
    return-void
.end method

.method private computeAnimation(Lcom/google/android/street/UserOrientation;J)V
    .locals 6

    .prologue
    const-wide/16 v3, 0x7d0

    .line 1371
    invoke-direct {p0, p2, p3}, Lcom/google/android/street/Overlay;->computeLabelOpacity(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/street/Overlay;->mNextDrawTime:J

    .line 1373
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/street/Overlay;->mSelectedLink:I

    .line 1374
    iget-wide v0, p0, Lcom/google/android/street/Overlay;->mLastTrackballTime:J

    sub-long v0, p2, v0

    cmp-long v0, v0, v3

    if-gez v0, :cond_1

    .line 1375
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    invoke-virtual {p1}, Lcom/google/android/street/UserOrientation;->getYaw()F

    move-result v1

    const/high16 v2, 0x42f00000    # 120.0f

    invoke-static {v0, v1, v2}, Lcom/google/android/street/PanoramaConfig;->getClosestLinkIndex([Lcom/google/android/street/PanoramaLink;FF)I

    move-result v0

    iput v0, p0, Lcom/google/android/street/Overlay;->mSelectedLink:I

    .line 1378
    iget-wide v0, p0, Lcom/google/android/street/Overlay;->mLastTrackballTime:J

    add-long/2addr v0, v3

    .line 1379
    iget-wide v2, p0, Lcom/google/android/street/Overlay;->mNextDrawTime:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/street/Overlay;->mNextDrawTime:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_1

    .line 1380
    :cond_0
    iput-wide v0, p0, Lcom/google/android/street/Overlay;->mNextDrawTime:J

    .line 1383
    :cond_1
    return-void
.end method

.method private computeLabelOpacity(J)J
    .locals 5
    .param p1, "currentTime"    # J

    .prologue
    const/4 v4, 0x0

    .line 1394
    iget-boolean v3, p0, Lcom/google/android/street/Overlay;->mTrackballUsed:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/street/Overlay;->mTouchUsed:Z

    if-eqz v3, :cond_1

    :cond_0
    const/4 v3, 0x1

    move v2, v3

    .line 1396
    .local v2, "labelVisible":Z
    :goto_0
    iput-boolean v4, p0, Lcom/google/android/street/Overlay;->mTrackballUsed:Z

    .line 1397
    iput-boolean v4, p0, Lcom/google/android/street/Overlay;->mTouchUsed:Z

    .line 1399
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

    invoke-virtual {v3, v2, p1, p2}, Lcom/google/android/street/Overlay$FadeAnimation;->computeLabelOpacity(ZJ)J

    move-result-wide v0

    .line 1402
    .local v0, "labelTimeout":J
    return-wide v0

    .end local v0    # "labelTimeout":J
    .end local v2    # "labelVisible":Z
    :cond_1
    move v2, v4

    .line 1394
    goto :goto_0
.end method

.method private static createDirectionsArrow(Lcom/google/android/street/PanoramaLink;Lcom/google/android/street/PanoramaLink;)Lcom/google/android/street/Overlay$Polygon;
    .locals 20
    .param p0, "inLink"    # Lcom/google/android/street/PanoramaLink;
    .param p1, "outLink"    # Lcom/google/android/street/PanoramaLink;

    .prologue
    .line 591
    const/4 v13, 0x0

    .line 592
    .local v13, "outX":F
    const v14, 0x3f51ac9b    # 0.81904f

    .line 593
    .local v14, "outZ":F
    const/4 v9, 0x0

    .line 594
    .local v9, "midX":F
    const v10, 0x3ea35936    # 0.31904f

    .line 596
    .local v10, "midZ":F
    const/high16 v4, 0x42b40000    # 90.0f

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/street/PanoramaLink;->mYawDeg:F

    move/from16 p1, v0

    .end local p1    # "outLink":Lcom/google/android/street/PanoramaLink;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/street/PanoramaLink;->mYawDeg:F

    move/from16 p0, v0

    .end local p0    # "inLink":Lcom/google/android/street/PanoramaLink;
    sub-float p0, p1, p0

    sub-float p0, v4, p0

    invoke-static/range {p0 .. p0}, Lcom/google/android/street/StreetMath;->normalizeDegrees(F)F

    move-result p0

    invoke-static/range {p0 .. p0}, Lcom/google/android/street/StreetMath;->degreesToUnit(F)F

    move-result p0

    .line 599
    .local p0, "outYawUnit":F
    const/high16 p1, 0x3f000000    # 0.5f

    invoke-static/range {p0 .. p0}, Lcom/google/android/street/StreetMath;->cosUnit(F)F

    move-result v4

    mul-float v7, p1, v4

    .line 600
    .local v7, "inX":F
    const/high16 p1, 0x3f000000    # 0.5f

    invoke-static/range {p0 .. p0}, Lcom/google/android/street/StreetMath;->sinUnit(F)F

    move-result p0

    .end local p0    # "outYawUnit":F
    mul-float p0, p0, p1

    const p1, 0x3ea35936    # 0.31904f

    add-float v8, p0, p1

    .line 614
    .local v8, "inZ":F
    const/16 v11, 0x17

    .line 615
    .local v11, "numVertices":I
    mul-int/lit8 p0, v11, 0x3

    move/from16 v0, p0

    new-array v0, v0, [F

    move-object v15, v0

    .line 616
    .local v15, "vertices":[F
    const v4, 0x3de38e39

    .line 617
    .local v4, "deltaT":F
    const/16 p0, 0x0

    .line 618
    .local p0, "componentIndex":I
    const/16 p1, 0x0

    .local p1, "i":I
    move/from16 v6, p1

    .end local p1    # "i":I
    .local v6, "i":I
    move/from16 p1, p0

    .end local p0    # "componentIndex":I
    .local p1, "componentIndex":I
    :goto_0
    const/16 p0, 0xa

    move v0, v6

    move/from16 v1, p0

    if-ge v0, v1, :cond_0

    .line 619
    move v0, v6

    int-to-float v0, v0

    move/from16 p0, v0

    mul-float v5, v4, p0

    .line 620
    .local v5, "t":F
    invoke-static {v7, v9, v13, v5}, Lcom/google/android/street/StreetMath;->bezier(FFFF)F

    move-result v16

    .line 621
    .local v16, "x":F
    invoke-static {v8, v10, v14, v5}, Lcom/google/android/street/StreetMath;->bezier(FFFF)F

    move-result v17

    .line 622
    .local v17, "z":F
    invoke-static {v7, v9, v13, v5}, Lcom/google/android/street/StreetMath;->bezierTangent(FFFF)F

    move-result p0

    .line 623
    .local p0, "dx":F
    invoke-static {v8, v10, v14, v5}, Lcom/google/android/street/StreetMath;->bezierTangent(FFFF)F

    move-result v5

    .line 624
    .local v5, "dz":F
    const v12, 0x3d6bedfa    # 0.0576f

    mul-float v18, p0, p0

    mul-float v19, v5, v5

    add-float v18, v18, v19

    invoke-static/range {v18 .. v18}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v18

    div-float v12, v12, v18

    .line 626
    .local v12, "scaleFactor":F
    neg-float v5, v5

    mul-float/2addr v5, v12

    .line 627
    .local v5, "orthoX":F
    mul-float v12, v12, p0

    .line 628
    .local v12, "orthoZ":F
    add-int/lit8 p0, p1, 0x1

    .end local p1    # "componentIndex":I
    .local p0, "componentIndex":I
    add-float v18, v16, v5

    aput v18, v15, p1

    .line 629
    add-int/lit8 p1, p0, 0x1

    .end local p0    # "componentIndex":I
    .restart local p1    # "componentIndex":I
    const v18, 0x3e570a3e    # 0.21000001f

    aput v18, v15, p0

    .line 630
    add-int/lit8 p0, p1, 0x1

    .end local p1    # "componentIndex":I
    .restart local p0    # "componentIndex":I
    add-float v18, v17, v12

    aput v18, v15, p1

    .line 631
    add-int/lit8 p1, p0, 0x1

    .end local p0    # "componentIndex":I
    .restart local p1    # "componentIndex":I
    sub-float v5, v16, v5

    aput v5, v15, p0

    .line 632
    .end local v5    # "orthoX":F
    add-int/lit8 p0, p1, 0x1

    .end local p1    # "componentIndex":I
    .restart local p0    # "componentIndex":I
    const v5, 0x3e570a3e    # 0.21000001f

    aput v5, v15, p1

    .line 633
    add-int/lit8 p1, p0, 0x1

    .end local p0    # "componentIndex":I
    .restart local p1    # "componentIndex":I
    sub-float v5, v17, v12

    aput v5, v15, p0

    .line 618
    add-int/lit8 p0, v6, 0x1

    .end local v6    # "i":I
    .local p0, "i":I
    move/from16 v6, p0

    .end local p0    # "i":I
    .restart local v6    # "i":I
    goto :goto_0

    .line 637
    .end local v12    # "orthoZ":F
    .end local v16    # "x":F
    .end local v17    # "z":F
    :cond_0
    add-int/lit8 p0, p1, 0x1

    .end local p1    # "componentIndex":I
    .local p0, "componentIndex":I
    const v4, -0x41c538ef    # -0.1824f

    aput v4, v15, p1

    .line 638
    .end local v4    # "deltaT":F
    add-int/lit8 p1, p0, 0x1

    .end local p0    # "componentIndex":I
    .restart local p1    # "componentIndex":I
    const v4, 0x3e570a3e    # 0.21000001f

    aput v4, v15, p0

    .line 639
    add-int/lit8 p0, p1, 0x1

    .end local p1    # "componentIndex":I
    .restart local p0    # "componentIndex":I
    const v4, 0x3f51ac9b    # 0.81904f

    aput v4, v15, p1

    .line 640
    add-int/lit8 p1, p0, 0x1

    .end local p0    # "componentIndex":I
    .restart local p1    # "componentIndex":I
    const v4, 0x3e3ac711    # 0.1824f

    aput v4, v15, p0

    .line 641
    add-int/lit8 p0, p1, 0x1

    .end local p1    # "componentIndex":I
    .restart local p0    # "componentIndex":I
    const v4, 0x3e570a3e    # 0.21000001f

    aput v4, v15, p1

    .line 642
    add-int/lit8 p1, p0, 0x1

    .end local p0    # "componentIndex":I
    .restart local p1    # "componentIndex":I
    const v4, 0x3f51ac9b    # 0.81904f

    aput v4, v15, p0

    .line 643
    add-int/lit8 p0, p1, 0x1

    .end local p1    # "componentIndex":I
    .restart local p0    # "componentIndex":I
    const/4 v4, 0x0

    aput v4, v15, p1

    .line 644
    add-int/lit8 p1, p0, 0x1

    .end local p0    # "componentIndex":I
    .restart local p1    # "componentIndex":I
    const v4, 0x3e570a3e    # 0.21000001f

    aput v4, v15, p0

    .line 645
    add-int/lit8 p0, p1, 0x1

    .end local p1    # "componentIndex":I
    .restart local p0    # "componentIndex":I
    const p0, 0x3f8a1dfc

    aput p0, v15, p1

    .line 652
    .end local p0    # "componentIndex":I
    invoke-static {v11}, Lcom/google/android/street/Overlay$Polygon;->identityIndexArray(I)[B

    move-result-object p0

    .line 653
    .local p0, "fillIndices":[B
    new-array v5, v11, [B

    .line 654
    .local v5, "outlineIndices":[B
    div-int/lit8 p1, v11, 0x2

    .line 655
    .local p1, "halfNumVertices":I
    const/4 v4, 0x0

    .end local v6    # "i":I
    .end local v7    # "inX":F
    .local v4, "i":I
    :goto_1
    move v0, v4

    move/from16 v1, p1

    if-ge v0, v1, :cond_1

    .line 656
    mul-int/lit8 v6, v4, 0x2

    int-to-byte v6, v6

    aput-byte v6, v5, v4

    .line 657
    add-int v6, p1, v4

    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v7, v4, 0x1

    mul-int/lit8 v7, v7, 0x2

    sub-int v7, v11, v7

    int-to-byte v7, v7

    aput-byte v7, v5, v6

    .line 655
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 660
    :cond_1
    const/4 v4, 0x1

    sub-int v4, v11, v4

    int-to-byte v4, v4

    aput-byte v4, v5, p1

    .line 661
    .end local v4    # "i":I
    new-instance p1, Lcom/google/android/street/Overlay$Polygon;

    .end local p1    # "halfNumVertices":I
    move-object/from16 v0, p1

    move-object v1, v15

    move-object/from16 v2, p0

    move-object v3, v5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/street/Overlay$Polygon;-><init>([F[B[B)V

    return-object p1
.end method

.method private createLabelMakers()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 425
    new-instance v1, Lcom/google/android/street/LabelMaker;

    const/16 v2, 0x200

    const/16 v3, 0x400

    invoke-direct {v1, v5, v2, v3, v5}, Lcom/google/android/street/LabelMaker;-><init>(ZIIZ)V

    iput-object v1, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    .line 426
    const/16 v0, 0x800

    .line 428
    .local v0, "width":I
    iget v1, p0, Lcom/google/android/street/Overlay;->mDisplayDensity:F

    float-to-double v1, v1

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    cmpg-double v1, v1, v3

    if-gez v1, :cond_0

    .line 429
    div-int/lit8 v0, v0, 0x2

    .line 431
    :cond_0
    new-instance v1, Lcom/google/android/street/LabelMaker;

    const/16 v2, 0x800

    const/4 v3, 0x0

    invoke-direct {v1, v5, v2, v0, v3}, Lcom/google/android/street/LabelMaker;-><init>(ZIIZ)V

    iput-object v1, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    .line 432
    return-void
.end method

.method private drawFancyStreetLabels(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;)V
    .locals 13
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "userOrientation"    # Lcom/google/android/street/UserOrientation;

    .prologue
    const/16 v12, 0x1700

    const/4 v11, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    const v9, 0x3ccccccd    # 0.025f

    const/4 v8, 0x0

    .line 869
    iget-boolean v5, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    if-nez v5, :cond_1

    .line 907
    :cond_0
    :goto_0
    return-void

    .line 872
    :cond_1
    iget-object v5, p0, Lcom/google/android/street/Overlay;->mFancyStreetLabelIds:[[I

    if-eqz v5, :cond_0

    .line 875
    iget-object v5, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    iget v6, p0, Lcom/google/android/street/Overlay;->mViewWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/street/Overlay;->mViewHeight:I

    int-to-float v7, v7

    invoke-virtual {v5, p1, v6, v7}, Lcom/google/android/street/LabelMaker;->beginDrawing(Ljavax/microedition/khronos/opengles/GL10;FF)V

    .line 876
    invoke-interface {p1, v12}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 877
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 878
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 879
    invoke-virtual {p2}, Lcom/google/android/street/UserOrientation;->getRotationMatrix()[F

    move-result-object v5

    invoke-interface {p1, v5, v11}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    .line 880
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v5, v5

    if-ge v3, v5, :cond_3

    .line 881
    iget-object v5, p0, Lcom/google/android/street/Overlay;->mFancyStreetLabelIds:[[I

    aget-object v5, v5, v3

    aget v5, v5, v11

    const/4 v6, -0x1

    if-ne v5, v6, :cond_2

    .line 880
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 884
    :cond_2
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 887
    const/high16 v5, 0x43340000    # 180.0f

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget v6, v6, Lcom/google/android/street/PanoramaConfig;->mTiltYawDeg:F

    sub-float v1, v5, v6

    .line 888
    .local v1, "groundTiltDeg":F
    invoke-static {v1}, Lcom/google/android/street/StreetMath;->degreesToRadians(F)F

    move-result v2

    .line 889
    .local v2, "groundTiltRad":F
    invoke-static {v2}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    .line 890
    .local v0, "csT":F
    invoke-static {v2}, Landroid/util/FloatMath;->sin(F)F

    move-result v4

    .line 892
    .local v4, "snT":F
    iget-object v5, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget v5, v5, Lcom/google/android/street/PanoramaConfig;->mTiltPitchDeg:F

    neg-float v5, v5

    neg-float v6, v4

    invoke-interface {p1, v5, v0, v8, v6}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 894
    iget-object v5, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    aget-object v5, v5, v3

    iget v5, v5, Lcom/google/android/street/PanoramaLink;->mYawDeg:F

    neg-float v5, v5

    invoke-interface {p1, v5, v8, v10, v8}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 895
    const/high16 v5, -0x40000000    # -2.0f

    invoke-interface {p1, v8, v8, v5}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 896
    const/high16 v5, -0x3f600000    # -5.0f

    invoke-interface {p1, v8, v5, v8}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 897
    const/high16 v5, -0x3d4c0000    # -90.0f

    invoke-interface {p1, v5, v10, v8, v8}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 898
    const/high16 v5, 0x42b40000    # 90.0f

    invoke-interface {p1, v5, v8, v8, v10}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 899
    invoke-interface {p1, v9, v9, v9}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 900
    const/high16 v5, -0x3d900000    # -60.0f

    iget v6, p0, Lcom/google/android/street/Overlay;->mDisplayDensity:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-interface {p1, v8, v5, v8}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 901
    iget-object v5, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mFancyStreetLabelIds:[[I

    aget-object v6, v6, v3

    aget v6, v6, v11

    invoke-virtual {v5, p1, v6}, Lcom/google/android/street/LabelMaker;->draw(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 902
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_2

    .line 904
    .end local v0    # "csT":F
    .end local v1    # "groundTiltDeg":F
    .end local v2    # "groundTiltRad":F
    .end local v4    # "snT":F
    :cond_3
    invoke-interface {p1, v12}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 905
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 906
    iget-object v5, p0, Lcom/google/android/street/Overlay;->m3DLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v5, p1}, Lcom/google/android/street/LabelMaker;->endDrawing(Ljavax/microedition/khronos/opengles/GL10;)V

    goto/16 :goto_0
.end method

.method private drawLabel(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/Overlay$Label;)V
    .locals 5
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "label"    # Lcom/google/android/street/Overlay$Label;

    .prologue
    .line 1358
    iget-object v0, p2, Lcom/google/android/street/Overlay$Label;->mPosition:[F

    .line 1359
    .local v0, "pos":[F
    const/4 v2, 0x0

    aget v1, v0, v2

    .line 1360
    .local v1, "x":F
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1361
    iget-object v2, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    const/4 v3, 0x1

    aget v3, v0, v3

    iget v4, p2, Lcom/google/android/street/Overlay$Label;->mLabelID:I

    invoke-virtual {v2, p1, v1, v3, v4}, Lcom/google/android/street/LabelMaker;->draw(Ljavax/microedition/khronos/opengles/GL10;FFI)V

    .line 1363
    :cond_0
    return-void
.end method

.method private drawLabels(Ljavax/microedition/khronos/opengles/GL10;ZZ)V
    .locals 8

    .prologue
    const/16 v5, 0x80

    const/4 v4, 0x0

    .line 1298
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    iget v1, p0, Lcom/google/android/street/Overlay;->mViewWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/street/Overlay;->mViewHeight:I

    int-to-float v2, v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/street/LabelMaker;->beginDrawing(Ljavax/microedition/khronos/opengles/GL10;FF)V

    .line 1300
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

    invoke-virtual {v0}, Lcom/google/android/street/Overlay$FadeAnimation;->isTransparent()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1301
    invoke-direct {p0, p1}, Lcom/google/android/street/Overlay;->beginLabelOpacity(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1304
    iget-boolean v0, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    if-eqz v0, :cond_2

    if-nez p3, :cond_2

    .line 1305
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v0, v0

    move v1, v4

    .line 1306
    :goto_0
    if-ge v1, v0, :cond_2

    .line 1308
    iget v2, p0, Lcom/google/android/street/Overlay;->mHighlight:I

    if-ne v1, v2, :cond_1

    .line 1309
    const/4 v2, 0x1

    .line 1313
    :goto_1
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mStreets:[[Lcom/google/android/street/Overlay$Label;

    aget-object v3, v3, v1

    aget-object v2, v3, v2

    .line 1314
    if-eqz v2, :cond_0

    .line 1315
    invoke-direct {p0, p1, v2}, Lcom/google/android/street/Overlay;->drawLabel(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/Overlay$Label;)V

    .line 1306
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1310
    :cond_1
    iget v2, p0, Lcom/google/android/street/Overlay;->mSelectedLink:I

    if-ne v1, v2, :cond_5

    .line 1311
    const/4 v2, 0x2

    goto :goto_1

    .line 1320
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/street/Overlay;->endLabelOpacity(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1324
    :cond_3
    if-eqz p2, :cond_4

    .line 1325
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    const/high16 v1, 0x41f00000    # 30.0f

    const/high16 v2, 0x42700000    # 60.0f

    iget v3, p0, Lcom/google/android/street/Overlay;->mPegmanOnPancakeLabelId:I

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/street/LabelMaker;->draw(Ljavax/microedition/khronos/opengles/GL10;FFI)V

    .line 1329
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    const/16 v1, 0x1e

    const/16 v2, 0x3c

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mPegmanOnPancake:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mPegmanOnPancake:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/lit8 v4, v4, 0x3c

    const/4 v7, -0x2

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/street/Overlay$HitTester;->add(IIIIIII)V

    .line 1336
    :cond_4
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    invoke-virtual {v0, p1}, Lcom/google/android/street/LabelMaker;->endDrawing(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1337
    return-void

    :cond_5
    move v2, v4

    goto :goto_1
.end method

.method private drawLink(Ljavax/microedition/khronos/opengles/GL10;FI)V
    .locals 18

    .prologue
    .line 961
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    move-object v5, v0

    aget-object v5, v5, p3

    .line 962
    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 965
    iget v6, v5, Lcom/google/android/street/PanoramaLink;->mYawDeg:F

    sub-float v13, p2, v6

    .line 968
    const/high16 v6, 0x43340000    # 180.0f

    iget v7, v5, Lcom/google/android/street/PanoramaLink;->mYawDeg:F

    sub-float/2addr v6, v7

    .line 971
    const/high16 v7, 0x43340000    # 180.0f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    move-object v8, v0

    iget v8, v8, Lcom/google/android/street/PanoramaConfig;->mTiltYawDeg:F

    sub-float/2addr v7, v8

    .line 972
    invoke-static {v7}, Lcom/google/android/street/StreetMath;->degreesToRadians(F)F

    move-result v7

    .line 973
    invoke-static {v7}, Landroid/util/FloatMath;->cos(F)F

    move-result v8

    .line 974
    invoke-static {v7}, Landroid/util/FloatMath;->sin(F)F

    move-result v7

    .line 976
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    move-object v9, v0

    iget v9, v9, Lcom/google/android/street/PanoramaConfig;->mTiltPitchDeg:F

    neg-float v9, v9

    const/4 v10, 0x0

    neg-float v7, v7

    move-object/from16 v0, p1

    move v1, v9

    move v2, v8

    move v3, v10

    move v4, v7

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 977
    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move v1, v6

    move v2, v7

    move v3, v8

    move v4, v9

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 979
    iget v6, v5, Lcom/google/android/street/PanoramaLink;->mRoadARGB:I

    .line 980
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    move-object v7, v0

    if-eq v5, v7, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    move-object v7, v0

    if-ne v5, v7, :cond_1

    .line 981
    :cond_0
    const v6, -0x7fa9a934

    .line 983
    :cond_1
    shr-int/lit8 v7, v6, 0x18

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v7, v7, 0x8

    .line 988
    const v8, 0x3f8ccccd    # 1.1f

    int-to-float v7, v7

    mul-float/2addr v7, v8

    float-to-int v7, v7

    const/high16 v8, 0x10000

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 989
    shr-int/lit8 v8, v6, 0x10

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x8

    .line 990
    shr-int/lit8 v9, v6, 0x8

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x8

    .line 991
    shr-int/lit8 v6, v6, 0x0

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    .line 992
    move-object/from16 v0, p1

    move v1, v8

    move v2, v9

    move v3, v6

    move v4, v7

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 993
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/street/Overlay;->mIsIndoorScene:Z

    move v6, v0

    if-nez v6, :cond_2

    .line 995
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mRoad:Lcom/google/android/street/Overlay$Polygon;

    move-object v6, v0

    const/4 v7, 0x6

    move-object v0, v6

    move-object/from16 v1, p1

    move v2, v7

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/Overlay$Polygon;->draw(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 996
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mRoad:Lcom/google/android/street/Overlay$Polygon;

    move-object v6, v0

    move-object v0, v6

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/street/Overlay$Polygon;->drawOutline(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1004
    :cond_2
    invoke-static {v13}, Lcom/google/android/street/StreetMath;->degreesToRadians(F)F

    move-result v6

    invoke-static {v6}, Landroid/util/FloatMath;->cos(F)F

    move-result v14

    .line 1005
    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v6, v14

    mul-float/2addr v6, v14

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float/2addr v6, v7

    .line 1006
    const/high16 v7, 0x40800000    # 4.0f

    mul-float/2addr v7, v14

    mul-float/2addr v7, v14

    const/high16 v8, 0x40400000    # 3.0f

    sub-float/2addr v7, v8

    mul-float/2addr v7, v14

    .line 1007
    const v8, 0x3e4ccccd    # 0.2f

    const/high16 v9, 0x3e800000    # 0.25f

    const v10, 0x3f2e147b    # 0.68f

    const v11, 0x3eb5c28f    # 0.355f

    mul-float/2addr v11, v14

    add-float/2addr v10, v11

    const v11, 0x3ea3d70a    # 0.32f

    mul-float/2addr v6, v11

    sub-float v6, v10, v6

    const v10, 0x3e5c28f6    # 0.215f

    mul-float/2addr v7, v10

    sub-float/2addr v6, v7

    mul-float/2addr v6, v9

    add-float/2addr v6, v8

    .line 1010
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v7, v0

    if-eqz v7, :cond_3

    .line 1014
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    move-object v7, v0

    if-ne v5, v7, :cond_9

    .line 1015
    const v7, 0x3f1b8bad    # 0.60760003f

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v6

    .line 1021
    :cond_3
    :goto_0
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move v1, v7

    move v2, v8

    move v3, v6

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 1025
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v6, v0

    if-eqz v6, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    move-object v6, v0

    if-eq v5, v6, :cond_4

    .line 1026
    const v6, 0x3f19999a    # 0.6f

    const/high16 v7, 0x3f800000    # 1.0f

    const v8, 0x3f19999a    # 0.6f

    move-object/from16 v0, p1

    move v1, v6

    move v2, v7

    move v3, v8

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 1029
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/street/Overlay;->mSelectedLink:I

    move v6, v0

    move v0, v6

    move/from16 v1, p3

    if-ne v0, v1, :cond_a

    const/4 v6, 0x1

    .line 1030
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/street/Overlay;->mHighlight:I

    move v7, v0

    move v0, v7

    move/from16 v1, p3

    if-ne v0, v1, :cond_b

    const/4 v7, 0x1

    .line 1031
    :goto_2
    if-eqz v7, :cond_5

    .line 1032
    const/4 v6, 0x0

    .line 1034
    :cond_5
    if-nez v6, :cond_6

    if-eqz v7, :cond_c

    :cond_6
    const/4 v7, 0x1

    move v15, v7

    .line 1036
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v7, v0

    if-eqz v7, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    move-object v7, v0

    if-ne v5, v7, :cond_d

    const/4 v5, 0x1

    move/from16 v16, v5

    .line 1038
    :goto_4
    if-eqz v15, :cond_10

    .line 1040
    if-eqz v6, :cond_e

    .line 1041
    const v5, 0x8800

    const/16 v6, 0x6d00

    const v7, 0xad00

    const/high16 v8, 0x10000

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    move v4, v8

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 1045
    :goto_5
    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 1046
    if-eqz v16, :cond_f

    .line 1049
    const/4 v5, 0x0

    const/4 v6, 0x0

    const v7, -0x42fd5239

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 1051
    const v5, 0x3f8ccccd    # 1.1f

    const/high16 v6, 0x3f800000    # 1.0f

    const v7, 0x3f8ccccd    # 1.1f

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    .line 1063
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    move-object v5, v0

    move-object v0, v5

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/street/Projector;->getCurrentModelView(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1065
    if-eqz v16, :cond_11

    .line 1066
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v5, v0

    const/4 v6, 0x5

    move-object v0, v5

    move-object/from16 v1, p1

    move v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/Overlay$Polygon;->draw(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 1067
    const/high16 v5, 0x10000

    const/high16 v6, 0x10000

    const/high16 v7, 0x10000

    const/high16 v8, 0x10000

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    move v4, v8

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 1068
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v5, v0

    move-object v0, v5

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/street/Overlay$Polygon;->drawOutline(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1069
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    move-object v5, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    move-object v6, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mScratch:[F

    move-object v8, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v9, v0

    const/4 v10, 0x0

    const/16 v11, 0x40

    move-object/from16 v7, p1

    move/from16 v12, p3

    invoke-virtual/range {v5 .. v12}, Lcom/google/android/street/Overlay$HitTester;->add(Lcom/google/android/street/Projector;Ljavax/microedition/khronos/opengles/GL10;[FLcom/google/android/street/Overlay$Polygon;III)V

    .line 1093
    :goto_7
    if-eqz v15, :cond_7

    .line 1094
    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 1097
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mStreets:[[Lcom/google/android/street/Overlay$Label;

    move-object v5, v0

    aget-object v5, v5, p3

    const/4 v6, 0x0

    aget-object v5, v5, v6

    .line 1098
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

    move-object v6, v0

    invoke-virtual {v6}, Lcom/google/android/street/Overlay$FadeAnimation;->isTransparent()Z

    move-result v6

    if-nez v6, :cond_18

    if-eqz v5, :cond_18

    .line 1102
    iget-object v6, v5, Lcom/google/android/street/Overlay$Label;->mPosition:[F

    .line 1106
    sget v7, Lcom/google/android/street/Overlay;->COS_NO_SHOW:F

    cmpl-float v7, v14, v7

    if-lez v7, :cond_17

    .line 1107
    iget v5, v5, Lcom/google/android/street/Overlay$Label;->mLabelID:I

    .line 1108
    if-eqz v16, :cond_14

    .line 1109
    sget-object v7, Lcom/google/android/street/Overlay;->STREET_ANCHOR_DIR_ARROW:[F

    move-object/from16 v0, p0

    move-object v1, v7

    move-object v2, v6

    invoke-direct {v0, v1, v2}, Lcom/google/android/street/Overlay;->getBaseProjection([F[F)V

    .line 1114
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    move-object v7, v0

    invoke-virtual {v7, v5}, Lcom/google/android/street/LabelMaker;->getWidth(I)F

    move-result v7

    .line 1115
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mLabelMaker:Lcom/google/android/street/LabelMaker;

    move-object v8, v0

    invoke-virtual {v8, v5}, Lcom/google/android/street/LabelMaker;->getHeight(I)F

    move-result v5

    .line 1119
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/street/Overlay;->mViewHeight:I

    move v8, v0

    int-to-float v8, v8

    const/high16 v9, 0x3f000000    # 0.5f

    mul-float/2addr v8, v9

    const v9, 0x3e19999a    # 0.15f

    mul-float/2addr v8, v9

    .line 1120
    neg-float v9, v7

    const/high16 v10, 0x3f000000    # 0.5f

    mul-float/2addr v9, v10

    .line 1121
    neg-float v7, v7

    sub-float/2addr v7, v8

    .line 1123
    neg-float v5, v5

    const v10, 0x3ecccccd    # 0.4f

    mul-float/2addr v5, v10

    .line 1126
    const/4 v10, 0x0

    aget v10, v6, v10

    .line 1127
    const/4 v11, 0x1

    aget v11, v6, v11

    .line 1135
    sget v12, Lcom/google/android/street/Overlay;->COS_SHOW_BELOW:F

    cmpg-float v12, v14, v12

    if-gtz v12, :cond_15

    .line 1137
    add-float v5, v10, v9

    .line 1138
    add-float v7, v11, v8

    move/from16 v17, v7

    move v7, v5

    move/from16 v5, v17

    .line 1157
    :goto_9
    const/4 v8, 0x0

    aput v7, v6, v8

    .line 1158
    const/4 v7, 0x1

    aput v5, v6, v7

    .line 1178
    :goto_a
    const/4 v5, 0x1

    :goto_b
    const/4 v7, 0x3

    if-ge v5, v7, :cond_18

    .line 1179
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mStreets:[[Lcom/google/android/street/Overlay$Label;

    move-object v7, v0

    aget-object v7, v7, p3

    aget-object v7, v7, v5

    if-eqz v7, :cond_8

    .line 1180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mStreets:[[Lcom/google/android/street/Overlay$Label;

    move-object v7, v0

    aget-object v7, v7, p3

    aget-object v7, v7, v5

    iget-object v7, v7, Lcom/google/android/street/Overlay$Label;->mPosition:[F

    .line 1181
    const/4 v8, 0x0

    const/4 v9, 0x0

    aget v9, v6, v9

    aput v9, v7, v8

    .line 1182
    const/4 v8, 0x1

    const/4 v9, 0x1

    aget v9, v6, v9

    aput v9, v7, v8

    .line 1178
    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_b

    .line 1017
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    move-object v7, v0

    if-ne v5, v7, :cond_3

    .line 1018
    const v6, -0x415ca6ca    # -0.31904f

    goto/16 :goto_0

    .line 1029
    :cond_a
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 1030
    :cond_b
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 1034
    :cond_c
    const/4 v7, 0x0

    move v15, v7

    goto/16 :goto_3

    .line 1036
    :cond_d
    const/4 v5, 0x0

    move/from16 v16, v5

    goto/16 :goto_4

    .line 1043
    :cond_e
    const v5, 0xf600

    const v6, 0x8a00

    const/16 v7, 0x1f00

    const/high16 v8, 0x10000

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    move v4, v8

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    goto/16 :goto_5

    .line 1054
    :cond_f
    const v5, 0x3fa66666    # 1.3f

    const/high16 v6, 0x3f800000    # 1.0f

    const v7, 0x3fa66666    # 1.3f

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glScalef(FFF)V

    goto/16 :goto_6

    .line 1058
    :cond_10
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const v8, 0x8000

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    move v4, v8

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    goto/16 :goto_6

    .line 1071
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/street/Overlay;->mIsIndoorScene:Z

    move v5, v0

    if-eqz v5, :cond_13

    .line 1073
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/street/Overlay;->mAspectRatio:F

    move v5, v0

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_12

    .line 1077
    const/4 v5, 0x0

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    invoke-interface {v0, v1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 1079
    :cond_12
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x5000

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    move v4, v8

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 1080
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mInteriorArrowShadow:Lcom/google/android/street/Overlay$Polygon;

    move-object v5, v0

    const/4 v6, 0x6

    move-object v0, v5

    move-object/from16 v1, p1

    move v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/Overlay$Polygon;->draw(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 1081
    const/high16 v5, 0x10000

    const/high16 v6, 0x10000

    const/high16 v7, 0x10000

    const/high16 v8, 0x10000

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    move v4, v8

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 1082
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mInteriorArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v5, v0

    const/4 v6, 0x6

    move-object v0, v5

    move-object/from16 v1, p1

    move v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/Overlay$Polygon;->draw(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 1083
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    move-object v5, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    move-object v6, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mScratch:[F

    move-object v8, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mInteriorArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v9, v0

    const/4 v10, 0x0

    const/16 v11, 0x40

    move-object/from16 v7, p1

    move/from16 v12, p3

    invoke-virtual/range {v5 .. v12}, Lcom/google/android/street/Overlay$HitTester;->add(Lcom/google/android/street/Projector;Ljavax/microedition/khronos/opengles/GL10;[FLcom/google/android/street/Overlay$Polygon;III)V

    goto/16 :goto_7

    .line 1086
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v5, v0

    const/4 v6, 0x6

    move-object v0, v5

    move-object/from16 v1, p1

    move v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/street/Overlay$Polygon;->draw(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 1087
    const/high16 v5, 0x10000

    const/high16 v6, 0x10000

    const/high16 v7, 0x10000

    const/high16 v8, 0x10000

    move-object/from16 v0, p1

    move v1, v5

    move v2, v6

    move v3, v7

    move v4, v8

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 1088
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v5, v0

    move-object v0, v5

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/street/Overlay$Polygon;->drawOutline(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1089
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    move-object v5, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    move-object v6, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mScratch:[F

    move-object v8, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/street/Overlay;->mArrow:Lcom/google/android/street/Overlay$Polygon;

    move-object v9, v0

    const/4 v10, 0x0

    const/16 v11, 0x40

    move-object/from16 v7, p1

    move/from16 v12, p3

    invoke-virtual/range {v5 .. v12}, Lcom/google/android/street/Overlay$HitTester;->add(Lcom/google/android/street/Projector;Ljavax/microedition/khronos/opengles/GL10;[FLcom/google/android/street/Overlay$Polygon;III)V

    goto/16 :goto_7

    .line 1111
    :cond_14
    sget-object v7, Lcom/google/android/street/Overlay;->STREET_ANCHOR:[F

    move-object/from16 v0, p0

    move-object v1, v7

    move-object v2, v6

    invoke-direct {v0, v1, v2}, Lcom/google/android/street/Overlay;->getBaseProjection([F[F)V

    goto/16 :goto_8

    .line 1141
    :cond_15
    invoke-static {v13}, Lcom/google/android/street/StreetMath;->degreesToRadians(F)F

    move-result v9

    invoke-static {v9}, Landroid/util/FloatMath;->sin(F)F

    move-result v9

    .line 1145
    const/4 v12, 0x0

    cmpl-float v9, v9, v12

    if-ltz v9, :cond_16

    .line 1148
    add-float/2addr v7, v10

    .line 1149
    add-float/2addr v5, v11

    goto/16 :goto_9

    .line 1153
    :cond_16
    add-float v7, v10, v8

    .line 1154
    add-float/2addr v5, v11

    goto/16 :goto_9

    .line 1175
    :cond_17
    const/4 v5, 0x0

    const/high16 v7, 0x7fc00000    # NaNf

    aput v7, v6, v5

    .line 1176
    const/4 v5, 0x1

    const/high16 v7, 0x7fc00000    # NaNf

    aput v7, v6, v5

    goto/16 :goto_a

    .line 1187
    :cond_18
    invoke-interface/range {p1 .. p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    .line 1188
    return-void
.end method

.method private drawLinks(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;)V
    .locals 9
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "userOrientation"    # Lcom/google/android/street/UserOrientation;

    .prologue
    const/16 v8, 0x1700

    const/16 v7, 0xbe2

    const/4 v6, 0x0

    .line 916
    iget-boolean v3, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    if-nez v3, :cond_1

    .line 951
    :cond_0
    :goto_0
    return-void

    .line 919
    :cond_1
    iget v3, p0, Lcom/google/android/street/Overlay;->mAspectRatio:F

    cmpl-float v3, v3, v6

    if-eqz v3, :cond_0

    .line 925
    const/16 v3, 0x1701

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 926
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 927
    invoke-virtual {p2}, Lcom/google/android/street/UserOrientation;->getScale()F

    move-result v2

    .line 928
    .local v2, "scale":F
    iget v3, p0, Lcom/google/android/street/Overlay;->mAspectRatio:F

    invoke-static {v3}, Lcom/google/android/street/Renderer;->getUnzoomedVerticalFov(F)F

    move-result v3

    mul-float v0, v3, v2

    .line 929
    .local v0, "fovYDeg":F
    iget v3, p0, Lcom/google/android/street/Overlay;->mAspectRatio:F

    const v4, 0x3dcccccd    # 0.1f

    const/high16 v5, 0x42c80000    # 100.0f

    invoke-static {p1, v0, v3, v4, v5}, Landroid/opengl/GLU;->gluPerspective(Ljavax/microedition/khronos/opengles/GL10;FFFF)V

    .line 931
    invoke-interface {p1, v8}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 932
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 935
    const/high16 v3, -0x40000000    # -2.0f

    invoke-interface {p1, v6, v6, v3}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 936
    invoke-virtual {p2}, Lcom/google/android/street/UserOrientation;->getRotationMatrix()[F

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {p1, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    .line 937
    const/high16 v3, -0x40800000    # -1.0f

    invoke-interface {p1, v6, v3, v6}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 939
    const/16 v3, 0x1d00

    invoke-interface {p1, v3}, Ljavax/microedition/khronos/opengles/GL10;->glShadeModel(I)V

    .line 940
    invoke-interface {p1, v7}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 941
    const/16 v3, 0x302

    const/16 v4, 0x303

    invoke-interface {p1, v3, v4}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 943
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    invoke-virtual {v3, p1}, Lcom/google/android/street/Projector;->getCurrentProjection(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 944
    invoke-interface {p1, v8}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 946
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 947
    invoke-virtual {p2}, Lcom/google/android/street/UserOrientation;->getYaw()F

    move-result v3

    invoke-direct {p0, p1, v3, v1}, Lcom/google/android/street/Overlay;->drawLink(Ljavax/microedition/khronos/opengles/GL10;FI)V

    .line 946
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 950
    :cond_2
    invoke-interface {p1, v7}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    goto :goto_0
.end method

.method private drawPancake(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;Lcom/google/android/street/Overlay$Pancake;)V
    .locals 10
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "userOrientation"    # Lcom/google/android/street/UserOrientation;
    .param p3, "pancake"    # Lcom/google/android/street/Overlay$Pancake;

    .prologue
    const/4 v9, 0x0

    const/high16 v8, 0x3e800000    # 0.25f

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v6, 0x10000

    const/4 v5, 0x0

    .line 767
    iget-object v2, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v2, v2, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    if-nez v2, :cond_1

    .line 824
    :cond_0
    :goto_0
    return-void

    .line 771
    :cond_1
    const/16 v2, 0x1700

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 772
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glLoadIdentity()V

    .line 774
    if-eqz p3, :cond_0

    .line 781
    invoke-virtual {p2}, Lcom/google/android/street/UserOrientation;->getRotationMatrix()[F

    move-result-object v2

    invoke-interface {p1, v2, v9}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    .line 782
    iget v2, p3, Lcom/google/android/street/Overlay$Pancake;->mYaw:F

    invoke-static {v2}, Lcom/google/android/street/StreetMath;->unitToDegrees(F)F

    move-result v2

    neg-float v2, v2

    invoke-interface {p1, v2, v5, v7, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 783
    iget v2, p3, Lcom/google/android/street/Overlay$Pancake;->mPitch:F

    sub-float v2, v8, v2

    invoke-static {v2}, Lcom/google/android/street/StreetMath;->unitToDegrees(F)F

    move-result v2

    neg-float v2, v2

    invoke-interface {p1, v2, v7, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 785
    iget v2, p3, Lcom/google/android/street/Overlay$Pancake;->mDistance:F

    cmpl-float v2, v2, v5

    if-lez v2, :cond_2

    .line 787
    iget v2, p3, Lcom/google/android/street/Overlay$Pancake;->mDistance:F

    neg-float v2, v2

    invoke-interface {p1, v5, v5, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 790
    iget v2, p3, Lcom/google/android/street/Overlay$Pancake;->mPitch:F

    sub-float v2, v8, v2

    invoke-static {v2}, Lcom/google/android/street/StreetMath;->unitToDegrees(F)F

    move-result v2

    invoke-interface {p1, v2, v7, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 792
    iget v2, p3, Lcom/google/android/street/Overlay$Pancake;->mYaw:F

    invoke-static {v2}, Lcom/google/android/street/StreetMath;->unitToDegrees(F)F

    move-result v2

    invoke-interface {p1, v2, v5, v7, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 795
    const/4 v2, 0x2

    new-array v0, v2, [F

    .line 799
    .local v0, "normalYawPitch":[F
    iget v2, p3, Lcom/google/android/street/Overlay$Pancake;->mNx:F

    iget v3, p3, Lcom/google/android/street/Overlay$Pancake;->mNy:F

    iget v4, p3, Lcom/google/android/street/Overlay$Pancake;->mNz:F

    neg-float v4, v4

    invoke-static {v2, v3, v4, v0}, Lcom/google/android/street/StreetMath;->rectangularToSphericalCoords(FFF[F)V

    .line 801
    aget v2, v0, v9

    invoke-static {v2}, Lcom/google/android/street/StreetMath;->unitToDegrees(F)F

    move-result v2

    neg-float v2, v2

    invoke-interface {p1, v2, v5, v7, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 803
    const/4 v2, 0x1

    aget v2, v0, v2

    sub-float v2, v8, v2

    invoke-static {v2}, Lcom/google/android/street/StreetMath;->unitToDegrees(F)F

    move-result v2

    invoke-interface {p1, v2, v7, v5, v5}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 810
    .end local v0    # "normalYawPitch":[F
    :cond_2
    if-eqz p3, :cond_3

    iget-boolean v2, p3, Lcom/google/android/street/Overlay$Pancake;->mIsGround:Z

    if-eqz v2, :cond_3

    .line 811
    sget-object v1, Lcom/google/android/street/Overlay;->GROUND_PANCAKE_POLYGON:Lcom/google/android/street/Overlay$Polygon;

    .line 815
    .local v1, "pancakePolygon":Lcom/google/android/street/Overlay$Polygon;
    :goto_1
    const/16 v2, 0xbe2

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 816
    const/16 v2, 0x302

    const/16 v3, 0x303

    invoke-interface {p1, v2, v3}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 817
    const v2, 0x8000

    invoke-interface {p1, v6, v6, v6, v2}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 818
    const/4 v2, 0x5

    invoke-virtual {v1, p1, v2}, Lcom/google/android/street/Overlay$Polygon;->draw(Ljavax/microedition/khronos/opengles/GL10;I)V

    .line 819
    invoke-interface {p1, v6, v6, v6, v6}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 820
    invoke-virtual {v1, p1}, Lcom/google/android/street/Overlay$Polygon;->drawOutline(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 821
    const/16 v2, 0xbe2

    invoke-interface {p1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 822
    iget-object v2, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    invoke-virtual {v2, p1}, Lcom/google/android/street/Projector;->getCurrentModelView(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 823
    iget-object v2, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    invoke-virtual {v2, p1}, Lcom/google/android/street/Projector;->getCurrentProjection(Ljavax/microedition/khronos/opengles/GL10;)V

    goto/16 :goto_0

    .line 813
    .end local v1    # "pancakePolygon":Lcom/google/android/street/Overlay$Polygon;
    :cond_3
    sget-object v1, Lcom/google/android/street/Overlay;->PANCAKE_POLYGON:Lcom/google/android/street/Overlay$Polygon;

    .restart local v1    # "pancakePolygon":Lcom/google/android/street/Overlay$Polygon;
    goto :goto_1
.end method

.method private drawPanoPoints(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 13
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    const/4 v12, 0x0

    const/high16 v11, 0x10000

    const/4 v5, 0x0

    .line 828
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    if-nez v0, :cond_1

    .line 862
    :cond_0
    return-void

    .line 832
    :cond_1
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget v0, v0, Lcom/google/android/street/PanoramaConfig;->mPanoYawDeg:F

    neg-float v0, v0

    const/high16 v1, 0x43340000    # 180.0f

    sub-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {p1, v0, v12, v1, v12}, Ljavax/microedition/khronos/opengles/GL10;->glRotatef(FFFF)V

    .line 833
    const/16 v0, 0x10

    new-array v9, v0, [F

    .line 834
    .local v9, "inverseTiltMatrix":[F
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    invoke-virtual {v0}, Lcom/google/android/street/PanoramaConfig;->getTiltMatrix()[F

    move-result-object v0

    invoke-static {v9, v5, v0, v5}, Landroid/opengl/Matrix;->invertM([FI[FI)Z

    .line 835
    invoke-interface {p1, v9, v5}, Ljavax/microedition/khronos/opengles/GL10;->glMultMatrixf([FI)V

    .line 837
    new-instance v4, Lcom/google/android/street/Overlay$Polygon;

    sget-object v0, Lcom/google/android/street/Overlay;->PYRAMID_DATA:[F

    sget-object v1, Lcom/google/android/street/Overlay;->PYRAMID_FILL_INDEX:[B

    sget-object v2, Lcom/google/android/street/Overlay;->PYRAMID_OUTLINE_INDEX:[B

    invoke-direct {v4, v0, v1, v2}, Lcom/google/android/street/Overlay$Polygon;-><init>([F[B[B)V

    .line 842
    .local v4, "pyramid":Lcom/google/android/street/Overlay$Polygon;
    const/4 v8, 0x1

    .local v8, "i":I
    :goto_0
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    invoke-virtual {v0}, Lcom/google/android/street/DepthMap;->numPanos()I

    move-result v0

    if-ge v8, v0, :cond_0

    .line 843
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    invoke-virtual {v0, v8}, Lcom/google/android/street/DepthMap;->getPanoId(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v1, v1, Lcom/google/android/street/PanoramaConfig;->mPanoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 842
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 847
    :cond_2
    iget v0, p0, Lcom/google/android/street/Overlay;->mHighlight:I

    add-int/lit8 v1, v8, 0x14

    if-ne v0, v1, :cond_3

    .line 848
    invoke-interface {p1, v5, v11, v5, v11}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 852
    :goto_2
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPushMatrix()V

    .line 853
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v0, v0, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    invoke-virtual {v0, v8}, Lcom/google/android/street/DepthMap;->getPanoPoint(I)Lcom/google/android/street/DepthMap$Point;

    move-result-object v10

    .line 854
    .local v10, "point":Lcom/google/android/street/DepthMap$Point;
    iget v0, v10, Lcom/google/android/street/DepthMap$Point;->x:F

    neg-float v0, v0

    iget v1, v10, Lcom/google/android/street/DepthMap$Point;->y:F

    invoke-interface {p1, v0, v12, v1}, Ljavax/microedition/khronos/opengles/GL10;->glTranslatef(FFF)V

    .line 855
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    invoke-virtual {v0, p1}, Lcom/google/android/street/Projector;->getCurrentModelView(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 856
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    invoke-virtual {v0, p1}, Lcom/google/android/street/Projector;->getCurrentProjection(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 857
    invoke-virtual {v4, p1}, Lcom/google/android/street/Overlay$Polygon;->drawOutline(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 858
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mScratch:[F

    add-int/lit8 v7, v8, 0x14

    move-object v2, p1

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/street/Overlay$HitTester;->add(Lcom/google/android/street/Projector;Ljavax/microedition/khronos/opengles/GL10;[FLcom/google/android/street/Overlay$Polygon;III)V

    .line 860
    invoke-interface {p1}, Ljavax/microedition/khronos/opengles/GL10;->glPopMatrix()V

    goto :goto_1

    .line 850
    .end local v10    # "point":Lcom/google/android/street/DepthMap$Point;
    :cond_3
    invoke-interface {p1, v5, v5, v11, v11}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    goto :goto_2
.end method

.method private endLabelOpacity(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4

    .prologue
    const/16 v3, 0x1000

    .line 1349
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

    invoke-virtual {v0}, Lcom/google/android/street/Overlay$FadeAnimation;->getOpacity()I

    move-result v0

    .line 1350
    const/high16 v1, 0x10000

    if-eq v0, v1, :cond_0

    .line 1351
    const/16 v0, 0x2300

    const/16 v1, 0x2200

    const v2, 0x45f00800    # 7681.0f

    invoke-interface {p1, v0, v1, v2}, Ljavax/microedition/khronos/opengles/GL10;->glTexEnvf(IIF)V

    .line 1353
    invoke-interface {p1, v3, v3, v3, v3}, Ljavax/microedition/khronos/opengles/GL10;->glColor4x(IIII)V

    .line 1355
    :cond_0
    return-void
.end method

.method private getBaseProjection([F[F)V
    .locals 2
    .param p1, "input"    # [F
    .param p2, "output"    # [F

    .prologue
    const/4 v1, 0x0

    .line 1218
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mProjector:Lcom/google/android/street/Projector;

    invoke-virtual {v0, p1, v1, p2, v1}, Lcom/google/android/street/Projector;->project([FI[FI)V

    .line 1219
    return-void
.end method

.method private static getCircle(FI)[F
    .locals 9

    .prologue
    .line 1415
    mul-int/lit8 v0, p1, 0xc

    new-array v0, v0, [F

    .line 1417
    const v1, 0x3fc90fdb

    int-to-float v2, p1

    div-float/2addr v1, v2

    .line 1418
    const/4 v2, 0x0

    .line 1421
    const/4 v3, 0x0

    move v8, v3

    move v3, v2

    move v2, v8

    :goto_0
    if-gt v2, p1, :cond_1

    .line 1422
    invoke-static {v3}, Landroid/util/FloatMath;->cos(F)F

    move-result v4

    .line 1423
    invoke-static {v3}, Landroid/util/FloatMath;->sin(F)F

    move-result v5

    .line 1424
    mul-int/lit8 v6, v2, 0x3

    aput v4, v0, v6

    .line 1425
    mul-int/lit8 v6, v2, 0x3

    add-int/lit8 v6, v6, 0x1

    aput v5, v0, v6

    .line 1426
    mul-int/lit8 v6, p1, 0x2

    mul-int/lit8 v6, v6, 0x3

    mul-int/lit8 v7, v2, 0x3

    add-int/2addr v6, v7

    neg-float v7, v4

    aput v7, v0, v6

    .line 1427
    mul-int/lit8 v6, p1, 0x2

    mul-int/lit8 v6, v6, 0x3

    mul-int/lit8 v7, v2, 0x3

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, 0x1

    neg-float v7, v5

    aput v7, v0, v6

    .line 1430
    if-lez v2, :cond_0

    if-ge v2, p1, :cond_0

    .line 1431
    mul-int/lit8 v6, p1, 0x2

    mul-int/lit8 v6, v6, 0x3

    mul-int/lit8 v7, v2, 0x3

    sub-int/2addr v6, v7

    neg-float v7, v4

    aput v7, v0, v6

    .line 1432
    mul-int/lit8 v6, p1, 0x2

    mul-int/lit8 v6, v6, 0x3

    mul-int/lit8 v7, v2, 0x3

    sub-int/2addr v6, v7

    add-int/lit8 v6, v6, 0x1

    aput v5, v0, v6

    .line 1433
    mul-int/lit8 v6, p1, 0x4

    mul-int/lit8 v6, v6, 0x3

    mul-int/lit8 v7, v2, 0x3

    sub-int/2addr v6, v7

    aput v4, v0, v6

    .line 1434
    mul-int/lit8 v4, p1, 0x4

    mul-int/lit8 v4, v4, 0x3

    mul-int/lit8 v6, v2, 0x3

    sub-int/2addr v4, v6

    add-int/lit8 v4, v4, 0x1

    neg-float v5, v5

    aput v5, v0, v4

    .line 1436
    :cond_0
    add-float/2addr v3, v1

    .line 1421
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1438
    :cond_1
    return-object v0
.end method

.method private getDirectionText(Lcom/google/android/street/PanoramaLink;)Ljava/lang/CharSequence;
    .locals 2
    .param p1, "link"    # Lcom/google/android/street/PanoramaLink;

    .prologue
    .line 1366
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mCompassDirectionNames:[Ljava/lang/CharSequence;

    iget v1, p1, Lcom/google/android/street/PanoramaLink;->mDirection:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method private static getPolygonStripIndices(I)[B
    .locals 4
    .param p0, "numVertices"    # I

    .prologue
    .line 1446
    new-array v1, p0, [B

    .line 1447
    .local v1, "indices":[B
    const/4 v0, 0x0

    .local v0, "i":B
    :goto_0
    if-ge v0, p0, :cond_1

    .line 1450
    rem-int/lit8 v2, v0, 0x2

    if-nez v2, :cond_0

    div-int/lit8 v2, v0, 0x2

    :goto_1
    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 1447
    add-int/lit8 v2, v0, 0x1

    int-to-byte v0, v2

    goto :goto_0

    .line 1450
    :cond_0
    const/4 v2, 0x1

    sub-int v2, p0, v2

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    goto :goto_1

    .line 1454
    :cond_1
    return-object v1
.end method

.method private static getRange(I)[B
    .locals 3
    .param p0, "n"    # I

    .prologue
    .line 1462
    new-array v1, p0, [B

    .line 1463
    .local v1, "range":[B
    const/4 v0, 0x0

    .local v0, "i":B
    :goto_0
    if-ge v0, p0, :cond_0

    .line 1464
    aput-byte v0, v1, v0

    .line 1463
    add-int/lit8 v2, v0, 0x1

    int-to-byte v0, v2

    goto :goto_0

    .line 1466
    :cond_0
    return-object v1
.end method

.method private handleLabelsOutOfMemory(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 1
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    .line 745
    invoke-direct {p0, p1}, Lcom/google/android/street/Overlay;->clearLabelMakers(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 749
    iget-boolean v0, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    if-eqz v0, :cond_0

    .line 750
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mDrawRoadLabels:Z

    .line 753
    invoke-direct {p0}, Lcom/google/android/street/Overlay;->createLabelMakers()V

    .line 759
    :goto_0
    return-void

    .line 757
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mDrawDisabled:Z

    goto :goto_0
.end method

.method private updateLinkInfo()V
    .locals 10

    .prologue
    const/high16 v5, 0x41a00000    # 20.0f

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 521
    iput-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    .line 522
    iput-object v3, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    .line 523
    iput-object v3, p0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    .line 524
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v3, v3, Lcom/google/android/street/PanoramaConfig;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    .line 526
    iget v3, p0, Lcom/google/android/street/Overlay;->mIncomingYaw:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_4

    iget v3, p0, Lcom/google/android/street/Overlay;->mOutgoingYaw:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_4

    .line 527
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget v4, p0, Lcom/google/android/street/Overlay;->mIncomingYaw:F

    invoke-virtual {v3, v4, v5}, Lcom/google/android/street/PanoramaConfig;->getClosestLink(FF)Lcom/google/android/street/PanoramaLink;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    .line 529
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget v4, p0, Lcom/google/android/street/Overlay;->mOutgoingYaw:F

    invoke-virtual {v3, v4, v5}, Lcom/google/android/street/PanoramaConfig;->getClosestLink(FF)Lcom/google/android/street/PanoramaLink;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    .line 534
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    if-ne v3, v4, :cond_0

    .line 535
    new-instance v3, Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iget v4, v4, Lcom/google/android/street/PanoramaLink;->mYawDeg:F

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iget-object v5, v5, Lcom/google/android/street/PanoramaLink;->mPanoId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iget v6, v6, Lcom/google/android/street/PanoramaLink;->mRoadARGB:I

    iget-object v7, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iget-object v7, v7, Lcom/google/android/street/PanoramaLink;->mLinkText:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/street/PanoramaLink;-><init>(FLjava/lang/String;ILjava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    .line 547
    :cond_0
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    if-nez v3, :cond_5

    move v3, v9

    :goto_0
    iget-object v4, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    if-nez v4, :cond_6

    move v4, v9

    :goto_1
    add-int v1, v3, v4

    .line 549
    .local v1, "numExtraLinks":I
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v3, v3

    add-int/2addr v3, v1

    new-array v3, v3, [Lcom/google/android/street/PanoramaLink;

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    .line 550
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v3, v3, Lcom/google/android/street/PanoramaConfig;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v5, v5, Lcom/google/android/street/PanoramaConfig;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v5, v5

    invoke-static {v3, v8, v4, v8, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 552
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    if-nez v3, :cond_1

    .line 553
    new-instance v3, Lcom/google/android/street/PanoramaLink;

    iget v4, p0, Lcom/google/android/street/Overlay;->mIncomingYaw:F

    const-string v5, ""

    const-string v6, ""

    invoke-direct {v3, v4, v5, v8, v6}, Lcom/google/android/street/PanoramaLink;-><init>(FLjava/lang/String;ILjava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    .line 554
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v4, v4

    add-int/lit8 v2, v1, -0x1

    .end local v1    # "numExtraLinks":I
    .local v2, "numExtraLinks":I
    sub-int/2addr v4, v1

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    aput-object v5, v3, v4

    move v1, v2

    .line 556
    .end local v2    # "numExtraLinks":I
    .restart local v1    # "numExtraLinks":I
    :cond_1
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    if-nez v3, :cond_2

    .line 557
    new-instance v3, Lcom/google/android/street/PanoramaLink;

    iget v4, p0, Lcom/google/android/street/Overlay;->mOutgoingYaw:F

    const-string v5, ""

    const-string v6, ""

    invoke-direct {v3, v4, v5, v8, v6}, Lcom/google/android/street/PanoramaLink;-><init>(FLjava/lang/String;ILjava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    .line 558
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v4, v4

    sub-int/2addr v4, v1

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    aput-object v5, v3, v4

    .line 563
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v3, v3

    sub-int/2addr v3, v9

    if-ge v0, v3, :cond_3

    .line 564
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    if-ne v3, v4, :cond_7

    .line 565
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v5, v5

    sub-int/2addr v5, v9

    aget-object v4, v4, v5

    aput-object v4, v3, v0

    .line 566
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    array-length v4, v4

    sub-int/2addr v4, v9

    iget-object v5, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    aput-object v5, v3, v4

    .line 570
    :cond_3
    iget-object v3, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    iget-object v4, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    invoke-static {v3, v4}, Lcom/google/android/street/Overlay;->createDirectionsArrow(Lcom/google/android/street/PanoramaLink;Lcom/google/android/street/PanoramaLink;)Lcom/google/android/street/Overlay$Polygon;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/street/Overlay;->mDirectionsArrow:Lcom/google/android/street/Overlay$Polygon;

    .line 573
    .end local v0    # "i":I
    .end local v1    # "numExtraLinks":I
    :cond_4
    return-void

    :cond_5
    move v3, v8

    .line 547
    goto/16 :goto_0

    :cond_6
    move v4, v8

    goto/16 :goto_1

    .line 563
    .restart local v0    # "i":I
    .restart local v1    # "numExtraLinks":I
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method


# virtual methods
.method public doSetMotionUse(IJ)V
    .locals 1
    .param p1, "device"    # I
    .param p2, "time"    # J

    .prologue
    const/4 v0, 0x1

    .line 483
    if-nez p1, :cond_1

    .line 484
    iput-wide p2, p0, Lcom/google/android/street/Overlay;->mLastTrackballTime:J

    .line 485
    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mTrackballUsed:Z

    .line 489
    :cond_0
    :goto_0
    return-void

    .line 486
    :cond_1
    if-ne p1, v0, :cond_0

    .line 487
    iput-boolean v0, p0, Lcom/google/android/street/Overlay;->mTouchUsed:Z

    goto :goto_0
.end method

.method public draw(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;Lcom/google/android/street/Overlay$Pancake;J)V
    .locals 9
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "userOrientation"    # Lcom/google/android/street/UserOrientation;
    .param p3, "pancake"    # Lcom/google/android/street/Overlay$Pancake;
    .param p4, "currentTime"    # J

    .prologue
    .line 673
    iget-boolean v6, p0, Lcom/google/android/street/Overlay;->mDrawDisabled:Z

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    if-nez v6, :cond_1

    .line 740
    :cond_0
    :goto_0
    return-void

    .line 678
    :cond_1
    const/4 v6, 0x1

    new-array v3, v6, [I

    .line 679
    .local v3, "maxTextureSize":[I
    const/16 v6, 0xd33

    const/4 v7, 0x0

    invoke-interface {p1, v6, v3, v7}, Ljavax/microedition/khronos/opengles/GL10;->glGetIntegerv(I[II)V

    .line 682
    iget-object v6, p0, Lcom/google/android/street/Overlay;->mIncomingLink:Lcom/google/android/street/PanoramaLink;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mOutgoingLink:Lcom/google/android/street/PanoramaLink;

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    move v2, v6

    .line 685
    .local v2, "isDirectionsMode":Z
    :goto_1
    iget-object v6, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-object v6, v6, Lcom/google/android/street/PanoramaConfig;->mDepthMap:Lcom/google/android/street/DepthMap;

    if-eqz v6, :cond_4

    const/4 v6, 0x1

    move v1, v6

    .line 691
    .local v1, "hasDepthMap":Z
    :goto_2
    if-nez v2, :cond_5

    const/4 v6, 0x0

    aget v6, v3, v6

    const/16 v7, 0x800

    if-lt v6, v7, :cond_5

    if-eqz v1, :cond_5

    iget-object v6, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget-boolean v6, v6, Lcom/google/android/street/PanoramaConfig;->mDisabled:Z

    if-nez v6, :cond_5

    const/4 v6, 0x1

    move v5, v6

    .line 695
    .local v5, "useFancyLabels":Z
    :goto_3
    invoke-direct {p0, p2, p4, p5}, Lcom/google/android/street/Overlay;->computeAnimation(Lcom/google/android/street/UserOrientation;J)V

    .line 698
    if-eqz v5, :cond_6

    :try_start_0
    iget-boolean v6, p0, Lcom/google/android/street/Overlay;->mIsIndoorScene:Z

    if-nez v6, :cond_6

    const/4 v6, 0x1

    :goto_4
    invoke-direct {p0, p1, v6}, Lcom/google/android/street/Overlay;->addLabels(Ljavax/microedition/khronos/opengles/GL10;Z)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 704
    iget-object v6, p0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    iget v7, p0, Lcom/google/android/street/Overlay;->mViewWidth:I

    iget v8, p0, Lcom/google/android/street/Overlay;->mViewHeight:I

    invoke-virtual {v6, v7, v8}, Lcom/google/android/street/Overlay$HitTester;->reset(II)V

    .line 706
    iget-boolean v6, p0, Lcom/google/android/street/Overlay;->mIsIndoorScene:Z

    if-eqz v6, :cond_7

    .line 709
    invoke-direct {p0, p1, p2}, Lcom/google/android/street/Overlay;->drawLinks(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;)V

    .line 710
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {p0, p1, v6, v7}, Lcom/google/android/street/Overlay;->drawLabels(Ljavax/microedition/khronos/opengles/GL10;ZZ)V

    .line 735
    :cond_2
    :goto_5
    iget-object v6, p0, Lcom/google/android/street/Overlay;->mHitTesterLock:Ljava/lang/Object;

    monitor-enter v6

    .line 736
    :try_start_1
    iget-object v4, p0, Lcom/google/android/street/Overlay;->mPublicHitTester:Lcom/google/android/street/Overlay$HitTester;

    .line 737
    .local v4, "temp":Lcom/google/android/street/Overlay$HitTester;
    iget-object v7, p0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    iput-object v7, p0, Lcom/google/android/street/Overlay;->mPublicHitTester:Lcom/google/android/street/Overlay$HitTester;

    .line 738
    iput-object v4, p0, Lcom/google/android/street/Overlay;->mNextFrameHitTester:Lcom/google/android/street/Overlay$HitTester;

    .line 739
    monitor-exit v6

    goto :goto_0

    .end local v4    # "temp":Lcom/google/android/street/Overlay$HitTester;
    :catchall_0
    move-exception v7

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v7

    .line 682
    .end local v1    # "hasDepthMap":Z
    .end local v2    # "isDirectionsMode":Z
    .end local v5    # "useFancyLabels":Z
    :cond_3
    const/4 v6, 0x0

    move v2, v6

    goto :goto_1

    .line 685
    .restart local v2    # "isDirectionsMode":Z
    :cond_4
    const/4 v6, 0x0

    move v1, v6

    goto :goto_2

    .line 691
    .restart local v1    # "hasDepthMap":Z
    :cond_5
    const/4 v6, 0x0

    move v5, v6

    goto :goto_3

    .line 698
    .restart local v5    # "useFancyLabels":Z
    :cond_6
    const/4 v6, 0x0

    goto :goto_4

    .line 699
    :catch_0
    move-exception v6

    move-object v0, v6

    .line 700
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-direct {p0, p1}, Lcom/google/android/street/Overlay;->handleLabelsOutOfMemory(Ljavax/microedition/khronos/opengles/GL10;)V

    goto :goto_0

    .line 712
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :cond_7
    if-eqz v5, :cond_9

    .line 714
    invoke-direct {p0, p1, p2}, Lcom/google/android/street/Overlay;->drawFancyStreetLabels(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;)V

    .line 722
    :goto_6
    if-nez p3, :cond_a

    if-eqz v1, :cond_a

    const/4 v6, 0x1

    :goto_7
    invoke-direct {p0, p1, v6, v5}, Lcom/google/android/street/Overlay;->drawLabels(Ljavax/microedition/khronos/opengles/GL10;ZZ)V

    .line 725
    iget-boolean v6, p0, Lcom/google/android/street/Overlay;->mEnablePanoPoints:Z

    if-eqz v6, :cond_8

    .line 726
    invoke-direct {p0, p1}, Lcom/google/android/street/Overlay;->drawPanoPoints(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 730
    :cond_8
    iget-boolean v6, p0, Lcom/google/android/street/Overlay;->mEnablePancake:Z

    if-eqz v6, :cond_2

    .line 731
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/street/Overlay;->drawPancake(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;Lcom/google/android/street/Overlay$Pancake;)V

    goto :goto_5

    .line 718
    :cond_9
    invoke-direct {p0, p1, p2}, Lcom/google/android/street/Overlay;->drawLinks(Ljavax/microedition/khronos/opengles/GL10;Lcom/google/android/street/UserOrientation;)V

    goto :goto_6

    .line 722
    :cond_a
    const/4 v6, 0x0

    goto :goto_7
.end method

.method public getNextDrawTime()J
    .locals 2

    .prologue
    .line 1195
    iget-wide v0, p0, Lcom/google/android/street/Overlay;->mNextDrawTime:J

    return-wide v0
.end method

.method public getPanoramaLink(I)Lcom/google/android/street/PanoramaLink;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 512
    :try_start_0
    iget-object v1, p0, Lcom/google/android/street/Overlay;->mLinks:[Lcom/google/android/street/PanoramaLink;

    aget-object v1, v1, p1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 514
    :goto_0
    return-object v1

    .line 513
    :catch_0
    move-exception v1

    move-object v0, v1

    .line 514
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hit(II)I
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1207
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mHitTesterLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1208
    :try_start_0
    iget-object v1, p0, Lcom/google/android/street/Overlay;->mPublicHitTester:Lcom/google/android/street/Overlay$HitTester;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/street/Overlay$HitTester;->hit(II)I

    move-result v1

    monitor-exit v0

    return v1

    .line 1209
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public initialize(Lcom/google/android/street/PanoramaConfig;II)V
    .locals 5
    .param p1, "config"    # Lcom/google/android/street/PanoramaConfig;
    .param p2, "viewWidth"    # I
    .param p3, "viewHeight"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 444
    iput-object p1, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    .line 445
    iget-object v1, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    iget v1, v1, Lcom/google/android/street/PanoramaConfig;->mSceneType:I

    if-ne v1, v3, :cond_0

    move v1, v3

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/street/Overlay;->mIsIndoorScene:Z

    .line 447
    invoke-direct {p0}, Lcom/google/android/street/Overlay;->updateLinkInfo()V

    .line 448
    iput p2, p0, Lcom/google/android/street/Overlay;->mViewWidth:I

    .line 449
    iput p3, p0, Lcom/google/android/street/Overlay;->mViewHeight:I

    .line 450
    int-to-float v1, p2

    int-to-float v2, p3

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/street/Overlay;->mAspectRatio:F

    .line 451
    iput-boolean v4, p0, Lcom/google/android/street/Overlay;->mLabelsComputed:Z

    .line 452
    iget-object v1, p0, Lcom/google/android/street/Overlay;->mLabelAnimation:Lcom/google/android/street/Overlay$FadeAnimation;

    invoke-virtual {v1, v3}, Lcom/google/android/street/Overlay$FadeAnimation;->reset(Z)V

    .line 453
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/street/Overlay;->mHighlight:I

    .line 454
    iget-object v1, p0, Lcom/google/android/street/Overlay;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 455
    .local v0, "resources":Landroid/content/res/Resources;
    const/4 v1, 0x3

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Lcom/google/android/street/Overlay;->mAddressBubble:[Landroid/graphics/drawable/Drawable;

    .line 456
    iget-object v1, p0, Lcom/google/android/street/Overlay;->mAddressBubble:[Landroid/graphics/drawable/Drawable;

    const/high16 v2, 0x7f020000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, v4

    .line 458
    iget-object v1, p0, Lcom/google/android/street/Overlay;->mAddressBubble:[Landroid/graphics/drawable/Drawable;

    const v2, 0x7f020001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, v3

    .line 460
    iget-object v1, p0, Lcom/google/android/street/Overlay;->mAddressBubble:[Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x2

    const v3, 0x7f020002

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v1, v2

    .line 462
    const v1, 0x7f020008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/street/Overlay;->mPegmanOnPancake:Landroid/graphics/drawable/Drawable;

    .line 463
    return-void

    .end local v0    # "resources":Landroid/content/res/Resources;
    :cond_0
    move v1, v4

    .line 445
    goto :goto_0
.end method

.method public setDirectionsArrowParams(FF)V
    .locals 1
    .param p1, "incomingYaw"    # F
    .param p2, "outgoingYaw"    # F

    .prologue
    .line 498
    iput p1, p0, Lcom/google/android/street/Overlay;->mIncomingYaw:F

    .line 499
    iput p2, p0, Lcom/google/android/street/Overlay;->mOutgoingYaw:F

    .line 500
    iget-object v0, p0, Lcom/google/android/street/Overlay;->mConfig:Lcom/google/android/street/PanoramaConfig;

    if-eqz v0, :cond_0

    .line 501
    invoke-direct {p0}, Lcom/google/android/street/Overlay;->updateLinkInfo()V

    .line 503
    :cond_0
    return-void
.end method

.method public setHighlight(I)V
    .locals 0
    .param p1, "highlight"    # I

    .prologue
    .line 1214
    iput p1, p0, Lcom/google/android/street/Overlay;->mHighlight:I

    .line 1215
    return-void
.end method

.method public shutdown(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 0
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    .line 466
    invoke-direct {p0, p1}, Lcom/google/android/street/Overlay;->clearLabelMakers(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 467
    return-void
.end method
