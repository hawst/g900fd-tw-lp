.class public abstract Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;
.super Lcom/sec/android/providers/tasks/SyncableContentProvider;
.source "AbstractSyncableContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final SYNC_ACCOUNT_WHERE_CLAUSE:Ljava/lang/String; = "_sync_account=? AND _sync_account_type=?"

.field private static final TAG:Ljava/lang/String; = "AbstractSyncableContentProvider"

.field private static final sAccountProjection:[Ljava/lang/String;


# instance fields
.field private mAccounts:[Landroid/accounts/Account;

.field private final mApplyingBatch:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mContainsDiffs:Z

.field private final mContentUri:Landroid/net/Uri;

.field private mCurrentMerger:Lcom/sec/android/providers/tasks/AbstractTableMerger;

.field private final mDatabaseName:Ljava/lang/String;

.field private final mDatabaseVersion:I

.field protected mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mIsMergeCancelled:Z

.field private mIsTemporary:Z

.field protected mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

.field private final mPendingBatchNotifications:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;>;"
        }
    .end annotation
.end field

.field private mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

.field private mSyncingAccount:Landroid/accounts/Account;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 70
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_sync_account"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_sync_account_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->sAccountProjection:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILandroid/net/Uri;)V
    .locals 3
    .param p1, "dbName"    # Ljava/lang/String;
    .param p2, "dbVersion"    # I
    .param p3, "contentUri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 105
    invoke-direct {p0}, Lcom/sec/android/providers/tasks/SyncableContentProvider;-><init>()V

    .line 68
    iput-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    .line 75
    iput-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mCurrentMerger:Lcom/sec/android/providers/tasks/AbstractTableMerger;

    .line 76
    iput-boolean v2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mIsMergeCancelled:Z

    .line 85
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mApplyingBatch:Ljava/lang/ThreadLocal;

    .line 86
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mPendingBatchNotifications:Ljava/lang/ThreadLocal;

    .line 106
    const-string v0, "SANDEEP"

    const-string v1, "In abstract syncable content"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iput-object p1, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDatabaseName:Ljava/lang/String;

    .line 108
    iput p2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDatabaseVersion:I

    .line 109
    iput-object p3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mContentUri:Landroid/net/Uri;

    .line 110
    iput-boolean v2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mIsTemporary:Z

    .line 111
    invoke-virtual {p0, v2}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->setContainsDiffs(Z)V

    .line 115
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDatabaseVersion:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mContentUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;)Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;[Landroid/accounts/Account;)[Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;
    .param p1, "x1"    # [Landroid/accounts/Account;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mAccounts:[Landroid/accounts/Account;

    return-object p1
.end method

.method private applyingBatch()Z
    .locals 2

    .prologue
    .line 385
    const-string v0, "AbstractSyncableContentProvider"

    const-string v1, "AbstractSyncableContentProvider applyingBatch()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mApplyingBatch:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mApplyingBatch:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 537
    .local p1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const-string v2, "AbstractSyncableContentProvider"

    const-string v3, "AbstractSyncableContentProvider applyBatch()"

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    const/4 v1, 0x0

    .line 539
    .local v1, "successful":Z
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->beginBatch()V

    .line 541
    :try_start_0
    invoke-super {p0, p1}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 542
    .local v0, "results":[Landroid/content/ContentProviderResult;
    const/4 v1, 0x1

    .line 545
    invoke-virtual {p0, v1}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->endBatch(Z)V

    return-object v0

    .end local v0    # "results":[Landroid/content/ContentProviderResult;
    :catchall_0
    move-exception v2

    invoke-virtual {p0, v1}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->endBatch(Z)V

    throw v2
.end method

.method public final beginBatch()V
    .locals 4

    .prologue
    .line 482
    const-string v2, "AbstractSyncableContentProvider"

    const-string v3, "AbstractSyncableContentProvider beginBatch()"

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    iget-object v2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mApplyingBatch:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 485
    iget-object v2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mApplyingBatch:Ljava/lang/ThreadLocal;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 486
    iget-object v2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mPendingBatchNotifications:Ljava/lang/ThreadLocal;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 489
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->applyingBatch()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 490
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "applyBatch is not reentrant but mApplyingBatch is already set"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 493
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 494
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 495
    const/4 v1, 0x0

    .line 497
    .local v1, "successful":Z
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mApplyingBatch:Ljava/lang/ThreadLocal;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 498
    const/4 v1, 0x1

    .line 500
    if-nez v1, :cond_2

    .line 502
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 505
    :cond_2
    return-void

    .line 500
    :catchall_0
    move-exception v2

    if-nez v1, :cond_3

    .line 502
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_3
    throw v2
.end method

.method protected bootstrapDatabase(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 134
    const-string v0, "AbstractSyncableContentProvider"

    const-string v1, "======== bootstrapDatabase ========"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->createDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 136
    return-void
.end method

.method public final bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 428
    const-string v5, "AbstractSyncableContentProvider"

    const-string v6, "bulkInsert()"

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    array-length v4, p2

    .line 430
    .local v4, "size":I
    const/4 v0, 0x0

    .line 431
    .local v0, "completed":I
    iget-object v5, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    invoke-virtual {v5, p1}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->matches(Landroid/net/Uri;)Z

    move-result v2

    .line 432
    .local v2, "isSyncStateUri":Z
    iget-object v5, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 433
    iget-object v5, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 435
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_2

    .line 437
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->isTemporary()Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v2, :cond_1

    .line 438
    iget-object v5, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    invoke-virtual {v5}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->asContentProvider()Landroid/content/ContentProvider;

    move-result-object v5

    aget-object v6, p2, v1

    invoke-virtual {v5, p1, v6}, Landroid/content/ContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    .line 443
    .local v3, "result":Landroid/net/Uri;
    :goto_1
    if-eqz v3, :cond_0

    .line 444
    add-int/lit8 v0, v0, 0x1

    .line 435
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 440
    .end local v3    # "result":Landroid/net/Uri;
    :cond_1
    aget-object v5, p2, v1

    invoke-virtual {p0, p1, v5}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->insertInternal(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    .line 441
    .restart local v3    # "result":Landroid/net/Uri;
    iget-object v5, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->yieldIfContended()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 449
    .end local v3    # "result":Landroid/net/Uri;
    :catchall_0
    move-exception v5

    iget-object v6, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v5

    .line 447
    :cond_2
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 449
    iget-object v5, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 451
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->isTemporary()Z

    move-result v5

    if-nez v5, :cond_3

    if-ne v0, v4, :cond_3

    .line 452
    const-string v5, "AbstractSyncableContentProvider"

    const-string v6, "notifyChange in bulkInsert"

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {p0, p1}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->changeRequiresLocalSync(Landroid/net/Uri;)Z

    move-result v7

    invoke-virtual {v5, p1, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 456
    :cond_3
    return v0
.end method

.method public changeRequiresLocalSync(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 555
    const-string v0, "AbstractSyncableContentProvider"

    const-string v1, "AbstractSyncableContentProvider changeRequiresLocalSync()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    const/4 v0, 0x1

    return v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V

    .line 125
    :cond_0
    return-void
.end method

.method public final delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "url"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 349
    const-string v3, "AbstractSyncableContentProvider"

    const-string v4, " delete()"

    invoke-static {v3, v4}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 351
    invoke-direct {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->applyingBatch()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v0, 0x1

    .line 352
    .local v0, "notApplyingBatch":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 353
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 356
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->isTemporary()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    invoke-virtual {v3, p1}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->matches(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 357
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    invoke-virtual {v3}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->asContentProvider()Landroid/content/ContentProvider;

    move-result-object v3

    invoke-virtual {v3, p1, p2, p3}, Landroid/content/ContentProvider;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 358
    .local v1, "numRows":I
    if-eqz v0, :cond_1

    .line 359
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 378
    :cond_1
    if-eqz v0, :cond_2

    .line 379
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .end local v1    # "numRows":I
    :cond_2
    :goto_1
    return v1

    .line 351
    .end local v0    # "notApplyingBatch":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 363
    .restart local v0    # "notApplyingBatch":Z
    :cond_4
    :try_start_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->deleteInternal(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 364
    .local v2, "result":I
    if-eqz v0, :cond_5

    .line 365
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 367
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->isTemporary()Z

    move-result v3

    if-nez v3, :cond_6

    if-lez v2, :cond_6

    .line 368
    if-eqz v0, :cond_8

    .line 369
    const-string v3, "AbstractSyncableContentProvider"

    const-string v4, "notifyChange in delete"

    invoke-static {v3, v4}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, p1}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->changeRequiresLocalSync(Landroid/net/Uri;)Z

    move-result v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 378
    :cond_6
    :goto_2
    if-eqz v0, :cond_7

    .line 379
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_7
    move v1, v2

    goto :goto_1

    .line 373
    :cond_8
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mPendingBatchNotifications:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 378
    .end local v2    # "result":I
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_9

    .line 379
    iget-object v4, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_9
    throw v3
.end method

.method protected abstract deleteInternal(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method protected deleteRowsForRemovedAccounts(Ljava/util/Map;Ljava/lang/String;)V
    .locals 20
    .param p2, "table"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 750
    .local p1, "accounts":Ljava/util/Map;, "Ljava/util/Map<Landroid/accounts/Account;Ljava/lang/Boolean;>;"
    const-string v3, "AbstractSyncableContentProvider"

    const-string v4, "AbstractSyncableContentProvider deleteRowsForRemovedAccounts()"

    invoke-static {v3, v4}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 751
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 752
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v4, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->sAccountProjection:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "_sync_account, _sync_account_type"

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 755
    .local v15, "c":Landroid/database/Cursor;
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 756
    const/4 v3, 0x0

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 757
    .local v12, "accountName":Ljava/lang/String;
    const/4 v3, 0x1

    invoke-interface {v15, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 758
    .local v13, "accountType":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 762
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string v4, "com.android.exchange"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v16

    .line 763
    .local v16, "exchangeAccounts":[Landroid/accounts/Account;
    const/16 v17, 0x0

    .line 764
    .local v17, "exchangeFound":Z
    move-object/from16 v14, v16

    .local v14, "arr$":[Landroid/accounts/Account;
    array-length v0, v14

    move/from16 v19, v0

    .local v19, "len$":I
    const/16 v18, 0x0

    .local v18, "i$":I
    :goto_1
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_3

    aget-object v10, v14, v18

    .line 765
    .local v10, "a":Landroid/accounts/Account;
    iget-object v3, v10, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 764
    :cond_1
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 768
    :cond_2
    iget-object v3, v10, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 769
    const/16 v17, 0x1

    .line 773
    .end local v10    # "a":Landroid/accounts/Account;
    :cond_3
    if-nez v17, :cond_0

    .line 777
    new-instance v11, Landroid/accounts/Account;

    invoke-direct {v11, v12, v13}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    .local v11, "account":Landroid/accounts/Account;
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 780
    const-string v3, "_sync_account=? AND _sync_account_type=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, v11, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, v11, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 790
    .end local v11    # "account":Landroid/accounts/Account;
    .end local v12    # "accountName":Ljava/lang/String;
    .end local v13    # "accountType":Ljava/lang/String;
    .end local v14    # "arr$":[Landroid/accounts/Account;
    .end local v16    # "exchangeAccounts":[Landroid/accounts/Account;
    .end local v17    # "exchangeFound":Z
    .end local v18    # "i$":I
    .end local v19    # "len$":I
    :catchall_0
    move-exception v3

    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_4
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 792
    return-void
.end method

.method public final endBatch(Z)V
    .locals 6
    .param p1, "successful"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 517
    const-string v2, "AbstractSyncableContentProvider"

    const-string v3, "endBatch()"

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    if-eqz p1, :cond_0

    .line 522
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 525
    :cond_0
    iget-object v2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mApplyingBatch:Ljava/lang/ThreadLocal;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 526
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 527
    iget-object v2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mPendingBatchNotifications:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 528
    .local v1, "url":Landroid/net/Uri;
    const-string v2, "AbstractSyncableContentProvider"

    const-string v3, "notifyChange in endBatch"

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {p0, v1}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->changeRequiresLocalSync(Landroid/net/Uri;)Z

    move-result v3

    invoke-virtual {v2, v1, v5, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    goto :goto_0

    .line 525
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "url":Landroid/net/Uri;
    :catchall_0
    move-exception v2

    move-object v3, v2

    iget-object v2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mApplyingBatch:Ljava/lang/ThreadLocal;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 526
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 527
    iget-object v2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mPendingBatchNotifications:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 528
    .restart local v1    # "url":Landroid/net/Uri;
    const-string v2, "AbstractSyncableContentProvider"

    const-string v4, "notifyChange in endBatch"

    invoke-static {v2, v4}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {p0, v1}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->changeRequiresLocalSync(Landroid/net/Uri;)Z

    move-result v4

    invoke-virtual {v2, v1, v5, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    goto :goto_1

    .line 531
    .end local v1    # "url":Landroid/net/Uri;
    :cond_1
    throw v3

    .line 533
    :cond_2
    return-void
.end method

.method public getContainsDiffs()Z
    .locals 2

    .prologue
    .line 275
    const-string v0, "AbstractSyncableContentProvider"

    const-string v1, "AbstractSyncableContentProvider getContainsDiffs()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    iget-boolean v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mContainsDiffs:Z

    return v0
.end method

.method public getDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 269
    const-string v0, "AbstractSyncableContentProvider"

    const-string v1, "AbstractSyncableContentProvider getDatabase()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method protected getMergers()Ljava/lang/Iterable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sec/android/providers/tasks/AbstractTableMerger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 302
    const-string v0, "AbstractSyncableContentProvider"

    const-string v1, "AbstractSyncableContentProvider getMergers()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected getSyncState()Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;
    .locals 1

    .prologue
    .line 839
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    return-object v0
.end method

.method public getSyncingAccount()Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 602
    const-string v0, "AbstractSyncableContentProvider"

    const-string v1, "AbstractSyncableContentProvider getSyncingAccount()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncingAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method public getTemporaryInstance()Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 241
    const-string v2, "AbstractSyncableContentProvider"

    const-string v3, "AbstractSyncableContentProvider getTemporaryInstance()"

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 255
    .local v1, "temp":Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;
    iput-boolean v4, v1, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mIsTemporary:Z

    .line 256
    invoke-virtual {v1, v4}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->setContainsDiffs(Z)V

    .line 257
    new-instance v2, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider$DatabaseHelper;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v2, v1, v5, v5}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider$DatabaseHelper;-><init>(Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v2, v1, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 258
    new-instance v2, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    iget-object v3, v1, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-direct {v2, v3}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;-><init>(Landroid/database/sqlite/SQLiteOpenHelper;)V

    iput-object v2, v1, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->isTemporary()Z

    move-result v2

    if-nez v2, :cond_0

    .line 260
    iget-object v2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getSyncingAccount()Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->copySyncState(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)V

    .line 265
    :cond_0
    return-object v1

    .line 245
    .end local v1    # "temp":Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;
    :catch_0
    move-exception v0

    .line 246
    .local v0, "e":Ljava/lang/InstantiationException;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "unable to instantiate class, this should never happen"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 248
    .end local v0    # "e":Ljava/lang/InstantiationException;
    :catch_1
    move-exception v0

    .line 249
    .local v0, "e":Ljava/lang/IllegalAccessException;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "IllegalAccess while instantiating class, this should never happen"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public bridge synthetic getTemporaryInstance()Lcom/sec/android/providers/tasks/SyncableContentProvider;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getTemporaryInstance()Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;

    move-result-object v0

    return-object v0
.end method

.method public final insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6
    .param p1, "url"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 391
    const-string v3, "AbstractSyncableContentProvider"

    const-string v4, "insert()"

    invoke-static {v3, v4}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 393
    invoke-direct {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->applyingBatch()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v0, 0x1

    .line 394
    .local v0, "notApplyingBatch":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 395
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 398
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->isTemporary()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    invoke-virtual {v3, p1}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->matches(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 399
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    invoke-virtual {v3}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->asContentProvider()Landroid/content/ContentProvider;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Landroid/content/ContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 400
    .local v1, "result":Landroid/net/Uri;
    if-eqz v0, :cond_1

    .line 401
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420
    :cond_1
    if-eqz v0, :cond_2

    .line 421
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_2
    move-object v2, v1

    .end local v1    # "result":Landroid/net/Uri;
    .local v2, "result":Landroid/net/Uri;
    :goto_1
    return-object v2

    .line 393
    .end local v0    # "notApplyingBatch":Z
    .end local v2    # "result":Landroid/net/Uri;
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 405
    .restart local v0    # "notApplyingBatch":Z
    :cond_4
    :try_start_1
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->insertInternal(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 406
    .restart local v1    # "result":Landroid/net/Uri;
    if-eqz v0, :cond_5

    .line 407
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 409
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->isTemporary()Z

    move-result v3

    if-nez v3, :cond_6

    if-eqz v1, :cond_6

    .line 410
    if-eqz v0, :cond_8

    .line 411
    const-string v3, "AbstractSyncableContentProvider"

    const-string v4, "notifyChange in insert"

    invoke-static {v3, v4}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, p1}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->changeRequiresLocalSync(Landroid/net/Uri;)Z

    move-result v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 420
    :cond_6
    :goto_2
    if-eqz v0, :cond_7

    .line 421
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_7
    move-object v2, v1

    .end local v1    # "result":Landroid/net/Uri;
    .restart local v2    # "result":Landroid/net/Uri;
    goto :goto_1

    .line 415
    .end local v2    # "result":Landroid/net/Uri;
    .restart local v1    # "result":Landroid/net/Uri;
    :cond_8
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mPendingBatchNotifications:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 420
    .end local v1    # "result":Landroid/net/Uri;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_9

    .line 421
    iget-object v4, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_9
    throw v3
.end method

.method protected abstract insertInternal(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
.end method

.method public isMergeCancelled()Z
    .locals 2

    .prologue
    .line 669
    const-string v0, "AbstractSyncableContentProvider"

    const-string v1, "AbstractSyncableContentProvider isMergeCancelled()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    iget-boolean v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mIsMergeCancelled:Z

    return v0
.end method

.method protected isTemporary()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mIsTemporary:Z

    return v0
.end method

.method public merge(Landroid/content/SyncContext;Lcom/sec/android/providers/tasks/SyncableContentProvider;Lcom/sec/android/providers/tasks/TempProviderSyncResult;Landroid/content/SyncResult;)V
    .locals 10
    .param p1, "context"    # Landroid/content/SyncContext;
    .param p2, "diffs"    # Lcom/sec/android/providers/tasks/SyncableContentProvider;
    .param p3, "result"    # Lcom/sec/android/providers/tasks/TempProviderSyncResult;
    .param p4, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 618
    const-string v1, "AbstractSyncableContentProvider"

    const-string v2, "AbstractSyncableContentProvider merge()"

    invoke-static {v1, v2}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    iget-object v1, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 620
    .local v7, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 622
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 623
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mIsMergeCancelled:Z

    .line 624
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 625
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getMergers()Ljava/lang/Iterable;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v9

    .line 627
    .local v9, "mergers":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lcom/sec/android/providers/tasks/AbstractTableMerger;>;"
    :try_start_3
    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/providers/tasks/AbstractTableMerger;

    .line 628
    .local v0, "merger":Lcom/sec/android/providers/tasks/AbstractTableMerger;
    monitor-enter p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 629
    :try_start_4
    iget-boolean v1, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mIsMergeCancelled:Z

    if-eqz v1, :cond_1

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 634
    .end local v0    # "merger":Lcom/sec/android/providers/tasks/AbstractTableMerger;
    :cond_0
    :try_start_5
    iget-boolean v1, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mIsMergeCancelled:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    if-eqz v1, :cond_2

    .line 642
    :try_start_6
    monitor-enter p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 643
    const/4 v1, 0x0

    :try_start_7
    iput-object v1, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mCurrentMerger:Lcom/sec/android/providers/tasks/AbstractTableMerger;

    .line 644
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 648
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 650
    .end local p2    # "diffs":Lcom/sec/android/providers/tasks/SyncableContentProvider;
    :goto_1
    return-void

    .line 624
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "mergers":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lcom/sec/android/providers/tasks/AbstractTableMerger;>;"
    .restart local p2    # "diffs":Lcom/sec/android/providers/tasks/SyncableContentProvider;
    :catchall_0
    move-exception v1

    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    throw v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 648
    .end local p2    # "diffs":Lcom/sec/android/providers/tasks/SyncableContentProvider;
    :catchall_1
    move-exception v1

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    .line 630
    .restart local v0    # "merger":Lcom/sec/android/providers/tasks/AbstractTableMerger;
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v9    # "mergers":Ljava/lang/Iterable;, "Ljava/lang/Iterable<+Lcom/sec/android/providers/tasks/AbstractTableMerger;>;"
    .restart local p2    # "diffs":Lcom/sec/android/providers/tasks/SyncableContentProvider;
    :cond_1
    :try_start_a
    iput-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mCurrentMerger:Lcom/sec/android/providers/tasks/AbstractTableMerger;

    .line 631
    monitor-exit p0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 632
    :try_start_b
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getSyncingAccount()Landroid/accounts/Account;

    move-result-object v2

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/providers/tasks/AbstractTableMerger;->merge(Landroid/content/SyncContext;Landroid/accounts/Account;Lcom/sec/android/providers/tasks/SyncableContentProvider;Lcom/sec/android/providers/tasks/TempProviderSyncResult;Landroid/content/SyncResult;Lcom/sec/android/providers/tasks/SyncableContentProvider;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto :goto_0

    .line 642
    .end local v0    # "merger":Lcom/sec/android/providers/tasks/AbstractTableMerger;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local p2    # "diffs":Lcom/sec/android/providers/tasks/SyncableContentProvider;
    :catchall_2
    move-exception v1

    :try_start_c
    monitor-enter p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 643
    const/4 v2, 0x0

    :try_start_d
    iput-object v2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mCurrentMerger:Lcom/sec/android/providers/tasks/AbstractTableMerger;

    .line 644
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    :try_start_e
    throw v1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 631
    .restart local v0    # "merger":Lcom/sec/android/providers/tasks/AbstractTableMerger;
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local p2    # "diffs":Lcom/sec/android/providers/tasks/SyncableContentProvider;
    :catchall_3
    move-exception v1

    :try_start_f
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    :try_start_10
    throw v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    .line 644
    .end local v0    # "merger":Lcom/sec/android/providers/tasks/AbstractTableMerger;
    :catchall_4
    move-exception v1

    :try_start_11
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    :try_start_12
    throw v1
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 635
    :cond_2
    if-eqz p2, :cond_3

    .line 636
    :try_start_13
    iget-object v1, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    check-cast p2, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;

    .end local p2    # "diffs":Lcom/sec/android/providers/tasks/SyncableContentProvider;
    iget-object v2, p2, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getSyncingAccount()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->copySyncState(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    .line 642
    :cond_3
    :try_start_14
    monitor-enter p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    .line 643
    const/4 v1, 0x0

    :try_start_15
    iput-object v1, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mCurrentMerger:Lcom/sec/android/providers/tasks/AbstractTableMerger;

    .line 644
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_5

    .line 646
    :try_start_16
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    .line 648
    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_1

    .line 644
    :catchall_5
    move-exception v1

    :try_start_17
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_5

    :try_start_18
    throw v1
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    .end local v8    # "i$":Ljava/util/Iterator;
    :catchall_6
    move-exception v1

    :try_start_19
    monitor-exit p0
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_6

    :try_start_1a
    throw v1
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1
.end method

.method protected onAccountsChanged([Landroid/accounts/Account;)V
    .locals 11
    .param p1, "accountsArray"    # [Landroid/accounts/Account;

    .prologue
    .line 717
    const-string v9, "AbstractSyncableContentProvider"

    const-string v10, "AbstractSyncableContentProvider onAccountsChanged()"

    invoke-static {v9, v10}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    invoke-static {}, Lcom/google/android/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v1

    .line 719
    .local v1, "accounts":Ljava/util/Map;, "Ljava/util/Map<Landroid/accounts/Account;Ljava/lang/Boolean;>;"
    move-object v2, p1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v0, v2, v4

    .line 720
    .local v0, "account":Landroid/accounts/Account;
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-interface {v1, v0, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 719
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 723
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    iget-object v9, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 724
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->getSyncedTables()Ljava/util/Map;

    move-result-object v7

    .line 725
    .local v7, "tableMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v8, Ljava/util/Vector;

    invoke-direct {v8}, Ljava/util/Vector;-><init>()V

    .line 726
    .local v8, "tables":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/String;>;"
    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    .line 727
    invoke-interface {v7}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    .line 729
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 731
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    iget-object v10, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mAccounts:[Landroid/accounts/Account;

    invoke-virtual {v9, v10}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->onAccountsChanged([Landroid/accounts/Account;)V

    .line 732
    invoke-virtual {v8}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 733
    .local v6, "table":Ljava/lang/String;
    invoke-virtual {p0, v1, v6}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->deleteRowsForRemovedAccounts(Ljava/util/Map;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 737
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "table":Ljava/lang/String;
    :catchall_0
    move-exception v9

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v9

    .line 735
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 737
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 739
    return-void
.end method

.method public onCreate()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 195
    const-string v0, "AbstractSyncableContentProvider"

    const-string v1, "AbstractSyncableContentProvider onCreate()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->isTemporary()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onCreate() called for temp provider"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_0
    new-instance v0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider$DatabaseHelper;

    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDatabaseName:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider$DatabaseHelper;-><init>(Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 199
    new-instance v0, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    iget-object v1, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-direct {v0, v1}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;-><init>(Landroid/database/sqlite/SQLiteOpenHelper;)V

    iput-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    new-instance v1, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider$1;

    invoke-direct {v1, p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider$1;-><init>(Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 230
    return v3
.end method

.method protected onDatabaseOpened(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 156
    return-void
.end method

.method public onSyncCanceled()V
    .locals 2

    .prologue
    .line 658
    const-string v0, "AbstractSyncableContentProvider"

    const-string v1, "AbstractSyncableContentProvider onSyncCanceled()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    monitor-enter p0

    .line 660
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mIsMergeCancelled:Z

    .line 661
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mCurrentMerger:Lcom/sec/android/providers/tasks/AbstractTableMerger;

    if-eqz v0, :cond_0

    .line 662
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mCurrentMerger:Lcom/sec/android/providers/tasks/AbstractTableMerger;

    invoke-virtual {v0}, Lcom/sec/android/providers/tasks/AbstractTableMerger;->onMergeCancelled()V

    .line 664
    :cond_0
    monitor-exit p0

    .line 665
    return-void

    .line 664
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onSyncStart(Landroid/content/SyncContext;Landroid/accounts/Account;)V
    .locals 2
    .param p1, "context"    # Landroid/content/SyncContext;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 581
    const-string v0, "AbstractSyncableContentProvider"

    const-string v1, "AbstractSyncableContentProvider onSyncStart()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    if-nez p2, :cond_0

    .line 583
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "you passed in an empty account"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 585
    :cond_0
    iput-object p2, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncingAccount:Landroid/accounts/Account;

    .line 586
    return-void
.end method

.method public onSyncStop(Landroid/content/SyncContext;Z)V
    .locals 0
    .param p1, "context"    # Landroid/content/SyncContext;
    .param p2, "success"    # Z

    .prologue
    .line 595
    return-void
.end method

.method public final query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "url"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 563
    const-string v0, "AbstractSyncableContentProvider"

    const-string v1, "AbstractSyncableContentProvider query()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    const-string v0, "SANDEEP"

    const-string v1, "In query method"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 566
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->isTemporary()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->matches(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 567
    const-string v0, "SANDEEP"

    const-string v1, "In sync states"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    invoke-virtual {v0}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->asContentProvider()Landroid/content/ContentProvider;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 571
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual/range {p0 .. p5}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->queryInternal(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract queryInternal(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
.end method

.method public readSyncDataBytes(Landroid/accounts/Account;)[B
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 827
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    iget-object v1, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->readSyncDataBytes(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)[B

    move-result-object v0

    return-object v0
.end method

.method public setContainsDiffs(Z)V
    .locals 2
    .param p1, "containsDiffs"    # Z

    .prologue
    .line 280
    const-string v0, "AbstractSyncableContentProvider"

    const-string v1, "AbstractSyncableContentProvider setContainsDiffs()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->isTemporary()Z

    move-result v0

    if-nez v0, :cond_0

    .line 282
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "only a temporary provider can contain diffs"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 285
    :cond_0
    iput-boolean p1, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mContainsDiffs:Z

    .line 286
    return-void
.end method

.method public final update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "url"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 309
    const-string v3, "AbstractSyncableContentProvider"

    const-string v4, "update()"

    invoke-static {v3, v4}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 311
    invoke-direct {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->applyingBatch()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v0, 0x1

    .line 312
    .local v0, "notApplyingBatch":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 313
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 316
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->isTemporary()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    invoke-virtual {v3, p1}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->matches(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 317
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    invoke-virtual {v3}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->asContentProvider()Landroid/content/ContentProvider;

    move-result-object v3

    invoke-virtual {v3, p1, p2, p3, p4}, Landroid/content/ContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 319
    .local v1, "numRows":I
    if-eqz v0, :cond_1

    .line 320
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 340
    :cond_1
    if-eqz v0, :cond_2

    .line 341
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .end local v1    # "numRows":I
    :cond_2
    :goto_1
    return v1

    .line 311
    .end local v0    # "notApplyingBatch":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 325
    .restart local v0    # "notApplyingBatch":Z
    :cond_4
    :try_start_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->updateInternal(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 326
    .local v2, "result":I
    if-eqz v0, :cond_5

    .line 327
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 329
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->isTemporary()Z

    move-result v3

    if-nez v3, :cond_6

    if-lez v2, :cond_6

    .line 330
    if-eqz v0, :cond_8

    .line 331
    const-string v3, "AbstractSyncableContentProvider"

    const-string v4, "notifyChange in update"

    invoke-static {v3, v4}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, p1}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->changeRequiresLocalSync(Landroid/net/Uri;)Z

    move-result v5

    invoke-virtual {v3, p1, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 340
    :cond_6
    :goto_2
    if-eqz v0, :cond_7

    .line 341
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_7
    move v1, v2

    goto :goto_1

    .line 335
    :cond_8
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mPendingBatchNotifications:Ljava/lang/ThreadLocal;

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Set;

    invoke-interface {v3, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 340
    .end local v2    # "result":I
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_9

    .line 341
    iget-object v4, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_9
    throw v3
.end method

.method protected abstract updateInternal(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method protected abstract upgradeDatabase(Landroid/database/sqlite/SQLiteDatabase;II)Z
.end method

.method public wipeAccount(Landroid/accounts/Account;)V
    .locals 9
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 799
    const-string v5, "AbstractSyncableContentProvider"

    const-string v6, "AbstractSyncableContentProvider wipeAccount()"

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 800
    iget-object v5, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 801
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->getSyncedTables()Ljava/util/Map;

    move-result-object v3

    .line 802
    .local v3, "tableMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 803
    .local v4, "tables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 804
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 806
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 810
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    invoke-virtual {v5, v0, p1}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->discardSyncData(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;)V

    .line 813
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 814
    .local v2, "table":Ljava/lang/String;
    const-string v5, "_sync_account=? AND _sync_account_type=?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v0, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 819
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "table":Ljava/lang/String;
    :catchall_0
    move-exception v5

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v5

    .line 817
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 819
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 821
    return-void
.end method

.method public writeSyncDataBytes(Landroid/accounts/Account;[B)V
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "data"    # [B

    .prologue
    .line 834
    iget-object v0, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mSyncState:Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    iget-object v1, p0, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->writeSyncDataBytes(Landroid/database/sqlite/SQLiteDatabase;Landroid/accounts/Account;[B)V

    .line 835
    return-void
.end method
