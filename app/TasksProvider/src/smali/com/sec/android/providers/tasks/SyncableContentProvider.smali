.class public abstract Lcom/sec/android/providers/tasks/SyncableContentProvider;
.super Landroid/content/ContentProvider;
.source "SyncableContentProvider.java"


# instance fields
.field private volatile mTempProviderSyncAdapter:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract bootstrapDatabase(Landroid/database/sqlite/SQLiteDatabase;)V
.end method

.method public abstract changeRequiresLocalSync(Landroid/net/Uri;)Z
.end method

.method public abstract close()V
.end method

.method protected abstract deleteInternal(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method protected abstract deleteRowsForRemovedAccounts(Ljava/util/Map;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract getContainsDiffs()Z
.end method

.method public abstract getDatabase()Landroid/database/sqlite/SQLiteDatabase;
.end method

.method protected abstract getMergers()Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sec/android/providers/tasks/AbstractTableMerger;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSyncingAccount()Landroid/accounts/Account;
.end method

.method public getTempProviderSyncAdapter()Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/providers/tasks/SyncableContentProvider;->mTempProviderSyncAdapter:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    return-object v0
.end method

.method public abstract getTemporaryInstance()Lcom/sec/android/providers/tasks/SyncableContentProvider;
.end method

.method protected abstract insertInternal(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
.end method

.method public abstract isMergeCancelled()Z
.end method

.method protected abstract isTemporary()Z
.end method

.method public abstract merge(Landroid/content/SyncContext;Lcom/sec/android/providers/tasks/SyncableContentProvider;Lcom/sec/android/providers/tasks/TempProviderSyncResult;Landroid/content/SyncResult;)V
.end method

.method protected abstract onAccountsChanged([Landroid/accounts/Account;)V
.end method

.method protected abstract onDatabaseOpened(Landroid/database/sqlite/SQLiteDatabase;)V
.end method

.method public abstract onSyncCanceled()V
.end method

.method public abstract onSyncStart(Landroid/content/SyncContext;Landroid/accounts/Account;)V
.end method

.method public abstract onSyncStop(Landroid/content/SyncContext;Z)V
.end method

.method protected abstract queryInternal(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
.end method

.method public abstract readSyncDataBytes(Landroid/accounts/Account;)[B
.end method

.method public abstract setContainsDiffs(Z)V
.end method

.method public setTempProviderSyncAdapter(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)V
    .locals 0
    .param p1, "syncAdapter"    # Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/providers/tasks/SyncableContentProvider;->mTempProviderSyncAdapter:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    .line 47
    return-void
.end method

.method protected abstract updateInternal(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method protected abstract upgradeDatabase(Landroid/database/sqlite/SQLiteDatabase;II)Z
.end method

.method public abstract wipeAccount(Landroid/accounts/Account;)V
.end method

.method public abstract writeSyncDataBytes(Landroid/accounts/Account;[B)V
.end method
