.class Lcom/sec/android/providers/tasks/TasksProvider$TimezoneCheckerThread;
.super Ljava/lang/Thread;
.source "TasksProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/providers/tasks/TasksProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TimezoneCheckerThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/providers/tasks/TasksProvider;


# direct methods
.method private constructor <init>(Lcom/sec/android/providers/tasks/TasksProvider;)V
    .locals 0

    .prologue
    .line 224
    iput-object p1, p0, Lcom/sec/android/providers/tasks/TasksProvider$TimezoneCheckerThread;->this$0:Lcom/sec/android/providers/tasks/TasksProvider;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/providers/tasks/TasksProvider;Lcom/sec/android/providers/tasks/TasksProvider$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/providers/tasks/TasksProvider;
    .param p2, "x1"    # Lcom/sec/android/providers/tasks/TasksProvider$1;

    .prologue
    .line 224
    invoke-direct {p0, p1}, Lcom/sec/android/providers/tasks/TasksProvider$TimezoneCheckerThread;-><init>(Lcom/sec/android/providers/tasks/TasksProvider;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 227
    const/16 v1, 0xa

    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    .line 229
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/providers/tasks/TasksProvider$TimezoneCheckerThread;->this$0:Lcom/sec/android/providers/tasks/TasksProvider;

    # invokes: Lcom/sec/android/providers/tasks/TasksProvider;->doUpdateTimezoneDependentFields()V
    invoke-static {v1}, Lcom/sec/android/providers/tasks/TasksProvider;->access$100(Lcom/sec/android/providers/tasks/TasksProvider;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    :goto_0
    return-void

    .line 230
    :catch_0
    move-exception v0

    .line 231
    .local v0, "e":Landroid/database/SQLException;
    const-string v1, "TasksProvider"

    const-string v2, "doUpdateTimezoneDependentFields() failed"

    invoke-static {v1, v2, v0}, Lcom/sec/android/providers/tasks/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
