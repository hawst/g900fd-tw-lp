.class Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;
.super Ljava/lang/Thread;
.source "TempProviderSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncThread"
.end annotation


# instance fields
.field private final mAccount:Landroid/accounts/Account;

.field private final mAuthority:Ljava/lang/String;

.field private final mExtras:Landroid/os/Bundle;

.field private mInitialRxBytes:J

.field private mInitialTxBytes:J

.field private volatile mIsCanceled:Z

.field private final mResult:Landroid/content/SyncResult;

.field private final mSyncContext:Landroid/content/SyncContext;

.field final synthetic this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Landroid/content/SyncContext;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p2, "syncContext"    # Landroid/content/SyncContext;
    .param p3, "account"    # Landroid/accounts/Account;
    .param p4, "authority"    # Ljava/lang/String;
    .param p5, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 216
    iput-object p1, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    .line 217
    const-string v0, "SyncThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 211
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mIsCanceled:Z

    .line 218
    iput-object p3, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mAccount:Landroid/accounts/Account;

    .line 219
    iput-object p4, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mAuthority:Ljava/lang/String;

    .line 220
    iput-object p5, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mExtras:Landroid/os/Bundle;

    .line 221
    iput-object p2, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mSyncContext:Landroid/content/SyncContext;

    .line 222
    new-instance v0, Landroid/content/SyncResult;

    invoke-direct {v0}, Landroid/content/SyncResult;-><init>()V

    iput-object v0, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    .line 223
    return-void
.end method

.method private runSyncLoop(Landroid/content/SyncContext;Landroid/accounts/Account;Landroid/os/Bundle;)V
    .locals 28
    .param p1, "syncContext"    # Landroid/content/SyncContext;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 336
    new-instance v25, Landroid/util/TimingLogger;

    const-string v2, "SyncProfiling"

    const-string v3, "sync"

    move-object/from16 v0, v25

    invoke-direct {v0, v2, v3}, Landroid/util/TimingLogger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    .local v25, "syncTimer":Landroid/util/TimingLogger;
    const-string v2, "start"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 338
    const/4 v14, 0x0

    .line 339
    .local v14, "loopCount":I
    const/16 v26, 0x0

    .line 341
    .local v26, "tooManyGetServerDiffsAttempts":Z
    const-string v2, "deletions_override"

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v20

    .line 344
    .local v20, "overrideTooManyDeletions":Z
    const-string v2, "discard_deletions"

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    .line 346
    .local v13, "discardLocalDeletions":Z
    const-string v2, "upload"

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v27

    .line 348
    .local v27, "uploadOnly":Z
    const/4 v5, 0x0

    .line 349
    .local v5, "serverDiffs":Lcom/sec/android/providers/tasks/SyncableContentProvider;
    new-instance v24, Lcom/sec/android/providers/tasks/TempProviderSyncResult;

    invoke-direct/range {v24 .. v24}, Lcom/sec/android/providers/tasks/TempProviderSyncResult;-><init>()V

    .line 351
    .local v24, "result":Lcom/sec/android/providers/tasks/TempProviderSyncResult;
    if-nez v27, :cond_0

    move v15, v14

    .line 360
    .end local v14    # "loopCount":I
    .local v15, "loopCount":I
    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mIsCanceled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v2, :cond_30

    .line 362
    add-int/lit8 v14, v15, 0x1

    .end local v15    # "loopCount":I
    .restart local v14    # "loopCount":I
    const/16 v2, 0x14

    if-ne v15, v2, :cond_7

    .line 363
    :try_start_1
    const-string v2, "Sync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "runSyncLoop: Hit max loop count while getting server diffs "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    const/16 v26, 0x1

    .line 455
    :cond_0
    :goto_1
    const/4 v14, 0x0

    .line 456
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    invoke-virtual {v2}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->isReadOnly()Z

    move-result v21

    .line 457
    .local v21, "readOnly":Z
    const-wide/16 v22, 0x0

    .line 458
    .local v22, "previousNumModifications":J
    if-eqz v5, :cond_1

    .line 459
    invoke-virtual {v5}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 460
    const/4 v5, 0x0

    .line 466
    :cond_1
    if-eqz v13, :cond_2

    .line 467
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;
    invoke-static {v2}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$200(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->getTemporaryInstance()Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v5

    .line 468
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    invoke-virtual {v2, v5}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->initTempProvider(Lcom/sec/android/providers/tasks/SyncableContentProvider;)V

    .line 469
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v5}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->writeSyncData(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;Lcom/sec/android/providers/tasks/SyncableContentProvider;)V

    .line 472
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mIsCanceled:Z

    if-nez v2, :cond_23

    .line 476
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    if-eqz v2, :cond_3

    .line 477
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    invoke-virtual {v2}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 478
    const/4 v2, 0x0

    move-object/from16 v0, v24

    iput-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    .line 480
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    invoke-virtual {v2}, Landroid/content/SyncResult;->clear()V

    .line 481
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;
    invoke-static {v2}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$200(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v3

    if-eqz v21, :cond_20

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v5, v2, v6}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->merge(Landroid/content/SyncContext;Lcom/sec/android/providers/tasks/SyncableContentProvider;Lcom/sec/android/providers/tasks/TempProviderSyncResult;Landroid/content/SyncResult;)V

    .line 483
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mIsCanceled:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_21

    .line 556
    if-eqz v13, :cond_4

    .line 557
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/content/SyncResult;->fullSyncRequested:Z

    .line 559
    :cond_4
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 560
    :cond_5
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    if-eqz v2, :cond_6

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    invoke-virtual {v2}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 561
    :cond_6
    const-string v2, "stop"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 562
    invoke-virtual/range {v25 .. v25}, Landroid/util/TimingLogger;->dumpToLog()V

    .line 564
    .end local v21    # "readOnly":Z
    .end local v22    # "previousNumModifications":J
    :goto_4
    return-void

    .line 374
    :cond_7
    if-eqz v5, :cond_8

    :try_start_2
    invoke-virtual {v5}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 375
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;
    invoke-static {v2}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$200(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->getTemporaryInstance()Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v5

    .line 378
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    invoke-virtual {v2, v5}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->initTempProvider(Lcom/sec/android/providers/tasks/SyncableContentProvider;)V

    .line 379
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    invoke-virtual {v2}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->createSyncInfo()Ljava/lang/Object;

    move-result-object v7

    .line 380
    .local v7, "syncInfo":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    invoke-virtual {v2, v5}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->readSyncData(Lcom/sec/android/providers/tasks/SyncableContentProvider;)Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;

    move-result-object v4

    .line 383
    .local v4, "syncData":Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;
    if-nez v4, :cond_9

    .line 384
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;
    invoke-static {v2}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$200(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->wipeAccount(Landroid/accounts/Account;)V

    .line 385
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    invoke-virtual {v2}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->newSyncData()Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;

    move-result-object v4

    .line 387
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    invoke-virtual {v2}, Landroid/content/SyncResult;->clear()V

    .line 388
    const-string v2, "Sync"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 389
    const-string v2, "Sync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "runSyncLoop: running getServerDiffs using syncData "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    move-object/from16 v3, p1

    move-object/from16 v6, p3

    invoke-virtual/range {v2 .. v8}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->getServerDiffs(Landroid/content/SyncContext;Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;Lcom/sec/android/providers/tasks/SyncableContentProvider;Landroid/os/Bundle;Ljava/lang/Object;Landroid/content/SyncResult;)V

    .line 395
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mIsCanceled:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_e

    .line 556
    if-eqz v13, :cond_b

    .line 557
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/content/SyncResult;->fullSyncRequested:Z

    .line 559
    :cond_b
    if-eqz v5, :cond_c

    invoke-virtual {v5}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 560
    :cond_c
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    if-eqz v2, :cond_d

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    invoke-virtual {v2}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 561
    :cond_d
    const-string v2, "stop"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 562
    invoke-virtual/range {v25 .. v25}, Landroid/util/TimingLogger;->dumpToLog()V

    goto/16 :goto_4

    .line 396
    :cond_e
    :try_start_3
    const-string v2, "Sync"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 397
    const-string v2, "Sync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "runSyncLoop: result: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    invoke-virtual {v2}, Landroid/content/SyncResult;->hasError()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v2

    if-eqz v2, :cond_13

    .line 556
    if-eqz v13, :cond_10

    .line 557
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/content/SyncResult;->fullSyncRequested:Z

    .line 559
    :cond_10
    if-eqz v5, :cond_11

    invoke-virtual {v5}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 560
    :cond_11
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    if-eqz v2, :cond_12

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    invoke-virtual {v2}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 561
    :cond_12
    const-string v2, "stop"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 562
    invoke-virtual/range {v25 .. v25}, Landroid/util/TimingLogger;->dumpToLog()V

    goto/16 :goto_4

    .line 400
    :cond_13
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    iget-boolean v2, v2, Landroid/content/SyncResult;->partialSyncUnavailable:Z

    if-eqz v2, :cond_14

    .line 402
    const-string v2, "Sync"

    const-string v3, "partialSyncUnavailable is set, setting ignoreSyncData and retrying"

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;
    invoke-static {v2}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$200(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->wipeAccount(Landroid/accounts/Account;)V

    move v15, v14

    .line 406
    .end local v14    # "loopCount":I
    .restart local v15    # "loopCount":I
    goto/16 :goto_0

    .line 410
    .end local v15    # "loopCount":I
    .restart local v14    # "loopCount":I
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->writeSyncData(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;Lcom/sec/android/providers/tasks/SyncableContentProvider;)V

    .line 413
    const-string v2, "Sync"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 414
    const-string v2, "Sync"

    const-string v3, "runSyncLoop: running merge"

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;
    invoke-static {v2}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$200(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v5, v3, v6}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->merge(Landroid/content/SyncContext;Lcom/sec/android/providers/tasks/SyncableContentProvider;Lcom/sec/android/providers/tasks/TempProviderSyncResult;Landroid/content/SyncResult;)V

    .line 418
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mIsCanceled:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v2, :cond_19

    .line 556
    if-eqz v13, :cond_16

    .line 557
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/content/SyncResult;->fullSyncRequested:Z

    .line 559
    :cond_16
    if-eqz v5, :cond_17

    invoke-virtual {v5}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 560
    :cond_17
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    if-eqz v2, :cond_18

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    invoke-virtual {v2}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 561
    :cond_18
    const-string v2, "stop"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 562
    invoke-virtual/range {v25 .. v25}, Landroid/util/TimingLogger;->dumpToLog()V

    goto/16 :goto_4

    .line 419
    :cond_19
    :try_start_5
    const-string v2, "Sync"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 420
    const-string v2, "Sync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "runSyncLoop: result: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    iget-boolean v2, v2, Landroid/content/SyncResult;->moreRecordsToGet:Z

    if-nez v2, :cond_1e

    .line 425
    const-string v2, "Sync"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 426
    const-string v2, "Sync"

    const-string v3, "runSyncLoop: fetched all data, moving on"

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 556
    .end local v4    # "syncData":Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;
    .end local v7    # "syncInfo":Ljava/lang/Object;
    :catchall_0
    move-exception v2

    :goto_5
    if-eqz v13, :cond_1b

    .line 557
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    const/4 v6, 0x1

    iput-boolean v6, v3, Landroid/content/SyncResult;->fullSyncRequested:Z

    .line 559
    :cond_1b
    if-eqz v5, :cond_1c

    invoke-virtual {v5}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 560
    :cond_1c
    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    if-eqz v3, :cond_1d

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    invoke-virtual {v3}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 561
    :cond_1d
    const-string v3, "stop"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 562
    invoke-virtual/range {v25 .. v25}, Landroid/util/TimingLogger;->dumpToLog()V

    throw v2

    .line 430
    .restart local v4    # "syncData":Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;
    .restart local v7    # "syncInfo":Ljava/lang/Object;
    :cond_1e
    :try_start_6
    const-string v2, "Sync"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 431
    const-string v2, "Sync"

    const-string v3, "runSyncLoop: more data to fetch, looping"

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1f
    move v15, v14

    .line 433
    .end local v14    # "loopCount":I
    .restart local v15    # "loopCount":I
    goto/16 :goto_0

    .end local v4    # "syncData":Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;
    .end local v7    # "syncInfo":Ljava/lang/Object;
    .end local v15    # "loopCount":I
    .restart local v14    # "loopCount":I
    .restart local v21    # "readOnly":Z
    .restart local v22    # "previousNumModifications":J
    :cond_20
    move-object/from16 v2, v24

    .line 481
    goto/16 :goto_3

    .line 484
    :cond_21
    const-string v2, "Sync"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 485
    const-string v2, "Sync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "runSyncLoop: result: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    :cond_22
    if-eqz v21, :cond_28

    const/4 v10, 0x0

    .line 490
    .local v10, "clientDiffs":Lcom/sec/android/providers/tasks/SyncableContentProvider;
    :goto_6
    if-nez v10, :cond_29

    .line 550
    .end local v10    # "clientDiffs":Lcom/sec/android/providers/tasks/SyncableContentProvider;
    :cond_23
    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    iget-boolean v3, v2, Landroid/content/SyncResult;->tooManyRetries:Z

    or-int v3, v3, v26

    iput-boolean v3, v2, Landroid/content/SyncResult;->tooManyRetries:Z

    .line 551
    const-string v2, "Sync"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 552
    const-string v2, "Sync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "runSyncLoop: final result: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 556
    :cond_24
    if-eqz v13, :cond_25

    .line 557
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/content/SyncResult;->fullSyncRequested:Z

    .line 559
    :cond_25
    if-eqz v5, :cond_26

    invoke-virtual {v5}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 560
    :cond_26
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    if-eqz v2, :cond_27

    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    invoke-virtual {v2}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 561
    :cond_27
    const-string v2, "stop"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 562
    invoke-virtual/range {v25 .. v25}, Landroid/util/TimingLogger;->dumpToLog()V

    goto/16 :goto_4

    .line 488
    :cond_28
    :try_start_7
    move-object/from16 v0, v24

    iget-object v10, v0, Lcom/sec/android/providers/tasks/TempProviderSyncResult;->tempContentProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    goto :goto_6

    .line 496
    .restart local v10    # "clientDiffs":Lcom/sec/android/providers/tasks/SyncableContentProvider;
    :cond_29
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v2, Landroid/content/SyncStats;->numUpdates:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    iget-object v6, v6, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v6, Landroid/content/SyncStats;->numDeletes:J

    add-long/2addr v2, v8

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    iget-object v6, v6, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v6, Landroid/content/SyncStats;->numInserts:J

    add-long v18, v2, v8

    .line 501
    .local v18, "numModifications":J
    cmp-long v2, v18, v22

    if-gez v2, :cond_2f

    .line 502
    const/4 v14, 0x0

    move v15, v14

    .line 504
    .end local v14    # "loopCount":I
    .restart local v15    # "loopCount":I
    :goto_8
    move-wide/from16 v22, v18

    .line 507
    add-int/lit8 v14, v15, 0x1

    .end local v15    # "loopCount":I
    .restart local v14    # "loopCount":I
    const/16 v2, 0xa

    if-lt v15, v2, :cond_2a

    .line 508
    const-string v2, "Sync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "runSyncLoop: Hit max loop count while syncing "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/content/SyncResult;->tooManyRetries:Z

    goto/16 :goto_7

    .line 514
    :cond_2a
    if-nez v20, :cond_2b

    if-nez v13, :cond_2b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    iget-object v3, v3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    invoke-virtual {v2, v3}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->hasTooManyDeletions(Landroid/content/SyncStats;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 517
    const-string v2, "Sync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "runSyncLoop: Too many deletions were found in provider "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", not doing any more updates"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v0, v2, Landroid/content/SyncStats;->numDeletes:J

    move-wide/from16 v16, v0

    .line 521
    .local v16, "numDeletes":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    invoke-virtual {v2}, Landroid/content/SyncStats;->clear()V

    .line 522
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/content/SyncResult;->tooManyDeletions:Z

    .line 523
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    move-wide/from16 v0, v16

    iput-wide v0, v2, Landroid/content/SyncStats;->numDeletes:J

    goto/16 :goto_7

    .line 528
    .end local v16    # "numDeletes":J
    :cond_2b
    if-eqz v5, :cond_2c

    invoke-virtual {v5}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->close()V

    .line 529
    :cond_2c
    invoke-virtual {v10}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->getTemporaryInstance()Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v5

    .line 530
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    invoke-virtual {v2, v5}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->initTempProvider(Lcom/sec/android/providers/tasks/SyncableContentProvider;)V

    .line 531
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    invoke-virtual {v2}, Landroid/content/SyncResult;->clear()V

    .line 532
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    move-object/from16 v9, p1

    move-object v11, v5

    invoke-virtual/range {v8 .. v13}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->sendClientDiffs(Landroid/content/SyncContext;Lcom/sec/android/providers/tasks/SyncableContentProvider;Lcom/sec/android/providers/tasks/SyncableContentProvider;Landroid/content/SyncResult;Z)V

    .line 534
    const-string v2, "Sync"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 535
    const-string v2, "Sync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "runSyncLoop: result: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    :cond_2d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    invoke-virtual {v2}, Landroid/content/SyncResult;->madeSomeProgress()Z

    move-result v2

    if-nez v2, :cond_2e

    .line 539
    const-string v2, "Sync"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 540
    const-string v2, "Sync"

    const-string v3, "runSyncLoop: No data from client diffs merge"

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_7

    .line 544
    :cond_2e
    const-string v2, "Sync"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 545
    const-string v2, "Sync"

    const-string v3, "runSyncLoop: made some progress, looping"

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    .line 556
    .end local v10    # "clientDiffs":Lcom/sec/android/providers/tasks/SyncableContentProvider;
    .end local v14    # "loopCount":I
    .end local v18    # "numModifications":J
    .end local v21    # "readOnly":Z
    .end local v22    # "previousNumModifications":J
    .restart local v15    # "loopCount":I
    :catchall_1
    move-exception v2

    move v14, v15

    .end local v15    # "loopCount":I
    .restart local v14    # "loopCount":I
    goto/16 :goto_5

    .restart local v10    # "clientDiffs":Lcom/sec/android/providers/tasks/SyncableContentProvider;
    .restart local v18    # "numModifications":J
    .restart local v21    # "readOnly":Z
    .restart local v22    # "previousNumModifications":J
    :cond_2f
    move v15, v14

    .end local v14    # "loopCount":I
    .restart local v15    # "loopCount":I
    goto/16 :goto_8

    .end local v10    # "clientDiffs":Lcom/sec/android/providers/tasks/SyncableContentProvider;
    .end local v18    # "numModifications":J
    .end local v21    # "readOnly":Z
    .end local v22    # "previousNumModifications":J
    :cond_30
    move v14, v15

    .end local v15    # "loopCount":I
    .restart local v14    # "loopCount":I
    goto/16 :goto_1
.end method

.method private sync(Landroid/content/SyncContext;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 14
    .param p1, "syncContext"    # Landroid/content/SyncContext;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 259
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mIsCanceled:Z

    .line 261
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProviderSyncStarted:Z
    invoke-static {v8, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$102(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z

    .line 262
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mAdapterSyncStarted:Z
    invoke-static {v8, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$002(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z

    .line 263
    const/4 v5, 0x0

    .line 266
    .local v5, "message":Ljava/lang/String;
    invoke-static/range {p2 .. p3}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v3

    .line 267
    .local v3, "isSyncable":I
    if-gez v3, :cond_0

    .line 269
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->getIsSyncable(Landroid/accounts/Account;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v3, 0x1

    .line 270
    :goto_0
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v0, v1, v3}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_2

    .line 281
    :cond_0
    :goto_1
    const-string v8, "initialize"

    const/4 v9, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 333
    :cond_1
    :goto_2
    return-void

    .line 269
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 271
    :catch_0
    move-exception v2

    .line 272
    .local v2, "e":Ljava/io/IOException;
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    iget-object v8, v8, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v8, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, v8, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_1

    .line 273
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 274
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    iget-object v8, v8, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v8, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    iput-wide v10, v8, Landroid/content/SyncStats;->numParseExceptions:J

    goto :goto_1

    .line 286
    .end local v2    # "e":Landroid/accounts/AuthenticatorException;
    :cond_3
    if-lez v3, :cond_1

    .line 290
    const-string v8, "force"

    const/4 v9, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 293
    .local v4, "manualSync":Z
    :try_start_1
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;
    invoke-static {v8}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$200(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v8

    move-object/from16 v0, p2

    invoke-virtual {v8, p1, v0}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->onSyncStart(Landroid/content/SyncContext;Landroid/accounts/Account;)V

    .line 294
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x1

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProviderSyncStarted:Z
    invoke-static {v8, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$102(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z

    .line 295
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    iget-object v9, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    move-object/from16 v0, p2

    invoke-virtual {v8, p1, v0, v4, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->onSyncStarting(Landroid/content/SyncContext;Landroid/accounts/Account;ZLandroid/content/SyncResult;)V

    .line 296
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    invoke-virtual {v8}, Landroid/content/SyncResult;->hasError()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 297
    const-string v5, "SyncAdapter failed while trying to start sync"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 321
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mAdapterSyncStarted:Z
    invoke-static {v8}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$000(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 322
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mAdapterSyncStarted:Z
    invoke-static {v8, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$002(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z

    .line 323
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x0

    invoke-virtual {v8, p1, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->onSyncEnding(Landroid/content/SyncContext;Z)V

    .line 325
    :cond_4
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProviderSyncStarted:Z
    invoke-static {v8}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$100(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 326
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProviderSyncStarted:Z
    invoke-static {v8, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$102(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z

    .line 327
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;
    invoke-static {v8}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$200(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, p1, v9}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->onSyncStop(Landroid/content/SyncContext;Z)V

    .line 329
    :cond_5
    iget-boolean v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mIsCanceled:Z

    if-nez v8, :cond_1

    .line 330
    if-eqz v5, :cond_1

    invoke-virtual {p1, v5}, Landroid/content/SyncContext;->setStatusText(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 300
    :cond_6
    :try_start_2
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x1

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mAdapterSyncStarted:Z
    invoke-static {v8, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$002(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z

    .line 301
    iget-boolean v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mIsCanceled:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v8, :cond_9

    .line 321
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mAdapterSyncStarted:Z
    invoke-static {v8}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$000(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 322
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mAdapterSyncStarted:Z
    invoke-static {v8, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$002(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z

    .line 323
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x0

    invoke-virtual {v8, p1, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->onSyncEnding(Landroid/content/SyncContext;Z)V

    .line 325
    :cond_7
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProviderSyncStarted:Z
    invoke-static {v8}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$100(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 326
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProviderSyncStarted:Z
    invoke-static {v8, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$102(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z

    .line 327
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;
    invoke-static {v8}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$200(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, p1, v9}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->onSyncStop(Landroid/content/SyncContext;Z)V

    .line 329
    :cond_8
    iget-boolean v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mIsCanceled:Z

    if-nez v8, :cond_1

    .line 330
    if-eqz v5, :cond_1

    invoke-virtual {p1, v5}, Landroid/content/SyncContext;->setStatusText(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 304
    :cond_9
    :try_start_3
    const-string v8, "SyncTracing"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 305
    .local v7, "syncTracingEnabledValue":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v8

    if-nez v8, :cond_e

    const/4 v6, 0x1

    .line 307
    .local v6, "syncTracingEnabled":Z
    :goto_3
    if-eqz v6, :cond_a

    .line 308
    :try_start_4
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 309
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 310
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "synctrace."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/os/Debug;->startMethodTracing(Ljava/lang/String;)V

    .line 312
    :cond_a
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->runSyncLoop(Landroid/content/SyncContext;Landroid/accounts/Account;Landroid/os/Bundle;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 314
    if-eqz v6, :cond_b

    :try_start_5
    invoke-static {}, Landroid/os/Debug;->stopMethodTracing()V

    .line 316
    :cond_b
    iget-object v9, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    invoke-virtual {v8}, Landroid/content/SyncResult;->hasError()Z

    move-result v8

    if-nez v8, :cond_13

    const/4 v8, 0x1

    :goto_4
    invoke-virtual {v9, p1, v8}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->onSyncEnding(Landroid/content/SyncContext;Z)V

    .line 317
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mAdapterSyncStarted:Z
    invoke-static {v8, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$002(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z

    .line 318
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;
    invoke-static {v8}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$200(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, p1, v9}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->onSyncStop(Landroid/content/SyncContext;Z)V

    .line 319
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProviderSyncStarted:Z
    invoke-static {v8, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$102(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 321
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mAdapterSyncStarted:Z
    invoke-static {v8}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$000(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 322
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mAdapterSyncStarted:Z
    invoke-static {v8, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$002(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z

    .line 323
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x0

    invoke-virtual {v8, p1, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->onSyncEnding(Landroid/content/SyncContext;Z)V

    .line 325
    :cond_c
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProviderSyncStarted:Z
    invoke-static {v8}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$100(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 326
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v9, 0x0

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProviderSyncStarted:Z
    invoke-static {v8, v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$102(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z

    .line 327
    iget-object v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;
    invoke-static {v8}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$200(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, p1, v9}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->onSyncStop(Landroid/content/SyncContext;Z)V

    .line 329
    :cond_d
    iget-boolean v8, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mIsCanceled:Z

    if-nez v8, :cond_1

    .line 330
    if-eqz v5, :cond_1

    invoke-virtual {p1, v5}, Landroid/content/SyncContext;->setStatusText(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 305
    .end local v6    # "syncTracingEnabled":Z
    :cond_e
    const/4 v6, 0x0

    goto/16 :goto_3

    .line 314
    .restart local v6    # "syncTracingEnabled":Z
    :catchall_0
    move-exception v8

    if-eqz v6, :cond_f

    :try_start_6
    invoke-static {}, Landroid/os/Debug;->stopMethodTracing()V

    :cond_f
    throw v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 321
    .end local v6    # "syncTracingEnabled":Z
    .end local v7    # "syncTracingEnabledValue":Ljava/lang/String;
    :catchall_1
    move-exception v8

    iget-object v9, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mAdapterSyncStarted:Z
    invoke-static {v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$000(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 322
    iget-object v9, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v10, 0x0

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mAdapterSyncStarted:Z
    invoke-static {v9, v10}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$002(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z

    .line 323
    iget-object v9, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v10, 0x0

    invoke-virtual {v9, p1, v10}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->onSyncEnding(Landroid/content/SyncContext;Z)V

    .line 325
    :cond_10
    iget-object v9, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProviderSyncStarted:Z
    invoke-static {v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$100(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 326
    iget-object v9, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    const/4 v10, 0x0

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProviderSyncStarted:Z
    invoke-static {v9, v10}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$102(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z

    .line 327
    iget-object v9, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;
    invoke-static {v9}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$200(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, p1, v10}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->onSyncStop(Landroid/content/SyncContext;Z)V

    .line 329
    :cond_11
    iget-boolean v9, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mIsCanceled:Z

    if-nez v9, :cond_12

    .line 330
    if-eqz v5, :cond_12

    invoke-virtual {p1, v5}, Landroid/content/SyncContext;->setStatusText(Ljava/lang/String;)V

    :cond_12
    throw v8

    .line 316
    .restart local v6    # "syncTracingEnabled":Z
    .restart local v7    # "syncTracingEnabledValue":Ljava/lang/String;
    :cond_13
    const/4 v8, 0x0

    goto/16 :goto_4

    .line 275
    .end local v4    # "manualSync":Z
    .end local v6    # "syncTracingEnabled":Z
    .end local v7    # "syncTracingEnabledValue":Ljava/lang/String;
    :catch_2
    move-exception v8

    goto/16 :goto_1
.end method


# virtual methods
.method cancelSync()V
    .locals 2

    .prologue
    .line 226
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mIsCanceled:Z

    .line 227
    iget-object v1, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mAdapterSyncStarted:Z
    invoke-static {v1}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$000(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    invoke-virtual {v1}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->onSyncCanceled()V

    .line 228
    :cond_0
    iget-object v1, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProviderSyncStarted:Z
    invoke-static {v1}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$100(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # getter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;
    invoke-static {v1}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$200(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Lcom/sec/android/providers/tasks/SyncableContentProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/providers/tasks/SyncableContentProvider;->onSyncCanceled()V

    .line 230
    :cond_1
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    .line 233
    .local v0, "uid":I
    return-void
.end method

.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 237
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v2

    const/16 v3, 0xa

    invoke-static {v2, v3}, Landroid/os/Process;->setThreadPriority(II)V

    .line 239
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    .line 243
    .local v1, "uid":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mSyncContext:Landroid/content/SyncContext;

    iget-object v3, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mAccount:Landroid/accounts/Account;

    iget-object v4, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mAuthority:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mExtras:Landroid/os/Bundle;

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->sync(Landroid/content/SyncContext;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    iget-object v2, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mSyncThread:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;
    invoke-static {v2, v6}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$302(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;)Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;

    .line 255
    :goto_0
    return-void

    .line 244
    :catch_0
    move-exception v0

    .line 245
    .local v0, "e":Landroid/database/SQLException;
    :try_start_1
    const-string v2, "Sync"

    const-string v3, "Sync failed"

    invoke-static {v2, v3, v0}, Lcom/sec/android/providers/tasks/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 246
    iget-object v2, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->mResult:Landroid/content/SyncResult;

    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/content/SyncResult;->databaseError:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 248
    iget-object v2, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mSyncThread:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;
    invoke-static {v2, v6}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$302(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;)Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;

    goto :goto_0

    .end local v0    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->this$0:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    # setter for: Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mSyncThread:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;
    invoke-static {v3, v6}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->access$302(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;)Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;

    throw v2
.end method
