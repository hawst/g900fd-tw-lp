.class public final Lcom/sec/android/providers/tasks/TasksProvider$EventInstancesMap;
.super Ljava/util/HashMap;
.source "TasksProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/providers/tasks/TasksProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EventInstancesMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/sec/android/providers/tasks/TasksProvider$InstancesList;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;Landroid/content/ContentValues;)V
    .locals 1
    .param p1, "syncId"    # Ljava/lang/String;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 156
    invoke-virtual {p0, p1}, Lcom/sec/android/providers/tasks/TasksProvider$EventInstancesMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/providers/tasks/TasksProvider$InstancesList;

    .line 157
    .local v0, "instances":Lcom/sec/android/providers/tasks/TasksProvider$InstancesList;
    if-nez v0, :cond_0

    .line 158
    new-instance v0, Lcom/sec/android/providers/tasks/TasksProvider$InstancesList;

    .end local v0    # "instances":Lcom/sec/android/providers/tasks/TasksProvider$InstancesList;
    invoke-direct {v0}, Lcom/sec/android/providers/tasks/TasksProvider$InstancesList;-><init>()V

    .line 159
    .restart local v0    # "instances":Lcom/sec/android/providers/tasks/TasksProvider$InstancesList;
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/providers/tasks/TasksProvider$EventInstancesMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    :cond_0
    invoke-virtual {v0, p2}, Lcom/sec/android/providers/tasks/TasksProvider$InstancesList;->add(Ljava/lang/Object;)Z

    .line 162
    return-void
.end method
