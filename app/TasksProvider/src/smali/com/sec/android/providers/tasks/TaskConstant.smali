.class public Lcom/sec/android/providers/tasks/TaskConstant;
.super Ljava/lang/Object;
.source "TaskConstant.java"


# static fields
.field public static final ACCOUNT_ID:Ljava/lang/String; = "_accountId"

.field public static final GROUP_ID:Ljava/lang/String; = "groupId"

.field public static final GROUP_NAME:Ljava/lang/String; = "groupName"

.field public static final INDENT:Ljava/lang/String; = "indent"

.field public static final IS_DEFAULT:Ljava/lang/String; = "isDefault"

.field public static final PARENT_ID:Ljava/lang/String; = "parentId"

.field public static final PREVIOUS_ID:Ljava/lang/String; = "previousId"

.field public static final SELECTED:Ljava/lang/String; = "selected"

.field public static final TASK_GROUP_TABLE:Ljava/lang/String; = "TaskGroup"

.field public static final TASK_ID:Ljava/lang/String; = "task_id"

.field public static final TASK_REMINDER_TIME:Ljava/lang/String; = "reminder_time"

.field public static final UN_INDENT:Ljava/lang/String; = "unIndent"

.field public static final _ID:Ljava/lang/String; = "_id"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
