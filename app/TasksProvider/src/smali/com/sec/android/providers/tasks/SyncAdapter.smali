.class public abstract Lcom/sec/android/providers/tasks/SyncAdapter;
.super Ljava/lang/Object;
.source "SyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/providers/tasks/SyncAdapter$Transport;
    }
.end annotation


# static fields
.field public static final LOG_SYNC_DETAILS:I = 0xab7

.field private static final TAG:Ljava/lang/String; = "SyncAdapter"


# instance fields
.field mTransport:Lcom/sec/android/providers/tasks/SyncAdapter$Transport;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lcom/sec/android/providers/tasks/SyncAdapter$Transport;

    invoke-direct {v0, p0}, Lcom/sec/android/providers/tasks/SyncAdapter$Transport;-><init>(Lcom/sec/android/providers/tasks/SyncAdapter;)V

    iput-object v0, p0, Lcom/sec/android/providers/tasks/SyncAdapter;->mTransport:Lcom/sec/android/providers/tasks/SyncAdapter$Transport;

    return-void
.end method


# virtual methods
.method public abstract cancelSync()V
.end method

.method public final getISyncAdapter()Landroid/content/ISyncAdapter;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/providers/tasks/SyncAdapter;->mTransport:Lcom/sec/android/providers/tasks/SyncAdapter$Transport;

    return-object v0
.end method

.method public initialize(Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "authority"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 88
    return-void
.end method

.method public abstract startSync(Landroid/content/SyncContext;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
.end method
