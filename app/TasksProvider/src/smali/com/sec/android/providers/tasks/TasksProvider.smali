.class public Lcom/sec/android/providers/tasks/TasksProvider;
.super Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;
.source "TasksProvider.java"

# interfaces
.implements Landroid/accounts/OnAccountsUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/providers/tasks/TasksProvider$1;,
        Lcom/sec/android/providers/tasks/TasksProvider$TimezoneCheckerThread;,
        Lcom/sec/android/providers/tasks/TasksProvider$EventInstancesMap;,
        Lcom/sec/android/providers/tasks/TasksProvider$InstancesList;
    }
.end annotation


# static fields
.field private static final ACCOUNTS_PROJECTION:[Ljava/lang/String;

.field static final ACCOUNT_KEY:Ljava/lang/String; = "_sync_account_key"

.field private static ACCOUNT_MYTASK_NAME:Ljava/lang/String; = null

.field static final ACCOUNT_NAME:Ljava/lang/String; = "_sync_account"

.field static final ACCOUNT_TYPE:Ljava/lang/String; = "_sync_account_type"

.field private static final ACCOUNT_TYPE_LOCAL:Ljava/lang/String; = "local"

.field public static ACTION_REFERESH_ALARM:Ljava/lang/String; = null

.field private static final DATABASE_NAME:Ljava/lang/String; = "tasks.db"

.field private static final DATABASE_VERSION:I = 0x1

.field private static final DELETED_TASKS:I = 0x9

.field private static final DELETED_TASKS_ID:I = 0xa

.field private static final DELETED_TASK_SYNC_DELETE:Ljava/lang/String; = "delete from DeletedTasks where _id="

.field private static final DELETED_TASK_SYNC_INSERT:Ljava/lang/String; = "insert or ignore into DeletedTasks select * from tasks where _id="

.field private static final EVENTS_SYNC_ACCOUNT_NAME_INDEX:I = 0x1

.field private static final EVENTS_SYNC_ACCOUNT_TYPE_INDEX:I = 0x2

.field private static final EVENTS_SYNC_ID_INDEX:I = 0x0

.field private static final MAX_ASSUMED_DURATION:I = 0x240c8400

.field private static final NOT_DEFINED:I = -0x64

.field private static final PROFILE:Z = false

.field private static final REMINDER_TASK_DELETE:Ljava/lang/String; = "delete from TasksReminders where task_id="

.field private static final REMINDER_TASK_UPDATE:Ljava/lang/String; = "update TasksReminders set reminder_time="

.field private static final SEARCH_RESULT:I = 0x14

.field private static final SEARCH_SUGGEST:I = 0x15

.field private static final SUGGESTION_BODY:I = 0x2

.field private static final SUGGESTION_ID:I = 0x0

.field private static final SUGGESTION_PROJECTION:[Ljava/lang/String;

.field private static final SUGGESTION_SUBJECT:I = 0x1

.field private static final SYNCED_TASKS:I = 0x7

.field private static final SYNCED_TASKS_ID:I = 0x8

.field private static final SYNCSTATE:I = 0x12

.field private static final TAG:Ljava/lang/String; = "TasksProvider"

.field private static final TASKS:I = 0x1

.field private static final TASKS_ACCOUNTS:I = 0xd

.field private static final TASKS_ACCOUNTS_ID:I = 0xe

.field private static final TASKS_ID:I = 0x2

.field private static final TASKS_PROJECTION:[Ljava/lang/String;

.field private static final TASKS_REMINDERS:I = 0xb

.field private static final TASKS_REMINDERS_ID:I = 0xc

.field private static final UPDATED_TASKS:I = 0x5

.field private static final UPDATED_TASKS_ID:I = 0x6

.field private static final UPDATED_TASK_SYNC_DELETE:Ljava/lang/String; = "delete from UpdatedTasks where _id="

.field private static final UPDATED_TASK_SYNC_INSERT:Ljava/lang/String; = "insert or ignore into UpdatedTasks select * from tasks where _id="

.field private static final _TASKS:I = 0x3

.field private static final _TASKS_ID:I = 0x4

.field public static mIsinit:Z

.field private static final sURLMatcher:Landroid/content/UriMatcher;


# instance fields
.field final COLUMNS:[Ljava/lang/String;

.field private mDeletedTasksInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mTasksAccountsInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mTasksInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mTasksReminderInserter:Landroid/database/DatabaseUtils$InsertHelper;

.field private mUpdatedTasksInserter:Landroid/database/DatabaseUtils$InsertHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0x14

    const/4 v5, 0x3

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 80
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_sync_account"

    aput-object v1, v0, v2

    const-string v1, "_sync_account_type"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->ACCOUNTS_PROJECTION:[Ljava/lang/String;

    .line 84
    const-string v0, "com.android.task.ACTION_REFERESH_ALARM"

    sput-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->ACTION_REFERESH_ALARM:Ljava/lang/String;

    .line 85
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_sync_id"

    aput-object v1, v0, v2

    const-string v1, "_sync_account"

    aput-object v1, v0, v3

    const-string v1, "_sync_account_type"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->TASKS_PROJECTION:[Ljava/lang/String;

    .line 97
    sput-boolean v3, Lcom/sec/android/providers/tasks/TasksProvider;->mIsinit:Z

    .line 99
    const-string v0, "My Task"

    sput-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->ACCOUNT_MYTASK_NAME:Ljava/lang/String;

    .line 1261
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    .line 1263
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "tasks._id"

    aput-object v1, v0, v2

    const-string v1, "subject"

    aput-object v1, v0, v3

    const-string v1, "body"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->SUGGESTION_PROJECTION:[Ljava/lang/String;

    .line 1277
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "tasks"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1278
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "tasks/#"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1279
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "_Tasks"

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1280
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "_Tasks/#"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1281
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "UpdatedTasks"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1282
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "UpdatedTasks/#"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1283
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "syncTasks"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1284
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "syncTasks/#"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1285
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "DeletedTasks"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1286
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "DeletedTasks/#"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1287
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "TasksReminders"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1288
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "TasksReminders/#"

    const/16 v3, 0xc

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1289
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "TasksAccounts"

    const/16 v3, 0xd

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1290
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "TasksAccounts/#"

    const/16 v3, 0xe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1292
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "searchresult"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1293
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "searchresult/*"

    invoke-virtual {v0, v1, v2, v6}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1295
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "search_suggest_query"

    const/16 v3, 0x15

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1296
    sget-object v0, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    const-string v1, "tasks"

    const-string v2, "search_suggest_query/*"

    const/16 v3, 0x15

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1336
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 171
    const-string v0, "tasks.db"

    sget-object v1, Lcom/sec/android/providers/tasks/Tasks;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {p0, v0, v3, v1}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;-><init>(Ljava/lang/String;ILandroid/net/Uri;)V

    .line 109
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const-string v1, "suggest_text_1"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "suggest_intent_action"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggest_intent_data"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/providers/tasks/TasksProvider;->COLUMNS:[Ljava/lang/String;

    .line 172
    const-string v0, "SANDEEP"

    const-string v1, "In task provider"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    const-string v0, "TasksProvider"

    const-string v1, "TaskProvider Constructor"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/providers/tasks/TasksProvider;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/providers/tasks/TasksProvider;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/providers/tasks/TasksProvider;->doUpdateTimezoneDependentFields()V

    return-void
.end method

.method private createTasksTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 278
    const-string v2, "TasksProvider"

    const-string v3, "TaskProvider createTasksTable()"

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    const-string v1, "_id INTEGER PRIMARY KEY,syncServerId TEXT,displayName TEXT,_sync_dirty INTEGER,clientId TEXT,sourceid INTEGER,bodyType INTEGER,body_size INTEGER,body_truncated INTEGER,due_date INTEGER,utc_due_date INTEGER,importance INTEGER,date_completed INTEGER,complete INTEGER,recurrence_type INTEGER,recurrence_start INTEGER,recurrence_until INTEGER,recurrence_occurrences INTEGER,recurrence_interval INTEGER,recurrence_day_of_month INTEGER,recurrence_day_of_week INTEGER,recurrence_week_of_month INTEGER,recurrence_month_of_year INTEGER,recurrence_regenerate INTEGER,recurrence_dead_occur INTEGER,reminder_set INTEGER,reminder_time INTEGER,sensitivity INTEGER,start_date INTEGER,utc_start_date INTEGER,parentId INTEGER,subject TEXT,body TEXT,category1 TEXT,category2 TEXT,category3 TEXT,mailboxKey integer,accountKey integer,accountName TEXT,reminder_type INTEGER"

    .line 333
    .local v1, "taskColumns":Ljava/lang/String;
    const-string v0, "_id INTEGER PRIMARY KEY,task_id INTEGER ,reminder_time LONG , state INTEGER , subject TEXT, start_date INTEGER, due_date INTEGER, accountkey INTEGER, reminder_type INTEGER"

    .line 341
    .local v0, "_reminder_Columns":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CREATE TABLE Tasks ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 343
    const-string v2, "CREATE INDEX eventSyncAccountAndIdIndex ON Tasks (sourceid)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 347
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CREATE TABLE DeletedTasks ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 349
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CREATE TABLE UpdatedTasks ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 351
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CREATE TABLE TasksReminders ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 384
    return-void
.end method

.method private createTasksView(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 387
    const-string v1, "TasksProvider"

    const-string v2, "======== createTasksView ========"

    invoke-static {v1, v2}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    const-string v1, "DROP VIEW IF EXISTS view_tasks;"

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 391
    const-string v0, "SELECT Tasks._id AS _id,syncServerId,Tasks.displayName AS displayName,_sync_dirty,clientId,sourceid,bodyType,body_size,body_truncated,due_date,utc_due_date,importance,date_completed,complete,recurrence_type,recurrence_start,recurrence_until,recurrence_occurrences,recurrence_interval,recurrence_day_of_month,recurrence_day_of_week,recurrence_week_of_month,recurrence_month_of_year,recurrence_regenerate,recurrence_dead_occur,reminder_set,reminder_time,sensitivity,start_date,utc_start_date,parentId,subject,body,category1,category2,category3,mailboxKey,accountKey,accountName,reminder_type,selected FROM Tasks JOIN TasksAccounts ON (Tasks.accountKey=TasksAccounts._sync_account_key)"

    .line 461
    .local v0, "tasksSelect":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CREATE VIEW view_tasks AS "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 462
    return-void
.end method

.method private doUpdateTimezoneDependentFields()V
    .locals 0

    .prologue
    .line 242
    return-void
.end method

.method private dropTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 270
    const-string v0, "TasksProvider"

    const-string v1, "TaskProvider dropTables()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    const-string v0, "DROP TABLE IF EXISTS Tasks;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 272
    const-string v0, "DROP TABLE IF EXISTS DeletedTasks;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 273
    const-string v0, "DROP TABLE IF EXISTS UpdatedTasks;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 274
    const-string v0, "DROP TABLE IF EXISTS TasksReminders;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 275
    return-void
.end method

.method private insertOrUpdateReminder(Landroid/content/ContentValues;Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 25
    .param p1, "cv"    # Landroid/content/ContentValues;
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3, "id"    # J

    .prologue
    .line 1015
    const-string v2, "TasksProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Entering insertOrUpdateReminder(), id="

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, p3

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1017
    const-string v5, "task_id=?"

    .line 1018
    .local v5, "selection":Ljava/lang/String;
    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v7, "_id"

    aput-object v7, v4, v2

    const/4 v2, 0x1

    const-string v7, "task_id"

    aput-object v7, v4, v2

    .line 1021
    .local v4, "projectionIn":[Ljava/lang/String;
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    .line 1024
    .local v6, "selectionArgs":[Ljava/lang/String;
    sget-object v3, Lcom/sec/android/providers/tasks/Tasks$TaskReminderAlerts;->REMINDER_CONTENT_URI:Landroid/net/Uri;

    .line 1025
    .local v3, "reminderUri":Landroid/net/Uri;
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 1026
    .local v15, "newReminder":Landroid/content/ContentValues;
    const/16 v18, 0x0

    .line 1027
    .local v18, "reminderSet":I
    const/16 v14, -0x64

    .line 1029
    .local v14, "completedSet":I
    const-string v2, "task_id"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v15, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1031
    if-nez p1, :cond_1

    .line 1032
    const-string v2, "TasksProvider"

    const-string v7, "insertOrUpdateReminder() - cv == null Exiting..."

    invoke-static {v2, v7}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1146
    :cond_0
    :goto_0
    return-void

    .line 1036
    :cond_1
    const/4 v13, 0x0

    .line 1037
    .local v13, "c1":Landroid/database/Cursor;
    const/16 v19, 0x0

    .line 1039
    .local v19, "tasksCursor":Landroid/database/Cursor;
    :try_start_0
    const-string v2, "SANDEEP"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "reminder uri is: "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1040
    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/providers/tasks/TasksProvider;->queryInternal(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1043
    if-eqz v13, :cond_6

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_6

    .line 1045
    const-string v2, "reminder_time"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "reminder_set"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1047
    :cond_2
    const-string v2, "TasksProvider"

    const-string v7, "insertOrUpdateReminder() - No REMINDER_TIME and REMINDER_SET Exiting..."

    invoke-static {v2, v7}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1049
    invoke-interface {v13}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1140
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1141
    if-eqz v19, :cond_0

    .line 1142
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1143
    const/16 v19, 0x0

    goto :goto_0

    .line 1053
    :cond_3
    :try_start_1
    new-instance v23, Landroid/content/ContentValues;

    invoke-direct/range {v23 .. v23}, Landroid/content/ContentValues;-><init>()V

    .line 1054
    .local v23, "values":Landroid/content/ContentValues;
    const-string v2, "reminder_time"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 1055
    .local v20, "reminderTime":J
    const-string v2, "reminder_time"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1056
    const-string v2, "reminder_set"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v18

    .line 1058
    if-lez v18, :cond_4

    .line 1059
    const-string v2, "state"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1064
    :goto_1
    const-string v2, "subject"

    const-string v7, "subject"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1065
    const-string v2, "start_date"

    const-string v7, "start_date"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1066
    const-string v2, "due_date"

    const-string v7, "due_date"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    const-string v2, "accountkey"

    const-string v7, "accountKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1068
    const-string v2, "reminder_type"

    const-string v7, "reminder_type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1070
    const-string v2, "TasksReminders"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v2, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1071
    const-string v2, "reminder_time"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v15, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1073
    const-string v2, "TasksProvider"

    const-string v7, "insertOrUpdateReminder() - existing reminder, updating"

    invoke-static {v2, v7}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1138
    .end local v20    # "reminderTime":J
    .end local v23    # "values":Landroid/content/ContentValues;
    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/sec/android/providers/tasks/TasksProvider;->scheduleReminder(Landroid/content/ContentValues;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1140
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1141
    if-eqz v19, :cond_0

    .line 1142
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1143
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 1061
    .restart local v20    # "reminderTime":J
    .restart local v23    # "values":Landroid/content/ContentValues;
    :cond_4
    :try_start_2
    const-string v2, "state"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 1140
    .end local v20    # "reminderTime":J
    .end local v23    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v2

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1141
    if-eqz v19, :cond_5

    .line 1142
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1143
    const/16 v19, 0x0

    :cond_5
    throw v2

    .line 1075
    :cond_6
    :try_start_3
    const-string v2, "complete"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1076
    const-string v2, "complete"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 1079
    :cond_7
    const-string v2, "reminder_set"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    if-nez v14, :cond_a

    .line 1082
    sget-object v2, Lcom/sec/android/providers/tasks/Tasks;->TASK_CONTENT_URI:Landroid/net/Uri;

    move-wide/from16 v0, p3

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v8

    .line 1083
    .local v8, "taskUri":Landroid/net/Uri;
    if-nez v8, :cond_8

    .line 1140
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1141
    if-eqz v19, :cond_0

    .line 1142
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1143
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 1086
    :cond_8
    const/4 v2, 0x7

    :try_start_4
    new-array v9, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v7, "reminder_set"

    aput-object v7, v9, v2

    const/4 v2, 0x1

    const-string v7, "subject"

    aput-object v7, v9, v2

    const/4 v2, 0x2

    const-string v7, "start_date"

    aput-object v7, v9, v2

    const/4 v2, 0x3

    const-string v7, "due_date"

    aput-object v7, v9, v2

    const/4 v2, 0x4

    const-string v7, "accountKey"

    aput-object v7, v9, v2

    const/4 v2, 0x5

    const-string v7, "reminder_type"

    aput-object v7, v9, v2

    const/4 v2, 0x6

    const-string v7, "reminder_time"

    aput-object v7, v9, v2

    .line 1091
    .local v9, "projectionTable":[Ljava/lang/String;
    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v7, p0

    invoke-virtual/range {v7 .. v12}, Lcom/sec/android/providers/tasks/TasksProvider;->queryInternal(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v19

    .line 1092
    if-nez v19, :cond_9

    .line 1140
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1141
    if-eqz v19, :cond_0

    .line 1142
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 1143
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 1095
    :cond_9
    :try_start_5
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_a

    .line 1096
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1098
    const-string v2, "reminder_set"

    const/4 v7, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1099
    const-string v2, "subject"

    const/4 v7, 0x1

    move-object/from16 v0, v19

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1101
    const-string v2, "start_date"

    const/4 v7, 0x2

    move-object/from16 v0, v19

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1103
    const-string v2, "due_date"

    const/4 v7, 0x3

    move-object/from16 v0, v19

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1104
    const-string v2, "accountKey"

    const/4 v7, 0x4

    move-object/from16 v0, v19

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1105
    const-string v2, "reminder_type"

    const/4 v7, 0x5

    move-object/from16 v0, v19

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1106
    const-string v2, "reminder_time"

    const/4 v7, 0x6

    move-object/from16 v0, v19

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1111
    .end local v8    # "taskUri":Landroid/net/Uri;
    .end local v9    # "projectionTable":[Ljava/lang/String;
    :cond_a
    const-string v2, "reminder_time"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 1112
    .local v16, "remTime":J
    const-string v2, "reminder_time"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v15, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1113
    const-string v2, "reminder_set"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v18

    .line 1115
    new-instance v22, Landroid/text/format/Time;

    invoke-direct/range {v22 .. v22}, Landroid/text/format/Time;-><init>()V

    .line 1116
    .local v22, "tmpTime":Landroid/text/format/Time;
    invoke-virtual/range {v22 .. v22}, Landroid/text/format/Time;->setToNow()V

    .line 1118
    if-lez v18, :cond_c

    .line 1119
    const-string v2, "state"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v15, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1125
    :goto_3
    const/4 v2, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    cmp-long v2, v16, v10

    if-gez v2, :cond_b

    .line 1126
    const-string v2, "state"

    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v15, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1130
    :cond_b
    const-string v2, "subject"

    const-string v7, "subject"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v15, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    const-string v2, "start_date"

    const-string v7, "start_date"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v15, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1132
    const-string v2, "due_date"

    const-string v7, "due_date"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v15, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1133
    const-string v2, "accountkey"

    const-string v7, "accountKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v15, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    const-string v2, "reminder_type"

    const-string v7, "reminder_type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v15, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/providers/tasks/TasksProvider;->mTasksReminderInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v2, v15}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    goto/16 :goto_2

    .line 1121
    :cond_c
    const-string v2, "state"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v15, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3
.end method

.method private processReminderSettings(Landroid/database/sqlite/SQLiteDatabase;JLandroid/content/ContentValues;)V
    .locals 20
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "id"    # J
    .param p4, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 956
    const-string v6, "TasksProvider"

    const-string v11, "Entering processReminderSettings"

    invoke-static {v6, v11}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 957
    const/16 v16, -0x64

    .line 958
    .local v16, "reminderSet":I
    const/16 v12, -0x64

    .line 960
    .local v12, "completedSet":I
    const-string v6, "complete"

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 961
    const-string v6, "complete"

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 964
    :cond_0
    const-string v6, "reminder_set"

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 965
    const-string v6, "reminder_set"

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 970
    :cond_1
    const/16 v6, -0x64

    move/from16 v0, v16

    if-ne v0, v6, :cond_2

    const/16 v6, -0x64

    if-eq v12, v6, :cond_5

    .line 976
    :cond_2
    sget-object v7, Lcom/sec/android/providers/tasks/Tasks;->TASK_CONTENT_URI:Landroid/net/Uri;

    .line 977
    .local v7, "uri":Landroid/net/Uri;
    const/4 v6, 0x1

    new-array v8, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v11, "reminder_set"

    aput-object v11, v8, v6

    .line 980
    .local v8, "projection":[Ljava/lang/String;
    const-string v9, "_id=?"

    .line 981
    .local v9, "selection":Ljava/lang/String;
    const/4 v6, 0x1

    new-array v10, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v6

    .line 985
    .local v10, "selectionArgs":[Ljava/lang/String;
    const/4 v11, 0x0

    move-object/from16 v6, p0

    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/providers/tasks/TasksProvider;->queryInternal(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 986
    .local v13, "cursor":Landroid/database/Cursor;
    const-wide/16 v14, 0x0

    .line 987
    .local v14, "hasReminder":J
    if-eqz v13, :cond_3

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_3

    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 988
    const/4 v6, 0x0

    invoke-interface {v13, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 992
    :cond_3
    if-eqz v13, :cond_4

    .line 993
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 995
    :cond_4
    const-string v6, "TAG"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "reminderSet: "

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v6, v11}, Lcom/sec/android/providers/tasks/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 996
    const-string v6, "TAG"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "completedSet: "

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v6, v11}, Lcom/sec/android/providers/tasks/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    const-string v6, "TAG"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "hasReminder: "

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v6, v11}, Lcom/sec/android/providers/tasks/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    const/4 v6, 0x1

    if-ne v12, v6, :cond_6

    .line 1000
    const-string v6, "TAG"

    const-string v11, "Task is completed so Delete task record from reminder table:"

    invoke-static {v6, v11}, Lcom/sec/android/providers/tasks/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "delete from TasksReminders where task_id="

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1012
    .end local v7    # "uri":Landroid/net/Uri;
    .end local v8    # "projection":[Ljava/lang/String;
    .end local v9    # "selection":Ljava/lang/String;
    .end local v10    # "selectionArgs":[Ljava/lang/String;
    .end local v13    # "cursor":Landroid/database/Cursor;
    .end local v14    # "hasReminder":J
    :cond_5
    :goto_0
    return-void

    .line 1003
    .restart local v7    # "uri":Landroid/net/Uri;
    .restart local v8    # "projection":[Ljava/lang/String;
    .restart local v9    # "selection":Ljava/lang/String;
    .restart local v10    # "selectionArgs":[Ljava/lang/String;
    .restart local v13    # "cursor":Landroid/database/Cursor;
    .restart local v14    # "hasReminder":J
    :cond_6
    if-gtz v16, :cond_7

    if-nez v12, :cond_8

    :cond_7
    const-wide/16 v18, 0x1

    cmp-long v6, v14, v18

    if-nez v6, :cond_8

    .line 1004
    const-string v6, "TAG"

    const-string v11, "inser or update reminder table:"

    invoke-static {v6, v11}, Lcom/sec/android/providers/tasks/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1005
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p1

    move-wide/from16 v3, p2

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/providers/tasks/TasksProvider;->insertOrUpdateReminder(Landroid/content/ContentValues;Landroid/database/sqlite/SQLiteDatabase;J)V

    goto :goto_0

    .line 1007
    :cond_8
    const-string v6, "TAG"

    const-string v11, "Delete task record from reminder table:"

    invoke-static {v6, v11}, Lcom/sec/android/providers/tasks/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1008
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "delete from TasksReminders where task_id="

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static removeReminder(Landroid/content/Context;J)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "task_id"    # J

    .prologue
    const/4 v11, 0x1

    const/4 v5, 0x0

    const/4 v12, 0x0

    .line 1412
    const-string v7, "TasksProvider"

    const-string v10, "TaskProvider removeReminder()"

    invoke-static {v7, v10}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1414
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1415
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v3, "task_id=?"

    .line 1416
    .local v3, "selection":Ljava/lang/String;
    new-array v2, v11, [Ljava/lang/String;

    const-string v7, "_id"

    aput-object v7, v2, v12

    .line 1420
    .local v2, "projectionIn":[Ljava/lang/String;
    new-array v4, v11, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v12

    .line 1423
    .local v4, "selectionArgs":[Ljava/lang/String;
    sget-object v1, Lcom/sec/android/providers/tasks/Tasks$TaskReminderAlerts;->REMINDER_CONTENT_URI:Landroid/net/Uri;

    .line 1425
    .local v1, "reminderUri":Landroid/net/Uri;
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1426
    .local v6, "c1":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 1443
    :cond_0
    :goto_0
    return-void

    .line 1430
    :cond_1
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1432
    invoke-interface {v6, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 1433
    .local v8, "id":J
    sget-object v7, Lcom/sec/android/providers/tasks/Tasks$TaskReminderAlerts;->REMINDER_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 1434
    if-eqz v1, :cond_1

    .line 1435
    const-string v7, "inside removereminder of task provider for reminder delete id is "

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1436
    invoke-virtual {v0, v1, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    .line 1439
    .end local v8    # "id":J
    :cond_2
    if-eqz v6, :cond_0

    .line 1440
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1441
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private scheduleReminder(Landroid/content/ContentValues;)V
    .locals 9
    .param p1, "initialValues"    # Landroid/content/ContentValues;

    .prologue
    const/4 v1, 0x0

    .line 803
    const-string v6, "TasksProvider"

    const-string v7, "TaskProvider scheduleReminder()"

    invoke-static {v6, v7}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    const-wide/16 v4, 0x0

    .line 805
    .local v4, "task_id":J
    const-wide/16 v2, 0x0

    .line 807
    .local v2, "alarm_time":J
    const-string v6, "task_id"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "reminder_time"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 809
    const-string v6, "task_id"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 810
    const-string v6, "reminder_time"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 811
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/TasksProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 812
    .local v0, "context":Landroid/content/Context;
    const-string v6, "TasksProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "scheduling reminder for this id and for this time "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    invoke-static/range {v0 .. v5}, Lcom/sec/android/providers/tasks/TasksProvider;->scheduleReminder(Landroid/content/Context;Landroid/app/AlarmManager;JJ)V

    .line 819
    .end local v0    # "context":Landroid/content/Context;
    :goto_0
    return-void

    .line 817
    :cond_0
    const-string v6, "TasksProvider"

    const-string v7, "There are no TASK_REMINDER_TIME and TASK_ID keys in the data set"

    invoke-static {v6, v7, v1}, Lcom/sec/android/providers/tasks/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static scheduleReminder(Landroid/content/Context;Landroid/app/AlarmManager;JJ)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Landroid/app/AlarmManager;
    .param p2, "alarmTime"    # J
    .param p4, "task_id"    # J

    .prologue
    const/4 v6, 0x0

    .line 1384
    const-string v4, "TasksProvider"

    const-string v5, "TaskProvider scheduleReminder()"

    invoke-static {v4, v5}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1385
    const-string v4, "TasksProvider"

    const-string v5, " inside schedule reminder to set a reminder for a task "

    invoke-static {v4, v5}, Lcom/sec/android/providers/tasks/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1387
    if-nez p1, :cond_0

    .line 1388
    const-string v4, "alarm"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    .end local p1    # "manager":Landroid/app/AlarmManager;
    check-cast p1, Landroid/app/AlarmManager;

    .line 1390
    .restart local p1    # "manager":Landroid/app/AlarmManager;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    sget-object v4, Lcom/sec/android/providers/tasks/TasksProvider;->ACTION_REFERESH_ALARM:Ljava/lang/String;

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1391
    .local v0, "intent":Landroid/content/Intent;
    const-string v4, "content://tasks/task"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1393
    const-string v4, "_id"

    invoke-virtual {v0, v4, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1399
    invoke-static {p0, v6, v0, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1401
    .local v1, "pi":Landroid/app/PendingIntent;
    invoke-virtual {p1, v6, p2, p3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 1403
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 1404
    .local v3, "time":Landroid/text/format/Time;
    invoke-virtual {v3, p2, p3}, Landroid/text/format/Time;->set(J)V

    .line 1405
    const-string v4, " %a, %b %d, %Y %I:%M%P"

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1406
    .local v2, "schedTime":Ljava/lang/String;
    const-string v4, "TasksProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Scheduled reminder at "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "reminder_time"

    const-wide/16 v8, -0x1

    invoke-virtual {v0, v6, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1409
    return-void
.end method

.method private scheduleSync(Landroid/accounts/Account;ZLjava/lang/String;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "uploadChangesOnly"    # Z
    .param p3, "url"    # Ljava/lang/String;

    .prologue
    .line 1156
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1157
    .local v0, "extras":Landroid/os/Bundle;
    const-string v1, "upload"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1158
    if-eqz p3, :cond_0

    .line 1159
    const-string v1, "feed"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    const-string v1, "force"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1162
    :cond_0
    sget-object v1, Lcom/sec/android/providers/tasks/Tasks$TasksTbl;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1163
    return-void
.end method

.method private updateTimezoneDependentFields()V
    .locals 2

    .prologue
    .line 220
    new-instance v0, Lcom/sec/android/providers/tasks/TasksProvider$TimezoneCheckerThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/providers/tasks/TasksProvider$TimezoneCheckerThread;-><init>(Lcom/sec/android/providers/tasks/TasksProvider;Lcom/sec/android/providers/tasks/TasksProvider$1;)V

    .line 221
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 222
    return-void
.end method


# virtual methods
.method public applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/OperationApplicationException;
        }
    .end annotation

    .prologue
    .line 1368
    .local p1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const-string v3, "TasksProvider"

    const-string v4, "TaskProvider applyBatch()"

    invoke-static {v3, v4}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1370
    iget-object v3, p0, Lcom/sec/android/providers/tasks/TasksProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1371
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1372
    const/4 v1, 0x0

    .line 1374
    .local v1, "results":[Landroid/content/ContentProviderResult;
    :try_start_0
    invoke-super {p0, p1}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v1

    .line 1375
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1377
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move-object v2, v1

    .line 1378
    .end local v1    # "results":[Landroid/content/ContentProviderResult;
    .local v2, "results":[Landroid/content/ContentProviderResult;
    :goto_0
    return-object v2

    .line 1377
    .end local v2    # "results":[Landroid/content/ContentProviderResult;
    .restart local v1    # "results":[Landroid/content/ContentProviderResult;
    :catchall_0
    move-exception v3

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move-object v2, v1

    .line 1378
    .end local v1    # "results":[Landroid/content/ContentProviderResult;
    .restart local v2    # "results":[Landroid/content/ContentProviderResult;
    goto :goto_0
.end method

.method bootCompleted()V
    .locals 0

    .prologue
    .line 1239
    return-void
.end method

.method protected bootstrapDatabase(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 466
    invoke-super {p0, p1}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->bootstrapDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 467
    const-string v0, "TasksProvider"

    const-string v1, "TaskProvider bootstrapDatabase()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    invoke-direct {p0, p1}, Lcom/sec/android/providers/tasks/TasksProvider;->createTasksTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 472
    const-string v0, "CREATE TABLE TasksAccounts (_id INTEGER PRIMARY KEY,_sync_account TEXT,_sync_account_key TEXT,_sync_account_type TEXT,url TEXT,selected INTEGER NOT NULL DEFAULT 1,displayName TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 484
    const-string v0, "CREATE TRIGGER tasks_cleanup before DELETE ON TasksAccounts BEGIN DELETE FROM tasks WHERE accountKey= old._sync_account_key;DELETE FROM UpdatedTasks WHERE accountKey= old._sync_account_key;DELETE FROM DeletedTasks WHERE accountKey= old._sync_account_key; END"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 581
    invoke-direct {p0, p1}, Lcom/sec/android/providers/tasks/TasksProvider;->createTasksView(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 582
    return-void
.end method

.method public deleteInternal(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 9
    .param p1, "url"    # Landroid/net/Uri;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "whereArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 823
    const-string v4, "TasksProvider"

    const-string v5, "TaskProvider deleteInternal()"

    invoke-static {v4, v5}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 824
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/TasksProvider;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 825
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v4, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 826
    .local v2, "match":I
    const-string v1, "0"

    .line 828
    .local v1, "id":Ljava/lang/String;
    const/4 v3, 0x0

    .line 829
    .local v3, "result":I
    const-string v4, "Inside deleteInternal of TaskProvider match"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "is"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    sparse-switch v2, :sswitch_data_0

    .line 889
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown URL "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 833
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/TasksProvider;->getSyncState()Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->asContentProvider()Landroid/content/ContentProvider;

    move-result-object v4

    invoke-virtual {v4, p1, p2, p3}, Landroid/content/ContentProvider;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 892
    :goto_0
    const-string v4, " This is the result after delete of this "

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    return v3

    .line 837
    :sswitch_1
    const-string v4, "SANDEEP"

    const-string v5, "In synced Task id"

    invoke-static {v4, v5}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 838
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "id":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 839
    .restart local v1    # "id":Ljava/lang/String;
    const/16 v4, 0x8

    if-ne v2, v4, :cond_0

    .line 843
    const-string v4, "SANDEEP"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "id is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 844
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert or ignore into DeletedTasks select * from tasks where _id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 845
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "delete from UpdatedTasks where _id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 847
    :cond_0
    if-eqz p2, :cond_1

    .line 848
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TasksProvider doesn\'t support where based deletion for type "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 852
    :cond_1
    const-string v4, "Tasks"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 853
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "delete from TasksReminders where task_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 857
    :sswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "id":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 858
    .restart local v1    # "id":Ljava/lang/String;
    const-string v4, "Inside deleteInternal of TaskProvider match is Updated_tasks_id"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "id is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    const-string v4, "Deleting the record in updated table"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "id is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    const-string v4, "UpdatedTasks"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 864
    goto/16 :goto_0

    .line 866
    :sswitch_3
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "id":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 867
    .restart local v1    # "id":Ljava/lang/String;
    const-string v4, "Inside deleteInternal of TaskProvider match is Deleted_tasks_id"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "id is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 869
    const-string v4, "Deleting the record in deleted table"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "id is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "delete from DeletedTasks where _id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 871
    const-string v4, "Tasks"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 873
    goto/16 :goto_0

    .line 875
    :sswitch_4
    const-string v4, "Inside deleteInternal of TaskProvider match is Deleted Reminder _tasks_id"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "id is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    const-string v4, "TasksReminders"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "task_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 879
    goto/16 :goto_0

    .line 882
    :sswitch_5
    const-string v4, "tasks"

    invoke-virtual {v0, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 883
    const-string v4, "UpdatedTasks"

    invoke-virtual {v0, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 884
    const-string v4, "DeletedTasks"

    invoke-virtual {v0, v4, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 885
    goto/16 :goto_0

    .line 830
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_1
        0x6 -> :sswitch_2
        0x8 -> :sswitch_1
        0xa -> :sswitch_3
        0xc -> :sswitch_4
        0x12 -> :sswitch_0
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1, "url"    # Landroid/net/Uri;

    .prologue
    .line 713
    const-string v1, "TasksProvider"

    const-string v2, "TaskProvider getType()"

    invoke-static {v1, v2}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    sget-object v1, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 715
    .local v0, "match":I
    packed-switch v0, :pswitch_data_0

    .line 746
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown URL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 717
    :pswitch_0
    const-string v1, "vnd.android.cursor.dir/task"

    .line 743
    :goto_0
    return-object v1

    .line 719
    :pswitch_1
    const-string v1, "vnd.android.cursor.item/task"

    goto :goto_0

    .line 721
    :pswitch_2
    const-string v1, "vnd.android.cursor.dir/task"

    goto :goto_0

    .line 723
    :pswitch_3
    const-string v1, "vnd.android.cursor.item/task"

    goto :goto_0

    .line 725
    :pswitch_4
    const-string v1, "vnd.android.cursor.dir/task"

    goto :goto_0

    .line 727
    :pswitch_5
    const-string v1, "vnd.android.cursor.item/task"

    goto :goto_0

    .line 729
    :pswitch_6
    const-string v1, "vnd.android.cursor.dir/task"

    goto :goto_0

    .line 731
    :pswitch_7
    const-string v1, "vnd.android.cursor.item/task"

    goto :goto_0

    .line 733
    :pswitch_8
    const-string v1, "vnd.android.cursor.dir/task"

    goto :goto_0

    .line 735
    :pswitch_9
    const-string v1, "vnd.android.cursor.item/task"

    goto :goto_0

    .line 737
    :pswitch_a
    const-string v1, "vnd.android.cursor.dir/task"

    goto :goto_0

    .line 739
    :pswitch_b
    const-string v1, "vnd.android.cursor.item/task"

    goto :goto_0

    .line 741
    :pswitch_c
    const-string v1, "vnd.android.cursor.dir/task"

    goto :goto_0

    .line 743
    :pswitch_d
    const-string v1, "vnd.android.cursor.item/task"

    goto :goto_0

    .line 715
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public insertInternal(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 10
    .param p1, "url"    # Landroid/net/Uri;
    .param p2, "initialValues"    # Landroid/content/ContentValues;

    .prologue
    .line 752
    const-string v7, "TasksProvider"

    const-string v8, "TaskProvider insertInternal()"

    invoke-static {v7, v8}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 753
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/TasksProvider;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 756
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "0"

    .line 757
    .local v2, "id":Ljava/lang/String;
    sget-object v7, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v7, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    .line 758
    .local v3, "match":I
    const-string v7, "SANDEEP"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Insert internal : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    sparse-switch v3, :sswitch_data_0

    .line 798
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unknown URL "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 761
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/TasksProvider;->getSyncState()Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->asContentProvider()Landroid/content/ContentProvider;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Landroid/content/ContentProvider;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v6

    .line 796
    :goto_0
    return-object v6

    .line 766
    :sswitch_1
    const/4 v7, 0x7

    if-ne v3, v7, :cond_0

    .line 767
    const-string v7, "_sync_dirty"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {p2, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 768
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "new_task_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 769
    .local v0, "clientId":Ljava/lang/String;
    const-string v7, "clientId"

    invoke-virtual {p2, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    .end local v0    # "clientId":Ljava/lang/String;
    :cond_0
    iget-object v7, p0, Lcom/sec/android/providers/tasks/TasksProvider;->mTasksInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v7, p2}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 779
    .local v4, "rowID":J
    const-string v7, "SANDEEP"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Authority is: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "content://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/tasks/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 781
    .local v6, "uri":Landroid/net/Uri;
    goto :goto_0

    .line 784
    .end local v4    # "rowID":J
    .end local v6    # "uri":Landroid/net/Uri;
    :sswitch_2
    iget-object v7, p0, Lcom/sec/android/providers/tasks/TasksProvider;->mTasksReminderInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v7, p2}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 785
    .restart local v4    # "rowID":J
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "content://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/TasksReminders/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 786
    .restart local v6    # "uri":Landroid/net/Uri;
    const-string v7, "TasksProvider"

    const-string v8, "Inside insertInternal going to call schedule reminder"

    invoke-static {v7, v8}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    invoke-direct {p0, p2}, Lcom/sec/android/providers/tasks/TasksProvider;->scheduleReminder(Landroid/content/ContentValues;)V

    goto/16 :goto_0

    .line 794
    .end local v4    # "rowID":J
    .end local v6    # "uri":Landroid/net/Uri;
    :sswitch_3
    iget-object v7, p0, Lcom/sec/android/providers/tasks/TasksProvider;->mTasksAccountsInserter:Landroid/database/DatabaseUtils$InsertHelper;

    invoke-virtual {v7, p2}, Landroid/database/DatabaseUtils$InsertHelper;->insert(Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 795
    .restart local v4    # "rowID":J
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "content://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/TasksAccounts/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 796
    .restart local v6    # "uri":Landroid/net/Uri;
    goto/16 :goto_0

    .line 759
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x7 -> :sswitch_1
        0xb -> :sswitch_2
        0xd -> :sswitch_3
        0x12 -> :sswitch_0
    .end sparse-switch
.end method

.method public onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 15
    .param p1, "accounts"    # [Landroid/accounts/Account;

    .prologue
    .line 1177
    const-string v12, "TasksProvider"

    const-string v13, "TaskProvider onAccountsUpdated()"

    invoke-static {v12, v13}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1178
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/TasksProvider;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    .line 1180
    .local v8, "mDb":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1181
    .local v1, "accountHasTasks":Ljava/util/HashMap;, "Ljava/util/HashMap<Landroid/accounts/Account;Ljava/lang/Boolean;>;"
    new-instance v11, Ljava/util/HashSet;

    invoke-direct {v11}, Ljava/util/HashSet;-><init>()V

    .line 1182
    .local v11, "validAccounts":Ljava/util/HashSet;, "Ljava/util/HashSet<Landroid/accounts/Account;>;"
    move-object/from16 v3, p1

    .local v3, "arr$":[Landroid/accounts/Account;
    array-length v7, v3

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v0, v3, v6

    .line 1183
    .local v0, "account":Landroid/accounts/Account;
    new-instance v12, Landroid/accounts/Account;

    iget-object v13, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v14, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-direct {v12, v13, v14}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v12}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1184
    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v1, v0, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1182
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1186
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1188
    .local v2, "accountsToDelete":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/accounts/Account;>;"
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1191
    const/4 v12, 0x1

    :try_start_0
    new-array v3, v12, [Ljava/lang/String;

    .end local v3    # "arr$":[Landroid/accounts/Account;
    const/4 v12, 0x0

    const-string v13, "TasksAccounts"

    aput-object v13, v3, v12

    .local v3, "arr$":[Ljava/lang/String;
    array-length v7, v3

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v7, :cond_3

    aget-object v10, v3, v6

    .line 1196
    .local v10, "table":Ljava/lang/String;
    const-string v12, "TasksProvider"

    const-string v13, "Querying for the tasks accounts"

    invoke-static {v12, v13}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1197
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "SELECT DISTINCT _sync_account,_sync_account_type from "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v8, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 1200
    .local v4, "c":Landroid/database/Cursor;
    :cond_1
    :goto_2
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1201
    const/4 v12, 0x0

    invoke-interface {v4, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_1

    const/4 v12, 0x1

    invoke-interface {v4, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_1

    .line 1202
    new-instance v5, Landroid/accounts/Account;

    const/4 v12, 0x0

    invoke-interface {v4, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    invoke-interface {v4, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v5, v12, v13}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1203
    .local v5, "currAccount":Landroid/accounts/Account;
    invoke-virtual {v11, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 1204
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 1225
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v4    # "c":Landroid/database/Cursor;
    .end local v5    # "currAccount":Landroid/accounts/Account;
    .end local v6    # "i$":I
    .end local v10    # "table":Ljava/lang/String;
    :catchall_0
    move-exception v12

    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v12

    .line 1208
    .restart local v3    # "arr$":[Ljava/lang/String;
    .restart local v4    # "c":Landroid/database/Cursor;
    .restart local v6    # "i$":I
    .restart local v10    # "table":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1191
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1211
    .end local v4    # "c":Landroid/database/Cursor;
    .end local v10    # "table":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 1212
    .restart local v0    # "account":Landroid/accounts/Account;
    iget-object v12, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v13, "local"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 1214
    const/4 v12, 0x2

    new-array v9, v12, [Ljava/lang/String;

    const/4 v12, 0x0

    iget-object v13, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v13, v9, v12

    const/4 v12, 0x1

    iget-object v13, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v13, v9, v12

    .line 1217
    .local v9, "params":[Ljava/lang/String;
    const-string v12, "DELETE FROM TasksAccounts WHERE _sync_account= ? AND _sync_account_type= ?"

    invoke-virtual {v8, v12, v9}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 1223
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v9    # "params":[Ljava/lang/String;
    :cond_5
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1225
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1227
    return-void
.end method

.method public onCreate()Z
    .locals 4

    .prologue
    .line 184
    invoke-super {p0}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->onCreate()Z

    .line 185
    const-string v1, "SANDEEP"

    const-string v2, "In on create"

    invoke-static {v1, v2}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    const-string v1, "TasksProvider"

    const-string v2, "TaskProvider onCreate()"

    invoke-static {v1, v2}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/TasksProvider;->verifyAccounts()V

    .line 189
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "( 1, \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/providers/tasks/TasksProvider;->ACCOUNT_MYTASK_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\', \'0\', \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "local"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\', \'\', 1, \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/providers/tasks/TasksProvider;->ACCOUNT_MYTASK_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 191
    .local v0, "values":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/providers/tasks/TasksProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/sec/android/providers/tasks/TasksProvider;->mIsinit:Z

    if-eqz v1, :cond_0

    .line 192
    iget-object v1, p0, Lcom/sec/android/providers/tasks/TasksProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "INSERT OR IGNORE INTO TasksAccounts Values "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 193
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/providers/tasks/TasksProvider;->mIsinit:Z

    .line 211
    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method protected onDatabaseOpened(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 246
    const-string v0, "TasksProvider"

    const-string v1, "TaskProvider onDatabaseOpened()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iput-object p1, p0, Lcom/sec/android/providers/tasks/TasksProvider;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 249
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "Tasks"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/providers/tasks/TasksProvider;->mTasksInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 250
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "DeletedTasks"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/providers/tasks/TasksProvider;->mDeletedTasksInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 251
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "UpdatedTasks"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/providers/tasks/TasksProvider;->mUpdatedTasksInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 252
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "TasksReminders"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/providers/tasks/TasksProvider;->mTasksReminderInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 253
    new-instance v0, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v1, "TasksAccounts"

    invoke-direct {v0, p1, v1}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/providers/tasks/TasksProvider;->mTasksAccountsInserter:Landroid/database/DatabaseUtils$InsertHelper;

    .line 254
    return-void
.end method

.method public onSyncStop(Landroid/content/SyncContext;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/SyncContext;
    .param p2, "success"    # Z

    .prologue
    .line 1167
    invoke-super {p0, p1, p2}, Lcom/sec/android/providers/tasks/AbstractSyncableContentProvider;->onSyncStop(Landroid/content/SyncContext;Z)V

    .line 1168
    const-string v0, "TasksProvider"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1169
    const-string v0, "TasksProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSyncStop() success: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1172
    :cond_0
    return-void
.end method

.method public queryEntities(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/content/EntityIterator;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 1345
    const-string v2, "TasksProvider"

    const-string v3, "TaskProvider queryEntities()"

    invoke-static {v2, v3}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1346
    sget-object v2, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 1347
    .local v0, "match":I
    packed-switch v0, :pswitch_data_0

    .line 1361
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown uri: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1350
    :pswitch_0
    const/4 v1, 0x0

    .line 1351
    .local v1, "taskId":Ljava/lang/String;
    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    .line 1352
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "taskId":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 1359
    .restart local v1    # "taskId":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x0

    return-object v2

    .line 1347
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public queryInternal(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 19
    .param p1, "url"    # Landroid/net/Uri;
    .param p2, "projectionIn"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sort"    # Ljava/lang/String;

    .prologue
    .line 587
    const-string v5, "TasksProvider"

    const-string v6, "TaskProvider queryInternal()"

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    const-string v5, "SANDEEP"

    const-string v6, "In query internal"

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/providers/tasks/TasksProvider;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 590
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 593
    .local v1, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v5, "Inside queryInternal match is"

    const-string v6, "=0"

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    sget-object v5, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v14

    .line 596
    .local v14, "match":I
    const-string v5, "SANDEEP"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "match11 is: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    const-string v5, "Inside queryInternal match is"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    packed-switch v14, :pswitch_data_0

    .line 698
    :pswitch_0
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown URL "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 602
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/providers/tasks/TasksProvider;->getSyncState()Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->asContentProvider()Landroid/content/ContentProvider;

    move-result-object v1

    .end local v1    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .end local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    move-result-object v16

    .line 708
    :goto_0
    return-object v16

    .line 607
    .restart local v1    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v2    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :pswitch_2
    const-string v5, "view_tasks"

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 700
    :goto_1
    const-string v5, "TasksProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "projectionIn"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    const-string v5, "TasksProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "selection"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    const-string v5, "TasksProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "selectionArgs"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    const-string v5, "TasksProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sort"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p5

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v5, v1

    move-object v6, v2

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v12, p5

    invoke-virtual/range {v5 .. v12}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 707
    .local v16, "ret":Landroid/database/Cursor;
    const-string v5, "SANDEEP"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ret size is:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 611
    .end local v16    # "ret":Landroid/database/Cursor;
    :pswitch_3
    const-string v5, "view_tasks"

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 612
    const-string v5, "_id="

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 613
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 617
    :pswitch_4
    const-string v5, "view_tasks"

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 620
    :pswitch_5
    const-string v5, "UpdatedTasks"

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 623
    :pswitch_6
    const-string v5, "DeletedTasks"

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 626
    :pswitch_7
    const-string v5, "SANDEEP"

    const-string v6, "In task account case"

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    const-string v5, "TasksAccounts"

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 631
    :pswitch_8
    const-string v5, "TasksReminders"

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 636
    :pswitch_9
    const-string v5, "view_tasks"

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 637
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v17

    .line 639
    .local v17, "searchString":Ljava/lang/String;
    const-string v5, "\'"

    const-string v6, "\'\'"

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 640
    const-string v5, "_"

    const-string v6, "`_"

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 641
    const-string v5, "%"

    const-string v6, "`%"

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 646
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "( subject LIKE \'%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%\' escape \'`\' "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 648
    .local v18, "where":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "OR body LIKE \'%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%\' escape \'`\' "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 649
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 651
    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 657
    .end local v17    # "searchString":Ljava/lang/String;
    .end local v18    # "where":Ljava/lang/String;
    :pswitch_a
    const-string v5, "TasksProvider"

    const-string v6, "SEARCH_SUGGEST"

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    const/4 v5, 0x0

    aget-object v5, p4, v5

    if-eqz v5, :cond_0

    const/4 v5, 0x0

    aget-object v5, p4, v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 660
    :cond_0
    const/16 v16, 0x0

    goto/16 :goto_0

    .line 664
    :cond_1
    const-string v5, "view_tasks"

    invoke-virtual {v1, v5}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 665
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/providers/tasks/TasksProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f030002

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 667
    .local v15, "noSubject":Ljava/lang/String;
    const/4 v5, 0x3

    new-array v3, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id AS _id"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "(CASE WHEN subject IS null THEN \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " WHEN subject=\'\' THEN \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ELSE subject END) AS "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "suggest_text_1"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-string v6, "_id AS suggest_intent_data_id"

    aput-object v6, v3, v5

    .line 675
    .local v3, "SUGGESTION_COLUMNS":[Ljava/lang/String;
    const-string v5, "TasksProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "projectionIn = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    const-string v5, "TasksProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "selection = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    move-object/from16 v0, p4

    array-length v5, v0

    if-ge v13, v5, :cond_2

    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 680
    :cond_2
    const-string v5, "TasksProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sort"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p5

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    const/4 v5, 0x0

    const/4 v6, 0x0

    aget-object v6, p4, v6

    const-string v7, "\'"

    const-string v8, "\'\'"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, p4, v5

    .line 685
    const/4 v5, 0x0

    const/4 v6, 0x0

    aget-object v6, p4, v6

    const-string v7, "_"

    const-string v8, "`_"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, p4, v5

    .line 686
    const/4 v5, 0x0

    const/4 v6, 0x0

    aget-object v6, p4, v6

    const-string v7, "%"

    const-string v8, "`%"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, p4, v5

    .line 687
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "( subject LIKE \'%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v6, p4, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%\' escape \'`\' "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 689
    .local v4, "sel":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "OR body LIKE \'%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v6, p4, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%\' escape \'`\' "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 690
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 693
    const/4 v5, 0x0

    const-string v6, "_id"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 695
    .restart local v16    # "ret":Landroid/database/Cursor;
    goto/16 :goto_0

    .line 599
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public updateInternal(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 10
    .param p1, "url"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "where"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 899
    sget-object v6, Lcom/sec/android/providers/tasks/TasksProvider;->sURLMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v6, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    .line 900
    .local v4, "match":I
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 901
    .local v2, "id":J
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 902
    .local v0, "date":Ljava/util/Date;
    const/4 v5, 0x0

    .line 904
    .local v5, "result":I
    const-string v6, "TasksProvider"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Entering updateInternal(), match ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " id ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 906
    const/16 v6, 0x12

    if-ne v6, v4, :cond_0

    .line 907
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/TasksProvider;->getSyncState()Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/providers/tasks/SyncStateContentProviderHelper;->asContentProvider()Landroid/content/ContentProvider;

    move-result-object v6

    invoke-virtual {v6, p1, p2, p3, p4}, Landroid/content/ContentProvider;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    .line 951
    :goto_0
    return v6

    .line 910
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/TasksProvider;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 912
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    sparse-switch v4, :sswitch_data_0

    .line 940
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unknown URL "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 914
    :sswitch_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "delete from UpdatedTasks where _id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 916
    const-string v6, "Tasks"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, p2, v7, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 949
    :goto_1
    invoke-direct {p0, v1, v2, v3, p2}, Lcom/sec/android/providers/tasks/TasksProvider;->processReminderSettings(Landroid/database/sqlite/SQLiteDatabase;JLandroid/content/ContentValues;)V

    move v6, v5

    .line 951
    goto :goto_0

    .line 920
    :sswitch_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "delete from UpdatedTasks where _id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 921
    const-string v6, "Tasks"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, p2, v7, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 922
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insert or ignore into UpdatedTasks select * from tasks where _id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_1

    .line 926
    :sswitch_2
    const-string v6, "Tasks"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, p2, v7, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 927
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insert or ignore into UpdatedTasks select * from tasks where _id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_1

    .line 931
    :sswitch_3
    const-string v6, "TasksReminders"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, p2, v7, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 932
    goto/16 :goto_1

    .line 935
    :sswitch_4
    const-string v6, "TasksAccounts"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, p2, v7, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 936
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insert or ignore into UpdatedTasks select * from tasks where _id="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 912
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x6 -> :sswitch_2
        0x8 -> :sswitch_1
        0xc -> :sswitch_3
        0xe -> :sswitch_4
    .end sparse-switch
.end method

.method protected upgradeDatabase(Landroid/database/sqlite/SQLiteDatabase;II)Z
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    const/4 v0, 0x1

    .line 258
    const-string v1, "TasksProvider"

    const-string v2, "TaskProvider upgradeDatabase()"

    invoke-static {v1, v2}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    const-string v1, "TasksProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Upgrading DB from version "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/providers/tasks/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    if-ge p2, v0, :cond_0

    .line 262
    invoke-direct {p0, p1}, Lcom/sec/android/providers/tasks/TasksProvider;->dropTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 263
    invoke-virtual {p0, p1}, Lcom/sec/android/providers/tasks/TasksProvider;->bootstrapDatabase(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 264
    const/4 v0, 0x0

    .line 266
    :cond_0
    return v0
.end method

.method protected verifyAccounts()V
    .locals 3

    .prologue
    .line 177
    const-string v0, "TasksProvider"

    const-string v1, "TaskProvider verifyAccounts()"

    invoke-static {v0, v1}, Lcom/sec/android/providers/tasks/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/TasksProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 179
    invoke-virtual {p0}, Lcom/sec/android/providers/tasks/TasksProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/providers/tasks/TasksProvider;->onAccountsUpdated([Landroid/accounts/Account;)V

    .line 180
    return-void
.end method
