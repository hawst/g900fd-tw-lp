.class public abstract Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;
.super Lcom/sec/android/providers/tasks/SyncAdapter;
.source "TempProviderSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;,
        Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;
    }
.end annotation


# static fields
.field private static final MAX_GET_SERVER_DIFFS_LOOP_COUNT:I = 0x14

.field private static final MAX_UPLOAD_CHANGES_LOOP_COUNT:I = 0xa

.field private static final NUM_ALLOWED_SIMULTANEOUS_DELETIONS:I = 0x5

.field private static final PERCENT_ALLOWED_SIMULTANEOUS_DELETIONS:J = 0x14L

.field private static final TAG:Ljava/lang/String; = "Sync"


# instance fields
.field private volatile mAdapterSyncStarted:Z

.field private mContext:Landroid/content/Context;

.field private volatile mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

.field private volatile mProviderSyncStarted:Z

.field private volatile mSyncThread:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;


# direct methods
.method public constructor <init>(Lcom/sec/android/providers/tasks/SyncableContentProvider;)V
    .locals 1
    .param p1, "provider"    # Lcom/sec/android/providers/tasks/SyncableContentProvider;

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sec/android/providers/tasks/SyncAdapter;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mSyncThread:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;

    .line 64
    iput-object p1, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mAdapterSyncStarted:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mAdapterSyncStarted:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProviderSyncStarted:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProviderSyncStarted:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;)Lcom/sec/android/providers/tasks/SyncableContentProvider;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mProvider:Lcom/sec/android/providers/tasks/SyncableContentProvider;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;)Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;
    .param p1, "x1"    # Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mSyncThread:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;

    return-object p1
.end method


# virtual methods
.method public cancelSync()V
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mSyncThread:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;

    if-eqz v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mSyncThread:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;

    invoke-virtual {v0}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->cancelSync()V

    .line 595
    :cond_0
    return-void
.end method

.method protected createSyncInfo()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public abstract getIsSyncable(Landroid/accounts/Account;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/accounts/AuthenticatorException;,
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation
.end method

.method public abstract getServerDiffs(Landroid/content/SyncContext;Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;Lcom/sec/android/providers/tasks/SyncableContentProvider;Landroid/os/Bundle;Ljava/lang/Object;Landroid/content/SyncResult;)V
.end method

.method protected hasTooManyDeletions(Landroid/content/SyncStats;)Z
    .locals 12
    .param p1, "stats"    # Landroid/content/SyncStats;

    .prologue
    const-wide/16 v4, 0x0

    .line 598
    iget-wide v2, p1, Landroid/content/SyncStats;->numEntries:J

    .line 599
    .local v2, "numEntries":J
    iget-wide v0, p1, Landroid/content/SyncStats;->numDeletes:J

    .line 601
    .local v0, "numDeletedEntries":J
    cmp-long v7, v0, v4

    if-nez v7, :cond_0

    .line 605
    .local v4, "percentDeleted":J
    :goto_0
    const-wide/16 v8, 0x5

    cmp-long v7, v0, v8

    if-lez v7, :cond_1

    const-wide/16 v8, 0x14

    cmp-long v7, v4, v8

    if-lez v7, :cond_1

    const/4 v6, 0x1

    .line 608
    .local v6, "tooManyDeletions":Z
    :goto_1
    return v6

    .line 601
    .end local v4    # "percentDeleted":J
    .end local v6    # "tooManyDeletions":Z
    :cond_0
    const-wide/16 v8, 0x64

    mul-long/2addr v8, v0

    add-long v10, v2, v0

    div-long v4, v8, v10

    goto :goto_0

    .line 605
    .restart local v4    # "percentDeleted":J
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method protected initTempProvider(Lcom/sec/android/providers/tasks/SyncableContentProvider;)V
    .locals 0
    .param p1, "cp"    # Lcom/sec/android/providers/tasks/SyncableContentProvider;

    .prologue
    .line 189
    return-void
.end method

.method public abstract isReadOnly()Z
.end method

.method protected logSyncDetails(JJLandroid/content/SyncResult;)V
    .locals 5
    .param p1, "bytesSent"    # J
    .param p3, "bytesReceived"    # J
    .param p5, "result"    # Landroid/content/SyncResult;

    .prologue
    .line 577
    const/16 v0, 0xab7

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "Sync"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, ""

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 578
    return-void
.end method

.method public newSyncData()Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract onAccountsChanged([Landroid/accounts/Account;)V
.end method

.method public abstract onSyncCanceled()V
.end method

.method public abstract onSyncEnding(Landroid/content/SyncContext;Z)V
.end method

.method public abstract onSyncStarting(Landroid/content/SyncContext;Landroid/accounts/Account;ZLandroid/content/SyncResult;)V
.end method

.method public readSyncData(Lcom/sec/android/providers/tasks/SyncableContentProvider;)Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;
    .locals 1
    .param p1, "contentProvider"    # Lcom/sec/android/providers/tasks/SyncableContentProvider;

    .prologue
    .line 159
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract sendClientDiffs(Landroid/content/SyncContext;Lcom/sec/android/providers/tasks/SyncableContentProvider;Lcom/sec/android/providers/tasks/SyncableContentProvider;Landroid/content/SyncResult;Z)V
.end method

.method public final setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mContext:Landroid/content/Context;

    .line 80
    return-void
.end method

.method public startSync(Landroid/content/SyncContext;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "syncContext"    # Landroid/content/SyncContext;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 582
    iget-object v0, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mSyncThread:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;

    if-eqz v0, :cond_0

    .line 583
    sget-object v0, Landroid/content/SyncResult;->ALREADY_IN_PROGRESS:Landroid/content/SyncResult;

    invoke-virtual {p1, v0}, Landroid/content/SyncContext;->onFinished(Landroid/content/SyncResult;)V

    .line 589
    :goto_0
    return-void

    .line 587
    :cond_0
    new-instance v0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;-><init>(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;Landroid/content/SyncContext;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mSyncThread:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;

    .line 588
    iget-object v0, p0, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter;->mSyncThread:Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;

    invoke-virtual {v0}, Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncThread;->start()V

    goto :goto_0
.end method

.method public writeSyncData(Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;Lcom/sec/android/providers/tasks/SyncableContentProvider;)V
    .locals 0
    .param p1, "syncData"    # Lcom/sec/android/providers/tasks/TempProviderSyncAdapter$SyncData;
    .param p2, "contentProvider"    # Lcom/sec/android/providers/tasks/SyncableContentProvider;

    .prologue
    .line 173
    return-void
.end method
