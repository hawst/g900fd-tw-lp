.class Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;
.super Landroid/os/Handler;
.source "ResultApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/panorama360view/ResultApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    .line 220
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x1

    .line 223
    iget v0, p1, Landroid/os/Message;->arg1:I

    packed-switch v0, :pswitch_data_0

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 225
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;
    invoke-static {v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$2(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->setRenderEnable(Z)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$3(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Landroid/opengl/GLSurfaceView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 227
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSavePanoramaImagesThread:Ljava/lang/Thread;
    invoke-static {v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$4(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Ljava/lang/Thread;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSavePanoramaImagesThread:Ljava/lang/Thread;
    invoke-static {v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$4(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    if-ne v0, v1, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSavePanoramaImagesThread:Ljava/lang/Thread;
    invoke-static {v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$4(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 232
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;
    invoke-static {v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$2(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->setRenderEnable(Z)V

    .line 233
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;
    invoke-static {v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$3(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Landroid/opengl/GLSurfaceView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 234
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    const/4 v1, 0x3

    # invokes: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->dismissDialogFragment(I)V
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$5(Lcom/sec/android/gallery3d/panorama360view/ResultApp;I)V

    .line 237
    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$6()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Time to be displayed : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # invokes: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getCurTime()J
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$7(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mLoadingTime:J
    invoke-static {v4}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$8(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 240
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # invokes: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->showDialogFragment(I)V
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$9(Lcom/sec/android/gallery3d/panorama360view/ResultApp;I)V

    goto :goto_0

    .line 243
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    const/4 v1, 0x0

    # invokes: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->showDialogFragment(I)V
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$9(Lcom/sec/android/gallery3d/panorama360view/ResultApp;I)V

    goto :goto_0

    .line 246
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # invokes: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->createGLSurface()V
    invoke-static {v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$10(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V

    goto/16 :goto_0

    .line 249
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    const/4 v1, 0x4

    # invokes: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->showDialogFragment(I)V
    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$9(Lcom/sec/android/gallery3d/panorama360view/ResultApp;I)V

    goto/16 :goto_0

    .line 223
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
