.class Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;
.super Ljava/lang/Object;
.source "ResultApp.java"

# interfaces
.implements Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer$PanoramaTimerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/panorama360view/ResultApp;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    .line 522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeout()V
    .locals 10

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const v8, 0x3d99999a    # 0.075f

    const/4 v5, 0x0

    const-wide v6, 0x3f9eb851eb851eb8L    # 0.03

    .line 525
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffX:F
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$26(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffX:F
    invoke-static {v4}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$26(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)F

    move-result v4

    mul-float/2addr v4, v8

    sub-float/2addr v3, v4

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$27(Lcom/sec/android/gallery3d/panorama360view/ResultApp;F)V

    .line 526
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffY:F
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$28(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffY:F
    invoke-static {v4}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$28(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)F

    move-result v4

    mul-float/2addr v4, v8

    sub-float/2addr v3, v4

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$29(Lcom/sec/android/gallery3d/panorama360view/ResultApp;F)V

    .line 528
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffX:F
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$26(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 529
    .local v0, "value_x":F
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffY:F
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$28(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 530
    .local v1, "value_y":F
    cmpg-float v2, v0, v9

    if-gez v2, :cond_0

    cmpg-float v2, v1, v9

    if-gez v2, :cond_0

    .line 531
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    invoke-static {v2, v5}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$27(Lcom/sec/android/gallery3d/panorama360view/ResultApp;F)V

    .line 532
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    invoke-static {v2, v5}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$29(Lcom/sec/android/gallery3d/panorama360view/ResultApp;F)V

    .line 539
    :goto_0
    return-void

    .line 535
    :cond_0
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$2(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffX:F
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$26(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)F

    move-result v3

    float-to-double v3, v3

    mul-double/2addr v3, v6

    double-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffY:F
    invoke-static {v4}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$28(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)F

    move-result v4

    float-to-double v4, v4

    mul-double/2addr v4, v6

    double-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->setSwipeAngle(FF)V

    .line 536
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$3(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Landroid/opengl/GLSurfaceView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 537
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mInertiaScrollTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$30(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    move-result-object v2

    const-wide/16 v3, 0x14

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->start(J)V

    goto :goto_0
.end method
