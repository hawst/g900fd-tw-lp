.class public Lcom/sec/android/gallery3d/panorama360view/drm/SphereUtils;
.super Ljava/lang/Object;
.source "SphereUtils.java"


# static fields
.field private static final META_DATA_FORMAT:Ljava/lang/String; = "http://ns.adobe.com/xap/1.0/\u0000<x:xmpmeta xmlns:x=\"adobe:ns:meta/\" x:xmptk=\"Adobe XMP Core 5.1.0-jc003\">\n  <rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n    <rdf:Description rdf:about=\"\"\n        xmlns:GPano=\"http://ns.google.com/photos/1.0/panorama/\"\n      GPano:UsePanoramaViewer=\"True\"\n      GPano:ProjectionType=\"equirectangular\"\n      GPano:PoseHeadingDegrees=\"31.0\"\n      GPano:CroppedAreaLeftPixels=\"0\"\n      GPano:FullPanoWidthPixels=\"%d\"\n      GPano:FirstPhotoDate=\"2012-12-03T04:13:21.228Z\"\n      GPano:CroppedAreaImageHeightPixels=\"%d\"\n      GPano:FullPanoHeightPixels=\"%d\"\n      GPano:SourcePhotosCount=\"50\"\n      GPano:CroppedAreaImageWidthPixels=\"%d\"\n      GPano:LastPhotoDate=\"2012-12-03T04:16:10.048Z\"\n      GPano:CroppedAreaTopPixels=\"0\"\n      GPano:LargestValidInteriorRectLeft=\"0\"\n      GPano:LargestValidInteriorRectTop=\"0\"\n      GPano:LargestValidInteriorRectWidth=\"%d\"\n      GPano:LargestValidInteriorRectHeight=\"%d\"/>\n  </rdf:RDF>\n</x:xmpmeta>\n"

.field public static final useAddSphereTAG:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addSphereMeta([BII)[B
    .locals 11
    .param p0, "buffer"    # [B
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v10, 0x0

    const/16 v9, -0x1f

    .line 37
    const/4 v3, 0x4

    .line 38
    .local v3, "offset":I
    invoke-static {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/drm/SphereUtils;->toShort2([BI)I

    move-result v6

    add-int/2addr v3, v6

    .line 39
    move v5, v3

    .line 40
    .local v5, "startoffset":I
    invoke-static {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/drm/SphereUtils;->toShort([BI)S

    move-result v2

    .line 41
    .local v2, "jfifmarker":S
    const-string v6, "Test"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "jfifmarker : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    if-eq v2, v9, :cond_0

    .line 43
    const-string v6, "Test"

    const-string v7, "jfifmarker != 0xffe1"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 45
    .local v4, "outputStream":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v4, p0, v10, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 46
    const-string v6, "http://ns.adobe.com/xap/1.0/\u0000<x:xmpmeta xmlns:x=\"adobe:ns:meta/\" x:xmptk=\"Adobe XMP Core 5.1.0-jc003\">\n  <rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n    <rdf:Description rdf:about=\"\"\n        xmlns:GPano=\"http://ns.google.com/photos/1.0/panorama/\"\n      GPano:UsePanoramaViewer=\"True\"\n      GPano:ProjectionType=\"equirectangular\"\n      GPano:PoseHeadingDegrees=\"31.0\"\n      GPano:CroppedAreaLeftPixels=\"0\"\n      GPano:FullPanoWidthPixels=\"%d\"\n      GPano:FirstPhotoDate=\"2012-12-03T04:13:21.228Z\"\n      GPano:CroppedAreaImageHeightPixels=\"%d\"\n      GPano:FullPanoHeightPixels=\"%d\"\n      GPano:SourcePhotosCount=\"50\"\n      GPano:CroppedAreaImageWidthPixels=\"%d\"\n      GPano:LastPhotoDate=\"2012-12-03T04:16:10.048Z\"\n      GPano:CroppedAreaTopPixels=\"0\"\n      GPano:LargestValidInteriorRectLeft=\"0\"\n      GPano:LargestValidInteriorRectTop=\"0\"\n      GPano:LargestValidInteriorRectWidth=\"%d\"\n      GPano:LargestValidInteriorRectHeight=\"%d\"/>\n  </rdf:RDF>\n</x:xmpmeta>\n"

    const/4 v7, 0x6

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    const/4 v8, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 48
    .local v0, "data":[B
    const/16 v6, -0x1f

    :try_start_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/panorama360view/drm/SphereUtils;->toByte(S)[B

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 49
    array-length v6, v0

    add-int/lit8 v6, v6, 0x2

    int-to-short v6, v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/panorama360view/drm/SphereUtils;->toByte(S)[B

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 50
    add-int/lit8 v3, v3, 0x2

    .line 51
    invoke-virtual {v4, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 52
    array-length v6, p0

    sub-int/2addr v6, v5

    invoke-virtual {v4, p0, v5, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :goto_0
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    .line 60
    .end local v0    # "data":[B
    .end local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .end local p0    # "buffer":[B
    :cond_0
    return-object p0

    .line 53
    .restart local v0    # "data":[B
    .restart local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local p0    # "buffer":[B
    :catch_0
    move-exception v1

    .line 55
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static toByte(S)[B
    .locals 3
    .param p0, "value"    # S

    .prologue
    .line 64
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 65
    .local v0, "buffer":[B
    const/4 v1, 0x0

    shr-int/lit8 v2, p0, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 66
    const/4 v1, 0x1

    and-int/lit16 v2, p0, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 67
    return-object v0
.end method

.method public static toShort([BI)S
    .locals 3
    .param p0, "buffer"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 70
    add-int/lit8 v0, p1, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    aget-byte v1, p0, p1

    shl-int/lit8 v1, v1, 0x8

    const v2, 0xff00

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method

.method public static toShort2([BI)I
    .locals 3
    .param p0, "buffer"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 74
    add-int/lit8 v0, p1, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    aget-byte v1, p0, p1

    shl-int/lit8 v1, v1, 0x8

    const v2, 0xff00

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    return v0
.end method
