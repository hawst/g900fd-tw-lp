.class public interface abstract Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface;
.super Ljava/lang/Object;
.source "DrmInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface$UnpackCallback;
    }
.end annotation


# static fields
.field public static final StateAcuireLicense:Ljava/lang/String; = "acquireLicense"

.field public static final StateCheckValidStatus:Ljava/lang/String; = "checkValidStatus"

.field public static final StateUnpack:Ljava/lang/String; = "Unpack"


# virtual methods
.method public abstract acquireLicense(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract isDrmFile(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract isValidDrmFile(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract unpack(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface$UnpackCallback;)Z
.end method

.method public abstract unpack(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;Ljava/io/ByteArrayOutputStream;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
