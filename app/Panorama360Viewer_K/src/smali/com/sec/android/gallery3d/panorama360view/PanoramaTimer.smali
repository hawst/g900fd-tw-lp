.class public Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;
.super Ljava/lang/Object;
.source "PanoramaTimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer$PanoramaTimerListener;
    }
.end annotation


# instance fields
.field private isStarted:Z

.field private mListner:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer$PanoramaTimerListener;

.field private mLock:Ljava/lang/Object;

.field private mTimer:Ljava/util/Timer;

.field private mTimerTask:Ljava/util/TimerTask;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->isStarted:Z

    .line 15
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimer:Ljava/util/Timer;

    .line 16
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mLock:Ljava/lang/Object;

    .line 17
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isDeamon"    # Z

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->isStarted:Z

    .line 20
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0, p1}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimer:Ljava/util/Timer;

    .line 21
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mLock:Ljava/lang/Object;

    .line 22
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->taskCancel()V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;)Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer$PanoramaTimerListener;
    .locals 1

    .prologue
    .line 8
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mListner:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer$PanoramaTimerListener;

    return-object v0
.end method

.method private taskCancel()V
    .locals 2

    .prologue
    .line 51
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 52
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimerTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimerTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimerTask:Ljava/util/TimerTask;

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->isStarted:Z

    .line 51
    :cond_0
    monitor-exit v1

    .line 59
    return-void

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public cancel()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->taskCancel()V

    .line 48
    return-void
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->isStarted:Z

    return v0
.end method

.method public release()V
    .locals 2

    .prologue
    .line 62
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimerTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimerTask:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimerTask:Ljava/util/TimerTask;

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->isStarted:Z

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimer:Ljava/util/Timer;

    .line 62
    :cond_1
    monitor-exit v1

    .line 73
    return-void

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setTimerListener(Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer$PanoramaTimerListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer$PanoramaTimerListener;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mListner:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer$PanoramaTimerListener;

    .line 77
    return-void
.end method

.method public start(J)V
    .locals 3
    .param p1, "time"    # J

    .prologue
    .line 29
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 30
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimerTask:Ljava/util/TimerTask;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 31
    new-instance v0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer$1;-><init>(Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimerTask:Ljava/util/TimerTask;

    .line 40
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimer:Ljava/util/Timer;

    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->mTimerTask:Ljava/util/TimerTask;

    invoke-virtual {v0, v2, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->isStarted:Z

    .line 29
    :cond_0
    monitor-exit v1

    .line 44
    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
