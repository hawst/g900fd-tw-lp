.class Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;
.super Ljava/lang/Object;
.source "ResultApp.java"

# interfaces
.implements Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer$PanoramaTimerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/panorama360view/ResultApp;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    .line 494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeout()V
    .locals 9

    .prologue
    .line 497
    const/4 v1, 0x0

    .line 498
    .local v1, "rot_x":F
    const/4 v2, 0x0

    .line 499
    .local v2, "rot_y":F
    const/4 v0, 0x0

    .line 500
    .local v0, "is_first":Z
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorLockObj:Ljava/lang/Object;
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$18(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 501
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    iget-object v5, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorMat:[D
    invoke-static {v5}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$19(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)[D

    move-result-object v5

    # invokes: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getSensorAngle([D)V
    invoke-static {v3, v5}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$20(Lcom/sec/android/gallery3d/panorama360view/ResultApp;[D)V

    .line 502
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPrevSensorMat:[D
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$21(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)[D

    move-result-object v3

    if-nez v3, :cond_1

    .line 503
    const/4 v0, 0x1

    .line 504
    iget-object v5, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorMat:[D
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$19(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)[D

    move-result-object v3

    invoke-virtual {v3}, [D->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [D

    invoke-static {v5, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$22(Lcom/sec/android/gallery3d/panorama360view/ResultApp;[D)V

    .line 500
    :goto_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 513
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isUseSensorDriven:Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$24(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-nez v0, :cond_0

    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$2(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 514
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$2(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->setSwipeAngle(FF)V

    .line 515
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$3(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Landroid/opengl/GLSurfaceView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 517
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorDrivenTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$25(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    move-result-object v3

    const-wide/16 v4, 0x14

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->start(J)V

    .line 518
    return-void

    .line 506
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorAngle:[D
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$23(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)[D

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorMat:[D
    invoke-static {v5}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$19(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)[D

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPrevSensorMat:[D
    invoke-static {v6}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$21(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)[D

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/gallery3d/panorama360view/MathUtil;->getAngleDiff([D[D[D)V

    .line 507
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorMat:[D
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$19(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)[D

    move-result-object v3

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPrevSensorMat:[D
    invoke-static {v6}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$21(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)[D

    move-result-object v6

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorMat:[D
    invoke-static {v8}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$19(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)[D

    move-result-object v8

    array-length v8, v8

    invoke-static {v3, v5, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 508
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorAngle:[D
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$23(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)[D

    move-result-object v3

    const/4 v5, 0x2

    aget-wide v5, v3, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v5

    neg-double v5, v5

    double-to-float v1, v5

    .line 509
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorAngle:[D
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$23(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)[D

    move-result-object v3

    const/4 v5, 0x1

    aget-wide v5, v3, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v5

    neg-double v5, v5

    double-to-float v2, v5

    goto :goto_0

    .line 500
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method
