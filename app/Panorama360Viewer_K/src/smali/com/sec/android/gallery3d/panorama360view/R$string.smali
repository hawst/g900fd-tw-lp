.class public final Lcom/sec/android/gallery3d/panorama360view/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/panorama360view/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final ERROR_DIALOG_MSG_2:I = 0x7f060006

.field public static final Import:I = 0x7f060073

.field public static final My_Contacts:I = 0x7f0600bd

.field public static final Tilt_message_1:I = 0x7f06028a

.field public static final Tilt_message_2:I = 0x7f06028b

.field public static final Tilt_message_3:I = 0x7f06028c

.field public static final a_document_detected:I = 0x7f0600d0

.field public static final a_face_detected:I = 0x7f0600cd

.field public static final a_second:I = 0x7f060132

.field public static final accounts:I = 0x7f06024a

.field public static final accumulated_time:I = 0x7f0600fd

.field public static final action_bar_album_selected:I = 0x7f060181

.field public static final action_bar_text_label:I = 0x7f060192

.field public static final activity_not_found:I = 0x7f060035

.field public static final adapt_display:I = 0x7f0602c1

.field public static final adapt_display_message:I = 0x7f0602c2

.field public static final add:I = 0x7f0601dc

.field public static final add_account:I = 0x7f060085

.field public static final add_name:I = 0x7f0600a9

.field public static final add_note_on_back_of_image:I = 0x7f0602dd

.field public static final add_pictures:I = 0x7f06018c

.field public static final add_weather_tag:I = 0x7f060209

.field public static final added_to_favourites:I = 0x7f060275

.field public static final advanced:I = 0x7f060154

.field public static final advanced_edit:I = 0x7f060090

.field public static final air_browse_text:I = 0x7f060272

.field public static final album:I = 0x7f0601ad

.field public static final album_created:I = 0x7f0601b9

.field public static final album_number:I = 0x7f060193

.field public static final albums:I = 0x7f06007e

.field public static final alert:I = 0x7f060095

.field public static final alert_msg_duplicated_name:I = 0x7f060096

.field public static final alert_msg_invalid_name:I = 0x7f060097

.field public static final all:I = 0x7f0600a3

.field public static final all_items_will_be_deleted:I = 0x7f060194

.field public static final all_sim:I = 0x7f06017e

.field public static final allshare:I = 0x7f06011c

.field public static final allshare_contents_location_is_not_available:I = 0x7f06011e

.field public static final allshare_device_is_added:I = 0x7f0602c9

.field public static final allshare_device_is_removed:I = 0x7f0602ca

.field public static final allshare_hide_nearby_devices:I = 0x7f060128

.field public static final allshare_nearby_devices:I = 0x7f06011d

.field public static final allshare_nearby_devices_found:I = 0x7f060124

.field public static final allshare_nearby_my_device:I = 0x7f060126

.field public static final allshare_no_device:I = 0x7f060120

.field public static final allshare_player_is_not_available:I = 0x7f06011f

.field public static final allshare_select_playback_device:I = 0x7f060009

.field public static final allshare_show_nearby_devices:I = 0x7f060127

.field public static final already_exists:I = 0x7f060109

.field public static final and:I = 0x7f0602e8

.field public static final aperture:I = 0x7f060060

.field public static final app_name:I = 0x7f06000e

.field public static final application_not_found:I = 0x7f0602b4

.field public static final appwidget_empty_text:I = 0x7f060048

.field public static final appwidget_title:I = 0x7f060047

.field public static final as_image_file:I = 0x7f060280

.field public static final as_video_file:I = 0x7f06027f

.field public static final assign_name:I = 0x7f060091

.field public static final at_contextual_tag:I = 0x7f0601fe

.field public static final attention:I = 0x7f06017f

.field public static final auto:I = 0x7f060067

.field public static final available_devices:I = 0x7f06011a

.field public static final available_uses:I = 0x7f0600ef

.field public static final avatar_signature:I = 0x7f0602ec

.field public static final back:I = 0x7f0601de

.field public static final baidu_sync:I = 0x7f060258

.field public static final baidu_sync_noti:I = 0x7f060259

.field public static final balloon_item_snippet:I = 0x7f0602d7

.field public static final balloon_item_title:I = 0x7f0602d6

.field public static final barge_in_buddy_photo_share:I = 0x7f060244

.field public static final barge_in_camera:I = 0x7f060245

.field public static final barge_in_next:I = 0x7f060241

.field public static final barge_in_notification:I = 0x7f0602ea

.field public static final barge_in_play:I = 0x7f06000b

.field public static final barge_in_previous:I = 0x7f060242

.field public static final barge_in_slideshow:I = 0x7f060243

.field public static final barge_in_stop:I = 0x7f06000c

.field public static final best_pic:I = 0x7f06014e

.field public static final best_pic_description:I = 0x7f06014f

.field public static final best_video_shot:I = 0x7f0602eb

.field public static final blind:I = 0x7f060304

.field public static final blue_wash:I = 0x7f0601eb

.field public static final buddy_add_contact_information:I = 0x7f0600c3

.field public static final buddy_no_contact_info:I = 0x7f0600c4

.field public static final buddy_no_face_dectected:I = 0x7f0600c1

.field public static final buddy_no_face_tagged:I = 0x7f0600c2

.field public static final buddy_not_send_to_multiple:I = 0x7f0602c6

.field public static final buddy_not_send_to_single:I = 0x7f0600c5

.field public static final buddy_photo_chaton:I = 0x7f060239

.field public static final buddy_photo_edit:I = 0x7f0600c9

.field public static final buddy_photo_email:I = 0x7f0602f9

.field public static final buddy_photo_mms:I = 0x7f0602fa

.field public static final buddy_photo_send:I = 0x7f0602f8

.field public static final buddy_photo_sendto:I = 0x7f0600c8

.field public static final buddy_photo_share:I = 0x7f0600bf

.field public static final buddy_photo_share1:I = 0x7f0602fb

.field public static final buddy_photo_sharing:I = 0x7f0602f1

.field public static final buddy_send_failed:I = 0x7f0600c7

.field public static final buddy_send_photo_to:I = 0x7f0600c0

.field public static final buddy_send_success:I = 0x7f0600c6

.field public static final burstplay:I = 0x7f060190

.field public static final caching_label:I = 0x7f060037

.field public static final call:I = 0x7f0600b1

.field public static final camera:I = 0x7f0601ba

.field public static final camera_connected:I = 0x7f060076

.field public static final camera_disconnected:I = 0x7f060077

.field public static final camera_setas_wallpaper:I = 0x7f060028

.field public static final cancel:I = 0x7f060003

.field public static final cartoonize:I = 0x7f0601ee

.field public static final change_picture:I = 0x7f06012b

.field public static final clear:I = 0x7f06020d

.field public static final clear_all:I = 0x7f0601c6

.field public static final clear_night:I = 0x7f060214

.field public static final click_import:I = 0x7f060078

.field public static final close:I = 0x7f06002e

.field public static final cloud_album_will_be_deleted:I = 0x7f06021d

.field public static final cloud_delete:I = 0x7f06021e

.field public static final cloud_sync:I = 0x7f060255

.field public static final cloudy:I = 0x7f06020b

.field public static final cloudy_day:I = 0x7f060211

.field public static final collage:I = 0x7f060281

.field public static final collapse:I = 0x7f060270

.field public static final complete_action_using:I = 0x7f060175

.field public static final completed_progress:I = 0x7f0602da

.field public static final confirm:I = 0x7f060092

.field public static final confirm_as:I = 0x7f060093

.field public static final confirm_cannot_save_with_sound_shot:I = 0x7f06029b

.field public static final confirm_clear_all:I = 0x7f0602de

.field public static final confirm_delete:I = 0x7f060000

.field public static final contact_call:I = 0x7f0602f4

.field public static final contact_email:I = 0x7f0602f6

.field public static final contact_info:I = 0x7f0600ac

.field public static final contact_more:I = 0x7f0602f3

.field public static final contact_msg:I = 0x7f0602f5

.field public static final contact_social:I = 0x7f0602f7

.field public static final content_to_display:I = 0x7f06013d

.field public static final contextual_location:I = 0x7f060218

.field public static final contextual_tag:I = 0x7f0601fd

.field public static final contextual_tag_guide:I = 0x7f060308

.field public static final contextual_tag_guide_face:I = 0x7f060249

.field public static final contextual_tag_guide_gps:I = 0x7f060248

.field public static final contextual_tag_guide_weather:I = 0x7f060247

.field public static final contextual_tag_guide_weather_no_widget_name:I = 0x7f060246

.field public static final continuousplay:I = 0x7f06018f

.field public static final copy:I = 0x7f060186

.field public static final copy_abb:I = 0x7f06029a

.field public static final copy_completed:I = 0x7f060197

.field public static final copy_exist:I = 0x7f060260

.field public static final copy_move:I = 0x7f0602d5

.field public static final copy_to:I = 0x7f060187

.field public static final copy_to_clipboard:I = 0x7f060114

.field public static final count:I = 0x7f0600fa

.field public static final count_items_selected:I = 0x7f0601a5

.field public static final coworkers_group:I = 0x7f06017b

.field public static final create_motion_picture:I = 0x7f06026b

.field public static final create_story_album:I = 0x7f060268

.field public static final crop_action:I = 0x7f060038

.field public static final crop_label:I = 0x7f060020

.field public static final crop_not_saved:I = 0x7f06004a

.field public static final crop_save_text:I = 0x7f06001c

.field public static final crop_saved:I = 0x7f060049

.field public static final cross_filter:I = 0x7f0602e4

.field public static final cube:I = 0x7f060306

.field public static final curtain:I = 0x7f0602cc

.field public static final data_usage:I = 0x7f06024b

.field public static final date:I = 0x7f0600fb

.field public static final date_modified:I = 0x7f0601a9

.field public static final date_taken:I = 0x7f0601a8

.field public static final day:I = 0x7f06020e

.field public static final dcim_name:I = 0x7f060240

.field public static final delete:I = 0x7f06000d

.field public static final delete_imagenote_continue:I = 0x7f0601d3

.field public static final delete_imagenote_msg:I = 0x7f0601d2

.field public static final delete_imagenote_title:I = 0x7f0601d1

.field public static final delete_item_failed:I = 0x7f0601b8

.field public static final delete_items_failed:I = 0x7f0602dc

.field public static final delete_this_item_q:I = 0x7f0600ea

.field public static final deleted:I = 0x7f06017d

.field public static final description:I = 0x7f060053

.field public static final deselect_all:I = 0x7f06002a

.field public static final details:I = 0x7f06002c

.field public static final details_hms:I = 0x7f060011

.field public static final details_ms:I = 0x7f060010

.field public static final details_size:I = 0x7f0601a6

.field public static final details_title:I = 0x7f06002d

.field public static final detect_documents:I = 0x7f06009b

.field public static final detect_faces:I = 0x7f060094

.field public static final dialog_msg_confirm_faces:I = 0x7f060099

.field public static final dialog_msg_detect_documents:I = 0x7f06009c

.field public static final dialog_msg_detect_faces:I = 0x7f060098

.field public static final dialog_msg_remove_faces:I = 0x7f06009a

.field public static final direct_share:I = 0x7f06026a

.field public static final display:I = 0x7f0600f9

.field public static final display_now_q:I = 0x7f0600e8

.field public static final do_not_have_enough_space_1:I = 0x7f06026c

.field public static final do_not_have_enough_space_2:I = 0x7f06026d

.field public static final do_not_show_again:I = 0x7f0600db

.field public static final document:I = 0x7f0600a6

.field public static final domino_flip:I = 0x7f0602cd

.field public static final done:I = 0x7f060050

.field public static final downlight:I = 0x7f0601e8

.field public static final download:I = 0x7f060104

.field public static final downloaded:I = 0x7f06012a

.field public static final drag:I = 0x7f060219

.field public static final drag_and_drop:I = 0x7f06027d

.field public static final drag_and_drop_message:I = 0x7f0602c0

.field public static final drag_here:I = 0x7f060231

.field public static final draw_on_image:I = 0x7f0601c3

.field public static final drm_expired:I = 0x7f060100

.field public static final drm_info:I = 0x7f0600f2

.field public static final drm_play:I = 0x7f0600f8

.field public static final drm_time_from:I = 0x7f0600f5

.field public static final drm_time_until:I = 0x7f0600f6

.field public static final drm_type:I = 0x7f0600f0

.field public static final drop:I = 0x7f0602fe

.field public static final drop_show:I = 0x7f060170

.field public static final dropbox_sync:I = 0x7f060256

.field public static final dropbox_sync_noti:I = 0x7f060257

.field public static final duration:I = 0x7f06005a

.field public static final edit:I = 0x7f060034

.field public static final edit_collage:I = 0x7f060283

.field public static final edit_name:I = 0x7f0600aa

.field public static final edit_weather_tag:I = 0x7f060208

.field public static final editing:I = 0x7f06027c

.field public static final effect:I = 0x7f060151

.field public static final email:I = 0x7f0600b3

.field public static final empty_album:I = 0x7f06004c

.field public static final empty_set_description:I = 0x7f060147

.field public static final empty_set_description_baidu:I = 0x7f06014c

.field public static final empty_set_description_center_for_people:I = 0x7f060298

.field public static final empty_set_description_dropbox:I = 0x7f06014a

.field public static final empty_set_description_facebook:I = 0x7f06014b

.field public static final empty_set_description_favorites:I = 0x7f060149

.field public static final empty_set_description_image:I = 0x7f060291

.field public static final empty_set_description_image_tap_here_for_camera:I = 0x7f060294

.field public static final empty_set_description_no_items:I = 0x7f060148

.field public static final empty_set_description_tap_here_for_camera:I = 0x7f060293

.field public static final empty_set_description_tap_here_for_location:I = 0x7f060296

.field public static final empty_set_description_tap_here_for_people:I = 0x7f060297

.field public static final empty_set_description_video:I = 0x7f060292

.field public static final empty_set_description_video_tap_here_for_camera:I = 0x7f060295

.field public static final enter_name:I = 0x7f0600a7

.field public static final eraser:I = 0x7f0601c9

.field public static final eraser_settings:I = 0x7f0601c5

.field public static final error:I = 0x7f060004

.field public static final error_device_not_found:I = 0x7f060137

.field public static final error_failed_to_download:I = 0x7f06013b

.field public static final error_file_already_exists:I = 0x7f060135

.field public static final error_file_error:I = 0x7f060136

.field public static final error_network_error_occurred:I = 0x7f060139

.field public static final error_not_enough_memory:I = 0x7f060138

.field public static final error_unknown:I = 0x7f06013a

.field public static final ex_wave:I = 0x7f060167

.field public static final exit_confirmation:I = 0x7f06021b

.field public static final expand:I = 0x7f060271

.field public static final exploid:I = 0x7f0602cf

.field public static final export:I = 0x7f060230

.field public static final exposure_time:I = 0x7f060063

.field public static final face_reco:I = 0x7f060171

.field public static final face_tag:I = 0x7f0600a8

.field public static final face_tag_dialog_message_1:I = 0x7f0600e4

.field public static final face_zoom:I = 0x7f060307

.field public static final faces_detected:I = 0x7f0600ca

.field public static final fade_in_out:I = 0x7f0602fd

.field public static final faded_color:I = 0x7f0601ec

.field public static final fail_to_load:I = 0x7f060019

.field public static final fail_to_load_invaild_file:I = 0x7f060285

.field public static final failed_to_rename:I = 0x7f06010a

.field public static final family_group:I = 0x7f060179

.field public static final fast:I = 0x7f0600e1

.field public static final favorite:I = 0x7f060273

.field public static final favorites:I = 0x7f0600a5

.field public static final filck_to_navigate:I = 0x7f0602bb

.field public static final file_size:I = 0x7f06005c

.field public static final first_time_view:I = 0x7f0600ee

.field public static final flash:I = 0x7f06005f

.field public static final flash_off:I = 0x7f060069

.field public static final flash_on:I = 0x7f060068

.field public static final flow:I = 0x7f0602ff

.field public static final focal_length:I = 0x7f060061

.field public static final forwarding:I = 0x7f0600f4

.field public static final forwarding_not_allowed:I = 0x7f0600ed

.field public static final free_space_format:I = 0x7f06006f

.field public static final friends_group:I = 0x7f06017a

.field public static final front:I = 0x7f0601dd

.field public static final gadget_title:I = 0x7f06000f

.field public static final gadget_title_simple:I = 0x7f060086

.field public static final geo_tag_alert_message:I = 0x7f060180

.field public static final get_text:I = 0x7f0601bf

.field public static final get_text_description:I = 0x7f060178

.field public static final get_text_download:I = 0x7f060117

.field public static final gray:I = 0x7f0601f5

.field public static final grid_view:I = 0x7f06018e

.field public static final group:I = 0x7f0600a4

.field public static final group_by:I = 0x7f060083

.field public static final group_by_album:I = 0x7f06003f

.field public static final group_by_document:I = 0x7f06009e

.field public static final group_by_faces:I = 0x7f06003e

.field public static final group_by_group:I = 0x7f06009d

.field public static final group_by_location:I = 0x7f06003b

.field public static final group_by_size:I = 0x7f060040

.field public static final group_by_tags:I = 0x7f06003d

.field public static final group_by_time:I = 0x7f06003c

.field public static final hand_writing:I = 0x7f0601c0

.field public static final height:I = 0x7f060058

.field public static final help:I = 0x7f0602bd

.field public static final help_invlaid_action:I = 0x7f0602be

.field public static final help_picker:I = 0x7f0602bf

.field public static final here:I = 0x7f06021a

.field public static final hidden_item:I = 0x7f0601af

.field public static final hide_item:I = 0x7f0601ae

.field public static final hide_item_failed:I = 0x7f0601b2

.field public static final hide_item_success:I = 0x7f0601b1

.field public static final hide_items_failed:I = 0x7f0601b3

.field public static final history_of_direct_share:I = 0x7f060269

.field public static final home:I = 0x7f0601bc

.field public static final hover_here:I = 0x7f060279

.field public static final image_and_video:I = 0x7f06018a

.field public static final image_editor_not_found:I = 0x7f0602db

.field public static final image_only:I = 0x7f060188

.field public static final image_too_small:I = 0x7f06025e

.field public static final imagenote:I = 0x7f0601c1

.field public static final import_complete:I = 0x7f060074

.field public static final import_fail:I = 0x7f060075

.field public static final impossible:I = 0x7f0600f3

.field public static final in_contextual_tag:I = 0x7f0601ff

.field public static final information:I = 0x7f0600d6

.field public static final information_preview_completed:I = 0x7f0602bc

.field public static final ink_painting:I = 0x7f0602e5

.field public static final interval:I = 0x7f0600fc

.field public static final invalid_character:I = 0x7f06010b

.field public static final invert:I = 0x7f0601ed

.field public static final iso:I = 0x7f060064

.field public static final item_clicked:I = 0x7f0602e7

.field public static final kaleidoscope:I = 0x7f0602e2

.field public static final last_month:I = 0x7f060225

.field public static final last_synced_on:I = 0x7f06024e

.field public static final last_three_days:I = 0x7f060221

.field public static final last_week:I = 0x7f060223

.field public static final last_year:I = 0x7f060227

.field public static final latitude:I = 0x7f0601aa

.field public static final learn_about_pan:I = 0x7f0600d5

.field public static final learn_about_tilt:I = 0x7f0600d4

.field public static final license_aquisition_failed:I = 0x7f0600ff

.field public static final loading:I = 0x7f060018

.field public static final loading_account:I = 0x7f060014

.field public static final loading_image:I = 0x7f060008

.field public static final loading_title:I = 0x7f060007

.field public static final loading_video:I = 0x7f060013

.field public static final location:I = 0x7f060055

.field public static final locations:I = 0x7f060080

.field public static final longitude:I = 0x7f0601ab

.field public static final magic_pen:I = 0x7f0601f0

.field public static final make_available_offline:I = 0x7f06004e

.field public static final make_collage:I = 0x7f060267

.field public static final maker:I = 0x7f06005d

.field public static final manual:I = 0x7f060066

.field public static final manual_fd_title:I = 0x7f0600cb

.field public static final mapview_alert_china:I = 0x7f06025c

.field public static final max_char_reached_msg:I = 0x7f06010c

.field public static final max_character:I = 0x7f06019d

.field public static final maximum_selection_number:I = 0x7f06010f

.field public static final maximum_selection_number_exceeded:I = 0x7f060290

.field public static final me:I = 0x7f0600ba

.field public static final message:I = 0x7f0600b2

.field public static final mimetype:I = 0x7f06005b

.field public static final miniature:I = 0x7f0601f8

.field public static final minimum_selection_number:I = 0x7f0602c8

.field public static final mms:I = 0x7f0600b8

.field public static final model:I = 0x7f06005e

.field public static final more:I = 0x7f0600b0

.field public static final more_options:I = 0x7f060284

.field public static final motion:I = 0x7f0600d2

.field public static final move:I = 0x7f060183

.field public static final move_abb:I = 0x7f060299

.field public static final move_completed:I = 0x7f060198

.field public static final move_device:I = 0x7f06028e

.field public static final move_exist:I = 0x7f060261

.field public static final move_to:I = 0x7f060185

.field public static final movie_view_label:I = 0x7f060012

.field public static final moving:I = 0x7f060184

.field public static final moving_frame:I = 0x7f060300

.field public static final multi_cube:I = 0x7f060172

.field public static final multiface_crop_help:I = 0x7f06001d

.field public static final multiple_albums_will_be_deleted:I = 0x7f060196

.field public static final multiple_items_will_be_moved:I = 0x7f0602d8

.field public static final music:I = 0x7f060152

.field public static final my_contact_group:I = 0x7f06017c

.field public static final n_items_will_be_deleted:I = 0x7f06008a

.field public static final n_unsupported_files_not_send:I = 0x7f060265

.field public static final name_cannot_empty:I = 0x7f06019b

.field public static final negative:I = 0x7f0601f6

.field public static final new_album:I = 0x7f060182

.field public static final new_album_message:I = 0x7f06021c

.field public static final new_album_one:I = 0x7f06018b

.field public static final new_file_name:I = 0x7f060107

.field public static final no_albums_alert:I = 0x7f06004b

.field public static final no_app_for_action:I = 0x7f06023b

.field public static final no_connectivity:I = 0x7f060043

.field public static final no_devices_found_scanning_etc:I = 0x7f060125

.field public static final no_document:I = 0x7f06009f

.field public static final no_group:I = 0x7f0600be

.field public static final no_hidden_item:I = 0x7f0601b7

.field public static final no_images_available:I = 0x7f0600a0

.field public static final no_location:I = 0x7f060042

.field public static final no_location_image:I = 0x7f06019e

.field public static final no_longer_available:I = 0x7f0600eb

.field public static final no_selection_items:I = 0x7f06010e

.field public static final no_such_item:I = 0x7f060033

.field public static final no_tags:I = 0x7f06020f

.field public static final no_thumbnail:I = 0x7f06001a

.field public static final no_usb_storage:I = 0x7f06014d

.field public static final no_videos_available:I = 0x7f0600a1

.field public static final noface:I = 0x7f0602f2

.field public static final normal:I = 0x7f0601e1

.field public static final nostalgia:I = 0x7f0601e9

.field public static final not_allowed_in_this_phone:I = 0x7f0600e9

.field public static final not_supported:I = 0x7f0602c3

.field public static final noti_date:I = 0x7f060217

.field public static final noti_date_att:I = 0x7f06029c

.field public static final noti_people:I = 0x7f060216

.field public static final noti_weather:I = 0x7f060215

.field public static final notice_low_storage:I = 0x7f060134

.field public static final number_of_albums_selected:I = 0x7f06008e

.field public static final number_of_groups_selected:I = 0x7f06008f

.field public static final number_of_item_selected:I = 0x7f06008d

.field public static final number_of_items_selected:I = 0x7f06008c

.field public static final ocr_add_bookmark:I = 0x7f060237

.field public static final ocr_add_contact:I = 0x7f060234

.field public static final ocr_error:I = 0x7f06022c

.field public static final ocr_image_is_too_large:I = 0x7f06022a

.field public static final ocr_no_result:I = 0x7f06029d

.field public static final ocr_no_text:I = 0x7f06022d

.field public static final ocr_open_url:I = 0x7f060236

.field public static final ocr_search_result:I = 0x7f06022f

.field public static final ocr_searching_text:I = 0x7f06022e

.field public static final ocr_send_mail:I = 0x7f060233

.field public static final ocr_send_msg:I = 0x7f060235

.field public static final ocr_title:I = 0x7f06022b

.field public static final ocr_unsupported_image_format:I = 0x7f060229

.field public static final ocr_web_search:I = 0x7f060232

.field public static final off:I = 0x7f060156

.field public static final oil_paint:I = 0x7f0601f1

.field public static final ok:I = 0x7f060002

.field public static final old_photo:I = 0x7f0601e3

.field public static final on:I = 0x7f0600b5

.field public static final on_contextual_tag:I = 0x7f060200

.field public static final one_album_will_be_deleted:I = 0x7f060195

.field public static final one_item_will_be_deleted:I = 0x7f060001

.field public static final one_item_will_be_moved:I = 0x7f0602d9

.field public static final one_p_turn:I = 0x7f0602d1

.field public static final one_unsupported_file_not_send:I = 0x7f060264

.field public static final online_pic_download:I = 0x7f060102

.field public static final open:I = 0x7f0601cc

.field public static final orientation:I = 0x7f060059

.field public static final others:I = 0x7f060207

.field public static final over_capacity:I = 0x7f0602fc

.field public static final overwrite:I = 0x7f06025f

.field public static final page_turn:I = 0x7f060302

.field public static final pan_complete_msg:I = 0x7f06028f

.field public static final panning_dialog_message_1:I = 0x7f0600da

.field public static final panning_help_dialog_message:I = 0x7f0602c7

.field public static final paper_artist:I = 0x7f060119

.field public static final path:I = 0x7f060056

.field public static final pc_blue:I = 0x7f0602e1

.field public static final pc_red:I = 0x7f0602e0

.field public static final pc_yellow:I = 0x7f0602df

.field public static final pd_documents_detected:I = 0x7f0600d1

.field public static final pd_faces_detected:I = 0x7f0600ce

.field public static final pd_seconds:I = 0x7f060133

.field public static final pen:I = 0x7f0601c7

.field public static final pen_settings:I = 0x7f0601c4

.field public static final pen_settings_already_exists:I = 0x7f0601da

.field public static final pen_settings_preset_delete_msg:I = 0x7f0601d9

.field public static final pen_settings_preset_delete_title:I = 0x7f0601d8

.field public static final pen_settings_preset_empty:I = 0x7f0601d7

.field public static final pen_settings_preset_maximum_msg:I = 0x7f0601db

.field public static final people:I = 0x7f060081

.field public static final person:I = 0x7f060202

.field public static final perspective:I = 0x7f060305

.field public static final photo_editor:I = 0x7f060115

.field public static final photo_editor_description:I = 0x7f060177

.field public static final photo_navigation:I = 0x7f0601be

.field public static final photo_wizard:I = 0x7f060116

.field public static final picasa_posts:I = 0x7f06004d

.field public static final picasa_web_albums:I = 0x7f0600e5

.field public static final picture_added:I = 0x7f06012c

.field public static final pictures_added:I = 0x7f06012d

.field public static final pixelize:I = 0x7f0601f4

.field public static final play_from_newest:I = 0x7f06016c

.field public static final play_from_oldest:I = 0x7f06016b

.field public static final polaroidframe:I = 0x7f0601c2

.field public static final pop_art:I = 0x7f0601e4

.field public static final posterize:I = 0x7f0601f2

.field public static final printer:I = 0x7f06010d

.field public static final process_caching_requests:I = 0x7f060036

.field public static final processing:I = 0x7f0600cc

.field public static final rainy:I = 0x7f06020c

.field public static final rainy_day:I = 0x7f060212

.field public static final random:I = 0x7f060174

.field public static final redo:I = 0x7f0601cb

.field public static final reflection:I = 0x7f0602e3

.field public static final refresh:I = 0x7f060121

.field public static final remove_face:I = 0x7f0600ab

.field public static final remove_tag:I = 0x7f0600b6

.field public static final removed_from_favourites:I = 0x7f060276

.field public static final rename:I = 0x7f060105

.field public static final rename_name_empty:I = 0x7f060106

.field public static final render_info_can_use_1_time:I = 0x7f0600e7

.field public static final render_info_can_use_n_times:I = 0x7f0600e6

.field public static final resolution:I = 0x7f0601a7

.field public static final resume_playing_message:I = 0x7f060016

.field public static final resume_playing_restart:I = 0x7f06001b

.field public static final resume_playing_resume:I = 0x7f060017

.field public static final resume_playing_title:I = 0x7f060015

.field public static final retro:I = 0x7f0601e6

.field public static final rot_cube:I = 0x7f060163

.field public static final rotate_left:I = 0x7f060031

.field public static final rotate_right:I = 0x7f060032

.field public static final sa_blind:I = 0x7f060168

.field public static final sa_break:I = 0x7f0602d4

.field public static final sa_collage:I = 0x7f06016a

.field public static final sa_flip:I = 0x7f060161

.field public static final sa_perspective:I = 0x7f060173

.field public static final sa_rotate:I = 0x7f060162

.field public static final save:I = 0x7f0601cd

.field public static final save_as:I = 0x7f0601d0

.field public static final save_error:I = 0x7f06001f

.field public static final save_imagenote_current_msg:I = 0x7f0601d5

.field public static final save_imagenote_msg:I = 0x7f0601d4

.field public static final save_photoframe_current_msg:I = 0x7f0601d6

.field public static final saved:I = 0x7f0601ce

.field public static final saved_in:I = 0x7f0601cf

.field public static final saving_image:I = 0x7f06001e

.field public static final scale:I = 0x7f060164

.field public static final scan_for_nearby_devices:I = 0x7f060122

.field public static final scan_started_device_list_etc:I = 0x7f060123

.field public static final scroll_left_or_right:I = 0x7f0602b5

.field public static final search:I = 0x7f0601bb

.field public static final search_hint:I = 0x7f0601bd

.field public static final search_text:I = 0x7f0601fb

.field public static final sec:I = 0x7f060158

.field public static final sec_1:I = 0x7f060159

.field public static final sec_3:I = 0x7f06015a

.field public static final sec_5:I = 0x7f06015b

.field public static final sec_7:I = 0x7f06015c

.field public static final select_a_picture:I = 0x7f060278

.field public static final select_a_picture_face:I = 0x7f0602b8

.field public static final select_album:I = 0x7f060024

.field public static final select_all:I = 0x7f060029

.field public static final select_an_album:I = 0x7f060277

.field public static final select_best_pic_to_save:I = 0x7f060150

.field public static final select_channel:I = 0x7f0600b7

.field public static final select_device:I = 0x7f06011b

.field public static final select_from:I = 0x7f060287

.field public static final select_group:I = 0x7f060025

.field public static final select_here:I = 0x7f0602f0

.field public static final select_image:I = 0x7f060021

.field public static final select_item:I = 0x7f060023

.field public static final select_video:I = 0x7f060022

.field public static final send:I = 0x7f0600ad

.field public static final send_this_photo:I = 0x7f0600b9

.field public static final sending:I = 0x7f0600cf

.field public static final sensitivity:I = 0x7f0600e0

.field public static final sensitivity_test:I = 0x7f0600df

.field public static final sepia:I = 0x7f0601e5

.field public static final sequence_in_set:I = 0x7f060051

.field public static final set_Wallpaper:I = 0x7f060113

.field public static final set_as:I = 0x7f060039

.field public static final set_as_caller_id:I = 0x7f0600af

.field public static final set_callerid_message:I = 0x7f0600ae

.field public static final set_image:I = 0x7f060026

.field public static final set_label_all_albums:I = 0x7f06006b

.field public static final set_label_local_albums:I = 0x7f06006c

.field public static final set_label_mtp_devices:I = 0x7f06006d

.field public static final set_label_picasa_albums:I = 0x7f06006e

.field public static final set_up_your_email_account:I = 0x7f0600bb

.field public static final setas_both:I = 0x7f060089

.field public static final setas_lockscreen:I = 0x7f060088

.field public static final setting_picture_frame:I = 0x7f060110

.field public static final settings:I = 0x7f060084

.field public static final share:I = 0x7f06000a

.field public static final share_soundshot:I = 0x7f06027e

.field public static final sharpen:I = 0x7f0601f7

.field public static final show_all:I = 0x7f060046

.field public static final show_hidden_item:I = 0x7f0601b0

.field public static final show_images_only:I = 0x7f060044

.field public static final show_item:I = 0x7f060238

.field public static final show_on_map:I = 0x7f060030

.field public static final show_videos_only:I = 0x7f060045

.field public static final sidewindow:I = 0x7f0602ce

.field public static final signature:I = 0x7f0602c4

.field public static final size_above:I = 0x7f060071

.field public static final size_below:I = 0x7f060070

.field public static final size_between:I = 0x7f060072

.field public static final sketch:I = 0x7f0601f3

.field public static final skip:I = 0x7f06008b

.field public static final slide:I = 0x7f06016d

.field public static final slideshow:I = 0x7f06002b

.field public static final slideshow_dream_name:I = 0x7f06007d

.field public static final slideshow_interval:I = 0x7f060131

.field public static final slideshow_music:I = 0x7f060252

.field public static final slideshow_music_count:I = 0x7f06025d

.field public static final slideshow_notibar_playback_on_tv:I = 0x7f06015f

.field public static final slideshow_notibar_tap_to_enable_mirroring:I = 0x7f060160

.field public static final slideshow_setting:I = 0x7f06015d

.field public static final slideshow_start:I = 0x7f06015e

.field public static final slow:I = 0x7f0600e2

.field public static final smooth:I = 0x7f06016e

.field public static final snail:I = 0x7f060165

.field public static final snowy:I = 0x7f0602e9

.field public static final snowy_day:I = 0x7f060213

.field public static final sns_data_control:I = 0x7f06025a

.field public static final social:I = 0x7f0600b4

.field public static final soft_glow:I = 0x7f0601ef

.field public static final sort:I = 0x7f060155

.field public static final sort_by_latest:I = 0x7f06027a

.field public static final sort_by_oldest:I = 0x7f06027b

.field public static final speak_add_face_as_name:I = 0x7f0602a9

.field public static final speak_add_name:I = 0x7f0602aa

.field public static final speak_burst_shot_pictures:I = 0x7f0602a8

.field public static final speak_edit_photo_note:I = 0x7f0602af

.field public static final speak_face_detected:I = 0x7f0602ad

.field public static final speak_face_name:I = 0x7f0602ae

.field public static final speak_face_recognized:I = 0x7f0602ac

.field public static final speak_folder_name:I = 0x7f0602ed

.field public static final speak_folder_name_1_item:I = 0x7f0602ee

.field public static final speak_folder_name_n_items:I = 0x7f0602a1

.field public static final speak_folder_name_new_photos_detected:I = 0x7f0602ef

.field public static final speak_item_selected:I = 0x7f0602a6

.field public static final speak_item_unselected:I = 0x7f0602a7

.field public static final speak_location:I = 0x7f0602b0

.field public static final speak_new_folder_name:I = 0x7f0602a2

.field public static final speak_new_photo_detected:I = 0x7f0602a3

.field public static final speak_now:I = 0x7f06023a

.field public static final speak_picture_name:I = 0x7f0602a4

.field public static final speak_remove_tag:I = 0x7f0602ab

.field public static final speak_video_name:I = 0x7f0602a5

.field public static final speed:I = 0x7f060153

.field public static final spiral:I = 0x7f0601a0

.field public static final spiral_view:I = 0x7f06019f

.field public static final split_close:I = 0x7f06023c

.field public static final split_screen:I = 0x7f06023e

.field public static final split_switch:I = 0x7f06023f

.field public static final split_window:I = 0x7f06023d

.field public static final start:I = 0x7f060157

.field public static final stms_version:I = 0x7f060266

.field public static final sunny:I = 0x7f06020a

.field public static final sunny_day:I = 0x7f060210

.field public static final sunshin:I = 0x7f0601e7

.field public static final swiping:I = 0x7f0602b7

.field public static final switch_to_camera:I = 0x7f06002f

.field public static final sync_disabled:I = 0x7f06024c

.field public static final sync_error:I = 0x7f06024d

.field public static final sync_in_progress:I = 0x7f06024f

.field public static final sync_on_wifi_only:I = 0x7f060250

.field public static final sync_on_wifi_only_help:I = 0x7f060251

.field public static final sync_on_wlan_only:I = 0x7f060253

.field public static final sync_on_wlan_only_help:I = 0x7f060254

.field public static final sync_picasa_albums:I = 0x7f06004f

.field public static final sync_with_dropbox:I = 0x7f060145

.field public static final sync_with_dropbox_description:I = 0x7f060146

.field public static final tab:I = 0x7f0602a0

.field public static final tags:I = 0x7f060082

.field public static final tap_and_hold:I = 0x7f06028d

.field public static final tap_the_name:I = 0x7f0602ba

.field public static final tap_the_yellow_box:I = 0x7f0602b9

.field public static final tap_to_add_images:I = 0x7f060087

.field public static final tap_to_sync_content:I = 0x7f06025b

.field public static final tap_to_view:I = 0x7f06026f

.field public static final tapping_and_holding:I = 0x7f0602b6

.field public static final text:I = 0x7f0601c8

.field public static final the_same_wallpaper_will_be_applied_to_the_lock_screen:I = 0x7f060112

.field public static final there_is_only_one_album:I = 0x7f0601a2

.field public static final this_file_will_be_deleted:I = 0x7f0601a4

.field public static final this_month:I = 0x7f060224

.field public static final this_week:I = 0x7f060222

.field public static final this_year:I = 0x7f060226

.field public static final through:I = 0x7f060166

.field public static final tilting_help_dialog_message:I = 0x7f060289

.field public static final time:I = 0x7f060054

.field public static final timeline_view:I = 0x7f0601a1

.field public static final times:I = 0x7f06007f

.field public static final title:I = 0x7f060052

.field public static final title_select_photo:I = 0x7f0601a3

.field public static final today:I = 0x7f06021f

.field public static final tornado:I = 0x7f060169

.field public static final trim:I = 0x7f060191

.field public static final try_air_motion:I = 0x7f0600dc

.field public static final try_air_motion_jpn:I = 0x7f060288

.field public static final try_panning:I = 0x7f0600de

.field public static final try_tilt:I = 0x7f0600dd

.field public static final try_to_set_local_album_available_offline:I = 0x7f06006a

.field public static final tumble:I = 0x7f0602cb

.field public static final twist:I = 0x7f0602d3

.field public static final two_p_turn:I = 0x7f0602d2

.field public static final two_years_ago:I = 0x7f060228

.field public static final unable_to_copy:I = 0x7f060199

.field public static final unable_to_create_folder:I = 0x7f06019c

.field public static final unable_to_find_content:I = 0x7f060282

.field public static final unable_to_move:I = 0x7f06019a

.field public static final unable_to_play_during_call:I = 0x7f060286

.field public static final unable_to_rename:I = 0x7f060108

.field public static final unable_to_save:I = 0x7f0601fa

.field public static final undo:I = 0x7f0601ca

.field public static final unfavourite:I = 0x7f060274

.field public static final unhide_item_failed:I = 0x7f0601b5

.field public static final unhide_item_success:I = 0x7f0601b4

.field public static final unhide_items_failed:I = 0x7f0601b6

.field public static final unit_mm:I = 0x7f060065

.field public static final unknown:I = 0x7f0600f7

.field public static final unknown_location:I = 0x7f0601ac

.field public static final unlimited:I = 0x7f0600fe

.field public static final unlock_it_now_q:I = 0x7f0600ec

.field public static final unnamed:I = 0x7f0600bc

.field public static final unsupported_file:I = 0x7f060005

.field public static final untagged:I = 0x7f060041

.field public static final up_button:I = 0x7f0601fc

.field public static final upload:I = 0x7f060103

.field public static final use_as_home_screen_wallpaper_too:I = 0x7f060111

.field public static final use_by_default_for_this_action:I = 0x7f060176

.field public static final use_motion:I = 0x7f0600d3

.field public static final use_motion_after_calibration:I = 0x7f0600e3

.field public static final validity:I = 0x7f0600f1

.field public static final video_err:I = 0x7f06003a

.field public static final video_maker:I = 0x7f060118

.field public static final video_only:I = 0x7f060189

.field public static final view_by:I = 0x7f06013c

.field public static final view_by_all:I = 0x7f06013e

.field public static final view_by_baidu:I = 0x7f060143

.field public static final view_by_dropbox:I = 0x7f060140

.field public static final view_by_facebook:I = 0x7f060141

.field public static final view_by_local:I = 0x7f06013f

.field public static final view_by_picasa:I = 0x7f060142

.field public static final view_by_tcloud:I = 0x7f060144

.field public static final vignetting:I = 0x7f0601f9

.field public static final vintage:I = 0x7f0601e2

.field public static final wallpaper:I = 0x7f060027

.field public static final wallpaper_failed_to_set:I = 0x7f060101

.field public static final warning:I = 0x7f0601df

.field public static final warning_change_side:I = 0x7f0601e0

.field public static final wave:I = 0x7f060303

.field public static final weather:I = 0x7f060203

.field public static final weather_app_name_chn:I = 0x7f060262

.field public static final white_balance:I = 0x7f060062

.field public static final widget_frametype_multi_left_rightupdown:I = 0x7f0602b1

.field public static final widget_frametype_multi_up_down:I = 0x7f0602b2

.field public static final widget_frametype_multi_up_downleftright:I = 0x7f0602b3

.field public static final widget_frametype_single:I = 0x7f06026e

.field public static final widget_layout_type:I = 0x7f06012e

.field public static final widget_multi_frame:I = 0x7f060130

.field public static final widget_single_frame:I = 0x7f06012f

.field public static final widget_stop:I = 0x7f06018d

.field public static final widget_type:I = 0x7f06007c

.field public static final widget_type_album:I = 0x7f060079

.field public static final widget_type_person:I = 0x7f0602c5

.field public static final widget_type_photo:I = 0x7f06007b

.field public static final widget_type_photos:I = 0x7f0600a2

.field public static final widget_type_shuffle:I = 0x7f06007a

.field public static final width:I = 0x7f060057

.field public static final wifi_dialog_body:I = 0x7f06029e

.field public static final wifi_dialog_title:I = 0x7f06029f

.field public static final with_contextual_tag:I = 0x7f060201

.field public static final with_person:I = 0x7f060204

.field public static final with_person_and_another:I = 0x7f060205

.field public static final with_person_and_others:I = 0x7f060206

.field public static final wizzle:I = 0x7f0602d0

.field public static final yellow_glow:I = 0x7f0601ea

.field public static final yesterday:I = 0x7f060220

.field public static final your_item_will_be_downloaded:I = 0x7f060129

.field public static final zero_file_size:I = 0x7f060263

.field public static final zoom:I = 0x7f060301

.field public static final zoomin:I = 0x7f06016f

.field public static final zooming_dialog_message_1:I = 0x7f0600d7

.field public static final zooming_dialog_message_2:I = 0x7f0600d8

.field public static final zooming_dialog_message_3:I = 0x7f0600d9

.field public static final zooming_shot:I = 0x7f0602e6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
