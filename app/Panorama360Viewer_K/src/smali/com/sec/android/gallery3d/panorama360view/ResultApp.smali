.class public Lcom/sec/android/gallery3d/panorama360view/ResultApp;
.super Landroid/app/Activity;
.source "ResultApp.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/View$OnTouchListener;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;
    }
.end annotation


# static fields
.field private static final ACTION_BAR_DISP_TIME:I = 0xfa0

.field private static final ACTION_VIEW_360_PHOTO:Ljava/lang/String; = "com.samsung.android.intent.action.VIEW_360_PHOTO"

.field private static final DECODE_POST_VIEWDATA_DONE:I = 0x4

.field private static final DIALOG_ID_DECODE_ERROR:I = 0x0

.field private static final DIALOG_ID_DISP_ERROR:I = 0x1

.field private static final DIALOG_ID_FILE_OPEN_ERROR:I = 0x4

.field private static final DIALOG_ID_LOADING:I = 0x3

.field private static final DIALOG_ID_PROCESSING:I = 0x2

.field private static FORMAT_STR_RGB888:Ljava/lang/String; = null

.field private static final GL_POST_VIEW_OFF:I = 0x1

.field private static final GL_POST_VIEW_ON:I = 0x0

.field private static final INTENT_DISP_TYPE:Ljava/lang/String; = "disp_type_setting"

.field private static final INTENT_FILENAME:Ljava/lang/String; = "file_name"

.field private static final INTENT_GL_POST_VIEW_RENDER_LOW_IMAGE:Ljava/lang/String; = "render_low_image"

.field private static final INTENT_GL_POST_VIEW_SETTING:Ljava/lang/String; = "post_view_setting"

.field private static final INTENT_RENDER_MODE:Ljava/lang/String; = "render_mode"

.field private static final INTENT_SENSOR_FUSION_MODE:Ljava/lang/String; = "sensor_fusion_mode"

.field private static LOG_TAG:Ljava/lang/String; = null

.field private static final MSG_POSTVIEW_DATA_DECODE_ERROR:I = 0x3

.field private static final MSG_REREGISTER_TEXTURE_COMP:I = 0x0

.field private static final MSG_SET_FILE_OPEN_ERROR:I = 0x5

.field private static final MSG_SET_POSTVIEW_DATA_COMP:I = 0x1

.field private static final MSG_SET_POSTVIEW_DATA_ERROR:I = 0x2

.field private static final SF_MODE_USE_ACCELEROMETER_AND_MAGNETIC_FIELD:I = 0x3

.field private static final SF_MODE_USE_ALL_SENSORS:I = 0x0

.field private static final SF_MODE_USE_GYROSCOPE:I = 0x1

.field private static final SF_MODE_USE_GYROSCOPE_AND_ROTATION_VECTOR:I = 0x4

.field private static final SF_MODE_USE_GYROSCOPE_WITH_ACCELEROMETER:I = 0x2

.field private static final SF_MODE_USE_GYROSCOPE_WITH_ACCELEROMETER_EX:I = 0x6

.field private static final SF_MODE_USE_ROTATION_VECTOR:I = 0x5

.field private static final TOAST_POS_DPI_RIGHT:I = 0x23

.field private static final TOAST_POS_DPI_TOP:I = 0x30

.field private static final newVersion:Z = true


# instance fields
.field private final INTERIA_SCROLL_DECELERATION_RATE:F

.field private final INTERIA_SCROLL_INTERVAL:I

.field private final SENSOR_DRIVEN_INTERVAL:I

.field private isDRMFile:Z

.field private isEnableSensor:Z

.field private isFileSelect:Z

.field private isMoveDisable:Z

.field private isRenderLowImage:Z

.field private isTouching:Z

.field private isUseSensorDriven:Z

.field private isValidFile:Z

.field private mAccelerometer:Landroid/hardware/Sensor;

.field private mActionBarTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

.field private mActivity:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

.field private mAppSensorFusionMode:I

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mDiffX:F

.field private mDiffY:F

.field private mDispType:I

.field private mExifOrientation:[I

.field private mFileName:Ljava/lang/String;

.field private mGLPanoramaView:Landroid/opengl/GLSurfaceView;

.field private mGLPostView:I

.field private mGalleryData:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mGyroscope:Landroid/hardware/Sensor;

.field private mHandler:Landroid/os/Handler;

.field private mIBGalleryPlayButton:Landroid/widget/ImageButton;

.field private mIBVideoClipPlayerButton:Landroid/widget/ImageButton;

.field private mInertiaScrollTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

.field private mIsSlideshowPlaying:Z

.field private mLoadingTime:J

.field private mMagneticField:Landroid/hardware/Sensor;

.field private mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

.field private mMorphoSensorFusion:Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;

.field private mNorthPolePos:Landroid/graphics/Point;

.field private mOrgBitmap:Landroid/graphics/Bitmap;

.field private mPanoramaDialog:[Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;

.field private mPartOfAccelerometerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;",
            ">;"
        }
    .end annotation
.end field

.field private mPartOfGyroscopeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;",
            ">;"
        }
    .end annotation
.end field

.field private mPartOfMagneticFieldList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;",
            ">;"
        }
    .end annotation
.end field

.field private mPartOfOrientationList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;",
            ">;"
        }
    .end annotation
.end field

.field private mPartOfRotationVectorList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;",
            ">;"
        }
    .end annotation
.end field

.field private mPlayButtonOnClickListener:Landroid/view/View$OnClickListener;

.field private mPlayButtonOnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mPostviewDefaultParam:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;

.field private mPostviewParam:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;

.field private mPrevSensorMat:[D

.field private mPreviousScale:F

.field private mPreviousX:F

.field private mPreviousY:F

.field private mRenderHandler:Landroid/os/Handler;

.field private mRenderTimer:Ljava/util/Timer;

.field private mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

.field private mRenderingMode:I

.field private mRotateCount:I

.field private mRotationVector:Landroid/hardware/Sensor;

.field private mSGListner:Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;

.field private mSavePanoramaImagesThread:Ljava/lang/Thread;

.field private mScaleBitmap:Landroid/graphics/Bitmap;

.field private mScaleGestureDtector:Landroid/view/ScaleGestureDetector;

.field private mSensorAngle:[D

.field private mSensorDrivenTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

.field private mSensorFusionMode:I

.field private mSensorLockObj:Ljava/lang/Object;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorMat:[D

.field private mSouthPolePos:Landroid/graphics/Point;

.field private mThread:Ljava/lang/Thread;

.field private mViewParam:[Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    const-string v0, "Panorama360Viewer"

    sput-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    .line 93
    const-string v0, "RGB888"

    sput-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->FORMAT_STR_RGB888:Ljava/lang/String;

    .line 338
    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/16 v5, 0x14

    const/16 v4, 0x9

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 97
    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mFileName:Ljava/lang/String;

    .line 101
    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPostviewParam:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;

    .line 102
    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPostviewDefaultParam:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;

    .line 120
    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGyroscope:Landroid/hardware/Sensor;

    .line 121
    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRotationVector:Landroid/hardware/Sensor;

    .line 122
    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mAccelerometer:Landroid/hardware/Sensor;

    .line 123
    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMagneticField:Landroid/hardware/Sensor;

    .line 125
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorLockObj:Ljava/lang/Object;

    .line 126
    const/4 v0, 0x3

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorAngle:[D

    .line 127
    new-array v0, v4, [D

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorMat:[D

    .line 128
    new-array v0, v4, [D

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPrevSensorMat:[D

    .line 137
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v2, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mNorthPolePos:Landroid/graphics/Point;

    .line 138
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v2, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSouthPolePos:Landroid/graphics/Point;

    .line 143
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPreviousScale:F

    .line 145
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isTouching:Z

    .line 147
    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mScaleBitmap:Landroid/graphics/Bitmap;

    .line 148
    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mOrgBitmap:Landroid/graphics/Bitmap;

    .line 157
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPanoramaDialog:[Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;

    .line 159
    iput-boolean v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIsSlideshowPlaying:Z

    .line 164
    const v0, 0x3d99999a    # 0.075f

    iput v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->INTERIA_SCROLL_DECELERATION_RATE:F

    .line 165
    iput v5, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->INTERIA_SCROLL_INTERVAL:I

    .line 168
    iput v5, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->SENSOR_DRIVEN_INTERVAL:I

    .line 170
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mExifOrientation:[I

    .line 171
    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mThread:Ljava/lang/Thread;

    .line 172
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mViewParam:[Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;

    .line 176
    new-instance v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$1;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$1;-><init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 191
    new-instance v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$2;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$2;-><init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderHandler:Landroid/os/Handler;

    .line 220
    new-instance v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$3;-><init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mHandler:Landroid/os/Handler;

    .line 263
    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBGalleryPlayButton:Landroid/widget/ImageButton;

    .line 264
    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBVideoClipPlayerButton:Landroid/widget/ImageButton;

    .line 302
    new-instance v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$4;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$4;-><init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPlayButtonOnClickListener:Landroid/view/View$OnClickListener;

    .line 313
    new-instance v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$5;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$5;-><init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPlayButtonOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 985
    new-instance v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$6;

    invoke-direct {v0, p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$6;-><init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSGListner:Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;

    .line 69
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V
    .locals 0

    .prologue
    .line 645
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->stopSlideShowTimer()V

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V
    .locals 0

    .prologue
    .line 854
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->setPostviewData()V

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V
    .locals 0

    .prologue
    .line 575
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->createGLSurface()V

    return-void
.end method

.method static synthetic access$11(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Z
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIsSlideshowPlaying:Z

    return v0
.end method

.method static synthetic access$12(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V
    .locals 0

    .prologue
    .line 597
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->startSlideShowTimer()V

    return-void
.end method

.method static synthetic access$13(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/ResultApp;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mActivity:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    return-object v0
.end method

.method static synthetic access$14(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)F
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPreviousScale:F

    return v0
.end method

.method static synthetic access$15(Lcom/sec/android/gallery3d/panorama360view/ResultApp;F)V
    .locals 0

    .prologue
    .line 143
    iput p1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPreviousScale:F

    return-void
.end method

.method static synthetic access$16(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBGalleryPlayButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$17(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBVideoClipPlayerButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$18(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorLockObj:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$19(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)[D
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorMat:[D

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    return-object v0
.end method

.method static synthetic access$20(Lcom/sec/android/gallery3d/panorama360view/ResultApp;[D)V
    .locals 0

    .prologue
    .line 1296
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getSensorAngle([D)V

    return-void
.end method

.method static synthetic access$21(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)[D
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPrevSensorMat:[D

    return-object v0
.end method

.method static synthetic access$22(Lcom/sec/android/gallery3d/panorama360view/ResultApp;[D)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPrevSensorMat:[D

    return-void
.end method

.method static synthetic access$23(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)[D
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorAngle:[D

    return-object v0
.end method

.method static synthetic access$24(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isUseSensorDriven:Z

    return v0
.end method

.method static synthetic access$25(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorDrivenTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    return-object v0
.end method

.method static synthetic access$26(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)F
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffX:F

    return v0
.end method

.method static synthetic access$27(Lcom/sec/android/gallery3d/panorama360view/ResultApp;F)V
    .locals 0

    .prologue
    .line 141
    iput p1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffX:F

    return-void
.end method

.method static synthetic access$28(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)F
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffY:F

    return v0
.end method

.method static synthetic access$29(Lcom/sec/android/gallery3d/panorama360view/ResultApp;F)V
    .locals 0

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffY:F

    return-void
.end method

.method static synthetic access$3(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Landroid/opengl/GLSurfaceView;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    return-object v0
.end method

.method static synthetic access$30(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mInertiaScrollTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    return-object v0
.end method

.method static synthetic access$31(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGalleryData:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;

    return-object v0
.end method

.method static synthetic access$32(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    return-object v0
.end method

.method static synthetic access$33(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isRenderLowImage:Z

    return v0
.end method

.method static synthetic access$34(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSavePanoramaImagesThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/gallery3d/panorama360view/ResultApp;I)V
    .locals 0

    .prologue
    .line 1199
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->dismissDialogFragment(I)V

    return-void
.end method

.method static synthetic access$6()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)J
    .locals 2

    .prologue
    .line 257
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getCurTime()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$8(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)J
    .locals 2

    .prologue
    .line 174
    iget-wide v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mLoadingTime:J

    return-wide v0
.end method

.method static synthetic access$9(Lcom/sec/android/gallery3d/panorama360view/ResultApp;I)V
    .locals 0

    .prologue
    .line 1189
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->showDialogFragment(I)V

    return-void
.end method

.method private clearArrayList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1344
    .local p1, "sd_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1345
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 1347
    :cond_0
    return-void
.end method

.method private createGLSurface()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 577
    const/high16 v1, 0x7f090000

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 578
    .local v0, "frame_layout":Landroid/widget/FrameLayout;
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isGLPostView()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 579
    new-instance v1, Landroid/opengl/GLSurfaceView;

    invoke-direct {v1, p0}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    .line 580
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/opengl/GLSurfaceView;->setEGLContextClientVersion(I)V

    .line 581
    new-instance v1, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    iget-boolean v4, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isFileSelect:Z

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;-><init>(Landroid/app/Activity;Landroid/os/Handler;Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;Z)V

    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    .line 582
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPostviewParam:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;

    if-eqz v1, :cond_0

    .line 583
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPostviewParam:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;

    iget-wide v2, v2, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;->scale:D

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->setDefaultScale(D)V

    .line 585
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    invoke-virtual {v1, v2}, Landroid/opengl/GLSurfaceView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 586
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v1, v5}, Landroid/opengl/GLSurfaceView;->setRenderMode(I)V

    .line 587
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 588
    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 589
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v1, v5}, Landroid/opengl/GLSurfaceView;->setVisibility(I)V

    .line 590
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v1, v6}, Landroid/opengl/GLSurfaceView;->setZOrderMediaOverlay(Z)V

    .line 591
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v1, p0}, Landroid/opengl/GLSurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 592
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v1, v6}, Landroid/opengl/GLSurfaceView;->setFocusableInTouchMode(Z)V

    .line 593
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    iget v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDispType:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->setDispType(I)V

    .line 595
    :cond_1
    return-void
.end method

.method private decodePostViewData(Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;Ljava/lang/String;[I[Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;)Z
    .locals 23
    .param p1, "image_stitcher"    # Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;
    .param p2, "file_path"    # Ljava/lang/String;
    .param p3, "exif_orientation"    # [I
    .param p4, "view_param"    # [Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;

    .prologue
    .line 677
    const/16 v17, 0x0

    .line 678
    .local v17, "type":I
    const/4 v6, 0x0

    .line 680
    .local v6, "data":[B
    new-instance v7, Lcom/sec/android/gallery3d/panorama360view/drm/DrmManagerClientController;

    sget-object v19, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->SamsungDrmInterface:Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v19

    invoke-direct {v7, v0, v1, v2}, Lcom/sec/android/gallery3d/panorama360view/drm/DrmManagerClientController;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface;)V

    .line 681
    .local v7, "drmManagerClientController":Lcom/sec/android/gallery3d/panorama360view/drm/DrmManagerClientController;
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/panorama360view/drm/DrmManagerClientController;->getMimeType()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_1

    .line 682
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isDRMFile:Z

    .line 686
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isDRMFile:Z

    move/from16 v19, v0

    if-eqz v19, :cond_3

    .line 687
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getCurTime()J

    move-result-wide v8

    .line 688
    .local v8, "drmloadingTime":J
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/panorama360view/drm/DrmManagerClientController;->acquireLicense()Z

    move-result v19

    if-eqz v19, :cond_2

    .line 689
    new-instance v13, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v13}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 691
    .local v13, "outputStream":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-virtual {v7, v13}, Lcom/sec/android/gallery3d/panorama360view/drm/DrmManagerClientController;->unpack(Ljava/io/ByteArrayOutputStream;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 692
    invoke-virtual {v13}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    .line 693
    invoke-static {v6}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->getContentType([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v17

    .line 698
    :cond_0
    :goto_1
    sget-object v19, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "Drm to be unpacked : "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getCurTime()J

    move-result-wide v21

    sub-long v21, v21, v8

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    .end local v8    # "drmloadingTime":J
    .end local v13    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :goto_2
    if-nez v17, :cond_4

    .line 707
    const/4 v6, 0x0

    .line 708
    const/16 v19, 0x0

    .line 775
    :goto_3
    return v19

    .line 684
    :cond_1
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isDRMFile:Z

    goto :goto_0

    .line 695
    .restart local v8    # "drmloadingTime":J
    .restart local v13    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v10

    .line 696
    .local v10, "e":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 700
    .end local v10    # "e":Ljava/io/IOException;
    .end local v13    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :cond_2
    sget-object v19, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "Drm acquireLicense fail : "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getCurTime()J

    move-result-wide v21

    sub-long v21, v21, v8

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 703
    .end local v8    # "drmloadingTime":J
    :cond_3
    invoke-static/range {p2 .. p2}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->getContentType(Ljava/lang/String;)I

    move-result v17

    goto :goto_2

    .line 711
    :cond_4
    const/16 v16, 0x0

    .line 712
    .local v16, "ret":I
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [I

    move-object/from16 v18, v0

    .line 713
    .local v18, "width":[I
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v12, v0, [I

    .line 714
    .local v12, "height":[I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isDRMFile:Z

    move/from16 v19, v0

    if-eqz v19, :cond_5

    .line 715
    move-object/from16 v0, v18

    invoke-static {v6, v0, v12}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getJpegImageSize([B[I[I)V

    .line 719
    :goto_4
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v5, v0, [I

    .line 721
    .local v5, "buffer_size":[I
    new-instance v14, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$PanoramaInitParam;

    invoke-direct {v14}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$PanoramaInitParam;-><init>()V

    .line 722
    .local v14, "param":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$PanoramaInitParam;
    const/16 v19, 0x1

    move/from16 v0, v19

    iput v0, v14, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$PanoramaInitParam;->mode:I

    .line 723
    const/16 v19, 0x1

    move/from16 v0, v19

    iput v0, v14, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$PanoramaInitParam;->render_mode:I

    .line 724
    const/16 v19, 0x0

    aget v19, v18, v19

    move/from16 v0, v19

    iput v0, v14, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$PanoramaInitParam;->input_width:I

    .line 725
    const/16 v19, 0x0

    aget v19, v12, v19

    move/from16 v0, v19

    iput v0, v14, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$PanoramaInitParam;->input_height:I

    .line 726
    sget-object v19, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->FORMAT_STR_RGB888:Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v0, v14, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$PanoramaInitParam;->format:Ljava/lang/String;

    .line 727
    const/16 v19, 0xb4

    move/from16 v0, v19

    iput v0, v14, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$PanoramaInitParam;->max_angle_fov:I

    .line 728
    const/16 v19, 0x1

    move/from16 v0, v19

    iput v0, v14, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$PanoramaInitParam;->scroll_limit_type:I

    .line 729
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v5}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->initialize(Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$PanoramaInitParam;[I)I

    move-result v16

    .line 730
    if-eqz v16, :cond_6

    .line 731
    const/4 v6, 0x0

    .line 732
    const/16 v19, 0x0

    goto/16 :goto_3

    .line 717
    .end local v5    # "buffer_size":[I
    .end local v14    # "param":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$PanoramaInitParam;
    :cond_5
    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-static {v0, v1, v12}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getJpegImageSize(Ljava/lang/String;[I[I)V

    goto :goto_4

    .line 735
    .restart local v5    # "buffer_size":[I
    .restart local v14    # "param":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$PanoramaInitParam;
    :cond_6
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v15, v0, [I

    .line 736
    .local v15, "postview_data_size":[I
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v11, v0, [I

    .line 738
    .local v11, "gallery_data_size":[I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isDRMFile:Z

    move/from16 v19, v0

    if-eqz v19, :cond_8

    .line 739
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v6, v1, v15, v11}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->getPostviewDataInfo([B[I[I[I)I

    move-result v19

    or-int v16, v16, v19

    .line 743
    :goto_5
    if-nez v16, :cond_9

    const/16 v19, 0x0

    aget v19, v15, v19

    if-gtz v19, :cond_7

    const/16 v19, 0x0

    aget v19, v11, v19

    if-lez v19, :cond_9

    .line 744
    :cond_7
    const/16 v19, 0x0

    aget v19, v15, v19

    if-nez v19, :cond_a

    const/16 v19, 0x0

    aget v19, v11, v19

    if-lez v19, :cond_a

    .line 745
    const/16 v19, 0x0

    aget v19, v18, v19

    const/16 v20, 0x0

    aget v20, v12, v20

    const/16 v21, 0x0

    aget v21, v11, v21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getGalleryData(Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;III)Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGalleryData:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;

    .line 746
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGalleryData:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;

    move-object/from16 v19, v0

    if-nez v19, :cond_a

    .line 747
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->finish()I

    .line 748
    const/4 v6, 0x0

    .line 749
    const/16 v19, 0x0

    goto/16 :goto_3

    .line 741
    :cond_8
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2, v15, v11}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->getPostviewDataInfo(Ljava/lang/String;[I[I[I)I

    move-result v19

    or-int v16, v16, v19

    goto :goto_5

    .line 754
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->finish()I

    .line 755
    const/4 v6, 0x0

    .line 756
    const/16 v19, 0x0

    goto/16 :goto_3

    .line 759
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isDRMFile:Z

    move/from16 v19, v0

    if-eqz v19, :cond_c

    .line 760
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGalleryData:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v6, v1, v12, v2}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->decodePostview([B[I[ILcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;)I

    move-result v19

    or-int v16, v16, v19

    .line 765
    :goto_6
    if-nez v16, :cond_d

    .line 766
    if-eqz p4, :cond_b

    const/16 v19, 0x0

    aget-object v19, p4, v19

    if-eqz v19, :cond_b

    const/16 v19, 0x1

    aget-object v19, p4, v19

    if-eqz v19, :cond_b

    .line 767
    const/16 v19, 0x0

    aget-object v19, p4, v19

    const/16 v20, 0x1

    aget-object v20, p4, v20

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->setPostviewParam(Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;)I

    .line 769
    :cond_b
    const/4 v6, 0x0

    .line 770
    const/16 v19, 0x1

    goto/16 :goto_3

    .line 762
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGalleryData:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v12, v3}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->decodePostview(Ljava/lang/String;[I[ILcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;)I

    move-result v19

    or-int v16, v16, v19

    goto :goto_6

    .line 773
    :cond_d
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->finish()I

    .line 774
    const/4 v6, 0x0

    .line 775
    const/16 v19, 0x0

    goto/16 :goto_3
.end method

.method private dismissDialogFragment(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 1200
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPanoramaDialog:[Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    .line 1201
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPanoramaDialog:[Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->dismiss()V

    .line 1202
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPanoramaDialog:[Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 1204
    :cond_0
    return-void
.end method

.method private getAttrIntValue(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I
    .locals 4
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2, "target_name"    # Ljava/lang/String;

    .prologue
    .line 831
    const/4 v1, 0x0

    invoke-interface {p1, v1, p2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 832
    .local v0, "str":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method private getCurTime()J
    .locals 2

    .prologue
    .line 258
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private getFilePath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 657
    if-nez p1, :cond_0

    .line 658
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v1, "uri is null!"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    :goto_0
    return-object v2

    .line 661
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    .line 662
    .local v8, "uri_str":Ljava/lang/String;
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "uri : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    const/4 v7, 0x0

    .line 664
    .local v7, "file_path":Ljava/lang/String;
    const-string v0, "file://"

    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 665
    const-string v0, "file://"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v8, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 666
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "file:// intent start filePath->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    move-object v2, v7

    .line 673
    goto :goto_0

    .line 667
    :cond_2
    const-string v0, "content://"

    invoke-virtual {v8, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 668
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 669
    .local v6, "c":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 670
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 671
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "content:// intent start filePath->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getGalleryData(Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;III)Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    .locals 30
    .param p1, "image_stitcher"    # Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;
    .param p2, "w"    # I
    .param p3, "h"    # I
    .param p4, "gallery_data_size"    # I

    .prologue
    .line 780
    const-string v9, "Description"

    .line 781
    .local v9, "TARGET_TAG":Ljava/lang/String;
    const-string v8, "FullPanoWidthPixels"

    .line 782
    .local v8, "NAME_FP_WIDTH":Ljava/lang/String;
    const-string v7, "FullPanoHeightPixels"

    .line 783
    .local v7, "NAME_FP_HEIGHT":Ljava/lang/String;
    const-string v4, "CroppedAreaLeftPixels"

    .line 784
    .local v4, "NAME_CA_LEFT":Ljava/lang/String;
    const-string v5, "CroppedAreaTopPixels"

    .line 785
    .local v5, "NAME_CA_TOP":Ljava/lang/String;
    const-string v6, "CroppedAreaImageWidthPixels"

    .line 786
    .local v6, "NAME_CA_WIDTH":Ljava/lang/String;
    const-string v3, "CroppedAreaImageHeightPixels"

    .line 788
    .local v3, "NAME_CA_HEIGHT":Ljava/lang/String;
    const/4 v14, 0x0

    .line 789
    .local v14, "data":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    move/from16 v0, p4

    new-array v0, v0, [B

    move-object/from16 v20, v0

    .line 790
    .local v20, "gallery_data":[B
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->getGalleryDataOfAppSeg([B)I

    move-result v23

    .line 791
    .local v23, "ret":I
    new-instance v21, Ljava/lang/String;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 792
    .local v21, "gdata_str":Ljava/lang/String;
    const-string v26, "<"

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v25

    .line 793
    .local v25, "start_ix":I
    if-nez v23, :cond_0

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1

    .line 794
    :cond_0
    const/16 v26, 0x0

    .line 827
    :goto_0
    return-object v26

    .line 796
    :cond_1
    new-instance v24, Ljava/io/StringReader;

    move-object/from16 v0, v21

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 797
    .local v24, "sr":Ljava/io/StringReader;
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v22

    .line 799
    .local v22, "parser":Lorg/xmlpull/v1/XmlPullParser;
    :try_start_0
    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 800
    invoke-interface/range {v22 .. v22}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v17

    .line 801
    .local v17, "event_type":I
    :goto_1
    const/16 v26, 0x1

    move/from16 v0, v17

    move/from16 v1, v26

    if-ne v0, v1, :cond_2

    .line 816
    :goto_2
    iget v0, v14, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;->cropped_area_image_width:I

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-double v12, v0

    .line 817
    .local v12, "c_w":D
    iget v0, v14, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;->cropped_area_image_height:I

    move/from16 v26, v0

    move/from16 v0, v26

    int-to-double v10, v0

    .line 818
    .local v10, "c_h":D
    move/from16 v0, p2

    int-to-double v0, v0

    move-wide/from16 v26, v0

    mul-double v26, v26, v10

    div-double v18, v26, v12

    .line 819
    .local v18, "expect_h":D
    move/from16 v0, p3

    int-to-double v0, v0

    move-wide/from16 v26, v0

    sub-double v26, v18, v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->abs(D)D

    move-result-wide v26

    const-wide/high16 v28, 0x3ff0000000000000L    # 1.0

    cmpl-double v26, v26, v28

    if-lez v26, :cond_4

    .line 820
    const/16 v26, 0x0

    goto :goto_0

    .line 802
    .end local v10    # "c_h":D
    .end local v12    # "c_w":D
    .end local v18    # "expect_h":D
    :cond_2
    const/16 v26, 0x2

    move/from16 v0, v17

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    .line 803
    const-string v26, "Description"

    invoke-interface/range {v22 .. v22}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3

    .line 804
    new-instance v15, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;

    invoke-direct {v15}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1

    .line 805
    .end local v14    # "data":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    .local v15, "data":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    :try_start_1
    const-string v26, "FullPanoWidthPixels"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getAttrIntValue(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    move-result v26

    move/from16 v0, v26

    iput v0, v15, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;->full_pano_width:I

    .line 806
    const-string v26, "FullPanoHeightPixels"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getAttrIntValue(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    move-result v26

    move/from16 v0, v26

    iput v0, v15, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;->full_pano_height:I

    .line 807
    const-string v26, "CroppedAreaLeftPixels"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getAttrIntValue(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    move-result v26

    move/from16 v0, v26

    iput v0, v15, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;->cropped_area_left:I

    .line 808
    const-string v26, "CroppedAreaTopPixels"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getAttrIntValue(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    move-result v26

    move/from16 v0, v26

    iput v0, v15, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;->cropped_area_top:I

    .line 809
    const-string v26, "CroppedAreaImageWidthPixels"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getAttrIntValue(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    move-result v26

    move/from16 v0, v26

    iput v0, v15, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;->cropped_area_image_width:I

    .line 810
    const-string v26, "CroppedAreaImageHeightPixels"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getAttrIntValue(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)I

    move-result v26

    move/from16 v0, v26

    iput v0, v15, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;->cropped_area_image_height:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v14, v15

    .line 811
    .end local v15    # "data":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    .restart local v14    # "data":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    goto/16 :goto_2

    .line 814
    :cond_3
    :try_start_2
    invoke-interface/range {v22 .. v22}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v17

    goto/16 :goto_1

    .line 822
    .end local v17    # "event_type":I
    :catch_0
    move-exception v16

    .line 823
    .local v16, "e":Ljava/io/IOException;
    :goto_3
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    .end local v16    # "e":Ljava/io/IOException;
    :cond_4
    :goto_4
    move-object/from16 v26, v14

    .line 827
    goto/16 :goto_0

    .line 824
    :catch_1
    move-exception v16

    .line 825
    .local v16, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_5
    invoke-virtual/range {v16 .. v16}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_4

    .line 824
    .end local v14    # "data":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    .end local v16    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .restart local v15    # "data":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    .restart local v17    # "event_type":I
    :catch_2
    move-exception v16

    move-object v14, v15

    .end local v15    # "data":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    .restart local v14    # "data":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    goto :goto_5

    .line 822
    .end local v14    # "data":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    .restart local v15    # "data":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    :catch_3
    move-exception v16

    move-object v14, v15

    .end local v15    # "data":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    .restart local v14    # "data":Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    goto :goto_3
.end method

.method private getImageUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 11
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x1

    .line 1076
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1077
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 1078
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 1079
    .local v9, "uri":Landroid/net/Uri;
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1080
    new-array v2, v10, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v5

    .line 1081
    const-string v3, "_data = ?"

    .line 1082
    new-array v4, v10, [Ljava/lang/String;

    aput-object p1, v4, v5

    .line 1083
    const/4 v5, 0x0

    .line 1079
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1084
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ne v1, v10, :cond_0

    .line 1085
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1086
    const-string v1, "_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 1087
    .local v7, "id":J
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 1089
    .end local v7    # "id":J
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1090
    return-object v9
.end method

.method private static getJpegImageSize(Ljava/lang/String;[I[I)V
    .locals 3
    .param p0, "filepath"    # Ljava/lang/String;
    .param p1, "width"    # [I
    .param p2, "height"    # [I

    .prologue
    const/4 v2, 0x0

    .line 837
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 839
    .local v0, "opt":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 840
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 841
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    aput v1, p1, v2

    .line 842
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    aput v1, p2, v2

    .line 843
    return-void
.end method

.method private static getJpegImageSize([B[I[I)V
    .locals 3
    .param p0, "data"    # [B
    .param p1, "width"    # [I
    .param p2, "height"    # [I

    .prologue
    const/4 v2, 0x0

    .line 846
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 848
    .local v0, "opt":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 849
    array-length v1, p0

    invoke-static {p0, v2, v1, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 850
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    aput v1, p1, v2

    .line 851
    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    aput v1, p2, v2

    .line 852
    return-void
.end method

.method private getSensorAngle([D)V
    .locals 7
    .param p1, "mat"    # [D

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 1299
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 1300
    .local v1, "disp_rotation":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoSensorFusion:Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->setRotation(I)I

    .line 1303
    :cond_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfGyroscopeList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 1304
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfGyroscopeList:Ljava/util/ArrayList;

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getSensorDataArray(Ljava/util/ArrayList;)[Ljava/lang/Object;

    move-result-object v0

    .line 1305
    .local v0, "data":[Ljava/lang/Object;
    invoke-direct {p0, v0, v5}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->setInputSensorData([Ljava/lang/Object;I)V

    .line 1307
    .end local v0    # "data":[Ljava/lang/Object;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfAccelerometerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 1308
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfAccelerometerList:Ljava/util/ArrayList;

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getSensorDataArray(Ljava/util/ArrayList;)[Ljava/lang/Object;

    move-result-object v0

    .line 1309
    .restart local v0    # "data":[Ljava/lang/Object;
    const/4 v3, 0x1

    invoke-direct {p0, v0, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->setInputSensorData([Ljava/lang/Object;I)V

    .line 1311
    .end local v0    # "data":[Ljava/lang/Object;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfMagneticFieldList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 1312
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfMagneticFieldList:Ljava/util/ArrayList;

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getSensorDataArray(Ljava/util/ArrayList;)[Ljava/lang/Object;

    move-result-object v0

    .line 1313
    .restart local v0    # "data":[Ljava/lang/Object;
    const/4 v3, 0x2

    invoke-direct {p0, v0, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->setInputSensorData([Ljava/lang/Object;I)V

    .line 1315
    .end local v0    # "data":[Ljava/lang/Object;
    :cond_3
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfRotationVectorList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_4

    .line 1316
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfRotationVectorList:Ljava/util/ArrayList;

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getSensorDataArray(Ljava/util/ArrayList;)[Ljava/lang/Object;

    move-result-object v0

    .line 1317
    .restart local v0    # "data":[Ljava/lang/Object;
    invoke-direct {p0, v0, v6}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->setInputSensorData([Ljava/lang/Object;I)V

    .line 1319
    .end local v0    # "data":[Ljava/lang/Object;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoSensorFusion:Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->calc()I

    .line 1320
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfGyroscopeList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_5

    .line 1321
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfAccelerometerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_5

    .line 1322
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfMagneticFieldList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_5

    .line 1323
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfRotationVectorList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_5

    .line 1324
    const/4 v2, 0x0

    .line 1328
    .local v2, "next":Z
    :goto_0
    if-nez v2, :cond_0

    .line 1329
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfGyroscopeList:Ljava/util/ArrayList;

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->clearArrayList(Ljava/util/ArrayList;)V

    .line 1330
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfAccelerometerList:Ljava/util/ArrayList;

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->clearArrayList(Ljava/util/ArrayList;)V

    .line 1331
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfMagneticFieldList:Ljava/util/ArrayList;

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->clearArrayList(Ljava/util/ArrayList;)V

    .line 1332
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfOrientationList:Ljava/util/ArrayList;

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->clearArrayList(Ljava/util/ArrayList;)V

    .line 1333
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfRotationVectorList:Ljava/util/ArrayList;

    invoke-direct {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->clearArrayList(Ljava/util/ArrayList;)V

    .line 1335
    iget v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mAppSensorFusionMode:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_6

    .line 1337
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoSensorFusion:Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;

    invoke-virtual {v3, v6, p1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->outputRotationMatrix3x3(I[D)I

    .line 1341
    :goto_1
    return-void

    .line 1326
    .end local v2    # "next":Z
    :cond_5
    const/4 v2, 0x1

    .restart local v2    # "next":Z
    goto :goto_0

    .line 1339
    :cond_6
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoSensorFusion:Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;

    invoke-virtual {v3, v5, p1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->outputRotationMatrix3x3(I[D)I

    goto :goto_1
.end method

.method private getSensorDataArray(Ljava/util/ArrayList;)[Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;",
            ">;)[",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 1350
    .local p1, "sd_list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-gtz v4, :cond_2

    .line 1351
    :cond_0
    const/4 v0, 0x0

    .line 1367
    :cond_1
    return-object v0

    .line 1353
    :cond_2
    const/4 v2, 0x0

    .line 1354
    .local v2, "input_num":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1355
    .local v3, "size":I
    const/16 v4, 0x200

    if-lt v3, v4, :cond_3

    .line 1356
    const/16 v2, 0x200

    .line 1360
    :goto_0
    new-array v0, v2, [Ljava/lang/Object;

    .line 1361
    .local v0, "dst":[Ljava/lang/Object;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_4

    .line 1364
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_1

    .line 1365
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1364
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1358
    .end local v0    # "dst":[Ljava/lang/Object;
    .end local v1    # "i":I
    :cond_3
    move v2, v3

    goto :goto_0

    .line 1362
    .restart local v0    # "dst":[Ljava/lang/Object;
    .restart local v1    # "i":I
    :cond_4
    new-instance v5, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;

    iget-wide v6, v4, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;->mTimeStamp:J

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;

    iget-object v4, v4, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;->mValues:[D

    invoke-direct {v5, v6, v7, v4}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;-><init>(J[D)V

    aput-object v5, v0, v1

    .line 1361
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private getShootingSfMode(I)I
    .locals 1
    .param p1, "app_sf_mode"    # I

    .prologue
    .line 1399
    packed-switch p1, :pswitch_data_0

    .line 1422
    const/4 v0, 0x0

    .line 1425
    .local v0, "sf_mode":I
    :goto_0
    return v0

    .line 1401
    .end local v0    # "sf_mode":I
    :pswitch_0
    const/4 v0, 0x0

    .line 1402
    .restart local v0    # "sf_mode":I
    goto :goto_0

    .line 1404
    .end local v0    # "sf_mode":I
    :pswitch_1
    const/4 v0, 0x1

    .line 1405
    .restart local v0    # "sf_mode":I
    goto :goto_0

    .line 1407
    .end local v0    # "sf_mode":I
    :pswitch_2
    const/4 v0, 0x2

    .line 1408
    .restart local v0    # "sf_mode":I
    goto :goto_0

    .line 1410
    .end local v0    # "sf_mode":I
    :pswitch_3
    const/4 v0, 0x3

    .line 1411
    .restart local v0    # "sf_mode":I
    goto :goto_0

    .line 1413
    .end local v0    # "sf_mode":I
    :pswitch_4
    const/4 v0, 0x4

    .line 1414
    .restart local v0    # "sf_mode":I
    goto :goto_0

    .line 1416
    .end local v0    # "sf_mode":I
    :pswitch_5
    const/4 v0, 0x4

    .line 1417
    .restart local v0    # "sf_mode":I
    goto :goto_0

    .line 1419
    .end local v0    # "sf_mode":I
    :pswitch_6
    const/4 v0, 0x1

    .line 1420
    .restart local v0    # "sf_mode":I
    goto :goto_0

    .line 1399
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private initImagerButtonResources()V
    .locals 2

    .prologue
    .line 267
    const v0, 0x7f090001

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBGalleryPlayButton:Landroid/widget/ImageButton;

    .line 268
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBGalleryPlayButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBGalleryPlayButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/gallery3d/panorama360view/ResultApp$7;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$7;-><init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278
    :cond_0
    const v0, 0x7f090002

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBVideoClipPlayerButton:Landroid/widget/ImageButton;

    .line 279
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBVideoClipPlayerButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBVideoClipPlayerButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPlayButtonOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBVideoClipPlayerButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPlayButtonOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 288
    :cond_1
    return-void
.end method

.method private isGLPostView()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1182
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderingMode:I

    if-ne v1, v0, :cond_0

    .line 1183
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPostView:I

    if-nez v1, :cond_0

    .line 1186
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private releasePrevPanoramaResource()V
    .locals 1

    .prologue
    .line 1391
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1392
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->finish()I

    .line 1393
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    .line 1395
    :cond_0
    return-void
.end method

.method private reregisterTexture()V
    .locals 2

    .prologue
    .line 884
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    if-nez v0, :cond_0

    .line 900
    :goto_0
    return-void

    .line 887
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    new-instance v1, Lcom/sec/android/gallery3d/panorama360view/ResultApp$12;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$12;-><init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView;->queueEvent(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private setInputSensorData([Ljava/lang/Object;I)V
    .locals 1
    .param p1, "sd_array"    # [Ljava/lang/Object;
    .param p2, "sensor_type"    # I

    .prologue
    .line 1371
    if-nez p1, :cond_0

    .line 1375
    :goto_0
    return-void

    .line 1374
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoSensorFusion:Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->setSensorData([Ljava/lang/Object;I)I

    goto :goto_0
.end method

.method private setPostviewData()V
    .locals 2

    .prologue
    .line 855
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    if-nez v0, :cond_0

    .line 881
    :goto_0
    return-void

    .line 858
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    new-instance v1, Lcom/sec/android/gallery3d/panorama360view/ResultApp$11;

    invoke-direct {v1, p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$11;-><init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V

    invoke-virtual {v0, v1}, Landroid/opengl/GLSurfaceView;->queueEvent(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private setVideoClipPlayerButtonPause()V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBVideoClipPlayerButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBVideoClipPlayerButton:Landroid/widget/ImageButton;

    const v1, 0x7f020005

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 300
    :cond_0
    return-void
.end method

.method private setVideoClipPlayerButtonPlay()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBVideoClipPlayerButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIBVideoClipPlayerButton:Landroid/widget/ImageButton;

    const v1, 0x7f020009

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 294
    :cond_0
    return-void
.end method

.method private showDialogFragment(I)V
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 1190
    invoke-direct {p0, p1}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->dismissDialogFragment(I)V

    .line 1191
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPanoramaDialog:[Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;

    new-instance v2, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;

    invoke-direct {v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;-><init>()V

    aput-object v2, v1, p1

    .line 1192
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1193
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1194
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPanoramaDialog:[Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;

    aget-object v1, v1, p1

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->setArguments(Landroid/os/Bundle;)V

    .line 1195
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPanoramaDialog:[Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;

    aget-object v1, v1, p1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->setCancelable(Z)V

    .line 1196
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPanoramaDialog:[Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;

    aget-object v1, v1, p1

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1197
    return-void
.end method

.method private showVersion()V
    .locals 8

    .prologue
    .line 1429
    invoke-static {}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->getVersion()Ljava/lang/String;

    move-result-object v2

    .line 1430
    .local v2, "engVersion":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->getVersion()Ljava/lang/String;

    move-result-object v5

    .line 1431
    .local v5, "sfusionVersion":Ljava/lang/String;
    const-string v0, ""

    .line 1433
    .local v0, "AppVersion":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 1436
    .local v4, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 1437
    .local v3, "info":Landroid/content/pm/PackageInfo;
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "App version : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1441
    .end local v3    # "info":Landroid/content/pm/PackageInfo;
    :goto_0
    sget-object v6, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    invoke-static {v6, v0}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1442
    sget-object v6, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1443
    sget-object v6, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    invoke-static {v6, v5}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1444
    return-void

    .line 1438
    :catch_0
    move-exception v1

    .line 1439
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private startPostView(Ljava/lang/String;)V
    .locals 4
    .param p1, "file_path"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1378
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->releasePrevPanoramaResource()V

    .line 1379
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1380
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1381
    const-string v1, "post_view_setting"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1382
    const-string v1, "file_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1383
    const-string v1, "render_low_image"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1384
    const-string v1, "render_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1385
    const-string v1, "disp_type_setting"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1386
    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->startActivity(Landroid/content/Intent;)V

    .line 1387
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->finish()V

    .line 1388
    return-void
.end method

.method private startSlideShowTimer()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 598
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mInertiaScrollTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    if-eqz v0, :cond_0

    .line 599
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mInertiaScrollTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mInertiaScrollTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->cancel()V

    .line 603
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 604
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 605
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderTimer:Ljava/util/Timer;

    .line 607
    :cond_1
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isUseSensorDriven:Z

    .line 608
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 609
    iput v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRotateCount:I

    .line 610
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0, v7}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderTimer:Ljava/util/Timer;

    .line 611
    new-instance v6, Landroid/os/Handler;

    invoke-direct {v6}, Landroid/os/Handler;-><init>()V

    .line 612
    .local v6, "handler":Landroid/os/Handler;
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderTimer:Ljava/util/Timer;

    .line 613
    new-instance v1, Lcom/sec/android/gallery3d/panorama360view/ResultApp$10;

    invoke-direct {v1, p0, v6}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$10;-><init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;Landroid/os/Handler;)V

    .line 640
    const-wide/16 v2, 0xa

    const-wide/16 v4, 0x1

    .line 612
    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 641
    iput-boolean v7, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIsSlideshowPlaying:Z

    .line 642
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->setVideoClipPlayerButtonPause()V

    .line 643
    return-void
.end method

.method private stopSlideShowTimer()V
    .locals 2

    .prologue
    .line 646
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 647
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 648
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderTimer:Ljava/util/Timer;

    .line 649
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 651
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isEnableSensor:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isUseSensorDriven:Z

    .line 652
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIsSlideshowPlaying:Z

    .line 653
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->setVideoClipPlayerButtonPlay()V

    .line 654
    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 1271
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 1167
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1168
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1169
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 1170
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1171
    .local v6, "c":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1172
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1173
    .local v7, "path":Ljava/lang/String;
    invoke-direct {p0, v7}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->startPostView(Ljava/lang/String;)V

    .line 1175
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v7    # "path":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v13, 0xb

    const/4 v12, 0x4

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 344
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 346
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/16 v7, 0x400

    invoke-virtual {v6, v7}, Landroid/view/Window;->addFlags(I)V

    .line 361
    const/high16 v6, 0x7f030000

    invoke-virtual {p0, v6}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->setContentView(I)V

    .line 362
    iput-object p0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mActivity:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    .line 364
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->initImagerButtonResources()V

    .line 366
    new-instance v6, Landroid/view/GestureDetector;

    invoke-direct {v6, p0, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGestureDetector:Landroid/view/GestureDetector;

    .line 367
    new-instance v6, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSGListner:Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;

    invoke-direct {v6, v7, v8}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mScaleGestureDtector:Landroid/view/ScaleGestureDetector;

    .line 369
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->isFinished()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 370
    :cond_0
    new-instance v6, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    invoke-direct {v6}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;-><init>()V

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    .line 372
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 373
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 374
    .local v0, "action":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "action : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    const-string v6, "android.intent.action.VIEW"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "android.intent.action.SEND"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 377
    :cond_2
    const-string v6, "android.intent.action.SEND"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 378
    const-string v6, "android.intent.extra.STREAM"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    .line 379
    .local v5, "uri":Landroid/net/Uri;
    invoke-direct {p0, v5}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mFileName:Ljava/lang/String;

    .line 388
    :goto_0
    iput v10, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPostView:I

    .line 389
    iput v9, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderingMode:I

    .line 390
    const-string v6, "post_view_setting"

    invoke-virtual {v2, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPostView:I

    .line 391
    const-string v6, "render_mode"

    invoke-virtual {v2, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderingMode:I

    .line 392
    const-string v6, "render_low_image"

    invoke-virtual {v2, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isRenderLowImage:Z

    .line 393
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isFileSelect:Z

    .line 394
    iput v10, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDispType:I

    .line 395
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mViewParam:[Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;

    iget-object v7, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPostviewParam:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;

    aput-object v7, v6, v10

    .line 396
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mViewParam:[Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;

    iget-object v7, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPostviewDefaultParam:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;

    aput-object v7, v6, v9

    .line 413
    .end local v5    # "uri":Landroid/net/Uri;
    :goto_1
    sget-object v6, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "target file path : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const-string v6, "sensor_fusion_mode"

    const/4 v7, 0x6

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mAppSensorFusionMode:I

    .line 416
    const-string v6, "com.samsung.android.intent.DRM"

    invoke-virtual {v2, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isDRMFile:Z

    .line 417
    sget-object v6, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "is DRM file : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v8, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isDRMFile:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    iput-boolean v10, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isUseSensorDriven:Z

    .line 421
    const-string v6, "sensor"

    invoke-virtual {p0, v6}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/hardware/SensorManager;

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorManager:Landroid/hardware/SensorManager;

    .line 422
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v7, -0x1

    invoke-virtual {v6, v7}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v4

    .line 423
    .local v4, "sensors":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Sensor;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_c

    .line 437
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGyroscope:Landroid/hardware/Sensor;

    if-nez v6, :cond_10

    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRotationVector:Landroid/hardware/Sensor;

    if-nez v6, :cond_10

    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mAccelerometer:Landroid/hardware/Sensor;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMagneticField:Landroid/hardware/Sensor;

    if-nez v6, :cond_10

    .line 438
    :cond_4
    iput-boolean v10, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isEnableSensor:Z

    .line 443
    :goto_3
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isFileSelect:Z

    if-eqz v6, :cond_11

    .line 445
    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mFileName:Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 446
    .local v1, "f":Ljava/io/File;
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isValidFile:Z

    .line 447
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isValidFile:Z

    .line 448
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isValidFile:Z

    if-nez v6, :cond_5

    sget-object v6, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "not exist! PATH = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 450
    :cond_5
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isValidFile:Z

    if-eqz v6, :cond_6

    .line 451
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isValidFile:Z

    .line 452
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isValidFile:Z

    if-nez v6, :cond_6

    sget-object v6, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "not file! PATH = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    :cond_6
    iget-boolean v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isValidFile:Z

    if-eqz v6, :cond_7

    .line 471
    const/4 v6, 0x3

    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->showDialogFragment(I)V

    .line 472
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getCurTime()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mLoadingTime:J

    .line 475
    :cond_7
    new-instance v6, Ljava/lang/Thread;

    invoke-direct {v6, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mThread:Ljava/lang/Thread;

    .line 476
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mThread:Ljava/lang/Thread;

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 481
    .end local v1    # "f":Ljava/io/File;
    :goto_4
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfGyroscopeList:Ljava/util/ArrayList;

    .line 482
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfAccelerometerList:Ljava/util/ArrayList;

    .line 483
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfMagneticFieldList:Ljava/util/ArrayList;

    .line 484
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfOrientationList:Ljava/util/ArrayList;

    .line 485
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfRotationVectorList:Ljava/util/ArrayList;

    .line 487
    new-instance v6, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;

    invoke-direct {v6}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;-><init>()V

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoSensorFusion:Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;

    .line 488
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoSensorFusion:Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->initialize()I

    .line 489
    iget v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mAppSensorFusionMode:I

    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getShootingSfMode(I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorFusionMode:I

    .line 490
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoSensorFusion:Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;

    iget v7, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorFusionMode:I

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->setMode(I)I

    .line 491
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoSensorFusion:Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->setOffsetMode(I)I

    .line 492
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoSensorFusion:Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;

    invoke-virtual {v6, v9}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->setAppState(I)I

    .line 493
    new-instance v6, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    invoke-direct {v6, v9}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;-><init>(Z)V

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorDrivenTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    .line 494
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorDrivenTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    new-instance v7, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;

    invoke-direct {v7, p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$8;-><init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->setTimerListener(Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer$PanoramaTimerListener;)V

    .line 521
    new-instance v6, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    invoke-direct {v6, v9}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;-><init>(Z)V

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mInertiaScrollTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    .line 522
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mInertiaScrollTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    new-instance v7, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;

    invoke-direct {v7, p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$9;-><init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->setTimerListener(Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer$PanoramaTimerListener;)V

    .line 557
    return-void

    .line 381
    .end local v4    # "sensors":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Sensor;>;"
    :cond_8
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    .line 382
    .restart local v5    # "uri":Landroid/net/Uri;
    if-nez v5, :cond_9

    .line 383
    const-string v6, "file_name"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mFileName:Ljava/lang/String;

    goto/16 :goto_0

    .line 385
    :cond_9
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mFileName:Ljava/lang/String;

    goto/16 :goto_0

    .line 398
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_a
    const-string v6, "com.samsung.android.intent.action.VIEW_360_PHOTO"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 399
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    .line 400
    .restart local v5    # "uri":Landroid/net/Uri;
    invoke-direct {p0, v5}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mFileName:Ljava/lang/String;

    .line 401
    iput v10, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPostView:I

    .line 402
    iput v9, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderingMode:I

    .line 403
    iput-boolean v10, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isRenderLowImage:Z

    .line 404
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isFileSelect:Z

    .line 405
    iput v10, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDispType:I

    goto/16 :goto_1

    .line 407
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_b
    const-string v6, "post_view_setting"

    invoke-virtual {v2, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPostView:I

    .line 408
    const-string v6, "file_name"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mFileName:Ljava/lang/String;

    .line 409
    const-string v6, "render_mode"

    invoke-virtual {v2, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderingMode:I

    .line 410
    iput-boolean v10, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isFileSelect:Z

    .line 411
    iput v10, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDispType:I

    goto/16 :goto_1

    .line 423
    .restart local v4    # "sensors":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Sensor;>;"
    :cond_c
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/Sensor;

    .line 424
    .local v3, "sensor":Landroid/hardware/Sensor;
    invoke-virtual {v3}, Landroid/hardware/Sensor;->getType()I

    move-result v7

    if-ne v7, v12, :cond_d

    .line 425
    iget-object v7, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v7, v12}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGyroscope:Landroid/hardware/Sensor;

    .line 427
    :cond_d
    invoke-virtual {v3}, Landroid/hardware/Sensor;->getType()I

    move-result v7

    if-ne v7, v13, :cond_e

    .line 428
    iget-object v7, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v7, v13}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRotationVector:Landroid/hardware/Sensor;

    .line 430
    :cond_e
    invoke-virtual {v3}, Landroid/hardware/Sensor;->getType()I

    move-result v7

    if-ne v7, v9, :cond_f

    .line 431
    iget-object v7, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v7, v9}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mAccelerometer:Landroid/hardware/Sensor;

    .line 433
    :cond_f
    invoke-virtual {v3}, Landroid/hardware/Sensor;->getType()I

    move-result v7

    if-ne v7, v11, :cond_3

    .line 434
    iget-object v7, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v7, v11}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMagneticField:Landroid/hardware/Sensor;

    goto/16 :goto_2

    .line 440
    .end local v3    # "sensor":Landroid/hardware/Sensor;
    :cond_10
    iput-boolean v9, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isEnableSensor:Z

    goto/16 :goto_3

    .line 478
    :cond_11
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->createGLSurface()V

    goto/16 :goto_4
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 951
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 953
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isFileSelect:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 954
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    if-eqz v0, :cond_0

    .line 955
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->finish()I

    .line 956
    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    .line 959
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 960
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 961
    iput-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderTimer:Ljava/util/Timer;

    .line 964
    :cond_1
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorDrivenTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    if-eqz v0, :cond_2

    .line 965
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorDrivenTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->release()V

    .line 967
    :cond_2
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mInertiaScrollTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    if-eqz v0, :cond_3

    .line 968
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mInertiaScrollTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->release()V

    .line 970
    :cond_3
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mActionBarTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    if-eqz v0, :cond_4

    .line 971
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mActionBarTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->release()V

    .line 973
    :cond_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mScaleBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    .line 974
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mScaleBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 976
    :cond_5
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mOrgBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_6

    .line 977
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mOrgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 980
    :cond_6
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1095
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->stopSlideShowTimer()V

    .line 1096
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onDoubleTap"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1097
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->setDefault()V

    .line 1098
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 1099
    const/4 v0, 0x1

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1104
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onDoubleTapEvent"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1105
    const/4 v0, 0x0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1121
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onDown"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1122
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 1128
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onFling"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1129
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1134
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onLongPress"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1135
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 932
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 933
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 935
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->showVersion()V

    .line 936
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    if-eqz v0, :cond_0

    .line 937
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v0}, Landroid/opengl/GLSurfaceView;->onPause()V

    .line 939
    :cond_0
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_1

    .line 940
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 942
    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 904
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 905
    sget-object v1, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onResume"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->showVersion()V

    .line 907
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    if-eqz v1, :cond_0

    .line 908
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v1}, Landroid/opengl/GLSurfaceView;->onResume()V

    .line 910
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGyroscope:Landroid/hardware/Sensor;

    if-eqz v1, :cond_1

    .line 911
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGyroscope:Landroid/hardware/Sensor;

    invoke-virtual {v1, p0, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 913
    :cond_1
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRotationVector:Landroid/hardware/Sensor;

    if-eqz v1, :cond_2

    .line 914
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRotationVector:Landroid/hardware/Sensor;

    invoke-virtual {v1, p0, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 916
    :cond_2
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mAccelerometer:Landroid/hardware/Sensor;

    if-eqz v1, :cond_3

    .line 917
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mAccelerometer:Landroid/hardware/Sensor;

    invoke-virtual {v1, p0, v2, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 919
    :cond_3
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMagneticField:Landroid/hardware/Sensor;

    if-eqz v1, :cond_4

    .line 920
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMagneticField:Landroid/hardware/Sensor;

    invoke-virtual {v1, p0, v2, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 923
    :cond_4
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 924
    .local v0, "filters":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.cover.OPEN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 925
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 928
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 1140
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onScroll"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1141
    const/4 v0, 0x0

    return v0
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 1275
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isTouching:Z

    if-eqz v1, :cond_0

    .line 1294
    :goto_0
    return-void

    .line 1276
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorDrivenTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorDrivenTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->isStarted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1277
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorDrivenTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    const-wide/16 v2, 0x14

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->start(J)V

    .line 1279
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSensorLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 1280
    :try_start_0
    new-instance v0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;

    iget-wide v3, p1, Landroid/hardware/SensorEvent;->timestamp:J

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-direct {v0, v3, v4, v1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;-><init>(J[F)V

    .line 1281
    .local v0, "sd":Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGyroscope:Landroid/hardware/Sensor;

    if-ne v1, v3, :cond_2

    .line 1282
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfGyroscopeList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1284
    :cond_2
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRotationVector:Landroid/hardware/Sensor;

    if-ne v1, v3, :cond_3

    .line 1285
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfRotationVectorList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1287
    :cond_3
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mAccelerometer:Landroid/hardware/Sensor;

    if-ne v1, v3, :cond_4

    .line 1288
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfAccelerometerList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1290
    :cond_4
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMagneticField:Landroid/hardware/Sensor;

    if-ne v1, v3, :cond_5

    .line 1291
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPartOfMagneticFieldList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1279
    :cond_5
    monitor-exit v2

    goto :goto_0

    .end local v0    # "sd":Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1146
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onShowPress"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1147
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1110
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onSingleTapConfirmed"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1151
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onSingleTapUp"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    const/4 v0, 0x0

    return v0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 946
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 947
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1070
    sget-object v0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onTouch"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1072
    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 1011
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->stopSlideShowTimer()V

    .line 1012
    sget-object v2, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v3, "onTouchEvent"

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isGLPostView()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    if-nez v2, :cond_1

    .line 1065
    :cond_0
    :goto_0
    return v5

    .line 1016
    :cond_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1019
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mScaleGestureDtector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1020
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-le v2, v5, :cond_2

    .line 1022
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isMoveDisable:Z

    goto :goto_0

    .line 1025
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1026
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 1027
    .local v1, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1062
    :cond_3
    :goto_1
    iput v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPreviousX:F

    .line 1063
    iput v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPreviousY:F

    goto :goto_0

    .line 1029
    :pswitch_0
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isMoveDisable:Z

    .line 1030
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isTouching:Z

    .line 1032
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mInertiaScrollTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->isStarted()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1033
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mInertiaScrollTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->cancel()V

    goto :goto_1

    .line 1037
    :pswitch_1
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isTouching:Z

    .line 1038
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isMoveDisable:Z

    if-nez v2, :cond_3

    .line 1039
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mInertiaScrollTimer:Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;

    const-wide/16 v3, 0x14

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/panorama360view/PanoramaTimer;->start(J)V

    goto :goto_1

    .line 1043
    :pswitch_2
    iget-boolean v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isMoveDisable:Z

    if-nez v2, :cond_3

    .line 1044
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    if-eqz v2, :cond_4

    .line 1045
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mNorthPolePos:Landroid/graphics/Point;

    iget-object v4, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSouthPolePos:Landroid/graphics/Point;

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->getPolesDispPosition(Landroid/graphics/Point;Landroid/graphics/Point;)I

    .line 1047
    :cond_4
    iget v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPreviousX:F

    sub-float v2, v0, v2

    iput v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffX:F

    .line 1048
    iget v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPreviousY:F

    sub-float v2, v1, v2

    iput v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffY:F

    .line 1049
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mNorthPolePos:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    if-ltz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mNorthPolePos:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    cmpg-float v2, v1, v2

    if-ltz v2, :cond_6

    .line 1050
    :cond_5
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSouthPolePos:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    if-ltz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mSouthPolePos:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-lez v2, :cond_7

    .line 1051
    :cond_6
    iget v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffX:F

    const/high16 v3, -0x40800000    # -1.0f

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffX:F

    .line 1053
    :cond_7
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    iget v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffX:F

    iget v4, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mDiffY:F

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->setSwipeDistance(FF)V

    .line 1054
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;

    invoke-virtual {v2}, Landroid/opengl/GLSurfaceView;->requestRender()V

    goto :goto_1

    .line 1059
    :pswitch_3
    iput-boolean v4, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isTouching:Z

    goto :goto_1

    .line 1027
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public run()V
    .locals 5

    .prologue
    .line 560
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 562
    .local v0, "msg":Landroid/os/Message;
    iget-boolean v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isFileSelect:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isValidFile:Z

    if-nez v1, :cond_0

    .line 563
    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 572
    :goto_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 573
    return-void

    .line 565
    :cond_0
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mFileName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mExifOrientation:[I

    iget-object v4, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mViewParam:[Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->decodePostViewData(Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;Ljava/lang/String;[I[Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$ViewParam;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 566
    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->arg1:I

    goto :goto_0

    .line 568
    :cond_1
    sget-object v1, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;

    const-string v2, "decodePostViewData : failed!"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->arg1:I

    goto :goto_0
.end method
