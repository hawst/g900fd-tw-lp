.class public Lcom/sec/android/gallery3d/panorama360view/MathUtil;
.super Ljava/lang/Object;
.source "MathUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAngleDiff([D[D[D)V
    .locals 17
    .param p0, "angle"    # [D
    .param p1, "mat"    # [D
    .param p2, "prev_mat"    # [D

    .prologue
    .line 6
    move-object/from16 v0, p0

    array-length v11, v0

    const/4 v12, 0x3

    if-ne v11, v12, :cond_0

    move-object/from16 v0, p1

    array-length v11, v0

    const/16 v12, 0x9

    if-ne v11, v12, :cond_0

    move-object/from16 v0, p2

    array-length v11, v0

    const/16 v12, 0x9

    if-eq v11, v12, :cond_1

    .line 18
    :cond_0
    :goto_0
    return-void

    .line 9
    :cond_1
    const/4 v11, 0x0

    aget-wide v11, p2, v11

    const/4 v13, 0x1

    aget-wide v13, p1, v13

    mul-double/2addr v11, v13

    const/4 v13, 0x3

    aget-wide v13, p2, v13

    const/4 v15, 0x4

    aget-wide v15, p1, v15

    mul-double/2addr v13, v15

    add-double/2addr v11, v13

    const/4 v13, 0x6

    aget-wide v13, p2, v13

    const/4 v15, 0x7

    aget-wide v15, p1, v15

    mul-double/2addr v13, v15

    add-double v1, v11, v13

    .line 10
    .local v1, "rd1":D
    const/4 v11, 0x1

    aget-wide v11, p2, v11

    const/4 v13, 0x1

    aget-wide v13, p1, v13

    mul-double/2addr v11, v13

    const/4 v13, 0x4

    aget-wide v13, p2, v13

    const/4 v15, 0x4

    aget-wide v15, p1, v15

    mul-double/2addr v13, v15

    add-double/2addr v11, v13

    const/4 v13, 0x7

    aget-wide v13, p2, v13

    const/4 v15, 0x7

    aget-wide v15, p1, v15

    mul-double/2addr v13, v15

    add-double v3, v11, v13

    .line 11
    .local v3, "rd4":D
    const/4 v11, 0x2

    aget-wide v11, p2, v11

    const/4 v13, 0x0

    aget-wide v13, p1, v13

    mul-double/2addr v11, v13

    const/4 v13, 0x5

    aget-wide v13, p2, v13

    const/4 v15, 0x3

    aget-wide v15, p1, v15

    mul-double/2addr v13, v15

    add-double/2addr v11, v13

    const/16 v13, 0x8

    aget-wide v13, p2, v13

    const/4 v15, 0x6

    aget-wide v15, p1, v15

    mul-double/2addr v13, v15

    add-double v5, v11, v13

    .line 12
    .local v5, "rd6":D
    const/4 v11, 0x2

    aget-wide v11, p2, v11

    const/4 v13, 0x1

    aget-wide v13, p1, v13

    mul-double/2addr v11, v13

    const/4 v13, 0x5

    aget-wide v13, p2, v13

    const/4 v15, 0x4

    aget-wide v15, p1, v15

    mul-double/2addr v13, v15

    add-double/2addr v11, v13

    const/16 v13, 0x8

    aget-wide v13, p2, v13

    const/4 v15, 0x7

    aget-wide v15, p1, v15

    mul-double/2addr v13, v15

    add-double v7, v11, v13

    .line 13
    .local v7, "rd7":D
    const/4 v11, 0x2

    aget-wide v11, p2, v11

    const/4 v13, 0x2

    aget-wide v13, p1, v13

    mul-double/2addr v11, v13

    const/4 v13, 0x5

    aget-wide v13, p2, v13

    const/4 v15, 0x5

    aget-wide v15, p1, v15

    mul-double/2addr v13, v15

    add-double/2addr v11, v13

    const/16 v13, 0x8

    aget-wide v13, p2, v13

    const/16 v15, 0x8

    aget-wide v15, p1, v15

    mul-double/2addr v13, v15

    add-double v9, v11, v13

    .line 15
    .local v9, "rd8":D
    const/4 v11, 0x0

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v12

    aput-wide v12, p0, v11

    .line 16
    const/4 v11, 0x1

    neg-double v12, v7

    invoke-static {v12, v13}, Ljava/lang/Math;->asin(D)D

    move-result-wide v12

    aput-wide v12, p0, v11

    .line 17
    const/4 v11, 0x2

    neg-double v12, v5

    invoke-static {v12, v13, v9, v10}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v12

    aput-wide v12, p0, v11

    goto/16 :goto_0
.end method
