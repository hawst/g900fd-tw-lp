.class Lcom/sec/android/gallery3d/panorama360view/ResultApp$5;
.super Ljava/lang/Object;
.source "ResultApp.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/panorama360view/ResultApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$5;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    .line 313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x0

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    .line 317
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$5;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mIsSlideshowPlaying:Z
    invoke-static {v6}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$11(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 318
    const v3, 0x7f06000c

    .line 322
    .local v3, "str_id":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$5;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mActivity:Lcom/sec/android/gallery3d/panorama360view/ResultApp;
    invoke-static {v6}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$13(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    move-result-object v6

    invoke-static {v6, v3, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    .line 325
    .local v4, "t":Landroid/widget/Toast;
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 326
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$5;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 327
    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    .line 328
    .local v0, "logicalDensity":F
    const/high16 v6, 0x420c0000    # 35.0f

    mul-float/2addr v6, v0

    float-to-double v6, v6

    add-double/2addr v6, v8

    double-to-int v2, v6

    .line 329
    .local v2, "right":I
    const/high16 v6, 0x42400000    # 48.0f

    mul-float/2addr v6, v0

    float-to-double v6, v6

    add-double/2addr v6, v8

    double-to-int v5, v6

    .line 332
    .local v5, "top":I
    const/16 v6, 0x35

    invoke-virtual {v4, v6, v2, v5}, Landroid/widget/Toast;->setGravity(III)V

    .line 333
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 334
    return v10

    .line 320
    .end local v0    # "logicalDensity":F
    .end local v1    # "metrics":Landroid/util/DisplayMetrics;
    .end local v2    # "right":I
    .end local v3    # "str_id":I
    .end local v4    # "t":Landroid/widget/Toast;
    .end local v5    # "top":I
    :cond_0
    const v3, 0x7f06000b

    .restart local v3    # "str_id":I
    goto :goto_0
.end method
