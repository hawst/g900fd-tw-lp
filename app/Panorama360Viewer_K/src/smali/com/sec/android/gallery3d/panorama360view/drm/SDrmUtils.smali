.class public Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;
.super Lcom/sec/android/gallery3d/panorama360view/drm/SDrmParameter;
.source "SDrmUtils.java"


# static fields
.field public static final SDRM_JPG_EXTENSION:Ljava/lang/String; = "jpg"

.field public static SamsungDrmInterface:Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface; = null

.field private static final TAG:Ljava/lang/String; = "SDrmUtils"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils$1;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils$1;-><init>()V

    sput-object v0, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->SamsungDrmInterface:Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface;

    .line 297
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmParameter;-><init>()V

    return-void
.end method

.method public static acquireLicense(Landroid/drm/DrmManagerClient;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "drmManagerClient"    # Landroid/drm/DrmManagerClient;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 180
    const-string v5, "SDrmUtils"

    const-string v6, "Start acquireLicense => "

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    if-nez p1, :cond_0

    .line 183
    const-string v5, "SDrmUtils"

    const-string v6, "acquireLicense : filePath is null."

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v4

    .line 211
    :goto_0
    return-object v2

    .line 187
    :cond_0
    invoke-static {p1}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->isSDRMFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 188
    .local v2, "mimeType":Ljava/lang/String;
    const-string v5, "SDrmUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "mimeType :"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 190
    const-string v5, "SDrmUtils"

    const-string v6, "acquireLicense => this file is not DRM. - "

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v4

    .line 191
    goto :goto_0

    .line 194
    :cond_1
    invoke-static {p0, p1, v2}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->setDrmManagerClientHandle(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    move-object v2, v4

    .line 195
    goto :goto_0

    .line 197
    :cond_2
    invoke-virtual {p0, p1}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v3

    .line 199
    .local v3, "rightStatus":I
    if-nez v3, :cond_3

    .line 200
    const-string v4, "SDrmUtils"

    const-string v5, "acquireLicense => this file is valid. "

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 203
    :cond_3
    invoke-static {p1, v2}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->buildSDRMInfoRequestRightsAcquisitionInfo(Ljava/lang/String;Ljava/lang/String;)Landroid/drm/DrmInfoRequest;

    move-result-object v1

    .line 205
    .local v1, "drmInfoRequest":Landroid/drm/DrmInfoRequest;
    invoke-virtual {p0, v1}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v0

    .line 206
    .local v0, "drmInfo":Landroid/drm/DrmInfo;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/drm/DrmInfo;->getData()[B

    move-result-object v5

    if-eqz v5, :cond_4

    .line 207
    invoke-virtual {p0, v0}, Landroid/drm/DrmManagerClient;->processDrmInfo(Landroid/drm/DrmInfo;)I

    goto :goto_0

    .line 210
    :cond_4
    const-string v5, "SDrmUtils"

    const-string v6, "acquireLicense => error get acquireDrmInfo "

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v4

    .line 211
    goto :goto_0
.end method

.method public static acquireLicense(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p0, "drmManagerClient"    # Landroid/drm/DrmManagerClient;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 224
    const-string v3, "SDrmUtils"

    const-string v4, "Start acquireLicense => "

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    invoke-static {p0, p1, p2}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->setDrmManagerClientHandle(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 236
    :goto_0
    return v2

    .line 228
    :cond_0
    invoke-static {p1, p2}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->buildSDRMInfoRequestRightsAcquisitionInfo(Ljava/lang/String;Ljava/lang/String;)Landroid/drm/DrmInfoRequest;

    move-result-object v1

    .line 230
    .local v1, "drmInfoRequest":Landroid/drm/DrmInfoRequest;
    invoke-virtual {p0, v1}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v0

    .line 231
    .local v0, "drmInfo":Landroid/drm/DrmInfo;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/drm/DrmInfo;->getData()[B

    move-result-object v3

    if-eqz v3, :cond_1

    .line 232
    invoke-virtual {p0, v0}, Landroid/drm/DrmManagerClient;->processDrmInfo(Landroid/drm/DrmInfo;)I

    .line 233
    const/4 v2, 0x1

    goto :goto_0

    .line 235
    :cond_1
    const-string v3, "SDrmUtils"

    const-string v4, "acquireLicense => error get acquireDrmInfo "

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static buildSDRMInfoRequestParse(Ljava/lang/String;Ljava/lang/String;I)Landroid/drm/DrmInfoRequest;
    .locals 3
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "buffersize"    # I

    .prologue
    .line 106
    new-instance v0, Landroid/drm/DrmInfoRequest;

    const/16 v1, 0x33

    invoke-direct {v0, v1, p1}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 107
    .local v0, "drmInfoRequest":Landroid/drm/DrmInfoRequest;
    const-string v1, "drm_path"

    invoke-virtual {v0, v1, p0}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 108
    const-string v1, "drm_buffer_size"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 109
    const-string v1, "drm_eof"

    const-string v2, "false"

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 110
    const-string v1, "drm_file_status"

    const-string v2, "close"

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 111
    return-object v0
.end method

.method public static buildSDRMInfoRequestRightsAcquisitionInfo(Ljava/lang/String;Ljava/lang/String;)Landroid/drm/DrmInfoRequest;
    .locals 3
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 85
    new-instance v0, Landroid/drm/DrmInfoRequest;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p1}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 86
    .local v0, "drmInfoRequest":Landroid/drm/DrmInfoRequest;
    const-string v1, "drm_path"

    invoke-virtual {v0, v1, p0}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 87
    const-string v1, "media_hub"

    const-string v2, "Media_hub"

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 88
    const-string v1, "app_Id"

    const-string v2, "0418hwir93"

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 89
    const-string v1, "user_guid"

    const-string v2, "vobcwu502h"

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 90
    const-string v1, "imei"

    const-string v2, "356567050038866"

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 91
    const-string v1, "orderId"

    const-string v2, "10070075201783682"

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 92
    const-string v1, "mv_id"

    const-string v2, "V"

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 93
    const-string v1, "svr_id"

    const-string v2, "STG"

    invoke-virtual {v0, v1, v2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 94
    return-object v0
.end method

.method public static isEOF(Landroid/drm/DrmInfo;)Z
    .locals 2
    .param p0, "drmInfo"    # Landroid/drm/DrmInfo;

    .prologue
    .line 152
    if-eqz p0, :cond_0

    const-string v1, "drm_eof"

    invoke-virtual {p0, v1}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 153
    :cond_0
    const/4 v1, 0x1

    .line 156
    :goto_0
    return v1

    .line 154
    :cond_1
    const-string v1, "drm_eof"

    invoke-virtual {p0, v1}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, "eofstatus":Ljava/lang/String;
    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isFileOpenStatus(Landroid/drm/DrmInfo;)Z
    .locals 2
    .param p0, "drmInfo"    # Landroid/drm/DrmInfo;

    .prologue
    .line 139
    if-eqz p0, :cond_0

    const-string v1, "drm_file_status"

    invoke-virtual {p0, v1}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 140
    :cond_0
    const/4 v1, 0x0

    .line 143
    :goto_0
    return v1

    .line 141
    :cond_1
    const-string v1, "drm_file_status"

    invoke-virtual {p0, v1}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, "fileOpenStatus":Ljava/lang/String;
    const-string v1, "open"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isSDRMFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 57
    if-nez p0, :cond_0

    .line 58
    const-string v1, "SDrmUtils"

    const-string v2, "isSDRMFile : filePath is null."

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const/4 v0, 0x0

    .line 75
    :goto_0
    return-object v0

    .line 63
    :cond_0
    const/4 v0, 0x0

    .line 65
    .local v0, "mimeType":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "sev"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    const-string v0, "env/vnd.sdrm-media.sev"

    .line 73
    :goto_1
    const-string v1, "SDrmUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mimeType :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 67
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "sm4"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 68
    const-string v0, "video/vnd.sdrm-media.sm4"

    .line 69
    goto :goto_1

    .line 70
    :cond_2
    const-string v1, "SDrmUtils"

    const-string v2, "isSDRMFile : this file is not SDRM. => "

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static isValidDrmFile(Landroid/drm/DrmManagerClient;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "drmManagerClient"    # Landroid/drm/DrmManagerClient;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 248
    if-nez p1, :cond_0

    .line 249
    const-string v3, "SDrmUtils"

    const-string v4, "isValidDrmFile : filePath is null."

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 269
    :goto_0
    return-object v0

    .line 252
    :cond_0
    invoke-static {p1}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->isSDRMFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 253
    .local v0, "mimeType":Ljava/lang/String;
    const-string v3, "SDrmUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "mimeType :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 255
    const-string v3, "SDrmUtils"

    const-string v4, "isValidDrmFile => this file is not DRM. - "

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 256
    goto :goto_0

    .line 259
    :cond_1
    invoke-static {p0, p1, v0}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->setDrmManagerClientHandle(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    move-object v0, v2

    .line 260
    goto :goto_0

    .line 262
    :cond_2
    invoke-virtual {p0, p1}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v1

    .line 264
    .local v1, "rightStatus":I
    if-nez v1, :cond_3

    .line 265
    const-string v2, "SDrmUtils"

    const-string v3, "isValidDrmFile => this file is valid. "

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 268
    :cond_3
    const-string v3, "SDrmUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "isValidDrmFile => this file is not valid.  rightStatus = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 269
    goto :goto_0
.end method

.method public static isValidDrmFile(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p0, "drmManagerClient"    # Landroid/drm/DrmManagerClient;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 281
    const-string v2, "SDrmUtils"

    const-string v3, "isValidDrmFile start "

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    invoke-static {p0, p1, p2}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->setDrmManagerClientHandle(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 292
    :goto_0
    return v1

    .line 285
    :cond_0
    invoke-virtual {p0, p1}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v0

    .line 287
    .local v0, "rightStatus":I
    if-nez v0, :cond_1

    .line 288
    const-string v1, "SDrmUtils"

    const-string v2, "isValidDrmFile => this file is valid. "

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    const/4 v1, 0x1

    goto :goto_0

    .line 291
    :cond_1
    const-string v2, "SDrmUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "isValidDrmFile => this file is not valid.  rightStatus = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static setDrmInfoRequestFileOffset(Landroid/drm/DrmInfoRequest;I)V
    .locals 2
    .param p0, "drmInfoRequest"    # Landroid/drm/DrmInfoRequest;
    .param p1, "offset"    # I

    .prologue
    .line 129
    if-eqz p0, :cond_0

    .line 130
    const-string v0, "drm_offset"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 131
    :cond_0
    return-void
.end method

.method public static setDrmInfoRequestFileOpenStatus(Landroid/drm/DrmInfoRequest;)V
    .locals 2
    .param p0, "drmInfoRequest"    # Landroid/drm/DrmInfoRequest;

    .prologue
    .line 120
    if-eqz p0, :cond_0

    .line 121
    const-string v0, "drm_file_status"

    const-string v1, "open"

    invoke-virtual {p0, v0, v1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 122
    :cond_0
    return-void
.end method

.method public static setDrmManagerClientHandle(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "drmManagerClient"    # Landroid/drm/DrmManagerClient;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 167
    if-eqz p0, :cond_0

    .line 168
    invoke-virtual {p0, p1, p2}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 170
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static unpack(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface$UnpackCallback;)Z
    .locals 11
    .param p0, "drmManagerClient"    # Landroid/drm/DrmManagerClient;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface$UnpackCallback;

    .prologue
    .line 372
    const/high16 v0, 0xa0000

    .line 374
    .local v0, "bufferSize":I
    invoke-static {p1, p2, v0}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->buildSDRMInfoRequestParse(Ljava/lang/String;Ljava/lang/String;I)Landroid/drm/DrmInfoRequest;

    move-result-object v3

    .line 375
    .local v3, "drmInfoRequest":Landroid/drm/DrmInfoRequest;
    invoke-virtual {p0, v3}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v2

    .line 377
    .local v2, "drmInfo":Landroid/drm/DrmInfo;
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->isFileOpenStatus(Landroid/drm/DrmInfo;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 378
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 379
    .local v5, "start":J
    const/4 v4, 0x0

    .line 380
    .local v4, "offset":I
    invoke-interface {p3}, Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface$UnpackCallback;->initUnpack()V

    .line 381
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->setDrmInfoRequestFileOpenStatus(Landroid/drm/DrmInfoRequest;)V

    .line 382
    :goto_0
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->isEOF(Landroid/drm/DrmInfo;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 388
    const-string v7, "SDrmUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "unpackJPG excution time : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v5

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ms"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    invoke-interface {p3}, Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface$UnpackCallback;->finishUnpack()V

    .line 390
    const/4 v7, 0x1

    .line 394
    .end local v4    # "offset":I
    .end local v5    # "start":J
    :goto_1
    return v7

    .line 383
    .restart local v4    # "offset":I
    .restart local v5    # "start":J
    :cond_0
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->setDrmInfoRequestFileOffset(Landroid/drm/DrmInfoRequest;I)V

    .line 384
    invoke-virtual {p0, v3}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v2

    .line 385
    invoke-virtual {v2}, Landroid/drm/DrmInfo;->getData()[B

    move-result-object v1

    .line 386
    .local v1, "decData":[B
    invoke-interface {p3, v1, v4}, Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface$UnpackCallback;->unpacked([BI)V

    goto :goto_0

    .line 392
    .end local v1    # "decData":[B
    .end local v4    # "offset":I
    .end local v5    # "start":J
    :cond_1
    const-string v7, "SDrmUtils"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "unpackJPG : Failed to file open : "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public static unpack(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;Ljava/io/ByteArrayOutputStream;)Z
    .locals 12
    .param p0, "drmManagerClient"    # Landroid/drm/DrmManagerClient;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "outputStream"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 405
    const/high16 v0, 0xa0000

    .line 407
    .local v0, "bufferSize":I
    invoke-static {p1, p2, v0}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->buildSDRMInfoRequestParse(Ljava/lang/String;Ljava/lang/String;I)Landroid/drm/DrmInfoRequest;

    move-result-object v3

    .line 408
    .local v3, "drmInfoRequest":Landroid/drm/DrmInfoRequest;
    invoke-virtual {p0, v3}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v2

    .line 410
    .local v2, "drmInfo":Landroid/drm/DrmInfo;
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->isFileOpenStatus(Landroid/drm/DrmInfo;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 411
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 412
    .local v5, "start":J
    const/4 v4, 0x0

    .line 413
    .local v4, "offset":I
    const/4 v7, 0x0

    .line 414
    .local v7, "started":Z
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->setDrmInfoRequestFileOpenStatus(Landroid/drm/DrmInfoRequest;)V

    .line 415
    :goto_0
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->isEOF(Landroid/drm/DrmInfo;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 432
    const-string v8, "SDrmUtils"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "unpackJPG excution time : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v5

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ms"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    const/4 v8, 0x1

    .line 437
    .end local v4    # "offset":I
    .end local v5    # "start":J
    .end local v7    # "started":Z
    :goto_1
    return v8

    .line 416
    .restart local v4    # "offset":I
    .restart local v5    # "start":J
    .restart local v7    # "started":Z
    :cond_0
    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->setDrmInfoRequestFileOffset(Landroid/drm/DrmInfoRequest;I)V

    .line 417
    invoke-virtual {p0, v3}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v2

    .line 418
    invoke-virtual {v2}, Landroid/drm/DrmInfo;->getData()[B

    move-result-object v1

    .line 430
    .local v1, "decData":[B
    invoke-virtual {p3, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    goto :goto_0

    .line 435
    .end local v1    # "decData":[B
    .end local v4    # "offset":I
    .end local v5    # "start":J
    .end local v7    # "started":Z
    :cond_1
    const-string v8, "SDrmUtils"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "unpackJPG : Failed to file open : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const/4 v8, 0x0

    goto :goto_1
.end method

.method public static unpackJPG(Landroid/drm/DrmManagerClient;Ljava/lang/String;)Z
    .locals 16
    .param p0, "drmManagerClient"    # Landroid/drm/DrmManagerClient;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 306
    if-nez p1, :cond_0

    .line 307
    const-string v12, "SDrmUtils"

    const-string v13, "unpackJPG : filePath is null."

    invoke-static {v12, v13}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const/4 v12, 0x0

    .line 361
    :goto_0
    return v12

    .line 311
    :cond_0
    invoke-static/range {p0 .. p1}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->acquireLicense(Landroid/drm/DrmManagerClient;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 312
    .local v6, "mimeType":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 313
    const-string v12, "SDrmUtils"

    const-string v13, "unpackJPG => this file is not ready to play. - "

    invoke-static {v12, v13}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    const/4 v12, 0x0

    goto :goto_0

    .line 317
    :cond_1
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 318
    .local v8, "outFile":Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "jpg"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 319
    const-string v12, "SDrmUtils"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "outFile : "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    const/high16 v1, 0xa0000

    .line 323
    .local v1, "bufferSize":I
    move-object/from16 v0, p1

    invoke-static {v0, v6, v1}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->buildSDRMInfoRequestParse(Ljava/lang/String;Ljava/lang/String;I)Landroid/drm/DrmInfoRequest;

    move-result-object v4

    .line 326
    .local v4, "drmInfoRequest":Landroid/drm/DrmInfoRequest;
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v3

    .line 328
    .local v3, "drmInfo":Landroid/drm/DrmInfo;
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->isFileOpenStatus(Landroid/drm/DrmInfo;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 329
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 330
    .local v10, "start":J
    const/4 v7, 0x0

    .line 331
    .local v7, "offset":I
    const/4 v9, 0x0

    .line 333
    .local v9, "outFileStream":Ljava/io/OutputStream;
    :try_start_0
    new-instance v9, Ljava/io/FileOutputStream;

    .end local v9    # "outFileStream":Ljava/io/OutputStream;
    invoke-direct {v9, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    .restart local v9    # "outFileStream":Ljava/io/OutputStream;
    invoke-static {v4}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->setDrmInfoRequestFileOpenStatus(Landroid/drm/DrmInfoRequest;)V

    .line 339
    :goto_1
    invoke-static {v3}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->isEOF(Landroid/drm/DrmInfo;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 350
    :goto_2
    const-string v12, "SDrmUtils"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "unpackJPG excution time : "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sub-long/2addr v14, v10

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " ms"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    :try_start_1
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 357
    :goto_3
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 334
    .end local v9    # "outFileStream":Ljava/io/OutputStream;
    :catch_0
    move-exception v5

    .line 335
    .local v5, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v5}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 336
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 340
    .end local v5    # "e":Ljava/io/FileNotFoundException;
    .restart local v9    # "outFileStream":Ljava/io/OutputStream;
    :cond_2
    invoke-static {v4, v7}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->setDrmInfoRequestFileOffset(Landroid/drm/DrmInfoRequest;I)V

    .line 341
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v3

    .line 343
    :try_start_2
    invoke-virtual {v3}, Landroid/drm/DrmInfo;->getData()[B

    move-result-object v2

    .line 344
    .local v2, "decData":[B
    invoke-virtual {v9, v2}, Ljava/io/OutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 345
    .end local v2    # "decData":[B
    :catch_1
    move-exception v5

    .line 346
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 354
    .end local v5    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v5

    .line 355
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 359
    .end local v5    # "e":Ljava/io/IOException;
    .end local v7    # "offset":I
    .end local v9    # "outFileStream":Ljava/io/OutputStream;
    .end local v10    # "start":J
    :cond_3
    const-string v12, "SDrmUtils"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "unpackJPG : Failed to file open : "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const/4 v12, 0x0

    goto/16 :goto_0
.end method
