.class Lcom/sec/android/gallery3d/panorama360view/ResultApp$11;
.super Ljava/lang/Object;
.source "ResultApp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/panorama360view/ResultApp;->setPostviewData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$11;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    .line 858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 862
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 863
    .local v0, "msg":Landroid/os/Message;
    iput v2, v0, Landroid/os/Message;->arg1:I

    .line 865
    iget-object v4, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$11;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGalleryData:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    invoke-static {v4}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$31(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 866
    iget-object v4, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$11;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;
    invoke-static {v4}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$32(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$11;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGalleryData:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;
    invoke-static {v5}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$31(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$11;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isRenderLowImage:Z
    invoke-static {v6}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$33(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Z

    move-result v6

    if-eqz v6, :cond_1

    :goto_0
    invoke-virtual {v4, v5, v3, v2}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->setGalleryData(Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher$GalleryData;II)I

    move-result v1

    .line 867
    .local v1, "ret":I
    if-eqz v1, :cond_0

    .line 868
    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$6()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mMorphoImageStitche.setGalleryData error ret:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    iput v7, v0, Landroid/os/Message;->arg1:I

    .line 878
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$11;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$34(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 879
    return-void

    .end local v1    # "ret":I
    :cond_1
    move v2, v3

    .line 866
    goto :goto_0

    .line 872
    :cond_2
    iget-object v4, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$11;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;
    invoke-static {v4}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$32(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$11;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->isRenderLowImage:Z
    invoke-static {v5}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$33(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Z

    move-result v5

    if-eqz v5, :cond_3

    :goto_2
    invoke-virtual {v4, v3, v2}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->setPostviewData(II)I

    move-result v1

    .line 873
    .restart local v1    # "ret":I
    if-eqz v1, :cond_0

    .line 874
    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$6()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mMorphoImageStitche.setPostviewData error ret:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    iput v7, v0, Landroid/os/Message;->arg1:I

    goto :goto_1

    .end local v1    # "ret":I
    :cond_3
    move v2, v3

    .line 872
    goto :goto_2
.end method
