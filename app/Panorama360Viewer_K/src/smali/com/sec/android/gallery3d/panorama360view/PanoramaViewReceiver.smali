.class public Lcom/sec/android/gallery3d/panorama360view/PanoramaViewReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PanoramaViewReceiver.java"


# static fields
.field private static final CONTENT_URI_360_PHOTO:Ljava/lang/String; = "com.samsung.android.intent.CONTENT_URI_360_PHOTO"

.field public static final DRM:Ljava/lang/String; = "com.samsung.android.intent.DRM"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private startPostView(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "received_intent"    # Landroid/content/Intent;

    .prologue
    .line 21
    const-string v2, "com.samsung.android.intent.CONTENT_URI_360_PHOTO"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 22
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v1, :cond_0

    .line 23
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 24
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 25
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 26
    const-string v2, "com.samsung.android.intent.DRM"

    const-string v3, "com.samsung.android.intent.DRM"

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 27
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 29
    const-string v2, "Panorama360ViewReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "received_intent.getAction() : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    const-string v2, "Panorama360ViewReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "send_intent.getAction()     : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    const-string v2, "Panorama360ViewReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "uri : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 43
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 15
    const-string v0, "Panorama360ViewReceiver"

    const-string v1, "onReceive"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewReceiver;->startPostView(Landroid/content/Context;Landroid/content/Intent;)V

    .line 17
    return-void
.end method
