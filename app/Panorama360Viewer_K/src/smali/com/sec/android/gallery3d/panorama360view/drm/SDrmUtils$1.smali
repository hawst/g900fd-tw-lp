.class Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils$1;
.super Ljava/lang/Object;
.source "SDrmUtils.java"

# interfaces
.implements Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public acquireLicense(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "drmManagerClient"    # Landroid/drm/DrmManagerClient;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-static {p1, p2, p3}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->acquireLicense(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isDrmFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-static {p1}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->isSDRMFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isValidDrmFile(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "drmManagerClient"    # Landroid/drm/DrmManagerClient;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-static {p1, p2, p3}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->isValidDrmFile(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public unpack(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface$UnpackCallback;)Z
    .locals 1
    .param p1, "drmManagerClient"    # Landroid/drm/DrmManagerClient;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;
    .param p4, "callback"    # Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface$UnpackCallback;

    .prologue
    .line 27
    invoke-static {p1, p2, p3, p4}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->unpack(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/panorama360view/drm/DrmInterface$UnpackCallback;)Z

    move-result v0

    return v0
.end method

.method public unpack(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;Ljava/io/ByteArrayOutputStream;)Z
    .locals 1
    .param p1, "drmManagerClient"    # Landroid/drm/DrmManagerClient;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;
    .param p4, "outputStream"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-static {p1, p2, p3, p4}, Lcom/sec/android/gallery3d/panorama360view/drm/SDrmUtils;->unpack(Landroid/drm/DrmManagerClient;Ljava/lang/String;Ljava/lang/String;Ljava/io/ByteArrayOutputStream;)Z

    move-result v0

    return v0
.end method
