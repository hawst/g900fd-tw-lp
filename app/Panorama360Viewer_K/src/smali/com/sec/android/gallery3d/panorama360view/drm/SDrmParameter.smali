.class public Lcom/sec/android/gallery3d/panorama360view/drm/SDrmParameter;
.super Ljava/lang/Object;
.source "SDrmParameter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/panorama360view/drm/SDrmParameter$SDrmAcquisitionInfoParameters;
    }
.end annotation


# static fields
.field public static final APP_ID:Ljava/lang/String; = "app_Id"

.field public static final DRM_BUFFER_SIZE:Ljava/lang/String; = "drm_buffer_size"

.field public static final DRM_END_OF_FILE:Ljava/lang/String; = "drm_eof"

.field public static final DRM_FALSE:Ljava/lang/String; = "false"

.field public static final DRM_FILE_OFFSET:Ljava/lang/String; = "drm_offset"

.field public static final DRM_FILE_STATUS:Ljava/lang/String; = "drm_file_status"

.field public static final DRM_PATH:Ljava/lang/String; = "drm_path"

.field public static final DRM_TRUE:Ljava/lang/String; = "true"

.field public static final FILE_STATUS_CLOSE:Ljava/lang/String; = "close"

.field public static final FILE_STATUS_OPEN:Ljava/lang/String; = "open"

.field public static final IMEI:Ljava/lang/String; = "imei"

.field public static final MEDIA_HUB:Ljava/lang/String; = "media_hub"

.field public static final ORDER_ID:Ljava/lang/String; = "orderId"

.field public static final SDRM_ENVTYPE:Ljava/lang/String; = "env/vnd.sdrm-media.sev"

.field public static final SDRM_ENV_EXTENSION:Ljava/lang/String; = "sev"

.field public static final SDRM_MIMETYPE:Ljava/lang/String; = "video/vnd.sdrm-media.sm4"

.field public static final SDRM_SM4_EXTENSION:Ljava/lang/String; = "sm4"

.field public static final TYPE_FILE_PARSE:I = 0x33

.field public static final USER_GUID:Ljava/lang/String; = "user_guid"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
