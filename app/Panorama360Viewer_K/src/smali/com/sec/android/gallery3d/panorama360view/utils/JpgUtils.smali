.class public Lcom/sec/android/gallery3d/panorama360view/utils/JpgUtils;
.super Ljava/lang/Object;
.source "JpgUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getImageSize([B)Landroid/graphics/Point;
    .locals 7
    .param p0, "buffer"    # [B

    .prologue
    .line 7
    const/4 v3, 0x0

    .line 10
    .local v3, "offset":I
    invoke-static {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/utils/JpgUtils;->toShort([BI)S

    move-result v4

    .line 11
    .local v4, "temp":S
    add-int/lit8 v3, v3, 0x2

    .line 12
    const/16 v6, -0x2701

    if-ne v4, v6, :cond_0

    .line 13
    invoke-static {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/utils/JpgUtils;->toShort([BI)S

    move-result v4

    .line 14
    add-int/lit8 v3, v3, 0x2

    .line 15
    const/16 v6, -0x1e01

    if-ne v4, v6, :cond_0

    .line 16
    add-int/lit8 v3, v3, 0x10

    .line 17
    invoke-static {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/utils/JpgUtils;->toShort([BI)S

    move-result v0

    .line 18
    .local v0, "count":S
    add-int/lit8 v3, v3, 0x2

    .line 19
    const/4 v5, 0x0

    .local v5, "width":I
    const/4 v1, 0x0

    .line 20
    .local v1, "height":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_1

    .line 40
    .end local v0    # "count":S
    .end local v1    # "height":I
    .end local v2    # "i":I
    .end local v5    # "width":I
    :cond_0
    const/4 v6, 0x0

    :goto_1
    return-object v6

    .line 21
    .restart local v0    # "count":S
    .restart local v1    # "height":I
    .restart local v2    # "i":I
    .restart local v5    # "width":I
    :cond_1
    invoke-static {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/utils/JpgUtils;->toShort([BI)S

    move-result v4

    .line 22
    add-int/lit8 v3, v3, 0x2

    .line 23
    const/16 v6, 0x100

    if-ne v4, v6, :cond_2

    .line 24
    add-int/lit8 v3, v3, 0x6

    .line 25
    invoke-static {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/utils/JpgUtils;->toShort([BI)S

    move-result v5

    .line 26
    add-int/lit8 v3, v3, 0x4

    .line 34
    :goto_2
    if-eqz v5, :cond_4

    if-eqz v1, :cond_4

    .line 35
    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6, v5, v1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_1

    .line 27
    :cond_2
    const/16 v6, 0x101

    if-ne v4, v6, :cond_3

    .line 28
    add-int/lit8 v3, v3, 0x6

    .line 29
    invoke-static {p0, v3}, Lcom/sec/android/gallery3d/panorama360view/utils/JpgUtils;->toShort([BI)S

    move-result v1

    .line 30
    add-int/lit8 v3, v3, 0x4

    .line 31
    goto :goto_2

    .line 32
    :cond_3
    add-int/lit8 v3, v3, 0xa

    goto :goto_2

    .line 20
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static toShort([BI)S
    .locals 3
    .param p0, "buffer"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 44
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    shl-int/lit8 v1, v1, 0x8

    const v2, 0xff00

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method
