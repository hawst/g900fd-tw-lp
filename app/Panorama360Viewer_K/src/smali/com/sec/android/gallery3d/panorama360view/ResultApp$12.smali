.class Lcom/sec/android/gallery3d/panorama360view/ResultApp$12;
.super Ljava/lang/Object;
.source "ResultApp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/gallery3d/panorama360view/ResultApp;->reregisterTexture()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$12;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    .line 887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 891
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 892
    .local v0, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$12;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$32(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->reRegisterTexture()I

    move-result v1

    .line 893
    .local v1, "ret":I
    if-eqz v1, :cond_0

    .line 894
    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$6()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mMorphoImageStitcher.reRegisterTexture error ret:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 896
    :cond_0
    const/4 v2, 0x0

    iput v2, v0, Landroid/os/Message;->arg1:I

    .line 897
    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$12;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$34(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 898
    return-void
.end method
