.class Lcom/sec/android/gallery3d/panorama360view/ResultApp$6;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "ResultApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/panorama360view/ResultApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$6;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    .line 985
    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 988
    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$6()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onScale scale:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    .line 990
    .local v0, "scale":F
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$6;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mRenderer:Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;
    invoke-static {v1}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$2(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$6;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mPreviousScale:F
    invoke-static {v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$14(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)F

    move-result v2

    div-float v2, v0, v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->setScale(F)V

    .line 991
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$6;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->mGLPanoramaView:Landroid/opengl/GLSurfaceView;
    invoke-static {v1}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$3(Lcom/sec/android/gallery3d/panorama360view/ResultApp;)Landroid/opengl/GLSurfaceView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 992
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$6;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    invoke-static {v1, v0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$15(Lcom/sec/android/gallery3d/panorama360view/ResultApp;F)V

    .line 993
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScale(Landroid/view/ScaleGestureDetector;)Z

    move-result v1

    return v1
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 3
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 997
    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$6()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onScaleBegin scale:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScaleBegin(Landroid/view/ScaleGestureDetector;)Z

    move-result v0

    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 3
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 1002
    # getter for: Lcom/sec/android/gallery3d/panorama360view/ResultApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$6()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onScaleEnd scale:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/LogFilter;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$6;->this$0:Lcom/sec/android/gallery3d/panorama360view/ResultApp;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/panorama360view/ResultApp;->access$15(Lcom/sec/android/gallery3d/panorama360view/ResultApp;F)V

    .line 1004
    invoke-super {p0, p1}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;->onScaleEnd(Landroid/view/ScaleGestureDetector;)V

    .line 1005
    return-void
.end method
