.class public Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;
.super Ljava/lang/Object;
.source "PanoramaViewRenderer.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer$ResultInfo;,
        Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer$onRenderPreviewListener;
    }
.end annotation


# static fields
.field public static final MSG_FINISH_RENDER_PREVIEW:I = 0x0

.field public static final MSG_REQUEST_REREGISTER_TEXTURE:I = 0x2

.field public static final MSG_REQUEST_SET_POSTVIEW_DATA:I = 0x1

.field public static final PANORAMA_POSTVIEW:I = 0x1


# instance fields
.field private isDefault:Z

.field private isFileSelect:Z

.field private isRegistered:Z

.field private mActivity:Landroid/app/Activity;

.field private mDispType:I

.field private mHandler:Landroid/os/Handler;

.field private mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

.field private mPreviewMode:I

.field private mRenderEnable:Z

.field private mRotateAngleX:D

.field private mRotateAngleY:D

.field private mRotateRatioX:D

.field private mRotateRatioY:D

.field private mScale:D

.field private mSyncObj:Ljava/lang/Object;

.field private mTouchSyncObj:Ljava/lang/Object;

.field private mViewHeight:I

.field private mViewWidth:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/os/Handler;Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;I)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "image_stitcher"    # Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;
    .param p4, "preview_buffer_size"    # I

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->isDefault:Z

    .line 24
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mSyncObj:Ljava/lang/Object;

    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mTouchSyncObj:Ljava/lang/Object;

    .line 29
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mScale:D

    .line 30
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mDispType:I

    .line 37
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRenderEnable:Z

    .line 38
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->isRegistered:Z

    .line 45
    iput-object p3, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    .line 46
    iput-object p2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mHandler:Landroid/os/Handler;

    .line 47
    iput-object p1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mActivity:Landroid/app/Activity;

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/os/Handler;Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;Z)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "image_stitcher"    # Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;
    .param p4, "is_file_select"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->isDefault:Z

    .line 24
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mSyncObj:Ljava/lang/Object;

    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mTouchSyncObj:Ljava/lang/Object;

    .line 29
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mScale:D

    .line 30
    iput v3, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mDispType:I

    .line 37
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRenderEnable:Z

    .line 38
    iput-boolean v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->isRegistered:Z

    .line 51
    iput v3, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mPreviewMode:I

    .line 52
    iput-object p3, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    .line 53
    iput-boolean p4, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->isFileSelect:Z

    .line 54
    iput-object p2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mHandler:Landroid/os/Handler;

    .line 55
    iput-object p1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mActivity:Landroid/app/Activity;

    .line 56
    return-void
.end method


# virtual methods
.method public getRenderEnable()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRenderEnable:Z

    return v0
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 13
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    const-wide/16 v11, 0x0

    .line 124
    iget-object v7, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mTouchSyncObj:Ljava/lang/Object;

    monitor-enter v7

    .line 125
    :try_start_0
    iget-wide v5, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mScale:D

    .line 127
    .local v5, "scale":D
    iget-wide v9, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateRatioX:D

    invoke-static {v9, v10}, Ljava/lang/Math;->abs(D)D

    move-result-wide v9

    cmpl-double v0, v9, v11

    if-gtz v0, :cond_0

    iget-wide v9, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateRatioY:D

    invoke-static {v9, v10}, Ljava/lang/Math;->abs(D)D

    move-result-wide v9

    cmpl-double v0, v9, v11

    if-lez v0, :cond_1

    .line 128
    :cond_0
    const/4 v8, 0x0

    .line 129
    .local v8, "set_angle":Z
    iget-wide v1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateRatioX:D

    .line 130
    .local v1, "x_rot":D
    iget-wide v3, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateRatioY:D

    .line 131
    .local v3, "y_rot":D
    const-wide/16 v9, 0x0

    iput-wide v9, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateRatioX:D

    .line 132
    const-wide/16 v9, 0x0

    iput-wide v9, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateRatioY:D

    .line 124
    :goto_0
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    iget-object v9, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mSyncObj:Ljava/lang/Object;

    monitor-enter v9

    .line 142
    :try_start_1
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRenderEnable:Z

    if-nez v0, :cond_2

    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 155
    :goto_1
    return-void

    .line 134
    .end local v1    # "x_rot":D
    .end local v3    # "y_rot":D
    .end local v8    # "set_angle":Z
    :cond_1
    const/4 v8, 0x1

    .line 135
    .restart local v8    # "set_angle":Z
    :try_start_2
    iget-wide v1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateAngleX:D

    .line 136
    .restart local v1    # "x_rot":D
    iget-wide v3, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateAngleY:D

    .line 137
    .restart local v3    # "y_rot":D
    const-wide/16 v9, 0x0

    iput-wide v9, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateAngleX:D

    .line 138
    const-wide/16 v9, 0x0

    iput-wide v9, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateAngleY:D

    goto :goto_0

    .line 124
    .end local v1    # "x_rot":D
    .end local v3    # "y_rot":D
    .end local v5    # "scale":D
    .end local v8    # "set_angle":Z
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 143
    .restart local v1    # "x_rot":D
    .restart local v3    # "y_rot":D
    .restart local v5    # "scale":D
    .restart local v8    # "set_angle":Z
    :cond_2
    :try_start_3
    iget-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->isDefault:Z

    if-eqz v0, :cond_3

    .line 144
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    iget v7, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mDispType:I

    invoke-virtual {v0, v7}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->renderPostviewDefault(I)I

    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->isDefault:Z

    .line 141
    :goto_2
    monitor-exit v9

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 147
    :cond_3
    if-eqz v8, :cond_4

    .line 148
    :try_start_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    iget v7, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mDispType:I

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->renderPostviewAngle(DDDI)I

    goto :goto_2

    .line 150
    :cond_4
    iget-object v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mMorphoImageStitcher:Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;

    iget v7, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mDispType:I

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoImageStitcher;->renderPostview(DDDI)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 6
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 179
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 180
    .local v1, "disp_w":I
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getHeight()I

    move-result v0

    .line 181
    .local v0, "disp_h":I
    iput v1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mViewWidth:I

    .line 182
    iput v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mViewHeight:I

    .line 183
    invoke-static {v4, v4, v1, v0}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 185
    invoke-static {v4, v4, v1, v0}, Landroid/opengl/GLES20;->glScissor(IIII)V

    .line 186
    iget v3, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mPreviewMode:I

    if-ne v3, v5, :cond_2

    .line 187
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->isRegistered:Z

    if-nez v3, :cond_0

    .line 189
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 190
    .local v2, "msg":Landroid/os/Message;
    iget-boolean v3, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->isFileSelect:Z

    if-eqz v3, :cond_1

    .line 191
    iput v5, v2, Landroid/os/Message;->arg1:I

    .line 195
    :goto_0
    iget-object v3, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 196
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->isRegistered:Z

    .line 201
    .end local v2    # "msg":Landroid/os/Message;
    :cond_0
    :goto_1
    return-void

    .line 193
    .restart local v2    # "msg":Landroid/os/Message;
    :cond_1
    const/4 v3, 0x2

    iput v3, v2, Landroid/os/Message;->arg1:I

    goto :goto_0

    .line 199
    .end local v2    # "msg":Landroid/os/Message;
    :cond_2
    iput-boolean v5, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRenderEnable:Z

    goto :goto_1
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 1
    .param p1, "gl"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    const/4 v0, 0x0

    .line 205
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->isRegistered:Z

    .line 206
    iput-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRenderEnable:Z

    .line 207
    return-void
.end method

.method public setDefault()V
    .locals 4

    .prologue
    .line 94
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mTouchSyncObj:Ljava/lang/Object;

    monitor-enter v1

    .line 95
    const-wide/16 v2, 0x0

    :try_start_0
    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateRatioX:D

    .line 96
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateRatioY:D

    .line 97
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateAngleX:D

    .line 98
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateAngleY:D

    .line 99
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mScale:D

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->isDefault:Z

    .line 94
    monitor-exit v1

    .line 102
    return-void

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setDefaultScale(D)V
    .locals 2
    .param p1, "scale"    # D

    .prologue
    .line 88
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mTouchSyncObj:Ljava/lang/Object;

    monitor-enter v1

    .line 89
    :try_start_0
    iput-wide p1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mScale:D

    .line 88
    monitor-exit v1

    .line 91
    return-void

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setDispType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 105
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mSyncObj:Ljava/lang/Object;

    monitor-enter v1

    .line 106
    :try_start_0
    iput p1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mDispType:I

    .line 105
    monitor-exit v1

    .line 108
    return-void

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setRenderEnable(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 111
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mSyncObj:Ljava/lang/Object;

    monitor-enter v1

    .line 112
    :try_start_0
    iput-boolean p1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRenderEnable:Z

    .line 111
    monitor-exit v1

    .line 114
    return-void

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setScale(F)V
    .locals 10
    .param p1, "scale"    # F

    .prologue
    const-wide/high16 v8, 0x4008000000000000L    # 3.0

    const-wide v6, 0x3fe999999999999aL    # 0.8

    .line 73
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mTouchSyncObj:Ljava/lang/Object;

    monitor-enter v1

    .line 74
    :try_start_0
    iget-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mScale:D

    float-to-double v4, p1

    mul-double/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mScale:D

    .line 75
    iget-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mScale:D

    cmpl-double v0, v2, v8

    if-lez v0, :cond_1

    .line 76
    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mScale:D

    .line 80
    :cond_0
    :goto_0
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateRatioX:D

    .line 81
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateRatioY:D

    .line 82
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateAngleX:D

    .line 83
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateAngleY:D

    .line 73
    monitor-exit v1

    .line 85
    return-void

    .line 77
    :cond_1
    iget-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mScale:D

    cmpg-double v0, v2, v6

    if-gez v0, :cond_0

    .line 78
    const-wide v2, 0x3fe999999999999aL    # 0.8

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mScale:D

    goto :goto_0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setSwipeAngle(FF)V
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 66
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mTouchSyncObj:Ljava/lang/Object;

    monitor-enter v1

    .line 67
    :try_start_0
    iget-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateAngleX:D

    float-to-double v4, p1

    add-double/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateAngleX:D

    .line 68
    iget-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateAngleY:D

    float-to-double v4, p2

    add-double/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateAngleY:D

    .line 66
    monitor-exit v1

    .line 70
    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setSwipeDistance(FF)V
    .locals 6
    .param p1, "distance_x"    # F
    .param p2, "distance_y"    # F

    .prologue
    .line 59
    iget-object v1, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mTouchSyncObj:Ljava/lang/Object;

    monitor-enter v1

    .line 60
    :try_start_0
    iget-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateRatioX:D

    iget v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mViewWidth:I

    int-to-float v0, v0

    div-float v0, p1, v0

    float-to-double v4, v0

    add-double/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateRatioX:D

    .line 61
    iget-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateRatioY:D

    iget v0, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mViewHeight:I

    int-to-float v0, v0

    div-float v0, p2, v0

    float-to-double v4, v0

    add-double/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/gallery3d/panorama360view/PanoramaViewRenderer;->mRotateRatioY:D

    .line 59
    monitor-exit v1

    .line 63
    return-void

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
