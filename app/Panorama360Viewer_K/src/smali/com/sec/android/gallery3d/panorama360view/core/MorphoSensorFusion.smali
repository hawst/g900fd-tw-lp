.class public Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;
.super Ljava/lang/Object;
.source "MorphoSensorFusion.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;
    }
.end annotation


# static fields
.field public static final MAXIMUM_DATA_SIZE:I = 0x200

.field public static final MODE_USE_ACCELEROMETER_AND_MAGNETIC_FIELD:I = 0x3

.field public static final MODE_USE_ALL_SENSORS:I = 0x0

.field public static final MODE_USE_GYROSCOPE:I = 0x1

.field public static final MODE_USE_GYROSCOPE_AND_ROTATION_VECTOR:I = 0x4

.field public static final MODE_USE_GYROSCOPE_WITH_ACCELEROMETER:I = 0x2

.field public static final OFFSET_MODE_DYNAMIC:I = 0x1

.field public static final OFFSET_MODE_STATIC:I = 0x0

.field public static final ROTATE_0:I = 0x0

.field public static final ROTATE_180:I = 0x2

.field public static final ROTATE_270:I = 0x3

.field public static final ROTATE_90:I = 0x1

.field public static final SENSOR_TYPE_ACCELEROMETER:I = 0x1

.field public static final SENSOR_TYPE_GYROSCOPE:I = 0x0

.field public static final SENSOR_TYPE_MAGNETIC_FIELD:I = 0x2

.field public static final SENSOR_TYPE_ROTATION_VECTOR:I = 0x3

.field public static final STATE_CALC_OFFSET:I = 0x0

.field public static final STATE_PROCESS:I = 0x1


# instance fields
.field private mNative:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11
    :try_start_0
    const-string v1, "morpho_sensor_fusion_for_viewer"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 12
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 13
    .restart local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    .line 69
    invoke-direct {p0}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->createNativeObject()I

    move-result v0

    .line 70
    .local v0, "ret":I
    if-eqz v0, :cond_0

    .line 72
    iput v0, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    .line 79
    :goto_0
    return-void

    .line 76
    :cond_0
    iput v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    .line 77
    const v0, -0x7ffffffc

    goto :goto_0
.end method

.method private final native calc(I)I
.end method

.method private final native createNativeObject()I
.end method

.method private final native deleteNativeObject(I)V
.end method

.method private final native finish(I)I
.end method

.method public static getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->nativeGetVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final native initialize(I)I
.end method

.method private static final native nativeGetVersion()Ljava/lang/String;
.end method

.method private final native outputRotationAngle(I[D)I
.end method

.method private final native outputRotationMatrix3x3(II[D)I
.end method

.method private final native setAppState(II)I
.end method

.method private final native setMode(II)I
.end method

.method private final native setOffset(ILcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;I)I
.end method

.method private final native setOffsetMode(II)I
.end method

.method private final native setRotation(II)I
.end method

.method private final native setSensorData(I[Ljava/lang/Object;I)I
.end method

.method private final native setSensorReliability(III)I
.end method


# virtual methods
.method public calc()I
    .locals 2

    .prologue
    .line 212
    const/4 v0, 0x0

    .line 213
    .local v0, "ret":I
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    if-eqz v1, :cond_0

    .line 215
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->calc(I)I

    move-result v0

    .line 221
    :goto_0
    return v0

    .line 219
    :cond_0
    const v0, -0x7ffffffe

    goto :goto_0
.end method

.method public finish()I
    .locals 2

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 99
    .local v0, "ret":I
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    if-eqz v1, :cond_0

    .line 101
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->finish(I)I

    move-result v0

    .line 102
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->deleteNativeObject(I)V

    .line 103
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    .line 109
    :goto_0
    return v0

    .line 107
    :cond_0
    const v0, -0x7ffffffe

    goto :goto_0
.end method

.method public initialize()I
    .locals 2

    .prologue
    .line 83
    const/4 v0, 0x0

    .line 84
    .local v0, "ret":I
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    if-eqz v1, :cond_0

    .line 86
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    invoke-direct {p0, v1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->initialize(I)I

    move-result v0

    .line 92
    :goto_0
    return v0

    .line 90
    :cond_0
    const v0, -0x7ffffffe

    goto :goto_0
.end method

.method public outputRotationAngle([D)I
    .locals 2
    .param p1, "angle"    # [D

    .prologue
    .line 240
    const/4 v0, 0x0

    .line 241
    .local v0, "ret":I
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    if-eqz v1, :cond_0

    .line 243
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    invoke-direct {p0, v1, p1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->outputRotationAngle(I[D)I

    move-result v0

    .line 249
    :goto_0
    return v0

    .line 247
    :cond_0
    const v0, -0x7ffffffe

    goto :goto_0
.end method

.method public outputRotationMatrix3x3(I[D)I
    .locals 2
    .param p1, "sensor_type"    # I
    .param p2, "dst_mat"    # [D

    .prologue
    .line 226
    const/4 v0, 0x0

    .line 227
    .local v0, "ret":I
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    if-eqz v1, :cond_0

    .line 229
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    invoke-direct {p0, v1, p1, p2}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->outputRotationMatrix3x3(II[D)I

    move-result v0

    .line 235
    :goto_0
    return v0

    .line 233
    :cond_0
    const v0, -0x7ffffffe

    goto :goto_0
.end method

.method public setAppState(I)I
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 128
    const/4 v0, 0x0

    .line 129
    .local v0, "ret":I
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    if-eqz v1, :cond_0

    .line 131
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    invoke-direct {p0, v1, p1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->setAppState(II)I

    move-result v0

    .line 137
    :goto_0
    return v0

    .line 135
    :cond_0
    const v0, -0x7ffffffe

    goto :goto_0
.end method

.method public setMode(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 114
    const/4 v0, 0x0

    .line 115
    .local v0, "ret":I
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    if-eqz v1, :cond_0

    .line 117
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    invoke-direct {p0, v1, p1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->setMode(II)I

    move-result v0

    .line 123
    :goto_0
    return v0

    .line 121
    :cond_0
    const v0, -0x7ffffffe

    goto :goto_0
.end method

.method public setOffset(Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;I)I
    .locals 2
    .param p1, "data"    # Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;
    .param p2, "sensor_type"    # I

    .prologue
    .line 184
    const/4 v0, 0x0

    .line 185
    .local v0, "ret":I
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    if-eqz v1, :cond_0

    .line 187
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    invoke-direct {p0, v1, p1, p2}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->setOffset(ILcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion$SensorData;I)I

    move-result v0

    .line 193
    :goto_0
    return v0

    .line 191
    :cond_0
    const v0, -0x7ffffffe

    goto :goto_0
.end method

.method public setOffsetMode(I)I
    .locals 2
    .param p1, "offset_mode"    # I

    .prologue
    .line 170
    const/4 v0, 0x0

    .line 171
    .local v0, "ret":I
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    if-eqz v1, :cond_0

    .line 173
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    invoke-direct {p0, v1, p1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->setOffsetMode(II)I

    move-result v0

    .line 179
    :goto_0
    return v0

    .line 177
    :cond_0
    const v0, -0x7ffffffe

    goto :goto_0
.end method

.method public setRotation(I)I
    .locals 2
    .param p1, "rotation"    # I

    .prologue
    .line 142
    const/4 v0, 0x0

    .line 143
    .local v0, "ret":I
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    if-eqz v1, :cond_0

    .line 145
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    invoke-direct {p0, v1, p1}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->setRotation(II)I

    move-result v0

    .line 151
    :goto_0
    return v0

    .line 149
    :cond_0
    const v0, -0x7ffffffe

    goto :goto_0
.end method

.method public setSensorData([Ljava/lang/Object;I)I
    .locals 2
    .param p1, "data"    # [Ljava/lang/Object;
    .param p2, "sensor_type"    # I

    .prologue
    .line 198
    const/4 v0, 0x0

    .line 199
    .local v0, "ret":I
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    if-eqz v1, :cond_0

    .line 201
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    invoke-direct {p0, v1, p1, p2}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->setSensorData(I[Ljava/lang/Object;I)I

    move-result v0

    .line 207
    :goto_0
    return v0

    .line 205
    :cond_0
    const v0, -0x7ffffffe

    goto :goto_0
.end method

.method public setSensorReliability(II)I
    .locals 2
    .param p1, "rel"    # I
    .param p2, "sensor_type"    # I

    .prologue
    .line 156
    const/4 v0, 0x0

    .line 157
    .local v0, "ret":I
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    if-eqz v1, :cond_0

    .line 159
    iget v1, p0, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->mNative:I

    invoke-direct {p0, v1, p1, p2}, Lcom/sec/android/gallery3d/panorama360view/core/MorphoSensorFusion;->setSensorReliability(III)I

    move-result v0

    .line 165
    :goto_0
    return v0

    .line 163
    :cond_0
    const v0, -0x7ffffffe

    goto :goto_0
.end method
