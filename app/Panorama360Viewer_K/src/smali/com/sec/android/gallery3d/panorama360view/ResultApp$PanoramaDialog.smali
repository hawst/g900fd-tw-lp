.class public Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;
.super Landroid/app/DialogFragment;
.source "ResultApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/gallery3d/panorama360view/ResultApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PanoramaDialog"
.end annotation


# instance fields
.field private mId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1209
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 1210
    return-void
.end method

.method private creteErrorDialog(Ljava/lang/String;)Landroid/app/Dialog;
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 1252
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1253
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f060004

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1254
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1255
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1256
    const v1, 0x7f060002

    new-instance v2, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog$1;

    invoke-direct {v2, p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog$1;-><init>(Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1263
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v5, 0x7f060005

    const/4 v4, 0x0

    .line 1214
    const/4 v0, 0x0

    .line 1215
    .local v0, "dialog":Landroid/app/Dialog;
    const/4 v1, 0x0

    .line 1216
    .local v1, "progressDialog":Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->mId:I

    .line 1217
    iget v2, p0, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->mId:I

    packed-switch v2, :pswitch_data_0

    .line 1244
    :goto_0
    :pswitch_0
    if-eqz v0, :cond_0

    .line 1247
    .end local v0    # "dialog":Landroid/app/Dialog;
    :goto_1
    return-object v0

    .line 1219
    .restart local v0    # "dialog":Landroid/app/Dialog;
    :pswitch_1
    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->creteErrorDialog(Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v0

    .line 1220
    goto :goto_0

    .line 1222
    :pswitch_2
    invoke-virtual {p0, v5}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->creteErrorDialog(Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v0

    .line 1223
    goto :goto_0

    .line 1225
    :pswitch_3
    new-instance v1, Landroid/app/ProgressDialog;

    .end local v1    # "progressDialog":Landroid/app/ProgressDialog;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 1226
    .restart local v1    # "progressDialog":Landroid/app/ProgressDialog;
    const v2, 0x7f060007

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 1227
    const v2, 0x7f060008

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1228
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1229
    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 1230
    move-object v0, v1

    .line 1231
    goto :goto_0

    .line 1233
    :pswitch_4
    const v2, 0x7f060006

    invoke-virtual {p0, v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/gallery3d/panorama360view/ResultApp$PanoramaDialog;->creteErrorDialog(Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 1247
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_1

    .line 1217
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
