.class Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 57
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onServiceConnected"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 58
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 57
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    .line 60
    invoke-static {p2}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    move-result-object v1

    .line 59
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$0(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;)V

    .line 61
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mIsConnected:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$1(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$2(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 66
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mRemoteListener:Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;
    invoke-static {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$3(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener$Stub;

    move-result-object v2

    .line 64
    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->addRemoteListener(Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;)V

    .line 67
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mAudioAppConnectionService.addRemoteStateListener "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 69
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 68
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 67
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$4(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;Z)V

    .line 75
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->notifyConnectionSet()V
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$5(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)V

    .line 76
    return-void

    .line 70
    :catch_0
    move-exception v0

    .line 71
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    const-string v1, "Adding remote state listener was not possible"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 80
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onServiceConnected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 81
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 80
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # getter for: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->mAudioAppConnectionService:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$2(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;->removeRemoteListener(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$0(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;)V

    .line 89
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$4(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;Z)V

    .line 90
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->notifyConnectionClosed()V
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->access$6(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;)V

    .line 92
    return-void

    .line 84
    :catch_0
    move-exception v0

    .line 85
    const-string v0, "professionalaudioconnection:library:j:SapaAppService "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mAudioAppConnectionService.removeRemoteListener "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 86
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService$1;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 85
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
