.class Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;
.super Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;
.source "SourceFile"


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;)V
    .locals 0

    .prologue
    .line 544
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public activateApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 549
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->activateApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$400(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public addActiveApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->addActiveApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$500(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V

    .line 556
    return-void
.end method

.method public addRemoteListener(Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;)V
    .locals 1

    .prologue
    .line 561
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->addRemoteListener(Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;)V
    invoke-static {v0, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$600(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;)V

    .line 563
    return-void
.end method

.method public changeAppInfo(Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->changeAppInfo(Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
    invoke-static {v0, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$700(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V

    .line 569
    return-void
.end method

.method public deactivateApp(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 573
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->deactivateApp(Ljava/lang/String;)Z
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$800(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public declareBeingTransportMaster(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 625
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->declareBeingTransportMaster(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$1700(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)V

    .line 626
    return-void
.end method

.method public getActiveAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    .locals 1

    .prologue
    .line 579
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getActiveAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$900(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    move-result-object v0

    return-object v0
.end method

.method public getAllActiveApp()Ljava/util/List;
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getAllActiveApp()Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$1000(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAllInstalledApp()Ljava/util/List;
    .locals 1

    .prologue
    .line 595
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getAllInstalledApp()Ljava/util/List;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$1200(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getInstalledAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    .locals 1

    .prologue
    .line 590
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getInstalledAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$1100(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    move-result-object v0

    return-object v0
.end method

.method public getLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 619
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$1600(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getTransportMaster()Ljava/lang/String;
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getTransportMaster()Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$1800(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public removeFromActiveApps(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->removeFromAppList(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$1300(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)V

    .line 602
    return-void
.end method

.method public removeRemoteListener(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 607
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->removeRemoteListener(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$1400(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)V

    .line 608
    return-void
.end method

.method public runAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;->this$0:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;

    # invokes: Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->runAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, p1, p2, p3}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->access$1500(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    return-void
.end method
