.class final Lcom/samsung/android/sdk/professionalaudio/ApaUpdateUsbDeviceInfoCommand;
.super Lcom/samsung/android/sdk/professionalaudio/ApaCommand;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const-string v0, "updateusbdeviceinfo"

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/ApaCommand;-><init>(Ljava/lang/String;)V

    .line 29
    return-void
.end method


# virtual methods
.method public setAttached(Z)Lcom/samsung/android/sdk/professionalaudio/ApaUpdateUsbDeviceInfoCommand;
    .locals 4

    .prologue
    .line 38
    if-eqz p1, :cond_0

    .line 39
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaUpdateUsbDeviceInfoCommand;->mInputs:Lorg/json/JSONArray;

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "attached"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 46
    :goto_0
    return-object p0

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaUpdateUsbDeviceInfoCommand;->mInputs:Lorg/json/JSONArray;

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "attached"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
