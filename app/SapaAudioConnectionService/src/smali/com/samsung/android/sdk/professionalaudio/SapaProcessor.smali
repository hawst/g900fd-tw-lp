.class public final Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final TAG:Ljava/lang/String; = "SapaProcessor"

.field private static mObjectIdSeed:I


# instance fields
.field private mArgs:Ljava/lang/String;

.field private mJackClientName:Ljava/lang/String;

.field private mModuleFileName:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mObjectId:I

.field private mPackageName:Ljava/lang/String;

.field private mPluginPackageName:Ljava/lang/String;

.field private volatile mRefCount:I

.field private mStatusListener:Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;)V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    sget v0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectIdSeed:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectIdSeed:I

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectId:I

    .line 124
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mPackageName:Ljava/lang/String;

    .line 125
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mRefCount:I

    .line 127
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CUSTOM"

    if-eq v0, v1, :cond_0

    .line 128
    invoke-virtual {p2}, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mPluginPackageName:Ljava/lang/String;

    .line 129
    new-instance v0, Ljava/lang/String;

    iget-object v1, p2, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;->mModuleFileName:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mModuleFileName:Ljava/lang/String;

    .line 130
    iget-object v0, p2, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;->mSetupArguments:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mArgs:Ljava/lang/String;

    .line 133
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 134
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mPluginPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    const-string v1, "com.samsung.android.sdk.professionalaudio.action.INIT_PLUGIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 136
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 150
    :goto_0
    iput-object p3, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mStatusListener:Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;

    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mPackageName:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mName:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mJackClientName:Ljava/lang/String;

    .line 155
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mPackageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mPluginPackageName:Ljava/lang/String;

    .line 146
    iput-object p4, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mArgs:Ljava/lang/String;

    .line 147
    const-string v0, "libwave.so"

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mModuleFileName:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized activate()Z
    .locals 3

    .prologue
    .line 315
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mRefCount:I

    if-lez v0, :cond_0

    .line 316
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectId:I

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "!activate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendCommand(ILjava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 317
    const/4 v0, 0x1

    .line 320
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 315
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized deactivate()Z
    .locals 3

    .prologue
    .line 331
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mRefCount:I

    if-lez v0, :cond_0

    .line 332
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectId:I

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "!deactivate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendCommand(ILjava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 333
    const/4 v0, 0x1

    .line 336
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 331
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized decRef()I
    .locals 1

    .prologue
    .line 205
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mRefCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mRefCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 403
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mName:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized getObjectId()I
    .locals 1

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPort(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/SapaPort;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getInstance()Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    move-result-object v0

    .line 51
    if-nez v0, :cond_0

    move-object v0, v1

    .line 59
    :goto_0
    return-object v0

    .line 54
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getAllPort()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v1

    .line 59
    goto :goto_0

    .line 54
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 55
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mJackClientName:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0
.end method

.method public getPorts()Ljava/util/List;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getInstance()Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    move-result-object v0

    .line 70
    if-nez v0, :cond_0

    move-object v0, v1

    .line 82
    :goto_0
    return-object v0

    .line 73
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 74
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getAllPort()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 79
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 80
    goto :goto_0

    .line 74
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 75
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mJackClientName:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 76
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 82
    goto :goto_0
.end method

.method getStatusListener()Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mStatusListener:Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;

    return-object v0
.end method

.method declared-synchronized incRef()I
    .locals 1

    .prologue
    .line 201
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mRefCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mRefCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method init()Z
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mArgs:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->init(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method declared-synchronized init(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 166
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "init_client:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mPluginPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mModuleFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->sendCommandInternal(Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    :try_start_1
    new-instance v0, Ljava/lang/String;

    const-string v1, "start_client "

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 174
    if-eqz p1, :cond_0

    .line 175
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 177
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->sendCommandInternal(Ljava/lang/String;I)Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 181
    :try_start_2
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mName:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mJackClientName:Ljava/lang/String;

    iget v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectId:I

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_getJackClientName(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mJackClientName:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 196
    :goto_0
    :try_start_3
    const-string v0, "SapaProcessor"

    const-string v1, "SapaProcessor was initialized."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 197
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 167
    :catch_0
    move-exception v0

    .line 168
    :try_start_4
    new-instance v0, Ljava/lang/InstantiationException;

    const-string v1, "error in initialize client. please check the return value of your wave\'s init()"

    invoke-direct {v0, v1}, Ljava/lang/InstantiationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 169
    :catch_1
    move-exception v0

    .line 170
    :try_start_5
    new-instance v0, Ljava/lang/InstantiationException;

    const-string v1, "error in initialize client. please check the return value of your wave\'s init()"

    invoke-direct {v0, v1}, Ljava/lang/InstantiationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :catch_2
    move-exception v0

    .line 191
    new-instance v0, Ljava/lang/InstantiationException;

    const-string v1, "error in initialize client. please check the return value of your wave\'s setUp()"

    invoke-direct {v0, v1}, Ljava/lang/InstantiationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :catch_3
    move-exception v0

    .line 193
    new-instance v0, Ljava/lang/InstantiationException;

    const-string v1, "error in initialize client. please check the return value of your wave\'s setUp()"

    invoke-direct {v0, v1}, Ljava/lang/InstantiationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 186
    :catch_4
    move-exception v0

    goto :goto_0

    .line 183
    :catch_5
    move-exception v0

    goto :goto_0
.end method

.method isJamMonitor()Z
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mPackageName:Ljava/lang/String;

    const-string v1, "com.samsung.android.sdk.professionalaudio.utility.jammonitor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    const/4 v0, 0x0

    .line 89
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized queryData(Ljava/lang/String;I)Ljava/nio/ByteBuffer;
    .locals 6

    .prologue
    .line 391
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mRefCount:I

    if-lez v0, :cond_1

    .line 392
    if-nez p1, :cond_0

    .line 393
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The query parameter is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 395
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectId:I

    int-to-long v2, p2

    const-wide/16 v4, 0x0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_request(ILjava/lang/String;JJ)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 396
    if-eqz v0, :cond_1

    .line 397
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asReadOnlyBuffer()Ljava/nio/ByteBuffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 400
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sendCommand(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->sendCommandInternal(Ljava/lang/String;I)Z

    .line 219
    return-void
.end method

.method sendCommand(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 232
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->sendCommandInternal(Ljava/lang/String;I)Z

    .line 233
    return-void
.end method

.method declared-synchronized sendCommandInternal(Ljava/lang/String;I)Z
    .locals 3

    .prologue
    .line 292
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mRefCount:I

    if-lez v0, :cond_2

    .line 293
    if-nez p1, :cond_0

    .line 294
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "fail to send a command. The command is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 296
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "fail to send a command. The command is empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectId:I

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendCommand(ILjava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 301
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 304
    :cond_2
    :try_start_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fail to send a command["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]. Maybe error in your SapaProcessor"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public declared-synchronized sendStream(Ljava/nio/ByteBuffer;I)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 248
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mRefCount:I

    if-lez v1, :cond_4

    .line 249
    if-nez p1, :cond_0

    .line 250
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "fail to send a command. The stream is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 252
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v1

    if-nez v1, :cond_1

    .line 253
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "fail to send a command. The stream is NOT direct buffer."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257
    :cond_1
    :try_start_2
    iget v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectId:I

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mName:Ljava/lang/String;

    invoke-static {v1, v2, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendStream(ILjava/lang/String;Ljava/nio/ByteBuffer;I)I

    move-result v1

    if-eqz v1, :cond_2

    .line 258
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "fail to send a stream. Please check the processing module\'s return value."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 261
    :catch_0
    move-exception v1

    .line 266
    :try_start_3
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 267
    new-instance v2, Ljava/lang/StringBuffer;

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v4, "stream:%d:"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 268
    array-length v3, v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_0
    if-lt v0, v3, :cond_3

    .line 271
    :try_start_4
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->sendCommand(Ljava/lang/String;I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 272
    :cond_2
    monitor-exit p0

    return-void

    .line 268
    :cond_3
    :try_start_5
    aget-byte v4, v1, v0

    const-string v5, "%02x"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 274
    :catch_1
    move-exception v0

    .line 275
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 280
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "fail to send a stream. Maybe error in your SapaProcessor"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public declared-synchronized setByPassEnabled(Z)V
    .locals 3

    .prologue
    .line 347
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mRefCount:I

    if-lez v0, :cond_0

    .line 348
    if-eqz p1, :cond_1

    .line 349
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectId:I

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "!setbypassenabledtrue"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendCommand(ILjava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 354
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 351
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectId:I

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "!setbypassenabledfalse"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendCommand(ILjava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 347
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final setMessageListener(Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$MessageListener;)V
    .locals 2

    .prologue
    .line 475
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getInstance()Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    move-result-object v0

    .line 476
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->setClientMessageListener(Ljava/lang/Integer;Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$MessageListener;)V

    .line 478
    return-void
.end method

.method public setStatusListener(Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;)V
    .locals 2

    .prologue
    .line 505
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getInstance()Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    move-result-object v0

    .line 506
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->setClientStatusListener(Ljava/lang/Integer;Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;)V

    .line 507
    return-void
.end method

.method public declared-synchronized setTransportEnabled(Z)V
    .locals 3

    .prologue
    .line 364
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mRefCount:I

    if-lez v0, :cond_0

    .line 365
    if-eqz p1, :cond_1

    .line 366
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectId:I

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "!enable_sync"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendCommand(ILjava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 375
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 370
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mObjectId:I

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->mName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "!disable_sync"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendCommand(ILjava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 364
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
