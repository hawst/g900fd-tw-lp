.class final Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult;
.super Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;
.source "SourceFile"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;-><init>(Ljava/lang/String;)V

    .line 33
    return-void
.end method


# virtual methods
.method public getPorts()Ljava/util/List;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 83
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult;->result()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 115
    :goto_0
    return-object v0

    .line 88
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONTokener;

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult;->mJsonString:Ljava/lang/String;

    invoke-direct {v0, v2}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONArray;

    .line 89
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 90
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 91
    :goto_1
    if-lt v3, v4, :cond_1

    move-object v0, v2

    .line 112
    goto :goto_0

    .line 92
    :cond_1
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "input"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 93
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "input"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 94
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "type"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 96
    const-string v7, "midi"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 97
    new-instance v7, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-direct {v7, v8, v9, v5, v6}, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;-><init>(ZZLjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 99
    :cond_2
    new-instance v7, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-direct {v7, v8, v9, v5, v6}, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;-><init>(ZZLjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 113
    :catch_0
    move-exception v0

    .line 114
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    move-object v0, v1

    .line 115
    goto :goto_0

    .line 102
    :cond_3
    :try_start_1
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "output"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 103
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "type"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 105
    const-string v7, "midi"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 106
    new-instance v7, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-direct {v7, v8, v9, v5, v6}, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;-><init>(ZZLjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 108
    :cond_4
    new-instance v7, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v7, v8, v9, v5, v6}, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;-><init>(ZZLjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
