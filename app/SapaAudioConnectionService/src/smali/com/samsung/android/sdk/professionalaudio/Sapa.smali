.class public final Lcom/samsung/android/sdk/professionalaudio/Sapa;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final SUPPORT_BOOST_MODE:I = 0x7

.field public static final SUPPORT_CAPTURE_DEVICE:I = 0x4

.field public static final SUPPORT_DUMMY:I = 0x5

.field public static final SUPPORT_HIGH_LATENCY:I = 0x3

.field public static final SUPPORT_LOW_LATENCY:I = 0x1

.field public static final SUPPORT_MID_LATENCY:I = 0x2

.field public static final SUPPORT_PLAYBACK_DEVICE:I = 0x6

.field static final SUPPORT_RESERVED:I = 0x0

.field public static final SUPPORT_SAMPLE_RATE_44100:I = 0x9

.field public static final SUPPORT_SAMPLE_RATE_48000:I = 0x8

.field private static mContext:Ljava/lang/ref/WeakReference;

.field private static mFeature:Lcom/samsung/android/sdk/professionalaudio/SapaFeature;

.field static mIsInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;->mIsInitialized:Z

    .line 39
    sput-object v1, Lcom/samsung/android/sdk/professionalaudio/Sapa;->mContext:Ljava/lang/ref/WeakReference;

    .line 41
    sput-object v1, Lcom/samsung/android/sdk/professionalaudio/Sapa;->mFeature:Lcom/samsung/android/sdk/professionalaudio/SapaFeature;

    .line 180
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;->mFeature:Lcom/samsung/android/sdk/professionalaudio/SapaFeature;

    .line 52
    return-void
.end method

.method static getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;->mContext:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 129
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 131
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private insertLog(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 233
    const/4 v0, -0x1

    .line 236
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.samsung.android.providers.context"

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 237
    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_0
    const-string v1, "SM_SDK"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "versionCode: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 243
    const-string v0, "com.samsung.android.providers.context.permission.WRITE_USE_APP_FEATURE_SURVEY"

    invoke-virtual {p1, v0}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 245
    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    .line 238
    :catch_0
    move-exception v1

    .line 239
    const-string v1, "SM_SDK"

    const-string v2, "Could not find ContextProvider"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 248
    :cond_0
    const-string v0, "SM_SDK"

    const-string v1, "Add com.samsung.android.providers.context.permission.WRITE_USE_APP_FEATURE_SURVEY permission"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :goto_1
    return-void

    .line 252
    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 254
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    .line 255
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/Sapa;->getVersionCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 257
    const-string v3, "app_id"

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string v1, "feature"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 262
    const-string v2, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 263
    const-string v2, "data"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 265
    const-string v0, "com.samsung.android.providers.context"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 267
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1
.end method


# virtual methods
.method public getVersionCode()I
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x2

    return v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    const-string v0, "2.0.0"

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 89
    if-nez p1, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The context is not vaild parameter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_0
    :try_start_0
    sget-boolean v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;->mIsInitialized:Z

    if-nez v0, :cond_1

    .line 95
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/Sapa;->insertLog(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :cond_1
    :try_start_1
    const-string v0, "apa_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3

    .line 110
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->isAPILevelAllowed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 111
    new-instance v0, Lcom/samsung/android/sdk/a;

    const-string v1, "Your sdk version is NOT allowed"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/a;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 97
    :catch_0
    move-exception v0

    .line 98
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "com.samsung.android.providers.context.permission.WRITE_USE_APP_FEATURE_SURVEY permission is required."

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :catch_1
    move-exception v0

    .line 103
    new-instance v0, Lcom/samsung/android/sdk/a;

    const-string v1, "Not Support JAM"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/a;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 104
    :catch_2
    move-exception v0

    .line 105
    new-instance v0, Lcom/samsung/android/sdk/a;

    const-string v1, "Not Support JAM."

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/a;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 106
    :catch_3
    move-exception v0

    .line 107
    new-instance v0, Lcom/samsung/android/sdk/a;

    const-string v1, "Not Support JAM"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/a;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 114
    :cond_2
    sput-boolean v2, Lcom/samsung/android/sdk/professionalaudio/Sapa;->mIsInitialized:Z

    .line 115
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;->mContext:Ljava/lang/ref/WeakReference;

    .line 119
    :try_start_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.samsung.android.sdk.professionalaudio.action.START_MONITOR"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_4

    .line 125
    :goto_0
    return-void

    .line 122
    :catch_4
    move-exception v0

    goto :goto_0

    .line 120
    :catch_5
    move-exception v0

    goto :goto_0
.end method

.method public isFeatureEnabled(I)Z
    .locals 1

    .prologue
    .line 222
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;->mFeature:Lcom/samsung/android/sdk/professionalaudio/SapaFeature;

    if-nez v0, :cond_0

    .line 223
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;->mFeature:Lcom/samsung/android/sdk/professionalaudio/SapaFeature;

    .line 225
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;->mFeature:Lcom/samsung/android/sdk/professionalaudio/SapaFeature;

    if-eqz v0, :cond_1

    .line 226
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;->mFeature:Lcom/samsung/android/sdk/professionalaudio/SapaFeature;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;->isFeatureEnabled(I)Z

    move-result v0

    .line 228
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
