.class public Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private mInstanceId:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->mInstanceId:Ljava/lang/String;

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->mPackageName:Ljava/lang/String;

    .line 15
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->mPackageName:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->mInstanceId:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public getInstanceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->mInstanceId:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->mPackageName:Ljava/lang/String;

    return-object v0
.end method
