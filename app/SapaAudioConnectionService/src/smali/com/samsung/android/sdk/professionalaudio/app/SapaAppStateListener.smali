.class public interface abstract Lcom/samsung/android/sdk/professionalaudio/app/SapaAppStateListener;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract onAppActivated(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)V
.end method

.method public abstract onAppChanged(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)V
.end method

.method public abstract onAppDeactivated(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)V
.end method

.method public abstract onAppInstalled(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)V
.end method

.method public abstract onAppUninstalled(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)V
.end method

.method public abstract onTransportMasterChanged(Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;)V
.end method
