.class final Lcom/samsung/android/sdk/professionalaudio/ApaGetParametersCommandResult;
.super Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;
.source "SourceFile"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;-><init>(Ljava/lang/String;)V

    .line 31
    return-void
.end method


# virtual methods
.method public getParameters()Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetParametersCommandResult;->result()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 68
    :goto_0
    return-object v0

    .line 42
    :cond_0
    new-instance v2, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;

    invoke-direct {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;-><init>()V

    .line 44
    :try_start_0
    new-instance v0, Lorg/json/JSONTokener;

    iget-object v3, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetParametersCommandResult;->mJsonString:Ljava/lang/String;

    invoke-direct {v0, v3}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONArray;

    .line 46
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 47
    const/4 v3, 0x1

    :goto_1
    if-lt v3, v4, :cond_1

    move-object v0, v2

    .line 68
    goto :goto_0

    .line 48
    :cond_1
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 49
    const-string v6, "latency"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 50
    const-string v6, "latency"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->setLatency(Ljava/lang/String;)V

    .line 52
    :cond_2
    const-string v6, "samplerate"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 53
    const-string v6, "samplerate"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->setSampleRate(I)V

    .line 55
    :cond_3
    const-string v6, "buffersize"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 56
    const-string v6, "buffersize"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->setBufferSize(I)V

    .line 58
    :cond_4
    const-string v6, "availablesapaprocessorcount"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 59
    const-string v6, "availablesapaprocessorcount"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;->setAvailableSapaProcessorCount(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 64
    :catch_0
    move-exception v0

    .line 65
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    move-object v0, v1

    .line 66
    goto :goto_0
.end method
