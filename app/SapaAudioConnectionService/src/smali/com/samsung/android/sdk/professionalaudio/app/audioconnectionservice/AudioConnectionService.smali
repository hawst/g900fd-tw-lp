.class public Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final ACTION_MAIN:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.action.MAIN"

.field private static final CATEGORY_LAUNCHER:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.category.LAUNCHER"

.field private static final INSTANCE_SEPARATOR:Ljava/lang/String; = "^"

.field private static final MAX_INSTANCES:I = 0x8

.field static final MSG_WATCH_REMOTE_LISTENER:I = 0xf1ac0

.field private static final TAG:Ljava/lang/String; = "professionalaudioconnection:audioconnectionservice:j"


# instance fields
.field private mActiveInstances:Ljava/util/HashMap;

.field private final mBinder:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;

.field private mInstallReceiver:Landroid/content/BroadcastReceiver;

.field private mInstalledApps:Ljava/util/HashMap;

.field private mIsRegistered:Z

.field private mRemoteListeners:Ljava/util/Map;

.field private mServiceHandler:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$ServiceHandler;

.field private mTakenIds:Ljava/util/ArrayList;

.field private mTransportMaster:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 524
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$1;-><init>(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mInstallReceiver:Landroid/content/BroadcastReceiver;

    .line 544
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$2;-><init>(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mBinder:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->checkRemoteListeners()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->sendWatchRemoteMessage()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;)Ljava/util/List;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getAllActiveApp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getInstalledAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;)Ljava/util/List;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getAllInstalledApp()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->removeFromAppList(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->removeRemoteListener(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->runAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->declareBeingTransportMaster(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getTransportMaster()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->onAppInstalled(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->onAppUninstalled(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->activateApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->addActiveApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->addRemoteListener(Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->changeAppInfo(Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->deactivateApp(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getActiveAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    move-result-object v0

    return-object v0
.end method

.method private activateApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 333
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->activateAppGeneral(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized activateAppGeneral(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 308
    monitor-enter p0

    :try_start_0
    const-string v0, ""

    .line 309
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mInstalledApps:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->isMultiInstanceEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getApp()Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 312
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getBackgroundServiceAction()Ljava/lang/String;

    move-result-object v1

    .line 313
    if-eqz v1, :cond_1

    .line 314
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getInstanceId(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)Ljava/lang/String;

    move-result-object v0

    .line 315
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->setInstanceId(Ljava/lang/String;)V

    .line 317
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 318
    invoke-static {v2, p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->setSapaAppInfo(Landroid/content/Intent;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V

    .line 319
    const/4 v1, 0x1

    invoke-static {v2, v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->setState(Landroid/content/Intent;I)V

    .line 322
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getCallerPackageName()Ljava/lang/String;

    move-result-object v1

    .line 323
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->removeCallerPackageName()V

    .line 324
    const-string v3, "com.samsung.android.sdk.professionalaudio.key.callerpackagename"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 325
    const-string v3, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " is activating "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 329
    :cond_1
    monitor-exit p0

    return-object v0

    .line 308
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized addActiveApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
    .locals 3

    .prologue
    .line 277
    monitor-enter p0

    if-eqz p1, :cond_1

    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getApp()Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->isMultiInstanceEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getApp()Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getApp()Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getApp()Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was added"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getApp()Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->notifyAppActivated(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    :cond_1
    monitor-exit p0

    return-void

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized addRemoteListener(Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;)V
    .locals 3

    .prologue
    .line 256
    monitor-enter p0

    :try_start_0
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Listener to be added "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " listener was overwritten."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    monitor-exit p0

    return-void

    .line 256
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized changeAppInfo(Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
    .locals 3

    .prologue
    .line 246
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    invoke-virtual {p2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->notifyAppChanged(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    :goto_0
    monitor-exit p0

    return-void

    .line 250
    :cond_0
    :try_start_1
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fail to change app info : invalid id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized checkIsLast(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 223
    monitor-enter p0

    .line 224
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    .line 225
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    add-int/lit8 v0, v1, 0x1

    if-le v0, v3, :cond_1

    move v0, v2

    .line 231
    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    move v0, v1

    :cond_1
    move v1, v0

    .line 230
    goto :goto_0

    :cond_2
    move v0, v3

    .line 231
    goto :goto_1

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized checkRemoteListeners()V
    .locals 3

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    :cond_0
    monitor-exit p0

    return-void

    .line 90
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 91
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 92
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 94
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 95
    if-eqz v1, :cond_2

    .line 97
    :try_start_2
    invoke-interface {v1}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;->check()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 98
    :catch_0
    move-exception v1

    .line 100
    :try_start_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->removeTheUnansweredListener(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized deactivateApp(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 199
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    if-eqz v0, :cond_1

    .line 202
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;

    .line 203
    invoke-interface {v1}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;->check()V

    .line 204
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 205
    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->checkIsLast(Ljava/lang/String;)Z

    move-result v3

    .line 206
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getBackgroundServiceAction()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 207
    invoke-static {v4, p1, v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->setInstanceId(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    if-eqz v3, :cond_0

    .line 209
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->stopService(Landroid/content/Intent;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214
    :goto_0
    const/4 v0, 0x1

    .line 219
    :goto_1
    monitor-exit p0

    return v0

    .line 211
    :cond_0
    const/4 v0, 0x2

    :try_start_2
    invoke-static {v4, v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->setState(Landroid/content/Intent;I)V

    .line 212
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 215
    :catch_0
    move-exception v0

    move v0, v2

    .line 216
    goto :goto_1

    :cond_1
    move v0, v2

    .line 219
    goto :goto_1

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized declareBeingTransportMaster(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 657
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mTransportMaster:Ljava/lang/String;

    .line 658
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->notifyTransportMasterChanged(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 659
    monitor-exit p0

    return-void

    .line 657
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized fillInstalledApps()V
    .locals 4

    .prologue
    .line 365
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getResolveInfo()Ljava/util/List;

    move-result-object v0

    .line 367
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369
    :try_start_1
    new-instance v2, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;-><init>(Landroid/content/pm/ResolveInfo;)V

    .line 370
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mInstalledApps:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lorg/apache/http/impl/cookie/DateParseException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 371
    :catch_0
    move-exception v0

    .line 372
    :try_start_2
    const-string v2, "professionalaudioconnection:audioconnectionservice:j"

    invoke-virtual {v0}, Lorg/apache/http/impl/cookie/DateParseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 365
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 375
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized getActiveAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    .locals 1

    .prologue
    .line 304
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getAllActiveApp()Ljava/util/List;
    .locals 2

    .prologue
    .line 298
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 299
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    monitor-exit p0

    return-object v0

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getAllInstalledApp()Ljava/util/List;
    .locals 2

    .prologue
    .line 160
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mInstalledApps:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mInstalledApps:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getInstalledAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    .locals 3

    .prologue
    .line 152
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mInstalledApps:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mInstalledApps:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :goto_0
    monitor-exit p0

    return-object v0

    .line 155
    :cond_0
    :try_start_1
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not exist, Please check your manifest again."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getInstanceId(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 340
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->isMultiInstanceEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 341
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->setInstanceId(Ljava/lang/String;)V

    .line 342
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 361
    :goto_0
    monitor-exit p0

    return-object v0

    .line 344
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getApp()Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v0

    .line 345
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 346
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mTakenIds:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 348
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mTakenIds:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 340
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 352
    :cond_1
    :try_start_2
    const-string v0, ""

    .line 353
    const/4 v0, 0x1

    move v1, v0

    :goto_1
    const/16 v0, 0x8

    if-gt v1, v0, :cond_3

    .line 354
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "^"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 355
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mTakenIds:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 357
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mTakenIds:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 353
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 361
    :cond_3
    const-string v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized getLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 165
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    .line 166
    if-eqz v0, :cond_0

    .line 167
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 168
    if-eqz v1, :cond_0

    .line 169
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 172
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;

    .line 174
    invoke-interface {v1}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;->check()V

    .line 175
    invoke-static {v2, v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->setSapaAppInfo(Landroid/content/Intent;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    .line 183
    :goto_0
    monitor-exit p0

    return-object v0

    .line 178
    :catch_0
    move-exception v0

    .line 179
    :try_start_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 183
    :cond_0
    :try_start_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method private getResolveInfo()Ljava/util/List;
    .locals 3

    .prologue
    .line 378
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 379
    if-eqz v0, :cond_0

    .line 380
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 381
    const-string v2, "com.samsung.android.sdk.professionalaudio.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 382
    const-string v2, "com.samsung.android.sdk.professionalaudio.action.MAIN"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 383
    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 387
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method private declared-synchronized getTransportMaster()Ljava/lang/String;
    .locals 1

    .prologue
    .line 653
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mTransportMaster:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized notifyAppActivated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 469
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 471
    :try_start_1
    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;->onAppActivated(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 472
    :catch_0
    move-exception v0

    .line 473
    :try_start_2
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "App activated notification impossible as ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]listener not available"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 469
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 476
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized notifyAppChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 494
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 496
    :try_start_1
    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;->onAppChanged(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 497
    :catch_0
    move-exception v0

    .line 498
    :try_start_2
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "App changed notification impossible as ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]listener not available"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 494
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 501
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized notifyAppDeactivated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 482
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    :try_start_1
    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;->onAppDeactivated(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 485
    :catch_0
    move-exception v0

    .line 486
    :try_start_2
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "App deactivated notification impossible as ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]listener not available"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 482
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 489
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized notifyAppInstalled(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 445
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 447
    :try_start_1
    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;->onAppInstalled(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 448
    :catch_0
    move-exception v0

    .line 449
    :try_start_2
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "App installed notification impossible as ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]listener not available"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 445
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 452
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized notifyAppUninstalled(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 457
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 459
    :try_start_1
    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;->onAppUninstalled(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 460
    :catch_0
    move-exception v0

    .line 461
    :try_start_2
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "App uninstalled notification impossible as ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]listener not available"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 457
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 464
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized notifyTransportMasterChanged(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 506
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 508
    :try_start_1
    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;->onTransportMasterChanged(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 509
    :catch_0
    move-exception v0

    .line 510
    :try_start_2
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TransportMaster changed notification impossible as ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]listener not available"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 506
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 513
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized onAppInstalled(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 422
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getResolveInfo()Ljava/util/List;

    move-result-object v0

    .line 423
    if-eqz v0, :cond_1

    .line 424
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getResolveInfo()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 425
    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 428
    :try_start_1
    new-instance v1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    invoke-direct {v1, v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;-><init>(Landroid/content/pm/ResolveInfo;)V

    .line 429
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mInstalledApps:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "App "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was added to installed audio apps"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->notifyAppInstalled(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/apache/http/impl/cookie/DateParseException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 440
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 433
    :catch_0
    move-exception v0

    .line 434
    :try_start_2
    const-string v1, "professionalaudioconnection:audioconnectionservice:j"

    invoke-virtual {v0}, Lorg/apache/http/impl/cookie/DateParseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 422
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized onAppUninstalled(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 397
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mInstalledApps:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->notifyAppUninstalled(Ljava/lang/String;)V

    .line 399
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mInstalledApps:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "App "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was removed from installed audio apps"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->removeUninstalledFromActive(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 404
    :cond_0
    monitor-exit p0

    return-void

    .line 397
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private registerInstallReceiver()V
    .locals 2

    .prologue
    .line 516
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 517
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 518
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 519
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 520
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mInstallReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 521
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mIsRegistered:Z

    .line 522
    return-void
.end method

.method private declared-synchronized removeFromAppList(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 236
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    .line 238
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mTakenIds:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 240
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->notifyAppDeactivated(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " removed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    :cond_0
    monitor-exit p0

    return-void

    .line 236
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized removeRemoteListener(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 266
    monitor-enter p0

    :try_start_0
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Listener to be removed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    monitor-exit p0

    return-void

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized removeTheUnansweredListener(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was unanswered. It\'ll be removed from the store."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    if-eqz p1, :cond_0

    .line 75
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->removeRemoteListener(Ljava/lang/String;)V

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    .line 78
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getApp()Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 79
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getApp()Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v0

    .line 80
    if-eqz v0, :cond_1

    .line 81
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->removeFromAppList(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    :cond_1
    monitor-exit p0

    return-void

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized removeUninstalledFromActive(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 408
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 409
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    .line 410
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 411
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getApp()Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;->getInstanceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 408
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 414
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 416
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->notifyAppDeactivated(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 419
    :cond_2
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized runAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    if-eqz v0, :cond_0

    .line 191
    :try_start_1
    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;->onActionCalled(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 196
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 192
    :catch_0
    move-exception v0

    .line 193
    :try_start_2
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    const-string v1, "Action cannot be called due remote error."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized sendWatchRemoteMessage()V
    .locals 4

    .prologue
    .line 108
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mServiceHandler:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$ServiceHandler;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mServiceHandler:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$ServiceHandler;

    const v1, 0xf1ac0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    :cond_0
    monitor-exit p0

    return-void

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mBinder:Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService$Stub;

    return-object v0
.end method

.method public declared-synchronized onCreate()V
    .locals 5

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 121
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 124
    if-eqz v0, :cond_0

    .line 125
    const-string v1, "professionalaudioconnection:audioconnectionservice:j"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AudioConnectionService v. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132
    :goto_0
    :try_start_2
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$ServiceHandler;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$ServiceHandler;-><init>(Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mServiceHandler:Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService$ServiceHandler;

    .line 133
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mTransportMaster:Ljava/lang/String;

    .line 134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mIsRegistered:Z

    .line 135
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mInstalledApps:Ljava/util/HashMap;

    .line 136
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mActiveInstances:Ljava/util/HashMap;

    .line 137
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mTakenIds:Ljava/util/ArrayList;

    .line 139
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->fillInstalledApps()V

    .line 140
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mRemoteListeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mInstalledApps:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 142
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->notifyAppInstalled(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 127
    :cond_0
    :try_start_3
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    const-string v1, "Version could not be determined."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 129
    :catch_0
    move-exception v0

    .line 130
    :try_start_4
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    const-string v1, "Version could not be determined."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 146
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->registerInstallReceiver()V

    .line 148
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->sendWatchRemoteMessage()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 149
    monitor-exit p0

    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 663
    const-string v0, "professionalaudioconnection:audioconnectionservice:j"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mIsRegistered:Z

    if-eqz v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->mInstallReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/audioconnectionservice/AudioConnectionService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 667
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 668
    return-void
.end method
