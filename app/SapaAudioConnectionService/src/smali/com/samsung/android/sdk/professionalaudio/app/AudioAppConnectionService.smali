.class public interface abstract Lcom/samsung/android/sdk/professionalaudio/app/AudioAppConnectionService;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract activateApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)Ljava/lang/String;
.end method

.method public abstract addActiveApp(Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
.end method

.method public abstract addRemoteListener(Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/RemoteListener;)V
.end method

.method public abstract changeAppInfo(Ljava/lang/String;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
.end method

.method public abstract deactivateApp(Ljava/lang/String;)Z
.end method

.method public abstract declareBeingTransportMaster(Ljava/lang/String;)V
.end method

.method public abstract getActiveAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
.end method

.method public abstract getAllActiveApp()Ljava/util/List;
.end method

.method public abstract getAllInstalledApp()Ljava/util/List;
.end method

.method public abstract getInstalledAppInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
.end method

.method public abstract getLaunchIntent(Ljava/lang/String;)Landroid/content/Intent;
.end method

.method public abstract getTransportMaster()Ljava/lang/String;
.end method

.method public abstract removeFromActiveApps(Ljava/lang/String;)V
.end method

.method public abstract removeRemoteListener(Ljava/lang/String;)V
.end method

.method public abstract runAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method
