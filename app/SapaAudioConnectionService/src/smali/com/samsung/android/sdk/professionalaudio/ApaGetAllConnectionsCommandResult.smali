.class final Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult;
.super Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;
.source "SourceFile"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;-><init>(Ljava/lang/String;)V

    .line 59
    return-void
.end method


# virtual methods
.method public getConnections()Ljava/util/List;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult;->result()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 81
    :goto_0
    return-object v0

    .line 70
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONTokener;

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult;->mJsonString:Ljava/lang/String;

    invoke-direct {v0, v2}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONArray;

    .line 72
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 73
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 74
    const/4 v3, 0x1

    :goto_1
    if-lt v3, v4, :cond_1

    move-object v0, v2

    .line 78
    goto :goto_0

    .line 75
    :cond_1
    new-instance v5, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult$OneConnection;

    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v7, "input"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 76
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    const-string v8, "output"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult$OneConnection;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 79
    :catch_0
    move-exception v0

    .line 80
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    move-object v0, v1

    .line 81
    goto :goto_0
.end method
