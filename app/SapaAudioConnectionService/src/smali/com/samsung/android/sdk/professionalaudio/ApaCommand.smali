.class Lcom/samsung/android/sdk/professionalaudio/ApaCommand;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field mInputs:Lorg/json/JSONArray;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaCommand;->mInputs:Lorg/json/JSONArray;

    .line 31
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaCommand;->mInputs:Lorg/json/JSONArray;

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v2, "command"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    :goto_0
    return-void

    .line 32
    :catch_0
    move-exception v0

    .line 33
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method newResult(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method toJsonString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaCommand;->mInputs:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
