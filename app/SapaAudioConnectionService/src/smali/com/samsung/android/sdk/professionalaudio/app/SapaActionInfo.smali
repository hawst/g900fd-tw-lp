.class public Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field private static final ACTION_ID:Ljava/lang/String; = "action_id"

.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final ENABLED:Ljava/lang/String; = "enabled"

.field private static final ICON_RES:Ljava/lang/String; = "icon_res"

.field private static final PACKAGE:Ljava/lang/String; = "package"

.field private static final VISIBLE:Ljava/lang/String; = "visible"


# instance fields
.field private mIconResId:I

.field private mId:Ljava/lang/String;

.field private mIsEnabled:Z

.field private mIsVisible:Z

.field private mPackageName:Ljava/lang/String;

.field private mParcelInfo:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 205
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 103
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mParcelInfo:Landroid/os/Bundle;

    .line 47
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mId:Ljava/lang/String;

    .line 48
    iput p2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIconResId:I

    .line 49
    iput-boolean v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsEnabled:Z

    .line 50
    iput-object p3, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mPackageName:Ljava/lang/String;

    .line 51
    iput-boolean v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsVisible:Z

    .line 52
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 75
    iput-boolean p4, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsEnabled:Z

    .line 76
    iput-boolean p5, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsVisible:Z

    .line 77
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 224
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mParcelInfo:Landroid/os/Bundle;

    .line 225
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mParcelInfo:Landroid/os/Bundle;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 226
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "action_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mId:Ljava/lang/String;

    .line 227
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "icon_res"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIconResId:I

    .line 228
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "enabled"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsEnabled:Z

    .line 229
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mPackageName:Ljava/lang/String;

    .line 230
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "visible"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsVisible:Z

    .line 232
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 257
    if-ne p0, p1, :cond_1

    .line 290
    :cond_0
    :goto_0
    return v0

    .line 260
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 261
    goto :goto_0

    .line 263
    :cond_2
    instance-of v2, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;

    if-nez v2, :cond_3

    move v0, v1

    .line 264
    goto :goto_0

    .line 266
    :cond_3
    check-cast p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;

    .line 267
    iget v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIconResId:I

    iget v3, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIconResId:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 268
    goto :goto_0

    .line 270
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mId:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 271
    iget-object v2, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mId:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 272
    goto :goto_0

    .line 274
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mId:Ljava/lang/String;

    iget-object v3, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 275
    goto :goto_0

    .line 277
    :cond_6
    iget-boolean v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsEnabled:Z

    iget-boolean v3, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsEnabled:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 278
    goto :goto_0

    .line 280
    :cond_7
    iget-boolean v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsVisible:Z

    iget-boolean v3, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsVisible:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 281
    goto :goto_0

    .line 283
    :cond_8
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mPackageName:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 284
    iget-object v2, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mPackageName:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 285
    goto :goto_0

    .line 287
    :cond_9
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mPackageName:Ljava/lang/String;

    iget-object v3, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 288
    goto :goto_0
.end method

.method public final getIcon(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 186
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 187
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    .line 188
    iget v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIconResId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 186
    return-object v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 240
    .line 242
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIconResId:I

    add-int/lit8 v0, v0, 0x1f

    .line 243
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mId:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    .line 244
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsEnabled:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v4

    .line 245
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsVisible:Z

    if-eqz v4, :cond_2

    :goto_2
    add-int/2addr v0, v2

    .line 246
    mul-int/lit8 v0, v0, 0x1f

    .line 247
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mPackageName:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 246
    :goto_3
    add-int/2addr v0, v1

    .line 248
    return v0

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v3

    .line 244
    goto :goto_1

    :cond_2
    move v2, v3

    .line 245
    goto :goto_2

    .line 247
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsEnabled:Z

    return v0
.end method

.method public final isVisible()Z
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsVisible:Z

    return v0
.end method

.method public final setEnabled(Z)V
    .locals 0

    .prologue
    .line 123
    iput-boolean p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsEnabled:Z

    .line 124
    return-void
.end method

.method public final setIcon(I)V
    .locals 0

    .prologue
    .line 167
    iput p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIconResId:I

    .line 168
    return-void
.end method

.method public final setVisible(Z)V
    .locals 0

    .prologue
    .line 145
    iput-boolean p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsVisible:Z

    .line 146
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 214
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "action_id"

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "icon_res"

    iget v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIconResId:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 216
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "enabled"

    iget-boolean v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsEnabled:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 217
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "package"

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "visible"

    iget-boolean v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mIsVisible:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 219
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->mParcelInfo:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 220
    return-void
.end method
