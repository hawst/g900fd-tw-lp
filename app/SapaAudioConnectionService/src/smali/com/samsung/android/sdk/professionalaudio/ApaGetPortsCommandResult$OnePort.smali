.class public final Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private mIsInputPort:Z

.field private mIsMidiPort:Z

.field private mPortName:Ljava/lang/String;

.field private mTypeName:Ljava/lang/String;


# direct methods
.method constructor <init>(ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-boolean p1, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;->mIsInputPort:Z

    .line 48
    iput-object p3, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;->mPortName:Ljava/lang/String;

    .line 49
    iput-object p4, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;->mTypeName:Ljava/lang/String;

    .line 50
    iput-boolean p2, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;->mIsMidiPort:Z

    .line 51
    return-void
.end method


# virtual methods
.method public getPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;->mPortName:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;->mTypeName:Ljava/lang/String;

    return-object v0
.end method

.method public isInputPort()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;->mIsInputPort:Z

    return v0
.end method

.method public isMidiPort()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;->mIsMidiPort:Z

    return v0
.end method
