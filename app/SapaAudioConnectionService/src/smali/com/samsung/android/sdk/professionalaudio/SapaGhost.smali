.class public Lcom/samsung/android/sdk/professionalaudio/SapaGhost;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static KillAllClientAndDaemon()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 17
    sget-object v1, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-nez v1, :cond_1

    .line 23
    :cond_0
    :goto_0
    return v0

    .line 20
    :cond_1
    const-string v1, "*1"

    invoke-static {v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendControlCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 23
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isNotificationNeed()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 7
    sget-object v1, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-nez v1, :cond_1

    .line 13
    :cond_0
    :goto_0
    return v0

    .line 10
    :cond_1
    const-string v1, "*3"

    invoke-static {v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendControlCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13
    const/4 v0, 0x1

    goto :goto_0
.end method
