.class public Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field private static final APPINFO_ACTIONS:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.actions"

.field private static final APPINFO_APP_NAME:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.appname"

.field private static final APPINFO_CATEGORY:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.category"

.field private static final APPINFO_CONFIGURATION:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.configuration"

.field private static final APPINFO_DESCRIPTION:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.description"

.field private static final APPINFO_INSTANCE_ID:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.instanceid"

.field private static final APPINFO_METADATA:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.metadata"

.field private static final APPINFO_MULTIINSTANCE_ENABLED:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.multiinstanceenabled"

.field private static final APPINFO_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.packagename"

.field private static final APPINFO_PORTS:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.ports"

.field private static final APPINFO_PORT_AUDIO_IN:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.audioinportcount"

.field private static final APPINFO_PORT_AUDIO_OUT:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.audiooutportcount"

.field private static final APPINFO_PORT_MIDI_IN:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.midiinportcount"

.field private static final APPINFO_PORT_MIDI_OUT:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.midioutportcount"

.field private static final APPINFO_PRODUCT_GROUP_NAME:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.productgroupname"

.field private static final APPINFO_SERVICE_ACTION:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.backgroundservice"

.field private static final APPINFO_VENDOR_NAME:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.vendorname"

.field private static final APPINFO_VERSION:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.sapaappinfo.version"

.field private static final APP_INFO:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.appinfo"

.field public static final CATEGORY_EFFECT:I = 0x2

.field private static final CATEGORY_EFFECT_STRING:Ljava/lang/String; = "effect"

.field public static final CATEGORY_INSTRUMENT:I = 0x1

.field private static final CATEGORY_INSTRUMENT_STRING:Ljava/lang/String; = "instrument"

.field public static final CATEGORY_UTILITY:I = 0x3

.field private static final CATEGORY_UTILITY_STRING:Ljava/lang/String; = "utility"

.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final KEY_CALLER_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.key.callerpackagename"

.field public static final STATE_ACTIVATING:I = 0x1

.field public static final STATE_DEACTIVATING:I = 0x2

.field private static final STATE_NAME:Ljava/lang/String; = "com.samsung.android.sdk.professionalaudio.processname"

.field private static final TAG:Ljava/lang/String; = "professionalaudioconnection:library:j:SapaAppInfo "

.field private static final VERSION_CODE:I = 0x7


# instance fields
.field private mActions:Landroid/util/SparseArray;

.field private mApplicationName:Ljava/lang/String;

.field private mAudioInputPortCount:I

.field private mAudioOutputPortCount:I

.field private mBackgroundService:Ljava/lang/String;

.field private mCategory:I

.field private mConfiguration:Landroid/os/Bundle;

.field private mDescription:Ljava/lang/String;

.field private mInstanceId:Ljava/lang/String;

.field private mMetaData:Landroid/os/Bundle;

.field private mMidiInputPortCount:I

.field private mMidiOutputPortCount:I

.field private mMultiInstanceEnabled:Z

.field private mPackageName:Ljava/lang/String;

.field private mParcelInfo:Landroid/os/Bundle;

.field private mPorts:Ljava/util/ArrayList;

.field private mProductGroup:Ljava/lang/String;

.field private mVendorName:Ljava/lang/String;

.field private mVersionName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 132
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    .line 141
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    .line 142
    return-void
.end method

.method public constructor <init>(Landroid/content/pm/ResolveInfo;)V
    .locals 3

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;-><init>()V

    .line 181
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    .line 183
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    .line 184
    if-eqz v1, :cond_0

    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.category"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.appname"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.backgroundservice"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.backgroundservice"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mBackgroundService:Ljava/lang/String;

    .line 188
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.appname"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mApplicationName:Ljava/lang/String;

    .line 189
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.category"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getCategory(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mCategory:I

    .line 194
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.midiinportcount"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiInputPortCount:I

    .line 195
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.midioutportcount"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiOutputPortCount:I

    .line 196
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.audioinportcount"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioInputPortCount:I

    .line 197
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.audiooutportcount"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioOutputPortCount:I

    .line 198
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.version"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.version"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 198
    :goto_0
    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mVersionName:Ljava/lang/String;

    .line 200
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.vendorname"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 201
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.vendorname"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 200
    :goto_1
    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mVendorName:Ljava/lang/String;

    .line 202
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.productgroupname"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 203
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.productgroupname"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 202
    :goto_2
    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mProductGroup:Ljava/lang/String;

    .line 205
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.multiinstanceenabled"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 204
    iput-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMultiInstanceEnabled:Z

    .line 206
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.description"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 207
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.description"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 206
    :goto_3
    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mDescription:Ljava/lang/String;

    .line 208
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.category"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 209
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.appname"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 210
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.backgroundservice"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 211
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.midiinportcount"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 212
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.midioutportcount"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 213
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.audioinportcount"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 214
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.audiooutportcount"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 215
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.version"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 216
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.vendorname"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 217
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.productgroupname"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 218
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.multiinstanceenabled"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 219
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.description"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 220
    iput-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMetaData:Landroid/os/Bundle;

    .line 222
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mInstanceId:Ljava/lang/String;

    .line 223
    return-void

    .line 191
    :cond_0
    new-instance v0, Lorg/apache/http/impl/cookie/DateParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Infomation about "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 192
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not prepared correctly"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 191
    invoke-direct {v0, v1}, Lorg/apache/http/impl/cookie/DateParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :cond_1
    const-string v0, ""

    goto/16 :goto_0

    .line 201
    :cond_2
    const-string v0, ""

    goto/16 :goto_1

    .line 203
    :cond_3
    const-string v0, ""

    goto/16 :goto_2

    .line 207
    :cond_4
    const-string v0, ""

    goto :goto_3
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 255
    return-void
.end method

.method private clearMarkers()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 365
    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioInputPortCount:I

    .line 366
    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioOutputPortCount:I

    .line 367
    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiInputPortCount:I

    .line 368
    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiOutputPortCount:I

    .line 369
    return-void
.end method

.method public static final getApp(Landroid/content/Intent;)Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 832
    .line 833
    if-eqz p0, :cond_1

    .line 834
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.instanceid"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 835
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.packagename"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v1

    move-object v1, v0

    .line 837
    :goto_0
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;-><init>()V

    goto :goto_1

    :cond_1
    move-object v1, v0

    move-object v2, v0

    goto :goto_0
.end method

.method public static final getAppInfo(Landroid/content/Intent;)Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;
    .locals 1

    .prologue
    .line 816
    if-nez p0, :cond_0

    .line 817
    const/4 v0, 0x0

    .line 819
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.samsung.android.sdk.professionalaudio.appinfo"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    goto :goto_0
.end method

.method private getCategory(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 226
    const-string v0, "instrument"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    const/4 v0, 0x1

    .line 231
    :goto_0
    return v0

    .line 228
    :cond_0
    const-string v0, "effect"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 229
    const/4 v0, 0x2

    goto :goto_0

    .line 230
    :cond_1
    const-string v0, "utility"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 231
    const/4 v0, 0x3

    goto :goto_0

    .line 233
    :cond_2
    new-instance v0, Lorg/apache/http/impl/cookie/DateParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Infomation about "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 234
    const-string v2, " was not prepared correctly"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 233
    invoke-direct {v0, v1}, Lorg/apache/http/impl/cookie/DateParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static final getState(Landroid/content/Intent;)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 857
    const-string v1, "com.samsung.android.sdk.professionalaudio.processname"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 858
    const-string v1, "com.samsung.android.sdk.professionalaudio.processname"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 860
    :cond_0
    return v0
.end method

.method public static getVersionCode(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 244
    const/4 v0, 0x7

    return v0
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 695
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    .line 696
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 698
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.packagename"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    .line 699
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.vendorname"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mVendorName:Ljava/lang/String;

    .line 700
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    .line 701
    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.productgroupname"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 700
    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mProductGroup:Ljava/lang/String;

    .line 702
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.appname"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mApplicationName:Ljava/lang/String;

    .line 703
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.version"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mVersionName:Ljava/lang/String;

    .line 704
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.category"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mCategory:I

    .line 705
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    .line 706
    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.multiinstanceenabled"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 705
    iput-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMultiInstanceEnabled:Z

    .line 707
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    .line 708
    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.backgroundservice"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 707
    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mBackgroundService:Ljava/lang/String;

    .line 710
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.instanceid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mInstanceId:Ljava/lang/String;

    .line 711
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.configuration"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mConfiguration:Landroid/os/Bundle;

    .line 713
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.ports"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    .line 715
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.midiinportcount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 716
    const-string v0, "professionalaudioconnection:library:j:SapaAppInfo "

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.midiinportcount not included in parcel."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.midioutportcount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 719
    const-string v0, "professionalaudioconnection:library:j:SapaAppInfo "

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.midioutportcount not included in parcel."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.audioinportcount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 722
    const-string v0, "professionalaudioconnection:library:j:SapaAppInfo "

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.audioinportcount not included in parcel."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.audiooutportcount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 725
    const-string v0, "professionalaudioconnection:library:j:SapaAppInfo "

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.audiooutportcount not included in parcel."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 727
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    .line 728
    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.midiinportcount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 727
    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiInputPortCount:I

    .line 729
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    .line 730
    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.midioutportcount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 729
    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiOutputPortCount:I

    .line 731
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    .line 732
    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.audioinportcount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 731
    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioInputPortCount:I

    .line 733
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    .line 734
    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.audiooutportcount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 733
    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioOutputPortCount:I

    .line 736
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    .line 737
    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.actions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    .line 739
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v2, "com.samsung.android.sdk.professionalaudio.sapaappinfo.description"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mDescription:Ljava/lang/String;

    .line 740
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v2, "com.samsung.android.sdk.professionalaudio.sapaappinfo.metadata"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMetaData:Landroid/os/Bundle;

    .line 742
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->setActions(Landroid/util/SparseArray;)V

    .line 744
    return-void
.end method

.method public static final setInstanceId(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 783
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 784
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.instanceid"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 785
    const-string v0, "com.samsung.android.sdk.professionalaudio.sapaappinfo.packagename"

    invoke-virtual {p0, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 787
    :cond_0
    return-void
.end method

.method private setMarkers()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 345
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->clearMarkers()V

    .line 346
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    .line 362
    return-void

    .line 347
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 348
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getInOutType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 349
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getSignalType()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 350
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioInputPortCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioInputPortCount:I

    .line 346
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 352
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiInputPortCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiInputPortCount:I

    goto :goto_1

    .line 355
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getSignalType()I

    move-result v0

    if-ne v0, v4, :cond_3

    .line 356
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioOutputPortCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioOutputPortCount:I

    goto :goto_1

    .line 358
    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiOutputPortCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiOutputPortCount:I

    goto :goto_1
.end method

.method public static final setSapaAppInfo(Landroid/content/Intent;Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;)V
    .locals 1

    .prologue
    .line 769
    const-string v0, "com.samsung.android.sdk.professionalaudio.appinfo"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 770
    return-void
.end method

.method public static final setState(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 800
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 801
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 802
    :cond_0
    const-string v0, "com.samsung.android.sdk.professionalaudio.processname"

    invoke-virtual {p0, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 804
    :cond_1
    return-void
.end method


# virtual methods
.method public final addPort(Lcom/samsung/android/sdk/professionalaudio/SapaPort;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 379
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->clearMarkers()V

    .line 382
    :cond_0
    if-eqz p1, :cond_1

    .line 383
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getInOutType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 385
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getSignalType()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 386
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioInputPortCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioInputPortCount:I

    .line 398
    :cond_1
    :goto_0
    return-void

    .line 388
    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiInputPortCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiInputPortCount:I

    goto :goto_0

    .line 391
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getSignalType()I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 392
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioOutputPortCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioOutputPortCount:I

    goto :goto_0

    .line 394
    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiOutputPortCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiOutputPortCount:I

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 652
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 905
    if-ne p0, p1, :cond_1

    move v2, v3

    .line 968
    :cond_0
    :goto_0
    return v2

    .line 908
    :cond_1
    if-eqz p1, :cond_0

    .line 911
    instance-of v0, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    if-eqz v0, :cond_0

    .line 914
    check-cast p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;

    .line 915
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    if-nez v0, :cond_8

    .line 916
    iget-object v0, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    .line 930
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mVersionName:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 931
    iget-object v0, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mVersionName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 937
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mConfiguration:Landroid/os/Bundle;

    if-nez v0, :cond_a

    .line 938
    iget-object v0, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mConfiguration:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 944
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mInstanceId:Ljava/lang/String;

    if-nez v0, :cond_b

    .line 945
    iget-object v0, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mInstanceId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 951
    :cond_5
    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMultiInstanceEnabled:Z

    iget-boolean v1, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMultiInstanceEnabled:Z

    if-ne v0, v1, :cond_0

    .line 954
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    if-nez v0, :cond_c

    .line 955
    iget-object v0, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 961
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    if-nez v0, :cond_d

    .line 962
    iget-object v0, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    :cond_7
    move v2, v3

    .line 968
    goto :goto_0

    .line 919
    :cond_8
    iget-object v0, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    .line 921
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    iget-object v1, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    move v1, v2

    .line 924
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 925
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;

    iget-object v4, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 924
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 934
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mVersionName:Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mVersionName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    .line 941
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mConfiguration:Landroid/os/Bundle;

    iget-object v1, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mConfiguration:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    goto/16 :goto_0

    .line 948
    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mInstanceId:Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mInstanceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    goto/16 :goto_0

    .line 958
    :cond_c
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    goto/16 :goto_0

    .line 965
    :cond_d
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    goto/16 :goto_0
.end method

.method public final getActionInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;
    .locals 3

    .prologue
    .line 546
    .line 547
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 548
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    .line 555
    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0

    .line 549
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;

    .line 550
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaActionInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 548
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final getActions()Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    return-object v0
.end method

.method public final getApp()Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;
    .locals 3

    .prologue
    .line 596
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mInstanceId:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/professionalaudio/app/SapaApp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public getAudioInputPortCount()I
    .locals 1

    .prologue
    .line 998
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioInputPortCount:I

    return v0
.end method

.method public getAudioOutputPortCount()I
    .locals 1

    .prologue
    .line 1008
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioOutputPortCount:I

    return v0
.end method

.method public final getBackgroundServiceAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 870
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mBackgroundService:Ljava/lang/String;

    return-object v0
.end method

.method public getCallerPackageName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMetaData:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMetaData:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.key.callerpackagename"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 319
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCategory()I
    .locals 1

    .prologue
    .line 643
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mCategory:I

    return v0
.end method

.method public final getConfiguration()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 633
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mConfiguration:Landroid/os/Bundle;

    return-object v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mDescription:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getIcon(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 480
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 481
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 482
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 484
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getLogo(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 530
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 531
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLogo(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 533
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getMetaData()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMetaData:Landroid/os/Bundle;

    return-object v0
.end method

.method public getMidiInputPortCount()I
    .locals 1

    .prologue
    .line 978
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiInputPortCount:I

    return v0
.end method

.method public getMidiOutputPortCount()I
    .locals 1

    .prologue
    .line 988
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiOutputPortCount:I

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mApplicationName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPortByName(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/SapaPort;
    .locals 3

    .prologue
    .line 266
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 271
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 266
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 267
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0
.end method

.method public final getPorts()Landroid/util/SparseArray;
    .locals 4

    .prologue
    .line 438
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    .line 439
    const/4 v0, 0x0

    .line 440
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 444
    return-object v2

    .line 440
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 441
    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 442
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final getProductGroupName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mProductGroup:Ljava/lang/String;

    return-object v0
.end method

.method public final getVendorName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mVendorName:Ljava/lang/String;

    return-object v0
.end method

.method public final getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mVersionName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 879
    .line 882
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    move v0, v1

    .line 881
    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 883
    mul-int/lit8 v2, v0, 0x1f

    .line 884
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mVersionName:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    .line 883
    :goto_1
    add-int/2addr v0, v2

    .line 885
    mul-int/lit8 v2, v0, 0x1f

    .line 887
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mApplicationName:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    .line 885
    :goto_2
    add-int/2addr v0, v2

    .line 888
    mul-int/lit8 v2, v0, 0x1f

    .line 889
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mConfiguration:Landroid/os/Bundle;

    if-nez v0, :cond_3

    move v0, v1

    .line 888
    :goto_3
    add-int/2addr v0, v2

    .line 890
    mul-int/lit8 v2, v0, 0x1f

    .line 891
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mInstanceId:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    .line 890
    :goto_4
    add-int/2addr v0, v2

    .line 892
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMultiInstanceEnabled:Z

    if-eqz v0, :cond_5

    const/16 v0, 0x4cf

    :goto_5
    add-int/2addr v0, v2

    .line 893
    mul-int/lit8 v2, v0, 0x1f

    .line 894
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    .line 893
    :goto_6
    add-int/2addr v0, v2

    .line 895
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    if-nez v2, :cond_7

    :goto_7
    add-int/2addr v0, v1

    .line 896
    return v0

    .line 882
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    .line 884
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mVersionName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 887
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mApplicationName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 889
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mConfiguration:Landroid/os/Bundle;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_3

    .line 891
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mInstanceId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 892
    :cond_5
    const/16 v0, 0x4d5

    goto :goto_5

    .line 894
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 895
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    goto :goto_7
.end method

.method public final isMultiInstanceEnabled()Z
    .locals 1

    .prologue
    .line 607
    iget-boolean v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMultiInstanceEnabled:Z

    return v0
.end method

.method putCallerPackageName(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMetaData:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMetaData:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.key.callerpackagename"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    :cond_0
    return-void
.end method

.method public removeCallerPackageName()V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMetaData:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMetaData:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.key.callerpackagename"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 327
    :cond_0
    return-void
.end method

.method public final removePort(Lcom/samsung/android/sdk/professionalaudio/SapaPort;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 408
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 409
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->clearMarkers()V

    .line 429
    :cond_0
    :goto_0
    return-void

    .line 412
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 414
    if-eqz v0, :cond_0

    .line 415
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getInOutType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 416
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getSignalType()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 417
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioInputPortCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioInputPortCount:I

    goto :goto_0

    .line 419
    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiInputPortCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiInputPortCount:I

    goto :goto_0

    .line 422
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getSignalType()I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 423
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioOutputPortCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioOutputPortCount:I

    goto :goto_0

    .line 425
    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiOutputPortCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiOutputPortCount:I

    goto :goto_0
.end method

.method public final setActions(Landroid/util/SparseArray;)V
    .locals 1

    .prologue
    .line 578
    if-eqz p1, :cond_0

    .line 579
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    .line 587
    :goto_0
    return-void

    .line 581
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    if-nez v0, :cond_1

    .line 582
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    goto :goto_0

    .line 584
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mActions:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    goto :goto_0
.end method

.method public final setConfiguration(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 621
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mConfiguration:Landroid/os/Bundle;

    .line 622
    return-void
.end method

.method public final setInstanceId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 755
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mInstanceId:Ljava/lang/String;

    .line 756
    return-void
.end method

.method public final setPortFromSapaProcessor(Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;)V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 338
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getPorts()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getPorts()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 341
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->setMarkers()V

    .line 342
    return-void
.end method

.method public final setPorts(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 293
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 294
    invoke-direct {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->setMarkers()V

    .line 295
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 661
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.packagename"

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.vendorname"

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mVendorName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.productgroupname"

    .line 664
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mProductGroup:Ljava/lang/String;

    .line 663
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.appname"

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mApplicationName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.version"

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mVersionName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.category"

    iget v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mCategory:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 668
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.multiinstanceenabled"

    .line 669
    iget-boolean v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMultiInstanceEnabled:Z

    .line 668
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 670
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.backgroundservice"

    .line 671
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mBackgroundService:Ljava/lang/String;

    .line 670
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.instanceid"

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mInstanceId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.ports"

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mPorts:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 675
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.configuration"

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mConfiguration:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 677
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.actions"

    .line 678
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->getActions()Landroid/util/SparseArray;

    move-result-object v2

    .line 677
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 680
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.midiinportcount"

    .line 681
    iget v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiInputPortCount:I

    .line 680
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 682
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.midioutportcount"

    .line 683
    iget v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMidiOutputPortCount:I

    .line 682
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 684
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.audioinportcount"

    iget v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioInputPortCount:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 685
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.audiooutportcount"

    .line 686
    iget v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mAudioOutputPortCount:I

    .line 685
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 687
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.description"

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mDescription:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    const-string v1, "com.samsung.android.sdk.professionalaudio.sapaappinfo.metadata"

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mMetaData:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 690
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/app/SapaAppInfo;->mParcelInfo:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 691
    return-void
.end method
