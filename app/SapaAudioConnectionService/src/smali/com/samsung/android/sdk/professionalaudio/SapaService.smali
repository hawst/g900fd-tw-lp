.class public final Lcom/samsung/android/sdk/professionalaudio/SapaService;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final START_PARAM_DEFAULT_LATENCY:I = 0x0

.field public static final START_PARAM_EXCEPT_CAPTURE:I = 0x20

.field public static final START_PARAM_HIGH_LATENCY:I = 0x4

.field static final START_PARAM_JACK_USE_USB:I = 0x40

.field public static final START_PARAM_LOW_LATENCY:I = 0x1

.field public static final START_PARAM_MID_LATENCY:I = 0x2

.field public static final START_PARAM_USE_DUMMY:I = 0x10

.field static mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    sget-boolean v0, Lcom/samsung/android/sdk/professionalaudio/Sapa;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 182
    new-instance v0, Ljava/lang/InstantiationException;

    const-string v1, "Sapa is NOT initialized - Need to call Sapa.initialize()"

    invoke-direct {v0, v1}, Ljava/lang/InstantiationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getInstance()Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    .line 185
    return-void
.end method


# virtual methods
.method public connect(Lcom/samsung/android/sdk/professionalaudio/SapaPort;Lcom/samsung/android/sdk/professionalaudio/SapaPort;)V
    .locals 4

    .prologue
    .line 584
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    new-instance v1, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->connect(Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;)V

    .line 585
    return-void
.end method

.method public connect(Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;)V
    .locals 1

    .prologue
    .line 572
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->connect(Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;)V

    .line 573
    return-void
.end method

.method public disconnect(Lcom/samsung/android/sdk/professionalaudio/SapaPort;Lcom/samsung/android/sdk/professionalaudio/SapaPort;)V
    .locals 4

    .prologue
    .line 711
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    new-instance v1, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->disconnect(Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;)V

    .line 712
    return-void
.end method

.method public disconnect(Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;)V
    .locals 1

    .prologue
    .line 699
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->disconnect(Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;)V

    .line 700
    return-void
.end method

.method public disconnectAllConnection()V
    .locals 1

    .prologue
    .line 689
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->disconnectAllConnection()V

    .line 690
    return-void
.end method

.method public getAllConnection()Ljava/util/List;
    .locals 1

    .prologue
    .line 720
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getAllConnection()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAllPlugin()Ljava/util/List;
    .locals 1

    .prologue
    .line 556
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getAllPlugin()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAllPort()Ljava/util/List;
    .locals 1

    .prologue
    .line 729
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getAllPort()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLoopbackPort(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/SapaPort;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 651
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 659
    :goto_0
    return-object v0

    .line 654
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getAllPort()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v1

    .line 659
    goto :goto_0

    .line 654
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 655
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "loopback:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0
.end method

.method public getLoopbackPorts()Ljava/util/List;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 669
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 681
    :goto_0
    return-object v0

    .line 672
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 673
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getAllPort()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 678
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 679
    goto :goto_0

    .line 673
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 674
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "loopback:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 675
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 681
    goto :goto_0
.end method

.method public getParameters()Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;
    .locals 1

    .prologue
    .line 416
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getParameters()Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;

    move-result-object v0

    return-object v0
.end method

.method public getSettings()Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;
    .locals 1

    .prologue
    .line 421
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getSettings()Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

    move-result-object v0

    return-object v0
.end method

.method public getSystemPort(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/SapaPort;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 596
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 613
    :cond_0
    :goto_0
    return-object v0

    .line 599
    :cond_1
    if-nez p1, :cond_2

    move-object v0, v1

    .line 600
    goto :goto_0

    .line 603
    :cond_2
    const-string v0, "__system_capture_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 604
    const-string v0, "__system_playback_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move-object v0, v1

    .line 605
    goto :goto_0

    .line 607
    :cond_4
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getAllPort()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6

    move-object v0, v1

    .line 613
    goto :goto_0

    .line 607
    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 608
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "in:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 609
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "out:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    goto :goto_0
.end method

.method public getSystemPorts()Ljava/util/List;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 623
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 639
    :goto_0
    return-object v0

    .line 626
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 627
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getAllPort()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 636
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 637
    goto :goto_0

    .line 627
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 628
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "in:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 629
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "out:"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 630
    :cond_3
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "in:__system_capture_"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 631
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "out:__system_playback_"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 632
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move-object v0, v2

    .line 639
    goto :goto_0
.end method

.method public isAudioUSBDeviceAttached()Z
    .locals 1

    .prologue
    .line 840
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-nez v0, :cond_0

    .line 841
    const/4 v0, 0x0

    .line 843
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->isAudioUSBDeviceAttached()Z

    move-result v0

    goto :goto_0
.end method

.method public isAvailablePort(Lcom/samsung/android/sdk/professionalaudio/SapaPort;)Z
    .locals 2

    .prologue
    .line 741
    if-nez p1, :cond_0

    .line 742
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "argument port is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 745
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getAllPort()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 750
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 745
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 746
    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 747
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 251
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->isStarted()Z

    move-result v0

    return v0
.end method

.method public register(Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;)V
    .locals 1

    .prologue
    .line 534
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->register(Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;)V

    .line 535
    return-void
.end method

.method public setAudioUSBDeviceAttached(Z)Z
    .locals 1

    .prologue
    .line 872
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-nez v0, :cond_0

    .line 873
    const/4 v0, 0x0

    .line 875
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->setAudioUSBDeviceAttached(Z)Z

    move-result v0

    goto :goto_0
.end method

.method public setGlobalLatencySettingValueChanged(I)V
    .locals 1

    .prologue
    .line 826
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-eqz v0, :cond_0

    .line 827
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->setGlobalLatencySettingValueChanged(I)V

    .line 829
    :cond_0
    return-void
.end method

.method public setGlobalUSBSettingValueChanged(I)V
    .locals 1

    .prologue
    .line 833
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-eqz v0, :cond_0

    .line 834
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->setGlobalUSBSettingValueChanged(I)V

    .line 836
    :cond_0
    return-void
.end method

.method public setPowerSavingEnabled(Z)Z
    .locals 1

    .prologue
    .line 808
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-nez v0, :cond_0

    .line 809
    const/4 v0, 0x0

    .line 812
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->setPowerSavingEnabled(Z)Z

    move-result v0

    goto :goto_0
.end method

.method public setSettings(Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;)V
    .locals 1

    .prologue
    .line 426
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->setSettings(Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;)V

    .line 427
    return-void
.end method

.method public setTransportFrame(I)V
    .locals 1

    .prologue
    .line 759
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->setTransportFrame(I)V

    .line 760
    return-void
.end method

.method public setTransportPosition(III)V
    .locals 1

    .prologue
    .line 771
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->setTransportPosition(III)V

    .line 772
    return-void
.end method

.method public setTransportTimeout(D)V
    .locals 1

    .prologue
    .line 798
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->setTransportTimeout(D)V

    .line 799
    return-void
.end method

.method public setUseUSBEnabled(Z)Z
    .locals 1

    .prologue
    .line 817
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-nez v0, :cond_0

    .line 818
    const/4 v0, 0x0

    .line 821
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->setUseUSBEnabled(Z)Z

    move-result v0

    goto :goto_0
.end method

.method public start(I)V
    .locals 4

    .prologue
    .line 463
    and-int/lit8 v0, p1, 0x77

    if-eq v0, p1, :cond_0

    .line 470
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid argument"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 472
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->getSettings()Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

    move-result-object v1

    .line 473
    if-eqz v1, :cond_3

    and-int/lit8 v0, p1, 0x7

    if-nez v0, :cond_3

    .line 474
    invoke-virtual {v1}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;->getDefaultLatency()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 480
    or-int/lit8 p1, p1, 0x2

    move v0, p1

    .line 489
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;->getUSBControl()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->isAudioUSBDeviceAttached()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 490
    or-int/lit8 v0, v0, 0x40

    .line 493
    :cond_1
    sget-object v2, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->start(I)V

    .line 496
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;->getUSBControl()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/SapaService;->isAudioUSBDeviceAttached()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 498
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/Sapa;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 500
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 501
    const-string v1, "com.samsung.android.sdk.professionalaudio.action.ASK_CONTROL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 502
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/Sapa;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 510
    :cond_2
    :goto_1
    return-void

    .line 476
    :pswitch_0
    or-int/lit8 p1, p1, 0x4

    move v0, p1

    .line 477
    goto :goto_0

    .line 483
    :pswitch_1
    or-int/lit8 p1, p1, 0x1

    move v0, p1

    goto :goto_0

    .line 505
    :catch_0
    move-exception v0

    goto :goto_1

    .line 503
    :catch_1
    move-exception v0

    goto :goto_1

    :cond_3
    move v0, p1

    goto :goto_0

    .line 474
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public startTransport()V
    .locals 1

    .prologue
    .line 779
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->startTransport()V

    .line 780
    return-void
.end method

.method public stop(Z)V
    .locals 1

    .prologue
    .line 520
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->stop(Z)V

    .line 521
    return-void
.end method

.method public stopTransport()V
    .locals 1

    .prologue
    .line 787
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->stopTransport()V

    .line 788
    return-void
.end method

.method public unregister(Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;)V
    .locals 1

    .prologue
    .line 543
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaService;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->unregister(Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;)V

    .line 544
    return-void
.end method
