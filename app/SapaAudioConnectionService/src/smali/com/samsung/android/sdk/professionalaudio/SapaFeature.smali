.class Lcom/samsung/android/sdk/professionalaudio/SapaFeature;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final ns:Ljava/lang/String;


# instance fields
.field private mList:Ljava/util/Vector;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;->ns:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;->initialize()V

    .line 38
    return-void
.end method

.method private getIndex(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 120
    const-string v0, "support_low_latency"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 121
    const/4 v0, 0x1

    .line 140
    :goto_0
    return v0

    .line 122
    :cond_0
    const-string v0, "support_mid_latency"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 123
    const/4 v0, 0x2

    goto :goto_0

    .line 124
    :cond_1
    const-string v0, "support_high_latency"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 125
    const/4 v0, 0x3

    goto :goto_0

    .line 126
    :cond_2
    const-string v0, "support_capture_device"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 127
    const/4 v0, 0x4

    goto :goto_0

    .line 128
    :cond_3
    const-string v0, "support_dummy_mode"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 129
    const/4 v0, 0x5

    goto :goto_0

    .line 130
    :cond_4
    const-string v0, "support_playback_device"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 131
    const/4 v0, 0x6

    goto :goto_0

    .line 132
    :cond_5
    const-string v0, "support_boost_mode"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    .line 133
    const/4 v0, 0x7

    goto :goto_0

    .line 134
    :cond_6
    const-string v0, "support_samplerate_48000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    .line 135
    const/16 v0, 0x8

    goto :goto_0

    .line 136
    :cond_7
    const-string v0, "support_samplerate_44100"

    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_8

    .line 137
    const/16 v0, 0x9

    goto :goto_0

    .line 140
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private parse(Ljava/io/InputStream;)Ljava/util/Vector;
    .locals 3

    .prologue
    .line 81
    :try_start_0
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 82
    const-string v1, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->setFeature(Ljava/lang/String;Z)V

    .line 83
    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 84
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    .line 85
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;->readFeed(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/Vector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 87
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 85
    return-object v0

    .line 86
    :catchall_0
    move-exception v0

    .line 87
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    .line 88
    throw v0
.end method

.method private readFeature(Lorg/xmlpull/v1/XmlPullParser;)Lcom/samsung/android/sdk/professionalaudio/SapaFeature$Feature;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 156
    const-string v1, ""

    .line 157
    const/4 v0, 0x2

    sget-object v2, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;->ns:Ljava/lang/String;

    const-string v3, "feature"

    invoke-interface {p1, v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 159
    const-string v0, ""

    .line 160
    const-string v3, "feature"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 161
    const-string v0, "name"

    invoke-interface {p1, v5, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 162
    const-string v1, "value"

    invoke-interface {p1, v5, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 163
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    .line 165
    :cond_0
    const/4 v2, 0x3

    sget-object v3, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;->ns:Ljava/lang/String;

    const-string v4, "feature"

    invoke-interface {p1, v2, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 166
    new-instance v2, Lcom/samsung/android/sdk/professionalaudio/SapaFeature$Feature;

    const-string v3, "true"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-direct {v2, v0, v1, v5}, Lcom/samsung/android/sdk/professionalaudio/SapaFeature$Feature;-><init>(Ljava/lang/String;ZLcom/samsung/android/sdk/professionalaudio/SapaFeature$Feature;)V

    return-object v2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private readFeed(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/Vector;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 92
    new-instance v0, Ljava/util/Vector;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    .line 93
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/util/Vector;->setSize(I)V

    .line 94
    sget-object v1, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;->ns:Ljava/lang/String;

    const-string v2, "feed"

    invoke-interface {p1, v4, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 95
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 116
    return-object v0

    .line 96
    :cond_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 99
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 101
    if-eqz v1, :cond_2

    const-string v2, "feature"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 102
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;->readFeature(Lorg/xmlpull/v1/XmlPullParser;)Lcom/samsung/android/sdk/professionalaudio/SapaFeature$Feature;

    move-result-object v1

    .line 103
    iget-object v2, v1, Lcom/samsung/android/sdk/professionalaudio/SapaFeature$Feature;->name:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;->getIndex(Ljava/lang/String;)I

    move-result v2

    .line 105
    :try_start_0
    invoke-virtual {v0, v2, v1}, Ljava/util/Vector;->add(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 106
    :catch_0
    move-exception v3

    .line 107
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    if-le v2, v3, :cond_0

    .line 108
    invoke-virtual {v0, v2}, Ljava/util/Vector;->setSize(I)V

    .line 109
    invoke-virtual {v0, v2, v1}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 113
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0
.end method

.method private skip(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 2

    .prologue
    .line 172
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 173
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 175
    :cond_0
    const/4 v0, 0x1

    .line 176
    :goto_0
    if-nez v0, :cond_1

    .line 186
    return-void

    .line 177
    :cond_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 182
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 179
    :pswitch_1
    add-int/lit8 v0, v0, -0x1

    .line 180
    goto :goto_0

    .line 177
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method initialize()V
    .locals 4

    .prologue
    .line 54
    new-instance v0, Ljava/io/File;

    const-string v1, "/etc/sapa_feature.xml"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 55
    const/4 v2, 0x0

    .line 57
    :try_start_0
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :try_start_1
    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;->parse(Ljava/io/InputStream;)Ljava/util/Vector;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;->mList:Ljava/util/Vector;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 66
    if-eqz v1, :cond_0

    .line 68
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 59
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 60
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 66
    if-eqz v1, :cond_0

    .line 68
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 69
    :catch_1
    move-exception v0

    .line 70
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 61
    :catch_2
    move-exception v0

    .line 62
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 66
    if-eqz v2, :cond_0

    .line 68
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 69
    :catch_3
    move-exception v0

    .line 70
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 63
    :catch_4
    move-exception v0

    .line 64
    :goto_3
    :try_start_7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 66
    if-eqz v2, :cond_0

    .line 68
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_0

    .line 69
    :catch_5
    move-exception v0

    .line 70
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 65
    :catchall_0
    move-exception v0

    .line 66
    :goto_4
    if-eqz v2, :cond_1

    .line 68
    :try_start_9
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 73
    :cond_1
    :goto_5
    throw v0

    .line 69
    :catch_6
    move-exception v1

    .line 70
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 69
    :catch_7
    move-exception v0

    .line 70
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 65
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_4

    .line 63
    :catch_8
    move-exception v0

    move-object v2, v1

    goto :goto_3

    .line 61
    :catch_9
    move-exception v0

    move-object v2, v1

    goto :goto_2

    .line 59
    :catch_a
    move-exception v0

    goto :goto_1
.end method

.method isFeatureEnabled(I)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;->mList:Ljava/util/Vector;

    if-nez v0, :cond_0

    move v0, v1

    .line 50
    :goto_0
    return v0

    .line 44
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaFeature;->mList:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaFeature$Feature;

    .line 45
    if-eqz v0, :cond_1

    .line 46
    iget-boolean v0, v0, Lcom/samsung/android/sdk/professionalaudio/SapaFeature$Feature;->value:Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 47
    :catch_0
    move-exception v0

    move v0, v1

    .line 48
    goto :goto_0

    :cond_1
    move v0, v1

    .line 50
    goto :goto_0
.end method
