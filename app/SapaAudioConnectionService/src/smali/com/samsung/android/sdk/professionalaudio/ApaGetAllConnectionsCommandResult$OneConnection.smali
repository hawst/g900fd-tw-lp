.class public final Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult$OneConnection;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private mInputPortName:Ljava/lang/String;

.field private mOutputPortName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult$OneConnection;->mInputPortName:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult$OneConnection;->mOutputPortName:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method public getInput()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult$OneConnection;->mInputPortName:Ljava/lang/String;

    return-object v0
.end method

.method public getOutput()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult$OneConnection;->mOutputPortName:Ljava/lang/String;

    return-object v0
.end method
