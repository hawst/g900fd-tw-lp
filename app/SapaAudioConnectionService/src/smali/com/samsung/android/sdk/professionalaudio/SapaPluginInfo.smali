.class public final Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field mModuleFileName:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field mSetupArguments:Ljava/lang/String;

.field private mVersion:I

.field private mVersionName:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;->mName:Ljava/lang/String;

    .line 71
    iput p2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;->mVersion:I

    .line 72
    iput-object p3, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;->mVersionName:Ljava/lang/String;

    .line 73
    iput-object p4, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;->mPackageName:Ljava/lang/String;

    .line 74
    iput-object p5, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;->mModuleFileName:Ljava/lang/String;

    .line 75
    iput-object p6, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;->mSetupArguments:Ljava/lang/String;

    .line 77
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionCode()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;->mVersion:I

    return v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPluginInfo;->mVersionName:Ljava/lang/String;

    return-object v0
.end method
