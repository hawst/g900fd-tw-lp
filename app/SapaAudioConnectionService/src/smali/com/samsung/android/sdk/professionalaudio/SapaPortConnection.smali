.class public final Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final API_LEVEL:I = 0x2

.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private mDestinationPort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

.field private mSourcePort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->$assertionsDisabled:Z

    .line 191
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 202
    return-void

    .line 20
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    const-class v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 181
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mSourcePort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 182
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mDestinationPort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 183
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;)V
    .locals 0

    .prologue
    .line 177
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/professionalaudio/SapaPort;Lcom/samsung/android/sdk/professionalaudio/SapaPort;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mSourcePort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 50
    iput-object p2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mDestinationPort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1, v2}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;-><init>(Ljava/lang/String;II)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mSourcePort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 37
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    invoke-direct {v0, p2, v2, v2}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;-><init>(Ljava/lang/String;II)V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mDestinationPort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 39
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 153
    const/16 v0, 0x271a

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 111
    if-ne p1, p0, :cond_1

    .line 123
    :cond_0
    :goto_0
    return v0

    .line 114
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 115
    goto :goto_0

    .line 118
    :cond_3
    instance-of v2, p1, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;

    if-nez v2, :cond_4

    move v0, v1

    .line 119
    goto :goto_0

    .line 122
    :cond_4
    check-cast p1, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;

    .line 123
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mSourcePort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mSourcePort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mSourcePort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getSourcePort()Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 124
    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mDestinationPort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mDestinationPort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mDestinationPort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v2

    .line 125
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getDestinationPort()Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_5
    move v0, v1

    .line 123
    goto :goto_0
.end method

.method public getDestinationPort()Lcom/samsung/android/sdk/professionalaudio/SapaPort;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mDestinationPort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    return-object v0
.end method

.method public getDestinationPortName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mDestinationPort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mDestinationPort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSourcePort()Lcom/samsung/android/sdk/professionalaudio/SapaPort;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mSourcePort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    return-object v0
.end method

.method public getSourcePortName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mSourcePort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mSourcePort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 138
    sget-boolean v0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "hashCode not designed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 139
    :cond_0
    const/16 v0, 0x2a

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 169
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mSourcePort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 170
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->mDestinationPort:Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 171
    return-void
.end method
