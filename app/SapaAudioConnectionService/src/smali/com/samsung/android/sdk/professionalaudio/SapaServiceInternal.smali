.class final Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays",
        "UseValueOf"
    }
.end annotation


# static fields
.field static final API_LEVEL:I = 0x1

.field static final MAJOR_VERSION_NUMBER:I = 0x1

.field static final MINOR_VERSION_NUMBER:I = 0x2

.field static final PATCH_VERSION_NUMBER:I = 0x0

.field static final RETURN_ERROR:I = 0x1

.field static final RETURN_NOT_SUPPORT:I = 0x3

.field static final RETURN_OK:I = 0x0

.field static final RETURN_SUPPORT:I = 0x2

.field static final SDK_VERSION_NUMBER:I = 0x4b0

.field private static final TAG:Ljava/lang/String; = "SapaServiceFramework"

.field static final VERSION:Ljava/lang/String; = "ProfessionalAudio_v1.2.7"

.field private static volatile mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;


# instance fields
.field private mClient:Ljava/util/Map;

.field private mClientLock:Ljava/lang/Object;

.field private mClientMessageListener:Ljava/util/Map;

.field private mClientMessageListenerLock:Ljava/lang/Object;

.field private mClientStatusListener:Ljava/util/Map;

.field private mClientStatusListenerLock:Ljava/lang/Object;

.field private mEventHandler:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;

.field private mNativeContext:I


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientMessageListener:Ljava/util/Map;

    .line 122
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientMessageListenerLock:Ljava/lang/Object;

    .line 123
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClient:Ljava/util/Map;

    .line 124
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientLock:Ljava/lang/Object;

    .line 125
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientStatusListener:Ljava/util/Map;

    .line 126
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientStatusListenerLock:Ljava/lang/Object;

    .line 131
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 132
    new-instance v1, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;-><init>(Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mEventHandler:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;

    .line 139
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 134
    new-instance v1, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;-><init>(Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mEventHandler:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;

    goto :goto_0

    .line 136
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mEventHandler:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 666
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientMessageListenerLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 665
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientMessageListener:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 669
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientStatusListenerLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 668
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientStatusListener:Ljava/util/Map;

    return-object v0
.end method

.method private declared-synchronized execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;
    .locals 1

    .prologue
    .line 248
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/ApaCommand;->toJsonString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendControlCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 249
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/professionalaudio/ApaCommand;->newResult(Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 248
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static getApiLevel()I
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x1

    return v0
.end method

.method static declared-synchronized getInstance()Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;
    .locals 3

    .prologue
    .line 89
    const-class v1, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    .line 92
    :cond_0
    const-string v0, "SapaServiceFramework"

    const-string v2, "ProfessionalAudio_v1.2.7"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    sget-object v0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mInstance:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    const-string v0, "ProfessionalAudio_v1.2.7"

    return-object v0
.end method

.method static isAPILevelAllowed()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 97
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_isAPILevelAllowed(I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    const-string v0, "SapaServiceFramework"

    const-string v1, "Your SapaService API is NOT compatible with TARGET. Pelease check the SDK version"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    const/4 v0, 0x0

    .line 101
    :cond_0
    return v0
.end method

.method private static native native_attachService(Ljava/lang/String;)I
.end method

.method private static native native_deleteClient(Ljava/lang/String;)I
.end method

.method private static native native_detachService(Ljava/lang/String;Z)I
.end method

.method static native native_getJackClientName(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
.end method

.method static native native_getTargetAPILevel()I
.end method

.method private static native native_isAPILevelAllowed(I)I
.end method

.method static native native_isFeatureSupport(I)I
.end method

.method private static native native_isServiceAttached()I
.end method

.method private static native native_newClient(Ljava/lang/String;)I
.end method

.method private final native native_release(I)V
.end method

.method static native native_request(ILjava/lang/String;JJ)Ljava/nio/ByteBuffer;
.end method

.method static native native_sendCommand(ILjava/lang/String;)I
.end method

.method static native native_sendControlCommand(Ljava/lang/String;)Ljava/lang/String;
.end method

.method static native native_sendStream(ILjava/lang/String;Ljava/nio/ByteBuffer;I)I
.end method

.method private final native native_setup(Ljava/lang/Object;Ljava/lang/String;I)I
.end method

.method private static postDataEventFromNative(Ljava/lang/Object;IIILjava/nio/ByteBuffer;)V
    .locals 3

    .prologue
    .line 740
    if-nez p0, :cond_1

    .line 741
    const-string v0, "SapaServiceFramework"

    const-string v1, "SapaService postEventFromNative object reference is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    :cond_0
    :goto_0
    return-void

    .line 744
    :cond_1
    const/4 v1, 0x0

    .line 745
    instance-of v0, p0, Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    .line 746
    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 747
    instance-of v2, v0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-eqz v2, :cond_3

    .line 748
    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    .line 750
    :goto_1
    if-nez v0, :cond_2

    .line 751
    const-string v0, "SapaServiceFramework"

    const-string v1, "SapaService postEventFromNative object is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 755
    :cond_2
    iget-object v1, v0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mEventHandler:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;

    if-eqz v1, :cond_0

    .line 756
    invoke-virtual {p4}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 757
    invoke-virtual {v1, p4}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 758
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 759
    iget-object v2, v0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mEventHandler:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;

    invoke-virtual {v2, p1, p2, p3, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 760
    iget-object v0, v0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mEventHandler:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 3

    .prologue
    .line 769
    if-nez p0, :cond_1

    .line 770
    const-string v0, "SapaServiceFramework"

    const-string v1, "SapaService postEventFromNative object reference is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    :cond_0
    :goto_0
    return-void

    .line 773
    :cond_1
    const/4 v1, 0x0

    .line 774
    instance-of v0, p0, Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    .line 775
    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 776
    instance-of v2, v0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    if-eqz v2, :cond_3

    .line 777
    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;

    .line 779
    :goto_1
    if-nez v0, :cond_2

    .line 780
    const-string v0, "SapaServiceFramework"

    const-string v1, "SapaService postEventFromNative object is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 784
    :cond_2
    iget-object v1, v0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mEventHandler:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;

    if-eqz v1, :cond_0

    .line 785
    iget-object v1, v0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mEventHandler:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 786
    iget-object v0, v0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mEventHandler:Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public connect(Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;)V
    .locals 3

    .prologue
    .line 258
    if-eqz p1, :cond_0

    .line 259
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getSourcePortName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getDestinationPortName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 260
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getSourcePortName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getDestinationPortName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 261
    :cond_0
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Error during disconnect a port connection - invalid parameter"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 263
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaConnectCommand;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaConnectCommand;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getSourcePortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getDestinationPortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/professionalaudio/ApaConnectCommand;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/ApaConnectCommand;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;->result()Z

    move-result v0

    if-nez v0, :cond_2

    .line 264
    new-instance v0, Landroid/util/AndroidRuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error during connect between "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getSourcePortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getDestinationPortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 266
    :cond_2
    return-void
.end method

.method public disconnect(Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;)V
    .locals 3

    .prologue
    .line 281
    if-eqz p1, :cond_0

    .line 282
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getSourcePortName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getDestinationPortName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 283
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getSourcePortName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getDestinationPortName()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    :cond_0
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Error during disconnect a port connection - invalid parameter"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaDisconnectCommand;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaDisconnectCommand;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getSourcePortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getDestinationPortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/professionalaudio/ApaDisconnectCommand;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/professionalaudio/ApaDisconnectCommand;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;->result()Z

    move-result v0

    if-nez v0, :cond_2

    .line 287
    new-instance v0, Landroid/util/AndroidRuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error during disconnect between "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getSourcePortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;->getDestinationPortName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 289
    :cond_2
    return-void
.end method

.method public disconnectAllConnection()V
    .locals 1

    .prologue
    .line 272
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaDisconnectAllCommand;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaDisconnectAllCommand;-><init>()V

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    .line 273
    return-void
.end method

.method public getAllConnection()Ljava/util/List;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 296
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 297
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->getAllPort()Ljava/util/List;

    move-result-object v5

    .line 298
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommand;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommand;-><init>()V

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult;

    .line 299
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult;->result()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 300
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult;->getConnections()Ljava/util/List;

    move-result-object v0

    .line 301
    if-eqz v0, :cond_0

    .line 302
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 303
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 321
    :cond_0
    return-object v4

    .line 304
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult$OneConnection;

    .line 305
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult$OneConnection;->getInput()Ljava/lang/String;

    move-result-object v7

    .line 307
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetAllConnectionsCommandResult$OneConnection;->getOutput()Ljava/lang/String;

    move-result-object v8

    .line 309
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-object v1, v2

    move-object v3, v2

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    .line 317
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;

    invoke-direct {v0, v3, v1}, Lcom/samsung/android/sdk/professionalaudio/SapaPortConnection;-><init>(Lcom/samsung/android/sdk/professionalaudio/SapaPort;Lcom/samsung/android/sdk/professionalaudio/SapaPort;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 309
    :cond_3
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    .line 310
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4

    move-object v3, v0

    .line 313
    :cond_4
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;->getFullName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    move-object v1, v0

    .line 314
    goto :goto_1
.end method

.method getAllPlugin()Ljava/util/List;
    .locals 2

    .prologue
    .line 508
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/Sapa;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 509
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/PluginManager;

    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/Sapa;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/professionalaudio/PluginManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/PluginManager;->getPluginList()Ljava/util/List;

    move-result-object v0

    .line 511
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAllPort()Ljava/util/List;
    .locals 8

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 356
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 357
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommand;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommand;-><init>()V

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult;

    .line 358
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult;->result()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 359
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult;->getPorts()Ljava/util/List;

    move-result-object v0

    .line 360
    if-eqz v0, :cond_0

    .line 361
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 362
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 370
    :cond_0
    return-object v4

    .line 363
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;

    .line 364
    new-instance v6, Lcom/samsung/android/sdk/professionalaudio/SapaPort;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;->getPort()Ljava/lang/String;

    move-result-object v7

    .line 365
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;->isMidiPort()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    .line 366
    :goto_1
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetPortsCommandResult$OnePort;->isInputPort()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    :goto_2
    invoke-direct {v6, v7, v1, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaPort;-><init>(Ljava/lang/String;II)V

    .line 364
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move v1, v3

    .line 365
    goto :goto_1

    :cond_3
    move v0, v2

    .line 366
    goto :goto_2
.end method

.method public getParameters()Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 329
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 336
    :goto_0
    return-object v0

    .line 332
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaGetParametersCommand;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetParametersCommand;-><init>()V

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/ApaGetParametersCommandResult;

    .line 333
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetParametersCommandResult;->result()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 334
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetParametersCommandResult;->getParameters()Lcom/samsung/android/sdk/professionalaudio/SapaService$Parameters;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 336
    goto :goto_0
.end method

.method public getSettings()Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;
    .locals 2

    .prologue
    .line 340
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaGetSettingsCommand;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetSettingsCommand;-><init>()V

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/ApaGetSettingsCommandResult;

    .line 341
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetSettingsCommandResult;->result()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 342
    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaGetSettingsCommandResult;->getSettings()Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;

    move-result-object v0

    .line 344
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isAudioUSBDeviceAttached()Z
    .locals 1

    .prologue
    .line 830
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaIsAudioUsbDeviceAttachedCommand;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaIsAudioUsbDeviceAttachedCommand;-><init>()V

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;->result()Z

    move-result v0

    return v0
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 149
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_isServiceAttached()I

    move-result v0

    if-nez v0, :cond_0

    .line 150
    const/4 v0, 0x1

    .line 152
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method printRegisteredClientList()V
    .locals 5

    .prologue
    .line 493
    const-string v0, "SapaServiceFramework"

    const-string v1, "******** SapaProcessor list ********"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientLock:Ljava/lang/Object;

    monitor-enter v1

    .line 495
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClient:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 494
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 499
    const-string v0, "SapaServiceFramework"

    const-string v1, "************************************"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    return-void

    .line 495
    :cond_0
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 496
    const-string v3, "SapaServiceFramework"

    iget-object v4, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClient:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 494
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public register(Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;)V
    .locals 5

    .prologue
    .line 417
    if-nez p1, :cond_0

    .line 467
    :goto_0
    return-void

    .line 422
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->isStarted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 423
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->isJamMonitor()Z

    move-result v0

    if-nez v0, :cond_1

    .line 424
    new-instance v0, Ljava/lang/InstantiationException;

    const-string v1, "SapaService is not started yet"

    invoke-direct {v0, v1}, Ljava/lang/InstantiationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 429
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientLock:Ljava/lang/Object;

    monitor-enter v1

    .line 430
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClient:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 432
    monitor-exit v1

    goto :goto_0

    .line 429
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 435
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getName()Ljava/lang/String;

    move-result-object v2

    .line 436
    invoke-static {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_newClient(Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_c

    .line 438
    :try_start_2
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_setup(Ljava/lang/Object;Ljava/lang/String;I)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v0

    if-nez v0, :cond_b

    .line 440
    :try_start_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->incRef()I

    .line 441
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->init()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 442
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getStatusListener()Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 443
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getStatusListener()Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->setClientStatusListener(Ljava/lang/Integer;Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;)V

    .line 445
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClient:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 450
    :try_start_4
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClient:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 451
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_release(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 457
    :cond_4
    :try_start_5
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClient:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 458
    invoke-static {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_deleteClient(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_5

    .line 459
    const-string v0, "SapaServiceFramework"

    const-string v2, " fail to kill the processing module process"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    :cond_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 449
    :catchall_1
    move-exception v0

    .line 450
    :try_start_6
    iget-object v3, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClient:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 451
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_release(I)V

    .line 452
    :cond_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 456
    :catchall_2
    move-exception v0

    .line 457
    :try_start_7
    iget-object v3, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClient:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 458
    invoke-static {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_deleteClient(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_7

    .line 459
    const-string v2, "SapaServiceFramework"

    const-string v3, " fail to kill the processing module process"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    :cond_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 450
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClient:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 451
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_release(I)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 457
    :cond_9
    :try_start_9
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClient:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 458
    invoke-static {v2}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_deleteClient(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_a

    .line 459
    const-string v0, "SapaServiceFramework"

    const-string v2, " fail to kill the processing module process"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    :cond_a
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 454
    :cond_b
    :try_start_a
    new-instance v0, Ljava/lang/InstantiationException;

    const-string v3, "fail to connect with the processing module process"

    invoke-direct {v0, v3}, Ljava/lang/InstantiationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 464
    :cond_c
    :try_start_b
    new-instance v0, Ljava/lang/InstantiationException;

    const-string v2, "error in createClient : fail to fork new process"

    invoke-direct {v0, v2}, Ljava/lang/InstantiationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0
.end method

.method setAudioUSBDeviceAttached(Z)Z
    .locals 1

    .prologue
    .line 834
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaUpdateUsbDeviceInfoCommand;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaUpdateUsbDeviceInfoCommand;-><init>()V

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/ApaUpdateUsbDeviceInfoCommand;->setAttached(Z)Lcom/samsung/android/sdk/professionalaudio/ApaUpdateUsbDeviceInfoCommand;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;->result()Z

    move-result v0

    return v0
.end method

.method final setClientMessageListener(Ljava/lang/Integer;Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$MessageListener;)V
    .locals 2

    .prologue
    .line 679
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientMessageListenerLock:Ljava/lang/Object;

    monitor-enter v1

    .line 680
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientMessageListener:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 679
    monitor-exit v1

    .line 682
    return-void

    .line 679
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method setClientStatusListener(Ljava/lang/Integer;Lcom/samsung/android/sdk/professionalaudio/SapaProcessor$StatusListener;)V
    .locals 2

    .prologue
    .line 629
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientStatusListenerLock:Ljava/lang/Object;

    monitor-enter v1

    .line 630
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientStatusListener:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 629
    monitor-exit v1

    .line 633
    return-void

    .line 629
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method setGlobalLatencySettingValueChanged(I)V
    .locals 1

    .prologue
    .line 812
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 813
    const-string v0, "[{\"command\":\"global_latency_setting_value_changed_high\"}]"

    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendControlCommand(Ljava/lang/String;)Ljava/lang/String;

    .line 819
    :cond_0
    :goto_0
    return-void

    .line 814
    :cond_1
    if-nez p1, :cond_2

    .line 815
    const-string v0, "[{\"command\":\"global_latency_setting_value_changed_mid\"}]"

    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendControlCommand(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 816
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 817
    const-string v0, "[{\"command\":\"global_latency_setting_value_changed_low\"}]"

    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendControlCommand(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method setGlobalUSBSettingValueChanged(I)V
    .locals 1

    .prologue
    .line 822
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 823
    const-string v0, "[{\"command\":\"global_usb_setting_value_changed_android\"}]"

    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendControlCommand(Ljava/lang/String;)Ljava/lang/String;

    .line 827
    :cond_0
    :goto_0
    return-void

    .line 824
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 825
    const-string v0, "[{\"command\":\"global_usb_setting_value_changed_sapa\"}]"

    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendControlCommand(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public setPowerSavingEnabled(Z)Z
    .locals 1

    .prologue
    .line 791
    if-eqz p1, :cond_0

    .line 792
    const-string v0, "[{\"command\":\"pause_audio_path\"}]"

    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendControlCommand(Ljava/lang/String;)Ljava/lang/String;

    .line 797
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 794
    :cond_0
    const-string v0, "[{\"command\":\"resume_audio_path\"}]"

    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendControlCommand(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public setSettings(Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;)V
    .locals 1

    .prologue
    .line 348
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaSetSettingsCommand;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/ApaSetSettingsCommand;-><init>(Lcom/samsung/android/sdk/professionalaudio/SapaService$Settings;)V

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    .line 349
    return-void
.end method

.method public setTransportFrame(I)V
    .locals 1

    .prologue
    .line 378
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportFrameCommand;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportFrameCommand;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    .line 379
    return-void
.end method

.method public setTransportPosition(III)V
    .locals 1

    .prologue
    .line 386
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportPositionCommand;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportPositionCommand;-><init>()V

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportPositionCommand;->setBar(I)Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportPositionCommand;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportPositionCommand;->setBeat(I)Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportPositionCommand;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportPositionCommand;->setTick(I)Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportPositionCommand;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    .line 387
    return-void
.end method

.method public setTransportTimeout(D)V
    .locals 1

    .prologue
    .line 407
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportTimeoutCommand;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/sdk/professionalaudio/ApaSetTransportTimeoutCommand;-><init>(D)V

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    .line 408
    return-void
.end method

.method public setUseUSBEnabled(Z)Z
    .locals 1

    .prologue
    .line 802
    if-eqz p1, :cond_0

    .line 803
    const-string v0, "[{\"command\":\"use_usb_device\"}]"

    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendControlCommand(Ljava/lang/String;)Ljava/lang/String;

    .line 808
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 805
    :cond_0
    const-string v0, "[{\"command\":\"dont_use_usb_device\"}]"

    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_sendControlCommand(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public start(I)V
    .locals 2

    .prologue
    .line 179
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_attachService(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 180
    return-void

    .line 182
    :cond_0
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Fail to start jack daemon"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public startTransport()V
    .locals 1

    .prologue
    .line 393
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaStartTransportCommand;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaStartTransportCommand;-><init>()V

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    .line 394
    return-void
.end method

.method public stop(Z)V
    .locals 1

    .prologue
    .line 192
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/Sapa;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 193
    invoke-static {}, Lcom/samsung/android/sdk/professionalaudio/Sapa;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_detachService(Ljava/lang/String;Z)I

    .line 195
    :cond_0
    return-void
.end method

.method public stopTransport()V
    .locals 1

    .prologue
    .line 400
    new-instance v0, Lcom/samsung/android/sdk/professionalaudio/ApaStopTransportCommand;

    invoke-direct {v0}, Lcom/samsung/android/sdk/professionalaudio/ApaStopTransportCommand;-><init>()V

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->execute(Lcom/samsung/android/sdk/professionalaudio/ApaCommand;)Lcom/samsung/android/sdk/professionalaudio/ApaCommandResult;

    .line 401
    return-void
.end method

.method public unregister(Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;)V
    .locals 3

    .prologue
    .line 475
    if-nez p1, :cond_0

    .line 490
    :goto_0
    return-void

    .line 479
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClientLock:Ljava/lang/Object;

    monitor-enter v1

    .line 480
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClient:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 482
    iget-object v0, p0, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->mClient:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->decRef()I

    .line 484
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 485
    invoke-virtual {p1}, Lcom/samsung/android/sdk/professionalaudio/SapaProcessor;->getObjectId()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_release(I)V

    .line 486
    invoke-static {v0}, Lcom/samsung/android/sdk/professionalaudio/SapaServiceInternal;->native_deleteClient(Ljava/lang/String;)I

    .line 479
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
