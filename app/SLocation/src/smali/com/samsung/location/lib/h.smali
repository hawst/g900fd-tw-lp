.class public Lcom/samsung/location/lib/h;
.super Landroid/os/Handler;


# static fields
.field public static final A:I = 0xcd

.field public static final B:I = 0xce

.field public static final C:I = 0x12d

.field public static final D:I = 0x12e

.field public static final E:I = 0x130

.field public static final F:I = 0x131

.field public static final G:I = 0x191

.field public static final H:I = 0x1f5

.field public static final I:I = 0x1f7

.field private static R:Lcom/samsung/location/lib/h; = null

.field public static final a:I = 0x1

.field public static final b:I = 0x2

.field public static final c:I = 0x3

.field public static final d:I = 0x4

.field public static final e:I = 0x5

.field public static final f:I = 0x6

.field public static final g:I = 0x7

.field public static final h:I = 0x8

.field public static final i:I = 0x9

.field public static final j:I = 0xa

.field public static final k:I = 0x65

.field public static final l:I = 0x66

.field public static final m:I = 0x67

.field public static final n:I = 0x68

.field public static final o:I = 0x69

.field public static final p:I = 0x6a

.field public static final q:I = 0x6b

.field public static final r:I = 0x6c

.field public static final s:I = 0x6d

.field public static final t:I = 0x6e

.field public static final u:I = 0x6f

.field public static final v:I = 0x70

.field public static final w:I = 0xc9

.field public static final x:I = 0xca

.field public static final y:I = 0xcb

.field public static final z:I = 0xcc


# instance fields
.field private J:Lcom/samsung/location/a/c;

.field private K:Lcom/samsung/location/a/l;

.field private L:Lcom/samsung/location/batching/SFusedLocationManager;

.field private M:Lcom/samsung/location/a/g;

.field private N:Lcom/samsung/location/a/a;

.field private O:Lcom/samsung/location/lib/SContentManager;

.field private P:Lcom/samsung/location/currentloc/SCurrentLocationManager;

.field private Q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/location/lib/h;->R:Lcom/samsung/location/lib/h;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/location/lib/h;->Q:Z

    return-void
.end method

.method public static a()Lcom/samsung/location/lib/h;
    .locals 1

    sget-object v0, Lcom/samsung/location/lib/h;->R:Lcom/samsung/location/lib/h;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/location/lib/h;

    invoke-direct {v0}, Lcom/samsung/location/lib/h;-><init>()V

    sput-object v0, Lcom/samsung/location/lib/h;->R:Lcom/samsung/location/lib/h;

    :cond_0
    sget-object v0, Lcom/samsung/location/lib/h;->R:Lcom/samsung/location/lib/h;

    return-object v0
.end method


# virtual methods
.method public b()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/samsung/location/a/g;->a()Lcom/samsung/location/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/h;->M:Lcom/samsung/location/a/g;

    invoke-static {}, Lcom/samsung/location/a/a;->a()Lcom/samsung/location/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/h;->N:Lcom/samsung/location/a/a;

    invoke-static {v1}, Lcom/samsung/location/a/c;->a(Landroid/content/Context;)Lcom/samsung/location/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    invoke-static {}, Lcom/samsung/location/a/l;->a()Lcom/samsung/location/a/l;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/h;->K:Lcom/samsung/location/a/l;

    invoke-static {}, Lcom/samsung/location/batching/SFusedLocationManager;->getInstance()Lcom/samsung/location/batching/SFusedLocationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/h;->L:Lcom/samsung/location/batching/SFusedLocationManager;

    invoke-static {v1}, Lcom/samsung/location/lib/SContentManager;->a(Landroid/content/Context;)Lcom/samsung/location/lib/SContentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/h;->O:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v1}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->getInstance(Landroid/content/Context;)Lcom/samsung/location/currentloc/SCurrentLocationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/h;->P:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/location/lib/h;->Q:Z

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    const/4 v4, 0x1

    iget-boolean v0, p0, Lcom/samsung/location/lib/h;->Q:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    invoke-virtual {v0}, Lcom/samsung/location/a/c;->i()V

    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    invoke-virtual {v0}, Lcom/samsung/location/a/c;->j()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleMessage got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :sswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    invoke-virtual {v0}, Lcom/samsung/location/a/c;->b()V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/samsung/location/a/c;->a(Landroid/content/Intent;Z)V

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/samsung/location/a/c;->a(Landroid/content/Intent;Z)V

    goto :goto_0

    :sswitch_4
    iget-object v1, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v1, v0}, Lcom/samsung/location/a/c;->a(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    invoke-virtual {v0}, Lcom/samsung/location/a/c;->e()V

    iget-object v0, p0, Lcom/samsung/location/lib/h;->P:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/h;->P:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->handleSCurLocScreenOn()V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    invoke-virtual {v0}, Lcom/samsung/location/a/c;->f()V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    invoke-virtual {v0}, Lcom/samsung/location/a/c;->b()V

    iget-object v0, p0, Lcom/samsung/location/lib/h;->O:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    invoke-virtual {v0}, Lcom/samsung/location/a/c;->d()V

    iget-object v0, p0, Lcom/samsung/location/lib/h;->N:Lcom/samsung/location/a/a;

    invoke-virtual {v0}, Lcom/samsung/location/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/h;->N:Lcom/samsung/location/a/a;

    invoke-virtual {v0}, Lcom/samsung/location/a/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/h;->N:Lcom/samsung/location/a/a;

    invoke-virtual {v0}, Lcom/samsung/location/a/a;->b()Z

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/samsung/location/lib/h;->P:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/h;->P:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->handleSCurLocAirPlaneOn()V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/samsung/location/lib/h;->P:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/h;->P:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->handlePackageRemoved()V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    invoke-virtual {v0}, Lcom/samsung/location/a/c;->a()V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    invoke-virtual {v0}, Lcom/samsung/location/a/c;->g()V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/samsung/location/lib/h;->K:Lcom/samsung/location/a/l;

    invoke-virtual {v0}, Lcom/samsung/location/a/l;->b()V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/samsung/location/lib/h;->K:Lcom/samsung/location/a/l;

    invoke-virtual {v0}, Lcom/samsung/location/a/l;->c()V

    goto/16 :goto_0

    :sswitch_e
    iget-object v1, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/samsung/location/a/c;->c(I)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/samsung/location/lib/h;->O:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->j()V

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/location/a/c;->a(II)V

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/location/a/c;->b(II)V

    goto/16 :goto_0

    :sswitch_12
    iget-object v1, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v1, v2, v3, v0}, Lcom/samsung/location/a/c;->a(IILandroid/location/Location;)V

    goto/16 :goto_0

    :sswitch_13
    iget-object v1, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v1, v2, v0}, Lcom/samsung/location/a/c;->a(ILandroid/location/Location;)V

    goto/16 :goto_0

    :sswitch_14
    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/location/a/c;->c(II)V

    goto/16 :goto_0

    :sswitch_15
    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/location/a/c;->d(II)V

    goto/16 :goto_0

    :sswitch_16
    iget-object v0, p0, Lcom/samsung/location/lib/h;->M:Lcom/samsung/location/a/g;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/location/a/g;->a(II)V

    goto/16 :goto_0

    :sswitch_17
    iget-object v0, p0, Lcom/samsung/location/lib/h;->M:Lcom/samsung/location/a/g;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/samsung/location/a/g;->a(I)V

    goto/16 :goto_0

    :sswitch_18
    iget-object v1, p0, Lcom/samsung/location/lib/h;->O:Lcom/samsung/location/lib/SContentManager;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/location/Location;

    invoke-virtual {v1, v0}, Lcom/samsung/location/lib/SContentManager;->a(Landroid/location/Location;)V

    goto/16 :goto_0

    :sswitch_19
    iget-object v0, p0, Lcom/samsung/location/lib/h;->N:Lcom/samsung/location/a/a;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/samsung/location/a/a;->e(I)V

    iget-object v0, p0, Lcom/samsung/location/lib/h;->N:Lcom/samsung/location/a/a;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/samsung/location/a/a;->c(I)V

    goto/16 :goto_0

    :sswitch_1a
    iget-object v0, p0, Lcom/samsung/location/lib/h;->N:Lcom/samsung/location/a/a;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/samsung/location/a/a;->i(I)V

    goto/16 :goto_0

    :sswitch_1b
    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    invoke-virtual {v0}, Lcom/samsung/location/a/c;->c()V

    goto/16 :goto_0

    :sswitch_1c
    iget-object v0, p0, Lcom/samsung/location/lib/h;->L:Lcom/samsung/location/batching/SFusedLocationManager;

    invoke-virtual {v0}, Lcom/samsung/location/batching/SFusedLocationManager;->handleRequestLocationsTimeout()V

    goto/16 :goto_0

    :sswitch_1d
    iget-object v0, p0, Lcom/samsung/location/lib/h;->J:Lcom/samsung/location/a/c;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Lcom/samsung/location/a/c;->b(I)V

    goto/16 :goto_0

    :sswitch_1e
    iget-object v0, p0, Lcom/samsung/location/lib/h;->O:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->d()V

    goto/16 :goto_0

    :sswitch_1f
    iget-object v0, p0, Lcom/samsung/location/lib/h;->O:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->j()V

    goto/16 :goto_0

    :sswitch_20
    iget-object v1, p0, Lcom/samsung/location/lib/h;->O:Lcom/samsung/location/lib/SContentManager;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/samsung/location/lib/SContentManager;->e(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_6
        0xa -> :sswitch_9
        0x65 -> :sswitch_c
        0x66 -> :sswitch_a
        0x67 -> :sswitch_b
        0x68 -> :sswitch_e
        0x69 -> :sswitch_f
        0x6a -> :sswitch_18
        0x6b -> :sswitch_d
        0x6d -> :sswitch_1b
        0x6f -> :sswitch_1f
        0x70 -> :sswitch_20
        0xc9 -> :sswitch_10
        0xca -> :sswitch_11
        0xcb -> :sswitch_12
        0xcc -> :sswitch_13
        0xcd -> :sswitch_14
        0xce -> :sswitch_15
        0x12d -> :sswitch_16
        0x12e -> :sswitch_17
        0x130 -> :sswitch_19
        0x131 -> :sswitch_1a
        0x191 -> :sswitch_1c
        0x1f5 -> :sswitch_1d
        0x1f7 -> :sswitch_1e
    .end sparse-switch
.end method
