.class public Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteSecureOpenHelper;


# instance fields
.field final synthetic a:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;


# direct methods
.method public constructor <init>(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;Landroid/content/Context;)V
    .locals 3

    iput-object p1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->a:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    const-string v0, "slocation.db"

    const/4 v1, 0x0

    const/16 v2, 0x8

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteSecureOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    const/4 v3, 0x1

    const-string v0, "createTable"

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->a:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    const-string v1, "g"

    invoke-static {v0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->a:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    const-string v1, "w"

    invoke-static {v0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->a:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    const-string v1, "u"

    invoke-static {v0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "create db error : 3"

    const/4 v1, 0x5

    invoke-static {v0, v1, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->a:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    const-string v1, "u"

    invoke-static {v0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    const/4 v2, 0x2

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->a:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    const-string v1, "g"

    invoke-static {v0, v2, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->a:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    const-string v1, "w"

    invoke-static {v0, v2, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->a:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    const-string v1, "u"

    invoke-static {v0, v2, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "update db error : 3"

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->a:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    const-string v1, "u"

    invoke-static {v0, v2, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    const-string v0, "onCreate"

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-direct {p0, p1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    const-string v0, "onUpgrade"

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-direct {p0, p1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method
