.class public Lcom/samsung/location/lib/NativeManager;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String; = "/system/lib/slocation"

.field private static final b:Ljava/lang/String; = "/system/lib64/slocation"

.field private static final c:Lcom/samsung/location/lib/NativeManager;

.field private static d:Landroid/content/Context;


# instance fields
.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/samsung/location/lib/NativeManager;

    invoke-direct {v0}, Lcom/samsung/location/lib/NativeManager;-><init>()V

    sput-object v0, Lcom/samsung/location/lib/NativeManager;->c:Lcom/samsung/location/lib/NativeManager;

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/location/lib/NativeManager;->d:Landroid/content/Context;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/location/lib/NativeManager;->e:Z

    return-void
.end method

.method public static a()Lcom/samsung/location/lib/NativeManager;
    .locals 1

    sget-object v0, Lcom/samsung/location/lib/NativeManager;->c:Lcom/samsung/location/lib/NativeManager;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/samsung/location/lib/NativeManager;
    .locals 3

    sget-object v0, Lcom/samsung/location/lib/NativeManager;->d:Landroid/content/Context;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/location/lib/NativeManager;->c:Lcom/samsung/location/lib/NativeManager;

    :goto_0
    return-object v0

    :cond_0
    sput-object p0, Lcom/samsung/location/lib/NativeManager;->d:Landroid/content/Context;

    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_1

    const-string v0, "/system/lib/slocation/libsgeo.so"

    invoke-static {v0}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget-object v0, Lcom/samsung/location/lib/NativeManager;->c:Lcom/samsung/location/lib/NativeManager;

    goto :goto_0

    :cond_1
    :try_start_1
    sget-object v0, Landroid/os/Build;->SUPPORTED_64_BIT_ABIS:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    const-string v0, "/system/lib64/slocation/libsgeo.so"

    invoke-static {v0}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_1

    :cond_2
    :try_start_2
    const-string v0, "/system/lib/slocation/libsgeo.so"

    invoke-static {v0}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method private native native_get()Ljava/lang/String;
.end method

.method private native native_getc()Ljava/lang/String;
.end method

.method private native native_getw(ILjava/lang/String;)Ljava/lang/String;
.end method

.method private native native_init([I[I)V
.end method


# virtual methods
.method public a(ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/location/lib/NativeManager;->native_getw(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/location/lib/NativeManager;->native_getc()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/Context;)V
    .locals 10

    const/4 v9, 0x5

    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v7, 0x6

    if-nez p1, :cond_1

    const-string v0, "Context is NULL"

    invoke-static {v0, v9, v8}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/samsung/location/lib/NativeManager;->e:Z

    if-nez v0, :cond_0

    iput-boolean v8, p0, Lcom/samsung/location/lib/NativeManager;->e:Z

    new-array v2, v7, [I

    new-array v3, v7, [I

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    if-lt v0, v7, :cond_3

    :cond_2
    :try_start_0
    invoke-direct {p0, v2, v3}, Lcom/samsung/location/lib/NativeManager;->native_init([I[I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iput-boolean v1, p0, Lcom/samsung/location/lib/NativeManager;->e:Z

    const-string v0, "Failed to init libslocation"

    invoke-static {v0, v9, v8}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_3
    aput v0, v2, v0

    aput v0, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    move v0, v1

    :goto_2
    if-ge v0, v7, :cond_2

    aput v0, v2, v0

    aget-object v5, v4, v0

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    aput v5, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public c()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/location/lib/NativeManager;->native_get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
