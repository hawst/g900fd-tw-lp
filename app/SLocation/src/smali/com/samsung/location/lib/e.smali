.class Lcom/samsung/location/lib/e;
.super Landroid/database/ContentObserver;


# instance fields
.field final synthetic a:Lcom/samsung/location/lib/SContentManager;


# direct methods
.method constructor <init>(Lcom/samsung/location/lib/SContentManager;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 10

    const/4 v1, 0x0

    const/4 v7, 0x2

    const/4 v2, 0x1

    const-string v0, "onChange"

    invoke-static {v0, v7, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v4, 0x1

    const/4 v5, 0x2

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->a(Lcom/samsung/location/lib/SContentManager;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "location_mode"

    invoke-static {v0, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "LOCATION_MODE_OFF"

    const/4 v3, 0x2

    const/4 v6, 0x1

    invoke-static {v0, v3, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v3}, Lcom/samsung/location/lib/SContentManager;->b(Lcom/samsung/location/lib/SContentManager;)Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    :try_start_1
    iget-object v3, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v3}, Lcom/samsung/location/lib/SContentManager;->c(Lcom/samsung/location/lib/SContentManager;)Z

    move-result v3

    iget-object v7, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v7}, Lcom/samsung/location/lib/SContentManager;->d(Lcom/samsung/location/lib/SContentManager;)Landroid/location/LocationManager;

    move-result-object v7

    const-string v8, "gps"

    invoke-virtual {v7, v8}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v7

    if-eq v3, v7, :cond_6

    iget-object v3, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    iget-object v7, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v7}, Lcom/samsung/location/lib/SContentManager;->d(Lcom/samsung/location/lib/SContentManager;)Landroid/location/LocationManager;

    move-result-object v7

    const-string v8, "gps"

    invoke-virtual {v7, v8}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v7

    invoke-static {v3, v7}, Lcom/samsung/location/lib/SContentManager;->a(Lcom/samsung/location/lib/SContentManager;Z)V

    move v3, v2

    :goto_1
    iget-object v7, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v7}, Lcom/samsung/location/lib/SContentManager;->e(Lcom/samsung/location/lib/SContentManager;)Z

    move-result v7

    iget-object v8, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v8}, Lcom/samsung/location/lib/SContentManager;->d(Lcom/samsung/location/lib/SContentManager;)Landroid/location/LocationManager;

    move-result-object v8

    const-string v9, "network"

    invoke-virtual {v8, v9}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v8

    if-eq v7, v8, :cond_5

    iget-object v1, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    iget-object v7, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v7}, Lcom/samsung/location/lib/SContentManager;->d(Lcom/samsung/location/lib/SContentManager;)Landroid/location/LocationManager;

    move-result-object v7

    const-string v8, "network"

    invoke-virtual {v7, v8}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v7

    invoke-static {v1, v7}, Lcom/samsung/location/lib/SContentManager;->b(Lcom/samsung/location/lib/SContentManager;Z)V

    :goto_2
    if-nez v3, :cond_0

    if-eqz v2, :cond_3

    :cond_0
    iget-object v1, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v1}, Lcom/samsung/location/lib/SContentManager;->f(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/a/c;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Lcom/samsung/location/a/c;->a(ZZ)V

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->g(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/batching/SFusedLocationManager;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->g(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/batching/SFusedLocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v1}, Lcom/samsung/location/lib/SContentManager;->c(Lcom/samsung/location/lib/SContentManager;)Z

    move-result v1

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, Lcom/samsung/location/batching/SFusedLocationManager;->handleProviderStatusChanged(ZZ)V

    :cond_1
    iget-object v0, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->h(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/currentloc/SCurrentLocationManager;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->h(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/currentloc/SCurrentLocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v1}, Lcom/samsung/location/lib/SContentManager;->c(Lcom/samsung/location/lib/SContentManager;)Z

    move-result v1

    invoke-virtual {v0, v4, v1}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->handleProvderStatusChanged(IZ)V

    :cond_2
    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->h(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/currentloc/SCurrentLocationManager;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->h(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/currentloc/SCurrentLocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/lib/e;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v1}, Lcom/samsung/location/lib/SContentManager;->e(Lcom/samsung/location/lib/SContentManager;)Z

    move-result v1

    invoke-virtual {v0, v5, v1}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->handleProvderStatusChanged(IZ)V

    :cond_3
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :cond_4
    :try_start_2
    const-string v0, "LOCATION_MODE_ON"

    const/4 v3, 0x2

    const/4 v6, 0x1

    invoke-static {v0, v3, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move v0, v2

    goto/16 :goto_0

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Settings.Secure.LOCATION_MODE_OFF got exception"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v7, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v2

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :cond_5
    move v2, v1

    goto :goto_2

    :cond_6
    move v3, v1

    goto/16 :goto_1
.end method
