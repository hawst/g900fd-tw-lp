.class Lcom/samsung/location/lib/f;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field final synthetic a:Lcom/samsung/location/lib/SContentManager;


# direct methods
.method constructor <init>(Lcom/samsung/location/lib/SContentManager;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/lib/f;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onLocationChanged "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/lib/f;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->i(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/lib/h;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/lib/f;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v1}, Lcom/samsung/location/lib/SContentManager;->i(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/lib/h;

    move-result-object v1

    const/16 v2, 0x6a

    new-instance v3, Landroid/location/Location;

    invoke-direct {v3, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    invoke-virtual {v1, v2, v3}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    return-void
.end method
