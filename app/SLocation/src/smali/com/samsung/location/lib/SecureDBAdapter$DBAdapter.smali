.class Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/samsung/location/lib/SecureDBAdapter;

.field private b:Landroid/database/sqlite/SQLiteDatabase;

.field private c:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;

.field private d:Lcom/samsung/location/lib/i;

.field private e:Landroid/content/Context;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/location/lib/SecureDBAdapter;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f:Ljava/lang/String;

    iput-object p2, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/location/lib/NativeManager;->a()Lcom/samsung/location/lib/NativeManager;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/samsung/location/lib/SecureDBAdapter;->a(Lcom/samsung/location/lib/SecureDBAdapter;Lcom/samsung/location/lib/NativeManager;)V

    invoke-static {p1}, Lcom/samsung/location/lib/SecureDBAdapter;->a(Lcom/samsung/location/lib/SecureDBAdapter;)Lcom/samsung/location/lib/NativeManager;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/samsung/location/lib/NativeManager;->b(Landroid/content/Context;)V

    return-void
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "secureRawQuery got exception"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;I)Lcom/samsung/location/a/e;
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e(I)Lcom/samsung/location/a/e;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(IIILjava/lang/String;)V
    .locals 4

    const/4 v3, 0x4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "createSGeoInfo: id("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " geofence data("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "g0"

    invoke-direct {p0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "g1"

    invoke-direct {p0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "g2"

    invoke-direct {p0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "g3"

    invoke-direct {p0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x3

    const-string v2, "g"

    invoke-direct {p0, v1, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    return-void
.end method

.method private a(ILjava/lang/String;IILjava/lang/String;)V
    .locals 4

    const/4 v3, 0x4

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "u1"

    invoke-direct {p0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "u2"

    invoke-direct {p0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "u3"

    invoke-direct {p0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "u4"

    invoke-direct {p0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "u5"

    invoke-direct {p0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "u6"

    invoke-direct {p0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v1, 0x3

    const-string v2, "u"

    invoke-direct {p0, v1, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z
    .locals 6

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "secureInsert got exception"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    invoke-static {v2, v3, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-direct {p0, v5}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method private c(ILjava/lang/String;)V
    .locals 4

    const/4 v3, 0x4

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "w0"

    invoke-direct {p0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "w1"

    invoke-direct {p0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "w2"

    invoke-direct {p0, v3, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v1, 0x3

    const-string v2, "w"

    invoke-direct {p0, v1, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)Z

    return-void
.end method

.method private d(ILjava/lang/String;)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-static {v0}, Lcom/samsung/location/lib/SecureDBAdapter;->a(Lcom/samsung/location/lib/SecureDBAdapter;)Lcom/samsung/location/lib/NativeManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/lib/NativeManager;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "8getQuery exception type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " arg "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :cond_0
    return-object v0
.end method

.method private d(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, p1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "securesimpleQueryForString SQLiteDoneException"

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method private e(Ljava/lang/String;)J
    .locals 5

    const-wide/16 v0, -0x1

    :try_start_0
    invoke-direct {p0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v2, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v4

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J

    move-result-wide v2

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->close()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-wide v0, v2

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v2, "securesimpleQueryForLong SQLiteDoneException"

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method private e(I)Lcom/samsung/location/a/e;
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v5, 0x4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "getSGeofenceInfoGeoData: id("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v8, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const-string v0, ""

    const/4 v0, 0x0

    const-string v1, "select %s from %s where %s=%s"

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "g1"

    invoke-direct {p0, v5, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    const-string v3, "g"

    invoke-direct {p0, v9, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    const-string v3, "g0"

    invoke-direct {p0, v5, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "select %s from %s where %s=%s"

    new-array v3, v5, [Ljava/lang/Object;

    const-string v4, "g3"

    invoke-direct {p0, v5, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    const-string v4, "g"

    invoke-direct {p0, v9, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v10

    const-string v4, "g0"

    invoke-direct {p0, v5, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e(Ljava/lang/String;)J

    move-result-wide v3

    invoke-direct {p0, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v5, 0x1

    cmp-long v2, v3, v5

    if-nez v2, :cond_1

    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/samsung/location/a/e;

    aget-object v1, v5, v7

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    aget-object v3, v5, v10

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v3

    aget-object v5, v5, v8

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/location/a/e;-><init>(DDI)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-wide/16 v5, 0x2

    cmp-long v2, v3, v5

    if-nez v2, :cond_2

    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lcom/samsung/location/a/e;

    aget-object v1, v1, v7

    invoke-direct {v0, v8, v1}, Lcom/samsung/location/a/e;-><init>(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    const-wide/16 v5, 0x3

    cmp-long v2, v3, v5

    if-nez v2, :cond_0

    new-instance v0, Lcom/samsung/location/a/e;

    invoke-direct {v0, v9, v1}, Lcom/samsung/location/a/e;-><init>(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private e()Z
    .locals 2

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/samsung/location/lib/SecureDBAdapter;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d:Lcom/samsung/location/lib/i;

    invoke-virtual {v0}, Lcom/samsung/location/lib/i;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->c:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;

    iget-object v1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->getWritableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    goto :goto_0
.end method

.method private f(Ljava/lang/String;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "secureExecSQL got exception"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    invoke-static {v2, v3, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method private g(Ljava/lang/String;)Ljava/util/List;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-direct {p0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Landroid/database/Cursor;)V

    return-object v0

    :cond_1
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/samsung/location/currentloc/c;)I
    .locals 10

    const/4 v9, 0x1

    const/4 v7, 0x0

    const/4 v6, -0x1

    const/4 v8, 0x0

    const/4 v5, 0x2

    const-string v0, "replaceSCurrentLocationInfo"

    invoke-static {v0, v5, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->c()Landroid/location/Location;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "invalid param"

    invoke-static {v0, v5, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v6

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->c()Landroid/location/Location;

    move-result-object v0

    const-string v1, "select %s from %s order by %s limit 1"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x4

    const-string v4, "u0"

    invoke-direct {p0, v3, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    const/4 v3, 0x3

    const-string v4, "u"

    invoke-direct {p0, v3, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    const/4 v3, 0x4

    const-string v4, "u5"

    invoke-direct {p0, v3, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e(Ljava/lang/String;)J

    move-result-wide v1

    long-to-int v7, v1

    if-lez v7, :cond_1

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "replace with id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v1, 0x7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f(Ljava/lang/String;)Z

    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ";"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->b()Lcom/samsung/location/currentloc/d;

    move-result-object v0

    iget v1, v0, Lcom/samsung/location/currentloc/d;->a:I

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->b()Lcom/samsung/location/currentloc/d;

    move-result-object v0

    iget v3, v0, Lcom/samsung/location/currentloc/d;->c:I

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->b()Lcom/samsung/location/currentloc/d;

    move-result-object v0

    iget v4, v0, Lcom/samsung/location/currentloc/d;->d:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(ILjava/lang/String;IILjava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, v8}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Landroid/database/Cursor;)V

    move v0, v7

    goto/16 :goto_0

    :cond_1
    invoke-direct {p0, v8}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Landroid/database/Cursor;)V

    move v0, v6

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "replaceSCurrentLocationInfo failed :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-direct {p0, v8}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Landroid/database/Cursor;)V

    move v0, v6

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0, v8}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Landroid/database/Cursor;)V

    throw v0
.end method

.method public a(Ljava/lang/String;)I
    .locals 6

    const/4 v0, 0x0

    const-string v1, "select %s from %s "

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x30

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x1

    const/4 v4, 0x3

    invoke-direct {p0, v4, p1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :cond_0
    invoke-direct {p0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Landroid/database/Cursor;)V

    return v0
.end method

.method public a(Ljava/lang/String;IZ)Lcom/samsung/location/currentloc/c;
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x2

    const/4 v7, 0x3

    const/4 v8, 0x0

    const/4 v6, 0x4

    const-string v0, "checkCellLocation"

    invoke-static {v0, v9, v8}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const-string v0, "select %s from %s where %s=%s and %s=%s"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "u4"

    invoke-direct {p0, v6, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const-string v2, "u"

    invoke-direct {p0, v7, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v10

    const-string v2, "u2"

    invoke-direct {p0, v6, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v9

    aput-object p1, v1, v7

    const-string v2, "u3"

    invoke-direct {p0, v6, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lcom/samsung/location/currentloc/c;

    aget-object v2, v1, v8

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    aget-object v1, v1, v10

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/samsung/location/currentloc/c;-><init>(DD)V

    const-string v1, "select %s from %s where %s=%s and %s=%s"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "u0"

    invoke-direct {p0, v6, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    const-string v3, "u"

    invoke-direct {p0, v7, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    const-string v3, "u2"

    invoke-direct {p0, v6, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    aput-object p1, v2, v7

    const-string v3, "u3"

    invoke-direct {p0, v6, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "select %s from %s where %s=%s and %s=%s"

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "u5"

    invoke-direct {p0, v6, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    const-string v4, "u"

    invoke-direct {p0, v7, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v10

    const-string v4, "u2"

    invoke-direct {p0, v6, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v9

    aput-object p1, v3, v7

    const-string v4, "u3"

    invoke-direct {p0, v6, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    if-eqz p3, :cond_0

    invoke-direct {p0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e(Ljava/lang/String;)J

    move-result-wide v3

    invoke-direct {p0, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e(Ljava/lang/String;)J

    move-result-wide v1

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0xca

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f(Ljava/lang/String;)Z

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "matchcount updated id "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " count "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v9, v8}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :cond_0
    return-object v0
.end method

.method public a()Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;
    .locals 4

    const/4 v0, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    const-string v1, "open"

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :try_start_0
    iget-object v1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-static {v1}, Lcom/samsung/location/lib/SecureDBAdapter;->a(Lcom/samsung/location/lib/SecureDBAdapter;)Lcom/samsung/location/lib/NativeManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/location/lib/NativeManager;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v1, "null key"

    const/4 v2, 0x4

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move-object p0, v0

    :goto_0
    return-object p0

    :catch_0
    move-exception v1

    const-string v1, "Failed to get from libslocation"

    const/4 v2, 0x5

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move-object p0, v0

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;

    iget-object v1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;-><init>(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->c:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->c:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;

    iget-object v1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->getWritableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "remove previous db file "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system"

    const-string v2, "slocation.db"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    new-instance v0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;

    iget-object v1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;-><init>(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->c:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->c:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;

    iget-object v1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->getWritableDatabase([B)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v0
.end method

.method public a(II)V
    .locals 3

    const-string v0, "update geofence cell exist"

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xc9

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f(Ljava/lang/String;)Z

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 3

    const-string v0, "update geofence mcc"

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xcb

    invoke-direct {p0, v1, p2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f(Ljava/lang/String;)Z

    return-void
.end method

.method public a(I)Z
    .locals 4

    const/4 v3, 0x2

    const/4 v0, 0x0

    const-string v1, "deleteSGeoData"

    invoke-static {v1, v3, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteSGeoData: id("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v3, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v1, 0x5

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f(Ljava/lang/String;)Z

    const/4 v1, 0x6

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public a(ILcom/samsung/location/a/e;)Z
    .locals 7

    const/4 v6, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x2

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "updateSGeoData : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x65

    const-string v2, "3"

    invoke-direct {p0, v1, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f(Ljava/lang/String;)Z

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "create DB for id "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const-string v0, ""

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->a()I

    move-result v1

    if-ne v1, v6, :cond_2

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->b()D

    move-result-wide v1

    const-wide v3, 0x4056800000000000L    # 90.0

    cmpl-double v1, v1, v3

    if-gtz v1, :cond_1

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->b()D

    move-result-wide v1

    const-wide v3, -0x3fa9800000000000L    # -90.0

    cmpg-double v1, v1, v3

    if-ltz v1, :cond_1

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->c()D

    move-result-wide v1

    const-wide v3, 0x4066800000000000L    # 180.0

    cmpl-double v1, v1, v3

    if-gtz v1, :cond_1

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->c()D

    move-result-wide v1

    const-wide v3, -0x3f99800000000000L    # -180.0

    cmpg-double v1, v1, v3

    if-ltz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->b()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->c()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->d()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->a()I

    move-result v1

    invoke-direct {p0, p1, v1, v5, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(IIILjava/lang/String;)V

    :cond_1
    :goto_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Landroid/database/Cursor;)V

    return v6

    :cond_2
    invoke-virtual {p2}, Lcom/samsung/location/a/e;->a()I

    move-result v0

    if-ne v0, v3, :cond_4

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->f()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->e()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p2}, Lcom/samsung/location/a/e;->a()I

    move-result v1

    invoke-direct {p0, p1, v1, v4, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(IIILjava/lang/String;)V

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-virtual {p2}, Lcom/samsung/location/a/e;->a()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->a()I

    move-result v1

    invoke-direct {p0, p1, v1, v4, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(IIILjava/lang/String;)V

    goto :goto_0
.end method

.method public a(ILjava/util/List;)Z
    .locals 13

    const/4 v12, 0x3

    const/4 v3, 0x1

    const/4 v11, 0x4

    const/4 v10, 0x2

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "putWifiData - scanresult is null"

    invoke-static {v0, v10, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    return v2

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "putWifiData "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " size :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v10, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const-string v0, "select %s from %s where %s=%s"

    new-array v1, v11, [Ljava/lang/Object;

    const-string v4, "g1"

    invoke-direct {p0, v11, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    const-string v4, "g"

    invoke-direct {p0, v12, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const-string v4, "g0"

    invoke-direct {p0, v11, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v10

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v12

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v4, -0x1

    cmp-long v4, v0, v4

    if-eqz v4, :cond_2

    const-wide/16 v4, 0x1

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    const-string v0, "geofence type mismatch"

    invoke-static {v0, v10, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_2
    const-string v0, "geofence data does not exist"

    invoke-static {v0, v10, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x66

    const-string v6, "a"

    invoke-direct {p0, v1, v6}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_4
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "matched num : "

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v10, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x64

    if-lez v0, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "deleteCount :"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v10, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const-string v1, "delete from %s where %s=%s and %s in (select %s from %s where %s=%s order by %s limit %s)"

    const/16 v4, 0xa

    new-array v4, v4, [Ljava/lang/Object;

    const-string v6, "w"

    invoke-direct {p0, v12, v6}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    const-string v6, "w0"

    invoke-direct {p0, v11, v6}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v10

    const-string v6, "w1"

    invoke-direct {p0, v11, v6}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v12

    const-string v6, "w1"

    invoke-direct {p0, v11, v6}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v11

    const/4 v6, 0x5

    const-string v7, "w"

    invoke-direct {p0, v12, v7}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x6

    const-string v7, "w0"

    invoke-direct {p0, v11, v7}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x7

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/16 v6, 0x8

    const-string v7, "w2"

    invoke-direct {p0, v11, v7}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    const/16 v6, 0x9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f(Ljava/lang/String;)Z

    :cond_5
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Landroid/database/Cursor;)V

    move v2, v3

    goto/16 :goto_0

    :cond_6
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_1

    :cond_7
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    iget v7, v0, Landroid/net/wifi/ScanResult;->level:I

    const/16 v8, -0x53

    if-ge v7, v8, :cond_8

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Skip weak signal BSSID :"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " RSSI :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v0, v0, Landroid/net/wifi/ScanResult;->level:I

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v10, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto/16 :goto_2

    :cond_8
    iget-object v7, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android"

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    const/4 v8, -0x1

    if-ne v7, v8, :cond_9

    iget-object v7, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    const-string v8, "iphone"

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_a

    :cond_9
    const-string v0, "Skip Mobile AP(android, iphone)"

    invoke-static {v0, v10, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto/16 :goto_2

    :cond_a
    iget-object v7, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-interface {v4, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    iget-object v0, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_b
    add-int/lit8 v1, v1, 0x1

    const-string v7, "where %s=%s and %s=\'%s\'"

    new-array v8, v11, [Ljava/lang/Object;

    const-string v9, "w0"

    invoke-direct {p0, v11, v9}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v3

    const-string v9, "w1"

    invoke-direct {p0, v11, v9}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    iget-object v0, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    aput-object v0, v8, v12

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v8, 0xca

    const-string v9, "w"

    invoke-direct {p0, v8, v9}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f(Ljava/lang/String;)Z

    goto/16 :goto_2

    :cond_c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "putWifi BSSID :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v10, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-direct {p0, p1, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->c(ILjava/lang/String;)V

    goto/16 :goto_3
.end method

.method public b()Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;
    .locals 3

    const/4 v2, 0x2

    const-string v0, "openPlainDB"

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :try_start_0
    new-instance v0, Lcom/samsung/location/lib/i;

    iget-object v1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/samsung/location/lib/i;-><init>(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d:Lcom/samsung/location/lib/i;

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d:Lcom/samsung/location/lib/i;

    invoke-virtual {v0}, Lcom/samsung/location/lib/i;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "remove previous db file "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system"

    const-string v2, "slocation.db"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    new-instance v0, Lcom/samsung/location/lib/i;

    iget-object v1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/samsung/location/lib/i;-><init>(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d:Lcom/samsung/location/lib/i;

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d:Lcom/samsung/location/lib/i;

    invoke-virtual {v0}, Lcom/samsung/location/lib/i;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v0
.end method

.method public b(I)Ljava/lang/String;
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "getSGeoMcc: id("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const-string v0, "select %s from %s where %s=%s"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "g4"

    invoke-direct {p0, v4, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x1

    const-string v3, "g"

    invoke-direct {p0, v6, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "g0"

    invoke-direct {p0, v4, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Ljava/util/List;
    .locals 6

    const-string v0, "select %s from %s "

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x30

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x3

    invoke-direct {p0, v3, p1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->g(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b(ILjava/lang/String;)V
    .locals 3

    const-string v0, "update geofence package"

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x65

    const-string v2, "5"

    invoke-direct {p0, v1, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xcd

    invoke-direct {p0, v1, p2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f(Ljava/lang/String;)Z

    :cond_0
    return-void
.end method

.method public b(Lcom/samsung/location/currentloc/c;)V
    .locals 14

    const/4 v5, 0x4

    const/4 v8, 0x1

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    const/4 v11, 0x2

    const/4 v10, 0x0

    const-string v0, "updateSCurLoc"

    invoke-static {v0, v11, v10}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->c()Landroid/location/Location;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "invalid param"

    invoke-static {v0, v11, v10}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->c()Landroid/location/Location;

    move-result-object v0

    const-string v1, "%s\'%s\' and %s=%s"

    new-array v2, v5, [Ljava/lang/Object;

    const/16 v3, 0x67

    const-string v4, "0"

    invoke-direct {p0, v3, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->b()Lcom/samsung/location/currentloc/d;

    move-result-object v3

    iget v3, v3, Lcom/samsung/location/currentloc/d;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    const-string v3, "u1"

    invoke-direct {p0, v5, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v11

    const/4 v3, 0x3

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->b()Lcom/samsung/location/currentloc/d;

    move-result-object v4

    iget v4, v4, Lcom/samsung/location/currentloc/d;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-gez v3, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "create DB for cellid "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->b()Lcom/samsung/location/currentloc/d;

    move-result-object v2

    iget v2, v2, Lcom/samsung/location/currentloc/d;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v11, v10}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const-string v1, ""

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ";"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->b()Lcom/samsung/location/currentloc/d;

    move-result-object v0

    iget v1, v0, Lcom/samsung/location/currentloc/d;->a:I

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->b()Lcom/samsung/location/currentloc/d;

    move-result-object v0

    iget v3, v0, Lcom/samsung/location/currentloc/d;->c:I

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->b()Lcom/samsung/location/currentloc/d;

    move-result-object v0

    iget v4, v0, Lcom/samsung/location/currentloc/d;->d:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(ILjava/lang/String;IILjava/lang/String;)V

    :cond_1
    :goto_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Landroid/database/Cursor;)V

    goto/16 :goto_0

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x67

    const-string v5, "5"

    invoke-direct {p0, v4, v5}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " prev data "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v11, v10}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    if-eqz v3, :cond_1

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    aget-object v6, v3, v10

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    add-double/2addr v4, v6

    div-double/2addr v4, v12

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    aget-object v0, v3, v8

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    add-double/2addr v6, v8

    div-double/2addr v6, v12

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "new data "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v11, v10}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0xcc

    invoke-direct {p0, v4, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f(Ljava/lang/String;)Z

    goto/16 :goto_1
.end method

.method public b(ILjava/util/List;)Z
    .locals 10

    const/4 v8, 0x3

    const/4 v7, 0x4

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "putWifiDataForWifiGeofence "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " size :"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v9, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    const-string v3, "select %s from %s where %s=%s"

    new-array v4, v7, [Ljava/lang/Object;

    const-string v5, "g1"

    invoke-direct {p0, v7, v5}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    const-string v5, "g"

    invoke-direct {p0, v8, v5}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    const-string v5, "g0"

    invoke-direct {p0, v7, v5}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "select %s from %s where %s=%s"

    new-array v5, v7, [Ljava/lang/Object;

    const-string v6, "g3"

    invoke-direct {p0, v7, v6}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    const-string v6, "g"

    invoke-direct {p0, v8, v6}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    const-string v6, "g0"

    invoke-direct {p0, v7, v6}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e(Ljava/lang/String;)J

    move-result-wide v5

    const-wide/16 v7, 0x2

    cmp-long v3, v5, v7

    if-eqz v3, :cond_0

    const-string v0, "geofence type mismatch 2"

    invoke-static {v0, v9, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    aget-object v4, v3, v1

    array-length v5, v3

    if-le v5, v2, :cond_6

    aget-object v0, v3, v2

    move-object v3, v0

    :goto_1
    if-eqz v3, :cond_2

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const-string v0, "ssid match failed"

    invoke-static {v0, v9, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-direct {p0, p1, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->c(ILjava/lang/String;)V

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    iget-object v5, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "putWifiDataForWifiGeofence BSSID :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " SSID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v9, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->c(ILjava/lang/String;)V

    goto :goto_2

    :cond_6
    move-object v3, v0

    goto :goto_1
.end method

.method public c(I)I
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "getSGeoInfoCellDB: id("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const-string v0, "select %s from %s where %s=%s"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "g2"

    invoke-direct {p0, v4, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x1

    const-string v3, "g"

    invoke-direct {p0, v6, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "g0"

    invoke-direct {p0, v4, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->e(Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public c(Ljava/lang/String;)Ljava/util/List;
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x4

    const-string v0, "select %s from %s where %s=\'%s\'"

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "g0"

    invoke-direct {p0, v4, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "g"

    invoke-direct {p0, v5, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "g5"

    invoke-direct {p0, v4, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    aput-object p1, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->g(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 3

    const-string v0, "close"

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->c:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter$DatabaseHelper;->close()V

    return-void
.end method

.method public d(I)Ljava/util/List;
    .locals 5

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getSGeofenceInfoWifilist: id("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x66

    const-string v4, "a"

    invoke-direct {p0, v3, v4}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    invoke-direct {p0, v2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Landroid/database/Cursor;)V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    :goto_1
    return-object v0

    :cond_1
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public d()Z
    .locals 4

    const/4 v0, 0x0

    const-string v1, "clearLocationData"

    const/4 v2, 0x2

    invoke-static {v1, v2, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "delete from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x3

    const-string v3, "u"

    invoke-direct {p0, v2, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-static {v2}, Lcom/samsung/location/lib/SecureDBAdapter;->b(Lcom/samsung/location/lib/SecureDBAdapter;)Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    move-result-object v2

    invoke-direct {v2, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->f(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method
