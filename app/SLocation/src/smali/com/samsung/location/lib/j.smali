.class public Lcom/samsung/location/lib/j;
.super Ljava/lang/Object;


# static fields
.field private static b:Lcom/samsung/location/lib/j;


# instance fields
.field private a:Lcom/samsung/location/lib/SecureDBAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-direct {v0, p1, p2}, Lcom/samsung/location/lib/SecureDBAdapter;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    return-void
.end method

.method public static a()Lcom/samsung/location/lib/j;
    .locals 1

    sget-object v0, Lcom/samsung/location/lib/j;->b:Lcom/samsung/location/lib/j;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Z)Lcom/samsung/location/lib/j;
    .locals 1

    sget-object v0, Lcom/samsung/location/lib/j;->b:Lcom/samsung/location/lib/j;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/location/lib/j;

    invoke-direct {v0, p0, p1}, Lcom/samsung/location/lib/j;-><init>(Landroid/content/Context;Z)V

    sput-object v0, Lcom/samsung/location/lib/j;->b:Lcom/samsung/location/lib/j;

    :cond_0
    sget-object v0, Lcom/samsung/location/lib/j;->b:Lcom/samsung/location/lib/j;

    return-object v0
.end method


# virtual methods
.method public a(I)I
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/SecureDBAdapter;->a(I)I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/SecureDBAdapter;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(II)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/lib/SecureDBAdapter;->a(II)V

    return-void
.end method

.method public a(ILcom/samsung/location/a/e;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/lib/SecureDBAdapter;->a(ILcom/samsung/location/a/e;)V

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/lib/SecureDBAdapter;->a(ILjava/lang/String;)V

    return-void
.end method

.method public a(ILjava/util/List;I)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/location/lib/SecureDBAdapter;->a(ILjava/util/List;I)V

    return-void
.end method

.method public a(Lcom/samsung/location/currentloc/c;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->d()I

    move-result v1

    if-gez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid data "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/c;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v1, v2, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v1, p1}, Lcom/samsung/location/lib/SecureDBAdapter;->a(Lcom/samsung/location/currentloc/c;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I)Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v1, p1, p2, v0}, Lcom/samsung/location/lib/SecureDBAdapter;->a(Ljava/lang/String;IZ)Lcom/samsung/location/currentloc/c;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SecureDBAdapter;->c()I

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;I)Lcom/samsung/location/currentloc/c;
    .locals 2

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/samsung/location/lib/SecureDBAdapter;->a(Ljava/lang/String;IZ)Lcom/samsung/location/currentloc/c;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/SecureDBAdapter;->b(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/lib/SecureDBAdapter;->b(ILjava/lang/String;)V

    return-void
.end method

.method public c(I)Lcom/samsung/location/a/e;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/SecureDBAdapter;->c(I)Lcom/samsung/location/a/e;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SecureDBAdapter;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public d(I)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/SecureDBAdapter;->d(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SecureDBAdapter;->d()Z

    move-result v0

    return v0
.end method

.method public e(I)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/j;->a:Lcom/samsung/location/lib/SecureDBAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/SecureDBAdapter;->e(I)Z

    move-result v0

    return v0
.end method
