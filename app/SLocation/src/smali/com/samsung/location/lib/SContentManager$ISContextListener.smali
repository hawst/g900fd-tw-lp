.class Lcom/samsung/location/lib/SContentManager$ISContextListener;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/scontext/SContextListener;


# instance fields
.field final synthetic a:Lcom/samsung/location/lib/SContentManager;


# direct methods
.method private constructor <init>(Lcom/samsung/location/lib/SContentManager;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/lib/SContentManager$ISContextListener;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/location/lib/SContentManager;Lcom/samsung/location/lib/SContentManager$ISContextListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/location/lib/SContentManager$ISContextListener;-><init>(Lcom/samsung/location/lib/SContentManager;)V

    return-void
.end method


# virtual methods
.method public onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget-object v0, p1, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    invoke-virtual {v0}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v1

    const/16 v2, 0x1e

    if-ne v1, v2, :cond_1

    const-string v0, "TYPE_ACTIVITY_NOTIFICATION_EX"

    invoke-static {v0, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getActivityNotificationExContext()Landroid/hardware/scontext/SContextActivityNotificationEx;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager$ISContextListener;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v1}, Lcom/samsung/location/lib/SContentManager;->i(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/lib/h;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager$ISContextListener;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v2}, Lcom/samsung/location/lib/SContentManager;->i(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/lib/h;

    move-result-object v2

    const/16 v3, 0x1f5

    invoke-virtual {v0}, Landroid/hardware/scontext/SContextActivityNotificationEx;->getStatus()I

    move-result v0

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v4}, Lcom/samsung/location/lib/h;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v0

    const/16 v1, 0x1a

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TYPE_ACTIVITY_BATCH "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager$ISContextListener;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v1}, Lcom/samsung/location/lib/SContentManager;->j(Lcom/samsung/location/lib/SContentManager;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager$ISContextListener;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->b(Lcom/samsung/location/lib/SContentManager;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager$ISContextListener;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getActivityBatchContext()Landroid/hardware/scontext/SContextActivityBatch;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/location/lib/SContentManager;->a(Lcom/samsung/location/lib/SContentManager;Landroid/hardware/scontext/SContextActivityBatch;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager$ISContextListener;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/SContentManager;->a(Lcom/samsung/location/lib/SContentManager;J)V

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager$ISContextListener;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->i(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/lib/h;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager$ISContextListener;->a:Lcom/samsung/location/lib/SContentManager;

    invoke-static {v1}, Lcom/samsung/location/lib/SContentManager;->i(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/lib/h;

    move-result-object v1

    const/16 v2, 0x1f7

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
