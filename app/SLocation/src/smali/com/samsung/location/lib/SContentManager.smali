.class public Lcom/samsung/location/lib/SContentManager;
.super Ljava/lang/Object;


# static fields
.field public static final a:I = 0x1

.field public static final b:I = 0x2

.field public static final c:I = 0x3

.field public static final d:I = 0x4

.field public static final e:I = 0x5

.field public static final f:I = 0x1

.field public static final g:I = 0x2

.field public static final h:I = 0x1

.field public static final i:I = 0x2

.field public static final j:I = 0x1

.field public static final k:I = 0x2

.field private static final l:I = 0x1

.field private static final m:I = 0x2

.field private static final n:I = 0x3

.field private static final o:I = 0x4

.field private static final p:I = 0x5

.field private static final q:I = -0x1

.field private static final r:I = 0x0

.field private static final s:I = 0x1

.field private static y:Lcom/samsung/location/lib/SContentManager;


# instance fields
.field private A:Lcom/samsung/location/batching/SFusedLocationManager;

.field private B:Lcom/samsung/location/currentloc/SCurrentLocationManager;

.field private C:Landroid/hardware/scontext/SContextManager;

.field private D:Landroid/location/LocationManager;

.field private E:Landroid/telephony/TelephonyManager;

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:I

.field private L:J

.field private M:J

.field private N:J

.field private O:Landroid/location/Location;

.field private P:Ljava/util/List;

.field private Q:Ljava/util/List;

.field private R:Ljava/util/List;

.field private S:Ljava/util/List;

.field private T:Ljava/lang/Object;

.field private U:Ljava/lang/StringBuffer;

.field private V:Landroid/hardware/scontext/SContextListener;

.field private W:Landroid/database/ContentObserver;

.field private X:Landroid/location/LocationListener;

.field private t:I

.field private u:I

.field private v:Landroid/net/ConnectivityManager;

.field private w:Landroid/content/Context;

.field private x:Lcom/samsung/location/lib/h;

.field private z:Lcom/samsung/location/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/location/lib/SContentManager;->y:Lcom/samsung/location/lib/SContentManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/samsung/location/lib/SContentManager;->t:I

    iput v0, p0, Lcom/samsung/location/lib/SContentManager;->u:I

    iput-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->F:Z

    iput-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->G:Z

    iput-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->H:Z

    iput-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->I:Z

    iput-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->J:Z

    iput v0, p0, Lcom/samsung/location/lib/SContentManager;->K:I

    iput-wide v1, p0, Lcom/samsung/location/lib/SContentManager;->L:J

    iput-wide v1, p0, Lcom/samsung/location/lib/SContentManager;->M:J

    iput-wide v1, p0, Lcom/samsung/location/lib/SContentManager;->N:J

    iput-object v3, p0, Lcom/samsung/location/lib/SContentManager;->O:Landroid/location/Location;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->Q:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->S:Ljava/util/List;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->T:Ljava/lang/Object;

    new-instance v0, Lcom/samsung/location/lib/SContentManager$ISContextListener;

    invoke-direct {v0, p0, v3}, Lcom/samsung/location/lib/SContentManager$ISContextListener;-><init>(Lcom/samsung/location/lib/SContentManager;Lcom/samsung/location/lib/SContentManager$ISContextListener;)V

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->V:Landroid/hardware/scontext/SContextListener;

    new-instance v0, Lcom/samsung/location/lib/e;

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager;->x:Lcom/samsung/location/lib/h;

    invoke-direct {v0, p0, v1}, Lcom/samsung/location/lib/e;-><init>(Lcom/samsung/location/lib/SContentManager;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->W:Landroid/database/ContentObserver;

    new-instance v0, Lcom/samsung/location/lib/f;

    invoke-direct {v0, p0}, Lcom/samsung/location/lib/f;-><init>(Lcom/samsung/location/lib/SContentManager;)V

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->X:Landroid/location/LocationListener;

    iput-object p1, p0, Lcom/samsung/location/lib/SContentManager;->w:Landroid/content/Context;

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->w:Landroid/content/Context;

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->D:Landroid/location/LocationManager;

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->w:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->E:Landroid/telephony/TelephonyManager;

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->D:Landroid/location/LocationManager;

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->F:Z

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->D:Landroid/location/LocationManager;

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->G:Z

    return-void
.end method

.method static synthetic a(Lcom/samsung/location/lib/SContentManager;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->w:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/samsung/location/lib/SContentManager;
    .locals 1

    sget-object v0, Lcom/samsung/location/lib/SContentManager;->y:Lcom/samsung/location/lib/SContentManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/location/lib/SContentManager;

    invoke-direct {v0, p0}, Lcom/samsung/location/lib/SContentManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/location/lib/SContentManager;->y:Lcom/samsung/location/lib/SContentManager;

    sget-object v0, Lcom/samsung/location/lib/SContentManager;->y:Lcom/samsung/location/lib/SContentManager;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/samsung/location/lib/SContentManager;->y:Lcom/samsung/location/lib/SContentManager;

    goto :goto_0
.end method

.method private a(Landroid/hardware/scontext/SContextActivityBatch;)V
    .locals 9

    invoke-virtual {p1}, Landroid/hardware/scontext/SContextActivityBatch;->getTimeStamp()[J

    move-result-object v1

    invoke-virtual {p1}, Landroid/hardware/scontext/SContextActivityBatch;->getStatus()[I

    move-result-object v2

    invoke-virtual {p1}, Landroid/hardware/scontext/SContextActivityBatch;->getAccuracy()[I

    move-result-object v3

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->S:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    :goto_0
    array-length v4, v2

    if-lt v0, v4, :cond_0

    return-void

    :cond_0
    new-instance v4, Lcom/samsung/location/lib/d;

    aget-wide v5, v1, v0

    aget v7, v2, v0

    aget v8, v3, v0

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/samsung/location/lib/d;-><init>(JII)V

    iget-object v5, p0, Lcom/samsung/location/lib/SContentManager;->S:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lcom/samsung/location/lib/SContentManager;J)V
    .locals 0

    iput-wide p1, p0, Lcom/samsung/location/lib/SContentManager;->L:J

    return-void
.end method

.method static synthetic a(Lcom/samsung/location/lib/SContentManager;Landroid/hardware/scontext/SContextActivityBatch;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/location/lib/SContentManager;->a(Landroid/hardware/scontext/SContextActivityBatch;)V

    return-void
.end method

.method static synthetic a(Lcom/samsung/location/lib/SContentManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/location/lib/SContentManager;->F:Z

    return-void
.end method

.method static synthetic b(Lcom/samsung/location/lib/SContentManager;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->T:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lcom/samsung/location/lib/SContentManager;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/location/lib/SContentManager;->G:Z

    return-void
.end method

.method static synthetic c(Lcom/samsung/location/lib/SContentManager;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->F:Z

    return v0
.end method

.method static synthetic d(Lcom/samsung/location/lib/SContentManager;)Landroid/location/LocationManager;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->D:Landroid/location/LocationManager;

    return-object v0
.end method

.method static synthetic e(Lcom/samsung/location/lib/SContentManager;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->G:Z

    return v0
.end method

.method static synthetic f(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/a/c;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->z:Lcom/samsung/location/a/c;

    return-object v0
.end method

.method private f(I)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    iget-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->I:Z

    if-nez v0, :cond_2

    iput-boolean v2, p0, Lcom/samsung/location/lib/SContentManager;->I:Z

    if-ne p1, v2, :cond_1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->D:Landroid/location/LocationManager;

    const-string v1, "gps"

    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager;->X:Landroid/location/LocationListener;

    invoke-virtual {v0, v1, v2, v5}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    const-string v0, "GPS requested"

    invoke-static {v0, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p1, v3, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->D:Landroid/location/LocationManager;

    const-string v1, "network"

    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager;->X:Landroid/location/LocationListener;

    invoke-virtual {v0, v1, v2, v5}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    const-string v0, "NLP requested"

    invoke-static {v0, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "already running "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/batching/SFusedLocationManager;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->A:Lcom/samsung/location/batching/SFusedLocationManager;

    return-object v0
.end method

.method static synthetic h(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/currentloc/SCurrentLocationManager;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->B:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    return-object v0
.end method

.method static synthetic i(Lcom/samsung/location/lib/SContentManager;)Lcom/samsung/location/lib/h;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->x:Lcom/samsung/location/lib/h;

    return-object v0
.end method

.method static synthetic j(Lcom/samsung/location/lib/SContentManager;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    return-object v0
.end method

.method private k()Z
    .locals 2

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->w:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(IDDDFFFJ)Landroid/location/Location;
    .locals 3

    new-instance v0, Landroid/location/Location;

    const-string v1, "gps"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    and-int/lit8 v1, p1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {v0, p2, p3}, Landroid/location/Location;->setLatitude(D)V

    invoke-virtual {v0, p4, p5}, Landroid/location/Location;->setLongitude(D)V

    invoke-virtual {v0, p11, p12}, Landroid/location/Location;->setTime(J)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V

    :cond_0
    and-int/lit8 v1, p1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    invoke-virtual {v0, p6, p7}, Landroid/location/Location;->setAltitude(D)V

    :goto_0
    and-int/lit8 v1, p1, 0x3

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    invoke-virtual {v0, p8}, Landroid/location/Location;->setSpeed(F)V

    :goto_1
    and-int/lit8 v1, p1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    invoke-virtual {v0, p9}, Landroid/location/Location;->setBearing(F)V

    :goto_2
    and-int/lit8 v1, p1, 0x5

    const/4 v2, 0x5

    if-ne v1, v2, :cond_4

    invoke-virtual {v0, p10}, Landroid/location/Location;->setAccuracy(F)V

    :goto_3
    return-object v0

    :cond_1
    invoke-virtual {v0}, Landroid/location/Location;->removeAltitude()V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/location/Location;->removeSpeed()V

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Landroid/location/Location;->removeBearing()V

    goto :goto_2

    :cond_4
    invoke-virtual {v0}, Landroid/location/Location;->removeAccuracy()V

    goto :goto_3
.end method

.method public a()V
    .locals 5

    const/4 v4, -0x1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->w:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/location/a/c;->a(Landroid/content/Context;)Lcom/samsung/location/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->z:Lcom/samsung/location/a/c;

    invoke-static {}, Lcom/samsung/location/batching/SFusedLocationManager;->getInstance()Lcom/samsung/location/batching/SFusedLocationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->A:Lcom/samsung/location/batching/SFusedLocationManager;

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->w:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->getInstance(Landroid/content/Context;)Lcom/samsung/location/currentloc/SCurrentLocationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->B:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->w:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->v:Landroid/net/ConnectivityManager;

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->w:Landroid/content/Context;

    const-string v1, "scontext"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/scontext/SContextManager;

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->C:Landroid/hardware/scontext/SContextManager;

    invoke-static {}, Lcom/samsung/location/lib/h;->a()Lcom/samsung/location/lib/h;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/SContentManager;->x:Lcom/samsung/location/lib/h;

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->w:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_providers_allowed"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/location/lib/SContentManager;->W:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    invoke-direct {p0}, Lcom/samsung/location/lib/SContentManager;->k()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->H:Z

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->C:Landroid/hardware/scontext/SContextManager;

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Landroid/hardware/scontext/SContextManager;->isAvailableService(I)Z

    move-result v0

    if-nez v0, :cond_0

    iput v4, p0, Lcom/samsung/location/lib/SContentManager;->t:I

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->C:Landroid/hardware/scontext/SContextManager;

    const/16 v1, 0x1a

    invoke-virtual {v0, v1}, Landroid/hardware/scontext/SContextManager;->isAvailableService(I)Z

    move-result v0

    if-nez v0, :cond_1

    iput v4, p0, Lcom/samsung/location/lib/SContentManager;->u:I

    :cond_1
    return-void
.end method

.method public a(II)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->H:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget v0, p0, Lcom/samsung/location/lib/SContentManager;->t:I

    if-ne v0, v5, :cond_0

    const-string v0, "unregisterSensorHub ACTIVITY_NOTIFICATION_EX"

    invoke-static {v0, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->C:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager;->V:Landroid/hardware/scontext/SContextListener;

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    iput v3, p0, Lcom/samsung/location/lib/SContentManager;->t:I

    iput v3, p0, Lcom/samsung/location/lib/SContentManager;->K:I

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    const-string v0, "activity batching is still running"

    invoke-static {v0, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/samsung/location/lib/SContentManager;->u:I

    if-ne v0, v5, :cond_0

    const-string v0, "unregisterSensorHub ACTIVITY_BATCHING"

    invoke-static {v0, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->C:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager;->V:Landroid/hardware/scontext/SContextListener;

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    iput v3, p0, Lcom/samsung/location/lib/SContentManager;->u:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/location/lib/SContentManager;->M:J

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/location/Location;)V
    .locals 8

    const/4 v7, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->O:Landroid/location/Location;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->O:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->O:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->O:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_7

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const-string v1, "network"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->Q:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->z:Lcom/samsung/location/a/c;

    invoke-virtual {v0, v4, p1}, Lcom/samsung/location/a/c;->b(ILandroid/location/Location;)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->Q:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->B:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    invoke-virtual {v0, p1}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->handleLocationReported(Landroid/location/Location;)V

    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/location/lib/SContentManager;->N:J

    iput-object p1, p0, Lcom/samsung/location/lib/SContentManager;->O:Landroid/location/Location;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->I:Z

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->J:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->D:Landroid/location/LocationManager;

    const-string v1, "passive"

    const-wide/16 v2, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    iget-object v5, p0, Lcom/samsung/location/lib/SContentManager;->X:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    iput-boolean v6, p0, Lcom/samsung/location/lib/SContentManager;->J:Z

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->z:Lcom/samsung/location/a/c;

    invoke-virtual {v0, v7, p1}, Lcom/samsung/location/a/c;->b(ILandroid/location/Location;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->z:Lcom/samsung/location/a/c;

    invoke-virtual {v0, v5, p1}, Lcom/samsung/location/a/c;->b(ILandroid/location/Location;)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    :cond_8
    :goto_2
    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->z:Lcom/samsung/location/a/c;

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/samsung/location/a/c;->b(ILandroid/location/Location;)V

    :cond_9
    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->B:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    invoke-virtual {v0, p1}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->handleLocationReported(Landroid/location/Location;)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public a(I)Z
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->F:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->G:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a([II)Z
    .locals 6

    const/16 v5, 0x1e

    const/4 v0, 0x1

    const/4 v4, 0x2

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/samsung/location/lib/SContentManager;->H:Z

    if-nez v2, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget v2, p0, Lcom/samsung/location/lib/SContentManager;->t:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    const-string v2, "ar notification ex not supported"

    invoke-static {v2, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :cond_1
    iget v2, p0, Lcom/samsung/location/lib/SContentManager;->t:I

    if-ne v2, v0, :cond_3

    iget v2, p0, Lcom/samsung/location/lib/SContentManager;->K:I

    if-ne p2, v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "already running ex with duration"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager;->C:Landroid/hardware/scontext/SContextManager;

    iget-object v3, p0, Lcom/samsung/location/lib/SContentManager;->V:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v2, v3, v5}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unregister ACTIVITY_NOTIFICATION_EX "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/location/lib/SContentManager;->K:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :cond_3
    iput v0, p0, Lcom/samsung/location/lib/SContentManager;->t:I

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ge v0, v2, :cond_4

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->C:Landroid/hardware/scontext/SContextManager;

    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager;->V:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v2, v5, p1, p2}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I[II)Z

    move-result v0

    :goto_1
    iput p2, p0, Lcom/samsung/location/lib/SContentManager;->K:I

    const-string v2, "register ACTIVITY_NOTIFICATION_EX"

    invoke-static {v2, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_4
    new-instance v0, Landroid/hardware/scontext/SContextActivityNotificationExAttribute;

    invoke-direct {v0, p1, p2}, Landroid/hardware/scontext/SContextActivityNotificationExAttribute;-><init>([II)V

    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager;->C:Landroid/hardware/scontext/SContextManager;

    iget-object v3, p0, Lcom/samsung/location/lib/SContentManager;->V:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v2, v3, v5, v0}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;ILandroid/hardware/scontext/SContextAttribute;)Z

    move-result v0

    goto :goto_1
.end method

.method public b()Z
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->v:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    if-nez v1, :cond_0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "No Active Data Connection"

    const/4 v2, 0x4

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_0
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Network Info : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public b(I)Z
    .locals 6

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/samsung/location/lib/SContentManager;->H:Z

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v2, p0, Lcom/samsung/location/lib/SContentManager;->u:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    const-string v2, "ar notification ex not supported"

    invoke-static {v2, v5, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :cond_1
    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "activity batching already running with"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v5, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v2, p0, Lcom/samsung/location/lib/SContentManager;->u:I

    if-ne v2, v1, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "activity batching already running with"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v5, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager;->C:Landroid/hardware/scontext/SContextManager;

    iget-object v3, p0, Lcom/samsung/location/lib/SContentManager;->V:Landroid/hardware/scontext/SContextListener;

    const/16 v4, 0x1a

    invoke-virtual {v2, v3, v4}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "register ACTIVITY_BATCHING with "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v5, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iput v1, p0, Lcom/samsung/location/lib/SContentManager;->u:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/location/lib/SContentManager;->M:J

    if-nez v2, :cond_4

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_4
    move v0, v2

    goto/16 :goto_0
.end method

.method public c()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->H:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/location/lib/SContentManager;->u:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    const-string v0, "requestToUpdateSensorHub"

    invoke-static {v0, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/location/lib/SContentManager;->L:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    const-string v0, "last batched event still available"

    invoke-static {v0, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->x:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager;->x:Lcom/samsung/location/lib/h;

    const/16 v2, 0x1f7

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->C:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager;->V:Landroid/hardware/scontext/SContextListener;

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->requestToUpdate(Landroid/hardware/scontext/SContextListener;I)V

    goto :goto_0
.end method

.method public c(I)Z
    .locals 7

    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v6, 0x2

    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-boolean v2, p0, Lcom/samsung/location/lib/SContentManager;->I:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager;->Q:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager;->Q:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    const-string v2, "location already running"

    invoke-static {v2, v6, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :cond_2
    :goto_0
    return v0

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/samsung/location/lib/SContentManager;->N:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7530

    cmp-long v2, v2, v4

    if-gez v2, :cond_4

    const-string v2, "last active location still available"

    invoke-static {v2, v6, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager;->O:Landroid/location/Location;

    invoke-virtual {p0, v1}, Lcom/samsung/location/lib/SContentManager;->a(Landroid/location/Location;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/samsung/location/lib/SContentManager;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lcom/samsung/location/lib/SContentManager;->G:Z

    if-nez v2, :cond_8

    :cond_5
    const-string v2, "NLP is unavailable"

    invoke-static {v2, v6, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    if-ne p1, v6, :cond_7

    iget-boolean v2, p0, Lcom/samsung/location/lib/SContentManager;->F:Z

    if-nez v2, :cond_6

    const-string v2, "GPS is unavailable"

    invoke-static {v2, v6, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v1

    goto :goto_0

    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestLocationUpdates("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") : GPS_PROVIDER"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v6, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-direct {p0, v0}, Lcom/samsung/location/lib/SContentManager;->f(I)V

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager;->Q:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager;->Q:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager;->Q:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/samsung/location/lib/SContentManager;->Q:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestLocationUpdates("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") : NETWORK_PROVIDER"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v6, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-direct {p0, v6}, Lcom/samsung/location/lib/SContentManager;->f(I)V

    goto/16 :goto_0
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->z:Lcom/samsung/location/a/c;

    invoke-virtual {v0}, Lcom/samsung/location/a/c;->k()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->P:Ljava/util/List;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->B:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->handleBatchEvent()V

    :cond_1
    return-void
.end method

.method public d(I)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->J:Z

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->D:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager;->X:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    :cond_1
    return-void
.end method

.method public e()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->S:Ljava/util/List;

    return-object v0
.end method

.method public e(I)V
    .locals 6

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->J:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->J:Z

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->D:Landroid/location/LocationManager;

    const-string v1, "passive"

    const-wide/16 v2, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    iget-object v5, p0, Lcom/samsung/location/lib/SContentManager;->X:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    :cond_1
    return-void
.end method

.method public f()J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/location/lib/SContentManager;->M:J

    return-wide v0
.end method

.method public g()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager;->E:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/location/lib/SContentManager;->U:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/location/lib/SContentManager;->U:Ljava/lang/StringBuffer;

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getCurrentMcc got exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->E:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/location/lib/SContentManager;->U:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getCurrentMcc got exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move-object v0, v1

    goto :goto_0
.end method

.method public i()J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/location/lib/SContentManager;->N:J

    return-wide v0
.end method

.method public j()V
    .locals 7

    const/4 v6, 0x1

    const-string v0, "handleLocationTimeout"

    const/4 v1, 0x2

    invoke-static {v0, v1, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->I:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->I:Z

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->Q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-boolean v0, p0, Lcom/samsung/location/lib/SContentManager;->J:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->R:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/SContentManager;->D:Landroid/location/LocationManager;

    const-string v1, "passive"

    const-wide/16 v2, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    iget-object v5, p0, Lcom/samsung/location/lib/SContentManager;->X:Landroid/location/LocationListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    iput-boolean v6, p0, Lcom/samsung/location/lib/SContentManager;->J:Z

    :cond_0
    return-void
.end method
