.class public Lcom/samsung/location/lib/d;
.super Ljava/lang/Object;


# instance fields
.field private a:J

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(JII)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/location/lib/d;->a:J

    iput v2, p0, Lcom/samsung/location/lib/d;->b:I

    iput v2, p0, Lcom/samsung/location/lib/d;->c:I

    iput-wide p1, p0, Lcom/samsung/location/lib/d;->a:J

    iput p3, p0, Lcom/samsung/location/lib/d;->b:I

    iput p4, p0, Lcom/samsung/location/lib/d;->c:I

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/location/lib/d;->a:J

    return-wide v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/lib/d;->b:I

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/lib/d;->c:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string v1, ""

    const-string v0, ""

    iget v0, p0, Lcom/samsung/location/lib/d;->b:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "unknown"

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "time : "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/location/lib/d;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " status : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " accuracy : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/location/lib/d;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_0
    const-string v0, "ACTIVITY_STATUS_STATIONARY"

    goto :goto_0

    :pswitch_1
    const-string v0, "ACTIVITY_STATUS_VEHICLE"

    goto :goto_0

    :pswitch_2
    const-string v0, "ACTIVITY_STATUS_RUN"

    goto :goto_0

    :pswitch_3
    const-string v0, "ACTIVITY_STATUS_WALK"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
