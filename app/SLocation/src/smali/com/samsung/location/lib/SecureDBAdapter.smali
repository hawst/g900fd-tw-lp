.class public Lcom/samsung/location/lib/SecureDBAdapter;
.super Ljava/lang/Object;


# static fields
.field private static A:Z = false

.field private static final a:Ljava/lang/String; = "slocation.db"

.field private static final b:Ljava/lang/String; = "/data/system"

.field private static final c:I = 0x8

.field private static final d:I = 0x1

.field private static final e:I = 0x2

.field private static final f:I = 0x3

.field private static final g:I = 0x4

.field private static final h:I = 0x5

.field private static final i:I = 0x6

.field private static final j:I = 0x7

.field private static final k:I = 0x8

.field private static final l:I = 0x65

.field private static final m:I = 0x66

.field private static final n:I = 0x67

.field private static final o:I = 0x68

.field private static final p:I = 0xc9

.field private static final q:I = 0xca

.field private static final r:I = 0xcb

.field private static final s:I = 0xcc

.field private static final t:I = 0xcd

.field private static final u:Ljava/lang/String; = "g"

.field private static final v:Ljava/lang/String; = "w"

.field private static final w:Ljava/lang/String; = "c"

.field private static final x:Ljava/lang/String; = "u"

.field private static z:Z


# instance fields
.field private B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

.field private C:Landroid/content/Context;

.field private D:Lcom/samsung/location/lib/NativeManager;

.field private y:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/location/lib/SecureDBAdapter;->z:Z

    sput-boolean v0, Lcom/samsung/location/lib/SecureDBAdapter;->A:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->y:Ljava/lang/Object;

    iput-object p1, p0, Lcom/samsung/location/lib/SecureDBAdapter;->C:Landroid/content/Context;

    sput-boolean p2, Lcom/samsung/location/lib/SecureDBAdapter;->A:Z

    return-void
.end method

.method static synthetic a(Lcom/samsung/location/lib/SecureDBAdapter;)Lcom/samsung/location/lib/NativeManager;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->D:Lcom/samsung/location/lib/NativeManager;

    return-object v0
.end method

.method static synthetic a(Lcom/samsung/location/lib/SecureDBAdapter;Lcom/samsung/location/lib/NativeManager;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/lib/SecureDBAdapter;->D:Lcom/samsung/location/lib/NativeManager;

    return-void
.end method

.method static synthetic b(Lcom/samsung/location/lib/SecureDBAdapter;)Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    return-object v0
.end method

.method static synthetic e()Z
    .locals 1

    sget-boolean v0, Lcom/samsung/location/lib/SecureDBAdapter;->A:Z

    return v0
.end method


# virtual methods
.method public a(I)I
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->c(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCellDBexist got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;IZ)Lcom/samsung/location/currentloc/c;
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Ljava/lang/String;IZ)Lcom/samsung/location/currentloc/c;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getData got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Ljava/util/List;
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->c(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getSGeofenceIdListForPackage got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(II)V
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "putSGeofenceCellDBInfo got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public a(ILcom/samsung/location/a/e;)V
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(ILcom/samsung/location/a/e;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "putSGeofenceGeoDataInfo got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public a(ILjava/lang/String;)V
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateCurrentMcc got exception. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public a(ILjava/util/List;I)V
    .locals 4

    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    if-ne p3, v3, :cond_1

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(ILjava/util/List;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b(ILjava/util/List;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "putSGeofenceWifiListInfo got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public a(Lcom/samsung/location/currentloc/c;)V
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    const-string v1, "u"

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x3e8

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Lcom/samsung/location/currentloc/c;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b(Lcom/samsung/location/currentloc/c;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "putSCurrentLocationInfo got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public a()Z
    .locals 6

    const/4 v5, 0x5

    const/4 v2, 0x0

    const/4 v1, 0x1

    sget-boolean v0, Lcom/samsung/location/lib/SecureDBAdapter;->z:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    sget-boolean v0, Lcom/samsung/location/lib/SecureDBAdapter;->A:Z

    if-eqz v0, :cond_3

    :try_start_0
    new-instance v0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    iget-object v3, p0, Lcom/samsung/location/lib/SecureDBAdapter;->C:Landroid/content/Context;

    invoke-direct {v0, p0, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;-><init>(Lcom/samsung/location/lib/SecureDBAdapter;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b()Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DB open error "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->c()V

    goto :goto_1

    :cond_2
    sput-boolean v1, Lcom/samsung/location/lib/SecureDBAdapter;->z:Z

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->C:Landroid/content/Context;

    const-string v3, "wifi"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fail to get address : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v2

    goto :goto_0

    :cond_4
    const-string v0, "initialize"

    const/4 v3, 0x2

    invoke-static {v0, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :try_start_1
    new-instance v0, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    iget-object v3, p0, Lcom/samsung/location/lib/SecureDBAdapter;->C:Landroid/content/Context;

    invoke-direct {v0, p0, v3}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;-><init>(Lcom/samsung/location/lib/SecureDBAdapter;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a()Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    if-nez v0, :cond_5

    move v0, v2

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DB open error "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->c()V

    goto :goto_2

    :cond_5
    sput-boolean v1, Lcom/samsung/location/lib/SecureDBAdapter;->z:Z

    move v0, v1

    goto/16 :goto_0
.end method

.method public b(I)Ljava/lang/String;
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getSGeoMccExist got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    const-string v1, "g"

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getSGeofenceIdList got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(ILjava/lang/String;)V
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->b(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateGeofencePackage got exception. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public c()I
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    const-string v1, "g"

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getSGeoCount got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public c(I)Lcom/samsung/location/a/e;
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-static {v0, p1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;I)Lcom/samsung/location/a/e;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getGeoData got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(I)Ljava/util/List;
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d(I)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getWifiList got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->d()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "clearLocationData got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(I)Z
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/lib/SecureDBAdapter;->a()Z

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->a(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteSGeofenceData got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected finalize()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/lib/SecureDBAdapter;->B:Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SecureDBAdapter$DBAdapter;->c()V

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void
.end method
