.class public Lcom/samsung/location/currentloc/SCurrentLocationManager;
.super Ljava/lang/Object;


# static fields
.field public static final PROVIDER_SLOCATION:Ljava/lang/String; = "slocation"

.field private static TAG:Ljava/lang/String;

.field private static currentLocManager:Lcom/samsung/location/currentloc/SCurrentLocationManager;

.field private static mDebug:Z


# instance fields
.field private cellDBCount:I

.field private celllocation:Landroid/telephony/CellLocation;

.field private gpsCount:I

.field private idRunningList:Ljava/util/List;

.field isVehicleStarted:Z

.field private lastLocationCount:I

.field private final mContext:Landroid/content/Context;

.field private mLastLocation:Lcom/samsung/location/currentloc/c;

.field private mLastLocationAccuracy:F

.field private mLastLocationTime:J

.field private mLock:Ljava/lang/Object;

.field private mSHandler:Lcom/samsung/location/lib/h;

.field private nlpCount:I

.field private scrm:Lcom/samsung/location/currentloc/e;

.field private sctm:Lcom/samsung/location/lib/SContentManager;

.field private sdbm:Lcom/samsung/location/lib/j;

.field private telm:Landroid/telephony/TelephonyManager;

.field vehicle_start_time:J

.field vehicle_stop_time:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->currentLocManager:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    const-string v0, "SCurrentLocationManager"

    sput-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mDebug:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->idRunningList:Ljava/util/List;

    iput v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->gpsCount:I

    iput v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->nlpCount:I

    iput v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->cellDBCount:I

    iput v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->lastLocationCount:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLock:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->isVehicleStarted:Z

    iput-wide v2, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->vehicle_start_time:J

    iput-wide v2, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->vehicle_stop_time:J

    iput-wide v2, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationTime:J

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationAccuracy:F

    new-instance v0, Lcom/samsung/location/currentloc/c;

    invoke-direct {v0}, Lcom/samsung/location/currentloc/c;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    iput-object p1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method private convertEventToMeters(IJ)F
    .locals 5

    const/4 v0, 0x0

    long-to-double v1, p2

    const-wide v3, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v1, v3

    double-to-float v1, v1

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    mul-float/2addr v0, v1

    sget-object v1, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "time "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " status "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " result "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    return v0

    :pswitch_1
    const/high16 v0, 0x3fc00000    # 1.5f

    goto :goto_0

    :pswitch_2
    const/high16 v0, 0x40400000    # 3.0f

    goto :goto_0

    :pswitch_3
    const/high16 v0, 0x41f00000    # 30.0f

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private convertNetworkToAccuracy(I)I
    .locals 1

    const/16 v0, 0x3e8

    sparse-switch p1, :sswitch_data_0

    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    const/16 v0, 0x2710

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0xd -> :sswitch_0
        0x10 -> :sswitch_1
    .end sparse-switch
.end method

.method private getCellLocation()I
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->telm:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->celllocation:Landroid/telephony/CellLocation;

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->celllocation:Landroid/telephony/CellLocation;

    instance-of v0, v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->celllocation:Landroid/telephony/CellLocation;

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->celllocation:Landroid/telephony/CellLocation;

    instance-of v0, v0, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->celllocation:Landroid/telephony/CellLocation;

    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getCurrentMcc got exception"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v2, v0, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private getCurrentLac()I
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->telm:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->celllocation:Landroid/telephony/CellLocation;

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->celllocation:Landroid/telephony/CellLocation;

    instance-of v0, v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->celllocation:Landroid/telephony/CellLocation;

    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->celllocation:Landroid/telephony/CellLocation;

    instance-of v0, v0, Landroid/telephony/cdma/CdmaCellLocation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->celllocation:Landroid/telephony/CellLocation;

    check-cast v0, Landroid/telephony/cdma/CdmaCellLocation;

    invoke-virtual {v0}, Landroid/telephony/cdma/CdmaCellLocation;->getBaseStationId()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getCurrentMcc got exception"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v2, v0, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/location/currentloc/SCurrentLocationManager;
    .locals 4

    const/4 v3, 0x2

    sget v0, Lcom/samsung/location/SLocationService;->feature_level:I

    if-ge v0, v3, :cond_0

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "slocation feature_level < 2"

    const/4 v2, 0x1

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "getInstance"

    const/4 v2, 0x0

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->currentLocManager:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    if-nez v0, :cond_1

    new-instance v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;

    invoke-direct {v0, p0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->currentLocManager:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    :cond_1
    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->currentLocManager:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;Z)Lcom/samsung/location/currentloc/SCurrentLocationManager;
    .locals 4

    const/4 v3, 0x2

    sget v0, Lcom/samsung/location/SLocationService;->feature_level:I

    if-ge v0, v3, :cond_0

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "slocation feature_level < 2"

    const/4 v2, 0x1

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getInstance debug:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    sput-boolean p1, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mDebug:Z

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->currentLocManager:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    if-nez v0, :cond_1

    new-instance v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;

    invoke-direct {v0, p0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->currentLocManager:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    :cond_1
    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->currentLocManager:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    goto :goto_0
.end method

.method private getPassiveLocation(I)Landroid/location/Location;
    .locals 7

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v0, 0x0

    iget v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationAccuracy:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    iget v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationAccuracy:F

    int-to-float v2, p1

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "last location is available"

    invoke-static {v0, v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/c;->c()Landroid/location/Location;

    move-result-object v0

    iget v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationAccuracy:F

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    sget-boolean v1, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mDebug:Z

    if-eqz v1, :cond_0

    const-string v1, "slocation_lastLocation"

    invoke-virtual {v0, v1}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    :cond_0
    iget v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->lastLocationCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->lastLocationCount:I

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->telm:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->convertNetworkToAccuracy(I)I

    move-result v1

    if-lt p1, v1, :cond_6

    sget-object v2, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v3, "accuracy may be rough"

    invoke-static {v2, v3, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-direct {p0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->getCellLocation()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    invoke-virtual {v3}, Lcom/samsung/location/currentloc/c;->b()Lcom/samsung/location/currentloc/d;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    invoke-virtual {v3}, Lcom/samsung/location/currentloc/c;->b()Lcom/samsung/location/currentloc/d;

    move-result-object v3

    iget v3, v3, Lcom/samsung/location/currentloc/d;->d:I

    if-ne v3, v2, :cond_4

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v2, "last id still available"

    invoke-static {v0, v2, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/c;->c()Landroid/location/Location;

    move-result-object v0

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    sget-boolean v1, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mDebug:Z

    if-eqz v1, :cond_3

    const-string v1, "slocation_lastLocation_cell"

    invoke-virtual {v0, v1}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    :cond_3
    iget v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->lastLocationCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->lastLocationCount:I

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sdbm:Lcom/samsung/location/lib/j;

    iget-object v3, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sctm:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v3}, Lcom/samsung/location/lib/SContentManager;->g()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->getCellLocation()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/location/lib/j;->b(Ljava/lang/String;I)Lcom/samsung/location/currentloc/c;

    move-result-object v2

    sget-object v3, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getPassiveLocation : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/samsung/location/currentloc/c;->c()Landroid/location/Location;

    move-result-object v0

    sget-boolean v1, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mDebug:Z

    if-eqz v1, :cond_5

    const-string v1, "slocation_cellDB"

    invoke-virtual {v0, v1}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    :cond_5
    iget v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->cellDBCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->cellDBCount:I

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    sget-object v1, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v2, "accuracy should be high"

    invoke-static {v1, v2, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto/16 :goto_0
.end method

.method private registerSensorHub()Z
    .locals 2

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sctm:Lcom/samsung/location/lib/SContentManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/SContentManager;->b(I)Z

    move-result v0

    return v0
.end method

.method private startPositioning()V
    .locals 4

    const/4 v3, 0x2

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "startPositioning"

    const/4 v2, 0x0

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sctm:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0, v3}, Lcom/samsung/location/lib/SContentManager;->c(I)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "startPositioning is failed"

    const/4 v2, 0x1

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mSHandler:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mSHandler:Lcom/samsung/location/lib/h;

    const/16 v2, 0x6f

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f40

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/location/lib/h;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method private unregisterSensorHub()V
    .locals 2

    const/4 v1, 0x2

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sctm:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0, v1, v1}, Lcom/samsung/location/lib/SContentManager;->a(II)V

    return-void
.end method

.method private updateLastLocationAccuracy(Ljava/util/List;)V
    .locals 13

    const-wide/16 v3, 0x0

    const/4 v12, 0x2

    const/4 v6, 0x0

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "updateLastLocationAccuracy"

    invoke-static {v0, v1, v12, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_0

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "batched event is empty"

    invoke-static {v0, v1, v12, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :goto_0
    return-void

    :cond_0
    move v5, v6

    :goto_1
    if-lt v5, v7, :cond_1

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "last location acc "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationAccuracy:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v12, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_1
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/lib/d;

    invoke-virtual {v0}, Lcom/samsung/location/lib/d;->a()J

    move-result-wide v8

    sget-object v1, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v10, "status "

    invoke-direct {v2, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/location/lib/d;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v12, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    add-int/lit8 v1, v5, 0x1

    if-ge v1, v7, :cond_6

    add-int/lit8 v1, v5, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/location/lib/d;

    invoke-virtual {v1}, Lcom/samsung/location/lib/d;->a()J

    move-result-wide v1

    :goto_2
    iget-wide v10, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationTime:J

    cmp-long v10, v8, v10

    if-gez v10, :cond_4

    cmp-long v8, v1, v3

    if-eqz v8, :cond_3

    iget-wide v8, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationTime:J

    cmp-long v8, v1, v8

    if-gez v8, :cond_2

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "skip previous event"

    invoke-static {v0, v1, v12, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :goto_3
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    :cond_2
    iget-wide v8, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationTime:J

    sub-long/2addr v1, v8

    sget-object v8, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v9, "timeDiff = nextEventTime - mLastLocationTime"

    invoke-static {v8, v9, v12, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :goto_4
    iget v8, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationAccuracy:F

    invoke-virtual {v0}, Lcom/samsung/location/lib/d;->b()I

    move-result v0

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->convertEventToMeters(IJ)F

    move-result v0

    add-float/2addr v0, v8

    iput v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationAccuracy:F

    goto :goto_3

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v8, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationTime:J

    sub-long/2addr v1, v8

    sget-object v8, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v9, "timeDiff = System.currentTimeMillis() - mLastLocationTime"

    invoke-static {v8, v9, v12, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_4

    :cond_4
    cmp-long v10, v1, v3

    if-eqz v10, :cond_5

    sub-long/2addr v1, v8

    sget-object v8, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v9, "timeDiff = nextEventTime - eventTime"

    invoke-static {v8, v9, v12, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_4

    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sub-long/2addr v1, v8

    sget-object v8, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v9, "timeDiff = System.currentTimeMillis() - eventTime"

    invoke-static {v8, v9, v12, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_4

    :cond_6
    move-wide v1, v3

    goto :goto_2
.end method


# virtual methods
.method public addSCurrentLocationDB(Lcom/samsung/location/currentloc/c;)V
    .locals 4

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addSCurrentLocationDB "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sdbm:Lcom/samsung/location/lib/j;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/j;->a(Lcom/samsung/location/currentloc/c;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public checkMccTable()Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    const/4 v2, 0x0

    move v0, v1

    :goto_0
    sget-object v3, Lcom/samsung/location/currentloc/b;->a:[[Ljava/lang/String;

    array-length v3, v3

    if-lt v0, v3, :cond_0

    move-object v0, v2

    :goto_1
    return-object v0

    :cond_0
    sget-object v3, Lcom/samsung/location/currentloc/b;->a:[[Ljava/lang/String;

    aget-object v3, v3, v0

    aget-object v3, v3, v1

    iget-object v4, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sctm:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v4}, Lcom/samsung/location/lib/SContentManager;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v1, Lcom/samsung/location/currentloc/b;->a:[[Ljava/lang/String;

    aget-object v0, v1, v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public checkPassiveLocation()Z
    .locals 4

    const/4 v3, 0x2

    const/4 v0, 0x0

    sget-object v1, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v2, "checkPassiveLocation"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const v1, 0x30d40

    invoke-direct {p0, v1}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->getPassiveLocation(I)Landroid/location/Location;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v2, "There\'s no available location"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 2

    sget-boolean v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mDebug:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SLocation provider count - GPS : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->gpsCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " NLP : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->nlpCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " CellDB : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->cellDBCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LastLocation : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->lastLocationCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getCurrentLocation(IILandroid/app/PendingIntent;)I
    .locals 9

    const v8, 0x30d40

    const/16 v0, -0x64

    const/4 v4, 0x2

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v3, "getCurrentLocation"

    invoke-static {v2, v3, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    :try_start_0
    iget-object v4, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    invoke-virtual {v4, p1, p3}, Lcom/samsung/location/currentloc/e;->a(ILandroid/app/PendingIntent;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationAccuracy:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    sget-object v4, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v5, "Last Location is available. update accuracy"

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v4, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sctm:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v4}, Lcom/samsung/location/lib/SContentManager;->c()V

    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->getPassiveLocation(I)Landroid/location/Location;

    move-result-object v4

    if-nez v4, :cond_3

    invoke-virtual {p3}, Landroid/app/PendingIntent;->getCreatorPackage()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.sec.android.app.shealth"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-ne p1, v8, :cond_1

    if-ne p2, v0, :cond_1

    sget-object v1, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v4, "UV location - check passive fail"

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static {v1, v4, v5, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    invoke-virtual {v1, p3}, Lcom/samsung/location/currentloc/e;->a(Landroid/app/PendingIntent;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :goto_0
    return v0

    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->startPositioning()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_1
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    move v0, v1

    goto :goto_0

    :cond_3
    :try_start_2
    invoke-virtual {p3}, Landroid/app/PendingIntent;->getCreatorPackage()Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.sec.android.app.shealth"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    if-ne p1, v8, :cond_4

    if-ne p2, v0, :cond_4

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v4, "UV location - check passive pass"

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static {v0, v4, v5, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    invoke-virtual {v0, p3}, Lcom/samsung/location/currentloc/e;->a(Landroid/app/PendingIntent;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    move v0, v1

    goto :goto_0

    :cond_4
    :try_start_3
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    invoke-virtual {v0, v4}, Lcom/samsung/location/currentloc/e;->a(Landroid/location/Location;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_4
    sget-object v1, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "requestCurrentLocation got exception"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-static {v1, v0, v4, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    invoke-virtual {v0, p3}, Lcom/samsung/location/currentloc/e;->a(Landroid/app/PendingIntent;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    const/4 v0, -0x4

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public handleBatchEvent()V
    .locals 4

    iget v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationAccuracy:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "Last Location is empty"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sctm:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->e()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->updateLastLocationAccuracy(Ljava/util/List;)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public handleLocationReported(Landroid/location/Location;)V
    .locals 9

    const/16 v8, 0x6f

    const/4 v7, 0x0

    const/4 v6, 0x2

    new-instance v5, Landroid/location/Location;

    invoke-direct {v5, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    if-eqz v5, :cond_0

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "handleLocationReported"

    invoke-static {v0, v1, v6, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/c;->c()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/c;->c()Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v5}, Landroid/location/Location;->getTime()J

    move-result-wide v1

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/32 v3, 0xea60

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mSHandler:Lcom/samsung/location/lib/h;

    invoke-virtual {v1, v8}, Lcom/samsung/location/lib/h;->hasMessages(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    cmpl-double v1, v1, v3

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mSHandler:Lcom/samsung/location/lib/h;

    invoke-virtual {v0, v8}, Lcom/samsung/location/lib/h;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mSHandler:Lcom/samsung/location/lib/h;

    invoke-virtual {v0, v8}, Lcom/samsung/location/lib/h;->removeMessages(I)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->idRunningList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    const-string v0, "slocation"

    invoke-virtual {v5, v0}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    invoke-virtual {v0, v5}, Lcom/samsung/location/currentloc/e;->b(Landroid/location/Location;)V

    :cond_4
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/e;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_7

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "reportLocation"

    invoke-static {v0, v1, v6, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    sget-boolean v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mDebug:Z

    if-eqz v0, :cond_9

    invoke-virtual {v5}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->gpsCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->gpsCount:I

    :cond_5
    invoke-virtual {v5}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->nlpCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->nlpCount:I

    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    invoke-virtual {v0, v5}, Lcom/samsung/location/currentloc/e;->a(Landroid/location/Location;)V

    :cond_7
    invoke-direct {p0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->registerSensorHub()Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "registerSensorHub"

    invoke-static {v0, v1, v6, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :cond_8
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sctm:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->h()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->getCellLocation()I

    move-result v4

    new-instance v0, Lcom/samsung/location/currentloc/c;

    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->telm:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    invoke-direct {p0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->getCurrentLac()I

    move-result v3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/location/currentloc/c;-><init>(ILjava/lang/String;IILandroid/location/Location;)V

    iput-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    invoke-virtual {v5}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    iput v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationAccuracy:F

    invoke-virtual {v5}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationTime:J

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->telm:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/currentloc/c;->b(I)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    invoke-virtual {v0, v4}, Lcom/samsung/location/currentloc/c;->a(I)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    invoke-virtual {p0, v0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->addSCurrentLocationDB(Lcom/samsung/location/currentloc/c;)V

    goto/16 :goto_0

    :cond_9
    const-string v0, "slocation"

    invoke-virtual {v5, v0}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public handleLocationTimeout()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "handleLocationTimeout"

    invoke-static {v0, v1, v4, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {p0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->checkMccTable()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v1, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v2, "checkMccTable exists."

    invoke-static {v1, v2, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/location/Location;

    const-string v2, "slocation"

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    aget-object v2, v0, v3

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    aget-object v0, v0, v5

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->idRunningList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "slocation"

    invoke-virtual {v1, v0}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    invoke-virtual {v0, v1}, Lcom/samsung/location/currentloc/e;->b(Landroid/location/Location;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "There\'s no available location in mcc table"

    invoke-static {v0, v1, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public handlePackageRemoved()V
    .locals 4

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "handlePackageRemoved"

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    return-void
.end method

.method public handleProvderStatusChanged(IZ)V
    .locals 4

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleProvderStatusChanged source :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  OnOff : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    return-void
.end method

.method public handleSCurLocAirPlaneOn()V
    .locals 4

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "handleSCurLocAirPlaneOn"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-direct {p0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->unregisterSensorHub()V

    return-void
.end method

.method public handleSCurLocScreenOn()V
    .locals 4

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "handleSCurLocScreenOn"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-direct {p0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->getCellLocation()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    invoke-virtual {v1}, Lcom/samsung/location/currentloc/c;->b()Lcom/samsung/location/currentloc/d;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    invoke-virtual {v1}, Lcom/samsung/location/currentloc/c;->b()Lcom/samsung/location/currentloc/d;

    move-result-object v1

    iget v1, v1, Lcom/samsung/location/currentloc/d;->d:I

    if-ne v1, v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sctm:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->c()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sdbm:Lcom/samsung/location/lib/j;

    iget-object v3, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sctm:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v3}, Lcom/samsung/location/lib/SContentManager;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/samsung/location/lib/j;->b(Ljava/lang/String;I)Lcom/samsung/location/currentloc/c;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->telm:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v1

    iput-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocation:Lcom/samsung/location/currentloc/c;

    invoke-virtual {v0, v1}, Lcom/samsung/location/currentloc/c;->b(I)V

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mLastLocationAccuracy:F

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public initialize()V
    .locals 5

    const/4 v4, 0x2

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->telm:Landroid/telephony/TelephonyManager;

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->a(Landroid/content/Context;)Lcom/samsung/location/lib/SContentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sctm:Lcom/samsung/location/lib/SContentManager;

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/location/currentloc/e;->a(Landroid/content/Context;)Lcom/samsung/location/currentloc/e;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    invoke-static {}, Lcom/samsung/location/lib/j;->a()Lcom/samsung/location/lib/j;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sdbm:Lcom/samsung/location/lib/j;

    invoke-static {}, Lcom/samsung/location/lib/h;->a()Lcom/samsung/location/lib/h;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mSHandler:Lcom/samsung/location/lib/h;

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mSHandler:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->mSHandler:Lcom/samsung/location/lib/h;

    const/16 v2, 0x70

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "SCurrentLocationManager initialized"

    const/4 v2, 0x1

    invoke-static {v0, v1, v4, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    return-void
.end method

.method public removeCurrentLocation(I)I
    .locals 7

    const/4 v2, 0x2

    const/4 v6, 0x0

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "removeCurrentLocation"

    invoke-static {v0, v1, v2, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    invoke-virtual {v0, p1}, Lcom/samsung/location/currentloc/e;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->idRunningList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->idRunningList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :goto_0
    return v6

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v3, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "removetCurrentLocation got exception"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-static {v3, v0, v4, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public requestCurrentLocation(Lcom/samsung/location/ISCurrentLocListener;)I
    .locals 8

    const/4 v3, 0x0

    const/4 v2, 0x2

    sget-object v0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "requestCurrentLocation"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/4 v1, -0x1

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    iget-object v4, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->idRunningList:Ljava/util/List;

    invoke-virtual {v0, v4}, Lcom/samsung/location/currentloc/e;->a(Ljava/util/List;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :try_start_1
    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    invoke-virtual {v1, v0, p1}, Lcom/samsung/location/currentloc/e;->a(ILcom/samsung/location/ISCurrentLocListener;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->idRunningList:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->registerSensorHub()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    const-string v4, "registerSensorHub"

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static {v1, v4, v5, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :cond_0
    iget-object v1, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->sctm:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v1}, Lcom/samsung/location/lib/SContentManager;->c()V

    const v1, 0x30d40

    invoke-direct {p0, v1}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->getPassiveLocation(I)Landroid/location/Location;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->startPositioning()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :goto_1
    return v0

    :cond_2
    :try_start_2
    iget-object v4, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->idRunningList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    const-string v4, "slocation"

    invoke-virtual {v1, v4}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    invoke-virtual {v4, v1}, Lcom/samsung/location/currentloc/e;->b(Landroid/location/Location;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v7, v1

    move v1, v0

    move-object v0, v7

    :goto_2
    :try_start_3
    sget-object v4, Lcom/samsung/location/currentloc/SCurrentLocationManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "requestCurrentLocation got exception"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x2

    const/4 v6, 0x1

    invoke-static {v4, v0, v5, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    if-lez v1, :cond_3

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->scrm:Lcom/samsung/location/currentloc/e;

    invoke-virtual {v0, v1}, Lcom/samsung/location/currentloc/e;->a(I)Z

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->idRunningList:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/location/currentloc/SCurrentLocationManager;->idRunningList:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    const/4 v0, -0x4

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0

    :catch_1
    move-exception v0

    goto :goto_2
.end method
