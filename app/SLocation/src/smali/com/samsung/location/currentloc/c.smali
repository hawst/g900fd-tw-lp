.class public Lcom/samsung/location/currentloc/c;
.super Ljava/lang/Object;


# static fields
.field private static final a:I = 0x0

.field private static final b:I = -0x1

.field private static final c:I = -0x2

.field private static final d:I = -0x3

.field private static final e:I = -0x4

.field private static final f:I = -0x5

.field private static final g:I = -0x6


# instance fields
.field private h:Landroid/location/Location;

.field private i:Lcom/samsung/location/currentloc/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    iput-object v0, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    return-void
.end method

.method public constructor <init>(DD)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    iput-object v0, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    new-instance v0, Landroid/location/Location;

    const-string v1, "passive"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    iget-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    invoke-virtual {v0, p1, p2}, Landroid/location/Location;->setLatitude(D)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    invoke-virtual {v0, p3, p4}, Landroid/location/Location;->setLongitude(D)V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;IILandroid/location/Location;)V
    .locals 6

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    iput-object v0, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    new-instance v0, Lcom/samsung/location/currentloc/d;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/location/currentloc/d;-><init>(Lcom/samsung/location/currentloc/c;ILjava/lang/String;II)V

    iput-object v0, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p5}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iput-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    return-void
.end method

.method public constructor <init>(Landroid/location/Location;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    iput-object v0, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iput-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    iget-object v0, v0, Lcom/samsung/location/currentloc/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(D)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    invoke-virtual {v0, p1, p2}, Landroid/location/Location;->setLatitude(D)V

    return-void
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    iput p1, v0, Lcom/samsung/location/currentloc/d;->d:I

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    iput-object p1, v0, Lcom/samsung/location/currentloc/d;->b:Ljava/lang/String;

    return-void
.end method

.method public b()Lcom/samsung/location/currentloc/d;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    return-object v0
.end method

.method public b(D)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    invoke-virtual {v0, p1, p2}, Landroid/location/Location;->setLongitude(D)V

    return-void
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    iput p1, v0, Lcom/samsung/location/currentloc/d;->a:I

    return-void
.end method

.method public c()Landroid/location/Location;
    .locals 2

    iget-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    const-string v1, "slocation"

    invoke-virtual {v0, v1}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    return-object v0
.end method

.method public d()I
    .locals 5

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    if-nez v1, :cond_1

    const/4 v0, -0x3

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    iget-object v1, v1, Lcom/samsung/location/currentloc/d;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    iget-object v1, v1, Lcom/samsung/location/currentloc/d;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    iget-object v1, v1, Lcom/samsung/location/currentloc/d;->b:Ljava/lang/String;

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const/4 v0, -0x4

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    iget v1, v1, Lcom/samsung/location/currentloc/d;->d:I

    if-eq v1, v0, :cond_4

    iget-object v1, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    iget v1, v1, Lcom/samsung/location/currentloc/d;->a:I

    if-nez v1, :cond_5

    :cond_4
    const/4 v0, -0x5

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    iget v1, v1, Lcom/samsung/location/currentloc/d;->c:I

    if-nez v1, :cond_6

    const/4 v0, -0x6

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    const-wide v3, 0x4056800000000000L    # 90.0

    cmpl-double v3, v1, v3

    if-gtz v3, :cond_0

    const-wide v3, -0x3fa9800000000000L    # -90.0

    cmpg-double v1, v1, v3

    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    const-wide v2, 0x4066800000000000L    # 180.0

    cmpl-double v2, v0, v2

    if-gtz v2, :cond_7

    const-wide v2, -0x3f99800000000000L    # -180.0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_8

    :cond_7
    const/4 v0, -0x2

    goto :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SCurrentLocationParameter cell type :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    iget v1, v1, Lcom/samsung/location/currentloc/d;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " networkOp :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    iget-object v1, v1, Lcom/samsung/location/currentloc/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cell id :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/currentloc/c;->i:Lcom/samsung/location/currentloc/d;

    iget v1, v1, Lcom/samsung/location/currentloc/d;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lat :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " lon :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/currentloc/c;->h:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
