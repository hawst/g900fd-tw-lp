.class Lcom/samsung/location/currentloc/f;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final synthetic a:Lcom/samsung/location/currentloc/e;

.field private b:Lcom/samsung/location/ISCurrentLocListener;

.field private c:Lcom/samsung/location/currentloc/h;

.field private d:I


# direct methods
.method constructor <init>(Lcom/samsung/location/currentloc/e;I)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/location/currentloc/f;->a:Lcom/samsung/location/currentloc/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/location/currentloc/f;->b:Lcom/samsung/location/ISCurrentLocListener;

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/location/currentloc/f;->d:I

    iput p2, p0, Lcom/samsung/location/currentloc/f;->d:I

    return-void
.end method


# virtual methods
.method public a()Lcom/samsung/location/currentloc/h;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/f;->c:Lcom/samsung/location/currentloc/h;

    return-object v0
.end method

.method public a(Lcom/samsung/location/ISCurrentLocListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/currentloc/f;->b:Lcom/samsung/location/ISCurrentLocListener;

    return-void
.end method

.method public a(Lcom/samsung/location/currentloc/h;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/currentloc/f;->c:Lcom/samsung/location/currentloc/h;

    return-void
.end method

.method public a(Landroid/location/Location;)Z
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/currentloc/f;->b:Lcom/samsung/location/ISCurrentLocListener;

    invoke-interface {v0, p1}, Lcom/samsung/location/ISCurrentLocListener;->onCurrentLocation(Landroid/location/Location;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lcom/samsung/location/ISCurrentLocListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/f;->b:Lcom/samsung/location/ISCurrentLocListener;

    return-object v0
.end method

.method public binderDied()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SCurrent Location listener died : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/location/currentloc/f;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {p0}, Lcom/samsung/location/currentloc/f;->b()Lcom/samsung/location/ISCurrentLocListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/location/ISCurrentLocListener;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    iget-object v0, p0, Lcom/samsung/location/currentloc/f;->c:Lcom/samsung/location/currentloc/h;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/h;->a()V

    iget-object v0, p0, Lcom/samsung/location/currentloc/f;->a:Lcom/samsung/location/currentloc/e;

    iget v1, p0, Lcom/samsung/location/currentloc/f;->d:I

    invoke-static {v0, v1}, Lcom/samsung/location/currentloc/e;->a(Lcom/samsung/location/currentloc/e;I)V

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/currentloc/f;->d:I

    return v0
.end method
