.class Lcom/samsung/location/currentloc/g;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/app/PendingIntent$OnFinished;
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final synthetic a:Lcom/samsung/location/currentloc/e;

.field private b:I

.field private c:Lcom/samsung/location/currentloc/i;

.field private d:Landroid/app/PendingIntent;


# direct methods
.method constructor <init>(Lcom/samsung/location/currentloc/e;ILandroid/app/PendingIntent;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/location/currentloc/g;->a:Lcom/samsung/location/currentloc/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/location/currentloc/g;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/location/currentloc/g;->d:Landroid/app/PendingIntent;

    iput p2, p0, Lcom/samsung/location/currentloc/g;->b:I

    iput-object p3, p0, Lcom/samsung/location/currentloc/g;->d:Landroid/app/PendingIntent;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/currentloc/g;->b:I

    return v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/location/currentloc/g;->b:I

    return-void
.end method

.method public a(Lcom/samsung/location/currentloc/i;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/currentloc/g;->c:Lcom/samsung/location/currentloc/i;

    return-void
.end method

.method public a(Landroid/location/Location;)Z
    .locals 8

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/samsung/location/currentloc/g;->d:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v0, "currentlocation"

    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/currentloc/g;->d:Landroid/app/PendingIntent;

    iget-object v1, p0, Lcom/samsung/location/currentloc/g;->a:Lcom/samsung/location/currentloc/e;

    invoke-static {v1}, Lcom/samsung/location/currentloc/e;->a(Lcom/samsung/location/currentloc/e;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v6

    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onGeofenceDetected got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v7

    goto :goto_0

    :cond_0
    move v0, v7

    goto :goto_0
.end method

.method public b()Landroid/app/PendingIntent;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/g;->d:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public binderDied()V
    .locals 3

    const-string v0, "SCurrentLocation listener died"

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/g;->c:Lcom/samsung/location/currentloc/i;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/i;->a()V

    return-void
.end method

.method public c()Lcom/samsung/location/currentloc/i;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/g;->c:Lcom/samsung/location/currentloc/i;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    move-object v0, p1

    check-cast v0, Lcom/samsung/location/currentloc/g;

    instance-of v1, p1, Lcom/samsung/location/currentloc/g;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/location/currentloc/g;->d:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/g;->b()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSendFinished(Landroid/app/PendingIntent;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PendingIntent onSendFinished intent :"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/g;->a:Lcom/samsung/location/currentloc/e;

    invoke-virtual {v0, p1}, Lcom/samsung/location/currentloc/e;->a(Landroid/app/PendingIntent;)Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, ""

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SCurrentLocationReceiver{mPendingIntent: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/location/currentloc/g;->d:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "mAccuracy: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/location/currentloc/g;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "mUpdateRecord: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/currentloc/g;->c:Lcom/samsung/location/currentloc/i;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
