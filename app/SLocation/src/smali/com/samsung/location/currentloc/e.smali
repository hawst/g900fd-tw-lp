.class public Lcom/samsung/location/currentloc/e;
.super Ljava/lang/Object;


# static fields
.field public static final a:I = 0x1

.field public static final b:I = 0x0

.field public static final c:I = 0x1

.field public static final d:I = 0x2

.field public static final e:I = 0x3

.field public static final f:I = 0x0

.field public static final g:I = 0x1

.field public static final h:I = 0x2

.field public static final i:I = 0x3

.field public static final j:I = 0x4

.field public static final k:I = 0x5

.field public static final l:I = 0x6

.field public static final m:I = 0x7

.field public static final n:I = 0x8

.field public static final o:I = 0x9

.field private static u:Lcom/samsung/location/currentloc/e;


# instance fields
.field private final p:Ljava/util/ArrayList;

.field private final q:Ljava/util/ArrayList;

.field private final r:Ljava/util/ArrayList;

.field private final s:Ljava/util/ArrayList;

.field private t:Lcom/samsung/location/currentloc/a;

.field private final v:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/location/currentloc/e;->u:Lcom/samsung/location/currentloc/e;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/currentloc/e;->p:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/currentloc/e;->q:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/currentloc/e;->r:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/currentloc/e;->s:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/location/currentloc/e;->t:Lcom/samsung/location/currentloc/a;

    iput-object p1, p0, Lcom/samsung/location/currentloc/e;->v:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/samsung/location/currentloc/e;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/e;->v:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/samsung/location/currentloc/e;
    .locals 1

    sget-object v0, Lcom/samsung/location/currentloc/e;->u:Lcom/samsung/location/currentloc/e;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/location/currentloc/e;->u:Lcom/samsung/location/currentloc/e;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/location/currentloc/e;

    invoke-direct {v0, p0}, Lcom/samsung/location/currentloc/e;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/location/currentloc/e;->u:Lcom/samsung/location/currentloc/e;

    sget-object v0, Lcom/samsung/location/currentloc/e;->u:Lcom/samsung/location/currentloc/e;

    goto :goto_0
.end method

.method static synthetic a(Lcom/samsung/location/currentloc/e;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/location/currentloc/e;->b(I)V

    return-void
.end method

.method private a(Lcom/samsung/location/currentloc/f;Lcom/samsung/location/ISCurrentLocListener;)Z
    .locals 6

    const/4 v5, 0x5

    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/f;->b()Lcom/samsung/location/ISCurrentLocListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/location/ISCurrentLocListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {v2, p1, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/samsung/location/currentloc/f;->a(Lcom/samsung/location/ISCurrentLocListener;)V

    const-string v1, "CurrentLocListener set to null"

    invoke-static {v1, v5, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/location/currentloc/f;->b()Lcom/samsung/location/ISCurrentLocListener;

    move-result-object v2

    if-nez v2, :cond_1

    :try_start_0
    invoke-virtual {p1, p2}, Lcom/samsung/location/currentloc/f;->a(Lcom/samsung/location/ISCurrentLocListener;)V

    invoke-virtual {p1}, Lcom/samsung/location/currentloc/f;->b()Lcom/samsung/location/ISCurrentLocListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/location/ISCurrentLocListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, p1, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    const-string v2, "setCurrentLocListener OK"

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "linkToDeath failed:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v5, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v1

    goto :goto_0

    :cond_1
    const-string v2, "setCurrentLocListener Fail : Already other listener is registered"

    invoke-static {v2, v3, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/samsung/location/currentloc/e;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/e;->s:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(I)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/e;->t:Lcom/samsung/location/currentloc/a;

    invoke-interface {v0, p1}, Lcom/samsung/location/currentloc/a;->a(I)V

    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 4

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-gez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/currentloc/f;

    iget-object v2, p0, Lcom/samsung/location/currentloc/e;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/f;->b()Lcom/samsung/location/ISCurrentLocListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/location/ISCurrentLocListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v0, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/f;->a()Lcom/samsung/location/currentloc/h;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/location/currentloc/h;->a()V

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/f;->c()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/location/currentloc/e;->b(I)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/samsung/location/currentloc/e;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/e;->q:Ljava/util/ArrayList;

    return-object v0
.end method

.method private c(Ljava/util/List;)V
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-gez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/currentloc/g;

    iget-object v2, p0, Lcom/samsung/location/currentloc/e;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/g;->c()Lcom/samsung/location/currentloc/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/i;->a()V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/List;)I
    .locals 5

    const/4 v4, 0x5

    const/4 v0, 0x1

    const/4 v3, 0x0

    if-nez p1, :cond_2

    const-string v1, "generateNewID : idArray is empty so return id is 1"

    invoke-static {v1, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "generateNewID : return id is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a()Ljava/util/List;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/currentloc/e;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, v2}, Lcom/samsung/location/currentloc/e;->c(Ljava/util/List;)V

    return-object v1

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/currentloc/i;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/i;->b()Lcom/samsung/location/currentloc/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/g;->b()Landroid/app/PendingIntent;

    move-result-object v4

    if-nez v4, :cond_1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/samsung/location/currentloc/g;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Landroid/location/Location;)V
    .locals 6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    iget-object v0, p0, Lcom/samsung/location/currentloc/e;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/samsung/location/currentloc/e;->c(Ljava/util/List;)V

    return-void

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/currentloc/i;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/i;->b()Lcom/samsung/location/currentloc/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/g;->a()I

    move-result v4

    int-to-float v4, v4

    cmpg-float v4, v4, v2

    if-gez v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " > "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/g;->a()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p1}, Lcom/samsung/location/currentloc/g;->a(Landroid/location/Location;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(I)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x2

    const/4 v2, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "removeCurrentLocReceiver id = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/e;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v0, v1

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Lcom/samsung/location/currentloc/f;->a(Lcom/samsung/location/ISCurrentLocListener;)V

    iget-object v1, p0, Lcom/samsung/location/currentloc/e;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/f;->a()Lcom/samsung/location/currentloc/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/h;->a()V

    const-string v0, "removeCurrentLocReceiver : disposeLocked"

    invoke-static {v0, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v2

    :goto_1
    return v0

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/currentloc/h;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/h;->b()Lcom/samsung/location/currentloc/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/f;->c()I

    move-result v4

    if-ne v4, p1, :cond_0

    goto :goto_0

    :cond_2
    const-string v0, "removeCurrentLocReceiver : receiver is null"

    invoke-static {v0, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(ILandroid/app/PendingIntent;)Z
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const-string v0, "addCurrentLocationReceiver"

    const/4 v1, 0x0

    invoke-static {v0, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/e;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/samsung/location/currentloc/g;

    invoke-direct {v0, p0, p1, p2}, Lcom/samsung/location/currentloc/g;-><init>(Lcom/samsung/location/currentloc/e;ILandroid/app/PendingIntent;)V

    iget-object v1, p0, Lcom/samsung/location/currentloc/e;->r:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/g;->c()Lcom/samsung/location/currentloc/i;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/g;->c()Lcom/samsung/location/currentloc/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/location/currentloc/i;->a()V

    :cond_1
    new-instance v1, Lcom/samsung/location/currentloc/i;

    invoke-direct {v1, p0, v0}, Lcom/samsung/location/currentloc/i;-><init>(Lcom/samsung/location/currentloc/e;Lcom/samsung/location/currentloc/g;)V

    invoke-virtual {v0, v1}, Lcom/samsung/location/currentloc/g;->a(Lcom/samsung/location/currentloc/i;)V

    :goto_0
    return v3

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/currentloc/i;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/i;->b()Lcom/samsung/location/currentloc/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/g;->b()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v1, "already exsit record! update accuracy"

    invoke-static {v1, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {v0, p1}, Lcom/samsung/location/currentloc/g;->a(I)V

    goto :goto_0
.end method

.method public a(ILcom/samsung/location/ISCurrentLocListener;)Z
    .locals 5

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "addCurrentLocReceiver"

    invoke-static {v0, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/currentloc/e;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/samsung/location/currentloc/f;

    invoke-direct {v0, p0, p1}, Lcom/samsung/location/currentloc/f;-><init>(Lcom/samsung/location/currentloc/e;I)V

    iget-object v3, p0, Lcom/samsung/location/currentloc/e;->p:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/f;->a()Lcom/samsung/location/currentloc/h;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/f;->a()Lcom/samsung/location/currentloc/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/location/currentloc/h;->a()V

    :cond_1
    new-instance v3, Lcom/samsung/location/currentloc/h;

    invoke-direct {v3, p0, v0}, Lcom/samsung/location/currentloc/h;-><init>(Lcom/samsung/location/currentloc/e;Lcom/samsung/location/currentloc/f;)V

    invoke-virtual {v0, v3}, Lcom/samsung/location/currentloc/f;->a(Lcom/samsung/location/currentloc/h;)V

    invoke-direct {p0, v0, p2}, Lcom/samsung/location/currentloc/e;->a(Lcom/samsung/location/currentloc/f;Lcom/samsung/location/ISCurrentLocListener;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    return v0

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/currentloc/h;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/h;->b()Lcom/samsung/location/currentloc/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/f;->c()I

    move-result v0

    if-ne v0, p1, :cond_0

    const-string v0, "already exsit record!"

    invoke-static {v0, v4, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public a(Landroid/app/PendingIntent;)Z
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x1

    const-string v0, "removeCurrentLocationReceiver"

    invoke-static {v0, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/samsung/location/currentloc/e;->s:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/samsung/location/currentloc/e;->r:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/g;->c()Lcom/samsung/location/currentloc/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/i;->a()V

    const-string v0, "removeReceiver : disposeLocked"

    invoke-static {v0, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v1

    :goto_1
    return v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/currentloc/i;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/i;->b()Lcom/samsung/location/currentloc/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/g;->b()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_2
    const-string v0, "removeReceiver : receiver is null"

    invoke-static {v0, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Lcom/samsung/location/currentloc/a;)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/currentloc/e;->t:Lcom/samsung/location/currentloc/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/samsung/location/currentloc/e;->t:Lcom/samsung/location/currentloc/a;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Landroid/location/Location;)V
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/currentloc/e;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/samsung/location/currentloc/e;->b(Ljava/util/List;)V

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/currentloc/h;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/h;->b()Lcom/samsung/location/currentloc/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/location/currentloc/f;->a(Landroid/location/Location;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
