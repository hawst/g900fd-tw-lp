.class public Lcom/samsung/location/a/e;
.super Ljava/lang/Object;


# static fields
.field public static final a:I = 0x1

.field public static final b:I = 0x2

.field public static final c:I = 0x3


# instance fields
.field private d:I

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Lcom/samsung/location/a/f;


# direct methods
.method public constructor <init>(DDI)V
    .locals 7

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/samsung/location/a/e;->d:I

    iput v1, p0, Lcom/samsung/location/a/e;->e:I

    iput-object v0, p0, Lcom/samsung/location/a/e;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/location/a/e;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/location/a/e;->h:Lcom/samsung/location/a/f;

    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/location/a/e;->e:I

    new-instance v0, Lcom/samsung/location/a/f;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/location/a/f;-><init>(Lcom/samsung/location/a/e;DDI)V

    iput-object v0, p0, Lcom/samsung/location/a/e;->h:Lcom/samsung/location/a/f;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/samsung/location/a/e;->d:I

    iput v1, p0, Lcom/samsung/location/a/e;->e:I

    iput-object v0, p0, Lcom/samsung/location/a/e;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/location/a/e;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/location/a/e;->h:Lcom/samsung/location/a/f;

    iput p1, p0, Lcom/samsung/location/a/e;->e:I

    iput-object p2, p0, Lcom/samsung/location/a/e;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/samsung/location/a/e;->d:I

    iput v1, p0, Lcom/samsung/location/a/e;->e:I

    iput-object v0, p0, Lcom/samsung/location/a/e;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/location/a/e;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/location/a/e;->h:Lcom/samsung/location/a/f;

    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/location/a/e;->e:I

    iput-object p1, p0, Lcom/samsung/location/a/e;->g:Ljava/lang/String;

    iput-object p2, p0, Lcom/samsung/location/a/e;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/e;->e:I

    return v0
.end method

.method public a(D)V
    .locals 2

    iget v0, p0, Lcom/samsung/location/a/e;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/e;->h:Lcom/samsung/location/a/f;

    invoke-static {v0, p1, p2}, Lcom/samsung/location/a/f;->a(Lcom/samsung/location/a/f;D)V

    :cond_0
    return-void
.end method

.method public a(DDI)V
    .locals 0

    return-void
.end method

.method public a(I)V
    .locals 2

    iget v0, p0, Lcom/samsung/location/a/e;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/e;->h:Lcom/samsung/location/a/f;

    invoke-static {v0, p1}, Lcom/samsung/location/a/f;->a(Lcom/samsung/location/a/f;I)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget v0, p0, Lcom/samsung/location/a/e;->e:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/location/a/e;->e:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    iput-object p1, p0, Lcom/samsung/location/a/e;->f:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public b()D
    .locals 2

    iget v0, p0, Lcom/samsung/location/a/e;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/e;->h:Lcom/samsung/location/a/f;

    invoke-static {v0}, Lcom/samsung/location/a/f;->a(Lcom/samsung/location/a/f;)D

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    goto :goto_0
.end method

.method public b(D)V
    .locals 2

    iget v0, p0, Lcom/samsung/location/a/e;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/e;->h:Lcom/samsung/location/a/f;

    invoke-static {v0, p1, p2}, Lcom/samsung/location/a/f;->b(Lcom/samsung/location/a/f;D)V

    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/location/a/e;->d:I

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    iget v0, p0, Lcom/samsung/location/a/e;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iput-object p1, p0, Lcom/samsung/location/a/e;->g:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public c()D
    .locals 2

    iget v0, p0, Lcom/samsung/location/a/e;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/e;->h:Lcom/samsung/location/a/f;

    invoke-static {v0}, Lcom/samsung/location/a/f;->b(Lcom/samsung/location/a/f;)D

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    goto :goto_0
.end method

.method public d()I
    .locals 2

    iget v0, p0, Lcom/samsung/location/a/e;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/e;->h:Lcom/samsung/location/a/f;

    invoke-static {v0}, Lcom/samsung/location/a/f;->c(Lcom/samsung/location/a/f;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/samsung/location/a/e;->e:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/location/a/e;->e:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/a/e;->f:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/samsung/location/a/e;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/e;->g:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/e;->d:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "Type : "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/location/a/e;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/samsung/location/a/e;->e:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, " data : "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/a/e;->h:Lcom/samsung/location/a/f;

    invoke-static {v1}, Lcom/samsung/location/a/f;->a(Lcom/samsung/location/a/f;)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/a/e;->h:Lcom/samsung/location/a/f;

    invoke-static {v1}, Lcom/samsung/location/a/f;->b(Lcom/samsung/location/a/f;)D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/a/e;->h:Lcom/samsung/location/a/f;

    invoke-static {v1}, Lcom/samsung/location/a/f;->c(Lcom/samsung/location/a/f;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, " data : "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/a/e;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
