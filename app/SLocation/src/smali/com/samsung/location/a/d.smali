.class Lcom/samsung/location/a/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/location/a/b;


# instance fields
.field final synthetic a:Lcom/samsung/location/a/c;


# direct methods
.method private constructor <init>(Lcom/samsung/location/a/c;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/a/d;->a:Lcom/samsung/location/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/location/a/c;Lcom/samsung/location/a/d;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/location/a/d;-><init>(Lcom/samsung/location/a/c;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    const/16 v4, 0x68

    iget-object v0, p0, Lcom/samsung/location/a/d;->a:Lcom/samsung/location/a/c;

    invoke-static {v0}, Lcom/samsung/location/a/c;->b(Lcom/samsung/location/a/c;)Lcom/samsung/location/lib/h;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/samsung/location/lib/h;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/location/a/c;->n()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onInternalNLPNeeded"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/d;->a:Lcom/samsung/location/a/c;

    invoke-static {v0}, Lcom/samsung/location/a/c;->b(Lcom/samsung/location/a/c;)Lcom/samsung/location/lib/h;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/a/d;->a:Lcom/samsung/location/a/c;

    invoke-static {v1}, Lcom/samsung/location/a/c;->b(Lcom/samsung/location/a/c;)Lcom/samsung/location/lib/h;

    move-result-object v1

    const/16 v2, 0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    invoke-static {}, Lcom/samsung/location/a/c;->n()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onListenerDied : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/d;->a:Lcom/samsung/location/a/c;

    invoke-static {v0}, Lcom/samsung/location/a/c;->a(Lcom/samsung/location/a/c;)I

    move-result v0

    if-ne v0, v4, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/a/d;->a:Lcom/samsung/location/a/c;

    invoke-static {v0, p1}, Lcom/samsung/location/a/c;->a(Lcom/samsung/location/a/c;I)I

    iget-object v0, p0, Lcom/samsung/location/a/d;->a:Lcom/samsung/location/a/c;

    iget-object v0, v0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/d;->a:Lcom/samsung/location/a/c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/location/a/c;->b(Lcom/samsung/location/a/c;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/samsung/location/a/c;->n()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onListenerDied got exception : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public a(IIZ)V
    .locals 1

    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/a/d;->a:Lcom/samsung/location/a/c;

    invoke-static {v0, p1, p3}, Lcom/samsung/location/a/c;->a(Lcom/samsung/location/a/c;IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/samsung/location/a/d;->a:Lcom/samsung/location/a/c;

    invoke-static {v0, p1, p3}, Lcom/samsung/location/a/c;->b(Lcom/samsung/location/a/c;IZ)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    if-ne p2, v0, :cond_3

    iget-object v0, p0, Lcom/samsung/location/a/d;->a:Lcom/samsung/location/a/c;

    invoke-static {v0, p1}, Lcom/samsung/location/a/c;->c(Lcom/samsung/location/a/c;I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/d;->a:Lcom/samsung/location/a/c;

    invoke-static {v0, p1}, Lcom/samsung/location/a/c;->d(Lcom/samsung/location/a/c;I)V

    goto :goto_0
.end method
