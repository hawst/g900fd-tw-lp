.class public Lcom/samsung/location/a/g;
.super Ljava/lang/Object;


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = 0x3

.field public static final e:I = 0x0

.field public static final f:I = 0x1

.field public static final g:I = 0x2

.field public static final h:I = 0x3

.field public static final i:I = 0x4

.field public static final j:I = 0x5

.field public static final k:I = 0x6

.field public static final l:I = 0x7

.field public static final m:I = 0x8

.field public static final n:I = 0x9

.field private static o:Lcom/samsung/location/lib/j;

.field private static p:Z

.field private static v:Lcom/samsung/location/a/g;


# instance fields
.field private final q:Ljava/util/ArrayList;

.field private final r:Ljava/util/ArrayList;

.field private s:Lcom/samsung/location/a/b;

.field private t:Ljava/util/ArrayList;

.field private final u:Landroid/content/Context;

.field private w:Lcom/samsung/location/lib/SContentManager;

.field private x:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/location/a/g;->p:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/location/a/g;->v:Lcom/samsung/location/a/g;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/g;->q:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/location/a/g;->s:Lcom/samsung/location/a/b;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/g;->t:Ljava/util/ArrayList;

    sput-boolean p2, Lcom/samsung/location/a/g;->p:Z

    invoke-static {p1, p2}, Lcom/samsung/location/lib/j;->a(Landroid/content/Context;Z)Lcom/samsung/location/lib/j;

    move-result-object v0

    sput-object v0, Lcom/samsung/location/a/g;->o:Lcom/samsung/location/lib/j;

    iput-object p1, p0, Lcom/samsung/location/a/g;->u:Landroid/content/Context;

    iget-object v0, p0, Lcom/samsung/location/a/g;->u:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->a(Landroid/content/Context;)Lcom/samsung/location/lib/SContentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/g;->w:Lcom/samsung/location/lib/SContentManager;

    iget-object v0, p0, Lcom/samsung/location/a/g;->u:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/samsung/location/a/g;->x:Landroid/net/wifi/WifiManager;

    return-void
.end method

.method static synthetic a(Lcom/samsung/location/a/g;)Lcom/samsung/location/a/b;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/g;->s:Lcom/samsung/location/a/b;

    return-object v0
.end method

.method public static a()Lcom/samsung/location/a/g;
    .locals 1

    sget-object v0, Lcom/samsung/location/a/g;->v:Lcom/samsung/location/a/g;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Z)Lcom/samsung/location/a/g;
    .locals 1

    sget-object v0, Lcom/samsung/location/a/g;->v:Lcom/samsung/location/a/g;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/location/a/g;->v:Lcom/samsung/location/a/g;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/location/a/g;

    invoke-direct {v0, p0, p1}, Lcom/samsung/location/a/g;-><init>(Landroid/content/Context;Z)V

    sput-object v0, Lcom/samsung/location/a/g;->v:Lcom/samsung/location/a/g;

    sget-object v0, Lcom/samsung/location/a/g;->v:Lcom/samsung/location/a/g;

    goto :goto_0
.end method

.method private a(IIIILcom/samsung/location/a/j;)V
    .locals 10

    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/samsung/location/a/g;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/g;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const-string v1, "[%04d%02d%02d%02d%02d%02d] "

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x3

    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const/16 v4, 0xc

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xd

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v9, p0, Lcom/samsung/location/a/g;->t:Ljava/util/ArrayList;

    new-instance v0, Lcom/samsung/location/a/i;

    invoke-static {p5}, Lcom/samsung/location/a/j;->c(Lcom/samsung/location/a/j;)Lcom/samsung/location/a/h;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/location/a/h;->b(Lcom/samsung/location/a/h;)I

    move-result v6

    invoke-static {p5}, Lcom/samsung/location/a/j;->c(Lcom/samsung/location/a/j;)Lcom/samsung/location/a/h;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/location/a/h;->h(Lcom/samsung/location/a/h;)I

    move-result v7

    move-object v1, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/samsung/location/a/i;-><init>(Lcom/samsung/location/a/g;Ljava/lang/String;IIIIII)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(IIZ)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/g;->s:Lcom/samsung/location/a/b;

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/location/a/b;->a(IIZ)V

    return-void
.end method

.method static synthetic a(Lcom/samsung/location/a/g;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/location/a/g;->j(I)V

    return-void
.end method

.method static synthetic a(Lcom/samsung/location/a/g;IIIILcom/samsung/location/a/j;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/samsung/location/a/g;->a(IIIILcom/samsung/location/a/j;)V

    return-void
.end method

.method static synthetic a(Lcom/samsung/location/a/g;IIZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/location/a/g;->a(IIZ)V

    return-void
.end method

.method static synthetic b(Lcom/samsung/location/a/g;)Lcom/samsung/location/lib/SContentManager;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/g;->w:Lcom/samsung/location/lib/SContentManager;

    return-object v0
.end method

.method static synthetic c(Lcom/samsung/location/a/g;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/g;->u:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/samsung/location/a/g;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    return-object v0
.end method

.method private d(Ljava/util/List;)V
    .locals 3

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-gez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/j;

    iget-object v2, p0, Lcom/samsung/location/a/g;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->f()Lcom/samsung/location/a/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/location/a/k;->a()V

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/location/a/g;->j(I)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method private j(I)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/g;->s:Lcom/samsung/location/a/b;

    invoke-interface {v0, p1}, Lcom/samsung/location/a/b;->a(I)V

    return-void
.end method

.method static synthetic k()Z
    .locals 1

    sget-boolean v0, Lcom/samsung/location/a/g;->p:Z

    return v0
.end method

.method static synthetic l()Lcom/samsung/location/lib/j;
    .locals 1

    sget-object v0, Lcom/samsung/location/a/g;->o:Lcom/samsung/location/lib/j;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/List;)I
    .locals 5

    const/4 v4, 0x5

    const/4 v0, 0x1

    const/4 v3, 0x0

    if-nez p1, :cond_2

    const-string v1, "generateNewID : idArray is empty so return id is 1"

    invoke-static {v1, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "generateNewID : return id is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(I)V
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/samsung/location/a/g;->d(Ljava/util/List;)V

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/j;->a(I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(II)V
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/samsung/location/a/g;->d(Ljava/util/List;)V

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    :cond_2
    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/a/j;->a(II)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(ILjava/lang/String;)V
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/samsung/location/a/g;->d(Ljava/util/List;)V

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/location/a/j;->b(Lcom/samsung/location/a/j;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, p2}, Lcom/samsung/location/a/j;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Landroid/location/Location;)V
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/samsung/location/a/g;->d(Ljava/util/List;)V

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/location/a/j;->b(Lcom/samsung/location/a/j;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/j;->c(Landroid/location/Location;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p1}, Lcom/samsung/location/a/j;->a(Landroid/location/Location;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Ljava/io/PrintWriter;)V
    .locals 4

    const-string v1, "List of receiver = "

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "WIFI="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/location/a/g;->x:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " , scan always available="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/location/a/g;->x:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isScanAlwaysAvailable()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "\nNLP="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/location/a/g;->w:Lcom/samsung/location/lib/SContentManager;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/samsung/location/lib/SContentManager;->a(I)Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " GPS="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/location/a/g;->w:Lcom/samsung/location/lib/SContentManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/location/lib/SContentManager;->a(I)Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v0, "recent slocation event:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/location/a/g;->t:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/g;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/g;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/i;

    invoke-virtual {v0}, Lcom/samsung/location/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lcom/samsung/location/a/j;->c(Lcom/samsung/location/a/j;)Lcom/samsung/location/a/h;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/location/a/h;->i(Lcom/samsung/location/a/h;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1
.end method

.method public a(IILcom/samsung/location/a/e;)Z
    .locals 4

    const/4 v3, 0x2

    const/4 v1, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "addReceiver type"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/samsung/location/a/j;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/samsung/location/a/j;-><init>(Lcom/samsung/location/a/g;IILcom/samsung/location/a/e;)V

    sget-object v2, Lcom/samsung/location/a/g;->o:Lcom/samsung/location/lib/j;

    invoke-virtual {v2, p2}, Lcom/samsung/location/lib/j;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/location/a/j;->a(Lcom/samsung/location/a/j;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/location/a/g;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->f()Lcom/samsung/location/a/k;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->f()Lcom/samsung/location/a/k;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/location/a/k;->a()V

    :cond_1
    new-instance v2, Lcom/samsung/location/a/k;

    invoke-direct {v2, p0, v0}, Lcom/samsung/location/a/k;-><init>(Lcom/samsung/location/a/g;Lcom/samsung/location/a/j;)V

    invoke-virtual {v0, v2}, Lcom/samsung/location/a/j;->a(Lcom/samsung/location/a/k;)V

    move v0, v1

    :goto_0
    return v0

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v0

    if-ne v0, p2, :cond_0

    const-string v0, "already exsit record!"

    invoke-static {v0, v3, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ILandroid/app/PendingIntent;Z)Z
    .locals 6

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "setPendingIntent Fail : there\'s no receiver"

    invoke-static {v0, v5, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v2

    :goto_0
    return v0

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v4

    if-ne v4, p1, :cond_0

    if-nez p3, :cond_3

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->c()Landroid/app/PendingIntent;

    move-result-object v3

    if-nez v3, :cond_2

    invoke-virtual {v0, p2}, Lcom/samsung/location/a/j;->a(Landroid/app/PendingIntent;)V

    const-string v0, "setPendingIntent"

    invoke-static {v0, v5, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v1

    goto :goto_0

    :cond_2
    const-string v0, "setPendingIntent Fail : the other is already registered"

    invoke-static {v0, v5, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/samsung/location/a/j;->c()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/app/PendingIntent;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/samsung/location/a/j;->a(Landroid/app/PendingIntent;)V

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->e()Lcom/samsung/location/a/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/location/a/h;->a(I)V

    const-string v0, "pendingIntent set to null"

    const/4 v2, 0x5

    invoke-static {v0, v2, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v1

    goto :goto_0

    :cond_4
    const-string v0, "removePendingIntent Fail : mismatch"

    invoke-static {v0, v5, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v2

    goto :goto_0
.end method

.method public a(Lcom/samsung/location/a/b;)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/g;->s:Lcom/samsung/location/a/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/samsung/location/a/g;->s:Lcom/samsung/location/a/b;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/samsung/location/a/g;->e()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "checkCurrentMcc"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x2

    invoke-static {v0, v3, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->h()I

    move-result v4

    if-ne v4, v2, :cond_1

    invoke-static {v0}, Lcom/samsung/location/a/j;->d(Lcom/samsung/location/a/j;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-static {v0}, Lcom/samsung/location/a/j;->d(Lcom/samsung/location/a/j;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public b()V
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/samsung/location/a/g;->d(Ljava/util/List;)V

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->h()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->a()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b(I)V
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/samsung/location/a/g;->d(Ljava/util/List;)V

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/j;->b(I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b(II)V
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/samsung/location/a/g;->d(Ljava/util/List;)V

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/a/j;->b(II)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b(Landroid/location/Location;)V
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/samsung/location/a/g;->d(Ljava/util/List;)V

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/location/a/j;->b(Lcom/samsung/location/a/j;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/j;->b(Landroid/location/Location;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b(Ljava/util/List;)V
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/samsung/location/a/g;->d(Ljava/util/List;)V

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/j;->a(Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public c()V
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/samsung/location/a/g;->d(Ljava/util/List;)V

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->h()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->b()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public c(II)V
    .locals 4

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-virtual {v0, p2}, Lcom/samsung/location/a/j;->c(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "setStatus ID = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / status = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public c(Ljava/util/List;)V
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/samsung/location/a/g;->d(Ljava/util/List;)V

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/location/a/j;->b(Lcom/samsung/location/a/j;)I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/j;->b(Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public c(I)Z
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "removeReceiver ID = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/samsung/location/a/g;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->f()Lcom/samsung/location/a/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->a()V

    const-string v0, "removeReceiver : disposeLocked"

    invoke-static {v0, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v1

    :goto_1
    return v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v3

    if-ne v3, p1, :cond_0

    goto :goto_0

    :cond_2
    const-string v0, "removeReceiver : receiver is null"

    invoke-static {v0, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    goto :goto_1
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public d(I)I
    .locals 4

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v0

    goto :goto_0
.end method

.method public d(II)I
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->h()I

    move-result v2

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v2

    if-ne v2, p1, :cond_0

    if-ne p2, v3, :cond_2

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->e()Lcom/samsung/location/a/h;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/location/a/h;->f(Lcom/samsung/location/a/h;)I

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v2, 0x2

    if-ne p2, v2, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->e()Lcom/samsung/location/a/h;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/location/a/h;->g(Lcom/samsung/location/a/h;)I

    move-result v0

    goto :goto_0
.end method

.method public e()I
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->h()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public e(I)I
    .locals 4

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->h()I

    move-result v0

    goto :goto_0
.end method

.method public f(I)Lcom/samsung/location/a/e;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-static {v0}, Lcom/samsung/location/a/j;->c(Lcom/samsung/location/a/j;)Lcom/samsung/location/a/h;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/location/a/h;->d(Lcom/samsung/location/a/h;)Lcom/samsung/location/a/e;

    move-result-object v0

    goto :goto_0
.end method

.method public f()Ljava/util/List;
    .locals 5

    const/4 v4, 0x1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-object v1

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->h()I

    move-result v3

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    if-le v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public g(I)I
    .locals 3

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->e()Lcom/samsung/location/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/h;->a()I

    move-result v0

    goto :goto_0
.end method

.method public g()Ljava/util/List;
    .locals 5

    const/4 v4, 0x1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-object v1

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->h()I

    move-result v3

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    if-le v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public h(I)I
    .locals 3

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->e()Lcom/samsung/location/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/h;->b()I

    move-result v0

    goto :goto_0
.end method

.method public h()Ljava/util/List;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-object v1

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->e()Lcom/samsung/location/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/h;->c()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public i(I)F
    .locals 3

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/high16 v0, -0x40800000    # -1.0f

    :goto_0
    return v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->e()Lcom/samsung/location/a/h;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/location/a/h;->e(Lcom/samsung/location/a/h;)F

    move-result v0

    goto :goto_0
.end method

.method public i()I
    .locals 5

    const/4 v4, 0x1

    const/16 v0, 0x1f4

    iget-object v1, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->h()I

    move-result v3

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    if-lt v3, v4, :cond_0

    invoke-static {v0}, Lcom/samsung/location/a/j;->c(Lcom/samsung/location/a/j;)Lcom/samsung/location/a/h;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/location/a/h;->d(Lcom/samsung/location/a/h;)Lcom/samsung/location/a/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/e;->d()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v1, v0

    goto :goto_0
.end method

.method public j()Ljava/util/List;
    .locals 6

    const/4 v5, 0x1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/a/g;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-object v1

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->b()Lcom/samsung/location/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->h()I

    move-result v3

    if-ne v3, v5, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->g()I

    move-result v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->e()Lcom/samsung/location/a/h;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/location/a/h;->a()I

    move-result v3

    if-ne v3, v5, :cond_0

    invoke-static {v0}, Lcom/samsung/location/a/j;->a(Lcom/samsung/location/a/j;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
