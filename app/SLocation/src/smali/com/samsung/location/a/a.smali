.class public Lcom/samsung/location/a/a;
.super Ljava/lang/Object;


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field private static final c:I = 0x1

.field private static final d:I = 0x2

.field private static final e:I = 0x1

.field private static final f:I = 0x2

.field private static final g:I = 0x0

.field private static final h:I = 0x1

.field private static i:Lcom/samsung/location/a/a;


# instance fields
.field private j:Lcom/samsung/location/ISLocationCellInterface;

.field private k:Lcom/samsung/location/a/g;

.field private l:Lcom/samsung/location/lib/j;

.field private m:Lcom/samsung/location/lib/SContentManager;

.field private n:Z

.field private o:Ljava/util/ArrayList;

.field private p:Ljava/util/ArrayList;

.field private q:Ljava/util/ArrayList;

.field private r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/location/a/a;->i:Lcom/samsung/location/a/a;

    return-void
.end method

.method private constructor <init>(Lcom/samsung/location/ISLocationCellInterface;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/samsung/location/a/a;->n:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/a;->o:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/a;->p:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/a;->q:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/samsung/location/a/a;->r:Z

    iput-object p1, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/location/a/a;->n:Z

    :cond_0
    invoke-static {}, Lcom/samsung/location/a/g;->a()Lcom/samsung/location/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/a;->k:Lcom/samsung/location/a/g;

    invoke-static {}, Lcom/samsung/location/lib/j;->a()Lcom/samsung/location/lib/j;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/a;->l:Lcom/samsung/location/lib/j;

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->a(Landroid/content/Context;)Lcom/samsung/location/lib/SContentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/a;->m:Lcom/samsung/location/lib/SContentManager;

    return-void
.end method

.method public static a()Lcom/samsung/location/a/a;
    .locals 1

    sget-object v0, Lcom/samsung/location/a/a;->i:Lcom/samsung/location/a/a;

    return-object v0
.end method

.method public static a(Lcom/samsung/location/ISLocationCellInterface;)Lcom/samsung/location/a/a;
    .locals 1

    sget-object v0, Lcom/samsung/location/a/a;->i:Lcom/samsung/location/a/a;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/location/a/a;->i:Lcom/samsung/location/a/a;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/location/a/a;

    invoke-direct {v0, p0}, Lcom/samsung/location/a/a;-><init>(Lcom/samsung/location/ISLocationCellInterface;)V

    sput-object v0, Lcom/samsung/location/a/a;->i:Lcom/samsung/location/a/a;

    sget-object v0, Lcom/samsung/location/a/a;->i:Lcom/samsung/location/a/a;

    goto :goto_0
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/location/a/a;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/location/a/a;->a(I)Z

    goto :goto_0
.end method

.method private h()V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x2

    iget-boolean v0, p0, Lcom/samsung/location/a/a;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "stopCellGeofence"

    invoke-static {v0, v3, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/samsung/location/ISLocationCellInterface;->initCellGeofence(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/location/a/a;->r:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stopCellGeofence got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v3, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/util/List;)V
    .locals 7

    const/4 v6, 0x2

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/samsung/location/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "syncCellGeofence("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [I

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    move v1, v2

    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_4

    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    array-length v1, v3

    array-length v2, v4

    invoke-interface {v0, v3, v1, v4, v2}, Lcom/samsung/location/ISLocationCellInterface;->syncCellGeofence([II[II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :cond_3
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "syncCellGeofence : "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget v5, v3, v1

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v4, v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "syncCellGeofence enbaled list: "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget v5, v4, v1

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method

.method public a(I)Z
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/samsung/location/a/a;->n:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v2, p0, Lcom/samsung/location/a/a;->r:Z

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/samsung/location/a/a;->b()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v1, p0, Lcom/samsung/location/a/a;->q:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addCellGeofence ID = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    invoke-interface {v0, p1}, Lcom/samsung/location/ISLocationCellInterface;->addCellGeofence(I)V

    iget-object v0, p0, Lcom/samsung/location/a/a;->p:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addCellGeofence got exception"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_1
.end method

.method public b(I)V
    .locals 3

    invoke-virtual {p0}, Lcom/samsung/location/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/location/a/a;->o:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "enableCellGeofence ID = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/samsung/location/ISLocationCellInterface;->enableCellGeofence(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public b()Z
    .locals 5

    const/4 v4, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/samsung/location/a/a;->n:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/location/a/a;->r:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/location/a/a;->k:Lcom/samsung/location/a/g;

    invoke-virtual {v2}, Lcom/samsung/location/a/g;->d()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/samsung/location/a/a;->m:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v2}, Lcom/samsung/location/lib/SContentManager;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "startCellGeofence"

    invoke-static {v2, v4, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lcom/samsung/location/ISLocationCellInterface;->initCellGeofence(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/location/a/a;->r:Z

    invoke-direct {p0}, Lcom/samsung/location/a/a;->g()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "startCellGeofence got exception"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_1
.end method

.method public c()V
    .locals 3

    const-string v0, "handleCellEnable"

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {p0}, Lcom/samsung/location/a/a;->b()Z

    return-void
.end method

.method public c(I)V
    .locals 3

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/samsung/location/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "disableCellGeofence ID = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    const/4 v1, 0x2

    invoke-interface {v0, p1, v1}, Lcom/samsung/location/ISLocationCellInterface;->enableCellGeofence(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public d(I)V
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/samsung/location/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "startCollectCell ID = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/a;->o:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Already collecting cell for this id"

    invoke-static {v0, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    invoke-interface {v0, p1}, Lcom/samsung/location/ISLocationCellInterface;->startCollectCell(I)V

    iget-object v0, p0, Lcom/samsung/location/a/a;->o:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startCollectCell got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v3, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/location/a/a;->n:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/location/a/a;->r:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(I)V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/samsung/location/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/location/a/a;->o:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "stopCollectCell ID = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    invoke-interface {v0, p1}, Lcom/samsung/location/ISLocationCellInterface;->stopCollectCell(I)V

    iget-object v0, p0, Lcom/samsung/location/a/a;->o:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/location/a/a;->l:Lcom/samsung/location/lib/j;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/samsung/location/lib/j;->a(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stopCollectCell got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/location/a/a;->n:Z

    return v0
.end method

.method public f(I)V
    .locals 3

    invoke-virtual {p0}, Lcom/samsung/location/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "deleteCellGeofence ID = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {p0, p1}, Lcom/samsung/location/a/a;->e(I)V

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/a/a;->j:Lcom/samsung/location/ISLocationCellInterface;

    invoke-interface {v0, p1}, Lcom/samsung/location/ISLocationCellInterface;->removeCellGeofence(I)V

    iget-object v0, p0, Lcom/samsung/location/a/a;->p:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/location/a/a;->p:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/samsung/location/a/a;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/location/a/a;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    invoke-direct {p0}, Lcom/samsung/location/a/a;->h()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/location/a/a;->r:Z

    return v0
.end method

.method public g(I)Z
    .locals 2

    iget-object v0, p0, Lcom/samsung/location/a/a;->o:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h(I)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/samsung/location/a/a;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/location/a/a;->l:Lcom/samsung/location/lib/j;

    invoke-virtual {v2, p1}, Lcom/samsung/location/lib/j;->a(I)I

    move-result v2

    :goto_0
    if-ne v2, v0, :cond_0

    :goto_1
    return v0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    move v2, v1

    goto :goto_0
.end method

.method public i(I)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "confirmCellDBExist "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/a;->o:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/a;->l:Lcom/samsung/location/lib/j;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/samsung/location/lib/j;->a(II)V

    :cond_0
    return-void
.end method
