.class Lcom/samsung/location/a/h;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/samsung/location/a/g;

.field private b:Lcom/samsung/location/a/e;

.field private c:Ljava/lang/String;

.field private d:Ljava/util/List;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:F

.field private j:Z

.field private k:I

.field private l:I


# direct methods
.method constructor <init>(Lcom/samsung/location/a/g;Lcom/samsung/location/a/e;)V
    .locals 3

    const/4 v2, -0x1

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/samsung/location/a/h;->a:Lcom/samsung/location/a/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/location/a/h;->c:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/location/a/h;->d:Ljava/util/List;

    iput v1, p0, Lcom/samsung/location/a/h;->e:I

    iput v1, p0, Lcom/samsung/location/a/h;->f:I

    iput v1, p0, Lcom/samsung/location/a/h;->g:I

    iput v1, p0, Lcom/samsung/location/a/h;->h:I

    iput-boolean v1, p0, Lcom/samsung/location/a/h;->j:Z

    iput v2, p0, Lcom/samsung/location/a/h;->k:I

    iput v2, p0, Lcom/samsung/location/a/h;->l:I

    iput-object p2, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->a()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v0}, Lcom/samsung/location/a/e;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x32

    const/16 v1, 0x64

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/location/a/h;->k:I

    iget-object v0, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v0}, Lcom/samsung/location/a/e;->d()I

    move-result v0

    const/16 v1, 0xc8

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x32

    iput v0, p0, Lcom/samsung/location/a/h;->l:I

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/samsung/location/a/h;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/location/a/h;->g:I

    return-void
.end method

.method static synthetic a(Lcom/samsung/location/a/h;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/a/h;->c:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/samsung/location/a/h;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/location/a/h;->j:Z

    return-void
.end method

.method static synthetic a(Lcom/samsung/location/a/h;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/location/a/h;->j:Z

    return v0
.end method

.method private a(Ljava/util/List;)Z
    .locals 5

    const/4 v2, 0x0

    const-string v0, "checkPlaceChanged "

    const/4 v1, 0x4

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Got new AP list : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", match count : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x2

    invoke-static {v0, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-direct {p0, v1}, Lcom/samsung/location/a/h;->b(I)Z

    move-result v0

    return v0

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    iget-object v4, p0, Lcom/samsung/location/a/h;->d:Ljava/util/List;

    iget-object v0, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/samsung/location/a/h;)I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/h;->e:I

    return v0
.end method

.method static synthetic b(Lcom/samsung/location/a/h;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/location/a/h;->e:I

    return-void
.end method

.method private b(I)Z
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget v2, p0, Lcom/samsung/location/a/h;->g:I

    iput v2, p0, Lcom/samsung/location/a/h;->h:I

    if-lt p1, v0, :cond_0

    iget v2, p0, Lcom/samsung/location/a/h;->g:I

    if-eq v2, v0, :cond_1

    iput v0, p0, Lcom/samsung/location/a/h;->g:I

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setCurrentState : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/location/a/h;->h:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-> ENTER"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    return v0

    :cond_0
    if-ge p1, v0, :cond_1

    iget v2, p0, Lcom/samsung/location/a/h;->g:I

    if-eq v2, v3, :cond_1

    iput v3, p0, Lcom/samsung/location/a/h;->g:I

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setCurrentState : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/location/a/h;->h:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-> EXIT"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method static synthetic c(Lcom/samsung/location/a/h;)I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/h;->h:I

    return v0
.end method

.method static synthetic d(Lcom/samsung/location/a/h;)Lcom/samsung/location/a/e;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    return-object v0
.end method

.method static synthetic e(Lcom/samsung/location/a/h;)F
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/h;->i:F

    return v0
.end method

.method static synthetic f(Lcom/samsung/location/a/h;)I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/h;->k:I

    return v0
.end method

.method static synthetic g(Lcom/samsung/location/a/h;)I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/h;->l:I

    return v0
.end method

.method static synthetic h(Lcom/samsung/location/a/h;)I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/h;->g:I

    return v0
.end method

.method static synthetic i(Lcom/samsung/location/a/h;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/h;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method a()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/h;->e:I

    return v0
.end method

.method a(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/location/a/h;->e:I

    return-void
.end method

.method a(IILcom/samsung/location/a/j;)Z
    .locals 7

    const/4 v6, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "handleCellNotification ID = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " / status = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    if-ne p2, v1, :cond_2

    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/location/a/h;->f:I

    :cond_0
    :goto_0
    invoke-static {}, Lcom/samsung/location/a/g;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/samsung/location/a/h;->f:I

    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3}, Lcom/samsung/location/a/j;->a(ILandroid/location/Location;)Z

    move-result v0

    :goto_1
    iget-object v3, p0, Lcom/samsung/location/a/h;->a:Lcom/samsung/location/a/g;

    invoke-virtual {p3}, Lcom/samsung/location/a/j;->d()I

    move-result v4

    iget v5, p0, Lcom/samsung/location/a/h;->f:I

    invoke-static {v3, v4, v5, v2}, Lcom/samsung/location/a/g;->a(Lcom/samsung/location/a/g;IIZ)V

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "RemoteException calling onGeoFenceDetected on "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v1, v2

    :cond_1
    return v1

    :cond_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    iput v6, p0, Lcom/samsung/location/a/h;->f:I

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method a(ILcom/samsung/location/a/j;)Z
    .locals 7

    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "handleCellFailNotification ID = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-static {}, Lcom/samsung/location/a/g;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p2, v6, v0}, Lcom/samsung/location/a/j;->a(ILandroid/location/Location;)Z

    move-result v0

    :goto_0
    iget-object v3, p0, Lcom/samsung/location/a/h;->a:Lcom/samsung/location/a/g;

    invoke-virtual {p2}, Lcom/samsung/location/a/j;->d()I

    move-result v4

    invoke-static {v3, v4, v6, v2}, Lcom/samsung/location/a/g;->a(Lcom/samsung/location/a/g;IIZ)V

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "RemoteException calling onGeoFenceDetected on "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v1, v2

    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method a(Landroid/location/Location;Lcom/samsung/location/a/j;)Z
    .locals 13

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v9, 0x0

    const/4 v0, 0x1

    new-array v8, v0, [F

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x0

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    iget-object v4, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v4}, Lcom/samsung/location/a/e;->b()D

    move-result-wide v4

    iget-object v6, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v6}, Lcom/samsung/location/a/e;->c()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v0, v8, v0

    iput v0, p0, Lcom/samsung/location/a/h;->i:F

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "handleLocation : id "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/samsung/location/a/j;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " with "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/location/a/h;->i:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v1}, Lcom/samsung/location/a/e;->d()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_f

    iget-boolean v0, p0, Lcom/samsung/location/a/h;->j:Z

    if-eqz v0, :cond_1

    const-string v0, "revert wifi direction"

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/location/a/h;->j:Z

    iget v0, p0, Lcom/samsung/location/a/h;->h:I

    iput v0, p0, Lcom/samsung/location/a/h;->g:I

    invoke-virtual {p0, p1, p2}, Lcom/samsung/location/a/h;->b(Landroid/location/Location;Lcom/samsung/location/a/j;)Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const-string v1, "type1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "inaccurate GPS location"

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/h;->a:Lcom/samsung/location/a/g;

    invoke-static {v0}, Lcom/samsung/location/a/g;->a(Lcom/samsung/location/a/g;)Lcom/samsung/location/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/location/a/b;->a()V

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_2
    const/4 v1, 0x1

    iget v0, p0, Lcom/samsung/location/a/h;->i:F

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    sub-float/2addr v0, v2

    move v2, v1

    move v1, v0

    :goto_1
    const/4 v0, 0x0

    if-nez v2, :cond_6

    iget v1, p0, Lcom/samsung/location/a/h;->i:F

    iget v2, p0, Lcom/samsung/location/a/h;->k:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_5

    const/4 v0, 0x1

    :cond_3
    :goto_2
    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "handleLocation enter : currentDirection "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/location/a/h;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget v0, p0, Lcom/samsung/location/a/h;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_c

    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/location/a/h;->e:I

    iget-object v0, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v0}, Lcom/samsung/location/a/e;->g()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {p2}, Lcom/samsung/location/a/j;->g()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_d

    const/4 v1, 0x1

    move v9, v0

    move v0, v1

    :goto_4
    if-nez v9, :cond_4

    invoke-virtual {p2}, Lcom/samsung/location/a/j;->g()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_4

    iget v1, p0, Lcom/samsung/location/a/h;->e:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    iget v1, p0, Lcom/samsung/location/a/h;->g:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    const/4 v1, 0x1

    iput v1, p0, Lcom/samsung/location/a/h;->g:I

    :cond_4
    if-eqz v9, :cond_b

    if-nez v0, :cond_9

    invoke-virtual {p2, v9, p1}, Lcom/samsung/location/a/j;->a(ILandroid/location/Location;)Z

    move-result v1

    if-nez v1, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "!!!RemoteException calling onGeoFenceDetected on "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_5
    iget v1, p0, Lcom/samsung/location/a/h;->i:F

    iget v2, p0, Lcom/samsung/location/a/h;->l:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    const/4 v0, 0x2

    goto :goto_2

    :cond_6
    iget v2, p0, Lcom/samsung/location/a/h;->l:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    const/4 v0, 0x2

    goto :goto_2

    :cond_7
    const/4 v1, 0x2

    if-ne v0, v1, :cond_c

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "handleLocation exit : currentDirection "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/location/a/h;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget v0, p0, Lcom/samsung/location/a/h;->e:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_c

    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/location/a/h;->e:I

    iget-object v0, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v0}, Lcom/samsung/location/a/e;->g()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_8

    const/4 v9, 0x2

    :cond_8
    invoke-virtual {p2}, Lcom/samsung/location/a/j;->g()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    goto :goto_4

    :cond_9
    iget-object v1, p0, Lcom/samsung/location/a/h;->a:Lcom/samsung/location/a/g;

    invoke-virtual {p2}, Lcom/samsung/location/a/j;->d()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v9, v3}, Lcom/samsung/location/a/g;->a(Lcom/samsung/location/a/g;IIZ)V

    if-eqz v0, :cond_a

    :cond_a
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/location/a/h;->j:Z

    :cond_b
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_c
    move v0, v10

    goto/16 :goto_4

    :cond_d
    move v9, v0

    move v0, v10

    goto/16 :goto_4

    :cond_e
    move v0, v9

    goto/16 :goto_3

    :cond_f
    move v1, v11

    move v2, v12

    goto/16 :goto_1
.end method

.method a(Lcom/samsung/location/a/j;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v2, 0x2

    iget v1, p0, Lcom/samsung/location/a/h;->e:I

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p1, v2, v1}, Lcom/samsung/location/a/j;->a(ILandroid/location/Location;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "!!!RemoteException calling onGeoFenceDetected on "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v1, v2, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iput v2, p0, Lcom/samsung/location/a/h;->e:I

    goto :goto_0
.end method

.method a(Ljava/lang/String;Lcom/samsung/location/a/j;)Z
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v3, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handle Wifi Connection ID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/samsung/location/a/j;->a(Lcom/samsung/location/a/j;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/a/h;->d:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/location/a/h;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/samsung/location/a/g;->l()Lcom/samsung/location/lib/j;

    move-result-object v1

    invoke-static {p2}, Lcom/samsung/location/a/j;->a(Lcom/samsung/location/a/j;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/j;->d(I)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/location/a/h;->d:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v1}, Lcom/samsung/location/a/e;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/location/a/h;->d:Ljava/util/List;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/samsung/location/a/h;->d:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleWifi ENTER : currentDirection "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/location/a/h;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget v1, p0, Lcom/samsung/location/a/h;->e:I

    if-ne v1, v0, :cond_3

    invoke-virtual {p2}, Lcom/samsung/location/a/j;->g()I

    move-result v1

    if-ne v1, v3, :cond_5

    :cond_3
    iput v0, p0, Lcom/samsung/location/a/h;->e:I

    iget-object v1, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v1}, Lcom/samsung/location/a/e;->g()I

    move-result v1

    if-eq v1, v3, :cond_4

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Lcom/samsung/location/a/j;->a(ILandroid/location/Location;)Z

    move-result v0

    :cond_4
    invoke-virtual {p2}, Lcom/samsung/location/a/j;->g()I

    move-result v1

    if-ne v1, v3, :cond_5

    invoke-virtual {p2, v6}, Lcom/samsung/location/a/j;->c(I)V

    :cond_5
    :goto_0
    return v0

    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleWifi EXIT : currentDirection "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/location/a/h;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget v1, p0, Lcom/samsung/location/a/h;->e:I

    if-ne v1, v3, :cond_7

    invoke-virtual {p2}, Lcom/samsung/location/a/j;->g()I

    move-result v1

    if-ne v1, v3, :cond_5

    :cond_7
    iput v3, p0, Lcom/samsung/location/a/h;->e:I

    iget-object v1, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v1}, Lcom/samsung/location/a/e;->g()I

    move-result v1

    if-eq v1, v0, :cond_8

    const/4 v0, 0x0

    invoke-virtual {p2, v3, v0}, Lcom/samsung/location/a/j;->a(ILandroid/location/Location;)Z

    move-result v0

    :cond_8
    invoke-virtual {p2}, Lcom/samsung/location/a/j;->g()I

    move-result v1

    if-ne v1, v3, :cond_5

    invoke-virtual {p2, v6}, Lcom/samsung/location/a/j;->c(I)V

    goto :goto_0
.end method

.method a(Ljava/util/List;Lcom/samsung/location/a/j;)Z
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v3, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handle BT List Connection ID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/samsung/location/a/j;->a(Lcom/samsung/location/a/j;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "list : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v1}, Lcom/samsung/location/a/e;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleWifi/BT ENTER : currentDirection "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/location/a/h;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget v1, p0, Lcom/samsung/location/a/h;->e:I

    if-ne v1, v0, :cond_0

    invoke-virtual {p2}, Lcom/samsung/location/a/j;->g()I

    move-result v1

    if-ne v1, v3, :cond_2

    :cond_0
    iput v0, p0, Lcom/samsung/location/a/h;->e:I

    iget-object v1, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v1}, Lcom/samsung/location/a/e;->g()I

    move-result v1

    if-eq v1, v3, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Lcom/samsung/location/a/j;->a(ILandroid/location/Location;)Z

    move-result v0

    :cond_1
    invoke-virtual {p2}, Lcom/samsung/location/a/j;->g()I

    move-result v1

    if-ne v1, v3, :cond_2

    invoke-virtual {p2, v6}, Lcom/samsung/location/a/j;->c(I)V

    :cond_2
    :goto_0
    return v0

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleBT EXIT : currentDirection "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/location/a/h;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget v1, p0, Lcom/samsung/location/a/h;->e:I

    if-ne v1, v3, :cond_4

    invoke-virtual {p2}, Lcom/samsung/location/a/j;->g()I

    move-result v1

    if-ne v1, v3, :cond_2

    :cond_4
    iput v3, p0, Lcom/samsung/location/a/h;->e:I

    iget-object v1, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v1}, Lcom/samsung/location/a/e;->g()I

    move-result v1

    if-eq v1, v0, :cond_5

    const/4 v0, 0x0

    invoke-virtual {p2, v3, v0}, Lcom/samsung/location/a/j;->a(ILandroid/location/Location;)Z

    move-result v0

    :cond_5
    invoke-virtual {p2}, Lcom/samsung/location/a/j;->g()I

    move-result v1

    if-ne v1, v3, :cond_2

    invoke-virtual {p2, v6}, Lcom/samsung/location/a/j;->c(I)V

    goto :goto_0
.end method

.method b()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/h;->f:I

    return v0
.end method

.method b(IILcom/samsung/location/a/j;)Z
    .locals 8

    const/4 v7, 0x7

    const/4 v6, 0x6

    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "handleGpsNotification ID = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " / status = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    if-ne p2, v1, :cond_4

    invoke-static {}, Lcom/samsung/location/a/g;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-virtual {p3, v6, v0}, Lcom/samsung/location/a/j;->a(ILandroid/location/Location;)Z

    move-result v0

    :goto_0
    iget-object v3, p0, Lcom/samsung/location/a/h;->a:Lcom/samsung/location/a/g;

    invoke-virtual {p3}, Lcom/samsung/location/a/j;->d()I

    move-result v4

    invoke-static {v3, v4, v6, v2}, Lcom/samsung/location/a/g;->a(Lcom/samsung/location/a/g;IIZ)V

    :goto_1
    const/4 v3, 0x2

    if-ne p2, v3, :cond_1

    invoke-static {}, Lcom/samsung/location/a/g;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p3, v7, v0}, Lcom/samsung/location/a/j;->a(ILandroid/location/Location;)Z

    move-result v0

    :cond_0
    iget-object v3, p0, Lcom/samsung/location/a/h;->a:Lcom/samsung/location/a/g;

    invoke-virtual {p3}, Lcom/samsung/location/a/j;->d()I

    move-result v4

    invoke-static {v3, v4, v7, v2}, Lcom/samsung/location/a/g;->a(Lcom/samsung/location/a/g;IIZ)V

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RemoteException calling onGeoFenceDetected on "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v1, v2

    :cond_2
    return v1

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method b(ILcom/samsung/location/a/j;)Z
    .locals 8

    const/16 v7, 0x9

    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "handleGpsStatusNotification status : "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    if-ne p1, v1, :cond_4

    invoke-static {}, Lcom/samsung/location/a/g;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-virtual {p2, v6, v0}, Lcom/samsung/location/a/j;->a(ILandroid/location/Location;)Z

    move-result v0

    :goto_0
    iget-object v3, p0, Lcom/samsung/location/a/h;->a:Lcom/samsung/location/a/g;

    invoke-virtual {p2}, Lcom/samsung/location/a/j;->d()I

    move-result v4

    invoke-static {v3, v4, v6, v2}, Lcom/samsung/location/a/g;->a(Lcom/samsung/location/a/g;IIZ)V

    :goto_1
    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    invoke-static {}, Lcom/samsung/location/a/g;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p2, v7, v0}, Lcom/samsung/location/a/j;->a(ILandroid/location/Location;)Z

    move-result v0

    :cond_0
    iget-object v3, p0, Lcom/samsung/location/a/h;->a:Lcom/samsung/location/a/g;

    invoke-virtual {p2}, Lcom/samsung/location/a/j;->d()I

    move-result v4

    invoke-static {v3, v4, v7, v2}, Lcom/samsung/location/a/g;->a(Lcom/samsung/location/a/g;IIZ)V

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RemoteException calling onGeoFenceDetected on "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v1, v2

    :cond_2
    return v1

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method b(Landroid/location/Location;Lcom/samsung/location/a/j;)Z
    .locals 13

    const/4 v12, 0x4

    const/4 v9, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    if-nez p1, :cond_0

    :goto_0
    return v10

    :cond_0
    new-array v8, v10, [F

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    iget-object v4, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v4}, Lcom/samsung/location/a/e;->b()D

    move-result-wide v4

    iget-object v6, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v6}, Lcom/samsung/location/a/e;->c()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    aget v0, v8, v11

    iput v0, p0, Lcom/samsung/location/a/h;->i:F

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "handleStartLocation : id "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/samsung/location/a/j;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " with "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/location/a/h;->i:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v12, v10}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-boolean v0, p0, Lcom/samsung/location/a/h;->j:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/samsung/location/a/h;->i:F

    iget-object v1, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v1}, Lcom/samsung/location/a/e;->d()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iput v11, p0, Lcom/samsung/location/a/h;->e:I

    move v0, v9

    :goto_1
    invoke-virtual {p2, v0, p1}, Lcom/samsung/location/a/j;->a(ILandroid/location/Location;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "!!!RemoteException calling onGeoFenceDetected on "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v12, v10}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v10, v11

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/samsung/location/a/h;->i:F

    iget-object v1, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v1}, Lcom/samsung/location/a/e;->d()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_2

    iput v10, p0, Lcom/samsung/location/a/h;->e:I

    iget-object v0, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v0}, Lcom/samsung/location/a/e;->g()I

    move-result v0

    if-eq v0, v9, :cond_5

    move v0, v10

    goto :goto_1

    :cond_2
    iput v9, p0, Lcom/samsung/location/a/h;->e:I

    iget-object v0, p0, Lcom/samsung/location/a/h;->b:Lcom/samsung/location/a/e;

    invoke-virtual {v0}, Lcom/samsung/location/a/e;->g()I

    move-result v0

    if-eq v0, v10, :cond_5

    move v0, v9

    goto :goto_1

    :cond_3
    iget-boolean v1, p0, Lcom/samsung/location/a/h;->j:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/samsung/location/a/h;->a:Lcom/samsung/location/a/g;

    invoke-virtual {p2}, Lcom/samsung/location/a/j;->d()I

    move-result v2

    invoke-static {v1, v2, v0, v11}, Lcom/samsung/location/a/g;->a(Lcom/samsung/location/a/g;IIZ)V

    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Lcom/samsung/location/a/j;->c(I)V

    goto/16 :goto_0

    :cond_5
    move v0, v11

    goto :goto_1
.end method

.method b(Ljava/util/List;Lcom/samsung/location/a/j;)Z
    .locals 4

    const/4 v3, 0x1

    const-string v0, "handleWifiScanResult "

    const/4 v1, 0x4

    invoke-static {v0, v1, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-static {}, Lcom/samsung/location/a/g;->l()Lcom/samsung/location/lib/j;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/location/a/j;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/j;->d(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/h;->d:Ljava/util/List;

    iget-object v0, p0, Lcom/samsung/location/a/h;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/location/a/h;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/location/a/h;->e:I

    iget v1, p0, Lcom/samsung/location/a/h;->g:I

    if-ne v0, v1, :cond_1

    const-string v0, "wifiDirection = currentDirection, do not trigger NLP"

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :cond_0
    :goto_0
    return v3

    :cond_1
    iput-boolean v3, p0, Lcom/samsung/location/a/h;->j:Z

    iget-object v0, p0, Lcom/samsung/location/a/h;->a:Lcom/samsung/location/a/g;

    invoke-virtual {p2}, Lcom/samsung/location/a/j;->d()I

    move-result v1

    iget v2, p0, Lcom/samsung/location/a/h;->g:I

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/a/g;->a(Lcom/samsung/location/a/g;IIZ)V

    goto :goto_0
.end method

.method c()F
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/h;->i:F

    return v0
.end method
