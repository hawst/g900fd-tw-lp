.class public Lcom/samsung/location/a/c;
.super Ljava/lang/Object;


# static fields
.field private static final F:I = 0x0

.field private static final G:I = -0x64

.field private static final H:I = 0x1

.field private static final I:I = 0x2

.field private static final J:I = 0x0

.field private static final K:I = 0x1

.field private static final L:I = 0x2

.field private static final M:I = 0x0

.field private static final N:I = 0x1

.field private static final O:I = 0x2

.field private static final P:I = 0x4

.field private static final Q:I = 0x8

.field private static final R:I = 0x2710

.field private static ad:Lcom/samsung/location/a/c; = null

.field public static final h:I = 0x1

.field public static final i:I = 0x2

.field public static final j:I = 0x4

.field public static final k:I = 0x64

.field public static final l:I = 0x65

.field public static final m:I = 0x66

.field private static n:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:I

.field private E:I

.field private S:J

.field private T:J

.field private U:J

.field private V:Ljava/lang/String;

.field private W:Ljava/lang/String;

.field private final X:Landroid/content/Context;

.field private Y:Lcom/samsung/location/lib/h;

.field private Z:Landroid/location/Location;

.field a:Ljava/util/List;

.field private aa:Ljava/lang/String;

.field private ab:Ljava/lang/Object;

.field private final ac:Ljava/lang/Object;

.field private ae:Lcom/samsung/location/a/d;

.field b:Ljava/util/List;

.field c:Ljava/util/List;

.field d:Ljava/util/List;

.field e:Ljava/util/List;

.field f:Ljava/util/List;

.field g:Ljava/util/List;

.field private o:Lcom/samsung/location/a/g;

.field private p:Landroid/net/wifi/WifiManager;

.field private q:Lcom/samsung/location/lib/j;

.field private r:Lcom/samsung/location/a/l;

.field private s:Lcom/samsung/location/a/a;

.field private t:Lcom/samsung/location/lib/SContentManager;

.field private u:Landroid/location/IGpsGeofenceHardware;

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "SGeofenceManager"

    sput-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/location/a/c;->ad:Lcom/samsung/location/a/c;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/c;->a:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/c;->b:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    iput-object v2, p0, Lcom/samsung/location/a/c;->d:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/c;->e:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/c;->f:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/c;->g:Ljava/util/List;

    iput-boolean v1, p0, Lcom/samsung/location/a/c;->v:Z

    iput-boolean v1, p0, Lcom/samsung/location/a/c;->w:Z

    iput-boolean v1, p0, Lcom/samsung/location/a/c;->x:Z

    iput-boolean v1, p0, Lcom/samsung/location/a/c;->y:Z

    iput-boolean v1, p0, Lcom/samsung/location/a/c;->z:Z

    iput-boolean v1, p0, Lcom/samsung/location/a/c;->A:Z

    iput-boolean v1, p0, Lcom/samsung/location/a/c;->B:Z

    iput-boolean v1, p0, Lcom/samsung/location/a/c;->C:Z

    iput v1, p0, Lcom/samsung/location/a/c;->D:I

    iput v1, p0, Lcom/samsung/location/a/c;->E:I

    iput-wide v3, p0, Lcom/samsung/location/a/c;->S:J

    iput-wide v3, p0, Lcom/samsung/location/a/c;->T:J

    iput-wide v3, p0, Lcom/samsung/location/a/c;->U:J

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/location/a/c;->V:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/location/a/c;->W:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/location/a/c;->Z:Landroid/location/Location;

    iput-object v2, p0, Lcom/samsung/location/a/c;->aa:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/c;->ac:Ljava/lang/Object;

    new-instance v0, Lcom/samsung/location/a/d;

    invoke-direct {v0, p0, v2}, Lcom/samsung/location/a/d;-><init>(Lcom/samsung/location/a/c;Lcom/samsung/location/a/d;)V

    iput-object v0, p0, Lcom/samsung/location/a/c;->ae:Lcom/samsung/location/a/d;

    iput-object p1, p0, Lcom/samsung/location/a/c;->X:Landroid/content/Context;

    invoke-direct {p0}, Lcom/samsung/location/a/c;->o()V

    return-void
.end method

.method private a(ILcom/samsung/location/a/e;)I
    .locals 8

    const/4 v0, -0x4

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    :try_start_0
    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1}, Lcom/samsung/location/a/g;->d()I

    move-result v1

    const/16 v4, 0x3c

    if-lt v1, v4, :cond_0

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v4, "add receiver failed : TOO_MANY_GEOFENCES"

    const/4 v5, 0x2

    const/4 v6, 0x1

    invoke-static {v1, v4, v5, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    move p1, v0

    :goto_0
    return p1

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->a()I

    move-result v4

    invoke-virtual {v1, v4, p1, p2}, Lcom/samsung/location/a/g;->a(IILcom/samsung/location/a/e;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v4, "add receiver failed"

    const/4 v5, 0x2

    const/4 v6, 0x1

    invoke-static {v1, v4, v5, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    move p1, v0

    goto :goto_0

    :cond_1
    :try_start_2
    iget-object v4, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    iget-object v1, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/location/lib/j;->a(ILcom/samsung/location/a/e;)V

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v1, p0, Lcom/samsung/location/a/c;->s:Lcom/samsung/location/a/a;

    invoke-virtual {v1}, Lcom/samsung/location/a/a;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/location/a/c;->s:Lcom/samsung/location/a/a;

    invoke-virtual {v1, p1}, Lcom/samsung/location/a/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    const/4 v4, 0x1

    invoke-virtual {v1, p1, v4}, Lcom/samsung/location/a/g;->c(II)V

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v4, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v5, 0x68

    const/16 v6, 0x64

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_2
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catch_0
    move-exception v1

    :try_start_7
    sget-object v4, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "add geopointgeofence got exception "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    const/4 v7, 0x1

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1, p1}, Lcom/samsung/location/a/g;->c(I)Z

    iget-object v1, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {v1, p1}, Lcom/samsung/location/lib/j;->e(I)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    move p1, v0

    goto :goto_0

    :catchall_1
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method private a(ILjava/lang/String;)I
    .locals 10

    const/4 v0, -0x4

    const/4 v9, 0x1

    const/4 v8, 0x2

    if-eqz p2, :cond_4

    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "wifi geofence added : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-direct {p0, p2}, Lcom/samsung/location/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v1, Lcom/samsung/location/a/e;

    const/4 v3, 0x2

    invoke-direct {v1, v3, p2}, Lcom/samsung/location/a/e;-><init>(ILjava/lang/String;)V

    :goto_0
    iget-object v3, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1}, Lcom/samsung/location/a/e;->a()I

    move-result v4

    invoke-virtual {v3, v4, p1, v1}, Lcom/samsung/location/a/g;->a(IILcom/samsung/location/a/e;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v2, "add receiver failed"

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    move p1, v0

    :goto_1
    return p1

    :cond_0
    new-instance v1, Lcom/samsung/location/a/e;

    invoke-direct {v1, v2, p2}, Lcom/samsung/location/a/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "add Wifigeofence got exception "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v8, v9}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1, p1}, Lcom/samsung/location/a/g;->c(I)Z

    iget-object v1, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {v1, p1}, Lcom/samsung/location/lib/j;->e(I)Z

    move p1, v0

    goto :goto_1

    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    const/4 v4, 0x1

    invoke-virtual {v3, p1, v4}, Lcom/samsung/location/a/g;->c(II)V

    if-nez v2, :cond_2

    iget-object v3, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v4, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v5, 0x66

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lcom/samsung/location/lib/h;->obtainMessage(III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    iget-object v3, p0, Lcom/samsung/location/a/c;->a:Ljava/util/List;

    const/4 v4, 0x1

    invoke-direct {p0, p1, v3, v4}, Lcom/samsung/location/a/c;->a(ILjava/util/List;Z)V

    :cond_2
    iget-object v3, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    iget-object v4, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {v4, p1, v1}, Lcom/samsung/location/lib/j;->a(ILcom/samsung/location/a/e;)V

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    iget-object v2, p0, Lcom/samsung/location/a/c;->d:Ljava/util/List;

    const/4 v4, 0x2

    invoke-virtual {v1, p1, v2, v4}, Lcom/samsung/location/lib/j;->a(ILjava/util/List;I)V

    :cond_3
    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1

    :cond_4
    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v2, "couldn\'t add geofence : bssid is null"

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const/4 p1, -0x2

    goto :goto_1
.end method

.method static synthetic a(Lcom/samsung/location/a/c;)I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/c;->E:I

    return v0
.end method

.method static synthetic a(Lcom/samsung/location/a/c;I)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/location/a/c;->d(I)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;)Lcom/samsung/location/a/c;
    .locals 4

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "getInstance"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    sget-object v0, Lcom/samsung/location/a/c;->ad:Lcom/samsung/location/a/c;

    if-nez v0, :cond_0

    if-eqz p0, :cond_0

    new-instance v0, Lcom/samsung/location/a/c;

    invoke-direct {v0, p0}, Lcom/samsung/location/a/c;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/location/a/c;->ad:Lcom/samsung/location/a/c;

    :cond_0
    sget-object v0, Lcom/samsung/location/a/c;->ad:Lcom/samsung/location/a/c;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x0

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBssid : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->d:Ljava/util/List;

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v2, "tempWifiList is null"

    invoke-static {v0, v2, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/a/c;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    iget-object v3, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBssid match ssid : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    goto :goto_0
.end method

.method private a(ILjava/util/List;Z)V
    .locals 4

    if-eqz p3, :cond_2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "already exist id"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-interface {p2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method private a(IZ)V
    .locals 8

    const-wide/32 v6, 0xea60

    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleDetectedEnter : ID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / direction = DIRECTION_ENTER"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    if-eqz p2, :cond_2

    iget v0, p0, Lcom/samsung/location/a/c;->E:I

    if-nez v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/location/a/c;->U:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/a/c;->Z:Landroid/location/Location;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "The last NLP location is still available, do not trigger NLP"

    invoke-static {v0, v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Z:Landroid/location/Location;

    invoke-virtual {v0, v1}, Lcom/samsung/location/a/g;->a(Landroid/location/Location;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x68

    const/16 v3, 0x64

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/location/a/c;->s:Lcom/samsung/location/a/a;

    invoke-virtual {v0}, Lcom/samsung/location/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/g;->d(I)I

    move-result v0

    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lcom/samsung/location/a/c;->s:Lcom/samsung/location/a/a;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/a;->b(I)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/location/a/c;->s:Lcom/samsung/location/a/a;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/a;->d(I)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/g;->d(I)I

    move-result v0

    if-ne v0, v4, :cond_5

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x130

    invoke-virtual {v1, v2, p1, v3}, Lcom/samsung/location/lib/h;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v7}, Lcom/samsung/location/lib/h;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_4
    :goto_1
    iget v0, p0, Lcom/samsung/location/a/c;->E:I

    if-ne v0, v4, :cond_6

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/g;->h(I)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/g;->d(I)I

    move-result v0

    if-le v0, v4, :cond_6

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/location/a/c;->e(ILcom/samsung/location/a/e;)I

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/g;->d(I)I

    move-result v0

    if-lt v0, v4, :cond_4

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x131

    invoke-virtual {v1, v2, p1, v3}, Lcom/samsung/location/lib/h;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v7}, Lcom/samsung/location/lib/h;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/location/a/c;->E:I

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/location/a/c;->d(I)I

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/samsung/location/a/c;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/samsung/location/a/c;->a(IZ)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->e()I

    move-result v0

    if-lez v0, :cond_6

    if-eqz p1, :cond_6

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0}, Lcom/samsung/location/a/l;->f()I

    move-result v0

    if-lez v0, :cond_3

    iget v0, p0, Lcom/samsung/location/a/c;->E:I

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->x:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0, v1, v1}, Lcom/samsung/location/lib/SContentManager;->a(II)V

    :cond_0
    invoke-direct {p0}, Lcom/samsung/location/a/c;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lcom/samsung/location/a/c;->x:Z

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "SensorHub batching registered"

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-boolean v2, p0, Lcom/samsung/location/a/c;->x:Z

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "SensorHub batching register failed"

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/samsung/location/a/c;->x:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0, v3, v1}, Lcom/samsung/location/lib/SContentManager;->a(II)V

    :cond_4
    const/16 v0, 0x258

    invoke-direct {p0, v0}, Lcom/samsung/location/a/c;->e(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iput-boolean v1, p0, Lcom/samsung/location/a/c;->x:Z

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "SensorHub ex registered"

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_5
    iput-boolean v2, p0, Lcom/samsung/location/a/c;->x:Z

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "SensorHub ex register failed"

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->e()I

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->x:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0, v1, v1}, Lcom/samsung/location/lib/SContentManager;->a(II)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0, v3, v1}, Lcom/samsung/location/lib/SContentManager;->a(II)V

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "SensorHub-unregistered"

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iput-boolean v2, p0, Lcom/samsung/location/a/c;->x:Z

    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/samsung/location/a/c;->D:I

    invoke-direct {p0}, Lcom/samsung/location/a/c;->q()V

    goto :goto_0
.end method

.method private b(ILcom/samsung/location/a/e;)I
    .locals 8

    const/4 v0, -0x4

    const/4 v1, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    sget-object v2, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "startGeoPointGeofence ID = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    :try_start_0
    iget-object v4, p0, Lcom/samsung/location/a/c;->s:Lcom/samsung/location/a/a;

    invoke-virtual {v4, p1}, Lcom/samsung/location/a/a;->g(I)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v5, 0x130

    invoke-virtual {v4, v5}, Lcom/samsung/location/lib/h;->removeMessages(I)V

    sget-object v4, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v5, "remove SWorkerHandler.MSG_CELL_COLLECT_CANCEL, keep collecting cell."

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :cond_0
    iget-object v4, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v5, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v6, 0x70

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    iget-object v4, p0, Lcom/samsung/location/a/c;->s:Lcom/samsung/location/a/a;

    invoke-virtual {v4, p1}, Lcom/samsung/location/a/a;->b(I)V

    iget-boolean v4, p0, Lcom/samsung/location/a/c;->y:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v5, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v6, 0x68

    const/16 v7, 0x64

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    sget-object v4, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v5, "Session paused, don\'t trigger new session"

    const/4 v6, 0x2

    const/4 v7, 0x1

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    move v0, v1

    :goto_1
    return v0

    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/samsung/location/a/c;->s:Lcom/samsung/location/a/a;

    invoke-virtual {v4, p1}, Lcom/samsung/location/a/a;->h(I)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v4}, Lcom/samsung/location/lib/SContentManager;->b()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v5, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v6, 0x68

    const/16 v7, 0x64

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    :goto_2
    iget-boolean v4, p0, Lcom/samsung/location/a/c;->v:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v4}, Lcom/samsung/location/a/l;->f()I

    move-result v4

    if-nez v4, :cond_3

    invoke-direct {p0, p1, p2}, Lcom/samsung/location/a/c;->e(ILcom/samsung/location/a/e;)I

    move-result v4

    if-eqz v4, :cond_4

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v4, "addGeofence exception"

    const/4 v5, 0x5

    const/4 v6, 0x1

    invoke-static {v1, v4, v5, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    :cond_2
    :try_start_2
    sget-object v4, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v5, "data is currently unavailable"

    const/4 v6, 0x2

    const/4 v7, 0x1

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v4, p0, Lcom/samsung/location/a/c;->b:Ljava/util/List;

    const/4 v5, 0x1

    invoke-direct {p0, p1, v4, v5}, Lcom/samsung/location/a/c;->a(ILjava/util/List;Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v1

    :try_start_3
    sget-object v4, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "start geopoint geofence got exception"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    const/4 v7, 0x1

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v1, p1, v4, v5}, Lcom/samsung/location/a/g;->a(ILandroid/app/PendingIntent;Z)Z

    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    const/4 v4, 0x0

    invoke-virtual {v1, p1, v4}, Lcom/samsung/location/a/g;->c(II)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_1

    :cond_3
    :try_start_4
    sget-object v4, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v5, "GPS : unavailable, so start NLP"

    const/4 v6, 0x2

    const/4 v7, 0x1

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v4, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/samsung/location/a/l;->a(Z)V

    :cond_4
    :goto_3
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/samsung/location/a/c;->a(Z)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0

    :cond_5
    :try_start_5
    sget-object v4, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ID "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " has a Cell DB. Wait for cell event..."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3
.end method

.method private b(ILjava/lang/String;)I
    .locals 7

    const/4 v0, -0x4

    const/4 v6, 0x2

    if-eqz p2, :cond_1

    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "BT geofence added : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    new-instance v1, Lcom/samsung/location/a/e;

    const/4 v2, 0x3

    invoke-direct {v1, v2, p2}, Lcom/samsung/location/a/e;-><init>(ILjava/lang/String;)V

    sget-object v2, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "param.gettype() :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/location/a/e;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v2, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1}, Lcom/samsung/location/a/e;->a()I

    move-result v3

    invoke-virtual {v2, v3, p1, v1}, Lcom/samsung/location/a/g;->a(IILcom/samsung/location/a/e;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v2, "add receiver failed"

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    move p1, v0

    :goto_0
    return p1

    :cond_0
    iget-object v2, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v3, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {v3, p1, v1}, Lcom/samsung/location/lib/j;->a(ILcom/samsung/location/a/e;)V

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "add BT geofence got exception "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-static {v2, v1, v6, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1, p1}, Lcom/samsung/location/a/g;->c(I)Z

    iget-object v1, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {v1, p1}, Lcom/samsung/location/lib/j;->e(I)Z

    move p1, v0

    goto :goto_0

    :cond_1
    :try_start_3
    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v2, "couldn\'t add geofence : bssid is null"

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const/4 p1, -0x2

    goto :goto_0
.end method

.method static synthetic b(Lcom/samsung/location/a/c;)Lcom/samsung/location/lib/h;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    return-object v0
.end method

.method private b(IZ)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleDetectedExit : ID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / direction = DIRECTION_EXIT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    if-eqz p2, :cond_2

    iget v0, p0, Lcom/samsung/location/a/c;->E:I

    if-nez v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/location/a/c;->U:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/a/c;->Z:Landroid/location/Location;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "The last NLP location is still available, do not trigger NLP"

    invoke-static {v0, v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Z:Landroid/location/Location;

    invoke-virtual {v0, v1}, Lcom/samsung/location/a/g;->a(Landroid/location/Location;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x68

    const/16 v3, 0x64

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/location/a/c;->s:Lcom/samsung/location/a/a;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/a;->e(I)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0}, Lcom/samsung/location/a/l;->f()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0, v4}, Lcom/samsung/location/a/l;->a(Z)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/samsung/location/a/c;->E:I

    if-eqz v0, :cond_4

    invoke-direct {p0, p1}, Lcom/samsung/location/a/c;->d(I)I

    :cond_4
    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_0

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "fake event"

    invoke-static {v0, v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {p0}, Lcom/samsung/location/a/c;->m()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/samsung/location/a/c;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/location/a/c;->E:I

    return-void
.end method

.method static synthetic b(Lcom/samsung/location/a/c;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/samsung/location/a/c;->b(IZ)V

    return-void
.end method

.method private c(ILcom/samsung/location/a/e;)I
    .locals 5

    const/4 v0, 0x0

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "startWifiGeofence ID = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/location/a/c;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :goto_0
    return v0

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v0, p1, v3, v4}, Lcom/samsung/location/a/g;->a(ILandroid/app/PendingIntent;Z)Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v3}, Lcom/samsung/location/a/g;->c(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    const/4 v0, -0x4

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method static synthetic c(Lcom/samsung/location/a/c;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/location/a/c;->f(I)V

    return-void
.end method

.method private d(I)I
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removeHardwareGeofence ID = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    iget-object v2, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/samsung/location/a/c;->u:Landroid/location/IGpsGeofenceHardware;

    add-int/lit16 v2, p1, 0x2710

    invoke-interface {v1, v2}, Landroid/location/IGpsGeofenceHardware;->removeHardwareGeofence(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removeHardwareGeofence error"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/16 v0, -0x64

    goto :goto_0
.end method

.method private d(ILcom/samsung/location/a/e;)I
    .locals 5

    const/4 v0, 0x0

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "startBTGeofence ID = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v1

    :try_start_0
    iget-object v3, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    iget-object v4, p0, Lcom/samsung/location/a/c;->e:Ljava/util/List;

    invoke-virtual {v3, v4}, Lcom/samsung/location/a/g;->c(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :goto_0
    return v0

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v0, p1, v3, v4}, Lcom/samsung/location/a/g;->a(ILandroid/app/PendingIntent;Z)Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v3}, Lcom/samsung/location/a/g;->c(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    const/4 v0, -0x4

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1, v2}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method static synthetic d(Lcom/samsung/location/a/c;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/location/a/c;->g(I)V

    return-void
.end method

.method private e(ILcom/samsung/location/a/e;)I
    .locals 12

    iget-object v0, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "already running hw geofence "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/16 v0, -0x64

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/location/a/c;->E:I

    if-nez p2, :cond_2

    iget-object v1, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {v0, p1}, Lcom/samsung/location/lib/j;->c(I)Lcom/samsung/location/a/e;

    move-result-object p2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p2, :cond_1

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "null param add request for HW geofence ID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/16 v0, -0x64

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    invoke-virtual {p2}, Lcom/samsung/location/a/e;->a()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "illegal param add request for HW geofence ID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/16 v0, -0x64

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1, p1}, Lcom/samsung/location/a/g;->g(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, Lcom/samsung/location/a/g;->d(II)I

    move-result v0

    move v6, v0

    :goto_1
    if-lez v6, :cond_4

    :try_start_2
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addHardwareGeofence ID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " radius : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->u:Landroid/location/IGpsGeofenceHardware;

    add-int/lit16 v1, p1, 0x2710

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->b()D

    move-result-wide v2

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->c()D

    move-result-wide v4

    int-to-double v6, v6

    const/4 v8, 0x4

    const/4 v9, 0x3

    const/16 v10, 0x1388

    const/16 v11, 0x7530

    invoke-interface/range {v0 .. v11}, Landroid/location/IGpsGeofenceHardware;->addCircularHardwareGeofence(IDDDIIII)Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1, p1}, Lcom/samsung/location/a/g;->g(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/samsung/location/a/g;->d(II)I

    move-result v0

    move v6, v0

    goto :goto_1

    :cond_4
    :try_start_3
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addHardwareGeofence ID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " radius : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->u:Landroid/location/IGpsGeofenceHardware;

    add-int/lit16 v1, p1, 0x2710

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->b()D

    move-result-wide v2

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->c()D

    move-result-wide v4

    invoke-virtual {p2}, Lcom/samsung/location/a/e;->d()I

    move-result v6

    int-to-double v6, v6

    const/4 v8, 0x4

    const/4 v9, 0x3

    const/16 v10, 0x1388

    const/16 v11, 0x7530

    invoke-interface/range {v0 .. v11}, Landroid/location/IGpsGeofenceHardware;->addCircularHardwareGeofence(IDDDIIII)Z
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addHardwareGeofence error"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v1, v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/16 v0, -0x64

    goto/16 :goto_0

    :cond_5
    move v6, v0

    goto/16 :goto_1
.end method

.method private e(I)Z
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0, v5, v4}, Lcom/samsung/location/lib/SContentManager;->a(II)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/16 v0, 0x258

    if-ne p1, v0, :cond_0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    const/16 v0, 0x3c

    if-ne p1, v0, :cond_1

    const/16 v0, 0x1e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "EX with duration "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [I

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v3

    if-lt v1, v0, :cond_2

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0, v3, p1}, Lcom/samsung/location/lib/SContentManager;->a([II)Z

    move-result v0

    return v0

    :cond_2
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private f(I)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x2

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleDetectedCellEnter : ID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / direction = DIRECTION_CELL_ENTER"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->y:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/g;->d(I)I

    move-result v0

    if-ge v0, v3, :cond_1

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onInternalGeofenceDetected : ID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not started yet.."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/samsung/location/a/c;->v:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/g;->f(I)Lcom/samsung/location/a/e;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/samsung/location/a/c;->e(ILcom/samsung/location/a/e;)I

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "GPS is off, so start NLP"

    invoke-static {v0, v1, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/location/a/l;->a(Z)V

    goto :goto_0
.end method

.method private g(I)V
    .locals 9

    const/4 v8, 0x2

    const/4 v2, 0x0

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "handleDetectedCellExit : ID = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " / direction = DIRECTION_CELL_EXIT"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->y:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->f()Ljava/util/List;

    move-result-object v3

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/g;->d(I)I

    move-result v0

    if-ge v0, v8, :cond_2

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "onInternalGeofenceDetected : ID = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is not started yet.."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/location/a/c;->d(I)I

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ne v1, v0, :cond_0

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "ALL running client are out of cell boundary, stop session"

    invoke-static {v0, v1, v8, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget v0, p0, Lcom/samsung/location/a/c;->E:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x6b

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v5, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v6, p0, Lcom/samsung/location/a/c;->s:Lcom/samsung/location/a/a;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/location/a/a;->h(I)Z

    move-result v6

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v5, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v5, v0}, Lcom/samsung/location/a/g;->h(I)I

    move-result v0

    const/4 v5, 0x4

    if-ne v0, v5, :cond_3

    if-eqz v6, :cond_3

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic n()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    return-object v0
.end method

.method private o()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/location/a/c;->X:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/samsung/location/a/c;->p:Landroid/net/wifi/WifiManager;

    invoke-static {}, Lcom/samsung/location/lib/j;->a()Lcom/samsung/location/lib/j;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-static {}, Lcom/samsung/location/a/g;->a()Lcom/samsung/location/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    iget-object v1, p0, Lcom/samsung/location/a/c;->ae:Lcom/samsung/location/a/d;

    invoke-virtual {v0, v1}, Lcom/samsung/location/a/g;->a(Lcom/samsung/location/a/b;)Z

    invoke-static {}, Lcom/samsung/location/a/l;->a()Lcom/samsung/location/a/l;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    iget-object v0, p0, Lcom/samsung/location/a/c;->X:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->a(Landroid/content/Context;)Lcom/samsung/location/lib/SContentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-static {}, Lcom/samsung/location/a/a;->a()Lcom/samsung/location/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/c;->s:Lcom/samsung/location/a/a;

    invoke-static {}, Lcom/samsung/location/lib/h;->a()Lcom/samsung/location/lib/h;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    iget-object v1, p0, Lcom/samsung/location/a/c;->X:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/samsung/location/a/l;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/SContentManager;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/location/a/c;->v:Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/SContentManager;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/location/a/c;->w:Z

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->w:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/samsung/location/a/c;->D:I

    invoke-direct {p0}, Lcom/samsung/location/a/c;->q()V

    :cond_0
    invoke-static {}, Lcom/samsung/location/lib/b;->c()V

    return-void
.end method

.method private p()V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :goto_1
    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    sget-object v2, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "removeHardwareGeofence ID = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v2, p0, Lcom/samsung/location/a/c;->u:Landroid/location/IGpsGeofenceHardware;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit16 v0, v0, 0x2710

    invoke-interface {v2, v0}, Landroid/location/IGpsGeofenceHardware;->removeHardwareGeofence(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removeHardwareGeofence error"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v7, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_1
.end method

.method private q()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x2

    iget-object v1, p0, Lcom/samsung/location/a/c;->ac:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/location/a/c;->y:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    if-nez v0, :cond_5

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/location/a/c;->y:Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->e()I

    move-result v0

    if-nez v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v2, "Resume Session"

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v2, 0x1e

    if-le v0, v2, :cond_1

    iget-object v0, p0, Lcom/samsung/location/a/c;->g:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Lcom/samsung/location/a/c;->g:Ljava/util/List;

    iget v2, p0, Lcom/samsung/location/a/c;->D:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const-string v2, "[%04d%02d%02d%02d%02d%02d] "

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const/16 v5, 0xb

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const/16 v5, 0xc

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const/16 v5, 0xd

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/c;->W:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->v:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lcom/samsung/location/a/c;->Z:Landroid/location/Location;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/location/a/c;->Z:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7530

    cmp-long v0, v2, v4

    if-gez v0, :cond_4

    monitor-exit v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v3, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/location/a/g;->h(I)I

    move-result v3

    if-eq v3, v7, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, 0x0

    invoke-direct {p0, v0, v3}, Lcom/samsung/location/a/c;->e(ILcom/samsung/location/a/e;)I

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v2, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v3, 0x68

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    :goto_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/location/a/c;->a(Z)V

    :cond_5
    iget-boolean v0, p0, Lcom/samsung/location/a/c;->y:Z

    if-nez v0, :cond_9

    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/location/a/c;->y:Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->e()I

    move-result v0

    if-nez v0, :cond_7

    monitor-exit v1

    goto/16 :goto_0

    :cond_6
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v2, "GPS is off, so start NLP"

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/samsung/location/a/l;->a(Z)V

    goto :goto_2

    :cond_7
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pause Session : Reason = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/location/a/c;->D:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v2, 0x1e

    if-le v0, v2, :cond_8

    iget-object v0, p0, Lcom/samsung/location/a/c;->g:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_8
    iget-object v0, p0, Lcom/samsung/location/a/c;->g:Ljava/util/List;

    iget v2, p0, Lcom/samsung/location/a/c;->D:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const-string v2, "[%04d%02d%02d%02d%02d%02d] "

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const/16 v5, 0xb

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const/16 v5, 0xc

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const/16 v5, 0xd

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/c;->W:Ljava/lang/String;

    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_a

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v2, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v3, 0x68

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    :goto_3
    invoke-direct {p0}, Lcom/samsung/location/a/c;->p()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/location/a/c;->E:I

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v2, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v3, 0x6b

    invoke-virtual {v2, v3}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    :cond_9
    monitor-exit v1

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x6a

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/h;->removeMessages(I)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x68

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/h;->removeMessages(I)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x69

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/h;->removeMessages(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/location/a/c;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3
.end method

.method private r()Z
    .locals 2

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/SContentManager;->b(I)Z

    move-result v0

    return v0
.end method

.method private s()V
    .locals 3

    iget v0, p0, Lcom/samsung/location/a/c;->E:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    const-wide/32 v1, 0x493e0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/location/a/l;->a(J)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 5

    const/4 v0, 0x0

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "removeGeofence ID = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    if-gez p1, :cond_0

    const/4 v0, -0x3

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/samsung/location/a/c;->s:Lcom/samsung/location/a/a;

    invoke-virtual {v2, p1}, Lcom/samsung/location/a/a;->f(I)V

    iget-object v2, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {v2, p1}, Lcom/samsung/location/lib/j;->e(I)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1, p1}, Lcom/samsung/location/a/g;->c(I)Z

    invoke-direct {p0, p1}, Lcom/samsung/location/a/c;->d(I)I

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, -0x4

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v1, v0}, Lcom/samsung/location/a/l;->a(Z)V

    iget-boolean v1, p0, Lcom/samsung/location/a/c;->x:Z

    if-eqz v1, :cond_2

    invoke-direct {p0, v0}, Lcom/samsung/location/a/c;->a(Z)V

    :cond_2
    iget-object v1, p0, Lcom/samsung/location/a/c;->b:Ljava/util/List;

    invoke-direct {p0, p1, v1, v0}, Lcom/samsung/location/a/c;->a(ILjava/util/List;Z)V

    iget-object v1, p0, Lcom/samsung/location/a/c;->a:Ljava/util/List;

    invoke-direct {p0, p1, v1, v0}, Lcom/samsung/location/a/c;->a(ILjava/util/List;Z)V

    goto :goto_0
.end method

.method public a(ILandroid/app/PendingIntent;)I
    .locals 7

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v0, -0x4

    const/4 v5, 0x2

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "startGeofence ID = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getCreatorPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Lcom/samsung/location/lib/j;->b(ILjava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {v2, p1}, Lcom/samsung/location/lib/j;->c(I)Lcom/samsung/location/a/e;

    move-result-object v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v2, "parameter is null "

    invoke-static {v1, v2, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :cond_0
    :goto_0
    return v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v2}, Lcom/samsung/location/a/e;->a()I

    move-result v3

    invoke-virtual {v1, v3, p1, v2}, Lcom/samsung/location/a/g;->a(IILcom/samsung/location/a/e;)Z

    if-eqz p2, :cond_2

    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1, p1, p2, v6}, Lcom/samsung/location/a/g;->a(ILandroid/app/PendingIntent;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1, p1, v5}, Lcom/samsung/location/a/g;->c(II)V

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "parameter "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/samsung/location/a/e;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v5, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {v2}, Lcom/samsung/location/a/e;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, p1, v2}, Lcom/samsung/location/a/c;->b(ILcom/samsung/location/a/e;)I

    move-result v0

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1, v2}, Lcom/samsung/location/a/c;->c(ILcom/samsung/location/a/e;)I

    move-result v0

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1, v2}, Lcom/samsung/location/a/c;->d(ILcom/samsung/location/a/e;)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/samsung/location/SLocationParameter;)I
    .locals 14

    const/4 v13, 0x1

    const/4 v12, 0x2

    const/4 v6, -0x4

    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/location/SLocationParameter;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Lcom/samsung/location/SLocationParameter;->getLongitude()D

    move-result-wide v3

    invoke-virtual {p1}, Lcom/samsung/location/SLocationParameter;->getRadius()I

    move-result v5

    invoke-virtual {p1}, Lcom/samsung/location/SLocationParameter;->getBssid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/location/SLocationParameter;->getType()I

    move-result v7

    sget-object v8, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "addGeofence : "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    const/4 v11, 0x0

    invoke-static {v8, v9, v10, v11}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    sget-object v8, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "add geofenceType "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    const/4 v11, 0x1

    invoke-static {v8, v9, v10, v11}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v8, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v9, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    iget-object v10, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {v10}, Lcom/samsung/location/lib/j;->c()Ljava/util/List;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/samsung/location/a/g;->a(Ljava/util/List;)I

    move-result v9

    monitor-exit v8

    if-gez v9, :cond_0

    move v0, v6

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "add geofence got exception "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v12, v13}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    move v0, v6

    goto :goto_0

    :cond_0
    packed-switch v7, :pswitch_data_0

    move v0, v6

    goto :goto_0

    :pswitch_0
    :try_start_3
    new-instance v0, Lcom/samsung/location/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/samsung/location/a/e;-><init>(DDI)V

    invoke-direct {p0, v9, v0}, Lcom/samsung/location/a/c;->a(ILcom/samsung/location/a/e;)I

    move-result v0

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v9, v0}, Lcom/samsung/location/a/c;->a(ILjava/lang/String;)I

    move-result v0

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v9, v0}, Lcom/samsung/location/a/c;->b(ILjava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a([I)I
    .locals 9

    const/4 v8, 0x2

    const/4 v1, 0x0

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v2, "sync geofence"

    invoke-static {v0, v2, v8, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, -0x2

    :goto_0
    return v0

    :cond_1
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    :goto_1
    array-length v2, p1

    if-lt v0, v2, :cond_3

    const-string v2, ""

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    iget-object v0, p0, Lcom/samsung/location/a/c;->X:Landroid/content/Context;

    const-string v5, "activity"

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "process name"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    iget-object v0, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/j;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_0

    :cond_3
    aget v2, p1, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v6, v4, :cond_2

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    move-object v2, v0

    goto :goto_2

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    sget-object v4, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "delete id"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/location/a/c;->a(I)I

    :cond_6
    sget-object v4, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sync id"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static {v4, v0, v5, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    sget-object v2, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "syncGeofence got exception : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v8, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, -0x4

    goto/16 :goto_0
.end method

.method public a()V
    .locals 4

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "requestWifiScan"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/location/a/c;->B:Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->p:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x67

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/location/lib/h;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public a(II)V
    .locals 5

    const/4 v4, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez p2, :cond_1

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "handleAddCallback : GEOFENCE_SUCCESS"

    invoke-static {v0, v1, v4, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "handleAddCallback : GEOFENCE_ERROR"

    invoke-static {v0, v1, v4, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    add-int/lit16 v0, p1, -0x2710

    iget-object v1, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    iget-object v2, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_2
    iget-object v0, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "there is no hw gps client"

    const/4 v1, 0x2

    invoke-static {v0, v1, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iput v3, p0, Lcom/samsung/location/a/c;->E:I

    goto :goto_0
.end method

.method public a(IILandroid/location/Location;)V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x1

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleTransitionCallback : ID = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleTransitionCallback : lat = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / lon = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/location/Location;->setProvider(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x6a

    invoke-virtual {v1, v2, p3}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    add-int/lit16 v1, p1, -0x2710

    invoke-virtual {v0, v1, p2}, Lcom/samsung/location/a/g;->b(II)V

    return-void
.end method

.method public a(ILandroid/location/Location;)V
    .locals 4

    const/4 v2, 0x4

    const/4 v3, 0x1

    if-ne p1, v3, :cond_1

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "handleStatusCallback : GEOFENCE_UNAVAILABLE"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget v0, p0, Lcom/samsung/location/a/c;->E:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/location/a/c;->E:I

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0, v3}, Lcom/samsung/location/a/l;->a(Z)V

    invoke-direct {p0}, Lcom/samsung/location/a/c;->p()V

    :goto_1
    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/g;->b(I)V

    invoke-direct {p0, v3}, Lcom/samsung/location/a/c;->a(Z)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "handleStatusCallback : GEOFENCE_AVAILABLE"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iput v3, p0, Lcom/samsung/location/a/c;->E:I

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x6b

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method

.method public a(Landroid/content/Intent;)V
    .locals 5

    const/4 v4, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "level"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "plugged"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v3, v1}, Lcom/samsung/location/a/l;->b(Z)V

    if-gt v2, v0, :cond_0

    move v0, v1

    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/samsung/location/a/c;->z:Z

    if-eq v2, v0, :cond_1

    sget-object v2, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v3, "battery state changed"

    const/4 v4, 0x2

    invoke-static {v2, v3, v4, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iput-boolean v0, p0, Lcom/samsung/location/a/c;->z:Z

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->z:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/samsung/location/a/c;->D:I

    :goto_1
    invoke-direct {p0}, Lcom/samsung/location/a/c;->q()V

    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v2, v0}, Lcom/samsung/location/a/l;->b(Z)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/location/a/c;->D:I

    goto :goto_1
.end method

.method public a(Landroid/content/Intent;Z)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    :try_start_0
    const-string v0, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_3

    iget-object v1, p0, Lcom/samsung/location/a/c;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/location/a/c;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bssid : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " connected"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :goto_1
    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    iget-object v1, p0, Lcom/samsung/location/a/c;->e:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/samsung/location/a/g;->c(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkBTInfo got exception "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/samsung/location/a/c;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/location/a/c;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_4
    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bt "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " disconnected"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public a(Landroid/location/IGpsGeofenceHardware;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/a/c;->u:Landroid/location/IGpsGeofenceHardware;

    return-void
.end method

.method public a(Ljava/io/PrintWriter;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "recent slocation ex status: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/location/a/c;->f:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/a/c;->V:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "recent slocation status: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/location/a/c;->g:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/a/c;->W:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    return-void
.end method

.method public a(ZZ)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0, v5}, Lcom/samsung/location/lib/SContentManager;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/location/a/c;->v:Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0, v3}, Lcom/samsung/location/lib/SContentManager;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/location/a/c;->w:Z

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isGPSon : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/location/a/c;->v:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isNLPon : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/location/a/c;->w:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->c()V

    :cond_0
    iget-boolean v0, p0, Lcom/samsung/location/a/c;->w:Z

    if-nez v0, :cond_3

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "NLP off - geopoint geofence paused"

    invoke-static {v0, v1, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    if-nez p2, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/samsung/location/a/c;->D:I

    invoke-direct {p0}, Lcom/samsung/location/a/c;->q()V

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_4

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->w:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "NLP on - resume geopoint geofence"

    invoke-static {v0, v1, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/samsung/location/a/c;->D:I

    invoke-direct {p0}, Lcom/samsung/location/a/c;->q()V

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lcom/samsung/location/a/c;->v:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x6b

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/samsung/location/a/c;->e(ILcom/samsung/location/a/e;)I

    goto :goto_1

    :cond_6
    iput v4, p0, Lcom/samsung/location/a/c;->E:I

    invoke-direct {p0}, Lcom/samsung/location/a/c;->p()V

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->e()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0, v5}, Lcom/samsung/location/a/l;->a(Z)V

    goto :goto_0
.end method

.method public b(ILandroid/app/PendingIntent;)I
    .locals 7

    const/4 v6, 0x2

    const/4 v0, -0x4

    const/4 v5, 0x1

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "stopGeofence ID = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    if-gez p1, :cond_0

    const/4 v0, -0x3

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    :try_start_0
    iget-object v4, p0, Lcom/samsung/location/a/c;->s:Lcom/samsung/location/a/a;

    invoke-virtual {v4, p1}, Lcom/samsung/location/a/a;->e(I)V

    iget-object v4, p0, Lcom/samsung/location/a/c;->s:Lcom/samsung/location/a/a;

    invoke-virtual {v4, p1}, Lcom/samsung/location/a/a;->c(I)V

    if-eqz p2, :cond_1

    iget-object v4, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    const/4 v5, 0x1

    invoke-virtual {v4, p1, p2, v5}, Lcom/samsung/location/a/g;->a(ILandroid/app/PendingIntent;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    const/4 v0, -0x2

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Lcom/samsung/location/a/g;->c(II)V

    invoke-direct {p0, p1}, Lcom/samsung/location/a/c;->d(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :cond_2
    :try_start_2
    iget-object v4, p0, Lcom/samsung/location/a/c;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x0

    iput v4, p0, Lcom/samsung/location/a/c;->E:I

    :cond_3
    iget-object v4, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/samsung/location/a/l;->a(Z)V

    iget-object v4, p0, Lcom/samsung/location/a/c;->b:Ljava/util/List;

    const/4 v5, 0x0

    invoke-direct {p0, p1, v4, v5}, Lcom/samsung/location/a/c;->a(ILjava/util/List;Z)V

    iget-object v4, p0, Lcom/samsung/location/a/c;->a:Ljava/util/List;

    const/4 v5, 0x0

    invoke-direct {p0, p1, v4, v5}, Lcom/samsung/location/a/c;->a(ILjava/util/List;Z)V

    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/samsung/location/a/c;->a(Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_3
    sget-object v4, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "stopGeofence got exception : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x2

    const/4 v6, 0x1

    invoke-static {v4, v1, v5, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public b()V
    .locals 6

    const/4 v5, 0x2

    iget-object v0, p0, Lcom/samsung/location/a/c;->p:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "connected wifi : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Lcom/samsung/location/a/g;->a(ILjava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "wifi disconnected"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/location/a/g;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "checkWifiInfo got exception "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v1, v0, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 8

    const/4 v7, 0x5

    const/4 v4, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/samsung/location/a/c;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x1e

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/c;->f:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/a/c;->f:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const-string v1, "[%04d%02d%02d%02d%02d%02d] "

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    const/4 v3, 0x3

    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const/16 v4, 0xc

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0xd

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/c;->V:Ljava/lang/String;

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "ACTIVITY_STATUS_STATIONARY"

    invoke-static {v0, v1, v6, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {p0}, Lcom/samsung/location/a/c;->l()V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "ACTIVITY_STATUS_WALK"

    invoke-static {v0, v1, v6, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {p0}, Lcom/samsung/location/a/c;->m()V

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "ACTIVITY_STATUS_RUN"

    invoke-static {v0, v1, v6, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {p0}, Lcom/samsung/location/a/c;->m()V

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "ACTIVITY_STATUS_VEHICLE"

    invoke-static {v0, v1, v6, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {p0}, Lcom/samsung/location/a/c;->m()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b(II)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x4

    if-nez p2, :cond_1

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "handleRemoveCallback : GEOFENCE_SUCCESS"

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    add-int/lit16 v0, p1, -0x2710

    iget-boolean v1, p0, Lcom/samsung/location/a/c;->v:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/samsung/location/a/c;->E:I

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/location/a/c;->y:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1, v0}, Lcom/samsung/location/a/g;->d(I)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1, v0}, Lcom/samsung/location/a/g;->h(I)I

    move-result v1

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1, v0}, Lcom/samsung/location/a/g;->f(I)Lcom/samsung/location/a/e;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/location/a/c;->e(ILcom/samsung/location/a/e;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "handleRemoveCallback : GEOFENCE_ERROR"

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public b(ILandroid/location/Location;)V
    .locals 13

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->d()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Location reported : fix method = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v12, v11}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Lat : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  /  Lon : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v12, v10}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-static {p1, p2}, Lcom/samsung/location/lib/b;->a(ILandroid/location/Location;)V

    iget-object v9, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v9

    :try_start_0
    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v12, :cond_3

    iget-object v0, p0, Lcom/samsung/location/a/c;->Z:Landroid/location/Location;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->A:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    new-array v8, v0, [F

    iget-object v0, p0, Lcom/samsung/location/a/c;->Z:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    iget-object v2, p0, Lcom/samsung/location/a/c;->Z:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {p2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v0, v8, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "rapid location changed during stationary "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    aget v2, v8, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0}, Lcom/samsung/location/a/l;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x66

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/location/lib/h;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    monitor-exit v9

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x68

    const/16 v3, 0x64

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0, p2}, Lcom/samsung/location/a/g;->a(Landroid/location/Location;)V

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->y:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0}, Lcom/samsung/location/a/l;->f()I

    move-result v0

    if-ne v0, v11, :cond_4

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->A:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0, p2}, Lcom/samsung/location/a/l;->a(Landroid/location/Location;)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->c()V

    :cond_4
    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p2}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iput-object v0, p0, Lcom/samsung/location/a/c;->Z:Landroid/location/Location;

    invoke-virtual {p2}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/location/a/c;->U:J

    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->A:Z

    if-eqz v0, :cond_5

    iput-boolean v10, p0, Lcom/samsung/location/a/c;->A:Z

    :cond_5
    if-ne p1, v12, :cond_0

    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    const/high16 v1, 0x43480000    # 200.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0}, Lcom/samsung/location/a/l;->f()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0, v11}, Lcom/samsung/location/a/l;->a(Z)V

    goto/16 :goto_0

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v2, v0}, Lcom/samsung/location/a/g;->f(I)Lcom/samsung/location/a/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/location/a/e;->a()I

    move-result v2

    if-ne v2, v11, :cond_6

    iget-object v2, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v2, v0}, Lcom/samsung/location/a/g;->i(I)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v3, v0}, Lcom/samsung/location/a/g;->f(I)Lcom/samsung/location/a/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/location/a/e;->d()I

    move-result v3

    const/16 v4, 0xc8

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_6

    iget-object v2, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v2, v0}, Lcom/samsung/location/a/g;->d(I)I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-object v4, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "currentTime - mPassiveWifiScanTime : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Lcom/samsung/location/a/c;->S:J

    sub-long v6, v2, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v12, v11}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-wide v4, p0, Lcom/samsung/location/a/c;->S:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x7530

    cmp-long v2, v2, v4

    if-gez v2, :cond_6

    iget-object v2, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v2

    :try_start_2
    iget-object v3, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    iget-object v4, p0, Lcom/samsung/location/a/c;->d:Ljava/util/List;

    const/4 v5, 0x1

    invoke-virtual {v3, v0, v4, v5}, Lcom/samsung/location/lib/j;->a(ILjava/util/List;I)V

    monitor-exit v2

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public c()V
    .locals 7

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {v0}, Lcom/samsung/location/lib/j;->c()Ljava/util/List;

    move-result-object v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v3, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    iget-object v4, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/location/lib/j;->c(I)Lcom/samsung/location/a/e;

    move-result-object v4

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v4, :cond_3

    iget-object v3, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v4}, Lcom/samsung/location/a/e;->a()I

    move-result v5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v3, v5, v6, v4}, Lcom/samsung/location/a/g;->a(IILcom/samsung/location/a/e;)Z

    :goto_1
    iget-object v3, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/location/a/g;->d(I)I

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_3
    sget-object v3, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v4, "updateGeofences : param is null(DB error) "

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_1
.end method

.method public c(I)V
    .locals 5

    const/16 v3, 0x2710

    const/4 v4, 0x2

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/samsung/location/a/c;->p:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/location/a/c;->p:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isScanAlwaysAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "WIFI setting is unavailable"

    invoke-static {v0, v1, v4, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->w:Z

    if-nez v0, :cond_3

    :cond_2
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "NLP is unavailable"

    invoke-static {v0, v1, v4, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->b:Ljava/util/List;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x66

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/c;->b:Ljava/util/List;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    const/16 v0, 0x64

    if-ne p1, v0, :cond_4

    iput-boolean v2, p0, Lcom/samsung/location/a/c;->A:Z

    :cond_4
    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/SContentManager;->c(I)Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x69

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/location/lib/h;->sendMessageDelayed(Landroid/os/Message;J)Z

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "NLP requested"

    const/4 v2, 0x0

    invoke-static {v0, v1, v4, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public c(II)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x0

    if-nez p2, :cond_0

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "handleResumeCallback : GEOFENCE_SUCCESS"

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/location/a/c;->E:I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "handleRemoveCallback : GEOFENCE_ERROR"

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public d()V
    .locals 6

    const/4 v5, 0x2

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->g()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "currentMcc :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/location/a/c;->aa:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v3, "currentMcc != mCurrentMcc"

    invoke-static {v1, v3, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/samsung/location/a/c;->aa:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v1, v0}, Lcom/samsung/location/a/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/samsung/location/a/c;->D:I

    :goto_0
    invoke-direct {p0}, Lcom/samsung/location/a/c;->q()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/a/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->w:Z

    if-nez v0, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/samsung/location/a/c;->D:I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/location/a/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v3, 0x68

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "trigger NLP with pending list "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/location/a/c;->b:Ljava/util/List;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :cond_5
    iget-object v0, p0, Lcom/samsung/location/a/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_1

    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v4, 0x2710

    if-eq v0, v4, :cond_7

    iget-object v4, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v4, v0}, Lcom/samsung/location/a/g;->d(I)I

    move-result v0

    if-ne v0, v5, :cond_4

    :cond_7
    const/4 v0, 0x1

    move v1, v0

    goto :goto_2
.end method

.method public d(II)V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x0

    if-nez p2, :cond_0

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "handlePauseCallback : GEOFENCE_SUCCESS"

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/location/a/c;->E:I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "handleRemoveCallback : GEOFENCE_ERROR"

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public e()V
    .locals 5

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/location/a/c;->C:Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->e()I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/location/a/c;->U:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget v0, p0, Lcom/samsung/location/a/c;->E:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "Screen on : trigger NLP"

    const/4 v2, 0x2

    invoke-static {v0, v1, v2, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v2, 0x68

    const/16 v3, 0x66

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0}, Lcom/samsung/location/a/l;->g()V

    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/location/a/c;->C:Z

    return-void
.end method

.method public g()V
    .locals 4

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "Wifi Scan timeout"

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-direct {p0}, Lcom/samsung/location/a/c;->s()V

    return-void
.end method

.method public h()V
    .locals 5

    const/4 v2, 0x4

    const/4 v3, 0x1

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "NLP timeout"

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->y:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Geofence paused "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/location/a/c;->D:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/samsung/location/a/c;->ac:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/location/a/c;->A:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/samsung/location/a/c;->s()V

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v2, "Cannot obtain NLP location"

    const/4 v3, 0x4

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/samsung/location/a/c;->A:Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->b()V

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v2, "Cannot obtain NLP location(once)"

    const/4 v3, 0x4

    const/4 v4, 0x1

    invoke-static {v0, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public i()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x2

    iget-object v0, p0, Lcom/samsung/location/a/c;->p:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/c;->d:Ljava/util/List;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/location/a/c;->S:J

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GOT PASSIVE wifi result on : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/location/a/c;->S:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->e()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "handleScanResult exception"

    const/4 v2, 0x1

    invoke-static {v0, v1, v4, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/samsung/location/a/c;->B:Z

    if-eqz v0, :cond_4

    iput-boolean v5, p0, Lcom/samsung/location/a/c;->B:Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->removeMessages(I)V

    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    iget-object v1, p0, Lcom/samsung/location/a/c;->d:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/samsung/location/a/g;->b(Ljava/util/List;)V

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0}, Lcom/samsung/location/a/l;->f()I

    move-result v0

    if-ne v0, v4, :cond_0

    iget-wide v0, p0, Lcom/samsung/location/a/c;->S:J

    iget-wide v2, p0, Lcom/samsung/location/a/c;->T:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/samsung/location/a/c;->S:J

    iput-wide v0, p0, Lcom/samsung/location/a/c;->T:J

    iget-object v0, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    iget-object v1, p0, Lcom/samsung/location/a/c;->d:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/samsung/location/a/g;->b(Ljava/util/List;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0}, Lcom/samsung/location/a/l;->f()I

    move-result v0

    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0}, Lcom/samsung/location/a/l;->g()V

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->c()V

    goto :goto_1
.end method

.method public j()V
    .locals 6

    const/4 v3, 0x2

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->B:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "handleScanResultsForWifi"

    const/4 v2, 0x1

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->Y:Lcom/samsung/location/lib/h;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->removeMessages(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/location/a/c;->B:Z

    iget-object v0, p0, Lcom/samsung/location/a/c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/location/a/c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lcom/samsung/location/a/c;->o:Lcom/samsung/location/a/g;

    invoke-virtual {v2, v0}, Lcom/samsung/location/a/g;->f(I)Lcom/samsung/location/a/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/location/a/e;->e()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/location/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v3}, Lcom/samsung/location/a/e;->b(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/location/a/c;->ab:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    invoke-virtual {v4, v0, v2}, Lcom/samsung/location/lib/j;->a(ILcom/samsung/location/a/e;)V

    iget-object v2, p0, Lcom/samsung/location/a/c;->q:Lcom/samsung/location/lib/j;

    iget-object v4, p0, Lcom/samsung/location/a/c;->d:Ljava/util/List;

    const/4 v5, 0x2

    invoke-virtual {v2, v0, v4, v5}, Lcom/samsung/location/lib/j;->a(ILjava/util/List;I)V

    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public k()V
    .locals 15

    const-wide/32 v5, 0x927c0

    const/4 v14, 0x4

    const/4 v13, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "handleActivityBatchEvent"

    invoke-static {v0, v1, v13, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->e()Ljava/util/List;

    move-result-object v1

    const/4 v0, -0x1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v2

    move v3, v2

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-nez v8, :cond_2

    if-eq v0, v4, :cond_0

    if-ne v0, v14, :cond_6

    move v1, v4

    move v3, v4

    :cond_0
    :goto_1
    if-eqz v3, :cond_4

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "vehicle included. set session alarm"

    invoke-static {v0, v1, v13, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0, v4}, Lcom/samsung/location/a/l;->c(Z)V

    const-wide/32 v0, 0x1d4c0

    iget-object v2, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v2, v0, v1}, Lcom/samsung/location/a/l;->a(J)V

    :cond_1
    :goto_2
    return-void

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/lib/d;

    sget-object v8, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "batch event "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/location/lib/d;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v13, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Lcom/samsung/location/lib/d;->b()I

    move-result v8

    if-eq v8, v4, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v0}, Lcom/samsung/location/lib/d;->a()J

    move-result-wide v10

    sub-long/2addr v8, v10

    cmp-long v8, v8, v5

    if-gez v8, :cond_3

    sget-object v1, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "time diff "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v0}, Lcom/samsung/location/lib/d;->a()J

    move-result-wide v11

    sub-long/2addr v9, v11

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8, v13, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Lcom/samsung/location/lib/d;->b()I

    move-result v1

    if-ne v1, v14, :cond_7

    move v1, v4

    move v3, v4

    :cond_3
    :goto_3
    invoke-virtual {v0}, Lcom/samsung/location/lib/d;->b()I

    move-result v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/samsung/location/a/c;->r:Lcom/samsung/location/a/l;

    invoke-virtual {v0, v2}, Lcom/samsung/location/a/l;->c(Z)V

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->f()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/location/a/c;->C:Z

    if-eqz v0, :cond_5

    const-wide/16 v0, 0x2

    div-long v0, v5, v0

    :goto_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/samsung/location/a/c;->t:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v4}, Lcom/samsung/location/lib/SContentManager;->f()J

    move-result-wide v4

    sub-long/2addr v2, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/location/a/c;->l()V

    goto/16 :goto_2

    :cond_5
    move-wide v0, v5

    goto :goto_4

    :cond_6
    move v1, v4

    goto/16 :goto_1

    :cond_7
    move v1, v4

    goto :goto_3
.end method

.method public l()V
    .locals 4

    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/samsung/location/a/c;->D:I

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "ACTIVITY NOTIFICATION : STATIONARY"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-direct {p0}, Lcom/samsung/location/a/c;->q()V

    const/16 v0, 0x3c

    invoke-direct {p0, v0}, Lcom/samsung/location/a/c;->e(I)Z

    return-void
.end method

.method public m()V
    .locals 4

    sget-object v0, Lcom/samsung/location/a/c;->n:Ljava/lang/String;

    const-string v1, "ACTIVITY NOTIFICATION : MOVEMENT"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget v0, p0, Lcom/samsung/location/a/c;->D:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/samsung/location/a/c;->D:I

    invoke-direct {p0}, Lcom/samsung/location/a/c;->q()V

    const/16 v0, 0x258

    invoke-direct {p0, v0}, Lcom/samsung/location/a/c;->e(I)Z

    return-void
.end method
