.class Lcom/samsung/location/a/j;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/app/PendingIntent$OnFinished;
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final synthetic a:Lcom/samsung/location/a/g;

.field private b:I

.field private c:Lcom/samsung/location/a/h;

.field private d:Lcom/samsung/location/a/k;

.field private e:I

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Landroid/app/PendingIntent;


# direct methods
.method constructor <init>(Lcom/samsung/location/a/g;IILcom/samsung/location/a/e;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/samsung/location/a/j;->a:Lcom/samsung/location/a/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/location/a/j;->b:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/location/a/j;->e:I

    iput-object v1, p0, Lcom/samsung/location/a/j;->g:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/location/a/j;->h:Landroid/app/PendingIntent;

    new-instance v0, Lcom/samsung/location/a/h;

    invoke-direct {v0, p1, p4}, Lcom/samsung/location/a/h;-><init>(Lcom/samsung/location/a/g;Lcom/samsung/location/a/e;)V

    iput-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    iput p3, p0, Lcom/samsung/location/a/j;->b:I

    iput p2, p0, Lcom/samsung/location/a/j;->f:I

    return-void
.end method

.method static synthetic a(Lcom/samsung/location/a/j;)I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/j;->b:I

    return v0
.end method

.method static synthetic a(Lcom/samsung/location/a/j;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/a/j;->g:Ljava/lang/String;

    return-void
.end method

.method static synthetic b(Lcom/samsung/location/a/j;)I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/j;->f:I

    return v0
.end method

.method static synthetic c(Lcom/samsung/location/a/j;)Lcom/samsung/location/a/h;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    return-object v0
.end method

.method static synthetic d(Lcom/samsung/location/a/j;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/app/PendingIntent;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/a/j;->h:Landroid/app/PendingIntent;

    return-void
.end method

.method public a(Lcom/samsung/location/a/k;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/a/j;->d:Lcom/samsung/location/a/k;

    return-void
.end method

.method public a()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget v2, p0, Lcom/samsung/location/a/j;->e:I

    if-ne v2, v1, :cond_1

    iget-object v2, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-static {v2, v0}, Lcom/samsung/location/a/h;->b(Lcom/samsung/location/a/h;I)V

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/samsung/location/a/j;->a(ILandroid/location/Location;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v2, 0x3

    iput v2, p0, Lcom/samsung/location/a/j;->e:I

    :cond_1
    iget-object v2, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-static {v2}, Lcom/samsung/location/a/h;->a(Lcom/samsung/location/a/h;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/location/a/j;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "prevWifiDirection : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-static {v3}, Lcom/samsung/location/a/h;->c(Lcom/samsung/location/a/h;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-static {v2, v3, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v2, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    iget-object v3, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-static {v3}, Lcom/samsung/location/a/h;->c(Lcom/samsung/location/a/h;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/samsung/location/a/h;->a(Lcom/samsung/location/a/h;I)V

    iget-object v2, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-static {v2, v0}, Lcom/samsung/location/a/h;->a(Lcom/samsung/location/a/h;Z)V

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(I)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-virtual {v0, p1, p0}, Lcom/samsung/location/a/h;->a(ILcom/samsung/location/a/j;)Z

    move-result v0

    return v0
.end method

.method public a(II)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-virtual {v0, p1, p2, p0}, Lcom/samsung/location/a/h;->a(IILcom/samsung/location/a/j;)Z

    move-result v0

    return v0
.end method

.method public a(ILandroid/location/Location;)Z
    .locals 10

    const/4 v7, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "callGeoFenceDetected "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/location/a/j;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v9, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, -0x1

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v9, :cond_2

    iget-object v1, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-static {v1}, Lcom/samsung/location/a/h;->a(Lcom/samsung/location/a/h;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x5

    move v6, v0

    :goto_0
    iget v0, p0, Lcom/samsung/location/a/j;->f:I

    if-ne v0, v8, :cond_0

    if-ne p1, v8, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/j;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/j;->a:Lcom/samsung/location/a/g;

    invoke-static {v0}, Lcom/samsung/location/a/g;->b(Lcom/samsung/location/a/g;)Lcom/samsung/location/lib/SContentManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/j;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/location/a/j;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/location/a/g;->l()Lcom/samsung/location/lib/j;

    move-result-object v0

    iget v1, p0, Lcom/samsung/location/a/j;->b:I

    iget-object v2, p0, Lcom/samsung/location/a/j;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/location/lib/j;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/a/j;->h:Landroid/app/PendingIntent;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "geofence id ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/location/a/j;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") detected : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v9, v8}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v0, "transition"

    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "location"

    invoke-virtual {v3, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/a/j;->h:Landroid/app/PendingIntent;

    iget-object v1, p0, Lcom/samsung/location/a/j;->a:Lcom/samsung/location/a/g;

    invoke-static {v1}, Lcom/samsung/location/a/g;->c(Lcom/samsung/location/a/g;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/samsung/location/a/j;->a:Lcom/samsung/location/a/g;

    iget v1, p0, Lcom/samsung/location/a/j;->b:I

    iget v2, p0, Lcom/samsung/location/a/j;->f:I

    move v3, p1

    move v4, v6

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/samsung/location/a/g;->a(Lcom/samsung/location/a/g;IIIILcom/samsung/location/a/j;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    move v0, v8

    :goto_1
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onGeofenceDetected got exception"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v9, v8}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v7

    goto :goto_1

    :cond_2
    move v6, v0

    goto/16 :goto_0
.end method

.method public a(Landroid/location/Location;)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-virtual {v0, p1, p0}, Lcom/samsung/location/a/h;->a(Landroid/location/Location;Lcom/samsung/location/a/j;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-virtual {v0, p1, p0}, Lcom/samsung/location/a/h;->a(Ljava/lang/String;Lcom/samsung/location/a/j;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/util/List;)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-virtual {v0, p1, p0}, Lcom/samsung/location/a/h;->b(Ljava/util/List;Lcom/samsung/location/a/j;)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-virtual {v0, p0}, Lcom/samsung/location/a/h;->a(Lcom/samsung/location/a/j;)Z

    move-result v0

    return v0
.end method

.method public b(I)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-virtual {v0, p1, p0}, Lcom/samsung/location/a/h;->b(ILcom/samsung/location/a/j;)Z

    move-result v0

    return v0
.end method

.method public b(II)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-virtual {v0, p1, p2, p0}, Lcom/samsung/location/a/h;->b(IILcom/samsung/location/a/j;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/location/Location;)Z
    .locals 2

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-static {v0}, Lcom/samsung/location/a/h;->a(Lcom/samsung/location/a/h;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-static {v0}, Lcom/samsung/location/a/h;->b(Lcom/samsung/location/a/h;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    iget-object v1, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-static {v1}, Lcom/samsung/location/a/h;->c(Lcom/samsung/location/a/h;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/location/a/h;->a(Lcom/samsung/location/a/h;I)V

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-virtual {v0, p1, p0}, Lcom/samsung/location/a/h;->b(Landroid/location/Location;Lcom/samsung/location/a/j;)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Ljava/util/List;)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-virtual {v0, p1, p0}, Lcom/samsung/location/a/h;->a(Ljava/util/List;Lcom/samsung/location/a/j;)Z

    move-result v0

    return v0
.end method

.method public binderDied()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SGeofence listener died : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/location/a/j;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/j;->d:Lcom/samsung/location/a/k;

    invoke-virtual {v0}, Lcom/samsung/location/a/k;->a()V

    iget-object v0, p0, Lcom/samsung/location/a/j;->a:Lcom/samsung/location/a/g;

    iget v1, p0, Lcom/samsung/location/a/j;->b:I

    invoke-static {v0, v1}, Lcom/samsung/location/a/g;->a(Lcom/samsung/location/a/g;I)V

    return-void
.end method

.method public c()Landroid/app/PendingIntent;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->h:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public c(I)V
    .locals 9

    const/4 v8, 0x5

    const/4 v7, 0x1

    const/4 v6, 0x2

    if-ne p1, v6, :cond_0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v1, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    const-string v2, "[%04d%02d%02d%02d%02d%02d] "

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {v0, v8}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x3

    const/16 v5, 0xb

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const/16 v5, 0xc

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0xd

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/location/a/h;->a(Lcom/samsung/location/a/h;Ljava/lang/String;)V

    :cond_0
    iput p1, p0, Lcom/samsung/location/a/j;->e:I

    return-void
.end method

.method public c(Landroid/location/Location;)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    invoke-virtual {v0, p1, p0}, Lcom/samsung/location/a/h;->b(Landroid/location/Location;Lcom/samsung/location/a/j;)Z

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/j;->b:I

    return v0
.end method

.method public e()Lcom/samsung/location/a/h;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->c:Lcom/samsung/location/a/h;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    move-object v0, p1

    check-cast v0, Lcom/samsung/location/a/j;

    instance-of v1, p1, Lcom/samsung/location/a/j;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/samsung/location/a/j;->b:I

    invoke-virtual {v0}, Lcom/samsung/location/a/j;->d()I

    move-result v0

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/samsung/location/a/k;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/a/j;->d:Lcom/samsung/location/a/k;

    return-object v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/j;->e:I

    return v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/j;->f:I

    return v0
.end method

.method public onSendFinished(Landroid/app/PendingIntent;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PendingIntent onSendFinished id:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/location/a/j;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "mUpdateRecord: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/a/j;->d:Lcom/samsung/location/a/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
