.class public Lcom/samsung/location/a/l;
.super Ljava/lang/Object;


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = -0x1

.field public static final e:Ljava/lang/String; = "sgeofence_session_alarm"

.field private static t:Lcom/samsung/location/a/l;

.field private static u:Lcom/samsung/location/a/g;

.field private static v:Lcom/samsung/location/lib/h;

.field private static w:Lcom/samsung/location/lib/j;

.field private static x:Lcom/samsung/location/lib/SContentManager;


# instance fields
.field private f:I

.field private g:I

.field private final h:Ljava/lang/Object;

.field private i:Landroid/content/Context;

.field private j:Landroid/app/AlarmManager;

.field private k:Landroid/app/PendingIntent;

.field private l:J

.field private m:J

.field private n:J

.field private o:J

.field private p:J

.field private q:Z

.field private r:Z

.field private s:Z


# direct methods
.method private constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/samsung/location/a/l;->f:I

    iput v1, p0, Lcom/samsung/location/a/l;->g:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/a/l;->h:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/location/a/l;->k:Landroid/app/PendingIntent;

    iput-wide v2, p0, Lcom/samsung/location/a/l;->l:J

    iput-wide v2, p0, Lcom/samsung/location/a/l;->m:J

    iput-wide v2, p0, Lcom/samsung/location/a/l;->n:J

    iput-wide v2, p0, Lcom/samsung/location/a/l;->o:J

    iput-boolean v1, p0, Lcom/samsung/location/a/l;->q:Z

    iput-boolean v1, p0, Lcom/samsung/location/a/l;->r:Z

    iput-boolean v1, p0, Lcom/samsung/location/a/l;->s:Z

    return-void
.end method

.method public static a()Lcom/samsung/location/a/l;
    .locals 1

    sget-object v0, Lcom/samsung/location/a/l;->t:Lcom/samsung/location/a/l;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/location/a/l;

    invoke-direct {v0}, Lcom/samsung/location/a/l;-><init>()V

    sput-object v0, Lcom/samsung/location/a/l;->t:Lcom/samsung/location/a/l;

    :cond_0
    sget-object v0, Lcom/samsung/location/a/l;->t:Lcom/samsung/location/a/l;

    return-object v0
.end method

.method private a(JJ)V
    .locals 3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/samsung/location/a/l;->h:Ljava/lang/Object;

    monitor-enter v2

    add-long/2addr v0, p1

    :try_start_0
    iput-wide v0, p0, Lcom/samsung/location/a/l;->l:J

    iput-wide p3, p0, Lcom/samsung/location/a/l;->m:J

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/samsung/location/a/l;->j()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private b(Landroid/location/Location;)J
    .locals 4

    sget-object v0, Lcom/samsung/location/a/l;->u:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->h()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const-wide/32 v0, 0x493e0

    const v3, 0x47c35000    # 100000.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_1

    const-wide/32 v0, 0x36ee80

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    const v3, 0x469c4000    # 20000.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_2

    const-wide/32 v0, 0x124f80

    goto :goto_0

    :cond_2
    const v3, 0x453b8000    # 3000.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    const-wide/32 v0, 0x927c0

    goto :goto_0
.end method

.method private b(J)V
    .locals 4

    iput-wide p1, p0, Lcom/samsung/location/a/l;->n:J

    iget-boolean v0, p0, Lcom/samsung/location/a/l;->q:Z

    iput-boolean v0, p0, Lcom/samsung/location/a/l;->r:Z

    iget-boolean v0, p0, Lcom/samsung/location/a/l;->q:Z

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/samsung/location/a/l;->n:J

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/location/a/l;->n:J

    :cond_0
    return-void
.end method

.method private i()V
    .locals 6

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    if-gtz v0, :cond_0

    const-string v0, "Session paused"

    const/4 v1, 0x5

    invoke-static {v0, v1, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    iget v1, p0, Lcom/samsung/location/a/l;->g:I

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "session already running "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/location/a/l;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "triggerSession "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/location/a/l;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/a/l;->h:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    iput v0, p0, Lcom/samsung/location/a/l;->g:I

    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    if-ne v0, v4, :cond_3

    const-wide/32 v2, 0x493e0

    iput-wide v2, p0, Lcom/samsung/location/a/l;->p:J

    :cond_2
    :goto_1
    iget-wide v2, p0, Lcom/samsung/location/a/l;->p:J

    invoke-direct {p0, v2, v3}, Lcom/samsung/location/a/l;->b(J)V

    iget-wide v2, p0, Lcom/samsung/location/a/l;->n:J

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-object v4, Lcom/samsung/location/a/l;->x:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v4}, Lcom/samsung/location/lib/SContentManager;->i()J

    move-result-wide v4

    sub-long/2addr v0, v4

    iget-wide v4, p0, Lcom/samsung/location/a/l;->n:J

    cmp-long v4, v0, v4

    if-gez v4, :cond_4

    sub-long v0, v2, v0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/location/a/l;->a(JJ)V

    goto :goto_0

    :cond_3
    :try_start_1
    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    if-ne v0, v2, :cond_2

    const-wide/32 v2, 0x493e0

    iput-wide v2, p0, Lcom/samsung/location/a/l;->p:J

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    const-wide/16 v0, 0xc8

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/location/a/l;->a(JJ)V

    goto :goto_0
.end method

.method private j()V
    .locals 7

    const/4 v1, 0x1

    const/4 v6, 0x2

    const-string v0, "triggerAlarm"

    invoke-static {v0, v6, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/l;->j:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/samsung/location/a/l;->k:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    iget-object v1, p0, Lcom/samsung/location/a/l;->h:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, p0, Lcom/samsung/location/a/l;->m:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_0

    const-string v0, "All alarm stopped"

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-wide v2, p0, Lcom/samsung/location/a/l;->l:J

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "triggerAlarm : next time "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v6, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/a/l;->j:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/samsung/location/a/l;->k:Landroid/app/PendingIntent;

    invoke-virtual {v0, v6, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(J)V
    .locals 8

    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    if-gez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "updateInterval"

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/a/l;->h:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, p0, Lcom/samsung/location/a/l;->n:J

    cmp-long v0, v2, p1

    if-nez v0, :cond_1

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iput-wide p1, p0, Lcom/samsung/location/a/l;->n:J

    iget-boolean v0, p0, Lcom/samsung/location/a/l;->q:Z

    if-eqz v0, :cond_2

    const-wide/16 v2, 0x2

    div-long v2, p1, v2

    iput-wide v2, p0, Lcom/samsung/location/a/l;->n:J

    :cond_2
    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    if-lez v0, :cond_3

    iget-wide v2, p0, Lcom/samsung/location/a/l;->o:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    iget-wide v2, p0, Lcom/samsung/location/a/l;->n:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/samsung/location/a/l;->o:J

    sub-long/2addr v4, v6

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lcom/samsung/location/a/l;->n:J

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/samsung/location/a/l;->a(JJ)V

    :cond_3
    :goto_1
    monitor-exit v1

    goto :goto_0

    :cond_4
    iget-wide v2, p0, Lcom/samsung/location/a/l;->n:J

    iget-wide v4, p0, Lcom/samsung/location/a/l;->n:J

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/samsung/location/a/l;->a(JJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public a(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "SGeofenceSessionManager initialize"

    const/4 v1, 0x2

    invoke-static {v0, v1, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iput-object p1, p0, Lcom/samsung/location/a/l;->i:Landroid/content/Context;

    iget-object v0, p0, Lcom/samsung/location/a/l;->i:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/samsung/location/a/l;->j:Landroid/app/AlarmManager;

    iget-object v0, p0, Lcom/samsung/location/a/l;->i:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "sgeofence_session_alarm"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v3, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/a/l;->k:Landroid/app/PendingIntent;

    invoke-static {}, Lcom/samsung/location/a/g;->a()Lcom/samsung/location/a/g;

    move-result-object v0

    sput-object v0, Lcom/samsung/location/a/l;->u:Lcom/samsung/location/a/g;

    invoke-static {}, Lcom/samsung/location/lib/h;->a()Lcom/samsung/location/lib/h;

    move-result-object v0

    sput-object v0, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    invoke-static {}, Lcom/samsung/location/lib/j;->a()Lcom/samsung/location/lib/j;

    move-result-object v0

    sput-object v0, Lcom/samsung/location/a/l;->w:Lcom/samsung/location/lib/j;

    iget-object v0, p0, Lcom/samsung/location/a/l;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->a(Landroid/content/Context;)Lcom/samsung/location/lib/SContentManager;

    move-result-object v0

    sput-object v0, Lcom/samsung/location/a/l;->x:Lcom/samsung/location/lib/SContentManager;

    return-void
.end method

.method public a(Landroid/location/Location;)V
    .locals 10

    const/4 v9, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/samsung/location/a/l;->b(Landroid/location/Location;)J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-wide v4, p0, Lcom/samsung/location/a/l;->p:J

    cmp-long v4, v4, v2

    if-nez v4, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/samsung/location/a/l;->q:Z

    iget-boolean v5, p0, Lcom/samsung/location/a/l;->r:Z

    if-eq v4, v5, :cond_3

    :cond_0
    iget-boolean v4, p0, Lcom/samsung/location/a/l;->s:Z

    if-nez v4, :cond_3

    iget-wide v4, p0, Lcom/samsung/location/a/l;->n:J

    iget-object v6, p0, Lcom/samsung/location/a/l;->h:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    iput-wide v2, p0, Lcom/samsung/location/a/l;->p:J

    iget-wide v2, p0, Lcom/samsung/location/a/l;->p:J

    invoke-direct {p0, v2, v3}, Lcom/samsung/location/a/l;->b(J)V

    iget-wide v2, p0, Lcom/samsung/location/a/l;->n:J

    iget-wide v7, p0, Lcom/samsung/location/a/l;->n:J

    invoke-direct {p0, v2, v3, v7, v8}, Lcom/samsung/location/a/l;->a(JJ)V

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Passive session, set alarm again : adaptive interval "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/location/a/l;->n:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v9, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Adaptive interval modified from "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/location/a/l;->n:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v9, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Adaptive interval retained as "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/location/a/l;->n:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v9, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_1
.end method

.method public a(Z)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "updateSession : trigger = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    if-eqz p1, :cond_3

    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string v0, "session has been paused"

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/location/a/l;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    iput v3, p0, Lcom/samsung/location/a/l;->f:I

    :goto_1
    invoke-direct {p0}, Lcom/samsung/location/a/l;->i()V

    goto :goto_0

    :cond_2
    iput v2, p0, Lcom/samsung/location/a/l;->f:I

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/samsung/location/a/l;->u:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->e()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    sget-object v1, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    const/16 v2, 0x6b

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public b()V
    .locals 6

    const/4 v3, 0x1

    const/4 v2, 0x2

    iget-object v1, p0, Lcom/samsung/location/a/l;->h:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    if-gtz v0, :cond_0

    const-string v0, "Session paused"

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/location/a/l;->g()V

    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    if-ne v0, v3, :cond_2

    const-string v0, "startSession : SESSION_NLP"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/location/a/l;->o:J

    sget-object v0, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    sget-object v2, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    const/16 v3, 0x68

    const/16 v4, 0x65

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v3, 0x1f4

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/location/lib/h;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    :goto_1
    sget-object v0, Lcom/samsung/location/a/l;->x:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->c()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    if-ne v0, v2, :cond_1

    const-string v0, "startSession : SESSION_WIFI"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/location/a/l;->o:J

    sget-object v0, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    sget-object v2, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    const/16 v3, 0x66

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/location/lib/h;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/location/a/l;->q:Z

    return-void
.end method

.method public c()V
    .locals 6

    const/4 v4, 0x2

    const/4 v3, 0x1

    const-string v0, "Session stopped"

    invoke-static {v0, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/a/l;->h:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    const/16 v2, 0x6b

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/h;->removeMessages(I)V

    sget-object v0, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    const/16 v2, 0x65

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/h;->removeMessages(I)V

    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    if-gtz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    if-ne v0, v3, :cond_2

    sget-object v0, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    const/16 v2, 0x68

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/h;->removeMessages(I)V

    sget-object v0, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    const/16 v2, 0x69

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/h;->removeMessages(I)V

    sget-object v0, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    sget-object v2, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    const/16 v3, 0x6e

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/location/lib/h;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    :goto_1
    const-wide/16 v2, -0x1

    const-wide/16 v4, -0x1

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/samsung/location/a/l;->a(JJ)V

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/location/a/l;->m:J

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/location/a/l;->o:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/location/a/l;->f:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/location/a/l;->s:Z

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    if-ne v0, v4, :cond_1

    sget-object v0, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    const/16 v2, 0x66

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/h;->removeMessages(I)V

    sget-object v0, Lcom/samsung/location/a/l;->v:Lcom/samsung/location/lib/h;

    const/16 v2, 0x67

    invoke-virtual {v0, v2}, Lcom/samsung/location/lib/h;->removeMessages(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/location/a/l;->s:Z

    return-void
.end method

.method public d()V
    .locals 1

    invoke-virtual {p0}, Lcom/samsung/location/a/l;->c()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/location/a/l;->f:I

    return-void
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/location/a/l;->f:I

    return-void
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    return v0
.end method

.method public g()V
    .locals 5

    iget v0, p0, Lcom/samsung/location/a/l;->f:I

    if-gtz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "updateAlarm"

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/samsung/location/a/l;->h:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-wide v3, p0, Lcom/samsung/location/a/l;->m:J

    add-long/2addr v0, v3

    iput-wide v0, p0, Lcom/samsung/location/a/l;->l:J

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/samsung/location/a/l;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public h()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/samsung/location/a/l;->u:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    sget-object v0, Lcom/samsung/location/a/l;->u:Lcom/samsung/location/a/g;

    invoke-virtual {v0}, Lcom/samsung/location/a/g;->f()Ljava/util/List;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    return v0

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v4, Lcom/samsung/location/a/l;->w:Lcom/samsung/location/lib/j;

    invoke-virtual {v4, v0}, Lcom/samsung/location/lib/j;->d(I)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method
