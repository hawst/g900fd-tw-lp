.class public Lcom/samsung/location/batching/SFusedBatchOptions;
.super Landroid/location/FusedBatchOptions;


# instance fields
.field private volatile mFlags:I

.field private volatile mMaxPowerAllocationInMW:D

.field private volatile mPeriodInMin:I

.field private volatile mPeriodInNS:J

.field private volatile mSourcesToUse:I


# direct methods
.method public constructor <init>(I)V
    .locals 4

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/location/FusedBatchOptions;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mPeriodInNS:J

    iput v2, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mSourcesToUse:I

    iput v2, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mFlags:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mMaxPowerAllocationInMW:D

    iput v2, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mPeriodInMin:I

    iput p1, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mPeriodInMin:I

    int-to-long v0, p1

    const-wide v2, 0xdf8475800L

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mPeriodInNS:J

    sget v0, Landroid/location/FusedBatchOptions$SourceTechnologies;->GNSS:I

    iput v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mSourcesToUse:I

    sget v0, Landroid/location/FusedBatchOptions$BatchFlags;->WAKEUP_ON_FIFO_FULL:I

    iput v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mFlags:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    check-cast p1, Lcom/samsung/location/batching/SFusedBatchOptions;

    invoke-virtual {p1}, Lcom/samsung/location/batching/SFusedBatchOptions;->getPeriodInNS()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mPeriodInNS:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFlags()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mFlags:I

    return v0
.end method

.method public getMaxPowerAllocationInMW()D
    .locals 2

    iget-wide v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mMaxPowerAllocationInMW:D

    return-wide v0
.end method

.method public getPeriodInNS()J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mPeriodInNS:J

    return-wide v0
.end method

.method public getPeroidInMin()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mPeriodInMin:I

    return v0
.end method

.method public getSourcesToUse()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mSourcesToUse:I

    return v0
.end method

.method public isFlagSet(I)Z
    .locals 1

    iget v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mFlags:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSourceToUseSet(I)Z
    .locals 1

    iget v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mSourcesToUse:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetFlag(I)V
    .locals 2

    iget v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mFlags:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mFlags:I

    return-void
.end method

.method public resetSourceToUse(I)V
    .locals 2

    iget v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mSourcesToUse:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mSourcesToUse:I

    return-void
.end method

.method public setFlag(I)V
    .locals 1

    iget v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mFlags:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mFlags:I

    return-void
.end method

.method public setMaxPowerAllocationInMW(D)V
    .locals 0

    iput-wide p1, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mMaxPowerAllocationInMW:D

    return-void
.end method

.method public setPeriodInNS(J)V
    .locals 0

    iput-wide p1, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mPeriodInNS:J

    return-void
.end method

.method public setPeroidInMin(I)V
    .locals 4

    iput p1, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mPeriodInMin:I

    int-to-long v0, p1

    const-wide v2, 0xdf8475800L

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mPeriodInNS:J

    return-void
.end method

.method public setSourceToUse(I)V
    .locals 1

    iget v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mSourcesToUse:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mSourcesToUse:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "SFusedBatchOptions : "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "period(min) : "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mPeriodInMin:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "period(ns) : "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/location/batching/SFusedBatchOptions;->mPeriodInNS:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
