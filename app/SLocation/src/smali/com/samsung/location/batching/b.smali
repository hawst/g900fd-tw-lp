.class public Lcom/samsung/location/batching/b;
.super Ljava/lang/Object;


# static fields
.field private static d:Lcom/samsung/location/batching/b;


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private final b:Ljava/util/ArrayList;

.field private c:Lcom/samsung/location/batching/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/location/batching/b;->d:Lcom/samsung/location/batching/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/batching/b;->a:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/batching/b;->b:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/location/batching/b;->c:Lcom/samsung/location/batching/a;

    return-void
.end method

.method public static a()Lcom/samsung/location/batching/b;
    .locals 1

    sget-object v0, Lcom/samsung/location/batching/b;->d:Lcom/samsung/location/batching/b;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/location/batching/b;->d:Lcom/samsung/location/batching/b;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/location/batching/b;

    invoke-direct {v0}, Lcom/samsung/location/batching/b;-><init>()V

    sput-object v0, Lcom/samsung/location/batching/b;->d:Lcom/samsung/location/batching/b;

    sget-object v0, Lcom/samsung/location/batching/b;->d:Lcom/samsung/location/batching/b;

    goto :goto_0
.end method

.method static synthetic a(Lcom/samsung/location/batching/b;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/batching/b;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic a(Lcom/samsung/location/batching/b;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/location/batching/b;->b(I)V

    return-void
.end method

.method private a(Lcom/samsung/location/batching/c;Lcom/samsung/location/ISLocationListener;)Z
    .locals 6

    const/4 v5, 0x5

    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/samsung/location/batching/c;->b()Lcom/samsung/location/ISLocationListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/location/ISLocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {v2, p1, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/samsung/location/batching/c;->a(Lcom/samsung/location/ISLocationListener;)V

    const-string v1, "LocationListener set to null"

    invoke-static {v1, v5, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/location/batching/c;->b()Lcom/samsung/location/ISLocationListener;

    move-result-object v2

    if-nez v2, :cond_1

    :try_start_0
    invoke-virtual {p1, p2}, Lcom/samsung/location/batching/c;->a(Lcom/samsung/location/ISLocationListener;)V

    invoke-virtual {p1}, Lcom/samsung/location/batching/c;->b()Lcom/samsung/location/ISLocationListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/location/ISLocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, p1, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    const-string v2, "setLocationListener OK"

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "linkToDeath failed:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v5, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v1

    goto :goto_0

    :cond_1
    const-string v2, "setLocationListener Fail : Already other listener is registered"

    invoke-static {v2, v3, v0}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v1

    goto :goto_0
.end method

.method private b(I)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/batching/b;->c:Lcom/samsung/location/batching/a;

    invoke-interface {v0, p1}, Lcom/samsung/location/batching/a;->a(I)V

    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 4

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-gez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/batching/c;

    iget-object v2, p0, Lcom/samsung/location/batching/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/samsung/location/batching/c;->b()Lcom/samsung/location/ISLocationListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/location/ISLocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v0, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    invoke-virtual {v0}, Lcom/samsung/location/batching/c;->a()Lcom/samsung/location/batching/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/location/batching/d;->a()V

    invoke-virtual {v0}, Lcom/samsung/location/batching/c;->c()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/location/batching/b;->b(I)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/List;)I
    .locals 5

    const/4 v4, 0x5

    const/4 v0, 0x1

    const/4 v3, 0x0

    if-nez p1, :cond_2

    const-string v1, "generateNewID : idArray is empty so return id is 1"

    invoke-static {v1, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "generateNewID : return id is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(II)V
    .locals 4

    iget-object v0, p0, Lcom/samsung/location/batching/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/batching/d;

    invoke-virtual {v0}, Lcom/samsung/location/batching/d;->b()Lcom/samsung/location/batching/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/batching/c;->c()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-virtual {v0, p2}, Lcom/samsung/location/batching/c;->a(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "setPeriod ID = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " / period = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public a([Landroid/location/Location;)V
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/location/batching/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/samsung/location/batching/b;->b(Ljava/util/List;)V

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/batching/d;

    invoke-virtual {v0}, Lcom/samsung/location/batching/d;->b()Lcom/samsung/location/batching/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/location/batching/c;->a([Landroid/location/Location;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(I)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x2

    const/4 v2, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "removeReceiver ID = "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/batching/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v0, v1

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Lcom/samsung/location/batching/c;->a(Lcom/samsung/location/ISLocationListener;)V

    iget-object v1, p0, Lcom/samsung/location/batching/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/samsung/location/batching/c;->a()Lcom/samsung/location/batching/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/batching/d;->a()V

    const-string v0, "removeReceiver : disposeLocked"

    invoke-static {v0, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v2

    :goto_1
    return v0

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/batching/d;

    invoke-virtual {v0}, Lcom/samsung/location/batching/d;->b()Lcom/samsung/location/batching/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/batching/c;->c()I

    move-result v4

    if-ne v4, p1, :cond_0

    goto :goto_0

    :cond_2
    const-string v0, "removeReceiver : receiver is null"

    invoke-static {v0, v5, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(ILcom/samsung/location/ISLocationListener;I)Z
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v0, "addLocationReceiver"

    invoke-static {v0, v4, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/batching/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/samsung/location/batching/c;

    invoke-direct {v0, p0, p1}, Lcom/samsung/location/batching/c;-><init>(Lcom/samsung/location/batching/b;I)V

    iget-object v3, p0, Lcom/samsung/location/batching/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/samsung/location/batching/c;->a()Lcom/samsung/location/batching/d;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/samsung/location/batching/c;->a()Lcom/samsung/location/batching/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/location/batching/d;->a()V

    :cond_1
    new-instance v3, Lcom/samsung/location/batching/d;

    invoke-direct {v3, p0, v0}, Lcom/samsung/location/batching/d;-><init>(Lcom/samsung/location/batching/b;Lcom/samsung/location/batching/c;)V

    invoke-virtual {v0, v3}, Lcom/samsung/location/batching/c;->a(Lcom/samsung/location/batching/d;)V

    invoke-direct {p0, v0, p2}, Lcom/samsung/location/batching/b;->a(Lcom/samsung/location/batching/c;Lcom/samsung/location/ISLocationListener;)Z

    move-result v3

    if-nez v3, :cond_3

    move v0, v1

    :goto_0
    return v0

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/batching/d;

    invoke-virtual {v0}, Lcom/samsung/location/batching/d;->b()Lcom/samsung/location/batching/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/location/batching/c;->c()I

    move-result v0

    if-ne v0, p1, :cond_0

    const-string v0, "already exsit record!"

    invoke-static {v0, v4, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {v0, p3}, Lcom/samsung/location/batching/c;->a(I)V

    move v0, v2

    goto :goto_0
.end method

.method public a(Lcom/samsung/location/batching/a;)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/batching/b;->c:Lcom/samsung/location/batching/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/samsung/location/batching/b;->c:Lcom/samsung/location/batching/a;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()I
    .locals 4

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/samsung/location/batching/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/batching/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    move v2, v1

    goto :goto_0

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/batching/d;

    invoke-virtual {v0}, Lcom/samsung/location/batching/d;->b()Lcom/samsung/location/batching/c;

    move-result-object v0

    if-ne v1, v2, :cond_2

    invoke-virtual {v0}, Lcom/samsung/location/batching/c;->d()I

    move-result v0

    move v1, v0

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lcom/samsung/location/batching/c;->d()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v1, v0

    goto :goto_1
.end method
