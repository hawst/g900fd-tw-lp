.class public Lcom/samsung/location/batching/SFusedLocationManager;
.super Ljava/lang/Object;


# static fields
.field private static final SLOCATION_BATCH_ID:I = 0x1

.field private static TAG:Ljava/lang/String;

.field private static fusedManager:Lcom/samsung/location/batching/SFusedLocationManager;


# instance fields
.field private batchSize:I

.field private idList:Ljava/util/List;

.field private isGPSon:Z

.field private mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

.field private mSHandler:Lcom/samsung/location/lib/h;

.field private realPeriod:I

.field private sfrm:Lcom/samsung/location/batching/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->fusedManager:Lcom/samsung/location/batching/SFusedLocationManager;

    const-string v0, "SFusedLocationManager"

    sput-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->idList:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->isGPSon:Z

    iput v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    iput v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->batchSize:I

    return-void
.end method

.method public static getInstance()Lcom/samsung/location/batching/SFusedLocationManager;
    .locals 4

    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "SFusedLocationManager.getInstance"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->fusedManager:Lcom/samsung/location/batching/SFusedLocationManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/location/batching/SFusedLocationManager;

    invoke-direct {v0}, Lcom/samsung/location/batching/SFusedLocationManager;-><init>()V

    sput-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->fusedManager:Lcom/samsung/location/batching/SFusedLocationManager;

    :cond_0
    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->fusedManager:Lcom/samsung/location/batching/SFusedLocationManager;

    return-object v0
.end method


# virtual methods
.method public handleProviderStatusChanged(ZZ)V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "SFusedLocationManager.handleProviderStatusChanged"

    const/4 v2, 0x0

    invoke-static {v0, v1, v6, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iput-boolean p1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->isGPSon:Z

    iget-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->idList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->sfrm:Lcom/samsung/location/batching/b;

    invoke-virtual {v0}, Lcom/samsung/location/batching/b;->b()I

    move-result v0

    iput v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    iget v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    if-lez v0, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->isGPSon:Z

    if-eqz v0, :cond_2

    new-instance v0, Lcom/samsung/location/batching/SFusedBatchOptions;

    iget v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    invoke-direct {v0, v1}, Lcom/samsung/location/batching/SFusedBatchOptions;-><init>(I)V

    sget-object v1, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "batching started : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    const/4 v2, 0x1

    invoke-interface {v1, v2, v0}, Landroid/hardware/location/IFusedLocationHardware;->startBatching(ILandroid/location/FusedBatchOptions;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleProviderStatusChanged got exception"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v6, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_1
    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "Batching stopped"

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/hardware/location/IFusedLocationHardware;->stopBatching(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public handleRequestLocationsTimeout()V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "SFusedLocationManager.handleRequestLocationsTimeout"

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "mFlpHardware is null"

    invoke-static {v0, v1, v3, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->sfrm:Lcom/samsung/location/batching/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/location/batching/b;->a([Landroid/location/Location;)V

    goto :goto_0
.end method

.method public initialize()V
    .locals 5

    const/4 v4, 0x1

    invoke-static {}, Lcom/samsung/location/batching/b;->a()Lcom/samsung/location/batching/b;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->sfrm:Lcom/samsung/location/batching/b;

    invoke-static {}, Lcom/samsung/location/lib/h;->a()Lcom/samsung/location/lib/h;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mSHandler:Lcom/samsung/location/lib/h;

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    invoke-interface {v0}, Landroid/hardware/location/IFusedLocationHardware;->getSupportedBatchSize()I

    move-result v0

    iput v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->batchSize:I

    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "supported batch size :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->batchSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Lcom/samsung/location/lib/b;->d()V

    return-void

    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "mFlpHardware is null"

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sflm initialize got exception"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x5

    invoke-static {v1, v0, v2, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public requestBatchOfLocations()I
    .locals 6

    const/4 v0, -0x1

    const/4 v5, 0x2

    const/4 v4, 0x1

    sget-object v1, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v2, "SFusedLocationManager.requestBatchOfLocations"

    invoke-static {v1, v2, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    if-nez v1, :cond_1

    sget-object v1, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v2, "mFlpHardware is null"

    invoke-static {v1, v2, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    :try_start_0
    iget v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->batchSize:I

    if-eq v1, v0, :cond_0

    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "supported batch size :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->batchSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    iget v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->batchSize:I

    invoke-interface {v0, v1}, Landroid/hardware/location/IFusedLocationHardware;->requestBatchOfLocations(I)V

    iget-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mSHandler:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mSHandler:Lcom/samsung/location/lib/h;

    const/16 v2, 0x191

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/location/lib/h;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestBatchOfLocations got exception"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v5, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/4 v0, -0x4

    goto :goto_0
.end method

.method public setFusedLocationHardware(Landroid/hardware/location/IFusedLocationHardware;)V
    .locals 4

    iput-object p1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "SFusedLocationManager.setFusedLocationHardware"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    return-void
.end method

.method public startBatching(ILcom/samsung/location/ISLocationListener;)I
    .locals 10

    const/4 v0, -0x1

    const/4 v1, -0x4

    const/4 v8, 0x2

    const/4 v7, 0x1

    sget-object v2, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v3, "SFusedLocationManager.startBatching"

    invoke-static {v2, v3, v8, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    if-nez v2, :cond_1

    sget-object v1, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v2, "mFlpHardware is null"

    invoke-static {v1, v2, v8, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-gtz p1, :cond_2

    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "period is invalid"

    invoke-static {v0, v1, v8, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/4 v0, -0x2

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->sfrm:Lcom/samsung/location/batching/b;

    iget-object v3, p0, Lcom/samsung/location/batching/SFusedLocationManager;->idList:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/samsung/location/batching/b;->a(Ljava/util/List;)I

    move-result v0

    iget-object v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->sfrm:Lcom/samsung/location/batching/b;

    invoke-virtual {v2, v0, p2, p1}, Lcom/samsung/location/batching/b;->a(ILcom/samsung/location/ISLocationListener;I)Z

    iget-object v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->idList:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "startBatching id : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->idList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v7, :cond_4

    if-lez p1, :cond_4

    iget-boolean v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->isGPSon:Z

    if-eqz v2, :cond_0

    new-instance v2, Lcom/samsung/location/batching/SFusedBatchOptions;

    invoke-direct {v2, p1}, Lcom/samsung/location/batching/SFusedBatchOptions;-><init>(I)V

    sget-object v3, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "batching started : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v3, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    const/4 v4, 0x1

    invoke-interface {v3, v4, v2}, Landroid/hardware/location/IFusedLocationHardware;->startBatching(ILandroid/location/FusedBatchOptions;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    move-object v9, v2

    move v2, v0

    move-object v0, v9

    sget-object v3, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "startBatching got exception"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v8, v7}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    if-lez v2, :cond_3

    iget-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->sfrm:Lcom/samsung/location/batching/b;

    invoke-virtual {v0, v2}, Lcom/samsung/location/batching/b;->a(I)Z

    iget-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->idList:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/location/batching/SFusedLocationManager;->idList:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    :try_start_1
    iget v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    iget-object v3, p0, Lcom/samsung/location/batching/SFusedLocationManager;->sfrm:Lcom/samsung/location/batching/b;

    invoke-virtual {v3}, Lcom/samsung/location/batching/b;->b()I

    move-result v3

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->sfrm:Lcom/samsung/location/batching/b;

    invoke-virtual {v2}, Lcom/samsung/location/batching/b;->b()I

    move-result v2

    iput v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    iget v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    if-gtz v2, :cond_5

    sget-object v2, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v3, "Batching stopped"

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/hardware/location/IFusedLocationHardware;->stopBatching(I)V

    move v0, v1

    goto/16 :goto_0

    :cond_5
    new-instance v2, Lcom/samsung/location/batching/SFusedBatchOptions;

    iget v3, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    invoke-direct {v2, v3}, Lcom/samsung/location/batching/SFusedBatchOptions;-><init>(I)V

    sget-object v3, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "updateBatchingOptions : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v3, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    const/4 v4, 0x1

    invoke-interface {v3, v4, v2}, Landroid/hardware/location/IFusedLocationHardware;->updateBatchingOptions(ILandroid/location/FusedBatchOptions;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public stopBatching(I)I
    .locals 8

    const/4 v0, -0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    sget-object v1, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v2, "SFusedLocationManager.stopBatching"

    invoke-static {v1, v2, v7, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    if-nez v1, :cond_0

    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "mFlpHardware is null"

    invoke-static {v0, v1, v7, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->sfrm:Lcom/samsung/location/batching/b;

    invoke-virtual {v1, p1}, Lcom/samsung/location/batching/b;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->idList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    sget-object v1, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stopBatching id : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    :cond_1
    iget-object v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->idList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v2, "Batching stopped"

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/hardware/location/IFusedLocationHardware;->stopBatching(I)V

    :cond_2
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    iget-object v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->sfrm:Lcom/samsung/location/batching/b;

    invoke-virtual {v2}, Lcom/samsung/location/batching/b;->b()I

    move-result v2

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->sfrm:Lcom/samsung/location/batching/b;

    invoke-virtual {v1}, Lcom/samsung/location/batching/b;->b()I

    move-result v1

    iput v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    iget v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    if-gtz v1, :cond_4

    sget-object v1, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v2, "Batching stopped"

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/hardware/location/IFusedLocationHardware;->stopBatching(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "stopBatching got exception"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v7, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_4
    :try_start_1
    new-instance v1, Lcom/samsung/location/batching/SFusedBatchOptions;

    iget v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    invoke-direct {v1, v2}, Lcom/samsung/location/batching/SFusedBatchOptions;-><init>(I)V

    sget-object v2, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateBatchingOptions : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    const/4 v3, 0x1

    invoke-interface {v2, v3, v1}, Landroid/hardware/location/IFusedLocationHardware;->updateBatchingOptions(ILandroid/location/FusedBatchOptions;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public updateBatchingOptions(II)I
    .locals 8

    const/4 v0, -0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    sget-object v1, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SFusedLocationManager.updateBatchingOptions id:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v7, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    if-nez v1, :cond_0

    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "mFlpHardware is null"

    invoke-static {v0, v1, v7, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    if-gtz p2, :cond_1

    sget-object v0, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v1, "period is invalid"

    invoke-static {v0, v1, v7, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    const/4 v0, -0x2

    goto :goto_0

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->sfrm:Lcom/samsung/location/batching/b;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/location/batching/b;->a(II)V

    iget v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    iget-object v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->sfrm:Lcom/samsung/location/batching/b;

    invoke-virtual {v2}, Lcom/samsung/location/batching/b;->b()I

    move-result v2

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->sfrm:Lcom/samsung/location/batching/b;

    invoke-virtual {v1}, Lcom/samsung/location/batching/b;->b()I

    move-result v1

    iput v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    iget v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    if-gtz v1, :cond_2

    sget-object v1, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    const-string v2, "Batching stopped"

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v1, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/hardware/location/IFusedLocationHardware;->stopBatching(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateBatchingOptions got exception"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v7, v6}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_2
    :try_start_1
    new-instance v1, Lcom/samsung/location/batching/SFusedBatchOptions;

    iget v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->realPeriod:I

    invoke-direct {v1, v2}, Lcom/samsung/location/batching/SFusedBatchOptions;-><init>(I)V

    sget-object v2, Lcom/samsung/location/batching/SFusedLocationManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateBatchingOptions : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const/4 v5, 0x1

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    iget-object v2, p0, Lcom/samsung/location/batching/SFusedLocationManager;->mFlpHardware:Landroid/hardware/location/IFusedLocationHardware;

    const/4 v3, 0x1

    invoke-interface {v2, v3, v1}, Landroid/hardware/location/IFusedLocationHardware;->updateBatchingOptions(ILandroid/location/FusedBatchOptions;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
