.class Lcom/samsung/location/batching/d;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/samsung/location/batching/b;

.field private b:Lcom/samsung/location/batching/c;


# direct methods
.method constructor <init>(Lcom/samsung/location/batching/b;Lcom/samsung/location/batching/c;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/location/batching/d;->a:Lcom/samsung/location/batching/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/samsung/location/batching/d;->b:Lcom/samsung/location/batching/c;

    invoke-static {p1}, Lcom/samsung/location/batching/b;->a(Lcom/samsung/location/batching/b;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/samsung/location/batching/b;->a(Lcom/samsung/location/batching/b;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/batching/d;->a:Lcom/samsung/location/batching/b;

    invoke-static {v0}, Lcom/samsung/location/batching/b;->a(Lcom/samsung/location/batching/b;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public b()Lcom/samsung/location/batching/c;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/batching/d;->b:Lcom/samsung/location/batching/c;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UpdateRecord{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
