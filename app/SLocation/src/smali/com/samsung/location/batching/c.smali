.class Lcom/samsung/location/batching/c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final synthetic a:Lcom/samsung/location/batching/b;

.field private b:Lcom/samsung/location/ISLocationListener;

.field private c:Lcom/samsung/location/batching/d;

.field private d:I

.field private e:I


# direct methods
.method constructor <init>(Lcom/samsung/location/batching/b;I)V
    .locals 2

    const/4 v1, -0x1

    iput-object p1, p0, Lcom/samsung/location/batching/c;->a:Lcom/samsung/location/batching/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/location/batching/c;->b:Lcom/samsung/location/ISLocationListener;

    iput v1, p0, Lcom/samsung/location/batching/c;->d:I

    iput v1, p0, Lcom/samsung/location/batching/c;->e:I

    iput p2, p0, Lcom/samsung/location/batching/c;->d:I

    return-void
.end method


# virtual methods
.method public a()Lcom/samsung/location/batching/d;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/batching/c;->c:Lcom/samsung/location/batching/d;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/location/batching/c;->e:I

    return-void
.end method

.method public a(Lcom/samsung/location/ISLocationListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/batching/c;->b:Lcom/samsung/location/ISLocationListener;

    return-void
.end method

.method public a(Lcom/samsung/location/batching/d;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/batching/c;->c:Lcom/samsung/location/batching/d;

    return-void
.end method

.method public a([Landroid/location/Location;)Z
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/location/batching/c;->b:Lcom/samsung/location/ISLocationListener;

    invoke-interface {v0, p1}, Lcom/samsung/location/ISLocationListener;->onLocationAvailable([Landroid/location/Location;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lcom/samsung/location/ISLocationListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/batching/c;->b:Lcom/samsung/location/ISLocationListener;

    return-object v0
.end method

.method public binderDied()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SGeofence listener died : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/location/batching/c;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {p0}, Lcom/samsung/location/batching/c;->b()Lcom/samsung/location/ISLocationListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/location/ISLocationListener;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    iget-object v0, p0, Lcom/samsung/location/batching/c;->c:Lcom/samsung/location/batching/d;

    invoke-virtual {v0}, Lcom/samsung/location/batching/d;->a()V

    iget-object v0, p0, Lcom/samsung/location/batching/c;->a:Lcom/samsung/location/batching/b;

    iget v1, p0, Lcom/samsung/location/batching/c;->d:I

    invoke-static {v0, v1}, Lcom/samsung/location/batching/b;->a(Lcom/samsung/location/batching/b;I)V

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/batching/c;->d:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/samsung/location/batching/c;->e:I

    return v0
.end method
