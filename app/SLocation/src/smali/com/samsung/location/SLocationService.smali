.class public Lcom/samsung/location/SLocationService;
.super Lcom/samsung/location/ISLocationManager$Stub;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static a:Ljava/lang/String; = null

.field private static final b:Ljava/lang/String; = "v4.4g"

.field public static feature_level:I = 0x0

.field private static final r:I = 0x0

.field private static final s:I = 0x1

.field private static final t:I = 0x2

.field private static final u:Ljava/lang/String; = "com.sec.android.app.shealth"


# instance fields
.field private final c:Landroid/content/Context;

.field private d:Lcom/samsung/location/lib/h;

.field private e:Landroid/location/IGpsGeofenceHardware;

.field private f:Lcom/samsung/location/ISLocationCellInterface;

.field private g:Landroid/hardware/location/IFusedLocationHardware;

.field private h:Ljava/lang/String;

.field private i:Lcom/samsung/location/a/c;

.field private j:Lcom/samsung/location/batching/SFusedLocationManager;

.field private k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

.field private l:Lcom/samsung/location/a/g;

.field private m:Lcom/samsung/location/batching/b;

.field private n:Lcom/samsung/location/lib/b;

.field private o:Lcom/samsung/location/a/a;

.field private p:Lcom/samsung/location/lib/NativeManager;

.field private q:Lcom/samsung/location/lib/SContentManager;

.field private final v:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ""

    sput-object v0, Lcom/samsung/location/SLocationService;->a:Ljava/lang/String;

    const/4 v0, -0x1

    sput v0, Lcom/samsung/location/SLocationService;->feature_level:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/samsung/location/ISLocationManager$Stub;-><init>()V

    iput-object v0, p0, Lcom/samsung/location/SLocationService;->e:Landroid/location/IGpsGeofenceHardware;

    iput-object v0, p0, Lcom/samsung/location/SLocationService;->f:Lcom/samsung/location/ISLocationCellInterface;

    iput-object v0, p0, Lcom/samsung/location/SLocationService;->g:Landroid/hardware/location/IFusedLocationHardware;

    const-string v0, "SLocation"

    iput-object v0, p0, Lcom/samsung/location/SLocationService;->h:Ljava/lang/String;

    new-instance v0, Lcom/samsung/location/g;

    invoke-direct {v0, p0}, Lcom/samsung/location/g;-><init>(Lcom/samsung/location/SLocationService;)V

    iput-object v0, p0, Lcom/samsung/location/SLocationService;->v:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/samsung/location/SLocationService;->c:Landroid/content/Context;

    return-void
.end method

.method private a(II)I
    .locals 2

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->c:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->c:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    return-object v0
.end method

.method private a()V
    .locals 4

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.slocation"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "com.sec.feature.slocation"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getSystemFeatureLevel(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v3, :cond_0

    sput v3, Lcom/samsung/location/SLocationService;->feature_level:I

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/location/lib/NativeManager;->a(Landroid/content/Context;)Lcom/samsung/location/lib/NativeManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/SLocationService;->p:Lcom/samsung/location/lib/NativeManager;

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->p:Lcom/samsung/location/lib/NativeManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/NativeManager;->b()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/location/SLocationService;->a:Ljava/lang/String;

    sget-object v0, Lcom/samsung/location/SLocationService;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/samsung/location/SLocationService;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    invoke-static {v1}, Lcom/samsung/location/lib/b;->a(Z)Lcom/samsung/location/lib/b;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/location/SLocationService;->n:Lcom/samsung/location/lib/b;

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->n:Lcom/samsung/location/lib/b;

    if-nez v1, :cond_3

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->h:Ljava/lang/String;

    const-string v1, "Fail to get instance of LogManager"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    return-void

    :cond_2
    new-instance v0, Ljava/io/File;

    const-string v1, "dummy"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/samsung/location/SLocationService;->c:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/samsung/location/a/g;->a(Landroid/content/Context;Z)Lcom/samsung/location/a/g;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/location/SLocationService;->l:Lcom/samsung/location/a/g;

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->l:Lcom/samsung/location/a/g;

    if-nez v1, :cond_4

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->h:Ljava/lang/String;

    const-string v1, "Fail to get instance of SReceiverManager"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    invoke-static {}, Lcom/samsung/location/batching/SFusedLocationManager;->getInstance()Lcom/samsung/location/batching/SFusedLocationManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/location/SLocationService;->j:Lcom/samsung/location/batching/SFusedLocationManager;

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->j:Lcom/samsung/location/batching/SFusedLocationManager;

    if-nez v1, :cond_5

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->h:Ljava/lang/String;

    const-string v1, "Fail to get SFusedLocationManager"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/samsung/location/SLocationService;->g:Landroid/hardware/location/IFusedLocationHardware;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->j:Lcom/samsung/location/batching/SFusedLocationManager;

    iget-object v2, p0, Lcom/samsung/location/SLocationService;->g:Landroid/hardware/location/IFusedLocationHardware;

    invoke-virtual {v1, v2}, Lcom/samsung/location/batching/SFusedLocationManager;->setFusedLocationHardware(Landroid/hardware/location/IFusedLocationHardware;)V

    invoke-static {}, Lcom/samsung/location/batching/b;->a()Lcom/samsung/location/batching/b;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/location/SLocationService;->m:Lcom/samsung/location/batching/b;

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->j:Lcom/samsung/location/batching/SFusedLocationManager;

    invoke-virtual {v1}, Lcom/samsung/location/batching/SFusedLocationManager;->initialize()V

    :cond_6
    iget-object v1, p0, Lcom/samsung/location/SLocationService;->f:Lcom/samsung/location/ISLocationCellInterface;

    invoke-static {v1}, Lcom/samsung/location/a/a;->a(Lcom/samsung/location/ISLocationCellInterface;)Lcom/samsung/location/a/a;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/location/SLocationService;->o:Lcom/samsung/location/a/a;

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->o:Lcom/samsung/location/a/a;

    if-nez v1, :cond_7

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->h:Ljava/lang/String;

    const-string v1, "Fail to get instance of SGeoCellManager"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lcom/samsung/location/SLocationService;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/location/a/c;->a(Landroid/content/Context;)Lcom/samsung/location/a/c;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    if-nez v1, :cond_8

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->h:Ljava/lang/String;

    const-string v1, "Fail to get instance of SGeofenceManager"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_8
    iget-object v1, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    iget-object v2, p0, Lcom/samsung/location/SLocationService;->e:Landroid/location/IGpsGeofenceHardware;

    invoke-virtual {v1, v2}, Lcom/samsung/location/a/c;->a(Landroid/location/IGpsGeofenceHardware;)V

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->c:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    invoke-static {v1, v0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->getInstance(Landroid/content/Context;Z)Lcom/samsung/location/currentloc/SCurrentLocationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/SLocationService;->k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->h:Ljava/lang/String;

    const-string v1, "Fail to get instance of SCurrentLocationManager"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget v0, Lcom/samsung/location/SLocationService;->feature_level:I

    if-ge v0, v3, :cond_1

    :goto_2
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/location/lib/SContentManager;->a(Landroid/content/Context;)Lcom/samsung/location/lib/SContentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/SLocationService;->q:Lcom/samsung/location/lib/SContentManager;

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->q:Lcom/samsung/location/lib/SContentManager;

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->h:Ljava/lang/String;

    const-string v1, "Fail to get instance of SContentManager"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_9
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->initialize()V

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->q:Lcom/samsung/location/lib/SContentManager;

    invoke-virtual {v0}, Lcom/samsung/location/lib/SContentManager;->a()V

    invoke-static {}, Lcom/samsung/location/lib/h;->a()Lcom/samsung/location/lib/h;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->h:Ljava/lang/String;

    const-string v1, "Fail to get instance of SWorkerHandler"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_b
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    invoke-virtual {v0}, Lcom/samsung/location/lib/h;->b()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "sgeofence_session_alarm"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.bluetooth.device.action.CLASS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    if-eqz v1, :cond_c

    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :cond_c
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->o:Lcom/samsung/location/a/a;

    invoke-virtual {v1}, Lcom/samsung/location/a/a;->e()Z

    move-result v1

    if-eqz v1, :cond_d

    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :cond_d
    const-string v1, "com.samsung.location.SERVICE_READY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/location/SLocationService;->v:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/location/SLocationService;->v:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->j:Lcom/samsung/location/batching/SFusedLocationManager;

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->q:Lcom/samsung/location/lib/SContentManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/SContentManager;->a(I)Z

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/location/batching/SFusedLocationManager;->handleProviderStatusChanged(ZZ)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->h:Ljava/lang/String;

    const-string v1, "SLOCATION_VERSION v4.4g Initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->c:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.location.SERVICE_READY"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1
.end method

.method private a(I)V
    .locals 2

    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Geofence usage requires ACCESS_FINE_LOCATION permission"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private b()I
    .locals 2

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/location/SLocationService;->a(II)I

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/samsung/location/SLocationService;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->c:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public addGeofence(Lcom/samsung/location/SLocationParameter;)I
    .locals 3

    invoke-direct {p0}, Lcom/samsung/location/SLocationService;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/location/SLocationService;->a(I)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    if-nez v0, :cond_0

    const-string v0, "SGeofenceManager is NOT supported"

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/c;->a(Lcom/samsung/location/SLocationParameter;)I

    move-result v0

    goto :goto_0
.end method

.method public checkPassiveLocation()Z
    .locals 3

    invoke-direct {p0}, Lcom/samsung/location/SLocationService;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/location/SLocationService;->a(I)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    if-nez v0, :cond_0

    const-string v0, "SCurrentLocation is NOT supported"

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    invoke-virtual {v0}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->checkPassiveLocation()Z

    move-result v0

    goto :goto_0
.end method

.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->c:Landroid/content/Context;

    const-string v1, "android.permission.DUMP"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Permission Denial: can\'t dump SLocationService from pid="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " without permission "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "android.permission.DUMP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->l:Lcom/samsung/location/a/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->l:Lcom/samsung/location/a/g;

    invoke-virtual {v0, p2}, Lcom/samsung/location/a/g;->a(Ljava/io/PrintWriter;)V

    :cond_2
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    invoke-virtual {v0, p2}, Lcom/samsung/location/a/c;->a(Ljava/io/PrintWriter;)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    invoke-virtual {v0, p2}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->dump(Ljava/io/PrintWriter;)V

    goto :goto_0
.end method

.method public removeCurrentLocation(I)I
    .locals 3

    invoke-direct {p0}, Lcom/samsung/location/SLocationService;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/location/SLocationService;->a(I)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    if-nez v0, :cond_0

    const-string v0, "SCurrentLocation is NOT supported"

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    invoke-virtual {v0, p1}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->removeCurrentLocation(I)I

    move-result v0

    goto :goto_0
.end method

.method public removeGeofence(I)I
    .locals 3

    invoke-direct {p0}, Lcom/samsung/location/SLocationService;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/location/SLocationService;->a(I)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    if-nez v0, :cond_0

    const-string v0, "SGeofenceManager is NOT supported"

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/c;->a(I)I

    move-result v0

    goto :goto_0
.end method

.method public removeSingleLocation(Landroid/app/PendingIntent;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public reportCellGeofenceDetected(II)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    const/16 v2, 0x12d

    invoke-virtual {v1, v2, p1, p2}, Lcom/samsung/location/lib/h;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public reportCellGeofenceRequestFail(I)V
    .locals 4

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    const/16 v2, 0x12e

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public reportFlpHardwareLocation([Landroid/location/Location;)V
    .locals 3

    const-string v0, "reportFlpHardwareLocation"

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    const/16 v1, 0x191

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->removeMessages(I)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->m:Lcom/samsung/location/batching/b;

    invoke-virtual {v0, p1}, Lcom/samsung/location/batching/b;->a([Landroid/location/Location;)V

    invoke-static {p1}, Lcom/samsung/location/lib/b;->a([Landroid/location/Location;)V

    return-void
.end method

.method public reportGpsGeofenceAddStatus(II)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "reportGpsGeofenceAddStatus : id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    const/16 v2, 0xc9

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, p2, v3}, Lcom/samsung/location/lib/h;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public reportGpsGeofencePauseStatus(II)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "reportGpsGeofencePauseStatus : id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    const/16 v2, 0xce

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, p2, v3}, Lcom/samsung/location/lib/h;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public reportGpsGeofenceRemoveStatus(II)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "reportGpsGeofenceRemoveStatus : id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    const/16 v2, 0xca

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, p2, v3}, Lcom/samsung/location/lib/h;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public reportGpsGeofenceResumeStatus(II)V
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "reportGpsGeofenceResumeStatus : id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    const/16 v2, 0xcd

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, p2, v3}, Lcom/samsung/location/lib/h;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public reportGpsGeofenceStatus(IIDDDFFFJ)V
    .locals 13

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "reportGpsGeofenceStatus : status= "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->q:Lcom/samsung/location/lib/SContentManager;

    move v1, p2

    move-wide/from16 v2, p3

    move-wide/from16 v4, p5

    move-wide/from16 v6, p7

    move/from16 v8, p9

    move/from16 v9, p10

    move/from16 v10, p11

    move-wide/from16 v11, p12

    invoke-virtual/range {v0 .. v12}, Lcom/samsung/location/lib/SContentManager;->a(IDDDFFFJ)Landroid/location/Location;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    iget-object v2, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    const/16 v3, 0xcc

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4, v0}, Lcom/samsung/location/lib/h;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public reportGpsGeofenceTransition(IIDDDFFFJIJ)V
    .locals 15

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reportGpsGeofenceTransition : ID="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / transition="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p14

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reportGpsGeofenceTransition : lat="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p3

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / lon="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p5

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v2, p0, Lcom/samsung/location/SLocationService;->q:Lcom/samsung/location/lib/SContentManager;

    move/from16 v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move/from16 v10, p9

    move/from16 v11, p10

    move/from16 v12, p11

    move-wide/from16 v13, p12

    invoke-virtual/range {v2 .. v14}, Lcom/samsung/location/lib/SContentManager;->a(IDDDFFFJ)Landroid/location/Location;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    iget-object v4, p0, Lcom/samsung/location/SLocationService;->d:Lcom/samsung/location/lib/h;

    const/16 v5, 0xcb

    move/from16 v0, p1

    move/from16 v1, p14

    invoke-virtual {v4, v5, v0, v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public requestBatchOfLocations()I
    .locals 3

    invoke-direct {p0}, Lcom/samsung/location/SLocationService;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/location/SLocationService;->a(I)V

    const-string v0, "requestBatchOfLocations"

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->j:Lcom/samsung/location/batching/SFusedLocationManager;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->j:Lcom/samsung/location/batching/SFusedLocationManager;

    invoke-virtual {v0}, Lcom/samsung/location/batching/SFusedLocationManager;->requestBatchOfLocations()I

    move-result v0

    goto :goto_0
.end method

.method public requestCurrentLocation(Lcom/samsung/location/ISCurrentLocListener;)I
    .locals 3

    invoke-direct {p0}, Lcom/samsung/location/SLocationService;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/location/SLocationService;->a(I)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    if-nez v0, :cond_0

    const-string v0, "SCurrentLocation is NOT supported"

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    invoke-virtual {v0, p1}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->requestCurrentLocation(Lcom/samsung/location/ISCurrentLocListener;)I

    move-result v0

    goto :goto_0
.end method

.method public requestSingleLocation(IILandroid/app/PendingIntent;)I
    .locals 3

    invoke-direct {p0}, Lcom/samsung/location/SLocationService;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/location/SLocationService;->a(I)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    if-nez v0, :cond_0

    const-string v0, "SCurrentLocation is NOT supported"

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->k:Lcom/samsung/location/currentloc/SCurrentLocationManager;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/location/currentloc/SCurrentLocationManager;->getCurrentLocation(IILandroid/app/PendingIntent;)I

    move-result v0

    goto :goto_0
.end method

.method public run()V
    .locals 1

    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-static {}, Landroid/os/Looper;->prepare()V

    invoke-direct {p0}, Lcom/samsung/location/SLocationService;->a()V

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void
.end method

.method public setFusedLocationHardware(Landroid/hardware/location/IFusedLocationHardware;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/samsung/location/SLocationService;->g:Landroid/hardware/location/IFusedLocationHardware;

    const-string v0, "setFusedLocationHardware"

    invoke-static {v0, v2, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "setFusedLocationHardware : interface is null"

    invoke-static {v0, v2, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public setGeofenceCellInterface(Lcom/samsung/location/ISLocationCellInterface;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/samsung/location/SLocationService;->f:Lcom/samsung/location/ISLocationCellInterface;

    const-string v0, "setGeofenceCellInterface"

    invoke-static {v0, v2, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "setGeofenceCellInterface : interface is null"

    invoke-static {v0, v2, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public setGpsGeofenceHardware(Landroid/location/IGpsGeofenceHardware;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/samsung/location/SLocationService;->e:Landroid/location/IGpsGeofenceHardware;

    const-string v0, "setGpsGeofenceHardware"

    invoke-static {v0, v2, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "setGpsGeofenceHardware : interface is null"

    invoke-static {v0, v2, v1}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public startGeofence(ILandroid/app/PendingIntent;)I
    .locals 3

    invoke-direct {p0}, Lcom/samsung/location/SLocationService;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/location/SLocationService;->a(I)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    if-nez v0, :cond_0

    const-string v0, "SGeofenceManager is NOT supported"

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/a/c;->a(ILandroid/app/PendingIntent;)I

    move-result v0

    goto :goto_0
.end method

.method public startLocationBatching(ILcom/samsung/location/ISLocationListener;)I
    .locals 3

    invoke-direct {p0}, Lcom/samsung/location/SLocationService;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/location/SLocationService;->a(I)V

    const-string v0, "startBatching"

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->j:Lcom/samsung/location/batching/SFusedLocationManager;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->j:Lcom/samsung/location/batching/SFusedLocationManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/batching/SFusedLocationManager;->startBatching(ILcom/samsung/location/ISLocationListener;)I

    move-result v0

    goto :goto_0
.end method

.method public stopGeofence(ILandroid/app/PendingIntent;)I
    .locals 3

    invoke-direct {p0}, Lcom/samsung/location/SLocationService;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/location/SLocationService;->a(I)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    if-nez v0, :cond_0

    const-string v0, "SGeofenceManager is NOT supported"

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/a/c;->b(ILandroid/app/PendingIntent;)I

    move-result v0

    goto :goto_0
.end method

.method public stopLocationBatching(I)I
    .locals 3

    invoke-direct {p0}, Lcom/samsung/location/SLocationService;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/location/SLocationService;->a(I)V

    const-string v0, "stopBatching"

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->j:Lcom/samsung/location/batching/SFusedLocationManager;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->j:Lcom/samsung/location/batching/SFusedLocationManager;

    invoke-virtual {v0, p1}, Lcom/samsung/location/batching/SFusedLocationManager;->stopBatching(I)I

    move-result v0

    goto :goto_0
.end method

.method public syncGeofence([I)I
    .locals 3

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    if-nez v0, :cond_0

    const-string v0, "SGeofenceManager is NOT supported"

    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->i:Lcom/samsung/location/a/c;

    invoke-virtual {v0, p1}, Lcom/samsung/location/a/c;->a([I)I

    move-result v0

    goto :goto_0
.end method

.method public systemReady()V
    .locals 3

    new-instance v0, Ljava/lang/Thread;

    const/4 v1, 0x0

    const-string v2, "SLocationeService"

    invoke-direct {v0, v1, p0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public updateBatchingOptions(II)I
    .locals 3

    const-string v0, "updateBatchingOptions"

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/SLocationService;->j:Lcom/samsung/location/batching/SFusedLocationManager;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/location/SLocationService;->j:Lcom/samsung/location/batching/SFusedLocationManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/location/batching/SFusedLocationManager;->updateBatchingOptions(II)I

    move-result v0

    goto :goto_0
.end method
