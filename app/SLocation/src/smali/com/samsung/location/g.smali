.class Lcom/samsung/location/g;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/samsung/location/SLocationService;


# direct methods
.method constructor <init>(Lcom/samsung/location/SLocationService;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v0, "BOOT_COMPLETED"

    invoke-static {v0, v2, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "sgeofence_session_alarm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "BroadcastReceiver : ALARM_SESSION"

    invoke-static {v0, v2, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v0}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v1}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v1

    const/16 v2, 0x65

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/location/lib/h;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_2
    const-string v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "BroadcastReceiver : SCAN_RESULTS_AVAILABLE_ACTION"

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v0}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v1}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_3
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v0, "BroadcastReceiver : WIFI_STATE_CHANGED_ACTION"

    invoke-static {v0, v2, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v0}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v1}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_4
    const-string v1, "android.bluetooth.device.action.ACL_CONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "BroadcastReceiver : bt ACTION_ACL_CONNECTED "

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v0}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v1}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p2}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_5
    const-string v1, "android.bluetooth.device.action.ACL_DISCONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v0, "BroadcastReceiver : bt ACTION_ACL_DISCONNECTED "

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v0}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v1}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2, p2}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_6
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v0, "BroadcastReceiver : CONNECTIVITY_ACTION"

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v0}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v1}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_7
    const-string v1, "com.samsung.location.SERVICE_READY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v0, "BroadcastReceiver : service is ready - v4.4g"

    invoke-static {v0, v2, v4}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto/16 :goto_0

    :cond_8
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v0, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v0}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v1}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2, p2}, Lcom/samsung/location/lib/h;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_9
    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v0, "BroadcastReceiver : ACTION_SIM_STATE_CHANGED - "

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    goto/16 :goto_0

    :cond_a
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v0, "BroadcastReceiver : ACTION_USER_PRESENT"

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v0}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v1}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_b
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v0, "BroadcastReceiver : ACTION_AIRPLANE_MODE_CHANGED"

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v0}, Lcom/samsung/location/SLocationService;->b(Lcom/samsung/location/SLocationService;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "airplane_mode_on"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v0}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v1}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_c
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    const-string v0, "BroadcastReceiver : ACTION_SCREEN_OFF"

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v0}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v1}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_d
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BroadcastReceiver : ACTION_PACKAGE_REMOVED"

    invoke-static {v0, v2, v3}, Lcom/samsung/location/lib/b;->a(Ljava/lang/String;IZ)V

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.shealth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v0}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/location/g;->a:Lcom/samsung/location/SLocationService;

    invoke-static {v1}, Lcom/samsung/location/SLocationService;->a(Lcom/samsung/location/SLocationService;)Lcom/samsung/location/lib/h;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/samsung/location/lib/h;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/location/lib/h;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method
