.class Lcom/sec/android/Preconfig/PreconfigService$1;
.super Landroid/os/Handler;
.source "PreconfigService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/Preconfig/PreconfigService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/Preconfig/PreconfigService;


# direct methods
.method constructor <init>(Lcom/sec/android/Preconfig/PreconfigService;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/sec/android/Preconfig/PreconfigService$1;->this$0:Lcom/sec/android/Preconfig/PreconfigService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 285
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 292
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/Preconfig/PreconfigService$1;->this$0:Lcom/sec/android/Preconfig/PreconfigService;

    # getter for: Lcom/sec/android/Preconfig/PreconfigService;->isDebuggableMonkeyBuild:Z
    invoke-static {v1}, Lcom/sec/android/Preconfig/PreconfigService;->access$000(Lcom/sec/android/Preconfig/PreconfigService;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 295
    iget-object v1, p0, Lcom/sec/android/Preconfig/PreconfigService$1;->this$0:Lcom/sec/android/Preconfig/PreconfigService;

    # invokes: Lcom/sec/android/Preconfig/PreconfigService;->setEndModeData()[B
    invoke-static {v1}, Lcom/sec/android/Preconfig/PreconfigService;->access$100(Lcom/sec/android/Preconfig/PreconfigService;)[B

    move-result-object v0

    .line 297
    .local v0, "data":[B
    iget-object v1, p0, Lcom/sec/android/Preconfig/PreconfigService$1;->this$0:Lcom/sec/android/Preconfig/PreconfigService;

    # getter for: Lcom/sec/android/Preconfig/PreconfigService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;
    invoke-static {v1}, Lcom/sec/android/Preconfig/PreconfigService;->access$200(Lcom/sec/android/Preconfig/PreconfigService;)Lcom/samsung/android/sec_platform_library/FactoryPhone;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/Preconfig/PreconfigService$1;->this$0:Lcom/sec/android/Preconfig/PreconfigService;

    iget-object v2, v2, Lcom/sec/android/Preconfig/PreconfigService;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3f0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0

    .line 301
    .end local v0    # "data":[B
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/Preconfig/PreconfigService$1;->this$0:Lcom/sec/android/Preconfig/PreconfigService;

    # invokes: Lcom/sec/android/Preconfig/PreconfigService;->runAndroidFactoryReset()V
    invoke-static {v1}, Lcom/sec/android/Preconfig/PreconfigService;->access$300(Lcom/sec/android/Preconfig/PreconfigService;)V

    goto :goto_0

    .line 285
    :sswitch_data_0
    .sparse-switch
        0x3f0 -> :sswitch_1
        0x7ce -> :sswitch_0
    .end sparse-switch
.end method
