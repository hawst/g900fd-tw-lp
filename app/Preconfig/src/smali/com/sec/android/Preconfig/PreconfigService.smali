.class public Lcom/sec/android/Preconfig/PreconfigService;
.super Landroid/app/Service;
.source "PreconfigService.java"


# instance fields
.field private final BOOST_CODE:Ljava/lang/String;

.field private final CHECK_CHAMELEON:Ljava/lang/String;

.field private final LOG_TAG:Ljava/lang/String;

.field private final OEM_CFG_EXEC_DEFAULT:B

.field private final OEM_FUNCTION_ID_CONFIGURATION:B

.field private final QUERT_SERVM_DONE:I

.field private final SET_RIL_CODE:I

.field private final SPRINT_CODE:Ljava/lang/String;

.field private final VIRGIN_CODE:Ljava/lang/String;

.field private final isDebuggableMonkeyBuild:Z

.field public mHandler:Landroid/os/Handler;

.field private mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

.field private final path_mps:Ljava/lang/String;

.field private triggeredByHidden:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 19
    const-string v0, "PreconfigService"

    iput-object v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->LOG_TAG:Ljava/lang/String;

    .line 21
    const/16 v0, 0x7ce

    iput v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->SET_RIL_CODE:I

    .line 23
    const/16 v0, 0x3f0

    iput v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->QUERT_SERVM_DONE:I

    .line 25
    const/16 v0, 0xc

    iput-byte v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->OEM_FUNCTION_ID_CONFIGURATION:B

    .line 28
    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->OEM_CFG_EXEC_DEFAULT:B

    .line 30
    const-string v0, "ro.monkey"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->isDebuggableMonkeyBuild:Z

    .line 32
    const-string v0, "/efs/imei/mps_code.dat"

    iput-object v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->path_mps:Ljava/lang/String;

    .line 34
    const-string v0, "310120"

    iput-object v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->SPRINT_CODE:Ljava/lang/String;

    .line 36
    const-string v0, "311870"

    iput-object v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->BOOST_CODE:Ljava/lang/String;

    .line 38
    const-string v0, "311490"

    iput-object v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->VIRGIN_CODE:Ljava/lang/String;

    .line 40
    const-string v0, "/sdcard/check_chameleon.txt"

    iput-object v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->CHECK_CHAMELEON:Ljava/lang/String;

    .line 42
    iput-boolean v1, p0, Lcom/sec/android/Preconfig/PreconfigService;->triggeredByHidden:Z

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 283
    new-instance v0, Lcom/sec/android/Preconfig/PreconfigService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/Preconfig/PreconfigService$1;-><init>(Lcom/sec/android/Preconfig/PreconfigService;)V

    iput-object v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/Preconfig/PreconfigService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/Preconfig/PreconfigService;

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->isDebuggableMonkeyBuild:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/Preconfig/PreconfigService;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/Preconfig/PreconfigService;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/Preconfig/PreconfigService;->setEndModeData()[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/Preconfig/PreconfigService;)Lcom/samsung/android/sec_platform_library/FactoryPhone;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/Preconfig/PreconfigService;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/Preconfig/PreconfigService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/Preconfig/PreconfigService;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/Preconfig/PreconfigService;->runAndroidFactoryReset()V

    return-void
.end method

.method private runAndroidFactoryReset()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 255
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MASTER_CLEAR"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 258
    .local v2, "i":Landroid/content/Intent;
    iget-boolean v3, p0, Lcom/sec/android/Preconfig/PreconfigService;->triggeredByHidden:Z

    if-eqz v3, :cond_1

    .line 259
    const-string v3, "PreconfigService"

    const-string v4, "FactoryReset"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    const-string v3, "PreconfigService"

    const-string v4, "WipeCustomerPartiotion"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    const-string v3, "WipeCustomerPartiotion"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 280
    :cond_0
    :goto_0
    invoke-virtual {p0, v2}, Lcom/sec/android/Preconfig/PreconfigService;->sendBroadcast(Landroid/content/Intent;)V

    .line 281
    :goto_1
    return-void

    .line 264
    :cond_1
    const-string v3, "PreconfigService"

    const-string v4, "WipeCache"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    const-string v3, "WipeCache"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 267
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v3, "/sdcard/check_chameleon.txt"

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 269
    .local v0, "cscfile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 270
    const-string v3, "PreconfigService"

    const-string v4, "create CHECK_CHAMELEON file"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 273
    .end local v0    # "cscfile":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 274
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "PreconfigService"

    const-string v4, "Failed to create CHECK_CHAMELEON file: "

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private setEndModeData()[B
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 217
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 218
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 219
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x4

    .line 222
    .local v3, "fileSize":I
    const/16 v5, 0xc

    :try_start_0
    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 223
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 224
    const/4 v5, 0x5

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 232
    iget-boolean v5, p0, Lcom/sec/android/Preconfig/PreconfigService;->triggeredByHidden:Z

    if-eqz v5, :cond_0

    .line 233
    const-string v5, "PreconfigService"

    const-string v6, "setEndModeData() case RTN- 0x02"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    const/4 v5, 0x2

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 250
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    :goto_1
    return-object v4

    .line 236
    :cond_0
    :try_start_2
    const-string v5, "PreconfigService"

    const-string v6, "setEndModeData() case Chameleon- 0x04"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    const/4 v5, 0x4

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 239
    :catch_0
    move-exception v2

    .line 240
    .local v2, "e":Ljava/io/IOException;
    :try_start_3
    const-string v5, "PreconfigService"

    const-string v6, "IOException in setEndModeData"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 244
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 245
    :catch_1
    move-exception v2

    .line 246
    goto :goto_1

    .line 245
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 246
    .restart local v2    # "e":Ljava/io/IOException;
    goto :goto_1

    .line 243
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 244
    :try_start_5
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 246
    throw v5

    .line 245
    :catch_3
    move-exception v2

    .line 246
    .restart local v2    # "e":Ljava/io/IOException;
    goto :goto_1
.end method

.method private setMpsCode(Ljava/lang/String;)V
    .locals 9
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 168
    const-string v0, ""

    .line 169
    .local v0, "Sales_str":Ljava/lang/String;
    const/4 v6, 0x3

    invoke-virtual {p1, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 171
    new-instance v5, Ljava/io/File;

    const-string v6, "/efs/imei"

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 172
    .local v5, "verifyDir":Ljava/io/File;
    const/4 v3, 0x0

    .line 175
    .local v3, "out":Ljava/io/BufferedWriter;
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_0

    .line 176
    const-string v6, "PreconfigService"

    const-string v7, "No directoy, make imei directoy"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 180
    :cond_0
    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 181
    const-string v6, "PreconfigService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "success setreadable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :cond_1
    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 184
    const-string v6, "PreconfigService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "success setexecutable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    :cond_2
    new-instance v4, Ljava/io/BufferedWriter;

    new-instance v6, Ljava/io/FileWriter;

    const-string v7, "/efs/imei/mps_code.dat"

    invoke-direct {v6, v7}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    .end local v3    # "out":Ljava/io/BufferedWriter;
    .local v4, "out":Ljava/io/BufferedWriter;
    :try_start_1
    invoke-virtual {v4, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 193
    new-instance v1, Ljava/io/File;

    const-string v6, "/efs/imei/mps_code.dat"

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 194
    .local v1, "SetPermission":Ljava/io/File;
    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 195
    const-string v6, "PreconfigService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "success setreadable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :cond_3
    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 198
    const-string v6, "PreconfigService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "success setexecutable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :cond_4
    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Ljava/io/File;->setWritable(ZZ)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 201
    const-string v6, "PreconfigService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "success setWritable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 208
    :cond_5
    if-eqz v4, :cond_6

    .line 209
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_6
    :goto_0
    move-object v3, v4

    .line 213
    .end local v1    # "SetPermission":Ljava/io/File;
    .end local v4    # "out":Ljava/io/BufferedWriter;
    .restart local v3    # "out":Ljava/io/BufferedWriter;
    :cond_7
    :goto_1
    return-void

    .line 204
    :catch_0
    move-exception v2

    .line 208
    .local v2, "e":Ljava/io/IOException;
    :goto_2
    if-eqz v3, :cond_7

    .line 209
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 210
    :catch_1
    move-exception v6

    goto :goto_1

    .line 207
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 208
    :goto_3
    if-eqz v3, :cond_8

    .line 209
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 211
    :cond_8
    :goto_4
    throw v6

    .line 210
    .end local v3    # "out":Ljava/io/BufferedWriter;
    .restart local v1    # "SetPermission":Ljava/io/File;
    .restart local v4    # "out":Ljava/io/BufferedWriter;
    :catch_2
    move-exception v6

    goto :goto_0

    .end local v1    # "SetPermission":Ljava/io/File;
    .end local v4    # "out":Ljava/io/BufferedWriter;
    .restart local v3    # "out":Ljava/io/BufferedWriter;
    :catch_3
    move-exception v7

    goto :goto_4

    .line 207
    .end local v3    # "out":Ljava/io/BufferedWriter;
    .restart local v4    # "out":Ljava/io/BufferedWriter;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "out":Ljava/io/BufferedWriter;
    .restart local v3    # "out":Ljava/io/BufferedWriter;
    goto :goto_3

    .line 204
    .end local v3    # "out":Ljava/io/BufferedWriter;
    .restart local v4    # "out":Ljava/io/BufferedWriter;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "out":Ljava/io/BufferedWriter;
    .restart local v3    # "out":Ljava/io/BufferedWriter;
    goto :goto_2
.end method

.method private setSalesCode(Ljava/lang/String;)V
    .locals 8
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 139
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 140
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 141
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v4, 0x3

    invoke-virtual {p1, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 142
    const-string v4, "PreconfigService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "set CSC code : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    array-length v4, v4

    add-int/lit8 v3, v4, 0x5

    .line 146
    .local v3, "fileSize":I
    const/4 v4, 0x6

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 147
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 148
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 149
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 150
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 151
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    array-length v6, v6

    invoke-virtual {v1, v4, v5, v6}, Ljava/io/DataOutputStream;->write([BII)V

    .line 154
    iget-object v4, p0, Lcom/sec/android/Preconfig/PreconfigService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/Preconfig/PreconfigService;->mHandler:Landroid/os/Handler;

    const/16 v7, 0x7ce

    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 165
    .end local v3    # "fileSize":I
    :goto_0
    return-void

    .line 161
    .restart local v3    # "fileSize":I
    :catch_0
    move-exception v2

    .line 162
    .local v2, "e":Ljava/io/IOException;
    goto :goto_0

    .line 156
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fileSize":I
    :catch_1
    move-exception v2

    .line 160
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 161
    :catch_2
    move-exception v2

    .line 162
    goto :goto_0

    .line 159
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 160
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 162
    throw v4

    .line 161
    :catch_3
    move-exception v2

    .line 162
    .restart local v2    # "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method private verifyInputCode(Ljava/lang/String;)Z
    .locals 2
    .param p1, "code"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 60
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v0

    .line 67
    :cond_1
    const-string v1, "SPR"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "BST"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "XAS"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "VMU"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 49
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-direct {v0, p0}, Lcom/samsung/android/sec_platform_library/FactoryPhone;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/Preconfig/PreconfigService;->mPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 57
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 74
    const-string v9, "PreconfigService"

    const-string v10, "Start"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    if-eqz p1, :cond_0

    const-string v9, "CODE"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 76
    const-string v9, "CODE"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 77
    .local v2, "code":Ljava/lang/String;
    const-string v9, "310120"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 78
    const-string v2, "SPR"

    .line 85
    :goto_0
    const-string v9, "PreconfigService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "CODE: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const-string v9, "type"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 90
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/Preconfig/PreconfigService;->triggeredByHidden:Z

    .line 91
    const-string v9, "PreconfigService"

    const-string v10, "triggeredByHidden: true"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :goto_1
    const-string v3, ""

    .line 98
    .local v3, "current_code":Ljava/lang/String;
    const/4 v7, 0x0

    .line 99
    .local v7, "fr":Ljava/io/FileReader;
    const/4 v0, 0x0

    .line 101
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v6, Ljava/io/File;

    const-string v9, "/efs/imei/mps_code.dat"

    invoke-direct {v6, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 103
    .local v6, "fe":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 104
    new-instance v8, Ljava/io/FileReader;

    const-string v9, "/efs/imei/mps_code.dat"

    invoke-direct {v8, v9}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    .end local v7    # "fr":Ljava/io/FileReader;
    .local v8, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 106
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 107
    const-string v9, "PreconfigService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Comfirm_SALES:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 110
    invoke-virtual {v8}, Ljava/io/FileReader;->close()V

    .line 112
    invoke-direct {p0, v2}, Lcom/sec/android/Preconfig/PreconfigService;->verifyInputCode(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 113
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/sec/android/Preconfig/PreconfigService;->setSalesCode(Ljava/lang/String;)V

    .line 114
    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/sec/android/Preconfig/PreconfigService;->setMpsCode(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v7, v8

    .line 135
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "code":Ljava/lang/String;
    .end local v3    # "current_code":Ljava/lang/String;
    .end local v6    # "fe":Ljava/io/File;
    .end local v8    # "fr":Ljava/io/FileReader;
    :cond_0
    :goto_2
    const/4 v9, 0x2

    return v9

    .line 79
    .restart local v2    # "code":Ljava/lang/String;
    :cond_1
    const-string v9, "311870"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 80
    const-string v2, "BST"

    goto/16 :goto_0

    .line 81
    :cond_2
    const-string v9, "311490"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 82
    const-string v2, "VMU"

    goto/16 :goto_0

    .line 84
    :cond_3
    const-string v2, "XAS"

    goto/16 :goto_0

    .line 93
    :cond_4
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/Preconfig/PreconfigService;->triggeredByHidden:Z

    .line 94
    const-string v9, "PreconfigService"

    const-string v10, "triggeredByHidden: false"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 116
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "current_code":Ljava/lang/String;
    .restart local v6    # "fe":Ljava/io/File;
    .restart local v8    # "fr":Ljava/io/FileReader;
    :cond_5
    :try_start_3
    const-string v9, "PreconfigService"

    const-string v10, "Code error"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/Preconfig/PreconfigService;->stopSelf()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v7, v8

    .end local v8    # "fr":Ljava/io/FileReader;
    .restart local v7    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .line 120
    :cond_6
    :try_start_4
    const-string v9, "PreconfigService"

    const-string v10, "A device has no /efs/imei/mps_code.dat"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/Preconfig/PreconfigService;->stopSelf()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    .line 123
    .end local v6    # "fe":Ljava/io/File;
    :catch_0
    move-exception v4

    .line 124
    .local v4, "e":Ljava/io/IOException;
    :goto_3
    const-string v9, "PreconfigService"

    const-string v10, "File error on /efs/imei/mps_code.dat"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 127
    invoke-virtual {v7}, Ljava/io/FileReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    .line 131
    :goto_4
    invoke-virtual {p0}, Lcom/sec/android/Preconfig/PreconfigService;->stopSelf()V

    goto :goto_2

    .line 128
    :catch_1
    move-exception v5

    .line 129
    .local v5, "ex":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    .line 123
    .end local v4    # "e":Ljava/io/IOException;
    .end local v5    # "ex":Ljava/lang/Exception;
    .end local v7    # "fr":Ljava/io/FileReader;
    .restart local v6    # "fe":Ljava/io/File;
    .restart local v8    # "fr":Ljava/io/FileReader;
    :catch_2
    move-exception v4

    move-object v7, v8

    .end local v8    # "fr":Ljava/io/FileReader;
    .restart local v7    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v7    # "fr":Ljava/io/FileReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "fr":Ljava/io/FileReader;
    :catch_3
    move-exception v4

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    move-object v7, v8

    .end local v8    # "fr":Ljava/io/FileReader;
    .restart local v7    # "fr":Ljava/io/FileReader;
    goto :goto_3
.end method
