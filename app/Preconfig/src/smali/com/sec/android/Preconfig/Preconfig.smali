.class public Lcom/sec/android/Preconfig/Preconfig;
.super Landroid/app/ListActivity;
.source "Preconfig.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static TAG_CSCFEATURE_AUTOPRECONFIG:Ljava/lang/String;

.field private static final isDebuggableMonkeyBuild:Z

.field private static final isMarvell:Z


# instance fields
.field LOG_TAG:Ljava/lang/String;

.field list_fold:[Ljava/lang/String;

.field private mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

.field public mHandler:Landroid/os/Handler;

.field protected mInputCode:Landroid/widget/EditText;

.field private newSalesCode:Ljava/lang/String;

.field path_csc:Ljava/lang/String;

.field path_mps:Ljava/lang/String;

.field userInput:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 62
    const-string v0, "ro.monkey"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/Preconfig/Preconfig;->isDebuggableMonkeyBuild:Z

    .line 65
    const-string v0, "mrvl"

    const-string v1, "ro.board.platform"

    const-string v2, "Unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/Preconfig/Preconfig;->isMarvell:Z

    .line 84
    const-string v0, "CscFeature_Common_AutoConfigurationType"

    sput-object v0, Lcom/sec/android/Preconfig/Preconfig;->TAG_CSCFEATURE_AUTOPRECONFIG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 54
    const-string v0, "Preconfig"

    iput-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->userInput:Ljava/lang/String;

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->newSalesCode:Ljava/lang/String;

    .line 76
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->list_fold:[Ljava/lang/String;

    .line 80
    const-string v0, "/system/csc"

    iput-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->path_csc:Ljava/lang/String;

    .line 82
    const-string v0, "/efs/imei/mps_code.dat"

    iput-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->path_mps:Ljava/lang/String;

    .line 360
    new-instance v0, Lcom/sec/android/Preconfig/Preconfig$2;

    invoke-direct {v0, p0}, Lcom/sec/android/Preconfig/Preconfig$2;-><init>(Lcom/sec/android/Preconfig/Preconfig;)V

    iput-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private _setMpsCode(Ljava/lang/String;)V
    .locals 3
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 299
    sget-boolean v1, Lcom/sec/android/Preconfig/Preconfig;->isMarvell:Z

    if-eqz v1, :cond_0

    .line 300
    iget-object v1, p0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    const-string v2, "send intent to factory to set mps code"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SET_MPS_CODE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 302
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 303
    invoke-virtual {p0, v0}, Lcom/sec/android/Preconfig/Preconfig;->sendBroadcast(Landroid/content/Intent;)V

    .line 308
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 305
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/Preconfig/Preconfig;->_setMpsCodeWrite(Ljava/lang/String;)V

    .line 306
    invoke-direct {p0}, Lcom/sec/android/Preconfig/Preconfig;->runAndroidFactoryReset()V

    goto :goto_0
.end method

.method private _setMpsCodeWrite(Ljava/lang/String;)V
    .locals 9
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 311
    const-string v0, ""

    .line 312
    .local v0, "Sales_str":Ljava/lang/String;
    const/4 v6, 0x3

    invoke-virtual {p1, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 314
    new-instance v5, Ljava/io/File;

    const-string v6, "/efs/imei"

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 315
    .local v5, "verifyDir":Ljava/io/File;
    const/4 v3, 0x0

    .line 317
    .local v3, "out":Ljava/io/BufferedWriter;
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-nez v6, :cond_0

    .line 318
    iget-object v6, p0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    const-string v7, "No directoy, make imei directoy"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 322
    :cond_0
    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 323
    iget-object v6, p0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "success setreadable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :cond_1
    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 326
    iget-object v6, p0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "success setexecutable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    :cond_2
    new-instance v4, Ljava/io/BufferedWriter;

    new-instance v6, Ljava/io/FileWriter;

    const-string v7, "/efs/imei/mps_code.dat"

    invoke-direct {v6, v7}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 333
    .end local v3    # "out":Ljava/io/BufferedWriter;
    .local v4, "out":Ljava/io/BufferedWriter;
    :try_start_1
    invoke-virtual {v4, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 335
    new-instance v1, Ljava/io/File;

    const-string v6, "/efs/imei/mps_code.dat"

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 336
    .local v1, "SetPermission":Ljava/io/File;
    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 337
    iget-object v6, p0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "success setreadable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    :cond_3
    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 340
    iget-object v6, p0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "success setexecutable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    :cond_4
    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Ljava/io/File;->setWritable(ZZ)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 343
    iget-object v6, p0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "success setWritable:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 351
    :cond_5
    if-eqz v4, :cond_6

    .line 352
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_6
    :goto_0
    move-object v3, v4

    .line 357
    .end local v1    # "SetPermission":Ljava/io/File;
    .end local v4    # "out":Ljava/io/BufferedWriter;
    .restart local v3    # "out":Ljava/io/BufferedWriter;
    :cond_7
    :goto_1
    return-void

    .line 353
    .end local v3    # "out":Ljava/io/BufferedWriter;
    .restart local v1    # "SetPermission":Ljava/io/File;
    .restart local v4    # "out":Ljava/io/BufferedWriter;
    :catch_0
    move-exception v2

    .line 354
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 346
    .end local v1    # "SetPermission":Ljava/io/File;
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "out":Ljava/io/BufferedWriter;
    .restart local v3    # "out":Ljava/io/BufferedWriter;
    :catch_1
    move-exception v2

    .line 347
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 351
    if-eqz v3, :cond_7

    .line 352
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 353
    :catch_2
    move-exception v2

    .line 354
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 350
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 351
    :goto_3
    if-eqz v3, :cond_8

    .line 352
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 355
    :cond_8
    :goto_4
    throw v6

    .line 353
    :catch_3
    move-exception v2

    .line 354
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 350
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "out":Ljava/io/BufferedWriter;
    .restart local v4    # "out":Ljava/io/BufferedWriter;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "out":Ljava/io/BufferedWriter;
    .restart local v3    # "out":Ljava/io/BufferedWriter;
    goto :goto_3

    .line 346
    .end local v3    # "out":Ljava/io/BufferedWriter;
    .restart local v4    # "out":Ljava/io/BufferedWriter;
    :catch_4
    move-exception v2

    move-object v3, v4

    .end local v4    # "out":Ljava/io/BufferedWriter;
    .restart local v3    # "out":Ljava/io/BufferedWriter;
    goto :goto_2
.end method

.method private _setSalesCode(Ljava/lang/String;)V
    .locals 8
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 267
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 268
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 269
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v4, 0x3

    invoke-virtual {p1, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 270
    iget-object v4, p0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "set user input code : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    array-length v4, v4

    add-int/lit8 v3, v4, 0x5

    .line 274
    .local v3, "fileSize":I
    const/4 v4, 0x6

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 275
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 276
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 277
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 278
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 279
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    array-length v6, v6

    invoke-virtual {v1, v4, v5, v6}, Ljava/io/DataOutputStream;->write([BII)V

    .line 282
    iget-object v4, p0, Lcom/sec/android/Preconfig/Preconfig;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/Preconfig/Preconfig;->mHandler:Landroid/os/Handler;

    const/16 v7, 0x7ce

    invoke-virtual {v6, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 296
    .end local v3    # "fileSize":I
    :goto_0
    return-void

    .line 291
    .restart local v3    # "fileSize":I
    :catch_0
    move-exception v2

    .line 292
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 285
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fileSize":I
    :catch_1
    move-exception v2

    .line 286
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 290
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 291
    :catch_2
    move-exception v2

    .line 292
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 289
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 290
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 293
    throw v4

    .line 291
    :catch_3
    move-exception v2

    .line 292
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 52
    sget-boolean v0, Lcom/sec/android/Preconfig/Preconfig;->isDebuggableMonkeyBuild:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/Preconfig/Preconfig;)Lcom/samsung/android/sec_platform_library/FactoryPhone;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/Preconfig/Preconfig;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/Preconfig/Preconfig;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/Preconfig/Preconfig;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->newSalesCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/Preconfig/Preconfig;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/Preconfig/Preconfig;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/Preconfig/Preconfig;->_setMpsCode(Ljava/lang/String;)V

    return-void
.end method

.method private removeAutoPreconfigFlag()V
    .locals 5

    .prologue
    .line 418
    new-instance v0, Ljava/io/File;

    const-string v2, "/efs/imei/ap_key.dat"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 419
    .local v0, "autoPreconfigFlag":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 420
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    .line 421
    .local v1, "result":Z
    iget-object v2, p0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remove AutoPreconfigFlag : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    .end local v1    # "result":Z
    :cond_0
    return-void
.end method

.method private runAndroidFactoryReset()V
    .locals 2

    .prologue
    .line 405
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MASTER_CLEAR"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/Preconfig/Preconfig;->sendBroadcast(Landroid/content/Intent;)V

    .line 406
    iget-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    const-string v1, "send MASTER_CLEAR intent to do factoryreset"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 229
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 248
    :goto_0
    return-void

    .line 233
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->userInput:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    const-string v1, "to short input!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    const-string v0, "Must be THREE chars."

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->userInput:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/Preconfig/Preconfig;->setSalesCode(Ljava/lang/String;)V

    goto :goto_0

    .line 245
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/Preconfig/Preconfig;->finish()V

    goto :goto_0

    .line 229
    nop

    :pswitch_data_0
    .packed-switch 0x7f050002
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 20
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 119
    invoke-super/range {p0 .. p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 121
    new-instance v17, Lcom/samsung/android/sec_platform_library/FactoryPhone;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/sec_platform_library/FactoryPhone;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/Preconfig/Preconfig;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 123
    const/high16 v17, 0x7f030000

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/Preconfig/Preconfig;->setContentView(I)V

    .line 125
    const/high16 v17, 0x7f050000

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/Preconfig/Preconfig;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/EditText;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/Preconfig/Preconfig;->mInputCode:Landroid/widget/EditText;

    .line 127
    const v17, 0x7f050002

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/Preconfig/Preconfig;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 128
    .local v6, "InstallButton":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    const v17, 0x7f050003

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/Preconfig/Preconfig;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 131
    .local v4, "CancelButton":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    const v17, 0x7f050001

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/Preconfig/Preconfig;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 135
    .local v5, "EmptyText":Landroid/widget/TextView;
    new-instance v11, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/Preconfig/Preconfig;->path_csc:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 136
    .local v11, "fp":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v13

    .line 137
    .local v13, "fs":[Ljava/io/File;
    if-eqz v13, :cond_2

    .line 138
    const/4 v7, 0x0

    .line 140
    .local v7, "Singlecheck":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    array-length v0, v13

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v14, v0, :cond_1

    .line 141
    aget-object v17, v13, v14

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 142
    add-int/lit8 v7, v7, 0x1

    .line 140
    :cond_0
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 145
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Singlecheck count : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    if-nez v7, :cond_3

    .line 147
    const-string v17, "PreconfigUI does not support SingleCSC."

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/widget/Toast;->show()V

    .line 149
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/Preconfig/Preconfig;->finish()V

    .line 208
    .end local v7    # "Singlecheck":I
    .end local v14    # "i":I
    :cond_2
    :goto_1
    return-void

    .line 151
    .restart local v7    # "Singlecheck":I
    .restart local v14    # "i":I
    :cond_3
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_5

    .line 152
    new-instance v17, Lcom/sec/android/Preconfig/Preconfig$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/Preconfig/Preconfig$1;-><init>(Lcom/sec/android/Preconfig/Preconfig;)V

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/Preconfig/Preconfig;->list_fold:[Ljava/lang/String;

    .line 168
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "OK - CSC_File Path"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :goto_2
    new-instance v17, Landroid/widget/ArrayAdapter;

    const v18, 0x109000f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/Preconfig/Preconfig;->list_fold:[Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move/from16 v2, v18

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/Preconfig/Preconfig;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 176
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/Preconfig/Preconfig;->getListView()Landroid/widget/ListView;

    move-result-object v15

    .line 178
    .local v15, "listView":Landroid/widget/ListView;
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 179
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 182
    :try_start_0
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/Preconfig/Preconfig;->path_mps:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 183
    .local v10, "fe":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_2

    .line 184
    new-instance v12, Ljava/io/FileReader;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/Preconfig/Preconfig;->path_mps:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    .line 186
    .local v12, "fr":Ljava/io/FileReader;
    new-instance v8, Ljava/io/BufferedReader;

    invoke-direct {v8, v12}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 188
    .local v8, "br":Ljava/io/BufferedReader;
    invoke-virtual {v8}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v16

    .line 190
    .local v16, "str":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Comfirm_SALES:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/Preconfig/Preconfig;->mInputCode:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 194
    const/4 v14, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/Preconfig/Preconfig;->list_fold:[Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v14, v0, :cond_6

    .line 195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/Preconfig/Preconfig;->list_fold:[Ljava/lang/String;

    move-object/from16 v17, v0

    aget-object v17, v17, v14

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 196
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v15, v14, v0}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 197
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/Preconfig/Preconfig;->userInput:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    :cond_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 170
    .end local v8    # "br":Ljava/io/BufferedReader;
    .end local v10    # "fe":Ljava/io/File;
    .end local v12    # "fr":Ljava/io/FileReader;
    .end local v15    # "listView":Landroid/widget/ListView;
    .end local v16    # "str":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "Error - CSC_File Path"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 200
    .restart local v8    # "br":Ljava/io/BufferedReader;
    .restart local v10    # "fe":Ljava/io/File;
    .restart local v12    # "fr":Ljava/io/FileReader;
    .restart local v15    # "listView":Landroid/widget/ListView;
    .restart local v16    # "str":Ljava/lang/String;
    :cond_6
    :try_start_1
    invoke-virtual {v8}, Ljava/io/BufferedReader;->close()V

    .line 201
    invoke-virtual {v12}, Ljava/io/FileReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 203
    .end local v8    # "br":Ljava/io/BufferedReader;
    .end local v10    # "fe":Ljava/io/File;
    .end local v12    # "fr":Ljava/io/FileReader;
    .end local v16    # "str":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 204
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->disconnectFromRilService()V

    .line 215
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 216
    return-void
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->list_fold:[Ljava/lang/String;

    aget-object v0, v0, p3

    iput-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->userInput:Ljava/lang/String;

    .line 223
    iget-object v0, p0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onListItemClick = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    invoke-super/range {p0 .. p5}, Landroid/app/ListActivity;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 226
    return-void
.end method

.method setEndModeData()[B
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 88
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 89
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 90
    .local v1, "dos":Ljava/io/DataOutputStream;
    const/4 v3, 0x4

    .line 93
    .local v3, "fileSize":I
    const/16 v5, 0xc

    :try_start_0
    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 94
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 95
    const/4 v5, 0x5

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 96
    const/4 v5, 0x2

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    :try_start_1
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 111
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    :goto_0
    return-object v4

    .line 105
    :catch_0
    move-exception v2

    .line 106
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 98
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 99
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    iget-object v5, p0, Lcom/sec/android/Preconfig/Preconfig;->LOG_TAG:Ljava/lang/String;

    const-string v6, "IOException in getServMQueryData!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 104
    :try_start_3
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 105
    :catch_2
    move-exception v2

    .line 106
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 103
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 104
    :try_start_4
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 107
    throw v5

    .line 105
    :catch_3
    move-exception v2

    .line 106
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected setSalesCode(Ljava/lang/String;)V
    .locals 4
    .param p1, "salesCode"    # Ljava/lang/String;

    .prologue
    .line 251
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/Preconfig/Preconfig;->newSalesCode:Ljava/lang/String;

    .line 254
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    sget-object v2, Lcom/sec/android/Preconfig/Preconfig;->TAG_CSCFEATURE_AUTOPRECONFIG:Ljava/lang/String;

    const-string v3, "DISABLE"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v0, v1, v2

    .line 255
    .local v0, "cscCode":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 256
    invoke-direct {p0}, Lcom/sec/android/Preconfig/Preconfig;->removeAutoPreconfigFlag()V

    .line 262
    :cond_0
    iget-object v1, p0, Lcom/sec/android/Preconfig/Preconfig;->newSalesCode:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/Preconfig/Preconfig;->_setSalesCode(Ljava/lang/String;)V

    .line 264
    return-void
.end method
