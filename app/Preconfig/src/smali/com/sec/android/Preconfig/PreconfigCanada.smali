.class public Lcom/sec/android/Preconfig/PreconfigCanada;
.super Lcom/sec/android/Preconfig/Preconfig;
.source "PreconfigCanada.java"


# static fields
.field static sCanadaOperatorLists:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 16
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "RCS,RWC,FMC,CHR,MTA"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "TLS,KDO,SPC,PMB"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "BMC,BWA,MTS,VMC,SOL,PCM"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "VTR,GLW,MCT,ESK"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/Preconfig/PreconfigCanada;->sCanadaOperatorLists:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/android/Preconfig/Preconfig;-><init>()V

    return-void
.end method

.method public static isCanadaOperator(Ljava/lang/String;)Z
    .locals 6
    .param p0, "operator"    # Ljava/lang/String;

    .prologue
    .line 24
    const/4 v1, 0x0

    .line 27
    .local v1, "bCanadaOperator":Z
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 28
    sget-object v0, Lcom/sec/android/Preconfig/PreconfigCanada;->sCanadaOperatorLists:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 29
    .local v4, "list":Ljava/lang/String;
    invoke-virtual {v4, p0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 30
    const/4 v1, 0x1

    .line 36
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "list":Ljava/lang/String;
    :cond_0
    return v1

    .line 28
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    .restart local v4    # "list":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private removeAutoPreconfigFlag()V
    .locals 5

    .prologue
    .line 100
    new-instance v0, Ljava/io/File;

    const-string v2, "/efs/imei/ap_key.dat"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 101
    .local v0, "autoPreconfigFlag":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    .line 103
    .local v1, "result":Z
    iget-object v2, p0, Lcom/sec/android/Preconfig/PreconfigCanada;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remove AutoPreconfigFlag : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    .end local v1    # "result":Z
    :cond_0
    return-void
.end method

.method private updateOperatorListForCanada(Ljava/lang/String;)Z
    .locals 12
    .param p1, "currentOperator"    # Ljava/lang/String;

    .prologue
    .line 50
    const/4 v1, 0x0

    .line 51
    .local v1, "bCanadaOperator":Z
    const/4 v8, 0x0

    .line 54
    .local v8, "opratorList":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_0

    .line 55
    sget-object v0, Lcom/sec/android/Preconfig/PreconfigCanada;->sCanadaOperatorLists:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 56
    .local v4, "list":Ljava/lang/String;
    invoke-virtual {v4, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 57
    move-object v8, v4

    .line 63
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "list":Ljava/lang/String;
    :cond_0
    if-eqz v8, :cond_4

    .line 65
    new-instance v6, Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/sec/android/Preconfig/PreconfigCanada;->list_fold:[Ljava/lang/String;

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-direct {v6, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 66
    .local v6, "newList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/android/Preconfig/PreconfigCanada;->list_fold:[Ljava/lang/String;

    .restart local v0    # "arr$":[Ljava/lang/String;
    array-length v3, v0

    .restart local v3    # "len$":I
    const/4 v2, 0x0

    .restart local v2    # "i$":I
    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v7, v0, v2

    .line 67
    .local v7, "operator":Ljava/lang/String;
    invoke-virtual {v8, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 68
    invoke-interface {v6, v7}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 66
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 55
    .end local v6    # "newList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v7    # "operator":Ljava/lang/String;
    .restart local v4    # "list":Ljava/lang/String;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 73
    .end local v4    # "list":Ljava/lang/String;
    .restart local v6    # "newList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_4

    .line 74
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Ljava/lang/String;

    invoke-interface {v6, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    iput-object v9, p0, Lcom/sec/android/Preconfig/PreconfigCanada;->list_fold:[Ljava/lang/String;

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/Preconfig/PreconfigCanada;->getListView()Landroid/widget/ListView;

    move-result-object v5

    .line 77
    .local v5, "listView":Landroid/widget/ListView;
    new-instance v9, Landroid/widget/ArrayAdapter;

    const v10, 0x109000f

    iget-object v11, p0, Lcom/sec/android/Preconfig/PreconfigCanada;->list_fold:[Ljava/lang/String;

    invoke-direct {v9, p0, v10, v11}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v5, v9}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 81
    invoke-interface {v6, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v9

    const/4 v10, 0x1

    invoke-virtual {v5, v9, v10}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 82
    const/4 v1, 0x1

    .line 86
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v5    # "listView":Landroid/widget/ListView;
    .end local v6    # "newList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    return v1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 90
    invoke-super {p0, p1}, Lcom/sec/android/Preconfig/Preconfig;->onClick(Landroid/view/View;)V

    .line 93
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f050002

    if-ne v0, v1, :cond_0

    .line 94
    invoke-direct {p0}, Lcom/sec/android/Preconfig/PreconfigCanada;->removeAutoPreconfigFlag()V

    .line 96
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/sec/android/Preconfig/Preconfig;->onCreate(Landroid/os/Bundle;)V

    .line 42
    iget-object v1, p0, Lcom/sec/android/Preconfig/PreconfigCanada;->mInputCode:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "currentOperator":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/Preconfig/PreconfigCanada;->updateOperatorListForCanada(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 44
    iget-object v1, p0, Lcom/sec/android/Preconfig/PreconfigCanada;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[bCanadaOperator=false]SALES_CODE:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/Preconfig/PreconfigCanada;->finish()V

    .line 47
    :cond_0
    return-void
.end method
