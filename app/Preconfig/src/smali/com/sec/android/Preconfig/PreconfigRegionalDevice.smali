.class public Lcom/sec/android/Preconfig/PreconfigRegionalDevice;
.super Lcom/sec/android/Preconfig/Preconfig;
.source "PreconfigRegionalDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;
    }
.end annotation


# static fields
.field static sRegionalDeviceInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->sRegionalDeviceInfoList:Ljava/util/ArrayList;

    .line 30
    sget-object v0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->sRegionalDeviceInfoList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;

    const-string v2, "Telefonica"

    const-string v3, "VIA"

    new-array v4, v10, [Ljava/lang/String;

    const-string v5, "XEC"

    aput-object v5, v4, v7

    const-string v5, "VIA"

    aput-object v5, v4, v8

    const-string v5, "O2C"

    aput-object v5, v4, v9

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    sget-object v0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->sRegionalDeviceInfoList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;

    const-string v2, "Telefonica"

    const-string v3, "VID"

    new-array v4, v10, [Ljava/lang/String;

    const-string v5, "O2U"

    aput-object v5, v4, v7

    const-string v5, "VID"

    aput-object v5, v4, v8

    const-string v5, "VIT"

    aput-object v5, v4, v9

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    sget-object v0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->sRegionalDeviceInfoList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;

    const-string v2, "Voda"

    const-string v3, "VNL,VNN"

    const/16 v4, 0x11

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "ATL"

    aput-object v5, v4, v7

    const-string v5, "TCL"

    aput-object v5, v4, v8

    const-string v5, "VDH"

    aput-object v5, v4, v9

    const-string v5, "VDI"

    aput-object v5, v4, v10

    const-string v5, "VDP"

    aput-object v5, v4, v11

    const/4 v5, 0x5

    const-string v6, "VDR"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string v6, "VNL"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    const-string v6, "VOP"

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string v6, "OMN"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    const-string v6, "CNX"

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string v6, "VD2"

    aput-object v6, v4, v5

    const/16 v5, 0xb

    const-string v6, "VDC"

    aput-object v6, v4, v5

    const/16 v5, 0xc

    const-string v6, "VDF"

    aput-object v6, v4, v5

    const/16 v5, 0xd

    const-string v6, "VGR"

    aput-object v6, v4, v5

    const/16 v5, 0xe

    const-string v6, "VNN"

    aput-object v6, v4, v5

    const/16 v5, 0xf

    const-string v6, "VOD"

    aput-object v6, v4, v5

    const/16 v5, 0x10

    const-string v6, "XFV"

    aput-object v6, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    sget-object v0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->sRegionalDeviceInfoList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;

    const-string v2, "H3N"

    const-string v3, "H3G"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "H3G"

    aput-object v5, v4, v7

    const-string v5, "3IE"

    aput-object v5, v4, v8

    const-string v5, "3NL"

    aput-object v5, v4, v9

    const-string v5, "DRE"

    aput-object v5, v4, v10

    const-string v5, "HTS"

    aput-object v5, v4, v11

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    sget-object v0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->sRegionalDeviceInfoList:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;

    const-string v2, "H3R"

    const-string v3, "HUI"

    new-array v4, v10, [Ljava/lang/String;

    const-string v5, "HUI"

    aput-object v5, v4, v7

    const-string v5, "HTD"

    aput-object v5, v4, v8

    const-string v5, "3NN"

    aput-object v5, v4, v9

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sec/android/Preconfig/Preconfig;-><init>()V

    .line 13
    return-void
.end method

.method private static getRegionalDeviceInfo(Ljava/lang/String;)Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;
    .locals 9
    .param p0, "currentSalesCode"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 80
    if-nez p0, :cond_1

    move-object v4, v7

    .line 93
    :cond_0
    :goto_0
    return-object v4

    .line 83
    :cond_1
    sget-object v8, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->sRegionalDeviceInfoList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;

    .line 84
    .local v4, "info":Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;
    iget-object v6, v4, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;->mSalesCodeList:[Ljava/lang/String;

    .line 86
    .local v6, "list":[Ljava/lang/String;
    move-object v0, v6

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_2

    aget-object v1, v0, v3

    .line 87
    .local v1, "customer":Ljava/lang/String;
    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 86
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "customer":Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "info":Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;
    .end local v5    # "len$":I
    .end local v6    # "list":[Ljava/lang/String;
    :cond_3
    move-object v4, v7

    .line 93
    goto :goto_0
.end method

.method public static isRegionalDevice(Ljava/lang/String;)Z
    .locals 1
    .param p0, "currentSalesCode"    # Ljava/lang/String;

    .prologue
    .line 97
    invoke-static {p0}, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->getRegionalDeviceInfo(Ljava/lang/String;)Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x1

    .line 99
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeAutoPreconfigFlag()V
    .locals 5

    .prologue
    .line 119
    new-instance v0, Ljava/io/File;

    const-string v2, "/efs/imei/ap_key.dat"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 120
    .local v0, "autoPreconfigFlag":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 121
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    .line 122
    .local v1, "result":Z
    iget-object v2, p0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remove AutoPreconfigFlag : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    .end local v1    # "result":Z
    :cond_0
    return-void
.end method

.method private setDefaultCustomer(Ljava/lang/String;)V
    .locals 10
    .param p1, "salesCode"    # Ljava/lang/String;

    .prologue
    .line 49
    iget-object v7, p0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Sales Code : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    if-eqz p1, :cond_1

    .line 52
    invoke-static {p1}, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->getRegionalDeviceInfo(Ljava/lang/String;)Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;

    move-result-object v5

    .line 54
    .local v5, "info":Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;
    if-eqz v5, :cond_3

    .line 55
    iget-object v7, v5, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;->mDefaultSalesCode:Ljava/lang/String;

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 57
    .local v3, "defaultSalesCodeArray":[Ljava/lang/String;
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_0

    aget-object v2, v0, v4

    .line 58
    .local v2, "defaultSalesCode":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/system/csc/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 59
    .local v1, "cscDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 60
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Set Default "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v5, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;->mOperatorName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Operator : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {p0, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 65
    invoke-virtual {p0, v2}, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->setSalesCode(Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->removeAutoPreconfigFlag()V

    .line 72
    .end local v1    # "cscDir":Ljava/io/File;
    .end local v2    # "defaultSalesCode":Ljava/lang/String;
    :cond_0
    iget-object v7, p0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->LOG_TAG:Ljava/lang/String;

    const-string v8, "There are no defaultSalesCodes in CSC."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "defaultSalesCodeArray":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "info":Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;
    .end local v6    # "len$":I
    :cond_1
    :goto_1
    return-void

    .line 69
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "cscDir":Ljava/io/File;
    .restart local v2    # "defaultSalesCode":Ljava/lang/String;
    .restart local v3    # "defaultSalesCodeArray":[Ljava/lang/String;
    .restart local v4    # "i$":I
    .restart local v5    # "info":Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;
    .restart local v6    # "len$":I
    :cond_2
    iget-object v7, p0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "is not exist in CSC."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 74
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "cscDir":Ljava/io/File;
    .end local v2    # "defaultSalesCode":Ljava/lang/String;
    .end local v3    # "defaultSalesCodeArray":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    :cond_3
    iget-object v7, p0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "is not RegionalDevice salescode."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/sec/android/Preconfig/Preconfig;->onCreate(Landroid/os/Bundle;)V

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 106
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "CODE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, "salesCode":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 109
    invoke-direct {p0, v1}, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->setDefaultCustomer(Ljava/lang/String;)V

    .line 114
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->finish()V

    .line 115
    return-void

    .line 111
    :cond_0
    iget-object v2, p0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice;->LOG_TAG:Ljava/lang/String;

    const-string v3, "CODE is null."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
