.class Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;
.super Ljava/lang/Object;
.source "PreconfigRegionalDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/Preconfig/PreconfigRegionalDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RegionalDeviceInfo"
.end annotation


# instance fields
.field mDefaultSalesCode:Ljava/lang/String;

.field mOperatorName:Ljava/lang/String;

.field mSalesCodeList:[Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "operatorName"    # Ljava/lang/String;
    .param p2, "defaultSalesCode"    # Ljava/lang/String;
    .param p3, "salesCodeList"    # [Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;->mOperatorName:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;->mDefaultSalesCode:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lcom/sec/android/Preconfig/PreconfigRegionalDevice$RegionalDeviceInfo;->mSalesCodeList:[Ljava/lang/String;

    .line 24
    return-void
.end method
