.class Lcom/sec/android/Preconfig/Preconfig$2;
.super Landroid/os/Handler;
.source "Preconfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/Preconfig/Preconfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/Preconfig/Preconfig;


# direct methods
.method constructor <init>(Lcom/sec/android/Preconfig/Preconfig;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/sec/android/Preconfig/Preconfig$2;->this$0:Lcom/sec/android/Preconfig/Preconfig;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 362
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 393
    :cond_0
    :goto_0
    return-void

    .line 365
    :sswitch_0
    const/4 v0, 0x0

    .line 373
    .local v0, "data":[B
    # getter for: Lcom/sec/android/Preconfig/Preconfig;->isDebuggableMonkeyBuild:Z
    invoke-static {}, Lcom/sec/android/Preconfig/Preconfig;->access$000()Z

    move-result v1

    if-nez v1, :cond_0

    .line 378
    iget-object v1, p0, Lcom/sec/android/Preconfig/Preconfig$2;->this$0:Lcom/sec/android/Preconfig/Preconfig;

    invoke-virtual {v1}, Lcom/sec/android/Preconfig/Preconfig;->setEndModeData()[B

    move-result-object v0

    .line 380
    iget-object v1, p0, Lcom/sec/android/Preconfig/Preconfig$2;->this$0:Lcom/sec/android/Preconfig/Preconfig;

    # getter for: Lcom/sec/android/Preconfig/Preconfig;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;
    invoke-static {v1}, Lcom/sec/android/Preconfig/Preconfig;->access$100(Lcom/sec/android/Preconfig/Preconfig;)Lcom/samsung/android/sec_platform_library/FactoryPhone;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/Preconfig/Preconfig$2;->this$0:Lcom/sec/android/Preconfig/Preconfig;

    iget-object v2, v2, Lcom/sec/android/Preconfig/Preconfig;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x3f0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto :goto_0

    .line 387
    .end local v0    # "data":[B
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/Preconfig/Preconfig$2;->this$0:Lcom/sec/android/Preconfig/Preconfig;

    iget-object v2, p0, Lcom/sec/android/Preconfig/Preconfig$2;->this$0:Lcom/sec/android/Preconfig/Preconfig;

    # getter for: Lcom/sec/android/Preconfig/Preconfig;->newSalesCode:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/Preconfig/Preconfig;->access$200(Lcom/sec/android/Preconfig/Preconfig;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/Preconfig/Preconfig;->_setMpsCode(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/Preconfig/Preconfig;->access$300(Lcom/sec/android/Preconfig/Preconfig;Ljava/lang/String;)V

    goto :goto_0

    .line 362
    nop

    :sswitch_data_0
    .sparse-switch
        0x3f0 -> :sswitch_1
        0x7ce -> :sswitch_0
    .end sparse-switch
.end method
