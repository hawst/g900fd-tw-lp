.class Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothTestService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bluetoothtest/BluetoothTestService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    const/high16 v7, -0x80000000

    .line 97
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "action":Ljava/lang/String;
    const-string v4, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 101
    const-string v4, "BluetoothTestService"

    const-string v5, "BluetoothDevice.ACTION_FOUND"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIfFound:Z
    invoke-static {v4}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$000(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 105
    const-string v4, "BluetoothTestService"

    const-string v5, "BluetoothDevice.ACTION_FOUND:again"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    const/4 v5, 0x1

    # setter for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIfFound:Z
    invoke-static {v4, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$002(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;Z)Z

    .line 109
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # invokes: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothDiscoveryStop()V
    invoke-static {v4}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$100(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)V

    .line 110
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    const-string v5, "FOUND"

    # invokes: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothResponse(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$200(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :cond_2
    const-string v4, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 112
    const-string v4, "android.bluetooth.adapter.extra.STATE"

    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 113
    .local v3, "state":I
    const-string v4, "BluetoothTestService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "BluetoothDevice.ACTTION_STATE_CHANGED, state="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    const/16 v4, 0xc

    if-ne v3, v4, :cond_3

    .line 116
    const-string v4, "BluetoothTestService"

    const-string v5, "BT STATE = ON"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    const-string v4, "BluetoothTestService"

    const-string v5, "Give Delay before setting discoverable"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const-wide/16 v4, 0x1f4

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    :goto_1
    const-string v4, "BluetoothTestService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Setting device to discoverable mode: timeout: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mDiscoverableTime:I
    invoke-static {v6}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$300(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v4}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$400(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mDiscoverableTime:I
    invoke-static {v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$300(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/bluetooth/BluetoothAdapter;->setDiscoverableTimeout(I)V

    .line 128
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v4}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$400(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v4

    const/16 v5, 0x17

    invoke-virtual {v4, v5, v8}, Landroid/bluetooth/BluetoothAdapter;->setScanMode(II)Z

    .line 129
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # setter for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIfFound:Z
    invoke-static {v4, v8}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$002(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;Z)Z

    .line 130
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    const-string v5, "ON"

    # invokes: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothResponse(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$200(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 121
    :catch_0
    move-exception v1

    .line 123
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 131
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_3
    const/16 v4, 0xa

    if-ne v3, v4, :cond_4

    .line 132
    const-string v4, "BluetoothTestService"

    const-string v5, "BT STATE = OFF"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    const-string v5, "OFF"

    # invokes: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothResponse(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$200(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 134
    :cond_4
    if-ne v3, v7, :cond_0

    .line 135
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    const-string v5, "NG"

    # invokes: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothResponse(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$200(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 137
    .end local v3    # "state":I
    :cond_5
    const-string v4, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 138
    const-string v4, "BluetoothTestService"

    const-string v5, "ACTION_DISCOVERY_FINISHED"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIsBleSearchCount:I
    invoke-static {v4}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)I

    move-result v4

    if-lez v4, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIsBleSearchCount:I
    invoke-static {v4}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)I

    move-result v4

    const/16 v5, 0x9

    if-gt v4, v5, :cond_6

    .line 141
    const-string v4, "BluetoothTestService"

    const-string v5, "IS BT LE SEARCH"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # operator++ for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIsBleSearchCount:I
    invoke-static {v4}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$508(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)I

    .line 143
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    const/16 v5, 0x2710

    # setter for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mTimeout:I
    invoke-static {v4, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$602(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;I)I

    .line 146
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v4}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$400(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;
    invoke-static {v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    goto/16 :goto_0

    .line 149
    :cond_6
    const-string v4, "BluetoothTestService"

    const-string v5, "IS NOT BT LE SEARCH"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # invokes: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothDiscoveryStop()V
    invoke-static {v4}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$100(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)V

    .line 152
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIfFound:Z
    invoke-static {v4}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$000(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 153
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    const-string v5, "NOT FOUND"

    # invokes: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothResponse(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$200(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 155
    :cond_7
    const-string v4, "android.bluetooth.adapter.action.ACTION_LE_TESE_END_COMPLETED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 156
    const-string v4, "android.bluetooth.adapter.extra.EXTRA_LE_PACKET_COUNTS"

    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 157
    .local v2, "pktCnts":I
    const-string v4, "BluetoothTestService"

    const-string v5, "LE Packet Counts"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    iget-object v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothResponse(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$200(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
