.class Lcom/sec/android/app/bluetoothtest/BluetoothTestService$2;
.super Landroid/os/Handler;
.source "BluetoothTestService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bluetoothtest/BluetoothTestService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$2;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 165
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 175
    const-string v0, "BluetoothTestService"

    const-string v1, "Wrong MSG"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    :goto_0
    return-void

    .line 167
    :pswitch_0
    const-string v0, "BluetoothTestService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mTimeout: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$2;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mTimeout:I
    invoke-static {v2}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$600(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    const-string v0, "BluetoothTestService"

    const-string v1, "MSG_BT_DISCOVERY_CANCEL_TIMEOUT"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$2;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    # invokes: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothDiscoveryStop()V
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$100(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$2;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    const-string v1, "NOT FOUND"

    # invokes: Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothResponse(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->access$200(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;Ljava/lang/String;)V

    goto :goto_0

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
