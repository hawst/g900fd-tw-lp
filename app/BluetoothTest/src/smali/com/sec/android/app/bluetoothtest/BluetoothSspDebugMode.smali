.class public Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;
.super Landroid/app/Activity;
.source "BluetoothSspDebugMode.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static isRegist:Z

.field private static mButtonHciDisable:Landroid/widget/Button;

.field private static mButtonHciEnable:Landroid/widget/Button;

.field private static mButtonLoopbackDisable:Landroid/widget/Button;

.field private static mButtonLoopbackEnable:Landroid/widget/Button;

.field private static mButtonSspDisable:Landroid/widget/Button;

.field private static mButtonSspEnable:Landroid/widget/Button;


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mContext:Landroid/content/Context;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->isRegist:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 29
    new-instance v0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode$1;-><init>(Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;)V

    iput-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonSspEnable:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$100()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonSspDisable:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonHciEnable:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$300()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonHciDisable:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonLoopbackEnable:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$500()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonLoopbackDisable:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 105
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 107
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->sspDebugConfigure(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mContext:Landroid/content/Context;

    const-string v1, "Enabled SSP debug mode"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 112
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothAdapter;->sspDebugConfigure(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mContext:Landroid/content/Context;

    const-string v1, "Disabled SSP debug mode"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 117
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->configHciSnoopLog(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mContext:Landroid/content/Context;

    const-string v1, "Enabled HCI Logging"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 122
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothAdapter;->configHciSnoopLog(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mContext:Landroid/content/Context;

    const-string v1, "Disabled HCI Logging"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 127
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->configScoLoopback(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mContext:Landroid/content/Context;

    const-string v1, "Enabled SCO Loopback"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 132
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothAdapter;->configScoLoopback(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mContext:Landroid/content/Context;

    const-string v1, "Disabled SCO Loopback"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 137
    :pswitch_6
    invoke-virtual {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->finish()V

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x7f050014
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 62
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    iput-object p0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mContext:Landroid/content/Context;

    .line 66
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothTestUtil;->isWorkMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 67
    const-string v1, "BluetoothSspDebugMode"

    const-string v2, "onCreate() : block BluetoothSspDebugMode"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->finish()V

    .line 101
    :goto_0
    return-void

    .line 72
    :cond_0
    const v1, 0x7f020003

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->setContentView(I)V

    .line 73
    const v1, 0x7f050014

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    sput-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonSspEnable:Landroid/widget/Button;

    .line 74
    const v1, 0x7f050015

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    sput-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonSspDisable:Landroid/widget/Button;

    .line 75
    const v1, 0x7f050016

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    sput-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonHciEnable:Landroid/widget/Button;

    .line 76
    const v1, 0x7f050017

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    sput-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonHciDisable:Landroid/widget/Button;

    .line 77
    const v1, 0x7f050018

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    sput-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonLoopbackEnable:Landroid/widget/Button;

    .line 78
    const v1, 0x7f050019

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    sput-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonLoopbackDisable:Landroid/widget/Button;

    .line 80
    sget-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonSspEnable:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    sget-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonSspDisable:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    sget-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonHciEnable:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    sget-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonHciDisable:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    sget-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonLoopbackEnable:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    sget-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonLoopbackDisable:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    const v1, 0x7f05001a

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_1

    .line 90
    sget-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonSspEnable:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 91
    sget-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonSspDisable:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 92
    sget-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonHciEnable:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 93
    sget-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonHciDisable:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 94
    sget-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonLoopbackEnable:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 95
    sget-object v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonLoopbackDisable:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 97
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 98
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 100
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->isRegist:Z

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 158
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 160
    sget-boolean v0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->isRegist:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 162
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->isRegist:Z

    .line 164
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 146
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 148
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 152
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 154
    return-void
.end method
