.class Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$1;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothLEDutMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/high16 v5, -0x80000000

    const/4 v4, 0x1

    .line 51
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "action":Ljava/lang/String;
    const-string v3, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 53
    const-string v3, "android.bluetooth.adapter.extra.STATE"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 54
    .local v2, "state":I
    packed-switch v2, :pswitch_data_0

    .line 79
    .end local v2    # "state":I
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 57
    .restart local v2    # "state":I
    :pswitch_1
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxLow:Landroid/widget/Spinner;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$000()Landroid/widget/Spinner;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 58
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxMid:Landroid/widget/Spinner;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$100()Landroid/widget/Spinner;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 59
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxMax:Landroid/widget/Spinner;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$200()Landroid/widget/Spinner;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 60
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxInband:Landroid/widget/Spinner;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$300()Landroid/widget/Spinner;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 61
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnRxTest:Landroid/widget/Spinner;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$400()Landroid/widget/Spinner;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setEnabled(Z)V

    goto :goto_0

    .line 75
    .end local v2    # "state":I
    :cond_1
    const-string v3, "android.bluetooth.adapter.action.ACTION_LE_TESE_END_COMPLETED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 76
    const-string v3, "android.bluetooth.adapter.extra.EXTRA_LE_PACKET_COUNTS"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 77
    .local v1, "pktCnts":I
    iget-object v3, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Packet Counts : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 54
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
