.class Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$2;
.super Ljava/lang/Object;
.source "BluetoothLEDutMode.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$2;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 286
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 288
    :sswitch_0
    const-string v0, "BluetoothLEDutMode"

    const-string v1, "Bluetooth LE Test mode Enable"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$2;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$2;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Starting to activate Bluetooth"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$2;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    goto :goto_0

    .line 292
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$2;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 305
    :sswitch_1
    const-string v0, "BluetoothLEDutMode"

    const-string v1, "Bluetooth LE Test End"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$2;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Test End"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$2;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3, v3}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 311
    :sswitch_2
    const-string v0, "BluetoothLEDutMode"

    const-string v1, "Enable Bluetooth LE Test mode(via 2 wires)"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$2;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Enable Bluetooth LE Test mode(via 2 wires)"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$2;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    invoke-virtual {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->EnableBluetoothLEDUTMode()V

    goto :goto_0

    .line 316
    :sswitch_3
    const-string v0, "BluetoothLEDutMode"

    const-string v1, "Bluetooth LE Test mode Disable"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$2;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    invoke-virtual {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->DisableBluetoothLEDUTMode()V

    goto/16 :goto_0

    .line 286
    :sswitch_data_0
    .sparse-switch
        0x7f050003 -> :sswitch_0
        0x7f05000e -> :sswitch_1
        0x7f050010 -> :sswitch_2
        0x7f050011 -> :sswitch_3
    .end sparse-switch
.end method
