.class public Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;
.super Landroid/app/Activity;
.source "BluetoothRfTest.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static isRegist:Z


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mContext:Landroid/content/Context;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->isRegist:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 28
    new-instance v0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest$1;-><init>(Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;)V

    iput-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 109
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 116
    :goto_0
    return-void

    .line 111
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->finish()V

    goto :goto_0

    .line 109
    :pswitch_data_0
    .packed-switch 0x7f050013
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    iput-object p0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mContext:Landroid/content/Context;

    .line 64
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 66
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothTestUtil;->isWorkMode()Z

    move-result v1

    if-nez v1, :cond_1

    .line 67
    const-string v1, "BluetoothRfTest"

    const-string v2, "onCreate() : block BluetoothRfTest"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->finish()V

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 74
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 75
    const-string v1, "com.android.samsungtest.BluetoothRfTestOff"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 77
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->isRegist:Z

    .line 79
    const v1, 0x7f020002

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->setContentView(I)V

    .line 81
    const v1, 0x7f050013

    invoke-virtual {p0, v1}, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_2

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    goto :goto_0

    .line 85
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v1

    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 143
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothAdapter;->dutModeConfigure(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mContext:Landroid/content/Context;

    const-string v1, "Disabled DUT mode"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    .line 151
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->isRegist:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 153
    sput-boolean v2, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->isRegist:Z

    .line 155
    :cond_1
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 120
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothRfTest;->finish()V

    .line 124
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 130
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 132
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 137
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 139
    return-void
.end method
