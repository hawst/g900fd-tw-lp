.class Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;
.super Ljava/lang/Object;
.source "BluetoothLEDutMode.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MySpnListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;
    .param p2, "x1"    # Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$1;

    .prologue
    .line 323
    invoke-direct {p0, p1}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;-><init>(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "v"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/16 v6, 0x27

    const/16 v5, 0x13

    const/4 v4, 0x1

    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 326
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 465
    :pswitch_0
    const-string v0, "BluetoothLEDutMode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Kyle_TxLow]case default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "case default"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 469
    :goto_0
    :pswitch_1
    return-void

    .line 328
    :pswitch_2
    packed-switch p3, :pswitch_data_1

    .line 348
    const-string v0, "BluetoothLEDutMode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Kyle_Output Power]MySpnListener:onItemSelected:pos = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 333
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Output : Low Ch"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v3, v3}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 338
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Output : Mid Ch"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v5, v3}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 343
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Output : Max Ch"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v6, v3}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 353
    :pswitch_6
    packed-switch p3, :pswitch_data_2

    .line 388
    const-string v0, "BluetoothLEDutMode"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Kyle_Modulation]MySpnListener:onItemSelected:pos = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 358
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Modulation : Low Ch with 0x0F"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v3, v4}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 363
    :pswitch_8
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Modulation : Mid Ch with 0x0F"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v5, v4}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 368
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Modulation : Max Ch with 0x0F"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v6, v4}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 373
    :pswitch_a
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Modulation : Low Ch with 0xAA"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v3, v2}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 378
    :pswitch_b
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Modulation : Mid Ch with 0xAA"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v5, v2}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 383
    :pswitch_c
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Modulation : Max Ch with 0xAA"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v6, v2}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 393
    :pswitch_d
    packed-switch p3, :pswitch_data_3

    goto/16 :goto_0

    .line 398
    :pswitch_e
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Carrier frequency : Low Ch"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v3, v2}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 403
    :pswitch_f
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Carrier frequency : Mid Ch"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v5, v2}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 408
    :pswitch_10
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Carrier frequency : Max Ch"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v6, v2}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 417
    :pswitch_11
    packed-switch p3, :pswitch_data_4

    goto/16 :goto_0

    .line 422
    :pswitch_12
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "In-band Emissions : Ch.2"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 427
    :pswitch_13
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "In-band Emissions : Ch.19"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 429
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, v2, v5, v3}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 432
    :pswitch_14
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "In-band Emissions : Ch.37"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    const/16 v1, 0x25

    invoke-virtual {v0, v2, v1, v3}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 441
    :pswitch_15
    packed-switch p3, :pswitch_data_5

    goto/16 :goto_0

    .line 446
    :pswitch_16
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Rx Test : Low Ch"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v4, v3, v1}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 451
    :pswitch_17
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Rx Test : Mid Ch"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v4, v5, v1}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 456
    :pswitch_18
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "Rx Test : Max Ch"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v4, v6, v1}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto/16 :goto_0

    .line 326
    :pswitch_data_0
    .packed-switch 0x7f050005
        :pswitch_2
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_11
        :pswitch_0
        :pswitch_15
    .end packed-switch

    .line 328
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 353
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 393
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_1
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 417
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_1
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch

    .line 441
    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_1
        :pswitch_16
        :pswitch_17
        :pswitch_18
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 473
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "onNothingSelected"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 474
    return-void
.end method
