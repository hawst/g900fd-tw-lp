.class public Lcom/sec/android/app/bluetoothtest/BluetoothTestUtil;
.super Ljava/lang/Object;
.source "BluetoothTestUtil.java"


# direct methods
.method private static checkHiddenMenuEnable()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 84
    const-string v0, "/efs/carrier/HiddenMenu"

    .line 85
    .local v0, "HIDDENMENU_ENABLE_PATH":Ljava/lang/String;
    const-string v1, "ON"

    .line 87
    .local v1, "HIDDEN_MENU_ON":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 89
    .local v5, "f":Ljava/io/File;
    const-string v6, "eng"

    sget-object v7, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v2

    .line 92
    :cond_1
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v6

    if-eq v6, v2, :cond_0

    .line 95
    const/4 v2, 0x0

    .line 96
    .local v2, "HiddenMenuEnable":Z
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 98
    :try_start_0
    invoke-static {v0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestUtil;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 99
    .local v3, "Result":Ljava/lang/String;
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_2

    .line 100
    const/4 v2, 0x1

    goto :goto_0

    .line 102
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 103
    .end local v3    # "Result":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 105
    .local v4, "e":Ljava/lang/Exception;
    const-string v6, "BluetoothTestUtil"

    const-string v7, "Exception in reading file"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static isJigOn()Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 112
    const-string v1, "/sys/class/sec/switch/adc"

    .line 113
    .local v1, "JIG_SYS_PATH":Ljava/lang/String;
    const/16 v0, 0x1c

    .line 114
    .local v0, "JIG_ON":B
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 115
    .local v4, "mJigOn":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 116
    invoke-static {v1}, Lcom/sec/android/app/bluetoothtest/BluetoothTestUtil;->read(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 119
    .local v3, "jigvalue":Ljava/lang/String;
    const/16 v6, 0x10

    :try_start_0
    invoke-static {v3, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v0, :cond_0

    .line 120
    const-string v6, "BluetoothTestUtil"

    const-string v7, "JIG ON"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const/4 v5, 0x1

    .line 132
    .end local v3    # "jigvalue":Ljava/lang/String;
    :goto_0
    return v5

    .line 123
    .restart local v3    # "jigvalue":Ljava/lang/String;
    :cond_0
    const-string v6, "BluetoothTestUtil"

    const-string v7, "Wrong value"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 126
    :catch_0
    move-exception v2

    .line 127
    .local v2, "e":Ljava/lang/Exception;
    const-string v6, "BluetoothTestUtil"

    const-string v7, "value has unknown"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 131
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "jigvalue":Ljava/lang/String;
    :cond_1
    const-string v6, "BluetoothTestUtil"

    const-string v7, "File Does not Exist!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isWorkMode()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 21
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothTestUtil;->checkHiddenMenuEnable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 25
    const-string v1, "BluetoothTestUtil"

    const-string v2, "HIDDEN_MENU_OFF, block activity"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothTestUtil;->isJigOn()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 27
    const-string v1, "BluetoothTestUtil"

    const-string v2, "But JIG_ON, so keep going"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    :cond_0
    :goto_0
    return v0

    .line 30
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static read(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 38
    const-string v6, "BluetoothTestUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "read file path = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    const-string v5, ""

    .line 41
    .local v5, "value":Ljava/lang/String;
    const/4 v3, 0x0

    .line 42
    .local v3, "reader":Ljava/io/BufferedReader;
    const/4 v1, 0x0

    .line 44
    .local v1, "freader":Ljava/io/FileReader;
    :try_start_0
    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, p0}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    .end local v1    # "freader":Ljava/io/FileReader;
    .local v2, "freader":Ljava/io/FileReader;
    if-eqz v2, :cond_0

    .line 46
    :try_start_1
    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 47
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .local v4, "reader":Ljava/io/BufferedReader;
    if-eqz v4, :cond_7

    .line 48
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 49
    if-eqz v5, :cond_7

    .line 50
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v5

    move-object v3, v4

    .line 59
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    .line 61
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 68
    :cond_1
    :goto_1
    if-eqz v2, :cond_6

    .line 70
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    move-object v1, v2

    .line 78
    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    :cond_2
    :goto_2
    const-string v6, "BluetoothTestUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "value  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    return-object v5

    .line 63
    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 65
    const-string v6, "BluetoothTestUtil"

    const-string v7, "Exception in closing buffer"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 72
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 73
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 74
    const-string v6, "BluetoothTestUtil"

    const-string v7, "Exception in closing file"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 75
    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_2

    .line 54
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 55
    .local v0, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 56
    const-string v6, "BluetoothTestUtil"

    const-string v7, "Exception in reading file"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 59
    if-eqz v3, :cond_3

    .line 61
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 68
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_4
    if-eqz v1, :cond_2

    .line 70
    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_2

    .line 72
    :catch_3
    move-exception v0

    .line 73
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 74
    const-string v6, "BluetoothTestUtil"

    const-string v7, "Exception in closing file"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 63
    .local v0, "e":Ljava/lang/Exception;
    :catch_4
    move-exception v0

    .line 64
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 65
    const-string v6, "BluetoothTestUtil"

    const-string v7, "Exception in closing buffer"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 59
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_5
    if-eqz v3, :cond_4

    .line 61
    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 68
    :cond_4
    :goto_6
    if-eqz v1, :cond_5

    .line 70
    :try_start_9
    invoke-virtual {v1}, Ljava/io/FileReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 75
    :cond_5
    :goto_7
    throw v6

    .line 63
    :catch_5
    move-exception v0

    .line 64
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 65
    const-string v7, "BluetoothTestUtil"

    const-string v8, "Exception in closing buffer"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 72
    .end local v0    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v0

    .line 73
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 74
    const-string v7, "BluetoothTestUtil"

    const-string v8, "Exception in closing file"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 59
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :catchall_1
    move-exception v6

    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_5

    .end local v1    # "freader":Ljava/io/FileReader;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catchall_2
    move-exception v6

    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_5

    .line 54
    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :catch_7
    move-exception v0

    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto :goto_3

    .end local v1    # "freader":Ljava/io/FileReader;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_8
    move-exception v0

    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .end local v1    # "freader":Ljava/io/FileReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    :cond_6
    move-object v1, v2

    .end local v2    # "freader":Ljava/io/FileReader;
    .restart local v1    # "freader":Ljava/io/FileReader;
    goto/16 :goto_2

    .end local v1    # "freader":Ljava/io/FileReader;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "freader":Ljava/io/FileReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :cond_7
    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto/16 :goto_0
.end method
