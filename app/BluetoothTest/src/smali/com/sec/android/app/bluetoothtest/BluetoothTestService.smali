.class public Lcom/sec/android/app/bluetoothtest/BluetoothTestService;
.super Landroid/app/Service;
.source "BluetoothTestService.java"


# static fields
.field private static mContext:Landroid/content/Context;

.field private static mIntentFilter:Landroid/content/IntentFilter;


# instance fields
.field private final BT_DISCOVERY_TIMEOUT:I

.field private final BT_LE_DISCOVERY_TIMEOUT:I

.field private final BT_LE_DISCOVERY_TIMEOUT_ON_10S:I

.field private bOn10s:Z

.field private mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBtReceiver:Landroid/content/BroadcastReceiver;

.field private mDiscoverableTime:I

.field private mHandler:Landroid/os/Handler;

.field private mIfFound:Z

.field private mIsBleSearchCount:I

.field private mIsBtReceiverRegistered:Z

.field private mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

.field private mTimeout:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x2710

    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 25
    iput v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mTimeout:I

    .line 26
    iput-boolean v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bOn10s:Z

    .line 27
    iput v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mDiscoverableTime:I

    .line 28
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 81
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIsBleSearchCount:I

    .line 83
    iput v2, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->BT_DISCOVERY_TIMEOUT:I

    .line 84
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->BT_LE_DISCOVERY_TIMEOUT:I

    .line 86
    iput v2, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->BT_LE_DISCOVERY_TIMEOUT_ON_10S:I

    .line 91
    iput-boolean v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIsBtReceiverRegistered:Z

    .line 92
    iput-boolean v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIfFound:Z

    .line 94
    new-instance v0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$1;-><init>(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)V

    iput-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtReceiver:Landroid/content/BroadcastReceiver;

    .line 163
    new-instance v0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$2;-><init>(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)V

    iput-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mHandler:Landroid/os/Handler;

    .line 182
    new-instance v0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService$3;-><init>(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)V

    iput-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIfFound:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothTestService;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIfFound:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothDiscoveryStop()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothTestService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothResponse(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mDiscoverableTime:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIsBleSearchCount:I

    return v0
.end method

.method static synthetic access$508(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIsBleSearchCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIsBleSearchCount:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mTimeout:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothTestService;
    .param p1, "x1"    # I

    .prologue
    .line 20
    iput p1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mTimeout:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/bluetoothtest/BluetoothTestService;)Landroid/bluetooth/BluetoothAdapter$LeScanCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothTestService;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    return-object v0
.end method

.method private bluetoothAudioTestStart()V
    .locals 0

    .prologue
    .line 384
    return-void
.end method

.method private bluetoothAudioTestStop()V
    .locals 0

    .prologue
    .line 391
    return-void
.end method

.method private bluetoothBleDiscoveryStart(Z)V
    .locals 5
    .param p1, "ack"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 342
    const-string v0, "BluetoothTestService"

    const-string v1, "bluetoothBleDiscoveryStart"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    .line 345
    iput v2, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIsBleSearchCount:I

    .line 346
    iget-boolean v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bOn10s:Z

    if-eqz v0, :cond_1

    .line 347
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mTimeout:I

    .line 348
    iput-boolean v4, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bOn10s:Z

    .line 349
    const/4 p1, 0x1

    .line 354
    :goto_0
    if-ne p1, v2, :cond_0

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mTimeout:I

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 357
    :cond_0
    return-void

    .line 351
    :cond_1
    const/16 v0, 0x4e20

    iput v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mTimeout:I

    goto :goto_0
.end method

.method private bluetoothBleDiscoveryStartOn10s(Z)V
    .locals 4
    .param p1, "ack"    # Z

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z

    .line 362
    const/16 v0, 0x2710

    iput v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mTimeout:I

    .line 364
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mTimeout:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 367
    :cond_0
    return-void
.end method

.method private bluetoothDiscoveryStart(Z)V
    .locals 4
    .param p1, "ack"    # Z

    .prologue
    const/4 v1, 0x0

    .line 333
    iput-boolean v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIfFound:Z

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    .line 336
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 339
    :cond_0
    return-void
.end method

.method private bluetoothDiscoveryStop()V
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 372
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 373
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIsBleSearchCount:I

    .line 374
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mLeScanCallback:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V

    .line 375
    return-void
.end method

.method private bluetoothLeDirectTestEnd()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    const-string v0, "BluetoothTestService"

    const-string v1, "bluetoothLeDirectTestEnd()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2, v2}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    .line 525
    :goto_0
    return-void

    .line 523
    :cond_0
    const-string v0, "BluetoothTestService"

    const-string v1, "BtAdapter is null or not enabled."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private bluetoothLeRxTest(I)V
    .locals 4
    .param p1, "ch"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 493
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 494
    const-string v0, "BluetoothTestService"

    const-string v1, "bluetoothLeRxTest()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    packed-switch p1, :pswitch_data_0

    .line 515
    :goto_0
    return-void

    .line 499
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 503
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/16 v1, 0x13

    invoke-virtual {v0, v3, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 507
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/16 v1, 0x27

    invoke-virtual {v0, v3, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 513
    :cond_0
    const-string v0, "BluetoothTestService"

    const-string v1, "BtAdapter is null or not enabled."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 496
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private bluetoothLeTxTest(II)V
    .locals 7
    .param p1, "payload_type"    # I
    .param p2, "ch"    # I

    .prologue
    const/16 v6, 0x27

    const/16 v5, 0x13

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    const-string v0, "BluetoothTestService"

    const-string v1, "bluetoothLeTxTest()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    packed-switch p2, :pswitch_data_0

    .line 490
    :goto_0
    return-void

    .line 418
    :pswitch_0
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    .line 421
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2, v3, v3}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 425
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2, v3, v4}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 429
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2, v3, v2}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 438
    :pswitch_4
    packed-switch p1, :pswitch_data_2

    goto :goto_0

    .line 441
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2, v5, v3}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 445
    :pswitch_6
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2, v5, v4}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 449
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2, v5, v2}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 458
    :pswitch_8
    packed-switch p1, :pswitch_data_3

    goto :goto_0

    .line 461
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2, v6, v3}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 465
    :pswitch_a
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2, v6, v4}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 469
    :pswitch_b
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2, v6, v2}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 478
    :pswitch_c
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 482
    :pswitch_d
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/16 v1, 0x25

    invoke-virtual {v0, v2, v1, v3}, Landroid/bluetooth/BluetoothAdapter;->leTestMode(III)Z

    goto :goto_0

    .line 488
    :cond_0
    const-string v0, "BluetoothTestService"

    const-string v1, "BtAdapter is null or not enabled."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 415
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_8
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 418
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 438
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 458
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private bluetoothOff()V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    .line 330
    :goto_0
    return-void

    .line 328
    :cond_0
    const-string v0, "OFF"

    invoke-direct {p0, v0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothResponse(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private bluetoothOn()V
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    .line 321
    :goto_0
    return-void

    .line 319
    :cond_0
    const-string v0, "ON"

    invoke-direct {p0, v0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothResponse(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private bluetoothResponse(Ljava/lang/String;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 394
    const-string v1, "BluetoothTestService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bluetoothResponse :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.factory.intent.ACTION_BT_SERVICE_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 396
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "result"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 397
    invoke-virtual {p0, v0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->sendBroadcast(Landroid/content/Intent;)V

    .line 398
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->stopBtReceiver()V

    .line 399
    return-void
.end method

.method private bluetoothSetDiscoverable()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    const-string v0, "BluetoothTestService"

    const-string v1, "setDiscoverableTimeout(0)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothAdapter;->setDiscoverableTimeout(I)V

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/16 v1, 0x17

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->setScanMode(II)Z

    .line 409
    :goto_0
    return-void

    .line 407
    :cond_0
    const-string v0, "BluetoothTestService"

    const-string v1, "BtAdapter is null or not enabled."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private startBtReceiver()V
    .locals 2

    .prologue
    .line 528
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 529
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 530
    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 531
    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 532
    const-string v1, "android.bluetooth.adapter.action.ACTION_LE_TESE_END_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 533
    iget-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 534
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIsBtReceiverRegistered:Z

    .line 535
    return-void
.end method

.method private stopBtReceiver()V
    .locals 1

    .prologue
    .line 538
    iget-boolean v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIsBtReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mBtReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 540
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIsBtReceiverRegistered:Z

    .line 542
    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 193
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 199
    const-string v0, "BluetoothTestService"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIntentFilter:Landroid/content/IntentFilter;

    .line 202
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 203
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 204
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 205
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 207
    invoke-virtual {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mContext:Landroid/content/Context;

    .line 208
    const-string v0, "BluetoothTestService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "app context is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 211
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 215
    const-string v0, ""

    .line 217
    .local v0, "action":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 218
    const-string v2, "CMDID"

    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 224
    .local v1, "mCmdId":I
    const-string v2, "BluetoothTestService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mComID="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    if-nez v1, :cond_1

    .line 228
    iput v5, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mDiscoverableTime:I

    .line 229
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->startBtReceiver()V

    .line 230
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothOn()V

    .line 312
    .end local v1    # "mCmdId":I
    :goto_0
    return v6

    .line 221
    :cond_0
    const-string v2, "BluetoothTestService"

    const-string v3, "intent is null. so return"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 231
    .restart local v1    # "mCmdId":I
    :cond_1
    if-ne v1, v7, :cond_2

    .line 232
    const/16 v2, 0x78

    iput v2, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->mDiscoverableTime:I

    .line 233
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->startBtReceiver()V

    .line 234
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothOn()V

    goto :goto_0

    .line 235
    :cond_2
    if-ne v1, v6, :cond_3

    .line 236
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->startBtReceiver()V

    .line 237
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothOff()V

    goto :goto_0

    .line 238
    :cond_3
    if-ne v1, v8, :cond_4

    .line 239
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->startBtReceiver()V

    .line 240
    invoke-direct {p0, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothDiscoveryStart(Z)V

    goto :goto_0

    .line 241
    :cond_4
    if-ne v1, v9, :cond_5

    .line 242
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->startBtReceiver()V

    .line 243
    invoke-direct {p0, v7}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothDiscoveryStart(Z)V

    goto :goto_0

    .line 244
    :cond_5
    const/4 v2, 0x5

    if-ne v1, v2, :cond_6

    .line 245
    const-string v2, "BluetoothTestService"

    const-string v3, "BT LE SEARCH START"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->startBtReceiver()V

    .line 247
    invoke-direct {p0, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothBleDiscoveryStart(Z)V

    goto :goto_0

    .line 250
    :cond_6
    const/4 v2, 0x6

    if-ne v1, v2, :cond_7

    .line 251
    iput-boolean v7, p0, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bOn10s:Z

    .line 252
    invoke-direct {p0, v7}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothBleDiscoveryStartOn10s(Z)V

    .line 253
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->startBtReceiver()V

    goto :goto_0

    .line 256
    :cond_7
    const/4 v2, 0x7

    if-ne v1, v2, :cond_8

    .line 257
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->startBtReceiver()V

    .line 258
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothDiscoveryStop()V

    goto :goto_0

    .line 260
    :cond_8
    const/16 v2, 0x8

    if-ne v1, v2, :cond_9

    .line 261
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->startBtReceiver()V

    .line 262
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothAudioTestStart()V

    goto :goto_0

    .line 263
    :cond_9
    const/16 v2, 0x9

    if-ne v1, v2, :cond_a

    .line 264
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->startBtReceiver()V

    .line 265
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothAudioTestStop()V

    goto :goto_0

    .line 266
    :cond_a
    const/16 v2, 0xa

    if-ne v1, v2, :cond_b

    .line 267
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothSetDiscoverable()V

    goto :goto_0

    .line 270
    :cond_b
    const/16 v2, 0xb

    if-ne v1, v2, :cond_c

    .line 271
    invoke-direct {p0, v5, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto :goto_0

    .line 272
    :cond_c
    const/16 v2, 0xc

    if-ne v1, v2, :cond_d

    .line 273
    invoke-direct {p0, v5, v7}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto/16 :goto_0

    .line 274
    :cond_d
    const/16 v2, 0xd

    if-ne v1, v2, :cond_e

    .line 275
    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto/16 :goto_0

    .line 276
    :cond_e
    const/16 v2, 0xe

    if-ne v1, v2, :cond_f

    .line 277
    invoke-direct {p0, v7, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto/16 :goto_0

    .line 278
    :cond_f
    const/16 v2, 0xf

    if-ne v1, v2, :cond_10

    .line 279
    invoke-direct {p0, v7, v7}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto/16 :goto_0

    .line 280
    :cond_10
    const/16 v2, 0x10

    if-ne v1, v2, :cond_11

    .line 281
    invoke-direct {p0, v7, v6}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto/16 :goto_0

    .line 282
    :cond_11
    const/16 v2, 0x11

    if-ne v1, v2, :cond_12

    .line 283
    invoke-direct {p0, v6, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto/16 :goto_0

    .line 284
    :cond_12
    const/16 v2, 0x12

    if-ne v1, v2, :cond_13

    .line 285
    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto/16 :goto_0

    .line 286
    :cond_13
    const/16 v2, 0x13

    if-ne v1, v2, :cond_14

    .line 287
    invoke-direct {p0, v6, v6}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto/16 :goto_0

    .line 288
    :cond_14
    const/16 v2, 0x14

    if-ne v1, v2, :cond_15

    .line 289
    invoke-direct {p0, v6, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto/16 :goto_0

    .line 290
    :cond_15
    const/16 v2, 0x15

    if-ne v1, v2, :cond_16

    .line 291
    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto/16 :goto_0

    .line 292
    :cond_16
    const/16 v2, 0x16

    if-ne v1, v2, :cond_17

    .line 293
    invoke-direct {p0, v6, v6}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto/16 :goto_0

    .line 294
    :cond_17
    const/16 v2, 0x17

    if-ne v1, v2, :cond_18

    .line 295
    invoke-direct {p0, v5, v8}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto/16 :goto_0

    .line 296
    :cond_18
    const/16 v2, 0x18

    if-ne v1, v2, :cond_19

    .line 297
    invoke-direct {p0, v5, v7}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto/16 :goto_0

    .line 298
    :cond_19
    const/16 v2, 0x19

    if-ne v1, v2, :cond_1a

    .line 299
    invoke-direct {p0, v5, v9}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeTxTest(II)V

    goto/16 :goto_0

    .line 300
    :cond_1a
    const/16 v2, 0x1a

    if-ne v1, v2, :cond_1b

    .line 301
    invoke-direct {p0, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeRxTest(I)V

    goto/16 :goto_0

    .line 302
    :cond_1b
    const/16 v2, 0x1b

    if-ne v1, v2, :cond_1c

    .line 303
    invoke-direct {p0, v7}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeRxTest(I)V

    goto/16 :goto_0

    .line 304
    :cond_1c
    const/16 v2, 0x1c

    if-ne v1, v2, :cond_1d

    .line 305
    invoke-direct {p0, v6}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeRxTest(I)V

    goto/16 :goto_0

    .line 306
    :cond_1d
    const/16 v2, 0x1d

    if-ne v1, v2, :cond_1e

    .line 307
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->startBtReceiver()V

    .line 308
    invoke-direct {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothTestService;->bluetoothLeDirectTestEnd()V

    goto/16 :goto_0

    .line 310
    :cond_1e
    const-string v2, "BluetoothTestService"

    const-string v3, "unknown cmdId, so skip."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
