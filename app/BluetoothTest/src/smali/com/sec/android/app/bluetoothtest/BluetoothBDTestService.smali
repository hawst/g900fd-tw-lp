.class public Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;
.super Landroid/app/Service;
.source "BluetoothBDTestService.java"


# instance fields
.field private bd_address_path:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->bd_address_path:Ljava/lang/String;

    return-void
.end method

.method public static checkBluetoothAddress(Ljava/lang/String;)Z
    .locals 5
    .param p0, "address"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x11

    const/4 v2, 0x0

    .line 325
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-eq v3, v4, :cond_1

    .line 345
    :cond_0
    :goto_0
    return v2

    .line 328
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v4, :cond_4

    .line 329
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 330
    .local v0, "c":C
    rem-int/lit8 v3, v1, 0x3

    packed-switch v3, :pswitch_data_0

    .line 328
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 333
    :pswitch_0
    const/16 v3, 0x30

    if-lt v0, v3, :cond_3

    const/16 v3, 0x39

    if-le v0, v3, :cond_2

    :cond_3
    const/16 v3, 0x41

    if-lt v0, v3, :cond_0

    const/16 v3, 0x46

    if-gt v0, v3, :cond_0

    goto :goto_2

    .line 339
    :pswitch_1
    const/16 v3, 0x3a

    if-ne v0, v3, :cond_0

    goto :goto_2

    .line 345
    .end local v0    # "c":C
    :cond_4
    const/4 v2, 0x1

    goto :goto_0

    .line 330
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updatePreviousOSBTAddress(Ljava/io/File;Ljava/lang/String;)V
    .locals 7
    .param p1, "file"    # Ljava/io/File;
    .param p2, "address"    # Ljava/lang/String;

    .prologue
    .line 363
    const/4 v2, 0x0

    .line 364
    .local v2, "writer":Ljava/io/BufferedWriter;
    const/4 v1, 0x0

    .line 366
    .local v1, "newAddress":Ljava/lang/String;
    if-nez p2, :cond_1

    .line 395
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    const-string v4, ":"

    invoke-virtual {p0, p2, v4}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->delChar(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 373
    :try_start_0
    const-string v4, "BluetoothBDTestService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updatePreviousOSBTAddress() is writable ==> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/io/File;->canWrite()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 376
    invoke-virtual {p1}, Ljava/io/File;->createNewFile()Z

    .line 378
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/FileWriter;

    invoke-direct {v4, p1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 379
    .end local v2    # "writer":Ljava/io/BufferedWriter;
    .local v3, "writer":Ljava/io/BufferedWriter;
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bt_macaddr:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 385
    if-eqz v3, :cond_3

    .line 387
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 391
    const/4 v2, 0x0

    .line 392
    .end local v3    # "writer":Ljava/io/BufferedWriter;
    .restart local v2    # "writer":Ljava/io/BufferedWriter;
    goto :goto_0

    .line 388
    .end local v2    # "writer":Ljava/io/BufferedWriter;
    .restart local v3    # "writer":Ljava/io/BufferedWriter;
    :catch_0
    move-exception v0

    .line 389
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 391
    const/4 v2, 0x0

    .line 392
    .end local v3    # "writer":Ljava/io/BufferedWriter;
    .restart local v2    # "writer":Ljava/io/BufferedWriter;
    goto :goto_0

    .line 391
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "writer":Ljava/io/BufferedWriter;
    .restart local v3    # "writer":Ljava/io/BufferedWriter;
    :catchall_0
    move-exception v4

    const/4 v2, 0x0

    .end local v3    # "writer":Ljava/io/BufferedWriter;
    .restart local v2    # "writer":Ljava/io/BufferedWriter;
    throw v4

    .line 380
    :catch_1
    move-exception v0

    .line 381
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_4
    const-string v4, "BluetoothBDTestService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updatePreviousOSBTAddress() file ==> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " -:- newAddress ==> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 385
    if-eqz v2, :cond_0

    .line 387
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 391
    const/4 v2, 0x0

    .line 392
    goto/16 :goto_0

    .line 388
    :catch_2
    move-exception v0

    .line 389
    :try_start_6
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 391
    const/4 v2, 0x0

    .line 392
    goto/16 :goto_0

    .line 391
    :catchall_1
    move-exception v4

    const/4 v2, 0x0

    throw v4

    .line 385
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_2
    move-exception v4

    :goto_2
    if-eqz v2, :cond_2

    .line 387
    :try_start_7
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 391
    const/4 v2, 0x0

    :cond_2
    :goto_3
    throw v4

    .line 388
    :catch_3
    move-exception v0

    .line 389
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_8
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 391
    const/4 v2, 0x0

    .line 392
    goto :goto_3

    .line 391
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_3
    move-exception v4

    const/4 v2, 0x0

    throw v4

    .line 385
    .end local v2    # "writer":Ljava/io/BufferedWriter;
    .restart local v3    # "writer":Ljava/io/BufferedWriter;
    :catchall_4
    move-exception v4

    move-object v2, v3

    .end local v3    # "writer":Ljava/io/BufferedWriter;
    .restart local v2    # "writer":Ljava/io/BufferedWriter;
    goto :goto_2

    .line 380
    .end local v2    # "writer":Ljava/io/BufferedWriter;
    .restart local v3    # "writer":Ljava/io/BufferedWriter;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "writer":Ljava/io/BufferedWriter;
    .restart local v2    # "writer":Ljava/io/BufferedWriter;
    goto :goto_1

    .end local v2    # "writer":Ljava/io/BufferedWriter;
    .restart local v3    # "writer":Ljava/io/BufferedWriter;
    :cond_3
    move-object v2, v3

    .end local v3    # "writer":Ljava/io/BufferedWriter;
    .restart local v2    # "writer":Ljava/io/BufferedWriter;
    goto/16 :goto_0
.end method


# virtual methods
.method addressFormatter(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 2
    .param p1, "address"    # Ljava/lang/StringBuffer;

    .prologue
    .line 284
    const/4 v0, 0x2

    .local v0, "k":I
    :goto_0
    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 285
    const-string v1, ":"

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    .line 284
    add-int/lit8 v0, v0, 0x3

    goto :goto_0

    .line 287
    :cond_0
    return-object p1
.end method

.method createBDAddrFileUpdateAddr(Ljava/lang/String;ZZ)Z
    .locals 13
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "mWriteSuccess"    # Z
    .param p3, "mBTWrite"    # Z

    .prologue
    .line 161
    invoke-static {p1}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 162
    const/4 v10, 0x0

    .line 241
    :goto_0
    return v10

    .line 164
    :cond_0
    const-string v10, "BluetoothBDTestService"

    const-string v11, "createBDAddrFileUpdateAddr"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    const/4 v6, 0x0

    .line 170
    .local v6, "output":Ljava/io/BufferedWriter;
    const/4 v4, 0x0

    .line 173
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v10, "/efs/bluetooth"

    invoke-direct {v1, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 175
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_1

    .line 176
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v9

    .line 178
    .local v9, "status":Z
    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v1, v10, v11}, Ljava/io/File;->setReadable(ZZ)Z

    .line 179
    const/4 v10, 0x1

    const/4 v11, 0x1

    invoke-virtual {v1, v10, v11}, Ljava/io/File;->setWritable(ZZ)Z

    .line 180
    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v1, v10, v11}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 181
    const-string v10, "BluetoothBDTestService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "directory creation status :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    .end local v9    # "status":Z
    :cond_1
    new-instance v2, Ljava/io/File;

    iget-object v10, p0, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->bd_address_path:Ljava/lang/String;

    invoke-direct {v2, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 186
    .local v2, "f1":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_2

    .line 187
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    move-result v9

    .line 189
    .restart local v9    # "status":Z
    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v2, v10, v11}, Ljava/io/File;->setReadable(ZZ)Z

    .line 190
    const-string v10, "BluetoothBDTestService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "file creation status :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    .end local v9    # "status":Z
    :cond_2
    new-instance v3, Ljava/io/File;

    const-string v10, "/efs/imei/bt.txt"

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 195
    .local v3, "f2":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 196
    if-eqz p3, :cond_9

    .line 198
    const-string v10, "BluetoothBDTestService"

    const-string v11, "******** check point 1 **************"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    invoke-direct {p0, v3, p1}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->updatePreviousOSBTAddress(Ljava/io/File;Ljava/lang/String;)V

    .line 209
    :cond_3
    :goto_1
    const-string v10, "BluetoothBDTestService"

    const-string v11, "*** get & remove previous address file ***"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :cond_4
    new-instance v5, Ljava/io/FileOutputStream;

    iget-object v10, p0, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->bd_address_path:Ljava/lang/String;

    invoke-direct {v5, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .local v5, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v7, Ljava/io/BufferedWriter;

    new-instance v10, Ljava/io/FileWriter;

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/FileWriter;-><init>(Ljava/io/FileDescriptor;)V

    invoke-direct {v7, v10}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 216
    .end local v6    # "output":Ljava/io/BufferedWriter;
    .local v7, "output":Ljava/io/BufferedWriter;
    const/4 p2, 0x0

    .line 217
    if-eqz p1, :cond_5

    .line 218
    :try_start_2
    invoke-virtual {v7, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 219
    invoke-virtual {v7}, Ljava/io/BufferedWriter;->flush()V

    .line 220
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/FileDescriptor;->sync()V

    .line 221
    const-string v10, "persist.service.bt.bt_macaddr"

    invoke-static {v10, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const/4 p2, 0x1

    .line 226
    :cond_5
    const-string v10, "BluetoothBDTestService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "createBDAddrFileUpdateAddr: mWriteSuccess is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 233
    if-eqz v7, :cond_6

    .line 234
    :try_start_3
    invoke-virtual {v7}, Ljava/io/BufferedWriter;->close()V

    .line 235
    :cond_6
    if-eqz v5, :cond_7

    .line 236
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_7
    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v1    # "f":Ljava/io/File;
    .end local v2    # "f1":Ljava/io/File;
    .end local v3    # "f2":Ljava/io/File;
    .end local v7    # "output":Ljava/io/BufferedWriter;
    .restart local v6    # "output":Ljava/io/BufferedWriter;
    :cond_8
    :goto_2
    move v10, p2

    .line 241
    goto/16 :goto_0

    .line 203
    .restart local v1    # "f":Ljava/io/File;
    .restart local v2    # "f1":Ljava/io/File;
    .restart local v3    # "f2":Ljava/io/File;
    :cond_9
    :try_start_4
    const-string v10, "BluetoothBDTestService"

    const-string v11, "******** check point 2 **************"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->getPrevAddress()Ljava/lang/String;

    move-result-object v8

    .line 205
    .local v8, "prevAddress":Ljava/lang/String;
    if-eqz v8, :cond_3

    invoke-static {v8}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->checkBluetoothAddress(Ljava/lang/String;)Z
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v10

    if-eqz v10, :cond_3

    .line 206
    move-object p1, v8

    goto :goto_1

    .line 237
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .end local v6    # "output":Ljava/io/BufferedWriter;
    .end local v8    # "prevAddress":Ljava/lang/String;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "output":Ljava/io/BufferedWriter;
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .line 240
    .end local v7    # "output":Ljava/io/BufferedWriter;
    .restart local v6    # "output":Ljava/io/BufferedWriter;
    goto :goto_2

    .line 227
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "f":Ljava/io/File;
    .end local v2    # "f1":Ljava/io/File;
    .end local v3    # "f2":Ljava/io/File;
    :catch_1
    move-exception v0

    .line 228
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_5
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 233
    if-eqz v6, :cond_a

    .line 234
    :try_start_6
    invoke-virtual {v6}, Ljava/io/BufferedWriter;->close()V

    .line 235
    :cond_a
    if-eqz v4, :cond_8

    .line 236
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_2

    .line 237
    :catch_2
    move-exception v0

    .line 238
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 229
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 230
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 233
    if-eqz v6, :cond_b

    .line 234
    :try_start_8
    invoke-virtual {v6}, Ljava/io/BufferedWriter;->close()V

    .line 235
    :cond_b
    if-eqz v4, :cond_8

    .line 236
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_2

    .line 237
    :catch_4
    move-exception v0

    .line 238
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 232
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    .line 233
    :goto_5
    if-eqz v6, :cond_c

    .line 234
    :try_start_9
    invoke-virtual {v6}, Ljava/io/BufferedWriter;->close()V

    .line 235
    :cond_c
    if-eqz v4, :cond_d

    .line 236
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 239
    :cond_d
    :goto_6
    throw v10

    .line 237
    :catch_5
    move-exception v0

    .line 238
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 232
    .end local v0    # "e":Ljava/io/IOException;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "f":Ljava/io/File;
    .restart local v2    # "f1":Ljava/io/File;
    .restart local v3    # "f2":Ljava/io/File;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v10

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .end local v6    # "output":Ljava/io/BufferedWriter;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "output":Ljava/io/BufferedWriter;
    :catchall_2
    move-exception v10

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "output":Ljava/io/BufferedWriter;
    .restart local v6    # "output":Ljava/io/BufferedWriter;
    goto :goto_5

    .line 229
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v0

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .end local v6    # "output":Ljava/io/BufferedWriter;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "output":Ljava/io/BufferedWriter;
    :catch_7
    move-exception v0

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "output":Ljava/io/BufferedWriter;
    .restart local v6    # "output":Ljava/io/BufferedWriter;
    goto :goto_4

    .line 227
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_8
    move-exception v0

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .end local v6    # "output":Ljava/io/BufferedWriter;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "output":Ljava/io/BufferedWriter;
    :catch_9
    move-exception v0

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    move-object v6, v7

    .end local v7    # "output":Ljava/io/BufferedWriter;
    .restart local v6    # "output":Ljava/io/BufferedWriter;
    goto :goto_3
.end method

.method delChar(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "src"    # Ljava/lang/String;
    .param p2, "delCh"    # Ljava/lang/String;

    .prologue
    .line 350
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 352
    .local v2, "sb":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 353
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 355
    .local v0, "ch":C
    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 356
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 352
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 359
    .end local v0    # "ch":C
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method generateRandomAdrress()Ljava/lang/String;
    .locals 9

    .prologue
    .line 247
    const-string v6, "BluetoothBDTestService"

    const-string v7, "generateRandomAdrress()"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    const/16 v6, 0xc

    new-array v1, v6, [B

    .line 249
    .local v1, "b":[B
    const/4 v2, 0x0

    .line 251
    .local v2, "i":I
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 252
    .local v5, "rand_data":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .line 254
    .local v0, "address":Ljava/lang/StringBuffer;
    new-instance v4, Ljava/util/Random;

    invoke-direct {v4}, Ljava/util/Random;-><init>()V

    .line 255
    .local v4, "ran":Ljava/util/Random;
    invoke-virtual {v4, v1}, Ljava/util/Random;->nextBytes([B)V

    .line 257
    const/4 v2, 0x0

    :goto_0
    const/4 v6, 0x6

    if-ge v2, v6, :cond_2

    .line 260
    aget-byte v6, v1, v2

    if-ltz v6, :cond_0

    aget-byte v6, v1, v2

    const/16 v7, 0xf

    if-gt v6, v7, :cond_0

    .line 261
    aget-byte v6, v1, v2

    add-int/lit8 v6, v6, 0x10

    int-to-byte v6, v6

    aput-byte v6, v1, v2

    .line 263
    :cond_0
    aget-byte v6, v1, v2

    and-int/lit16 v6, v6, 0xff

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 267
    .local v3, "j":Ljava/lang/Integer;
    if-nez v2, :cond_1

    .line 269
    const/16 v6, 0x22

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 270
    const-string v6, "BluetoothBDTestService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "The first byte must have a even value, first byte value: 0x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    :cond_1
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "next value :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 257
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 277
    .end local v3    # "j":Ljava/lang/Integer;
    :cond_2
    invoke-virtual {p0, v5}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->addressFormatter(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 278
    const-string v6, "BluetoothBDTestService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Address is :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method getPrevAddress()Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 291
    const-string v8, "BluetoothBDTestService"

    const-string v9, "*** get previous address, for os upgrade **"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    const/4 v6, 0x0

    .line 293
    .local v6, "prevAddress":Ljava/lang/String;
    const/4 v0, 0x0

    .line 294
    .local v0, "address":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .line 298
    .local v1, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    const-string v8, "/efs/imei/bt.txt"

    invoke-direct {v4, v8}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 299
    .local v4, "fstream":Ljava/io/FileInputStream;
    new-instance v5, Ljava/io/DataInputStream;

    invoke-direct {v5, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 300
    .local v5, "in":Ljava/io/DataInputStream;
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    invoke-direct {v8, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 301
    .end local v1    # "br":Ljava/io/BufferedReader;
    .local v2, "br":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 303
    if-nez v6, :cond_0

    .line 315
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :goto_0
    move-object v1, v2

    .line 320
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v4    # "fstream":Ljava/io/FileInputStream;
    .end local v5    # "in":Ljava/io/DataInputStream;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :goto_1
    return-object v7

    .line 316
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fstream":Ljava/io/FileInputStream;
    .restart local v5    # "in":Ljava/io/DataInputStream;
    :catch_0
    move-exception v3

    .line 317
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 307
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_0
    :try_start_3
    new-instance v8, Ljava/lang/StringBuffer;

    const/16 v9, 0xb

    const/16 v10, 0x17

    invoke-virtual {v6, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v8}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->addressFormatter(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v0

    .line 308
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v7

    .line 315
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :goto_2
    move-object v1, v2

    .line 318
    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    goto :goto_1

    .line 316
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    :catch_1
    move-exception v3

    .line 317
    .restart local v3    # "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 309
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "fstream":Ljava/io/FileInputStream;
    .end local v5    # "in":Ljava/io/DataInputStream;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_2
    move-exception v3

    .line 310
    .local v3, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 315
    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_1

    .line 316
    :catch_3
    move-exception v3

    .line 317
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 311
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v3

    .line 312
    .local v3, "e":Ljava/io/IOException;
    :goto_4
    :try_start_7
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 315
    :try_start_8
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_1

    .line 316
    :catch_5
    move-exception v3

    .line 317
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 314
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    .line 315
    :goto_5
    :try_start_9
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    .line 318
    :goto_6
    throw v7

    .line 316
    :catch_6
    move-exception v3

    .line 317
    .restart local v3    # "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 314
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "fstream":Ljava/io/FileInputStream;
    .restart local v5    # "in":Ljava/io/DataInputStream;
    :catchall_1
    move-exception v7

    move-object v1, v2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    goto :goto_5

    .line 311
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    :catch_7
    move-exception v3

    move-object v1, v2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    goto :goto_4

    .line 309
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    :catch_8
    move-exception v3

    move-object v1, v2

    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    goto :goto_3
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 41
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 47
    const-string v0, "BluetoothBDTestService"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 18
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 52
    if-nez p1, :cond_0

    .line 54
    const-string v14, "BluetoothBDTestService"

    const-string v15, "onStart - intent is null."

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->stopSelf()V

    .line 129
    :goto_0
    return-void

    .line 59
    :cond_0
    const-string v14, "com.sec.android.app.bluetoothtest.EXTRA_TYPE"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 60
    .local v7, "extra_type":Ljava/lang/String;
    const-string v14, "BluetoothBDTestService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "onStart(), extra_type: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    const-string v14, "ro.bt.bdaddr_path"

    invoke-static {v14}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->bd_address_path:Ljava/lang/String;

    .line 63
    const-string v14, "BluetoothBDTestService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "bd_address_path: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->bd_address_path:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    const-string v14, "BOOT_COMPLETED"

    invoke-virtual {v7, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 66
    new-instance v8, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->bd_address_path:Ljava/lang/String;

    invoke-direct {v8, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 67
    .local v8, "f":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->bd_address_path:Ljava/lang/String;

    if-eqz v14, :cond_1

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-lez v14, :cond_1

    .line 68
    const-string v14, "BluetoothBDTestService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "already exist!( "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->bd_address_path:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " )"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", file length: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 72
    :cond_1
    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-nez v14, :cond_2

    .line 73
    const-string v14, "BluetoothBDTestService"

    const-string v15, "file length is 0, this file will be removed and make new random address!"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    .line 76
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->generateRandomAdrress()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v14, v15, v1}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->createBDAddrFileUpdateAddr(Ljava/lang/String;ZZ)Z

    goto/16 :goto_0

    .line 78
    .end local v8    # "f":Ljava/io/File;
    :cond_3
    const-string v14, "BT_ID_WRITE"

    invoke-virtual {v7, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 79
    const/4 v13, 0x0

    .line 80
    .local v13, "mWriteSuccess":Z
    const-string v14, "MAC_DATA"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 81
    .local v6, "extra":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->bd_address_path:Ljava/lang/String;

    invoke-direct {v8, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 83
    .restart local v8    # "f":Ljava/io/File;
    const-string v14, "BluetoothBDTestService"

    const-string v15, "com.sec.android.app.bluetoothtest.BT_ID_WRITE"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const-string v14, "BluetoothBDTestService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Get extra, Write address: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 88
    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    .line 89
    const-string v14, "BluetoothBDTestService"

    const-string v15, "The previous file is deleted, and BT address will update in new file."

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_4
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v13, v14}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->createBDAddrFileUpdateAddr(Ljava/lang/String;ZZ)Z

    move-result v13

    .line 92
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->sendIntentToFactoryApp(Z)V

    goto/16 :goto_0

    .line 93
    .end local v6    # "extra":Ljava/lang/String;
    .end local v8    # "f":Ljava/io/File;
    .end local v13    # "mWriteSuccess":Z
    :cond_5
    const-string v14, "BT_ID_READ"

    invoke-virtual {v7, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 94
    const/4 v12, 0x0

    .line 95
    .local v12, "mReadSuccess":Z
    const/4 v2, 0x0

    .line 96
    .local v2, "address":Ljava/lang/String;
    const/4 v3, 0x0

    .line 97
    .local v3, "br":Ljava/io/BufferedReader;
    const/4 v8, 0x0

    .line 100
    .restart local v8    # "f":Ljava/io/File;
    :try_start_0
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->bd_address_path:Ljava/lang/String;

    invoke-direct {v9, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    .end local v8    # "f":Ljava/io/File;
    .local v9, "f":Ljava/io/File;
    :try_start_1
    invoke-virtual {v9}, Ljava/io/File;->exists()Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v14

    if-nez v14, :cond_6

    .line 102
    const/4 v12, 0x0

    .line 118
    :goto_1
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-object v8, v9

    .line 123
    .end local v9    # "f":Ljava/io/File;
    .restart local v8    # "f":Ljava/io/File;
    :goto_2
    const-string v14, "BluetoothBDTestService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "BT_ID_READ: mReadSuccess is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", address is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v12}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->sendIntentToRIL(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 104
    .end local v8    # "f":Ljava/io/File;
    .restart local v9    # "f":Ljava/io/File;
    :cond_6
    :try_start_3
    new-instance v10, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->bd_address_path:Ljava/lang/String;

    invoke-direct {v10, v14}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 105
    .local v10, "fstream":Ljava/io/FileInputStream;
    new-instance v11, Ljava/io/DataInputStream;

    invoke-direct {v11, v10}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 106
    .local v11, "in":Ljava/io/DataInputStream;
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v14, Ljava/io/InputStreamReader;

    invoke-direct {v14, v11}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v14}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 107
    .end local v3    # "br":Ljava/io/BufferedReader;
    .local v4, "br":Ljava/io/BufferedReader;
    :try_start_4
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .line 109
    if-eqz v2, :cond_8

    invoke-static {v2}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->checkBluetoothAddress(Ljava/lang/String;)Z
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result v14

    if-eqz v14, :cond_8

    .line 110
    const/4 v12, 0x1

    move-object v3, v4

    .end local v4    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    goto :goto_1

    .line 119
    .end local v10    # "fstream":Ljava/io/FileInputStream;
    .end local v11    # "in":Ljava/io/DataInputStream;
    :catch_0
    move-exception v5

    .line 120
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    move-object v8, v9

    .line 122
    .end local v9    # "f":Ljava/io/File;
    .restart local v8    # "f":Ljava/io/File;
    goto :goto_2

    .line 112
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v5

    .line 113
    .local v5, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_5
    invoke-virtual {v5}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 118
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_2

    .line 119
    :catch_2
    move-exception v5

    .line 120
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 114
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v5

    .line 115
    .local v5, "e":Ljava/io/IOException;
    :goto_4
    :try_start_7
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 118
    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_2

    .line 119
    :catch_4
    move-exception v5

    .line 120
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 117
    .end local v5    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v14

    .line 118
    :goto_5
    :try_start_9
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5

    .line 121
    :goto_6
    throw v14

    .line 119
    :catch_5
    move-exception v5

    .line 120
    .restart local v5    # "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 127
    .end local v2    # "address":Ljava/lang/String;
    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v8    # "f":Ljava/io/File;
    .end local v12    # "mReadSuccess":Z
    :cond_7
    const-string v14, "BluetoothBDTestService"

    const-string v15, " **** not expecting this intent *** "

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 117
    .restart local v2    # "address":Ljava/lang/String;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v12    # "mReadSuccess":Z
    :catchall_1
    move-exception v14

    move-object v8, v9

    .end local v9    # "f":Ljava/io/File;
    .restart local v8    # "f":Ljava/io/File;
    goto :goto_5

    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v8    # "f":Ljava/io/File;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v10    # "fstream":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/io/DataInputStream;
    :catchall_2
    move-exception v14

    move-object v8, v9

    .end local v9    # "f":Ljava/io/File;
    .restart local v8    # "f":Ljava/io/File;
    move-object v3, v4

    .end local v4    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    goto :goto_5

    .line 114
    .end local v8    # "f":Ljava/io/File;
    .end local v10    # "fstream":Ljava/io/FileInputStream;
    .end local v11    # "in":Ljava/io/DataInputStream;
    .restart local v9    # "f":Ljava/io/File;
    :catch_6
    move-exception v5

    move-object v8, v9

    .end local v9    # "f":Ljava/io/File;
    .restart local v8    # "f":Ljava/io/File;
    goto :goto_4

    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v8    # "f":Ljava/io/File;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v10    # "fstream":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/io/DataInputStream;
    :catch_7
    move-exception v5

    move-object v8, v9

    .end local v9    # "f":Ljava/io/File;
    .restart local v8    # "f":Ljava/io/File;
    move-object v3, v4

    .end local v4    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    goto :goto_4

    .line 112
    .end local v8    # "f":Ljava/io/File;
    .end local v10    # "fstream":Ljava/io/FileInputStream;
    .end local v11    # "in":Ljava/io/DataInputStream;
    .restart local v9    # "f":Ljava/io/File;
    :catch_8
    move-exception v5

    move-object v8, v9

    .end local v9    # "f":Ljava/io/File;
    .restart local v8    # "f":Ljava/io/File;
    goto :goto_3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v8    # "f":Ljava/io/File;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v10    # "fstream":Ljava/io/FileInputStream;
    .restart local v11    # "in":Ljava/io/DataInputStream;
    :catch_9
    move-exception v5

    move-object v8, v9

    .end local v9    # "f":Ljava/io/File;
    .restart local v8    # "f":Ljava/io/File;
    move-object v3, v4

    .end local v4    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    goto :goto_3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v8    # "f":Ljava/io/File;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    .restart local v9    # "f":Ljava/io/File;
    :cond_8
    move-object v3, v4

    .end local v4    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    goto/16 :goto_1
.end method

.method sendIntentToFactoryApp(Z)V
    .locals 4
    .param p1, "status"    # Z

    .prologue
    .line 133
    const-string v1, "BluetoothBDTestService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendIntentToFactoryApp(), status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.bluetoothtest.BT_ID_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 136
    .local v0, "mFactIntent":Landroid/content/Intent;
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 137
    const-string v1, "S_DATA"

    const-string v2, "OK"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->sendBroadcast(Landroid/content/Intent;)V

    .line 141
    return-void

    .line 139
    :cond_0
    const-string v1, "S_DATA"

    const-string v2, "NG"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method sendIntentToRIL(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "status"    # Z

    .prologue
    .line 145
    const-string v1, "BluetoothBDTestService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendIntentToRIL(), status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.bluetoothtest.BT_ID_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 148
    .local v0, "mRILIntent":Landroid/content/Intent;
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 149
    const-string v1, "S_DATA"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/bluetoothtest/BluetoothBDTestService;->sendBroadcast(Landroid/content/Intent;)V

    .line 153
    return-void

    .line 151
    :cond_0
    const-string v1, "S_DATA"

    const-string v2, "NG"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method
