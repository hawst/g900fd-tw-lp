.class public Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;
.super Landroid/app/Activity;
.source "BluetoothLEDutMode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;
    }
.end annotation


# static fields
.field private static isRegist:Z

.field private static le_test_on:Z

.field private static mSpnRxTest:Landroid/widget/Spinner;

.field private static mSpnTxInband:Landroid/widget/Spinner;

.field private static mSpnTxLow:Landroid/widget/Spinner;

.field private static mSpnTxMax:Landroid/widget/Spinner;

.field private static mSpnTxMid:Landroid/widget/Spinner;


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mContext:Landroid/content/Context;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mListener:Landroid/view/View$OnClickListener;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    sput-boolean v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->le_test_on:Z

    .line 42
    sput-boolean v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->isRegist:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 47
    new-instance v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$1;-><init>(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)V

    iput-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 283
    new-instance v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$2;-><init>(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)V

    iput-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mListener:Landroid/view/View$OnClickListener;

    .line 323
    return-void
.end method

.method static synthetic access$000()Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxLow:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$100()Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxMid:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$200()Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxMax:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$300()Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxInband:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$400()Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnRxTest:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0
.end method


# virtual methods
.method public DisableBluetoothLEDUTMode()V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 266
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->le_test_on:Z

    if-eqz v0, :cond_1

    .line 267
    const-string v0, "ctl.start"

    const-string v1, "LE_dut_cmd_off"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->le_test_on:Z

    .line 271
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->finish()V

    .line 272
    return-void
.end method

.method public EnableBluetoothLEDUTMode()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 219
    sget-boolean v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->le_test_on:Z

    if-nez v0, :cond_0

    .line 220
    const-string v0, "ctl.start"

    const-string v1, "LE_dut_cmd_on"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string v0, "BluetoothLEDutMode"

    const-string v1, "Bluetooth 2wire LE Test mode Enable"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->le_test_on:Z

    .line 225
    :goto_0
    return-void

    .line 224
    :cond_0
    const-string v0, "BluetoothLEDutMode"

    const-string v1, "Bluetooth 2wire LE Test mode aleady Enable"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    const-string v10, "BluetoothLEDutMode"

    const-string v11, "onCreate()"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const-string v10, "power"

    invoke-virtual {p0, v10}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/os/PowerManager;

    .line 87
    .local v9, "pm":Landroid/os/PowerManager;
    const v10, 0x3000001a

    const-string v11, "BluetoothLEDutMode"

    invoke-virtual {v9, v10, v11}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 89
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 91
    iput-object p0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;

    .line 93
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 95
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothTestUtil;->isWorkMode()Z

    move-result v10

    if-nez v10, :cond_1

    .line 96
    const-string v10, "BluetoothLEDutMode"

    const-string v11, "onCreate() : block BluetoothLEDutMode"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->finish()V

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    const v10, 0x7f020001

    invoke-virtual {p0, v10}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->setContentView(I)V

    .line 102
    const v10, 0x7f050003

    invoke-virtual {p0, v10}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 103
    .local v8, "buttonon":Landroid/view/View;
    iget-object v10, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    const v10, 0x7f050005

    invoke-virtual {p0, v10}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Spinner;

    sput-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxLow:Landroid/widget/Spinner;

    .line 114
    const/high16 v10, 0x7f030000

    const v11, 0x1090008

    invoke-static {p0, v10, v11}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v2

    .line 116
    .local v2, "adtTxLow":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v10, 0x1090009

    invoke-virtual {v2, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 117
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxLow:Landroid/widget/Spinner;

    invoke-virtual {v10, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 118
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxLow:Landroid/widget/Spinner;

    new-instance v11, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;

    const/4 v12, 0x0

    invoke-direct {v11, p0, v12}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;-><init>(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$1;)V

    invoke-virtual {v10, v11}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 119
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxLow:Landroid/widget/Spinner;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 122
    const v10, 0x7f050007

    invoke-virtual {p0, v10}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Spinner;

    sput-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxMid:Landroid/widget/Spinner;

    .line 123
    const v10, 0x7f030001

    const v11, 0x1090008

    invoke-static {p0, v10, v11}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v4

    .line 125
    .local v4, "adtTxMid":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v10, 0x1090009

    invoke-virtual {v4, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 126
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxMid:Landroid/widget/Spinner;

    invoke-virtual {v10, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 127
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxMid:Landroid/widget/Spinner;

    new-instance v11, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;

    const/4 v12, 0x0

    invoke-direct {v11, p0, v12}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;-><init>(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$1;)V

    invoke-virtual {v10, v11}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 128
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxMid:Landroid/widget/Spinner;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 131
    const v10, 0x7f050009

    invoke-virtual {p0, v10}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Spinner;

    sput-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxMax:Landroid/widget/Spinner;

    .line 132
    const/high16 v10, 0x7f030000

    const v11, 0x1090008

    invoke-static {p0, v10, v11}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v3

    .line 134
    .local v3, "adtTxMax":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v10, 0x1090009

    invoke-virtual {v3, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 135
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxMax:Landroid/widget/Spinner;

    invoke-virtual {v10, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 136
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxMax:Landroid/widget/Spinner;

    new-instance v11, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;

    const/4 v12, 0x0

    invoke-direct {v11, p0, v12}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;-><init>(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$1;)V

    invoke-virtual {v10, v11}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 137
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxMax:Landroid/widget/Spinner;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 140
    const v10, 0x7f05000b

    invoke-virtual {p0, v10}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Spinner;

    sput-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxInband:Landroid/widget/Spinner;

    .line 141
    const v10, 0x7f030002

    const v11, 0x1090008

    invoke-static {p0, v10, v11}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v1

    .line 143
    .local v1, "adtTxInband":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v10, 0x1090009

    invoke-virtual {v1, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 144
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxInband:Landroid/widget/Spinner;

    invoke-virtual {v10, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 145
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxInband:Landroid/widget/Spinner;

    new-instance v11, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;

    const/4 v12, 0x0

    invoke-direct {v11, p0, v12}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;-><init>(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$1;)V

    invoke-virtual {v10, v11}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 146
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnTxInband:Landroid/widget/Spinner;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 149
    const v10, 0x7f05000d

    invoke-virtual {p0, v10}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Spinner;

    sput-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnRxTest:Landroid/widget/Spinner;

    .line 150
    const/high16 v10, 0x7f030000

    const v11, 0x1090008

    invoke-static {p0, v10, v11}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 152
    .local v0, "adtRxTest":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v10, 0x1090009

    invoke-virtual {v0, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 153
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnRxTest:Landroid/widget/Spinner;

    invoke-virtual {v10, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 154
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnRxTest:Landroid/widget/Spinner;

    new-instance v11, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;

    const/4 v12, 0x0

    invoke-direct {v11, p0, v12}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$MySpnListener;-><init>(Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode$1;)V

    invoke-virtual {v10, v11}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 155
    sget-object v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mSpnRxTest:Landroid/widget/Spinner;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 158
    const v10, 0x7f05000e

    invoke-virtual {p0, v10}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 159
    .local v6, "btnTestEnd":Landroid/view/View;
    iget-object v10, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    const v10, 0x7f050010

    invoke-virtual {p0, v10}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 163
    .local v5, "btnEnableHwTest":Landroid/view/View;
    iget-object v10, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    const v10, 0x7f050011

    invoke-virtual {p0, v10}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 167
    .local v7, "buttonoff":Landroid/view/View;
    iget-object v10, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    new-instance v10, Landroid/content/IntentFilter;

    invoke-direct {v10}, Landroid/content/IntentFilter;-><init>()V

    iput-object v10, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mIntentFilter:Landroid/content/IntentFilter;

    .line 170
    iget-object v10, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v11, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v10, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 171
    iget-object v10, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v11, "android.bluetooth.adapter.action.ACTION_LE_TESE_END_COMPLETED"

    invoke-virtual {v10, v11}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 173
    iget-object v10, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;

    iget-object v11, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v12, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 174
    const/4 v10, 0x1

    sput-boolean v10, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->isRegist:Z

    .line 176
    iget-object v10, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v10}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v10

    const/16 v11, 0xc

    if-ne v10, v11, :cond_0

    .line 177
    iget-object v10, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v10}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 199
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;

    const-string v1, "Exit Bluetooth LE Test"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    .line 205
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->isRegist:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 207
    sput-boolean v2, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->isRegist:Z

    .line 210
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->le_test_on:Z

    if-eqz v0, :cond_2

    .line 211
    const-string v0, "ctl.start"

    const-string v1, "LE_dut_cmd_off"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    sput-boolean v2, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->le_test_on:Z

    .line 214
    :cond_2
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 276
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 278
    invoke-virtual {p0}, Lcom/sec/android/app/bluetoothtest/BluetoothLEDutMode;->DisableBluetoothLEDUTMode()V

    .line 280
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 186
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 187
    const-string v0, "BluetoothLEDutMode"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 193
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 194
    const-string v0, "BluetoothLEDutMode"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    return-void
.end method
