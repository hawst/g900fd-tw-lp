.class public Lcom/sec/android/app/bluetoothtest/att/BluetoothTestBroadcastReceiverATT;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothTestBroadcastReceiverATT.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private getIndexOfNameOfPairedDeviceRequest(Ljava/lang/String;)I
    .locals 8
    .param p1, "param"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 133
    :try_start_0
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v5, ","

    invoke-direct {v3, p1, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    .local v3, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 135
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 136
    .local v2, "idx":I
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 137
    .local v0, "command":Ljava/lang/String;
    const-string v5, "BluetoothTestBroadcastReceiverATT"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getIndexOfNameOfPairedDeviceRequest():idx:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", command:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    const-string v5, "NM"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_0

    if-le v2, v4, :cond_0

    .line 146
    .end local v0    # "command":Ljava/lang/String;
    .end local v2    # "idx":I
    .end local v3    # "st":Ljava/util/StringTokenizer;
    :goto_0
    return v2

    .line 142
    :catch_0
    move-exception v1

    .line 143
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "BluetoothTestBroadcastReceiverATT"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getIndexOfNameOfPairedDeviceRequest():"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v4

    .line 144
    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v3    # "st":Ljava/util/StringTokenizer;
    :cond_0
    move v2, v4

    .line 146
    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "receivedIntent"    # Landroid/content/Intent;

    .prologue
    .line 36
    if-nez p2, :cond_1

    .line 37
    const-string v14, "BluetoothTestBroadcastReceiverATT"

    const-string v15, "receivedIntent is null"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "action":Ljava/lang/String;
    const-string v14, "command"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 43
    .local v3, "command":Ljava/lang/String;
    const-string v14, "BluetoothTestBroadcastReceiverATT"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "onReceive(), action: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", command: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 47
    :goto_1
    const-string v14, "android.intent.action.BCS_REQUEST"

    invoke-virtual {v14, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_d

    const-string v14, "AT+CBLTH="

    invoke-virtual {v3, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_d

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v14

    const-string v15, "AT+CBLTH="

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    if-le v14, v15, :cond_d

    .line 50
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v9

    .line 51
    .local v9, "mAdapter":Landroid/bluetooth/BluetoothAdapter;
    const-string v14, "AT+CBLTH="

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    invoke-virtual {v3, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 52
    .local v11, "param":Ljava/lang/String;
    const/4 v6, 0x0

    .line 53
    .local v6, "doBroadcast":Z
    const-string v14, "BluetoothTestBroadcastReceiverATT"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "onReceive(), param: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    new-instance v12, Landroid/content/Intent;

    const-string v14, "android.intent.action.BCS_RESPONSE"

    invoke-direct {v12, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 57
    .local v12, "responseIntent":Landroid/content/Intent;
    const-string v14, "NM"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 58
    invoke-virtual {v9}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v10

    .line 59
    .local v10, "name":Ljava/lang/String;
    const-string v14, "BluetoothTestBroadcastReceiverATT"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "PARAM_DEVICE_NAME : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    const-string v14, "response"

    if-eqz v10, :cond_4

    .end local v10    # "name":Ljava/lang/String;
    :goto_2
    invoke-virtual {v12, v14, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    const/4 v6, 0x1

    .line 105
    :cond_2
    :goto_3
    if-eqz v6, :cond_0

    .line 106
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 45
    .end local v6    # "doBroadcast":Z
    .end local v9    # "mAdapter":Landroid/bluetooth/BluetoothAdapter;
    .end local v11    # "param":Ljava/lang/String;
    .end local v12    # "responseIntent":Landroid/content/Intent;
    :cond_3
    const-string v3, ""

    goto/16 :goto_1

    .line 60
    .restart local v6    # "doBroadcast":Z
    .restart local v9    # "mAdapter":Landroid/bluetooth/BluetoothAdapter;
    .restart local v10    # "name":Ljava/lang/String;
    .restart local v11    # "param":Ljava/lang/String;
    .restart local v12    # "responseIntent":Landroid/content/Intent;
    :cond_4
    const-string v10, "0"

    goto :goto_2

    .line 63
    .end local v10    # "name":Ljava/lang/String;
    :cond_5
    const-string v14, "NR"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 64
    invoke-virtual {v9}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v2

    .line 65
    .local v2, "bondedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    const/4 v4, 0x0

    .line 66
    .local v4, "count":I
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/bluetooth/BluetoothDevice;

    .line 67
    .local v5, "device":Landroid/bluetooth/BluetoothDevice;
    add-int/lit8 v4, v4, 0x1

    .line 68
    goto :goto_4

    .line 70
    .end local v5    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_6
    const-string v14, "BluetoothTestBroadcastReceiverATT"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "PARAM_NUMBER_OF_PAIRED_DEVICE : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    const-string v15, "response"

    const/4 v14, -0x1

    if-le v4, v14, :cond_7

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v14

    :goto_5
    invoke-virtual {v12, v15, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    const/4 v6, 0x1

    .line 76
    goto :goto_3

    .line 71
    :cond_7
    const-string v14, "0"

    goto :goto_5

    .line 77
    .end local v2    # "bondedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    .end local v4    # "count":I
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_8
    const/4 v8, 0x0

    .line 78
    .local v8, "idx":I
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/sec/android/app/bluetoothtest/att/BluetoothTestBroadcastReceiverATT;->getIndexOfNameOfPairedDeviceRequest(Ljava/lang/String;)I

    move-result v8

    const/4 v14, -0x1

    if-le v8, v14, :cond_2

    .line 81
    invoke-virtual {v9}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v2

    .line 83
    .restart local v2    # "bondedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    const/4 v4, 0x0

    .line 84
    .restart local v4    # "count":I
    const/4 v13, 0x0

    .line 85
    .local v13, "result":Z
    const/4 v10, 0x0

    .line 86
    .restart local v10    # "name":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .restart local v7    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/bluetooth/BluetoothDevice;

    .line 87
    .restart local v5    # "device":Landroid/bluetooth/BluetoothDevice;
    if-ne v4, v8, :cond_a

    .line 88
    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v10

    .line 89
    const-string v14, "BluetoothTestBroadcastReceiverATT"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getIndexOfNameOfPairedDeviceRequest["

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "]:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const/4 v13, 0x1

    .line 96
    .end local v5    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_9
    if-eqz v13, :cond_c

    .line 97
    const-string v14, "response"

    if-eqz v10, :cond_b

    .end local v10    # "name":Ljava/lang/String;
    :goto_7
    invoke-virtual {v12, v14, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    :goto_8
    const/4 v6, 0x1

    goto/16 :goto_3

    .line 93
    .restart local v5    # "device":Landroid/bluetooth/BluetoothDevice;
    .restart local v10    # "name":Ljava/lang/String;
    :cond_a
    add-int/lit8 v4, v4, 0x1

    .line 94
    goto :goto_6

    .line 97
    .end local v5    # "device":Landroid/bluetooth/BluetoothDevice;
    :cond_b
    const-string v10, "0"

    goto :goto_7

    .line 99
    :cond_c
    const-string v14, "response"

    const-string v15, "0"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_8

    .line 108
    .end local v2    # "bondedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    .end local v4    # "count":I
    .end local v6    # "doBroadcast":Z
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "idx":I
    .end local v9    # "mAdapter":Landroid/bluetooth/BluetoothAdapter;
    .end local v10    # "name":Ljava/lang/String;
    .end local v11    # "param":Ljava/lang/String;
    .end local v12    # "responseIntent":Landroid/content/Intent;
    .end local v13    # "result":Z
    :cond_d
    const-string v14, "AT+BTVALUE"

    invoke-virtual {v14, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 109
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v9

    .line 110
    .restart local v9    # "mAdapter":Landroid/bluetooth/BluetoothAdapter;
    new-instance v12, Landroid/content/Intent;

    const-string v14, "android.intent.action.BCS_RESPONSE"

    invoke-direct {v12, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 112
    .restart local v12    # "responseIntent":Landroid/content/Intent;
    if-nez v9, :cond_e

    .line 113
    const-string v14, "BluetoothTestBroadcastReceiverATT"

    const-string v15, "COMMAND_REQ_BTVALUE_ATT error"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const-string v14, "response"

    const-string v15, "ERROR"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    :goto_9
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 115
    :cond_e
    invoke-virtual {v9}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v14

    if-eqz v14, :cond_f

    .line 116
    const-string v14, "BluetoothTestBroadcastReceiverATT"

    const-string v15, "COMMAND_REQ_BTVALUE_ATT on"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    const-string v14, "response"

    const-string v15, "ON"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_9

    .line 119
    :cond_f
    const-string v14, "BluetoothTestBroadcastReceiverATT"

    const-string v15, "COMMAND_REQ_BTVALUE_ATT off"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const-string v14, "response"

    const-string v15, "OFF"

    invoke-virtual {v12, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_9
.end method
