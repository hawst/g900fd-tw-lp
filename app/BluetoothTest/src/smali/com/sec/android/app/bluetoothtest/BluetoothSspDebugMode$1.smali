.class Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode$1;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothSspDebugMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;


# direct methods
.method constructor <init>(Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode$1;->this$0:Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 32
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 35
    const-string v2, "android.bluetooth.adapter.extra.STATE"

    const/high16 v3, -0x80000000

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 36
    .local v1, "state":I
    packed-switch v1, :pswitch_data_0

    .line 57
    .end local v1    # "state":I
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 38
    .restart local v1    # "state":I
    :pswitch_1
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonSspEnable:Landroid/widget/Button;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->access$000()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 39
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonSspDisable:Landroid/widget/Button;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->access$100()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 40
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonHciEnable:Landroid/widget/Button;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->access$200()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 41
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonHciDisable:Landroid/widget/Button;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->access$300()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 42
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonLoopbackEnable:Landroid/widget/Button;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->access$400()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 43
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonLoopbackDisable:Landroid/widget/Button;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->access$500()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 46
    :pswitch_2
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonSspEnable:Landroid/widget/Button;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->access$000()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 47
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonSspDisable:Landroid/widget/Button;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->access$100()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 48
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonHciEnable:Landroid/widget/Button;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->access$200()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 49
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonHciDisable:Landroid/widget/Button;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->access$300()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 50
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonLoopbackEnable:Landroid/widget/Button;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->access$400()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 51
    # getter for: Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->mButtonLoopbackDisable:Landroid/widget/Button;
    invoke-static {}, Lcom/sec/android/app/bluetoothtest/BluetoothSspDebugMode;->access$500()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 36
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
