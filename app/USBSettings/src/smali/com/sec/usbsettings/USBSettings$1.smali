.class Lcom/sec/usbsettings/USBSettings$1;
.super Ljava/lang/Object;
.source "USBSettings.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/usbsettings/USBSettings;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/usbsettings/USBSettings;


# direct methods
.method constructor <init>(Lcom/sec/usbsettings/USBSettings;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/usbsettings/USBSettings$1;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 3
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    .line 97
    const v1, 0x7f050002

    if-ne p2, v1, :cond_1

    .line 99
    :try_start_0
    iget-object v1, p0, Lcom/sec/usbsettings/USBSettings$1;->this$0:Lcom/sec/usbsettings/USBSettings;

    const-string v2, "MODEM"

    # invokes: Lcom/sec/usbsettings/USBSettings;->changeUsb(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/usbsettings/USBSettings;->access$000(Lcom/sec/usbsettings/USBSettings;Ljava/lang/String;)V

    .line 100
    iget-object v1, p0, Lcom/sec/usbsettings/USBSettings$1;->this$0:Lcom/sec/usbsettings/USBSettings;

    const/4 v2, 0x0

    # setter for: Lcom/sec/usbsettings/USBSettings;->mUsbToAp:Z
    invoke-static {v1, v2}, Lcom/sec/usbsettings/USBSettings;->access$102(Lcom/sec/usbsettings/USBSettings;Z)Z

    .line 101
    iget-object v1, p0, Lcom/sec/usbsettings/USBSettings$1;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbSettingRadioGroup:Landroid/widget/RadioGroup;
    invoke-static {v1}, Lcom/sec/usbsettings/USBSettings;->access$200(Lcom/sec/usbsettings/USBSettings;)Landroid/widget/RadioGroup;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 102
    iget-object v1, p0, Lcom/sec/usbsettings/USBSettings$1;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbSettingTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/usbsettings/USBSettings;->access$300(Lcom/sec/usbsettings/USBSettings;)Landroid/widget/TextView;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    const-string v1, "USBSettings"

    const-string v2, "USB_STATUS1 : MODEM"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "USBSettings"

    const-string v2, "Exception! USB to MODEM"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    .line 108
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    const v1, 0x7f050003

    if-ne p2, v1, :cond_0

    .line 110
    :try_start_1
    iget-object v1, p0, Lcom/sec/usbsettings/USBSettings$1;->this$0:Lcom/sec/usbsettings/USBSettings;

    const-string v2, "PDA"

    # invokes: Lcom/sec/usbsettings/USBSettings;->changeUsb(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/usbsettings/USBSettings;->access$000(Lcom/sec/usbsettings/USBSettings;Ljava/lang/String;)V

    .line 111
    iget-object v1, p0, Lcom/sec/usbsettings/USBSettings$1;->this$0:Lcom/sec/usbsettings/USBSettings;

    const/4 v2, 0x1

    # setter for: Lcom/sec/usbsettings/USBSettings;->mUsbToAp:Z
    invoke-static {v1, v2}, Lcom/sec/usbsettings/USBSettings;->access$102(Lcom/sec/usbsettings/USBSettings;Z)Z

    .line 112
    iget-object v1, p0, Lcom/sec/usbsettings/USBSettings$1;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v1}, Lcom/sec/usbsettings/USBSettings;->readCurrentSettings()V

    .line 113
    iget-object v1, p0, Lcom/sec/usbsettings/USBSettings$1;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbSettingRadioGroup:Landroid/widget/RadioGroup;
    invoke-static {v1}, Lcom/sec/usbsettings/USBSettings;->access$200(Lcom/sec/usbsettings/USBSettings;)Landroid/widget/RadioGroup;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 114
    iget-object v1, p0, Lcom/sec/usbsettings/USBSettings$1;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbSettingTextView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/usbsettings/USBSettings;->access$300(Lcom/sec/usbsettings/USBSettings;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    const-string v1, "USBSettings"

    const-string v2, "USB_STATUS1 : PDA"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 116
    :catch_1
    move-exception v0

    .line 117
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v1, "USBSettings"

    const-string v2, "Exception! USB to PDA"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method
