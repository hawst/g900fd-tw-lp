.class public Lcom/sec/usbsettings/USBSettings;
.super Landroid/app/Activity;
.source "USBSettings.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mUartToAp:Z

.field private mUsbManager:Landroid/hardware/usb/UsbManager;

.field private mUsbPathTextView:Landroid/widget/TextView;

.field private mUsbRadioGroup:Landroid/widget/RadioGroup;

.field private mUsbSettingRadioGroup:Landroid/widget/RadioGroup;

.field private mUsbSettingTextView:Landroid/widget/TextView;

.field private mUsbToAp:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/usbsettings/USBSettings;->mUsbToAp:Z

    .line 71
    invoke-direct {p0}, Lcom/sec/usbsettings/USBSettings;->isUartAP()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/usbsettings/USBSettings;->mUartToAp:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/usbsettings/USBSettings;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/usbsettings/USBSettings;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/usbsettings/USBSettings;->changeUsb(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/usbsettings/USBSettings;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/usbsettings/USBSettings;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/sec/usbsettings/USBSettings;->mUsbToAp:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/usbsettings/USBSettings;)Landroid/widget/RadioGroup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/usbsettings/USBSettings;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/usbsettings/USBSettings;->mUsbSettingRadioGroup:Landroid/widget/RadioGroup;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/usbsettings/USBSettings;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/usbsettings/USBSettings;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/usbsettings/USBSettings;->mUsbSettingTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/usbsettings/USBSettings;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;

    return-object v0
.end method

.method private changeUsb(Ljava/lang/String;)V
    .locals 2
    .param p1, "usb"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 441
    const-string v0, "PDA"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 442
    const-string v0, "USBSettings"

    const-string v1, "USB to PDA"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    const-string v0, "/sys/class/sec/switch/usb_sel"

    const-string v1, "PDA"

    invoke-direct {p0, v0, v1}, Lcom/sec/usbsettings/USBSettings;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    const-string v0, "MODEM"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    const-string v0, "USBSettings"

    const-string v1, "USB to MODEM"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    const-string v0, "/sys/class/sec/switch/usb_sel"

    const-string v1, "MODEM"

    invoke-direct {p0, v0, v1}, Lcom/sec/usbsettings/USBSettings;->writeFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doRebootNSave(I)V
    .locals 5
    .param p1, "state"    # I

    .prologue
    .line 378
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "swsel"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 379
    .local v1, "switchsel":Ljava/lang/String;
    const-string v2, "USBSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "switchsel: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/usbsettings/USBSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 381
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    .line 382
    return-void
.end method

.method private initRadioItems()V
    .locals 5

    .prologue
    const/4 v4, 0x7

    .line 423
    invoke-direct {p0}, Lcom/sec/usbsettings/USBSettings;->isMDMBaseband()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 424
    new-array v1, v4, [I

    fill-array-data v1, :array_0

    .line 434
    .local v1, "modeIds":[I
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_1

    .line 435
    aget v3, v1, v0

    invoke-virtual {p0, v3}, Lcom/sec/usbsettings/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    .line 436
    .local v2, "rb":Landroid/widget/RadioButton;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 434
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 429
    .end local v0    # "i":I
    .end local v1    # "modeIds":[I
    .end local v2    # "rb":Landroid/widget/RadioButton;
    :cond_0
    new-array v1, v4, [I

    fill-array-data v1, :array_1

    .restart local v1    # "modeIds":[I
    goto :goto_0

    .line 438
    .restart local v0    # "i":I
    :cond_1
    return-void

    .line 424
    :array_0
    .array-data 4
        0x7f050006
        0x7f050007
        0x7f050008
        0x7f050009
        0x7f05000a
        0x7f05000c
        0x7f050015
    .end array-data

    .line 429
    :array_1
    .array-data 4
        0x7f050006
        0x7f050007
        0x7f050008
        0x7f050009
        0x7f05000a
        0x7f05000b
        0x7f05000c
    .end array-data
.end method

.method private initialSetting(Landroid/widget/RadioGroup;)V
    .locals 5
    .param p1, "usb"    # Landroid/widget/RadioGroup;

    .prologue
    const/16 v3, 0x8

    .line 385
    new-instance v1, Ljava/io/File;

    const-string v2, "/sys/class/sec/switch/usb_sel"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 386
    .local v1, "usbPath":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 387
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings;->mUsbPathTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 388
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    const-string v2, "/sys/class/sec/switch/usb_sel"

    invoke-direct {p0, v2}, Lcom/sec/usbsettings/USBSettings;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 392
    .local v0, "currentUsb":Ljava/lang/String;
    const-string v2, "USBSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "USB_Init : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    const-string v2, "MODEM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 395
    const-string v2, "USBSettings"

    const-string v3, "Initial USB_STATUS1 : MODEM"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    const v2, 0x7f050002

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 397
    :cond_2
    const-string v2, "PDA"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 398
    const-string v2, "USBSettings"

    const-string v3, "Initial USB_STATUS1 : PDA"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    const v2, 0x7f050003

    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0
.end method

.method private isMDMBaseband()Z
    .locals 4

    .prologue
    .line 359
    const-string v2, "ro.chipname"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 361
    .local v1, "solution":Ljava/lang/String;
    const-string v2, "NONE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 362
    const-string v2, "ro.product.board"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 365
    :cond_0
    const-string v2, "ro.baseband"

    const-string v3, "NONE"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 367
    .local v0, "Baseband":Ljava/lang/String;
    const-string v2, "smdk4x12"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "universal5410"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "exynos5410"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "exynos5260"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "exynos5420"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    const-string v2, "mdm"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 371
    const/4 v2, 0x1

    .line 374
    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isUartAP()Z
    .locals 2

    .prologue
    .line 491
    const-string v1, "/sys/class/sec/switch/uart_sel"

    invoke-direct {p0, v1}, Lcom/sec/usbsettings/USBSettings;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 492
    .local v0, "currentUart":Ljava/lang/String;
    const-string v1, "AP"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 493
    const/4 v1, 0x1

    .line 494
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 451
    const-string v6, ""

    .line 452
    .local v6, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 453
    .local v0, "buf":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 456
    .local v4, "fr":Ljava/io/FileReader;
    :try_start_0
    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 457
    .end local v4    # "fr":Ljava/io/FileReader;
    .local v5, "fr":Ljava/io/FileReader;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    const/16 v7, 0x1fa0

    invoke-direct {v1, v5, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 459
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 460
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v6

    .line 470
    :cond_0
    if-eqz v5, :cond_1

    .line 471
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V

    .line 474
    :cond_1
    if-eqz v1, :cond_2

    .line 475
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 483
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_3
    :goto_0
    if-nez v6, :cond_8

    .line 484
    const-string v7, ""

    .line 486
    :goto_1
    return-object v7

    .line 477
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_0
    move-exception v2

    .line 478
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "USBSettings"

    const-string v8, "IOException close()"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .line 481
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 462
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 463
    .local v3, "ex":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_4
    const-string v7, "USBSettings"

    const-string v8, "FileNotFoundException"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 470
    if-eqz v4, :cond_4

    .line 471
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 474
    :cond_4
    if-eqz v0, :cond_3

    .line 475
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 477
    :catch_2
    move-exception v2

    .line 478
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "USBSettings"

    const-string v8, "IOException close()"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 465
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v2

    .line 466
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    const-string v7, "USBSettings"

    const-string v8, "IOException"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 470
    if-eqz v4, :cond_5

    .line 471
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 474
    :cond_5
    if-eqz v0, :cond_3

    .line 475
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_0

    .line 477
    :catch_4
    move-exception v2

    .line 478
    const-string v7, "USBSettings"

    const-string v8, "IOException close()"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 469
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 470
    :goto_4
    if-eqz v4, :cond_6

    .line 471
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileReader;->close()V

    .line 474
    :cond_6
    if-eqz v0, :cond_7

    .line 475
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 480
    :cond_7
    :goto_5
    throw v7

    .line 477
    :catch_5
    move-exception v2

    .line 478
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v8, "USBSettings"

    const-string v9, "IOException close()"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 486
    .end local v2    # "e":Ljava/io/IOException;
    :cond_8
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 469
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_1
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_4

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catchall_2
    move-exception v7

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_4

    .line 465
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_6
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_3

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_7
    move-exception v2

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_3

    .line 462
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_8
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    goto :goto_2

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v4    # "fr":Ljava/io/FileReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v5    # "fr":Ljava/io/FileReader;
    :catch_9
    move-exception v3

    move-object v4, v5

    .end local v5    # "fr":Ljava/io/FileReader;
    .restart local v4    # "fr":Ljava/io/FileReader;
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method private writeFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 498
    const/4 v1, 0x0

    .line 501
    .local v1, "fw":Ljava/io/FileWriter;
    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 502
    .end local v1    # "fw":Ljava/io/FileWriter;
    .local v2, "fw":Ljava/io/FileWriter;
    :try_start_1
    invoke-virtual {v2, p2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 506
    const-string v3, "USBSettings"

    const-string v4, "Write Success!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    if-eqz v2, :cond_2

    .line 510
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v1, v2

    .line 516
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    :cond_0
    :goto_0
    return-void

    .line 511
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_0
    move-exception v0

    .line 512
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "USBSettings"

    const-string v4, "IOException"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    .line 513
    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0

    .line 503
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 504
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v3, "USBSettings"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOExceptionfilepath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " value : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 506
    const-string v3, "USBSettings"

    const-string v4, "Write Success!"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    if-eqz v1, :cond_0

    .line 510
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 511
    :catch_2
    move-exception v0

    .line 512
    const-string v3, "USBSettings"

    const-string v4, "IOException"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 506
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    :goto_2
    const-string v4, "USBSettings"

    const-string v5, "Write Success!"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    if-eqz v1, :cond_1

    .line 510
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 513
    :cond_1
    :goto_3
    throw v3

    .line 511
    :catch_3
    move-exception v0

    .line 512
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "USBSettings"

    const-string v5, "IOException"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 506
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_2

    .line 503
    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_1

    .end local v1    # "fw":Ljava/io/FileWriter;
    .restart local v2    # "fw":Ljava/io/FileWriter;
    :cond_2
    move-object v1, v2

    .end local v2    # "fw":Ljava/io/FileWriter;
    .restart local v1    # "fw":Ljava/io/FileWriter;
    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 263
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 277
    :goto_0
    return-void

    .line 265
    :pswitch_0
    const-string v1, "USBSettings"

    const-string v2, "OK USBSetting"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    const-string v1, "Saved! "

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 267
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 270
    :pswitch_1
    const-string v1, "Rebooting "

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 271
    iget-boolean v1, p0, Lcom/sec/usbsettings/USBSettings;->mUartToAp:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_1
    shl-int/lit8 v1, v1, 0x1

    iget-boolean v4, p0, Lcom/sec/usbsettings/USBSettings;->mUsbToAp:Z

    if-eqz v4, :cond_1

    :goto_2
    or-int v0, v1, v2

    .line 272
    .local v0, "parameter":I
    invoke-direct {p0, v0}, Lcom/sec/usbsettings/USBSettings;->doRebootNSave(I)V

    goto :goto_0

    .end local v0    # "parameter":I
    :cond_0
    move v1, v3

    .line 271
    goto :goto_1

    :cond_1
    move v2, v3

    goto :goto_2

    .line 263
    :pswitch_data_0
    .packed-switch 0x7f050016
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    const-string v2, "USBSettings"

    const-string v3, "Class create!"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const/high16 v2, 0x7f030000

    invoke-virtual {p0, v2}, Lcom/sec/usbsettings/USBSettings;->setContentView(I)V

    .line 81
    const-string v2, "usb"

    invoke-virtual {p0, v2}, Lcom/sec/usbsettings/USBSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/usb/UsbManager;

    iput-object v2, p0, Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;

    .line 84
    const v2, 0x7f050016

    invoke-virtual {p0, v2}, Lcom/sec/usbsettings/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 85
    .local v0, "ok_button":Landroid/view/View;
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    const v2, 0x7f050017

    invoke-virtual {p0, v2}, Lcom/sec/usbsettings/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 88
    .local v1, "reboot_button":Landroid/view/View;
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    const v2, 0x7f050005

    invoke-virtual {p0, v2}, Lcom/sec/usbsettings/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    iput-object v2, p0, Lcom/sec/usbsettings/USBSettings;->mUsbSettingRadioGroup:Landroid/widget/RadioGroup;

    .line 91
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings;->mUsbSettingRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v2, p0}, Landroid/widget/RadioGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    const/high16 v2, 0x7f050000

    invoke-virtual {p0, v2}, Lcom/sec/usbsettings/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/usbsettings/USBSettings;->mUsbPathTextView:Landroid/widget/TextView;

    .line 93
    const v2, 0x7f050004

    invoke-virtual {p0, v2}, Lcom/sec/usbsettings/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/usbsettings/USBSettings;->mUsbSettingTextView:Landroid/widget/TextView;

    .line 94
    const v2, 0x7f050001

    invoke-virtual {p0, v2}, Lcom/sec/usbsettings/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    iput-object v2, p0, Lcom/sec/usbsettings/USBSettings;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    .line 95
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    new-instance v3, Lcom/sec/usbsettings/USBSettings$1;

    invoke-direct {v3, p0}, Lcom/sec/usbsettings/USBSettings$1;-><init>(Lcom/sec/usbsettings/USBSettings;)V

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 124
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings;->mUsbSettingRadioGroup:Landroid/widget/RadioGroup;

    new-instance v3, Lcom/sec/usbsettings/USBSettings$2;

    invoke-direct {v3, p0}, Lcom/sec/usbsettings/USBSettings$2;-><init>(Lcom/sec/usbsettings/USBSettings;)V

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 234
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings;->mUsbRadioGroup:Landroid/widget/RadioGroup;

    invoke-direct {p0, v2}, Lcom/sec/usbsettings/USBSettings;->initialSetting(Landroid/widget/RadioGroup;)V

    .line 235
    invoke-direct {p0}, Lcom/sec/usbsettings/USBSettings;->initRadioItems()V

    .line 236
    invoke-virtual {p0}, Lcom/sec/usbsettings/USBSettings;->readCurrentSettings()V

    .line 238
    return-void
.end method

.method public readCurrentSettings()V
    .locals 6

    .prologue
    .line 280
    const-string v3, "USBSettings"

    const-string v4, "readCurrentSettings"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    const/4 v1, -0x1

    .line 282
    .local v1, "selected_mode_id":I
    const-string v3, "sys.usb.config"

    const-string v4, "None"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 284
    .local v2, "usbSetStatus":Ljava/lang/String;
    const-string v3, "USBSettings"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CurrentUSB Setting : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    const-string v3, "mtp,adb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 287
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is mtp,adb"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    const v1, 0x7f050007

    .line 351
    :cond_0
    :goto_0
    const/4 v3, -0x1

    if-le v1, v3, :cond_1

    .line 352
    invoke-virtual {p0, v1}, Lcom/sec/usbsettings/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 353
    .local v0, "rb":Landroid/widget/RadioButton;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 355
    .end local v0    # "rb":Landroid/widget/RadioButton;
    :cond_1
    return-void

    .line 289
    :cond_2
    const-string v3, "mtp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 290
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is mtp"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    const v1, 0x7f050006

    goto :goto_0

    .line 292
    :cond_3
    const-string v3, "ptp,adb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 293
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is ptp,adb"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const v1, 0x7f050009

    goto :goto_0

    .line 295
    :cond_4
    const-string v3, "ptp"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 296
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is ptp"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    const v1, 0x7f050008

    goto :goto_0

    .line 298
    :cond_5
    const-string v3, "rndis,acm,diag"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 299
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is rndis,acm,diag"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    const v1, 0x7f05000a

    goto :goto_0

    .line 301
    :cond_6
    const-string v3, "rmnet,acm,diag"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 302
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is rmnet,acm,diag"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    const v1, 0x7f05000b

    goto :goto_0

    .line 304
    :cond_7
    const-string v3, "diag,acm,adb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 305
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is diag,acm,adb"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    const v1, 0x7f05000c

    goto :goto_0

    .line 310
    :cond_8
    const-string v3, "rndis,adb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 311
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is rndis,adb"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    const v1, 0x7f05000d

    goto/16 :goto_0

    .line 313
    :cond_9
    const-string v3, "rndis,acm,dm"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 314
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is rndis,acm,dm"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    const v1, 0x7f05000e

    goto/16 :goto_0

    .line 316
    :cond_a
    const-string v3, "rndis,acm,dm,adb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 317
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is rndis,acm,dm,adb"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    const v1, 0x7f05000f

    goto/16 :goto_0

    .line 322
    :cond_b
    const-string v3, "acm,adb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 323
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is acm,adb"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    const v1, 0x7f050011

    goto/16 :goto_0

    .line 326
    :cond_c
    const-string v3, "acm"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 327
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is acm"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    const v1, 0x7f050010

    goto/16 :goto_0

    .line 330
    :cond_d
    const-string v3, "rndis,acm"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 331
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is rndis,acm"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    const v1, 0x7f050012

    goto/16 :goto_0

    .line 334
    :cond_e
    const-string v3, "rndis,adb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 335
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is rndis,adb"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    const v1, 0x7f05000d

    goto/16 :goto_0

    .line 338
    :cond_f
    const-string v3, "rndis,acm,adb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 339
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is rndis,acm,adb"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    const v1, 0x7f050013

    goto/16 :goto_0

    .line 342
    :cond_10
    const-string v3, "rndis,acm,diag,adb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 343
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is rndis,acm,diag,adb"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    const v1, 0x7f050015

    goto/16 :goto_0

    .line 346
    :cond_11
    const-string v3, "dm,acm,adb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 347
    const-string v3, "USBSettings"

    const-string v4, "Check Radio Button is dm,acm,adb"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    const v1, 0x7f050014

    goto/16 :goto_0
.end method

.method public showNotification()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 241
    const-string v6, "notification"

    invoke-virtual {p0, v6}, Lcom/sec/usbsettings/USBSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 242
    .local v2, "nm":Landroid/app/NotificationManager;
    if-nez v2, :cond_0

    .line 243
    const-string v6, "USBSettings"

    const-string v7, "NotificationManager is null"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :goto_0
    return-void

    .line 246
    :cond_0
    const-string v5, "Data service is NOT available"

    .line 247
    .local v5, "title":Ljava/lang/CharSequence;
    const-string v1, "Data service will not be available with RMNET+DM+MODEM combination after reboot."

    .line 248
    .local v1, "message":Ljava/lang/CharSequence;
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Lcom/sec/usbsettings/USBSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 249
    .local v0, "builder":Landroid/app/Notification$Builder;
    new-instance v4, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v4, v0}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    .line 250
    .local v4, "style":Landroid/app/Notification$BigTextStyle;
    invoke-virtual {v4, v5}, Landroid/app/Notification$BigTextStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    .line 251
    invoke-virtual {v4, v1}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    .line 252
    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    .line 253
    const/high16 v6, 0x7f020000

    invoke-virtual {v0, v6}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 254
    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 255
    invoke-virtual {v0, v7}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    .line 256
    invoke-virtual {v0, v7}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 257
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    .line 258
    .local v3, "noti":Landroid/app/Notification;
    const/16 v6, 0x6f

    invoke-virtual {v2, v6, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method
