.class Lcom/sec/usbsettings/USBSettings$2;
.super Ljava/lang/Object;
.source "USBSettings.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/usbsettings/USBSettings;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/usbsettings/USBSettings;


# direct methods
.method constructor <init>(Lcom/sec/usbsettings/USBSettings;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 6
    .param p1, "arg0"    # Landroid/widget/RadioGroup;
    .param p2, "arg1"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 127
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2, p2}, Lcom/sec/usbsettings/USBSettings;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 128
    .local v1, "rb":Landroid/widget/RadioButton;
    const-string v2, "ro.board.platform"

    const-string v3, "None"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, "chipsetName":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v1}, Landroid/widget/RadioButton;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 130
    const-string v2, "USBSettings"

    const-string v3, "OnClickListener"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    const-string v2, "APQ8084"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    const v2, 0x7f05000b

    if-ne p2, v2, :cond_1

    .line 134
    const-string v2, "persist.data.mode"

    const-string v3, "tethered"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->showNotification()V

    .line 142
    :cond_0
    :goto_0
    packed-switch p2, :pswitch_data_0

    .line 228
    const-string v2, "LOG_TAG"

    const-string v3, "default"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :goto_1
    return-void

    .line 138
    :cond_1
    const-string v2, "persist.data.mode"

    const-string v3, ""

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 144
    :pswitch_0
    const-string v2, "USBSettings"

    const-string v3, "MTP_MODE "

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 146
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "mtp"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto :goto_1

    .line 149
    :pswitch_1
    const-string v2, "USBSettings"

    const-string v3, "MTP_ADB_MODE "

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 151
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "mtp,adb"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto :goto_1

    .line 154
    :pswitch_2
    const-string v2, "USBSettings"

    const-string v3, "PTP_MODE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 156
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "ptp"

    invoke-virtual {v2, v3, v5}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto :goto_1

    .line 159
    :pswitch_3
    const-string v2, "USBSettings"

    const-string v3, "PTP_ADB_MODE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 161
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "ptp,adb"

    invoke-virtual {v2, v3, v5}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto :goto_1

    .line 164
    :pswitch_4
    const-string v2, "USBSettings"

    const-string v3, "RNDIS_DM_MODEM"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 166
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "rndis,acm,diag"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 169
    :pswitch_5
    const-string v2, "USBSettings"

    const-string v3, "RMNET_DM_MODEM"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 171
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "rmnet,acm,diag"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 174
    :pswitch_6
    const-string v2, "USBSettings"

    const-string v3, "DM_MODEM_ADB"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 176
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "diag,acm,adb"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 181
    :pswitch_7
    const-string v2, "USBSettings"

    const-string v3, "RNDIS_ADB_MODE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 183
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "rndis,adb"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 186
    :pswitch_8
    const-string v2, "USBSettings"

    const-string v3, "RNDIS_ACM_DM_MODE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 188
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "rndis,acm,dm"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 191
    :pswitch_9
    const-string v2, "USBSettings"

    const-string v3, "RNDIS_ACM_DM_ADB_MODE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 193
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "rndis,acm,dm,adb"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 198
    :pswitch_a
    const-string v2, "USBSettings"

    const-string v3, "ACM_MODE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 200
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "acm"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 203
    :pswitch_b
    const-string v2, "USBSettings"

    const-string v3, "ACM_ADB_MODE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 205
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "acm,adb"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 208
    :pswitch_c
    const-string v2, "USBSettings"

    const-string v3, "RNDIS_ACM_MODE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 210
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "rndis,acm"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 213
    :pswitch_d
    const-string v2, "USBSettings"

    const-string v3, "RNDIS_ACM_ADB_MODE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 215
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "rndis,acm,adb"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 218
    :pswitch_e
    const-string v2, "USBSettings"

    const-string v3, "RNDIS_DM_MODEM_ADB_MODE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 220
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "rndis,acm,diag,adb"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 223
    :pswitch_f
    const-string v2, "USBSettings"

    const-string v3, "RNDIS_DM_MODEM_ADB_MODE"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    invoke-virtual {v2}, Lcom/sec/usbsettings/USBSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "adb_enabled"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 225
    iget-object v2, p0, Lcom/sec/usbsettings/USBSettings$2;->this$0:Lcom/sec/usbsettings/USBSettings;

    # getter for: Lcom/sec/usbsettings/USBSettings;->mUsbManager:Landroid/hardware/usb/UsbManager;
    invoke-static {v2}, Lcom/sec/usbsettings/USBSettings;->access$400(Lcom/sec/usbsettings/USBSettings;)Landroid/hardware/usb/UsbManager;

    move-result-object v2

    const-string v3, "dm,acm,adb"

    invoke-virtual {v2, v3, v4}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 142
    :pswitch_data_0
    .packed-switch 0x7f050006
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_f
        :pswitch_e
    .end packed-switch
.end method
