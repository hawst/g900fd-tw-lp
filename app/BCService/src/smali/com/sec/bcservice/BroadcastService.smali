.class public Lcom/sec/bcservice/BroadcastService;
.super Landroid/app/Service;
.source "BroadcastService.java"


# static fields
.field private static final SWVER:Ljava/lang/String;

.field private static msize:J


# instance fields
.field private final CGMI:Ljava/lang/String;

.field private final CGMR:Ljava/lang/String;

.field private final DELAYTIME:I

.field private final OEM_REQ_DATA_HDR_SIZE:I

.field private PARAM_CDCONT_GANR:Ljava/lang/String;

.field private PARAM_CDCONT_GASZ:Ljava/lang/String;

.field private USBATCOMMAND_REQUEST:Ljava/lang/String;

.field private USBATCOMMAND_RESPONSE:Ljava/lang/String;

.field private commandMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private connectDRSocket:Ljava/lang/Runnable;

.field private dis:Ljava/io/DataInputStream;

.field private dos:Ljava/io/DataOutputStream;

.field private final id:I

.field private in:Ljava/io/InputStream;

.field private intentTimer:Landroid/os/Handler;

.field private keyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

.field private mIsRegistered:Z

.field private mIsSlateCommand:Z

.field private out:Ljava/io/OutputStream;

.field private socket:Landroid/net/LocalSocket;

.field private sysscopeResult:Ljava/lang/String;

.field private sysscopeStatus:I

.field private threadStatus:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 121
    const-string v0, "ro.build.PDA"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/bcservice/BroadcastService;->SWVER:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 79
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sec/bcservice/BroadcastService;->DELAYTIME:I

    .line 81
    iput-object v1, p0, Lcom/sec/bcservice/BroadcastService;->socket:Landroid/net/LocalSocket;

    .line 83
    iput-object v1, p0, Lcom/sec/bcservice/BroadcastService;->in:Ljava/io/InputStream;

    .line 84
    iput-object v1, p0, Lcom/sec/bcservice/BroadcastService;->dis:Ljava/io/DataInputStream;

    .line 86
    iput-object v1, p0, Lcom/sec/bcservice/BroadcastService;->out:Ljava/io/OutputStream;

    .line 87
    iput-object v1, p0, Lcom/sec/bcservice/BroadcastService;->dos:Ljava/io/DataOutputStream;

    .line 89
    iput-object v1, p0, Lcom/sec/bcservice/BroadcastService;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 90
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/bcservice/BroadcastService;->OEM_REQ_DATA_HDR_SIZE:I

    .line 92
    iput-boolean v2, p0, Lcom/sec/bcservice/BroadcastService;->threadStatus:Z

    .line 93
    iput-boolean v2, p0, Lcom/sec/bcservice/BroadcastService;->mIsSlateCommand:Z

    .line 94
    iput-boolean v2, p0, Lcom/sec/bcservice/BroadcastService;->mIsRegistered:Z

    .line 97
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    iput v0, p0, Lcom/sec/bcservice/BroadcastService;->id:I

    .line 99
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/bcservice/BroadcastService;->intentTimer:Landroid/os/Handler;

    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    .line 103
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/bcservice/BroadcastService;->keyMap:Ljava/util/HashMap;

    .line 109
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/bcservice/BroadcastService;->sysscopeStatus:I

    .line 110
    new-instance v0, Ljava/lang/String;

    const-string v1, "SCANNING"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/bcservice/BroadcastService;->sysscopeResult:Ljava/lang/String;

    .line 113
    const-string v0, "android.intent.action.BCS_REQUEST"

    iput-object v0, p0, Lcom/sec/bcservice/BroadcastService;->USBATCOMMAND_REQUEST:Ljava/lang/String;

    .line 114
    const-string v0, "android.intent.action.BCS_RESPONSE"

    iput-object v0, p0, Lcom/sec/bcservice/BroadcastService;->USBATCOMMAND_RESPONSE:Ljava/lang/String;

    .line 119
    const-string v0, "GA,NR"

    iput-object v0, p0, Lcom/sec/bcservice/BroadcastService;->PARAM_CDCONT_GANR:Ljava/lang/String;

    .line 120
    const-string v0, "GA,SZ"

    iput-object v0, p0, Lcom/sec/bcservice/BroadcastService;->PARAM_CDCONT_GASZ:Ljava/lang/String;

    .line 122
    const-string v0, "gsm.version.baseband"

    const-string v1, "ERROR"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/bcservice/BroadcastService;->CGMR:Ljava/lang/String;

    .line 123
    new-instance v0, Ljava/lang/String;

    const-string v1, "SAMSUNG ELECTRONICS CORPORATION"

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/bcservice/BroadcastService;->CGMI:Ljava/lang/String;

    .line 165
    new-instance v0, Lcom/sec/bcservice/BroadcastService$1;

    invoke-direct {v0, p0}, Lcom/sec/bcservice/BroadcastService$1;-><init>(Lcom/sec/bcservice/BroadcastService;)V

    iput-object v0, p0, Lcom/sec/bcservice/BroadcastService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 216
    new-instance v0, Lcom/sec/bcservice/BroadcastService$2;

    invoke-direct {v0, p0}, Lcom/sec/bcservice/BroadcastService$2;-><init>(Lcom/sec/bcservice/BroadcastService;)V

    iput-object v0, p0, Lcom/sec/bcservice/BroadcastService;->connectDRSocket:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->USBATCOMMAND_RESPONSE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/bcservice/BroadcastService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    iget v0, p0, Lcom/sec/bcservice/BroadcastService;->sysscopeStatus:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/bcservice/BroadcastService;Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/sec/bcservice/BroadcastService;->getArgument(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/bcservice/BroadcastService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;
    .param p1, "x1"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/sec/bcservice/BroadcastService;->sysscopeStatus:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->sysscopeResult:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->PARAM_CDCONT_GANR:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sec/bcservice/BroadcastService;->getNumApks()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->PARAM_CDCONT_GASZ:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sec/bcservice/BroadcastService;->getSizeApks()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->USBATCOMMAND_REQUEST:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/sec/bcservice/BroadcastService;->SWVER:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->CGMR:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/bcservice/BroadcastService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sec/bcservice/BroadcastService;->changeSysScopeResult()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->CGMI:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sec/bcservice/BroadcastService;->process_action_event_svc_if_pgm()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/bcservice/BroadcastService;)Lcom/samsung/android/sec_platform_library/FactoryPhone;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    return-object v0
.end method

.method static synthetic access$2314(J)J
    .locals 2
    .param p0, "x0"    # J

    .prologue
    .line 75
    sget-wide v0, Lcom/sec/bcservice/BroadcastService;->msize:J

    add-long/2addr v0, p0

    sput-wide v0, Lcom/sec/bcservice/BroadcastService;->msize:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/sec/bcservice/BroadcastService;)Landroid/net/LocalSocket;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->socket:Landroid/net/LocalSocket;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/bcservice/BroadcastService;Landroid/net/LocalSocket;)Landroid/net/LocalSocket;
    .locals 0
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;
    .param p1, "x1"    # Landroid/net/LocalSocket;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/bcservice/BroadcastService;->socket:Landroid/net/LocalSocket;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/bcservice/BroadcastService;)Ljava/io/InputStream;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->in:Ljava/io/InputStream;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/bcservice/BroadcastService;Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 0
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;
    .param p1, "x1"    # Ljava/io/InputStream;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/bcservice/BroadcastService;->in:Ljava/io/InputStream;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/bcservice/BroadcastService;)Ljava/io/DataInputStream;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->dis:Ljava/io/DataInputStream;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/bcservice/BroadcastService;Ljava/io/DataInputStream;)Ljava/io/DataInputStream;
    .locals 0
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;
    .param p1, "x1"    # Ljava/io/DataInputStream;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/bcservice/BroadcastService;->dis:Ljava/io/DataInputStream;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/bcservice/BroadcastService;)Ljava/io/OutputStream;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->out:Ljava/io/OutputStream;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/bcservice/BroadcastService;Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 0
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;
    .param p1, "x1"    # Ljava/io/OutputStream;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/bcservice/BroadcastService;->out:Ljava/io/OutputStream;

    return-object p1
.end method

.method static synthetic access$702(Lcom/sec/bcservice/BroadcastService;Ljava/io/DataOutputStream;)Ljava/io/DataOutputStream;
    .locals 0
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;
    .param p1, "x1"    # Ljava/io/DataOutputStream;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/bcservice/BroadcastService;->dos:Ljava/io/DataOutputStream;

    return-object p1
.end method

.method static synthetic access$802(Lcom/sec/bcservice/BroadcastService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;
    .param p1, "x1"    # Z

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/sec/bcservice/BroadcastService;->threadStatus:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/bcservice/BroadcastService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/bcservice/BroadcastService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/sec/bcservice/BroadcastService;->extractCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private changeSysScopeResult()V
    .locals 7

    .prologue
    const v6, 0x7f030004

    .line 618
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long v0, v2, v4

    .line 620
    .local v0, "ut":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 622
    const-wide/16 v0, 0x1

    .line 625
    :cond_0
    iget v2, p0, Lcom/sec/bcservice/BroadcastService;->sysscopeStatus:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    const-wide/16 v2, 0x78

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 626
    invoke-virtual {p0, v6}, Lcom/sec/bcservice/BroadcastService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/bcservice/BroadcastService;->sysscopeResult:Ljava/lang/String;

    .line 639
    :goto_0
    return-void

    .line 628
    :cond_1
    iget v2, p0, Lcom/sec/bcservice/BroadcastService;->sysscopeStatus:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 630
    const v2, 0x7f030003

    invoke-virtual {p0, v2}, Lcom/sec/bcservice/BroadcastService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/bcservice/BroadcastService;->sysscopeResult:Ljava/lang/String;

    goto :goto_0

    .line 631
    :cond_2
    iget v2, p0, Lcom/sec/bcservice/BroadcastService;->sysscopeStatus:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 633
    invoke-virtual {p0, v6}, Lcom/sec/bcservice/BroadcastService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/bcservice/BroadcastService;->sysscopeResult:Ljava/lang/String;

    goto :goto_0

    .line 636
    :cond_3
    const v2, 0x7f030005

    invoke-virtual {p0, v2}, Lcom/sec/bcservice/BroadcastService;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/bcservice/BroadcastService;->sysscopeResult:Ljava/lang/String;

    goto :goto_0
.end method

.method private extractCommand(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "receivedCmd"    # Ljava/lang/String;

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 593
    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 594
    .local v0, "recStr":Ljava/lang/String;
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v5, :cond_0

    .line 596
    const-string v1, "BCService"

    const-string v2, "There\'s space"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 598
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 599
    const-string v1, "BCService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "recStr is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    :cond_0
    const-string v1, "\r"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v5, :cond_1

    .line 603
    const-string v1, "BCService"

    const-string v2, "THere\'s rn"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    const-string v1, "\r"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 612
    :goto_0
    return-object v1

    .line 605
    :cond_1
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v5, :cond_2

    .line 607
    const-string v1, "BCService"

    const-string v2, "There\'s n"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 611
    :cond_2
    const-string v1, "BCService"

    const-string v2, "there\'s none of it"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getArgument(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 583
    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 585
    .local v0, "res":[Ljava/lang/String;
    return-object v0
.end method

.method private getNumApks()Ljava/lang/String;
    .locals 5

    .prologue
    .line 643
    const-string v2, "BCService"

    const-string v3, "getNumApks in"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    invoke-virtual {p0}, Lcom/sec/bcservice/BroadcastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 645
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/16 v2, 0x2200

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    .line 646
    .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    const-string v2, "BCService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getNumApks "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getSNoteFileCount(Ljava/lang/String;)I
    .locals 10
    .param p1, "szPath"    # Ljava/lang/String;

    .prologue
    .line 747
    const/4 v3, 0x0

    .line 748
    .local v3, "nTotalCount":I
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 749
    .local v4, "oFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 750
    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    .line 751
    .local v5, "oFileLists":[Ljava/io/File;
    move-object v0, v5

    .local v0, "arr$":[Ljava/io/File;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_3

    aget-object v6, v0, v1

    .line 752
    .local v6, "oList":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 753
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/bcservice/BroadcastService;->getSNoteFileCount(Ljava/lang/String;)I

    move-result v8

    add-int/2addr v3, v8

    .line 751
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 755
    :cond_1
    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    .line 756
    .local v7, "szName":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    const-string v9, ".snb"

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    const-string v9, ".spd"

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 757
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 761
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v5    # "oFileLists":[Ljava/io/File;
    .end local v6    # "oList":Ljava/io/File;
    .end local v7    # "szName":Ljava/lang/String;
    :cond_3
    return v3
.end method

.method private getSizeApks()Ljava/lang/String;
    .locals 9

    .prologue
    .line 652
    const-wide/16 v6, 0x0

    sput-wide v6, Lcom/sec/bcservice/BroadcastService;->msize:J

    .line 653
    new-instance v2, Lcom/sec/bcservice/BroadcastService$3;

    invoke-direct {v2, p0}, Lcom/sec/bcservice/BroadcastService$3;-><init>(Lcom/sec/bcservice/BroadcastService;)V

    .line 664
    .local v2, "mStatsObserver":Landroid/content/pm/IPackageStatsObserver$Stub;
    :try_start_0
    const-string v6, "package"

    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    move-result-object v5

    .line 667
    .local v5, "pm":Landroid/content/pm/IPackageManager;
    const/4 v6, 0x0

    invoke-virtual {p0, v6, v5}, Lcom/sec/bcservice/BroadcastService;->getInstalledPackages(ILandroid/content/pm/IPackageManager;)Ljava/util/List;

    move-result-object v4

    .line 668
    .local v4, "pkgList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    if-eqz v4, :cond_0

    .line 669
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageInfo;

    .line 670
    .local v3, "pkgInfo":Landroid/content/pm/PackageInfo;
    const-string v6, "BCService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "package list : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    iget-object v6, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iget v7, p0, Lcom/sec/bcservice/BroadcastService;->id:I

    invoke-interface {v5, v6, v7, v2}, Landroid/content/pm/IPackageManager;->getPackageSizeInfo(Ljava/lang/String;ILandroid/content/pm/IPackageStatsObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 674
    const-wide/16 v6, 0xf

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 675
    :catch_0
    move-exception v0

    .line 676
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v6, "BCService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 681
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "pkgInfo":Landroid/content/pm/PackageInfo;
    .end local v4    # "pkgList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v5    # "pm":Landroid/content/pm/IPackageManager;
    :catch_1
    move-exception v0

    .line 682
    .local v0, "e":Landroid/os/RemoteException;
    const-string v6, "BCService"

    const-string v7, "remote exception"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 683
    const-string v6, "0"

    .line 686
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    return-object v6

    .restart local v4    # "pkgList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .restart local v5    # "pm":Landroid/content/pm/IPackageManager;
    :cond_0
    sget-wide v6, Lcom/sec/bcservice/BroadcastService;->msize:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method

.method private initCommandMap()V
    .locals 3

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "SYSSCOPE"

    const-string v2, "AT+SYSSCOPE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1043
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "DRSDISCONNECT"

    const-string v2, "AT+DRSDISCONNECT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1044
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "CDCONT"

    const-string v2, "AT+CDCONT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1045
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "CDVOL"

    const-string v2, "AT+CDVOL"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1046
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "CGSN"

    const-string v2, "AT+CGSN"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1047
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "CKPD"

    const-string v2, "AT+CKPD"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1048
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "CNUM"

    const-string v2, "AT+CNUM"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1049
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "CIMI"

    const-string v2, "AT+CIMI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1050
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "ATI"

    const-string v2, "ATI1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1051
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "CFUN"

    const-string v2, "AT+CFUN"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1052
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "CGMR"

    const-string v2, "AT+CGMR"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1053
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "CGMI"

    const-string v2, "AT+CGMI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1054
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "FACTORST"

    const-string v2, "AT+FACTORST"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1055
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "USBDEBUG"

    const-string v2, "AT+USBDEBUG"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1056
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "SVCIFPGM"

    const-string v2, "AT+SVCIFPGM"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1057
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "USBMODEM"

    const-string v2, "AT+USBMODEM"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1058
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;

    const-string v1, "NSRI"

    const-string v2, "AT$NSRI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1059
    return-void
.end method

.method private process_action_event_svc_if_pgm()Ljava/lang/String;
    .locals 44

    .prologue
    .line 766
    invoke-virtual/range {p0 .. p0}, Lcom/sec/bcservice/BroadcastService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 768
    .local v2, "cr":Landroid/content/ContentResolver;
    const/16 v18, 0x0

    .line 769
    .local v18, "contactcount":I
    const/16 v28, 0x0

    .line 770
    .local v28, "imagecount":I
    const/16 v42, 0x0

    .line 771
    .local v42, "videocount":I
    const/16 v29, 0x0

    .line 772
    .local v29, "installedpakagecount":I
    const/16 v17, 0x0

    .line 773
    .local v17, "calendarcount":I
    const/16 v35, 0x0

    .line 776
    .local v35, "memocount":I
    const/16 v20, 0x0

    .line 778
    .local v20, "cursorContact":Landroid/database/Cursor;
    :try_start_0
    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x1

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "_id"

    aput-object v8, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 779
    if-eqz v20, :cond_0

    .line 780
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->getCount()I

    move-result v18

    .line 781
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 787
    :cond_0
    if-eqz v20, :cond_1

    .line 788
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 793
    :cond_1
    :goto_0
    const/16 v34, 0x0

    .line 794
    .local v34, "mediascanning":Z
    const/16 v22, 0x0

    .line 796
    .local v22, "cursorMedia":Landroid/database/Cursor;
    :try_start_1
    invoke-static {}, Landroid/provider/MediaStore;->getMediaScannerUri()Landroid/net/Uri;

    move-result-object v3

    const/4 v5, 0x1

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "volume"

    aput-object v8, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 799
    if-eqz v22, :cond_4

    .line 800
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->getCount()I

    move-result v5

    const/4 v8, 0x1

    if-ne v5, v8, :cond_3

    .line 801
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    .line 802
    const/4 v5, 0x0

    move-object/from16 v0, v22

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v43

    .line 803
    .local v43, "volumeName":Ljava/lang/String;
    const-string v5, "external"

    move-object/from16 v0, v43

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "internal"

    move-object/from16 v0, v43

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 804
    :cond_2
    const/16 v34, 0x1

    .line 806
    .end local v43    # "volumeName":Ljava/lang/String;
    :cond_3
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 812
    :cond_4
    if-eqz v22, :cond_5

    .line 813
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    .line 818
    :cond_5
    :goto_1
    if-nez v34, :cond_f

    .line 821
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 822
    .local v3, "uriImages":Landroid/net/Uri;
    const/4 v5, 0x2

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "bucket_id"

    aput-object v8, v4, v5

    const/4 v5, 0x1

    const-string v8, "bucket_display_name"

    aput-object v8, v4, v5

    .line 825
    .local v4, "BUCKET_PROJECTION_IMAGES":[Ljava/lang/String;
    const/16 v21, 0x0

    .line 827
    .local v21, "cursorImages":Landroid/database/Cursor;
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    :try_start_2
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 829
    if-eqz v21, :cond_6

    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_6

    .line 830
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->getCount()I

    move-result v28

    .line 831
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 837
    :cond_6
    if-eqz v21, :cond_7

    .line 838
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    .line 843
    :cond_7
    :goto_2
    sget-object v6, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 844
    .local v6, "uriVideos":Landroid/net/Uri;
    const/4 v5, 0x3

    new-array v7, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "bucket_id"

    aput-object v8, v7, v5

    const/4 v5, 0x1

    const-string v8, "bucket_display_name"

    aput-object v8, v7, v5

    const/4 v5, 0x2

    const-string v8, "_data"

    aput-object v8, v7, v5

    .line 848
    .local v7, "BUCKET_PROJECTION_VIDEOS":[Ljava/lang/String;
    const/16 v24, 0x0

    .line 850
    .local v24, "cursorVideos":Landroid/database/Cursor;
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v5, v2

    :try_start_3
    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    .line 852
    if-eqz v24, :cond_8

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_8

    .line 853
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->getCount()I

    move-result v42

    .line 854
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 860
    :cond_8
    if-eqz v24, :cond_9

    .line 861
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 871
    .end local v3    # "uriImages":Landroid/net/Uri;
    .end local v4    # "BUCKET_PROJECTION_IMAGES":[Ljava/lang/String;
    .end local v6    # "uriVideos":Landroid/net/Uri;
    .end local v7    # "BUCKET_PROJECTION_VIDEOS":[Ljava/lang/String;
    .end local v21    # "cursorImages":Landroid/database/Cursor;
    .end local v24    # "cursorVideos":Landroid/database/Cursor;
    :cond_9
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/bcservice/BroadcastService;->getApplication()Landroid/app/Application;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v37

    .line 872
    .local v37, "pm":Landroid/content/pm/PackageManager;
    if-eqz v37, :cond_11

    .line 873
    const/16 v5, 0x2000

    move-object/from16 v0, v37

    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v33

    .line 874
    .local v33, "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    const/16 v40, 0x0

    .line 875
    .local v40, "systempakagecount":I
    const/16 v36, 0x0

    .local v36, "n":I
    :goto_4
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v5

    move/from16 v0, v36

    if-ge v0, v5, :cond_10

    .line 877
    move-object/from16 v0, v33

    move/from16 v1, v36

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_a

    .line 879
    add-int/lit8 v40, v40, 0x1

    .line 875
    :cond_a
    add-int/lit8 v36, v36, 0x1

    goto :goto_4

    .line 783
    .end local v22    # "cursorMedia":Landroid/database/Cursor;
    .end local v33    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    .end local v34    # "mediascanning":Z
    .end local v36    # "n":I
    .end local v37    # "pm":Landroid/content/pm/PackageManager;
    .end local v40    # "systempakagecount":I
    :catch_0
    move-exception v25

    .line 785
    .local v25, "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 787
    if-eqz v20, :cond_1

    .line 788
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 787
    .end local v25    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    if-eqz v20, :cond_b

    .line 788
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v5

    .line 808
    .restart local v22    # "cursorMedia":Landroid/database/Cursor;
    .restart local v34    # "mediascanning":Z
    :catch_1
    move-exception v25

    .line 810
    .restart local v25    # "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 812
    if-eqz v22, :cond_5

    .line 813
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 812
    .end local v25    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v5

    if-eqz v22, :cond_c

    .line 813
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v5

    .line 833
    .restart local v3    # "uriImages":Landroid/net/Uri;
    .restart local v4    # "BUCKET_PROJECTION_IMAGES":[Ljava/lang/String;
    .restart local v21    # "cursorImages":Landroid/database/Cursor;
    :catch_2
    move-exception v25

    .line 835
    .restart local v25    # "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 837
    if-eqz v21, :cond_7

    .line 838
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 837
    .end local v25    # "e":Ljava/lang/Exception;
    :catchall_2
    move-exception v5

    if-eqz v21, :cond_d

    .line 838
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    :cond_d
    throw v5

    .line 856
    .restart local v6    # "uriVideos":Landroid/net/Uri;
    .restart local v7    # "BUCKET_PROJECTION_VIDEOS":[Ljava/lang/String;
    .restart local v24    # "cursorVideos":Landroid/database/Cursor;
    :catch_3
    move-exception v25

    .line 858
    .restart local v25    # "e":Ljava/lang/Exception;
    :try_start_7
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 860
    if-eqz v24, :cond_9

    .line 861
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 860
    .end local v25    # "e":Ljava/lang/Exception;
    :catchall_3
    move-exception v5

    if-eqz v24, :cond_e

    .line 861
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    :cond_e
    throw v5

    .line 865
    .end local v3    # "uriImages":Landroid/net/Uri;
    .end local v4    # "BUCKET_PROJECTION_IMAGES":[Ljava/lang/String;
    .end local v6    # "uriVideos":Landroid/net/Uri;
    .end local v7    # "BUCKET_PROJECTION_VIDEOS":[Ljava/lang/String;
    .end local v21    # "cursorImages":Landroid/database/Cursor;
    .end local v24    # "cursorVideos":Landroid/database/Cursor;
    :cond_f
    const/16 v28, -0x1

    .line 866
    const/16 v42, -0x1

    goto :goto_3

    .line 882
    .restart local v33    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    .restart local v36    # "n":I
    .restart local v37    # "pm":Landroid/content/pm/PackageManager;
    .restart local v40    # "systempakagecount":I
    :cond_10
    invoke-interface/range {v33 .. v33}, Ljava/util/List;->size()I

    move-result v5

    sub-int v29, v5, v40

    .line 885
    .end local v33    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    .end local v36    # "n":I
    .end local v40    # "systempakagecount":I
    :cond_11
    const/16 v19, 0x0

    .line 888
    .local v19, "cursorCalendar":Landroid/database/Cursor;
    :try_start_8
    const-string v5, "CHM"

    const-string v8, "ro.csc.sales_code"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    const-string v5, "CHN"

    const-string v8, "ro.csc.sales_code"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    const-string v5, "CHU"

    const-string v8, "ro.csc.sales_code"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    const-string v5, "CHC"

    const-string v8, "ro.csc.sales_code"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    const-string v5, "CTC"

    const-string v8, "ro.csc.sales_code"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_19

    .line 890
    :cond_12
    sget-object v9, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v10, 0x0

    const-string v11, "calendar_id != 2 AND calendar_id != 3 AND deleted = 0"

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v8, v2

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 896
    :goto_5
    if-eqz v19, :cond_13

    .line 897
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    move-result v17

    .line 903
    :cond_13
    if-eqz v19, :cond_14

    .line 904
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 908
    :cond_14
    :goto_6
    const/16 v32, 0x0

    .line 909
    .local v32, "isSnote":Z
    const/16 v31, 0x0

    .line 910
    .local v31, "isSmemo":Z
    const/16 v30, 0x0

    .line 911
    .local v30, "isMemo":Z
    if-eqz v37, :cond_17

    .line 913
    :try_start_9
    const-string v5, "com.sec.android.provider.snote"

    const/16 v8, 0x2000

    move-object/from16 v0, v37

    invoke-virtual {v0, v5, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_9
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_9 .. :try_end_9} :catch_e

    .line 914
    const/16 v32, 0x1

    .line 920
    :goto_7
    if-nez v32, :cond_15

    .line 922
    :try_start_a
    const-string v5, "com.samsung.android.snote"

    const/16 v8, 0x2000

    move-object/from16 v0, v37

    invoke-virtual {v0, v5, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v39

    .line 923
    .local v39, "sNote3":Landroid/content/pm/PackageInfo;
    move-object/from16 v0, v39

    iget v5, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    const/4 v8, 0x1

    if-ne v5, v8, :cond_1b

    const-string v5, "2.9.9"

    move-object/from16 v0, v39

    iget-object v8, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_a .. :try_end_a} :catch_d

    move-result v5

    if-eqz v5, :cond_1b

    .line 924
    const/16 v32, 0x0

    .line 932
    .end local v39    # "sNote3":Landroid/content/pm/PackageInfo;
    :cond_15
    :goto_8
    if-nez v32, :cond_16

    .line 934
    :try_start_b
    const-string v5, "com.sec.android.widgetapp.diotek.smemo"

    const/16 v8, 0x2000

    move-object/from16 v0, v37

    invoke-virtual {v0, v5, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v38

    .line 935
    .local v38, "sMemoPackage":Landroid/content/pm/PackageInfo;
    const-string v5, "com.sec.android.widgetapp.diotek.smemo"

    move-object/from16 v0, v38

    iget-object v8, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_b .. :try_end_b} :catch_5

    move-result v5

    if-eqz v5, :cond_16

    .line 936
    const/16 v31, 0x1

    .line 945
    .end local v38    # "sMemoPackage":Landroid/content/pm/PackageInfo;
    :cond_16
    :goto_9
    :try_start_c
    const-string v5, "com.samsung.android.app.memo"

    const/16 v8, 0x2000

    move-object/from16 v0, v37

    invoke-virtual {v0, v5, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_c .. :try_end_c} :catch_c

    .line 946
    const/16 v30, 0x1

    .line 953
    :cond_17
    :goto_a
    const/4 v5, 0x1

    move/from16 v0, v32

    if-ne v0, v5, :cond_1c

    .line 955
    const/16 v41, 0x0

    .line 958
    .local v41, "szPath":Ljava/lang/String;
    :try_start_d
    const-string v5, "com.sec.android.app.snotebook"

    const/16 v8, 0x2000

    move-object/from16 v0, v37

    invoke-virtual {v0, v5, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 959
    const-string v41, "/storage/sdcard0/S Note/"
    :try_end_d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_d .. :try_end_d} :catch_6

    .line 974
    :goto_b
    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-direct {v0, v1}, Lcom/sec/bcservice/BroadcastService;->getSNoteFileCount(Ljava/lang/String;)I

    move-result v35

    .line 1031
    .end local v41    # "szPath":Ljava/lang/String;
    :cond_18
    :goto_c
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v42

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v35

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "OK"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 894
    .end local v30    # "isMemo":Z
    .end local v31    # "isSmemo":Z
    .end local v32    # "isSnote":Z
    :cond_19
    :try_start_e
    sget-object v9, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const/4 v10, 0x0

    const-string v11, "calendar_id != 2 AND deleted = 0"

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v8, v2

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    move-result-object v19

    goto/16 :goto_5

    .line 899
    :catch_4
    move-exception v25

    .line 901
    .restart local v25    # "e":Ljava/lang/Exception;
    :try_start_f
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    .line 903
    if-eqz v19, :cond_14

    .line 904
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    goto/16 :goto_6

    .line 903
    .end local v25    # "e":Ljava/lang/Exception;
    :catchall_4
    move-exception v5

    if-eqz v19, :cond_1a

    .line 904
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    :cond_1a
    throw v5

    .line 926
    .restart local v30    # "isMemo":Z
    .restart local v31    # "isSmemo":Z
    .restart local v32    # "isSnote":Z
    .restart local v39    # "sNote3":Landroid/content/pm/PackageInfo;
    :cond_1b
    const/16 v32, 0x1

    goto/16 :goto_8

    .line 938
    .end local v39    # "sNote3":Landroid/content/pm/PackageInfo;
    :catch_5
    move-exception v25

    .line 939
    .local v25, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/16 v31, 0x0

    goto/16 :goto_9

    .line 960
    .end local v25    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v41    # "szPath":Ljava/lang/String;
    :catch_6
    move-exception v25

    .line 962
    .restart local v25    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_10
    const-string v5, "com.samsung.android.snote"

    const/16 v8, 0x2000

    move-object/from16 v0, v37

    invoke-virtual {v0, v5, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 963
    const-string v41, "/storage/sdcard0/SnoteData"
    :try_end_10
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_10 .. :try_end_10} :catch_7

    goto/16 :goto_b

    .line 964
    :catch_7
    move-exception v26

    .line 966
    .local v26, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_11
    const-string v5, "com.sec.android.widgetapp.diotek.smemo"

    const/16 v8, 0x2000

    move-object/from16 v0, v37

    invoke-virtual {v0, v5, v8}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    .line 967
    const-string v41, "/storage/sdcard0/SMemo/"
    :try_end_11
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_11 .. :try_end_11} :catch_8

    goto/16 :goto_b

    .line 968
    :catch_8
    move-exception v27

    .line 970
    .local v27, "e2":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/TMemo/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    goto/16 :goto_b

    .line 975
    .end local v25    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v26    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v27    # "e2":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v41    # "szPath":Ljava/lang/String;
    :cond_1c
    const/4 v5, 0x1

    move/from16 v0, v31

    if-ne v0, v5, :cond_1f

    .line 977
    const-string v16, "content://com.sec.android.widgetapp.q1_penmemo/PenMemo"

    .line 978
    .local v16, "CONTENT_URI_PATH":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 979
    .local v9, "CONTENT_URI_SMEMO":Landroid/net/Uri;
    const/16 v23, 0x0

    .line 981
    .local v23, "cursorMemo":Landroid/database/Cursor;
    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v8, v2

    :try_start_12
    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 982
    if-eqz v23, :cond_1d

    .line 983
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->getCount()I
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_9
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    move-result v35

    .line 989
    :cond_1d
    if-eqz v23, :cond_18

    .line 990
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    goto/16 :goto_c

    .line 985
    :catch_9
    move-exception v25

    .line 987
    .local v25, "e":Ljava/lang/Exception;
    :try_start_13
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_5

    .line 989
    if-eqz v23, :cond_18

    .line 990
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    goto/16 :goto_c

    .line 989
    .end local v25    # "e":Ljava/lang/Exception;
    :catchall_5
    move-exception v5

    if-eqz v23, :cond_1e

    .line 990
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    :cond_1e
    throw v5

    .line 994
    .end local v9    # "CONTENT_URI_SMEMO":Landroid/net/Uri;
    .end local v16    # "CONTENT_URI_PATH":Ljava/lang/String;
    .end local v23    # "cursorMemo":Landroid/database/Cursor;
    :cond_1f
    if-eqz v30, :cond_22

    .line 996
    const-string v16, "content://com.samsung.android.memo/memo"

    .line 997
    .restart local v16    # "CONTENT_URI_PATH":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 998
    .restart local v9    # "CONTENT_URI_SMEMO":Landroid/net/Uri;
    const/16 v23, 0x0

    .line 1000
    .restart local v23    # "cursorMemo":Landroid/database/Cursor;
    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v8, v2

    :try_start_14
    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 1001
    if-eqz v23, :cond_20

    .line 1002
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->getCount()I
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_a
    .catchall {:try_start_14 .. :try_end_14} :catchall_6

    move-result v35

    .line 1008
    :cond_20
    if-eqz v23, :cond_18

    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    goto/16 :goto_c

    .line 1004
    :catch_a
    move-exception v25

    .line 1006
    .restart local v25    # "e":Ljava/lang/Exception;
    :try_start_15
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_6

    .line 1008
    if-eqz v23, :cond_18

    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    goto/16 :goto_c

    .end local v25    # "e":Ljava/lang/Exception;
    :catchall_6
    move-exception v5

    if-eqz v23, :cond_21

    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    :cond_21
    throw v5

    .line 1013
    .end local v9    # "CONTENT_URI_SMEMO":Landroid/net/Uri;
    .end local v16    # "CONTENT_URI_PATH":Ljava/lang/String;
    .end local v23    # "cursorMemo":Landroid/database/Cursor;
    :cond_22
    const-string v16, "content://com.samsung.sec.android/memo"

    .line 1014
    .restart local v16    # "CONTENT_URI_PATH":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/all"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 1015
    .local v11, "CONTENT_URI_MEMO":Landroid/net/Uri;
    const/16 v23, 0x0

    .line 1017
    .restart local v23    # "cursorMemo":Landroid/database/Cursor;
    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v10, v2

    :try_start_16
    invoke-virtual/range {v10 .. v15}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v23

    .line 1018
    if-eqz v23, :cond_23

    .line 1019
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->getCount()I
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_b
    .catchall {:try_start_16 .. :try_end_16} :catchall_7

    move-result v35

    .line 1025
    :cond_23
    if-eqz v23, :cond_18

    .line 1026
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    goto/16 :goto_c

    .line 1021
    :catch_b
    move-exception v25

    .line 1023
    .restart local v25    # "e":Ljava/lang/Exception;
    :try_start_17
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_7

    .line 1025
    if-eqz v23, :cond_18

    .line 1026
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    goto/16 :goto_c

    .line 1025
    .end local v25    # "e":Ljava/lang/Exception;
    :catchall_7
    move-exception v5

    if-eqz v23, :cond_24

    .line 1026
    invoke-interface/range {v23 .. v23}, Landroid/database/Cursor;->close()V

    :cond_24
    throw v5

    .line 947
    .end local v11    # "CONTENT_URI_MEMO":Landroid/net/Uri;
    .end local v16    # "CONTENT_URI_PATH":Ljava/lang/String;
    .end local v23    # "cursorMemo":Landroid/database/Cursor;
    :catch_c
    move-exception v5

    goto/16 :goto_a

    .line 927
    :catch_d
    move-exception v5

    goto/16 :goto_8

    .line 915
    :catch_e
    move-exception v5

    goto/16 :goto_7
.end method


# virtual methods
.method public WriteToDR(Ljava/lang/String;)V
    .locals 5
    .param p1, "Message"    # Ljava/lang/String;

    .prologue
    .line 730
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 731
    .local v0, "byteTmp":[B
    const-string v2, "BCService"

    const-string v3, "WriteToDR"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    :try_start_0
    iget-object v2, p0, Lcom/sec/bcservice/BroadcastService;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 735
    const-string v2, "BCService"

    const-string v3, "Write OK"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 740
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/bcservice/BroadcastService;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 744
    :goto_1
    return-void

    .line 736
    :catch_0
    move-exception v1

    .line 737
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "BCService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 741
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 742
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v2, "BCService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception occurred: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getInstalledPackages(ILandroid/content/pm/IPackageManager;)Ljava/util/List;
    .locals 4
    .param p1, "flags"    # I
    .param p2, "pm"    # Landroid/content/pm/IPackageManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/pm/IPackageManager;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PackageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 692
    const/4 v2, 0x0

    :try_start_0
    invoke-interface {p2, p1, v2}, Landroid/content/pm/IPackageManager;->getInstalledPackages(II)Landroid/content/pm/ParceledListSlice;

    move-result-object v1

    .line 693
    .local v1, "slice":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/content/pm/PackageInfo;>;"
    if-eqz v1, :cond_0

    .line 694
    invoke-virtual {v1}, Landroid/content/pm/ParceledListSlice;->getList()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 696
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 697
    .end local v1    # "slice":Landroid/content/pm/ParceledListSlice;, "Landroid/content/pm/ParceledListSlice<Landroid/content/pm/PackageInfo;>;"
    :catch_0
    move-exception v0

    .line 698
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Package manager has died"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 196
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 132
    new-instance v0, Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-direct {v0, p0}, Lcom/samsung/android/sec_platform_library/FactoryPhone;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/bcservice/BroadcastService;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    .line 134
    invoke-direct {p0}, Lcom/sec/bcservice/BroadcastService;->initCommandMap()V

    .line 135
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;

    invoke-virtual {v0}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->disconnectFromRilService()V

    .line 204
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/bcservice/BroadcastService;->mIsRegistered:Z

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/bcservice/BroadcastService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 207
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/bcservice/BroadcastService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 208
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/bcservice/BroadcastService;->mIsRegistered:Z

    .line 212
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/sec/bcservice/BroadcastService;->socket:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    :goto_0
    return-void

    .line 213
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 140
    const-string v1, "BCService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "String #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-boolean v1, p0, Lcom/sec/bcservice/BroadcastService;->threadStatus:Z

    if-nez v1, :cond_0

    .line 144
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/bcservice/BroadcastService;->threadStatus:Z

    .line 145
    new-instance v0, Ljava/lang/Thread;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/bcservice/BroadcastService;->connectDRSocket:Ljava/lang/Runnable;

    const-string v3, "DRService"

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 146
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 149
    .end local v0    # "thread":Ljava/lang/Thread;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/bcservice/BroadcastService;->startReceiver()V

    .line 151
    const/4 v1, 0x0

    return v1
.end method

.method public startReceiver()V
    .locals 3

    .prologue
    .line 156
    const-string v1, "BCService"

    const-string v2, "startReceiver"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 158
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/sec/bcservice/BroadcastService;->USBATCOMMAND_RESPONSE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 160
    const-string v1, "com.sec.intent.action.SYSSCOPESTATUS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 161
    iget-object v1, p0, Lcom/sec/bcservice/BroadcastService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/bcservice/BroadcastService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 162
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/bcservice/BroadcastService;->mIsRegistered:Z

    .line 163
    return-void
.end method
