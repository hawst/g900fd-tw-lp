.class Lcom/sec/bcservice/BroadcastService$1;
.super Landroid/content/BroadcastReceiver;
.source "BroadcastService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/bcservice/BroadcastService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/bcservice/BroadcastService;


# direct methods
.method constructor <init>(Lcom/sec/bcservice/BroadcastService;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/bcservice/BroadcastService$1;->this$0:Lcom/sec/bcservice/BroadcastService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 170
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "action":Ljava/lang/String;
    const-string v2, "BCService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onReceive "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    iget-object v2, p0, Lcom/sec/bcservice/BroadcastService$1;->this$0:Lcom/sec/bcservice/BroadcastService;

    # getter for: Lcom/sec/bcservice/BroadcastService;->USBATCOMMAND_RESPONSE:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/bcservice/BroadcastService;->access$000(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 175
    const-string v2, "response"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 177
    .local v1, "result":Ljava/lang/String;
    const-string v2, "BCService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    if-eqz v1, :cond_0

    .line 180
    const-string v2, "\r\nOK\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 181
    const-string v2, "BCService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    iget-object v2, p0, Lcom/sec/bcservice/BroadcastService$1;->this$0:Lcom/sec/bcservice/BroadcastService;

    invoke-virtual {v2, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    .line 190
    .end local v1    # "result":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    const-string v2, "com.sec.intent.action.SYSSCOPESTATUS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 186
    iget-object v2, p0, Lcom/sec/bcservice/BroadcastService$1;->this$0:Lcom/sec/bcservice/BroadcastService;

    const-string v3, "Result"

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    # setter for: Lcom/sec/bcservice/BroadcastService;->sysscopeStatus:I
    invoke-static {v2, v3}, Lcom/sec/bcservice/BroadcastService;->access$102(Lcom/sec/bcservice/BroadcastService;I)I

    .line 187
    const-string v2, "BCService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sysscopeStatus: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/bcservice/BroadcastService$1;->this$0:Lcom/sec/bcservice/BroadcastService;

    # getter for: Lcom/sec/bcservice/BroadcastService;->sysscopeStatus:I
    invoke-static {v4}, Lcom/sec/bcservice/BroadcastService;->access$100(Lcom/sec/bcservice/BroadcastService;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget-object v2, p0, Lcom/sec/bcservice/BroadcastService$1;->this$0:Lcom/sec/bcservice/BroadcastService;

    # invokes: Lcom/sec/bcservice/BroadcastService;->changeSysScopeResult()V
    invoke-static {v2}, Lcom/sec/bcservice/BroadcastService;->access$200(Lcom/sec/bcservice/BroadcastService;)V

    goto :goto_0
.end method
