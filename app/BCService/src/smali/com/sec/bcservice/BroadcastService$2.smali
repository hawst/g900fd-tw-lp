.class Lcom/sec/bcservice/BroadcastService$2;
.super Ljava/lang/Object;
.source "BroadcastService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/bcservice/BroadcastService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/bcservice/BroadcastService;


# direct methods
.method constructor <init>(Lcom/sec/bcservice/BroadcastService;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 38

    .prologue
    .line 219
    const-string v25, ""

    .line 220
    .local v25, "receiveDrCmd":Ljava/lang/String;
    const-string v29, ""

    .line 221
    .local v29, "responseStr":Ljava/lang/String;
    const-string v5, ""

    .line 222
    .local v5, "DrParam":Ljava/lang/String;
    const/4 v7, 0x0

    .line 223
    .local v7, "arg":[Ljava/lang/String;
    const/16 v34, 0x8

    move/from16 v0, v34

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v34, 0x0

    const-string v35, "ip6tnl0"

    aput-object v35, v20, v34

    const/16 v34, 0x1

    const-string v35, "p2p0"

    aput-object v35, v20, v34

    const/16 v34, 0x2

    const-string v35, "rmnet_usb0"

    aput-object v35, v20, v34

    const/16 v34, 0x3

    const-string v35, "rmnet_usb1"

    aput-object v35, v20, v34

    const/16 v34, 0x4

    const-string v35, "rmnet_usb2"

    aput-object v35, v20, v34

    const/16 v34, 0x5

    const-string v35, "sit0"

    aput-object v35, v20, v34

    const/16 v34, 0x6

    const-string v35, "vip0"

    aput-object v35, v20, v34

    const/16 v34, 0x7

    const-string v35, "wlan0"

    aput-object v35, v20, v34

    .line 224
    .local v20, "iface":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v28

    .line 226
    .local v28, "resolver":Landroid/content/ContentResolver;
    const/16 v24, 0x0

    .line 229
    .local v24, "readSize":I
    const-string v9, "/system/bin/atparser"

    .line 230
    .local v9, "atpPath":Ljava/lang/String;
    const-string v11, "/system/bin/atxd"

    .line 231
    .local v11, "atxdPath":Ljava/lang/String;
    const/16 v33, 0x0

    .line 233
    .local v33, "useBcomAtparser":Z
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 234
    .local v8, "atpFile":Ljava/io/File;
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 235
    .local v10, "atxdFile":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v34

    if-nez v34, :cond_0

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v34

    if-eqz v34, :cond_1

    .line 237
    :cond_0
    const/16 v33, 0x1

    .line 241
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    new-instance v35, Landroid/net/LocalSocket;

    invoke-direct/range {v35 .. v35}, Landroid/net/LocalSocket;-><init>()V

    # setter for: Lcom/sec/bcservice/BroadcastService;->socket:Landroid/net/LocalSocket;
    invoke-static/range {v34 .. v35}, Lcom/sec/bcservice/BroadcastService;->access$302(Lcom/sec/bcservice/BroadcastService;Landroid/net/LocalSocket;)Landroid/net/LocalSocket;

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->socket:Landroid/net/LocalSocket;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$300(Lcom/sec/bcservice/BroadcastService;)Landroid/net/LocalSocket;

    move-result-object v34

    new-instance v35, Landroid/net/LocalSocketAddress;

    const-string v36, "/data/.consocket_stream"

    sget-object v37, Landroid/net/LocalSocketAddress$Namespace;->FILESYSTEM:Landroid/net/LocalSocketAddress$Namespace;

    invoke-direct/range {v35 .. v37}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    invoke-virtual/range {v34 .. v35}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 243
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "Socket connected as "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v36, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->socket:Landroid/net/LocalSocket;
    invoke-static/range {v36 .. v36}, Lcom/sec/bcservice/BroadcastService;->access$300(Lcom/sec/bcservice/BroadcastService;)Landroid/net/LocalSocket;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v35, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->socket:Landroid/net/LocalSocket;
    invoke-static/range {v35 .. v35}, Lcom/sec/bcservice/BroadcastService;->access$300(Lcom/sec/bcservice/BroadcastService;)Landroid/net/LocalSocket;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v35

    # setter for: Lcom/sec/bcservice/BroadcastService;->in:Ljava/io/InputStream;
    invoke-static/range {v34 .. v35}, Lcom/sec/bcservice/BroadcastService;->access$402(Lcom/sec/bcservice/BroadcastService;Ljava/io/InputStream;)Ljava/io/InputStream;

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    new-instance v35, Ljava/io/DataInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v36, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->in:Ljava/io/InputStream;
    invoke-static/range {v36 .. v36}, Lcom/sec/bcservice/BroadcastService;->access$400(Lcom/sec/bcservice/BroadcastService;)Ljava/io/InputStream;

    move-result-object v36

    invoke-direct/range {v35 .. v36}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    # setter for: Lcom/sec/bcservice/BroadcastService;->dis:Ljava/io/DataInputStream;
    invoke-static/range {v34 .. v35}, Lcom/sec/bcservice/BroadcastService;->access$502(Lcom/sec/bcservice/BroadcastService;Ljava/io/DataInputStream;)Ljava/io/DataInputStream;

    .line 248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v35, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->socket:Landroid/net/LocalSocket;
    invoke-static/range {v35 .. v35}, Lcom/sec/bcservice/BroadcastService;->access$300(Lcom/sec/bcservice/BroadcastService;)Landroid/net/LocalSocket;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v35

    # setter for: Lcom/sec/bcservice/BroadcastService;->out:Ljava/io/OutputStream;
    invoke-static/range {v34 .. v35}, Lcom/sec/bcservice/BroadcastService;->access$602(Lcom/sec/bcservice/BroadcastService;Ljava/io/OutputStream;)Ljava/io/OutputStream;

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    new-instance v35, Ljava/io/DataOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v36, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->out:Ljava/io/OutputStream;
    invoke-static/range {v36 .. v36}, Lcom/sec/bcservice/BroadcastService;->access$600(Lcom/sec/bcservice/BroadcastService;)Ljava/io/OutputStream;

    move-result-object v36

    invoke-direct/range {v35 .. v36}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    # setter for: Lcom/sec/bcservice/BroadcastService;->dos:Ljava/io/DataOutputStream;
    invoke-static/range {v34 .. v35}, Lcom/sec/bcservice/BroadcastService;->access$702(Lcom/sec/bcservice/BroadcastService;Ljava/io/DataOutputStream;)Ljava/io/DataOutputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v26, v25

    .line 253
    .end local v25    # "receiveDrCmd":Ljava/lang/String;
    .local v26, "receiveDrCmd":Ljava/lang/String;
    :cond_2
    :goto_0
    const/16 v34, 0x100

    :try_start_1
    move/from16 v0, v34

    new-array v12, v0, [B

    .line 255
    .local v12, "byteMsg":[B
    const-string v34, "BCService"

    const-string v35, "before din.read()"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->dis:Ljava/io/DataInputStream;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$500(Lcom/sec/bcservice/BroadcastService;)Ljava/io/DataInputStream;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v12}, Ljava/io/DataInputStream;->read([B)I

    move-result v24

    .line 257
    const-string v34, "BCService"

    const-string v35, "after din.read()"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    const/16 v34, -0x1

    move/from16 v0, v24

    move/from16 v1, v34

    if-ne v0, v1, :cond_3

    .line 261
    const-string v34, "BCService"

    const-string v35, "din.read() error"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->stopSelf()V

    .line 263
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    const/16 v35, 0x1

    # setter for: Lcom/sec/bcservice/BroadcastService;->threadStatus:Z
    invoke-static/range {v34 .. v35}, Lcom/sec/bcservice/BroadcastService;->access$802(Lcom/sec/bcservice/BroadcastService;Z)Z

    .line 266
    :cond_3
    if-eqz v24, :cond_2

    .line 270
    new-instance v25, Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-direct {v0, v12}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 272
    .end local v26    # "receiveDrCmd":Ljava/lang/String;
    .restart local v25    # "receiveDrCmd":Ljava/lang/String;
    :try_start_2
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "receive DR cmd: "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, "<END>"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v25

    # invokes: Lcom/sec/bcservice/BroadcastService;->extractCommand(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/bcservice/BroadcastService;->access$900(Lcom/sec/bcservice/BroadcastService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 278
    .local v30, "splitCmd":Ljava/lang/String;
    new-instance v4, Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 279
    .local v4, "DrAtCmd":Ljava/lang/String;
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v35, Ljava/lang/String;

    move-object/from16 v0, v35

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, "\r\n"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 280
    .local v17, "echoCmd":Ljava/lang/String;
    const-string v34, " "

    move-object/from16 v0, v25

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v34

    const/16 v35, 0x1

    aget-object v27, v34, v35

    .line 281
    .local v27, "recvCmd":Ljava/lang/String;
    if-eqz v33, :cond_4

    .line 283
    const-string v17, ""

    .line 286
    :cond_4
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "splitCmd = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, "<END>"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    const-string v34, "="

    move-object/from16 v0, v30

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v34

    const/16 v35, -0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-eq v0, v1, :cond_5

    .line 290
    const-string v34, "="

    move-object/from16 v0, v30

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v34

    add-int/lit8 v34, v34, 0x1

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v35

    move-object/from16 v0, v30

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    const-string v35, "="

    move-object/from16 v0, v30

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v35

    add-int/lit8 v35, v35, 0x1

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v36

    move-object/from16 v0, v30

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v35

    # invokes: Lcom/sec/bcservice/BroadcastService;->getArgument(Ljava/lang/String;)[Ljava/lang/String;
    invoke-static/range {v34 .. v35}, Lcom/sec/bcservice/BroadcastService;->access$1000(Lcom/sec/bcservice/BroadcastService;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 292
    const/16 v34, 0x0

    const-string v35, "="

    move-object/from16 v0, v30

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v35

    move-object/from16 v0, v30

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v30

    .line 294
    :cond_5
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "SplitCmd is "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;

    move-result-object v34

    const-string v35, "SYSSCOPE"

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_8

    .line 298
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const/16 v35, 0x2

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v36

    move-object/from16 v0, v30

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 299
    if-eqz v7, :cond_6

    const/16 v34, 0x0

    aget-object v34, v7, v34

    if-eqz v34, :cond_6

    .line 301
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, ":"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const/16 v35, 0x0

    aget-object v35, v7, v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, ","

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 303
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->sysscopeResult:Ljava/lang/String;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1200(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 304
    const-string v34, "\r\nOK\r\n"

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 306
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "result to send = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    :cond_7
    :goto_1
    move-object/from16 v26, v25

    .line 550
    .end local v25    # "receiveDrCmd":Ljava/lang/String;
    .restart local v26    # "receiveDrCmd":Ljava/lang/String;
    goto/16 :goto_0

    .line 309
    .end local v26    # "receiveDrCmd":Ljava/lang/String;
    .restart local v25    # "receiveDrCmd":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;

    move-result-object v34

    const-string v35, "DRSDISCONNECT"

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_9

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->stopSelf()V

    .line 312
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    const/16 v35, 0x0

    # setter for: Lcom/sec/bcservice/BroadcastService;->threadStatus:Z
    invoke-static/range {v34 .. v35}, Lcom/sec/bcservice/BroadcastService;->access$802(Lcom/sec/bcservice/BroadcastService;Z)Z

    .line 313
    const/16 v34, 0x2

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v35

    move-object/from16 v0, v30

    move/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v29

    .line 314
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "result to send = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    const-string v34, "\r\nOK\r\n"

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 316
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 551
    .end local v4    # "DrAtCmd":Ljava/lang/String;
    .end local v12    # "byteMsg":[B
    .end local v17    # "echoCmd":Ljava/lang/String;
    .end local v27    # "recvCmd":Ljava/lang/String;
    .end local v30    # "splitCmd":Ljava/lang/String;
    :catch_0
    move-exception v16

    .line 552
    .local v16, "e":Ljava/lang/Exception;
    :goto_2
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "Exception occurred: "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    return-void

    .line 318
    .end local v16    # "e":Ljava/lang/Exception;
    .restart local v4    # "DrAtCmd":Ljava/lang/String;
    .restart local v12    # "byteMsg":[B
    .restart local v17    # "echoCmd":Ljava/lang/String;
    .restart local v27    # "recvCmd":Ljava/lang/String;
    .restart local v30    # "splitCmd":Ljava/lang/String;
    :cond_9
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;

    move-result-object v34

    const-string v35, "CDCONT"

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_c

    .line 320
    const-string v34, "BCService"

    const-string v35, ">> CDCONT= "

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->PARAM_CDCONT_GANR:Ljava/lang/String;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1300(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_a

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # invokes: Lcom/sec/bcservice/BroadcastService;->getNumApks()Ljava/lang/String;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1400(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;

    move-result-object v29

    .line 325
    const-string v34, "\r\nOK\r\n"

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    .line 327
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, ">> result to send = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 330
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->PARAM_CDCONT_GASZ:Ljava/lang/String;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1500(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_b

    .line 332
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "getSizeApks: "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # invokes: Lcom/sec/bcservice/BroadcastService;->getSizeApks()Ljava/lang/String;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1600(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;

    move-result-object v29

    .line 334
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, ">> getSizeApks: "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    const-string v34, "\r\nOK\r\n"

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 336
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 340
    :cond_b
    new-instance v21, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->USBATCOMMAND_REQUEST:Ljava/lang/String;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1700(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v21

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 341
    .local v21, "intent":Landroid/content/Intent;
    const-string v34, "command"

    move-object/from16 v0, v21

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 342
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "command:"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->sendBroadcast(Landroid/content/Intent;)V

    .line 344
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "sendBroadcast intent "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v36, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->USBATCOMMAND_REQUEST:Ljava/lang/String;
    invoke-static/range {v36 .. v36}, Lcom/sec/bcservice/BroadcastService;->access$1700(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 347
    .end local v21    # "intent":Landroid/content/Intent;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;

    move-result-object v34

    const-string v35, "CKPD"

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_d

    .line 349
    const-string v34, "BCService"

    const-string v35, ">> CKPD= "

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "DrParam is "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    new-instance v15, Lcom/sec/bcservice/CommandCKPD;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-direct {v15, v0}, Lcom/sec/bcservice/CommandCKPD;-><init>(Landroid/content/Context;)V

    .line 354
    .local v15, "cmdCKPD":Lcom/sec/bcservice/CommandCKPD;
    invoke-virtual {v15, v5}, Lcom/sec/bcservice/CommandCKPD;->processCmd(Ljava/lang/String;)V

    .line 356
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const/16 v35, 0x2

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v36

    move-object/from16 v0, v30

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 357
    const-string v34, "\r\nOK\r\n"

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 358
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "result to send = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 361
    .end local v15    # "cmdCKPD":Lcom/sec/bcservice/CommandCKPD;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;

    move-result-object v34

    const-string v35, "CDVOL"

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_e

    .line 363
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, ">>CDVOL "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    new-instance v14, Lcom/sec/bcservice/CommandCDVOL;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-direct {v14, v0}, Lcom/sec/bcservice/CommandCDVOL;-><init>(Landroid/content/Context;)V

    .line 367
    .local v14, "cmdCDVOL":Lcom/sec/bcservice/CommandCDVOL;
    invoke-virtual {v14, v5}, Lcom/sec/bcservice/CommandCDVOL;->processCmd(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 368
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "CDVOL response:"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    const-string v34, "\r\nOK\r\n"

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 371
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 373
    .end local v14    # "cmdCDVOL":Lcom/sec/bcservice/CommandCDVOL;
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;

    move-result-object v34

    const-string v35, "ATI"

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_f

    .line 375
    const-string v34, "BCService"

    const-string v35, ">>ATI :"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    # getter for: Lcom/sec/bcservice/BroadcastService;->SWVER:Ljava/lang/String;
    invoke-static {}, Lcom/sec/bcservice/BroadcastService;->access$1800()Ljava/lang/String;

    move-result-object v29

    .line 378
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "ATI response:"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    const-string v34, "\r\nOK\r\n"

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 380
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 383
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;

    move-result-object v34

    const-string v35, "CFUN"

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_11

    .line 385
    const-string v34, "BCService"

    const-string v35, "AT+CFUN"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Lcom/sec/bcservice/BroadcastService;->getApplicationContext()Landroid/content/Context;

    const-string v35, "power"

    invoke-virtual/range {v34 .. v35}, Lcom/sec/bcservice/BroadcastService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/os/PowerManager;

    .line 388
    .local v23, "pm":Landroid/os/PowerManager;
    if-eqz v23, :cond_10

    .line 390
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    const-string v35, "\r\nOK\r\n"

    invoke-virtual/range {v34 .. v35}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    .line 391
    const-string v34, ""

    move-object/from16 v0, v23

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->reboot(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 394
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    const-string v35, "\r\nERROR\r\n"

    invoke-virtual/range {v34 .. v35}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 397
    .end local v23    # "pm":Landroid/os/PowerManager;
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;

    move-result-object v34

    const-string v35, "CGMR"

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_12

    .line 399
    const-string v34, "BCService"

    const-string v35, "AT+CGMR"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v35, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->CGMR:Ljava/lang/String;
    invoke-static/range {v35 .. v35}, Lcom/sec/bcservice/BroadcastService;->access$1900(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 402
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "CGMR response: "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    const-string v34, "\r\nOK\r\n"

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 406
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;

    move-result-object v34

    const-string v35, "CGMI"

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_13

    .line 408
    const-string v34, "BCService"

    const-string v35, "AT+CGMI"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v35, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->CGMI:Ljava/lang/String;
    invoke-static/range {v35 .. v35}, Lcom/sec/bcservice/BroadcastService;->access$2000(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 411
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "CGMI response: "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    const-string v34, "\r\nOK\r\n"

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 413
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 415
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;

    move-result-object v34

    const-string v35, "FACTORST"

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_14

    .line 417
    const-string v34, "BCService"

    const-string v35, "AT+FACTORST"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    const-string v34, "BCService"

    const-string v35, "SEC_PRODUCT_FEATURE_RIL_SUPPORT_BATTARY_FACTORYRESET is false"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 430
    :try_start_4
    new-instance v19, Landroid/content/Intent;

    const-string v34, "android.intent.action.MAIN"

    move-object/from16 v0, v19

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 431
    .local v19, "i":Landroid/content/Intent;
    const-string v34, "com.sec.factory"

    const-string v35, "com.sec.factory.sysdump.FactoryReset"

    move-object/from16 v0, v19

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 433
    const/high16 v34, 0x14000000

    move-object/from16 v0, v19

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 434
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    invoke-virtual/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->getApplicationContext()Landroid/content/Context;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_4
    .catch Landroid/content/ActivityNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 444
    .end local v19    # "i":Landroid/content/Intent;
    :goto_3
    :try_start_5
    const-string v29, "+FACTORST:0"

    .line 445
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "FACTORST response: "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    const-string v34, "\r\nOK\r\n"

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 447
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 436
    :catch_1
    move-exception v6

    .line 438
    .local v6, "anfe":Landroid/content/ActivityNotFoundException;
    const-string v34, "BCService"

    const-string v35, "FTP Activity Not Found Exception"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    new-instance v18, Landroid/content/Intent;

    const-string v34, "android.intent.action.SEC_FACTORY_RESET"

    move-object/from16 v0, v18

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 441
    .local v18, "factorst":Landroid/content/Intent;
    const-string v34, "factory"

    const/16 v35, 0x1

    move-object/from16 v0, v18

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 442
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_3

    .line 449
    .end local v6    # "anfe":Landroid/content/ActivityNotFoundException;
    .end local v18    # "factorst":Landroid/content/Intent;
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;

    move-result-object v34

    const-string v35, "USBDEBUG"

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_16

    .line 451
    const-string v34, "BCService"

    const-string v35, "AT+USBDEBUG"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    if-eqz v7, :cond_7

    const/16 v34, 0x0

    aget-object v34, v7, v34

    if-eqz v34, :cond_7

    .line 453
    const/16 v34, 0x0

    aget-object v34, v7, v34

    const-string v35, "1"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_15

    .line 455
    const-string v34, "BCService"

    const-string v35, "Enable USBDEBUG"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    const-string v34, "persist.sys.auto_confirm"

    const-string v35, "1"

    invoke-static/range {v34 .. v35}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    const-string v34, "adb_enabled"

    const/16 v35, 0x1

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 465
    :goto_4
    const-string v29, "\r\nOK\r\n"

    .line 466
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "USBDEBUG response: "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 461
    :cond_15
    const-string v34, "BCService"

    const-string v35, "Disable USBDEBUG"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    const-string v34, "persist.sys.auto_confirm"

    const-string v35, "0"

    invoke-static/range {v34 .. v35}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    const-string v34, "adb_enabled"

    const/16 v35, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_4

    .line 470
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;

    move-result-object v34

    const-string v35, "USBMODEM"

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_19

    .line 472
    const-string v34, "BCService"

    const-string v35, "AT+USBMODEM"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Lcom/sec/bcservice/BroadcastService;->getApplicationContext()Landroid/content/Context;

    const-string v35, "usb"

    invoke-virtual/range {v34 .. v35}, Lcom/sec/bcservice/BroadcastService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Landroid/hardware/usb/UsbManager;

    .line 475
    .local v32, "usbm":Landroid/hardware/usb/UsbManager;
    if-nez v32, :cond_17

    .line 477
    const-string v29, "\r\nERROR\r\n"

    .line 478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 483
    :cond_17
    const-string v29, "\r\nOK\r\n"

    .line 484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    .line 486
    const-wide/16 v34, 0xdac

    invoke-static/range {v34 .. v35}, Ljava/lang/Thread;->sleep(J)V

    .line 488
    if-eqz v7, :cond_7

    const/16 v34, 0x0

    aget-object v34, v7, v34

    if-eqz v34, :cond_7

    .line 489
    const/16 v34, 0x0

    aget-object v34, v7, v34

    const-string v35, "1"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-eqz v34, :cond_18

    .line 491
    const-string v34, "BCService"

    const-string v35, "Enable USB DM"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    const-string v34, "persist.sys.auto_confirm"

    const-string v35, "0"

    invoke-static/range {v34 .. v35}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    const-string v34, "adb_enabled"

    const/16 v35, 0x1

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 494
    const-string v34, "diag,acm,adb"

    const/16 v35, 0x1

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 498
    :cond_18
    const-string v34, "BCService"

    const-string v35, "Disable USB DM"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    const-string v34, "persist.sys.auto_confirm"

    const-string v35, "0"

    invoke-static/range {v34 .. v35}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    const-string v34, "adb_enabled"

    const/16 v35, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 501
    const-string v34, "mtp"

    const/16 v35, 0x1

    move-object/from16 v0, v32

    move-object/from16 v1, v34

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbManager;->setCurrentFunction(Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 506
    .end local v32    # "usbm":Landroid/hardware/usb/UsbManager;
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;

    move-result-object v34

    const-string v35, "SVCIFPGM"

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_1a

    .line 508
    const-string v34, "BCService"

    const-string v35, "AT+SVCIFPGM=1,2"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const/16 v35, 0x2

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v36

    move-object/from16 v0, v30

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 510
    const-string v34, ":1,2,"

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 511
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # invokes: Lcom/sec/bcservice/BroadcastService;->process_action_event_svc_if_pgm()Ljava/lang/String;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$2100(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 512
    const-string v34, "\r\nOK\r\n"

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 513
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "SVCIFPGM: "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->WriteToDR(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 516
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->commandMap:Ljava/util/HashMap;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1100(Lcom/sec/bcservice/BroadcastService;)Ljava/util/HashMap;

    move-result-object v34

    const-string v35, "NSRI"

    invoke-virtual/range {v34 .. v35}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Ljava/lang/String;

    move-object/from16 v0, v34

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_1c

    .line 518
    const-string v34, "BCService"

    const-string v35, "AT$NSRI"

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    const/16 v22, 0x0

    .line 521
    .local v22, "len":I
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->getBytes()[B

    move-result-object v31

    .line 523
    .local v31, "tmpStr":[B
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v34

    add-int/lit8 v34, v34, 0x4

    add-int/lit8 v22, v34, 0x1

    .line 525
    move/from16 v0, v22

    new-array v13, v0, [B

    .line 526
    .local v13, "cmd":[B
    const/16 v34, 0x0

    const/16 v35, 0x16

    aput-byte v35, v13, v34

    .line 527
    const/16 v34, 0x1

    const/16 v35, 0x60

    aput-byte v35, v13, v34

    .line 528
    const/16 v34, 0x2

    shr-int/lit8 v35, v22, 0x8

    move/from16 v0, v35

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-byte v0, v0

    move/from16 v35, v0

    aput-byte v35, v13, v34

    .line 529
    const/16 v34, 0x3

    move/from16 v0, v22

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    move/from16 v0, v35

    int-to-byte v0, v0

    move/from16 v35, v0

    aput-byte v35, v13, v34

    .line 531
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_5
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v34

    move/from16 v0, v19

    move/from16 v1, v34

    if-ge v0, v1, :cond_1b

    .line 532
    add-int/lit8 v34, v19, 0x4

    aget-byte v35, v31, v19

    aput-byte v35, v13, v34

    .line 531
    add-int/lit8 v19, v19, 0x1

    goto :goto_5

    .line 534
    :cond_1b
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "cmd = "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->mFactoryPhone:Lcom/samsung/android/sec_platform_library/FactoryPhone;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$2200(Lcom/sec/bcservice/BroadcastService;)Lcom/samsung/android/sec_platform_library/FactoryPhone;

    move-result-object v34

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-virtual {v0, v13, v1}, Lcom/samsung/android/sec_platform_library/FactoryPhone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    goto/16 :goto_1

    .line 541
    .end local v13    # "cmd":[B
    .end local v19    # "i":I
    .end local v22    # "len":I
    .end local v31    # "tmpStr":[B
    :cond_1c
    new-instance v21, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->USBATCOMMAND_REQUEST:Ljava/lang/String;
    invoke-static/range {v34 .. v34}, Lcom/sec/bcservice/BroadcastService;->access$1700(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v21

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 542
    .restart local v21    # "intent":Landroid/content/Intent;
    const-string v34, "command"

    move-object/from16 v0, v21

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 543
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "command:"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/bcservice/BroadcastService;->sendBroadcast(Landroid/content/Intent;)V

    .line 546
    const-string v34, "BCService"

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "sendBroadcast intent "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/bcservice/BroadcastService$2;->this$0:Lcom/sec/bcservice/BroadcastService;

    move-object/from16 v36, v0

    # getter for: Lcom/sec/bcservice/BroadcastService;->USBATCOMMAND_REQUEST:Ljava/lang/String;
    invoke-static/range {v36 .. v36}, Lcom/sec/bcservice/BroadcastService;->access$1700(Lcom/sec/bcservice/BroadcastService;)Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_1

    .line 551
    .end local v4    # "DrAtCmd":Ljava/lang/String;
    .end local v12    # "byteMsg":[B
    .end local v17    # "echoCmd":Ljava/lang/String;
    .end local v21    # "intent":Landroid/content/Intent;
    .end local v25    # "receiveDrCmd":Ljava/lang/String;
    .end local v27    # "recvCmd":Ljava/lang/String;
    .end local v30    # "splitCmd":Ljava/lang/String;
    .restart local v26    # "receiveDrCmd":Ljava/lang/String;
    :catch_2
    move-exception v16

    move-object/from16 v25, v26

    .end local v26    # "receiveDrCmd":Ljava/lang/String;
    .restart local v25    # "receiveDrCmd":Ljava/lang/String;
    goto/16 :goto_2
.end method
