.class public Lcom/sec/bcservice/CommandCDVOL;
.super Ljava/lang/Object;
.source "CommandCDVOL.java"


# instance fields
.field private final PARAM_CDVOL_LSRX:Ljava/lang/String;

.field private final PARAM_CDVOL_LSTX:Ljava/lang/String;

.field private final PARAM_CDVOL_RX:Ljava/lang/String;

.field private final PARAM_CDVOL_TX:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private statsService:Landroid/net/INetworkStatsService;

.field private statsSession:Landroid/net/INetworkStatsSession;

.field private final strError:Ljava/lang/String;

.field private final strNA:Ljava/lang/String;

.field private subscriberId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-string v2, "BCService"

    iput-object v2, p0, Lcom/sec/bcservice/CommandCDVOL;->TAG:Ljava/lang/String;

    .line 17
    iput-object v3, p0, Lcom/sec/bcservice/CommandCDVOL;->statsService:Landroid/net/INetworkStatsService;

    .line 18
    iput-object v3, p0, Lcom/sec/bcservice/CommandCDVOL;->statsSession:Landroid/net/INetworkStatsSession;

    .line 22
    const-string v2, "TX"

    iput-object v2, p0, Lcom/sec/bcservice/CommandCDVOL;->PARAM_CDVOL_TX:Ljava/lang/String;

    .line 23
    const-string v2, "RX"

    iput-object v2, p0, Lcom/sec/bcservice/CommandCDVOL;->PARAM_CDVOL_RX:Ljava/lang/String;

    .line 24
    const-string v2, "LS,TX"

    iput-object v2, p0, Lcom/sec/bcservice/CommandCDVOL;->PARAM_CDVOL_LSTX:Ljava/lang/String;

    .line 25
    const-string v2, "LS,RX"

    iput-object v2, p0, Lcom/sec/bcservice/CommandCDVOL;->PARAM_CDVOL_LSRX:Ljava/lang/String;

    .line 27
    const-string v2, "ERROR"

    iput-object v2, p0, Lcom/sec/bcservice/CommandCDVOL;->strError:Ljava/lang/String;

    .line 28
    const-string v2, "N/A"

    iput-object v2, p0, Lcom/sec/bcservice/CommandCDVOL;->strNA:Ljava/lang/String;

    .line 30
    iput-object v3, p0, Lcom/sec/bcservice/CommandCDVOL;->subscriberId:Ljava/lang/String;

    .line 34
    iput-object p1, p0, Lcom/sec/bcservice/CommandCDVOL;->context:Landroid/content/Context;

    .line 37
    :try_start_0
    iget-object v2, p0, Lcom/sec/bcservice/CommandCDVOL;->statsService:Landroid/net/INetworkStatsService;

    if-nez v2, :cond_0

    .line 39
    const-string v2, "netstats"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/net/INetworkStatsService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetworkStatsService;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/bcservice/CommandCDVOL;->statsService:Landroid/net/INetworkStatsService;

    .line 42
    :cond_0
    iget-object v2, p0, Lcom/sec/bcservice/CommandCDVOL;->statsService:Landroid/net/INetworkStatsService;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/bcservice/CommandCDVOL;->statsSession:Landroid/net/INetworkStatsSession;

    if-nez v2, :cond_1

    .line 44
    iget-object v2, p0, Lcom/sec/bcservice/CommandCDVOL;->statsService:Landroid/net/INetworkStatsService;

    invoke-interface {v2}, Landroid/net/INetworkStatsService;->openSession()Landroid/net/INetworkStatsSession;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/bcservice/CommandCDVOL;->statsSession:Landroid/net/INetworkStatsSession;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :cond_1
    :goto_0
    const-string v2, "phone"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 50
    .local v1, "telephonyMgr":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/bcservice/CommandCDVOL;->subscriberId:Ljava/lang/String;

    .line 51
    return-void

    .line 46
    .end local v1    # "telephonyMgr":Landroid/telephony/TelephonyManager;
    :catch_0
    move-exception v0

    .line 47
    .local v0, "ex":Landroid/os/RemoteException;
    const-string v2, "BCService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RemoteException occurred: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public processCmd(Ljava/lang/String;)Ljava/lang/String;
    .locals 18
    .param p1, "param"    # Ljava/lang/String;

    .prologue
    .line 55
    const-wide/16 v12, 0x0

    .line 56
    .local v12, "totalPackets":J
    const-wide/16 v16, 0x0

    .line 57
    .local v16, "wifiPackets":J
    const-wide/16 v10, 0x0

    .line 59
    .local v10, "mobilePackets":J
    if-nez p1, :cond_0

    .line 61
    const-string v2, "ERROR"

    .line 115
    :goto_0
    return-object v2

    .line 64
    :cond_0
    const-string v2, "TX"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 67
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/bcservice/CommandCDVOL;->statsService:Landroid/net/INetworkStatsService;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/bcservice/CommandCDVOL;->statsSession:Landroid/net/INetworkStatsSession;

    if-nez v2, :cond_2

    .line 69
    :cond_1
    const-string v2, "ERROR"

    goto :goto_0

    .line 72
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/bcservice/CommandCDVOL;->statsService:Landroid/net/INetworkStatsService;

    invoke-interface {v2}, Landroid/net/INetworkStatsService;->forceUpdate()V

    .line 73
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/bcservice/CommandCDVOL;->statsSession:Landroid/net/INetworkStatsSession;

    invoke-static {}, Landroid/net/NetworkTemplate;->buildTemplateWifi()Landroid/net/NetworkTemplate;

    move-result-object v3

    const-wide/high16 v4, -0x8000000000000000L

    const-wide v6, 0x7fffffffffffffffL

    invoke-interface/range {v2 .. v7}, Landroid/net/INetworkStatsSession;->getSummaryForNetwork(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    move-result-object v14

    .line 74
    .local v14, "wifiEntry":Landroid/net/NetworkStats$Entry;
    iget-wide v0, v14, Landroid/net/NetworkStats$Entry;->txPackets:J

    move-wide/from16 v16, v0

    .line 75
    const-string v2, "BCService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wifi tx: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/bcservice/CommandCDVOL;->statsSession:Landroid/net/INetworkStatsSession;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/bcservice/CommandCDVOL;->subscriberId:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/NetworkTemplate;->buildTemplateMobileAll(Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v3

    const-wide/high16 v4, -0x8000000000000000L

    const-wide v6, 0x7fffffffffffffffL

    invoke-interface/range {v2 .. v7}, Landroid/net/INetworkStatsSession;->getSummaryForNetwork(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    move-result-object v9

    .line 78
    .local v9, "mobileEntry":Landroid/net/NetworkStats$Entry;
    iget-wide v10, v9, Landroid/net/NetworkStats$Entry;->txPackets:J

    .line 79
    const-string v2, "BCService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mobile tx: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    .end local v9    # "mobileEntry":Landroid/net/NetworkStats$Entry;
    .end local v14    # "wifiEntry":Landroid/net/NetworkStats$Entry;
    :goto_1
    add-long v12, v16, v10

    .line 86
    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 80
    :catch_0
    move-exception v8

    .line 81
    .local v8, "ex":Landroid/os/RemoteException;
    const-string v2, "BCService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RemoteException occurred: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 87
    .end local v8    # "ex":Landroid/os/RemoteException;
    :cond_3
    const-string v2, "RX"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 90
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/bcservice/CommandCDVOL;->statsService:Landroid/net/INetworkStatsService;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/bcservice/CommandCDVOL;->statsSession:Landroid/net/INetworkStatsSession;

    if-nez v2, :cond_5

    .line 92
    :cond_4
    const-string v2, "ERROR"

    goto/16 :goto_0

    .line 95
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/bcservice/CommandCDVOL;->statsService:Landroid/net/INetworkStatsService;

    invoke-interface {v2}, Landroid/net/INetworkStatsService;->forceUpdate()V

    .line 96
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/bcservice/CommandCDVOL;->statsSession:Landroid/net/INetworkStatsSession;

    invoke-static {}, Landroid/net/NetworkTemplate;->buildTemplateWifi()Landroid/net/NetworkTemplate;

    move-result-object v3

    const-wide/high16 v4, -0x8000000000000000L

    const-wide v6, 0x7fffffffffffffffL

    invoke-interface/range {v2 .. v7}, Landroid/net/INetworkStatsSession;->getSummaryForNetwork(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    move-result-object v14

    .line 97
    .restart local v14    # "wifiEntry":Landroid/net/NetworkStats$Entry;
    iget-wide v0, v14, Landroid/net/NetworkStats$Entry;->rxPackets:J

    move-wide/from16 v16, v0

    .line 98
    const-string v2, "BCService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wifi rx: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/bcservice/CommandCDVOL;->statsSession:Landroid/net/INetworkStatsSession;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/bcservice/CommandCDVOL;->subscriberId:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/NetworkTemplate;->buildTemplateMobileAll(Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v3

    const-wide/high16 v4, -0x8000000000000000L

    const-wide v6, 0x7fffffffffffffffL

    invoke-interface/range {v2 .. v7}, Landroid/net/INetworkStatsSession;->getSummaryForNetwork(Landroid/net/NetworkTemplate;JJ)Landroid/net/NetworkStats;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/net/NetworkStats;->getTotal(Landroid/net/NetworkStats$Entry;)Landroid/net/NetworkStats$Entry;

    move-result-object v9

    .line 101
    .restart local v9    # "mobileEntry":Landroid/net/NetworkStats$Entry;
    iget-wide v10, v9, Landroid/net/NetworkStats$Entry;->rxPackets:J

    .line 102
    const-string v2, "BCService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mobile rx: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 107
    .end local v9    # "mobileEntry":Landroid/net/NetworkStats$Entry;
    .end local v14    # "wifiEntry":Landroid/net/NetworkStats$Entry;
    :goto_2
    add-long v12, v16, v10

    .line 109
    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 103
    :catch_1
    move-exception v8

    .line 104
    .restart local v8    # "ex":Landroid/os/RemoteException;
    const-string v2, "BCService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RemoteException occurred: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v8}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 110
    .end local v8    # "ex":Landroid/os/RemoteException;
    :cond_6
    const-string v2, "LS,TX"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "LS,RX"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 112
    :cond_7
    const-string v2, "ERROR"

    goto/16 :goto_0

    .line 115
    :cond_8
    const-string v2, "N/A"

    goto/16 :goto_0
.end method
