.class Lcom/sec/automation/TetheringSettings$1;
.super Ljava/lang/Object;
.source "TetheringSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/automation/TetheringSettings;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/automation/TetheringSettings;


# direct methods
.method constructor <init>(Lcom/sec/automation/TetheringSettings;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/automation/TetheringSettings$1;->this$0:Lcom/sec/automation/TetheringSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 77
    check-cast p1, Landroid/preference/SwitchPreference;

    .end local p1    # "preference":Landroid/preference/Preference;
    invoke-virtual {p1}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/automation/TetheringSettings$1;->this$0:Lcom/sec/automation/TetheringSettings;

    const/4 v1, 0x0

    # invokes: Lcom/sec/automation/TetheringSettings;->tetherAlwaysOn(Z)Z
    invoke-static {v0, v1}, Lcom/sec/automation/TetheringSettings;->access$000(Lcom/sec/automation/TetheringSettings;Z)Z

    move-result v0

    .line 80
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/automation/TetheringSettings$1;->this$0:Lcom/sec/automation/TetheringSettings;

    const/4 v1, 0x1

    # invokes: Lcom/sec/automation/TetheringSettings;->tetherAlwaysOn(Z)Z
    invoke-static {v0, v1}, Lcom/sec/automation/TetheringSettings;->access$000(Lcom/sec/automation/TetheringSettings;Z)Z

    move-result v0

    goto :goto_0
.end method
