.class Lcom/sec/automation/TetheringSettings$2$1;
.super Ljava/lang/Object;
.source "TetheringSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/automation/TetheringSettings$2;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/automation/TetheringSettings$2;

.field final synthetic val$input:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/sec/automation/TetheringSettings$2;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/sec/automation/TetheringSettings$2$1;->this$1:Lcom/sec/automation/TetheringSettings$2;

    iput-object p2, p0, Lcom/sec/automation/TetheringSettings$2$1;->val$input:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 289
    iget-object v3, p0, Lcom/sec/automation/TetheringSettings$2$1;->val$input:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 290
    .local v0, "ip":Ljava/lang/String;
    const-string v3, "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$"

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 291
    .local v2, "ippattern":Ljava/util/regex/Pattern;
    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 292
    .local v1, "ipmatcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-nez v3, :cond_0

    .line 294
    iget-object v3, p0, Lcom/sec/automation/TetheringSettings$2$1;->this$1:Lcom/sec/automation/TetheringSettings$2;

    iget-object v3, v3, Lcom/sec/automation/TetheringSettings$2;->this$0:Lcom/sec/automation/TetheringSettings;

    const-string v4, "This is not IPv4 Address"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 301
    :goto_0
    return-void

    .line 299
    :cond_0
    iget-object v3, p0, Lcom/sec/automation/TetheringSettings$2$1;->this$1:Lcom/sec/automation/TetheringSettings$2;

    iget-object v3, v3, Lcom/sec/automation/TetheringSettings$2;->this$0:Lcom/sec/automation/TetheringSettings;

    # invokes: Lcom/sec/automation/TetheringSettings;->SetPortFwdIp(Ljava/lang/String;)V
    invoke-static {v3, v0}, Lcom/sec/automation/TetheringSettings;->access$300(Lcom/sec/automation/TetheringSettings;Ljava/lang/String;)V

    .line 300
    iget-object v3, p0, Lcom/sec/automation/TetheringSettings$2$1;->this$1:Lcom/sec/automation/TetheringSettings$2;

    iget-object v3, v3, Lcom/sec/automation/TetheringSettings$2;->this$0:Lcom/sec/automation/TetheringSettings;

    # invokes: Lcom/sec/automation/TetheringSettings;->GetPortFwdText()V
    invoke-static {v3}, Lcom/sec/automation/TetheringSettings;->access$400(Lcom/sec/automation/TetheringSettings;)V

    goto :goto_0
.end method
