.class Lcom/sec/automation/TetheringSettings$2;
.super Ljava/lang/Object;
.source "TetheringSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/automation/TetheringSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/automation/TetheringSettings;


# direct methods
.method constructor <init>(Lcom/sec/automation/TetheringSettings;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Lcom/sec/automation/TetheringSettings$2;->this$0:Lcom/sec/automation/TetheringSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4
    .param p1, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 278
    iget-object v2, p0, Lcom/sec/automation/TetheringSettings$2;->this$0:Lcom/sec/automation/TetheringSettings;

    # getter for: Lcom/sec/automation/TetheringSettings;->mPortfwdIp:Landroid/preference/Preference;
    invoke-static {v2}, Lcom/sec/automation/TetheringSettings;->access$100(Lcom/sec/automation/TetheringSettings;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 279
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/automation/TetheringSettings$2;->this$0:Lcom/sec/automation/TetheringSettings;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 280
    .local v0, "alert":Landroid/app/AlertDialog$Builder;
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/automation/TetheringSettings$2;->this$0:Lcom/sec/automation/TetheringSettings;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 281
    .local v1, "input":Landroid/widget/EditText;
    const-string v2, "Port Forwarding"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 282
    const-string v2, "Insert IP."

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 283
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 284
    iget-object v2, p0, Lcom/sec/automation/TetheringSettings$2;->this$0:Lcom/sec/automation/TetheringSettings;

    # invokes: Lcom/sec/automation/TetheringSettings;->GetPortFwdIp()Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/automation/TetheringSettings;->access$200(Lcom/sec/automation/TetheringSettings;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 286
    const-string v2, "OK"

    new-instance v3, Lcom/sec/automation/TetheringSettings$2$1;

    invoke-direct {v3, p0, v1}, Lcom/sec/automation/TetheringSettings$2$1;-><init>(Lcom/sec/automation/TetheringSettings$2;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 305
    const-string v2, "Cancel"

    new-instance v3, Lcom/sec/automation/TetheringSettings$2$2;

    invoke-direct {v3, p0}, Lcom/sec/automation/TetheringSettings$2$2;-><init>(Lcom/sec/automation/TetheringSettings$2;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 312
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 314
    .end local v0    # "alert":Landroid/app/AlertDialog$Builder;
    .end local v1    # "input":Landroid/widget/EditText;
    :cond_0
    iget-object v2, p0, Lcom/sec/automation/TetheringSettings$2;->this$0:Lcom/sec/automation/TetheringSettings;

    # getter for: Lcom/sec/automation/TetheringSettings;->mPortfwdPort:Landroid/preference/Preference;
    invoke-static {v2}, Lcom/sec/automation/TetheringSettings;->access$500(Lcom/sec/automation/TetheringSettings;)Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 316
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/automation/TetheringSettings$2;->this$0:Lcom/sec/automation/TetheringSettings;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 317
    .restart local v0    # "alert":Landroid/app/AlertDialog$Builder;
    new-instance v1, Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/automation/TetheringSettings$2;->this$0:Lcom/sec/automation/TetheringSettings;

    invoke-direct {v1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 318
    .restart local v1    # "input":Landroid/widget/EditText;
    const-string v2, "Port Forwarding"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 319
    const-string v2, "Insert Port."

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 320
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 321
    iget-object v2, p0, Lcom/sec/automation/TetheringSettings$2;->this$0:Lcom/sec/automation/TetheringSettings;

    # invokes: Lcom/sec/automation/TetheringSettings;->GetPortFwdPort()Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/automation/TetheringSettings;->access$600(Lcom/sec/automation/TetheringSettings;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 323
    const-string v2, "OK"

    new-instance v3, Lcom/sec/automation/TetheringSettings$2$3;

    invoke-direct {v3, p0, v1}, Lcom/sec/automation/TetheringSettings$2$3;-><init>(Lcom/sec/automation/TetheringSettings$2;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 342
    const-string v2, "Cancel"

    new-instance v3, Lcom/sec/automation/TetheringSettings$2$4;

    invoke-direct {v3, p0}, Lcom/sec/automation/TetheringSettings$2$4;-><init>(Lcom/sec/automation/TetheringSettings$2;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 349
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 351
    .end local v0    # "alert":Landroid/app/AlertDialog$Builder;
    .end local v1    # "input":Landroid/widget/EditText;
    :cond_1
    const/4 v2, 0x1

    return v2
.end method
