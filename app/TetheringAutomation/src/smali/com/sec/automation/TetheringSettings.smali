.class public Lcom/sec/automation/TetheringSettings;
.super Landroid/preference/PreferenceActivity;
.source "TetheringSettings.java"


# instance fields
.field private builder:Landroid/app/AlertDialog$Builder;

.field private mClicked:Landroid/preference/Preference$OnPreferenceClickListener;

.field mMenuReset:Landroid/view/MenuItem;

.field private mPortfwdIp:Landroid/preference/Preference;

.field private mPortfwdPort:Landroid/preference/Preference;

.field private mTetheringAlwaysModeSwitch:Landroid/preference/SwitchPreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 275
    new-instance v0, Lcom/sec/automation/TetheringSettings$2;

    invoke-direct {v0, p0}, Lcom/sec/automation/TetheringSettings$2;-><init>(Lcom/sec/automation/TetheringSettings;)V

    iput-object v0, p0, Lcom/sec/automation/TetheringSettings;->mClicked:Landroid/preference/Preference$OnPreferenceClickListener;

    return-void
.end method

.method private GetPortFwdIp()Ljava/lang/String;
    .locals 9

    .prologue
    .line 135
    const-string v5, ""

    .line 136
    .local v5, "ip":Ljava/lang/String;
    const/4 v0, 0x0

    .line 137
    .local v0, "bufferReader":Ljava/io/BufferedReader;
    const/4 v3, 0x0

    .line 140
    .local v3, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    const-string v6, "/data/log/portfwd_ip"

    invoke-direct {v4, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 142
    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .local v1, "bufferReader":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v5

    .line 148
    if-eqz v1, :cond_0

    .line 149
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 154
    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    .line 155
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_1
    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    move-object v0, v1

    .line 160
    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    :cond_2
    :goto_1
    if-nez v5, :cond_3

    const-string v5, ""

    .line 161
    :cond_3
    return-object v5

    .line 150
    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v2

    .line 151
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 156
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 157
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    move-object v0, v1

    .line 159
    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 143
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 144
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    const-string v6, "TetheringAutomation"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GetPortFwdText ip no file:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 148
    if-eqz v0, :cond_4

    .line 149
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 154
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_3
    if-eqz v3, :cond_2

    .line 155
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 156
    :catch_3
    move-exception v2

    .line 157
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 150
    .local v2, "e":Ljava/lang/Exception;
    :catch_4
    move-exception v2

    .line 151
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 147
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 148
    :goto_4
    if-eqz v0, :cond_5

    .line 149
    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 154
    :cond_5
    :goto_5
    if-eqz v3, :cond_6

    .line 155
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 158
    :cond_6
    :goto_6
    throw v6

    .line 150
    :catch_5
    move-exception v2

    .line 151
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 156
    .end local v2    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v2

    .line 157
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 147
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v6

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    move-object v0, v1

    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    goto :goto_4

    .line 143
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_7
    move-exception v2

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_8
    move-exception v2

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    move-object v0, v1

    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method private GetPortFwdPort()Ljava/lang/String;
    .locals 9

    .prologue
    .line 166
    const-string v5, ""

    .line 167
    .local v5, "port":Ljava/lang/String;
    const/4 v0, 0x0

    .line 168
    .local v0, "bufferReader":Ljava/io/BufferedReader;
    const/4 v3, 0x0

    .line 171
    .local v3, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    const-string v6, "/data/log/portfwd_port"

    invoke-direct {v4, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 173
    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .local v1, "bufferReader":Ljava/io/BufferedReader;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v5

    .line 179
    if-eqz v1, :cond_0

    .line 180
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 186
    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    .line 187
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_1
    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    move-object v0, v1

    .line 193
    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    :cond_2
    :goto_1
    if-nez v5, :cond_3

    const-string v5, ""

    .line 194
    :cond_3
    return-object v5

    .line 181
    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v2

    .line 182
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 188
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 189
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    move-object v0, v1

    .line 191
    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    goto :goto_1

    .line 174
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 175
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    const-string v6, "TetheringAutomation"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GetPortFwdText port no file:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 179
    if-eqz v0, :cond_4

    .line 180
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 186
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_3
    if-eqz v3, :cond_2

    .line 187
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 188
    :catch_3
    move-exception v2

    .line 189
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 181
    .local v2, "e":Ljava/lang/Exception;
    :catch_4
    move-exception v2

    .line 182
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 178
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 179
    :goto_4
    if-eqz v0, :cond_5

    .line 180
    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 186
    :cond_5
    :goto_5
    if-eqz v3, :cond_6

    .line 187
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 190
    :cond_6
    :goto_6
    throw v6

    .line 181
    :catch_5
    move-exception v2

    .line 182
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 188
    .end local v2    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v2

    .line 189
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 178
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v6

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    move-object v0, v1

    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    goto :goto_4

    .line 174
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_7
    move-exception v2

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_8
    move-exception v2

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    move-object v0, v1

    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method private GetPortFwdText()V
    .locals 4

    .prologue
    .line 115
    const-string v0, ""

    .line 116
    .local v0, "ip":Ljava/lang/String;
    const-string v1, ""

    .line 118
    .local v1, "port":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/automation/TetheringSettings;->GetPortFwdIp()Ljava/lang/String;

    move-result-object v0

    .line 119
    invoke-direct {p0}, Lcom/sec/automation/TetheringSettings;->GetPortFwdPort()Ljava/lang/String;

    move-result-object v1

    .line 121
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 122
    iget-object v2, p0, Lcom/sec/automation/TetheringSettings;->mPortfwdIp:Landroid/preference/Preference;

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 126
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 127
    iget-object v2, p0, Lcom/sec/automation/TetheringSettings;->mPortfwdPort:Landroid/preference/Preference;

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 131
    :goto_1
    return-void

    .line 124
    :cond_0
    iget-object v2, p0, Lcom/sec/automation/TetheringSettings;->mPortfwdIp:Landroid/preference/Preference;

    const-string v3, "not set "

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 129
    :cond_1
    iget-object v2, p0, Lcom/sec/automation/TetheringSettings;->mPortfwdPort:Landroid/preference/Preference;

    const-string v3, "not set"

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private ResultMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/automation/TetheringSettings;->builder:Landroid/app/AlertDialog$Builder;

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 268
    iget-object v0, p0, Lcom/sec/automation/TetheringSettings;->builder:Landroid/app/AlertDialog$Builder;

    const-string v1, "Result"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 269
    iget-object v0, p0, Lcom/sec/automation/TetheringSettings;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 270
    iget-object v0, p0, Lcom/sec/automation/TetheringSettings;->builder:Landroid/app/AlertDialog$Builder;

    const v1, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 271
    iget-object v0, p0, Lcom/sec/automation/TetheringSettings;->builder:Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 272
    iget-object v0, p0, Lcom/sec/automation/TetheringSettings;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 273
    return-void
.end method

.method private SetPortFwdIp(Ljava/lang/String;)V
    .locals 7
    .param p1, "ip"    # Ljava/lang/String;

    .prologue
    .line 199
    const/4 v2, 0x0

    .line 201
    .local v2, "ip_writer":Ljava/io/Writer;
    :try_start_0
    new-instance v3, Ljava/io/FileWriter;

    const-string v4, "/data/log/portfwd_ip"

    invoke-direct {v3, v4}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    .end local v2    # "ip_writer":Ljava/io/Writer;
    .local v3, "ip_writer":Ljava/io/Writer;
    :try_start_1
    invoke-virtual {v3, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 211
    if-eqz v3, :cond_0

    .line 212
    :try_start_2
    invoke-virtual {v3}, Ljava/io/Writer;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 217
    .end local v3    # "ip_writer":Ljava/io/Writer;
    .restart local v2    # "ip_writer":Ljava/io/Writer;
    :cond_1
    :goto_0
    return-void

    .line 213
    .end local v2    # "ip_writer":Ljava/io/Writer;
    .restart local v3    # "ip_writer":Ljava/io/Writer;
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v2, v3

    .line 216
    .end local v3    # "ip_writer":Ljava/io/Writer;
    .restart local v2    # "ip_writer":Ljava/io/Writer;
    goto :goto_0

    .line 204
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 206
    .local v1, "ex":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    const-string v4, "TetheringAutomation"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SetPortFwdIp exception :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 211
    if-eqz v2, :cond_1

    .line 212
    :try_start_4
    invoke-virtual {v2}, Ljava/io/Writer;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 213
    :catch_2
    move-exception v0

    .line 214
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 210
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 211
    :goto_2
    if-eqz v2, :cond_2

    .line 212
    :try_start_5
    invoke-virtual {v2}, Ljava/io/Writer;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 215
    :cond_2
    :goto_3
    throw v4

    .line 213
    :catch_3
    move-exception v0

    .line 214
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 210
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "ip_writer":Ljava/io/Writer;
    .restart local v3    # "ip_writer":Ljava/io/Writer;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "ip_writer":Ljava/io/Writer;
    .restart local v2    # "ip_writer":Ljava/io/Writer;
    goto :goto_2

    .line 204
    .end local v2    # "ip_writer":Ljava/io/Writer;
    .restart local v3    # "ip_writer":Ljava/io/Writer;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "ip_writer":Ljava/io/Writer;
    .restart local v2    # "ip_writer":Ljava/io/Writer;
    goto :goto_1
.end method

.method private SetPortFwdPort(Ljava/lang/String;)V
    .locals 7
    .param p1, "port"    # Ljava/lang/String;

    .prologue
    .line 221
    const/4 v2, 0x0

    .line 223
    .local v2, "port_writer":Ljava/io/Writer;
    :try_start_0
    new-instance v3, Ljava/io/FileWriter;

    const-string v4, "/data/log/portfwd_port"

    invoke-direct {v3, v4}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    .end local v2    # "port_writer":Ljava/io/Writer;
    .local v3, "port_writer":Ljava/io/Writer;
    :try_start_1
    invoke-virtual {v3, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 232
    if-eqz v3, :cond_0

    .line 233
    :try_start_2
    invoke-virtual {v3}, Ljava/io/Writer;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 238
    .end local v3    # "port_writer":Ljava/io/Writer;
    .restart local v2    # "port_writer":Ljava/io/Writer;
    :cond_1
    :goto_0
    return-void

    .line 234
    .end local v2    # "port_writer":Ljava/io/Writer;
    .restart local v3    # "port_writer":Ljava/io/Writer;
    :catch_0
    move-exception v0

    .line 235
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v2, v3

    .line 237
    .end local v3    # "port_writer":Ljava/io/Writer;
    .restart local v2    # "port_writer":Ljava/io/Writer;
    goto :goto_0

    .line 226
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 228
    .local v1, "ex":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    const-string v4, "TetheringAutomation"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SetPortFwdPort exception :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 232
    if-eqz v2, :cond_1

    .line 233
    :try_start_4
    invoke-virtual {v2}, Ljava/io/Writer;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 234
    :catch_2
    move-exception v0

    .line 235
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 231
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 232
    :goto_2
    if-eqz v2, :cond_2

    .line 233
    :try_start_5
    invoke-virtual {v2}, Ljava/io/Writer;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 236
    :cond_2
    :goto_3
    throw v4

    .line 234
    :catch_3
    move-exception v0

    .line 235
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 231
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "port_writer":Ljava/io/Writer;
    .restart local v3    # "port_writer":Ljava/io/Writer;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "port_writer":Ljava/io/Writer;
    .restart local v2    # "port_writer":Ljava/io/Writer;
    goto :goto_2

    .line 226
    .end local v2    # "port_writer":Ljava/io/Writer;
    .restart local v3    # "port_writer":Ljava/io/Writer;
    :catch_4
    move-exception v1

    move-object v2, v3

    .end local v3    # "port_writer":Ljava/io/Writer;
    .restart local v2    # "port_writer":Ljava/io/Writer;
    goto :goto_1
.end method

.method static synthetic access$000(Lcom/sec/automation/TetheringSettings;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/automation/TetheringSettings;
    .param p1, "x1"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/automation/TetheringSettings;->tetherAlwaysOn(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/automation/TetheringSettings;)Landroid/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/automation/TetheringSettings;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/automation/TetheringSettings;->mPortfwdIp:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/automation/TetheringSettings;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/automation/TetheringSettings;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/automation/TetheringSettings;->GetPortFwdIp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/automation/TetheringSettings;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/automation/TetheringSettings;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/automation/TetheringSettings;->SetPortFwdIp(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/automation/TetheringSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/automation/TetheringSettings;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/automation/TetheringSettings;->GetPortFwdText()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/automation/TetheringSettings;)Landroid/preference/Preference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/automation/TetheringSettings;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/automation/TetheringSettings;->mPortfwdPort:Landroid/preference/Preference;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/automation/TetheringSettings;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/automation/TetheringSettings;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/automation/TetheringSettings;->GetPortFwdPort()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/automation/TetheringSettings;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/automation/TetheringSettings;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/automation/TetheringSettings;->SetPortFwdPort(Ljava/lang/String;)V

    return-void
.end method

.method private resetData()V
    .locals 1

    .prologue
    .line 242
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/sec/automation/TetheringSettings;->SetPortFwdIp(Ljava/lang/String;)V

    .line 243
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/sec/automation/TetheringSettings;->SetPortFwdPort(Ljava/lang/String;)V

    .line 244
    invoke-direct {p0}, Lcom/sec/automation/TetheringSettings;->GetPortFwdText()V

    .line 245
    return-void
.end method

.method private runTetheringAlwaysOn()V
    .locals 4

    .prologue
    .line 438
    const-string v2, "ro.board.platform"

    const-string v3, "None"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 439
    .local v0, "chipsetName":Ljava/lang/String;
    const-string v2, "APQ8084"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 440
    const-string v2, "TetheringAutomation"

    const-string v3, "APQ8084 : persist.data.mode is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    const-string v2, "persist.data.mode"

    const-string v3, ""

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    :cond_0
    const-string v2, "TetheringAutomation"

    const-string v3, "tethering always mode on"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    invoke-virtual {p0}, Lcom/sec/automation/TetheringSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 445
    .local v1, "cr":Landroid/content/ContentResolver;
    const-string v2, "TETHER_ALWAYS_ON_MODE"

    const-string v3, "enabled"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 446
    const-string v2, "tethering always mode on! Please restart the phone"

    invoke-direct {p0, v2}, Lcom/sec/automation/TetheringSettings;->ResultMessage(Ljava/lang/String;)V

    .line 447
    return-void
.end method

.method private tetherAlwaysOn(Z)Z
    .locals 8
    .param p1, "on"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 356
    if-eqz p1, :cond_2

    .line 357
    const-string v7, "ro.build.type"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 409
    .local v0, "buildtype":Ljava/lang/String;
    const/4 v2, 0x0

    .line 411
    .local v2, "isTestSim":Z
    const-string v7, "gsm.sim.state"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 413
    .local v4, "simState":Ljava/lang/String;
    const-string v7, "ABSENT"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 414
    const-string v7, "gsm.sim.operator.numeric"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 415
    .local v3, "numeric":Ljava/lang/String;
    const/4 v7, 0x5

    invoke-virtual {v3, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 416
    const-string v7, "00101"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 419
    .end local v3    # "numeric":Ljava/lang/String;
    :cond_0
    if-nez v2, :cond_1

    const-string v7, "eng"

    invoke-virtual {v0, v7}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v7

    if-eqz v7, :cond_1

    .line 420
    const-string v6, "Invalid SIM or NOT engineer mode. Don\'t start tethering always mode"

    invoke-direct {p0, v6}, Lcom/sec/automation/TetheringSettings;->ResultMessage(Ljava/lang/String;)V

    .line 432
    .end local v0    # "buildtype":Ljava/lang/String;
    .end local v2    # "isTestSim":Z
    .end local v4    # "simState":Ljava/lang/String;
    :goto_0
    return v5

    .line 423
    .restart local v0    # "buildtype":Ljava/lang/String;
    .restart local v2    # "isTestSim":Z
    .restart local v4    # "simState":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/sec/automation/TetheringSettings;->runTetheringAlwaysOn()V

    move v5, v6

    .line 424
    goto :goto_0

    .line 428
    .end local v0    # "buildtype":Ljava/lang/String;
    .end local v2    # "isTestSim":Z
    .end local v4    # "simState":Ljava/lang/String;
    :cond_2
    const-string v5, "TetheringAutomation"

    const-string v7, "tethering always mode off"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    invoke-virtual {p0}, Lcom/sec/automation/TetheringSettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 430
    .local v1, "cr":Landroid/content/ContentResolver;
    const-string v5, "TETHER_ALWAYS_ON_MODE"

    const-string v7, "disabled"

    invoke-static {v1, v5, v7}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 431
    const-string v5, "tethering always mode  off! Please restart the phone"

    invoke-direct {p0, v5}, Lcom/sec/automation/TetheringSettings;->ResultMessage(Ljava/lang/String;)V

    move v5, v6

    .line 432
    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 69
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 71
    const/high16 v0, 0x7f020000

    invoke-virtual {p0, v0}, Lcom/sec/automation/TetheringSettings;->addPreferencesFromResource(I)V

    .line 73
    const-string v0, "switch_preference"

    invoke-virtual {p0, v0}, Lcom/sec/automation/TetheringSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/sec/automation/TetheringSettings;->mTetheringAlwaysModeSwitch:Landroid/preference/SwitchPreference;

    .line 74
    iget-object v0, p0, Lcom/sec/automation/TetheringSettings;->mTetheringAlwaysModeSwitch:Landroid/preference/SwitchPreference;

    new-instance v1, Lcom/sec/automation/TetheringSettings$1;

    invoke-direct {v1, p0}, Lcom/sec/automation/TetheringSettings$1;-><init>(Lcom/sec/automation/TetheringSettings;)V

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 84
    const-string v0, "ip_preference"

    invoke-virtual {p0, v0}, Lcom/sec/automation/TetheringSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/automation/TetheringSettings;->mPortfwdIp:Landroid/preference/Preference;

    .line 85
    iget-object v0, p0, Lcom/sec/automation/TetheringSettings;->mPortfwdIp:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/sec/automation/TetheringSettings;->mClicked:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 86
    const-string v0, "port_preference"

    invoke-virtual {p0, v0}, Lcom/sec/automation/TetheringSettings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/automation/TetheringSettings;->mPortfwdPort:Landroid/preference/Preference;

    .line 87
    iget-object v0, p0, Lcom/sec/automation/TetheringSettings;->mPortfwdPort:Landroid/preference/Preference;

    iget-object v1, p0, Lcom/sec/automation/TetheringSettings;->mClicked:Landroid/preference/Preference$OnPreferenceClickListener;

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 89
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/automation/TetheringSettings;->builder:Landroid/app/AlertDialog$Builder;

    .line 91
    invoke-direct {p0}, Lcom/sec/automation/TetheringSettings;->GetPortFwdText()V

    .line 92
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 96
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 97
    const/4 v0, 0x2

    const v1, 0x7f030005

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/automation/TetheringSettings;->mMenuReset:Landroid/view/MenuItem;

    .line 98
    iget-object v0, p0, Lcom/sec/automation/TetheringSettings;->mMenuReset:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 100
    return v3
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 249
    packed-switch p1, :pswitch_data_0

    .line 262
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 253
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/automation/TetheringSettings;->finish()V

    goto :goto_0

    .line 258
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/automation/TetheringSettings;->finish()V

    goto :goto_0

    .line 249
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 105
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 110
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 107
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/automation/TetheringSettings;->resetData()V

    goto :goto_0

    .line 105
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
