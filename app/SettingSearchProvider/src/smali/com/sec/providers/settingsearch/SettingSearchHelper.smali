.class public Lcom/sec/providers/settingsearch/SettingSearchHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SettingSearchHelper.java"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 13
    const-string v0, "settingsearch.db"

    const/4 v1, 0x0

    const/4 v2, 0x6

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 14
    iput-object p1, p0, Lcom/sec/providers/settingsearch/SettingSearchHelper;->mContext:Landroid/content/Context;

    .line 15
    return-void
.end method

.method private createTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/providers/settingsearch/SettingSearchHelper;->deleteTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 38
    const-string v0, "CREATE VIRTUAL TABLE searchinfo USING fts3 (id_key  TEXT,title TEXT, title_res_id INTEGER, summary TEXT, summary_res_id INTEGER,icon_res_id INTEGER,parentskey TEXT,menu_type INTEGER,package_name TEXT,registerMode INTEGER NOT NULL DEFAULT 0 ,)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 52
    const-string v0, "CREATE VIRTUAL TABLE titleinfo USING fts3 (id_key  TEXT,icon INTEGER, title TEXT, title_hex TEXT, summary TEXT, summary_hex TEXT, menu_type INTEGER,language TEXT, menu_path TEXT,package_name TEXT,registerMode INTEGER,)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 67
    return-void
.end method

.method private deleteTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 70
    const-string v0, "DROP TABLE IF EXISTS searchinfo"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 71
    const-string v0, "DROP TABLE IF EXISTS titleinfo"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 72
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sec/providers/settingsearch/SettingSearchHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 21
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/providers/settingsearch/SettingSearchHelper;->deleteTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 32
    invoke-direct {p0, p1}, Lcom/sec/providers/settingsearch/SettingSearchHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 33
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/providers/settingsearch/SettingSearchHelper;->deleteTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 26
    invoke-direct {p0, p1}, Lcom/sec/providers/settingsearch/SettingSearchHelper;->createTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 27
    return-void
.end method
