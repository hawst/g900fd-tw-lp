.class public Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
.super Ljava/lang/Exception;
.source "SyncException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mExceptionCode:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "code"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 27
    iput p1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;->mExceptionCode:I

    .line 28
    return-void
.end method


# virtual methods
.method public getmExceptionCode()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;->mExceptionCode:I

    return v0
.end method
