.class Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$17;
.super Ljava/lang/Object;
.source "MemoContentFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->showDiscardDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$17;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    .line 1658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v1, 0x0

    .line 1660
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$17;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1661
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$17;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->unregisterClipboard()V

    .line 1662
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$17;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->cancelCurrentContent(Z)V
    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$28(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V

    .line 1663
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$17;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$29(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V

    .line 1664
    return-void
.end method
