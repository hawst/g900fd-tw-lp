.class Lcom/samsung/android/app/memo/uifragment/MemoListFragment$3;
.super Ljava/lang/Object;
.source "MemoListFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->onActivityCreated(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$3;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    .line 566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 570
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$3;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->hideUndoBar()V

    .line 571
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$3;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$12(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$3;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$41(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$3;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$41(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 572
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$3;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$12(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/app/Activity;

    move-result-object v1

    .line 574
    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 572
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 576
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 577
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 579
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return v3
.end method
