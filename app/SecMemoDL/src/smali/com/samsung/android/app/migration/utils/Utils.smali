.class public Lcom/samsung/android/app/migration/utils/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field public static final APP_ATTACH_FOLDER:Ljava/lang/String; = "app_attach"

.field public static final BK_EXTENSION:Ljava/lang/String; = ".bk"

.field public static final BLOB_EXTENSION:Ljava/lang/String; = ".blob"

.field public static final KIES_FOLDER_PATH:Ljava/lang/String;

.field public static final MEMO_DB:Ljava/lang/String; = "memo.db"

.field public static final SNB_EXTENSION:Ljava/lang/String; = ".snb"

.field public static final SNB_TEXTMEMO_TEMPLATE:Ljava/lang/String; = "14"

.field public static final SNB_TMP_FOLDER_PATH:Ljava/lang/String;

.field public static final SNB_TMP_KIES_FOLDER_PATH:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "Utils"

.field public static final TMP_FOLDER_PATH:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/_SamsungBnR_/ABR/NMemo/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/migration/utils/Utils;->KIES_FOLDER_PATH:Ljava/lang/String;

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 52
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/Android/data/com.samsung.android.app.memo/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/migration/utils/Utils;->TMP_FOLDER_PATH:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/migration/utils/Utils;->TMP_FOLDER_PATH:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "tmemo2/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_FOLDER_PATH:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_FOLDER_PATH:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "kies/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_KIES_FOLDER_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static concat(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "folderPath"    # Ljava/lang/String;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 59
    move-object v0, p0

    .line 60
    .local v0, "path":Ljava/lang/String;
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    .line 63
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static forSQL(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 107
    const-string v0, "\'"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 109
    :cond_0
    return-object p0
.end method

.method public static formatContent(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "content"    # Ljava/lang/String;

    .prologue
    .line 100
    const-string v1, " "

    const-string v2, "&nbsp;"

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "formattedContent":Ljava/lang/String;
    const-string v1, "<"

    const-string v2, "&lt;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 102
    const-string v1, ">"

    const-string v2, "&gt;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 103
    return-object v0
.end method

.method public static getFolderPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "snbPath"    # Ljava/lang/String;

    .prologue
    .line 68
    const-string v2, "/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 69
    .local v1, "startIndex":I
    const/4 v2, 0x0

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "folderPath":Ljava/lang/String;
    return-object v0
.end method

.method public static getTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 74
    const-string v3, "/"

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 75
    .local v1, "startIndex":I
    const-string v3, "."

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 76
    .local v0, "finishIndex":I
    const/4 v2, 0x0

    .line 77
    .local v2, "title":Ljava/lang/String;
    if-ltz v1, :cond_0

    if-ltz v0, :cond_0

    .line 78
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 81
    :goto_0
    return-object v2

    .line 80
    :cond_0
    const-string v2, "tmp"

    goto :goto_0
.end method

.method public static handleNewLines(Ljava/lang/String;)Ljava/lang/StringBuilder;
    .locals 4
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 85
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .local v2, "out":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lt v1, v3, :cond_0

    .line 96
    return-object v2

    .line 87
    :cond_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 88
    .local v0, "c":C
    const/16 v3, 0xa

    if-ne v0, v3, :cond_1

    .line 89
    const-string v3, "</p><p>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 90
    :cond_1
    const/16 v3, 0xd

    if-ne v0, v3, :cond_2

    .line 91
    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 93
    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static isNetworkReachable(Landroid/content/Context;)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 121
    const-string v11, "connectivity"

    invoke-virtual {p0, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 122
    .local v0, "connMgr":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v10}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v5

    .line 123
    .local v5, "wifi":Landroid/net/NetworkInfo;
    invoke-virtual {v0, v9}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 124
    .local v3, "mobile":Landroid/net/NetworkInfo;
    const/4 v11, 0x6

    invoke-virtual {v0, v11}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v7

    .line 125
    .local v7, "wimax":Landroid/net/NetworkInfo;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 126
    .local v1, "dataNetwork":Landroid/net/NetworkInfo;
    const/4 v6, 0x0

    .local v6, "wifiConnected":Z
    const/4 v4, 0x0

    .local v4, "mobileConnected":Z
    const/4 v8, 0x0

    .local v8, "wimaxConnected":Z
    const/4 v2, 0x0

    .line 127
    .local v2, "dataNetworkConnected":Z
    if-eqz v5, :cond_0

    .line 128
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    .line 130
    :cond_0
    if-eqz v3, :cond_1

    .line 131
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    .line 133
    :cond_1
    if-eqz v7, :cond_2

    .line 134
    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v8

    .line 136
    :cond_2
    if-eqz v1, :cond_3

    .line 137
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    .line 139
    :cond_3
    if-nez v6, :cond_4

    if-nez v4, :cond_4

    if-nez v8, :cond_4

    if-eqz v2, :cond_5

    :cond_4
    move v9, v10

    .line 142
    :cond_5
    return v9
.end method

.method public static resetSyncDialogsState(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 113
    invoke-static {p0}, Lcom/samsung/android/app/migration/utils/SharedPref;->getSyncMemoDialogState(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 114
    invoke-static {p0, v2}, Lcom/samsung/android/app/migration/utils/SharedPref;->setSyncMemoDialogState(Landroid/content/Context;I)V

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    invoke-static {p0}, Lcom/samsung/android/app/migration/utils/SharedPref;->getTMemoImportDialogState(Landroid/content/Context;)I

    move-result v0

    if-lez v0, :cond_0

    .line 116
    invoke-static {p0, v2}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemoImportDialogState(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public static sendRequestToSAService(Landroid/content/Context;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requestType"    # I

    .prologue
    .line 164
    const-string v1, "Utils"

    const-string v2, "Starting SamsungAccountService..."

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-static {}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->getInstance()Lcom/samsung/android/app/migration/service/SamsungAccountService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 166
    invoke-static {}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->getInstance()Lcom/samsung/android/app/migration/service/SamsungAccountService;

    move-result-object v1

    iget-boolean v1, v1, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mIsAwaitingResponse:Z

    if-eqz v1, :cond_0

    .line 167
    const-string v1, "Utils"

    const-string v2, "Skipped starting, still waiting for response."

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :goto_0
    return-void

    .line 170
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/migration/service/SamsungAccountService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 171
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "requestType"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 172
    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 173
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public static sendRequestToSAService(Landroid/content/Context;ILjava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requestType"    # I
    .param p2, "expiredAccessToken"    # Ljava/lang/String;

    .prologue
    .line 177
    const-string v1, "Utils"

    const-string v2, "Starting SamsungAccountService..."

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    invoke-static {}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->getInstance()Lcom/samsung/android/app/migration/service/SamsungAccountService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 179
    invoke-static {}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->getInstance()Lcom/samsung/android/app/migration/service/SamsungAccountService;

    move-result-object v1

    iget-boolean v1, v1, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mIsAwaitingResponse:Z

    if-eqz v1, :cond_0

    .line 180
    const-string v1, "Utils"

    const-string v2, "Skipped starting, still waiting for response."

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :goto_0
    return-void

    .line 183
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/migration/service/SamsungAccountService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 184
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "requestType"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 185
    const-string v1, "expiredAccessToken"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 186
    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 187
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public static updateSharedPrefWhenLoginToSA(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 156
    invoke-static {p0, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->setMemoRelaunchedForSyncMemoDialogFlag(Landroid/content/Context;Z)V

    .line 157
    invoke-static {p0, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->setMemoRelaunchedForTMemoImportDialogFlag(Landroid/content/Context;Z)V

    .line 158
    invoke-static {p0, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->setSAVerificationStartedFlag(Landroid/content/Context;Z)V

    .line 159
    invoke-static {p0, p1}, Lcom/samsung/android/app/migration/utils/SharedPref;->setSAName(Landroid/content/Context;Ljava/lang/String;)V

    .line 160
    return-void
.end method

.method public static updateSharedPrefWhenLogoutFromSA(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 146
    invoke-static {p0, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->updateVerifiedSA(Landroid/content/Context;Z)V

    .line 147
    invoke-static {p0, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->setImportDoNotShowFlag(Landroid/content/Context;Z)V

    .line 148
    invoke-static {p0, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->setSyncMemoDialogState(Landroid/content/Context;I)V

    .line 149
    invoke-static {p0, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemoImportDialogState(Landroid/content/Context;I)V

    .line 150
    invoke-static {p0, v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->setMemoRelaunchedForSyncMemoDialogFlag(Landroid/content/Context;Z)V

    .line 151
    invoke-static {p0, v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->setMemoRelaunchedForTMemoImportDialogFlag(Landroid/content/Context;Z)V

    .line 152
    invoke-static {p0, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->setSAVerificationStartedFlag(Landroid/content/Context;Z)V

    .line 153
    return-void
.end method
