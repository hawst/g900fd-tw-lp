.class public Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;
.super Landroid/app/Fragment;
.source "ImageViewFragment.java"


# static fields
.field public static final KEY_CLICK_POS:Ljava/lang/String;

.field public static final KEY_CONTENT_URI:Ljava/lang/String;

.field public static final KEY_ORIENTATION:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field loadImageHandler:Landroid/os/Handler;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mContentUri:Landroid/net/Uri;

.field private mImageView:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

.field private mLastClickPos:I

.field private mOrientation:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    const-class v0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->TAG:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_uri"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->KEY_CONTENT_URI:Ljava/lang/String;

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_orientation"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->KEY_ORIENTATION:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_position"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->KEY_CLICK_POS:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mLastClickPos:I

    .line 117
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment$1;-><init>(Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->loadImageHandler:Landroid/os/Handler;

    .line 50
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mContentUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;)I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mOrientation:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;)Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mImageView:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$5()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private initView(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 99
    const v0, 0x7f0e001e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mImageView:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    .line 100
    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->loadImageHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 115
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 105
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->setHasOptionsMenu(Z)V

    .line 107
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 108
    .local v0, "args":Landroid/os/Bundle;
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->KEY_CONTENT_URI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mContentUri:Landroid/net/Uri;

    .line 109
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->KEY_ORIENTATION:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mOrientation:I

    .line 110
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->initialize()V

    .line 111
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 71
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 72
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->setHasOptionsMenu(Z)V

    .line 73
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 147
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 148
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 84
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 85
    .local v0, "parent":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 86
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->KEY_CLICK_POS:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->KEY_CLICK_POS:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getExtra(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mLastClickPos:I

    .line 88
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    const v7, 0x7f0e0092

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 157
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 158
    const v3, 0x7f0d0001

    invoke-virtual {p2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 159
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 160
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 161
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 165
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x102002c

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 166
    .local v1, "homeView":Landroid/view/View;
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 167
    invoke-virtual {v1, v5, v5, v5, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 168
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 169
    .local v2, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 170
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 171
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mImageView:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mImageView:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v3, :cond_0

    .line 172
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 175
    :goto_0
    return-void

    .line 174
    :cond_0
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 92
    const v1, 0x7f040015

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 93
    .local v0, "v":Landroid/view/View;
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 94
    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->initView(Landroid/view/View;)V

    .line 95
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 134
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mBitmap:Landroid/graphics/Bitmap;

    .line 138
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 180
    .local v1, "parent":Landroid/app/Activity;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 188
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setResult(I)V

    .line 192
    :goto_0
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 193
    const/4 v2, 0x1

    return v2

    .line 182
    :pswitch_0
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 183
    .local v0, "i":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mContentUri:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 184
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->KEY_CLICK_POS:Ljava/lang/String;

    iget v3, p0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->mLastClickPos:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 185
    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 180
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0092
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 152
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 153
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 142
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 143
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 78
    return-void
.end method
