.class Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;
.super Landroid/os/AsyncTask;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/util/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BitmapWorkerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final imageViewReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mPath:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/app/memo/util/ImageLoader;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/memo/util/ImageLoader;Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 1
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->mPath:Ljava/lang/String;

    .line 184
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    .line 185
    iput-object p3, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->mPath:Ljava/lang/String;

    .line 186
    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method private getAttachedImageView()Landroid/widget/ImageView;
    .locals 3

    .prologue
    .line 237
    iget-object v2, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->imageViewReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 238
    .local v1, "imageView":Landroid/widget/ImageView;
    # invokes: Lcom/samsung/android/app/memo/util/ImageLoader;->getBitmapWorkerTask(Landroid/widget/ImageView;)Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;
    invoke-static {v1}, Lcom/samsung/android/app/memo/util/ImageLoader;->access$4(Landroid/widget/ImageView;)Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;

    move-result-object v0

    .line 240
    .local v0, "bitmapWorkerTask":Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;
    if-ne p0, v0, :cond_0

    .line 244
    .end local v1    # "imageView":Landroid/widget/ImageView;
    :goto_0
    return-object v1

    .restart local v1    # "imageView":Landroid/widget/ImageView;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    .line 190
    const/4 v0, 0x0

    .line 193
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    # getter for: Lcom/samsung/android/app/memo/util/ImageLoader;->mPauseWorkLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/app/memo/util/ImageLoader;->access$0(Lcom/samsung/android/app/memo/util/ImageLoader;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 194
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    iget-boolean v2, v2, Lcom/samsung/android/app/memo/util/ImageLoader;->mPauseWork:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 193
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->getAttachedImageView()Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 204
    iget-object v2, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    iget-object v3, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->mPath:Ljava/lang/String;

    # invokes: Lcom/samsung/android/app/memo/util/ImageLoader;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/ImageLoader;->access$1(Lcom/samsung/android/app/memo/util/ImageLoader;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 207
    :cond_1
    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    # getter for: Lcom/samsung/android/app/memo/util/ImageLoader;->mImageCache:Lcom/samsung/android/app/memo/util/ImageCache;
    invoke-static {v2}, Lcom/samsung/android/app/memo/util/ImageLoader;->access$2(Lcom/samsung/android/app/memo/util/ImageLoader;)Lcom/samsung/android/app/memo/util/ImageCache;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 208
    iget-object v2, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    # getter for: Lcom/samsung/android/app/memo/util/ImageLoader;->mImageCache:Lcom/samsung/android/app/memo/util/ImageCache;
    invoke-static {v2}, Lcom/samsung/android/app/memo/util/ImageLoader;->access$2(Lcom/samsung/android/app/memo/util/ImageLoader;)Lcom/samsung/android/app/memo/util/ImageCache;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->mPath:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/samsung/android/app/memo/util/ImageCache;->addBitmapToCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 211
    :cond_2
    return-object v0

    .line 196
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    # getter for: Lcom/samsung/android/app/memo/util/ImageLoader;->mPauseWorkLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/app/memo/util/ImageLoader;->access$0(Lcom/samsung/android/app/memo/util/ImageLoader;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 197
    :catch_0
    move-exception v1

    .line 198
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v2, "ImageLoader"

    const-string v4, "BitmapWorkerTask "

    invoke-static {v2, v4, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 193
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->doInBackground([Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 230
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 231
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    # getter for: Lcom/samsung/android/app/memo/util/ImageLoader;->mPauseWorkLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/app/memo/util/ImageLoader;->access$0(Lcom/samsung/android/app/memo/util/ImageLoader;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 232
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    # getter for: Lcom/samsung/android/app/memo/util/ImageLoader;->mPauseWorkLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/app/memo/util/ImageLoader;->access$0(Lcom/samsung/android/app/memo/util/ImageLoader;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 231
    monitor-exit v1

    .line 234
    return-void

    .line 231
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->onCancelled(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    const/4 p1, 0x0

    .line 222
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->getAttachedImageView()Landroid/widget/ImageView;

    move-result-object v0

    .line 223
    .local v0, "imageView":Landroid/widget/ImageView;
    if-eqz p1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 224
    iget-object v1, p0, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->this$0:Lcom/samsung/android/app/memo/util/ImageLoader;

    # invokes: Lcom/samsung/android/app/memo/util/ImageLoader;->setImageBitmap(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    invoke-static {v1, v0, p1}, Lcom/samsung/android/app/memo/util/ImageLoader;->access$3(Lcom/samsung/android/app/memo/util/ImageLoader;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    .line 226
    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
