.class Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$5;
.super Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;
.source "LinkActionChooserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "$anonymous0"    # I

    .prologue
    .line 338
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;-><init>(I)V

    .line 1
    return-void
.end method


# virtual methods
.method public execute(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/app/Activity;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 342
    const-string v3, "tel:"

    invoke-virtual {p2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 343
    const-string v3, "tel:"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->getContactIDfromNumber(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v0

    .line 346
    .local v0, "contactID":I
    :goto_0
    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    .line 347
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 346
    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 348
    .local v2, "uri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 354
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v3, 0x80000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 355
    invoke-static {p1, v1}, Lcom/samsung/android/app/memo/util/Utils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 356
    return-void

    .line 345
    .end local v0    # "contactID":I
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_0
    const-string v3, "mailto:"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->getContactIDfromMail(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v0

    .restart local v0    # "contactID":I
    goto :goto_0
.end method
