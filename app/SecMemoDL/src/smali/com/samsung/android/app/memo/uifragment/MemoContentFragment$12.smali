.class Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$12;
.super Ljava/lang/Thread;
.source "MemoContentFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->resetFragment(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

.field private final synthetic val$ss:Lcom/samsung/android/app/memo/Session;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Lcom/samsung/android/app/memo/Session;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$12;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iput-object p2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$12;->val$ss:Lcom/samsung/android/app/memo/Session;

    .line 1120
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 1123
    const-string v0, ""

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$12;->val$ss:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Session;->getCategoryUUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1124
    const-string v0, "lookup category UUID for during call"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$12;->setName(Ljava/lang/String;)V

    .line 1125
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$12;->val$ss:Lcom/samsung/android/app/memo/Session;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$12;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/app/memo/util/Utils;->findCategoryUUIDofDuringCall(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/Session;->setCategoryUUID(Ljava/lang/String;)V

    .line 1127
    :cond_0
    return-void
.end method
