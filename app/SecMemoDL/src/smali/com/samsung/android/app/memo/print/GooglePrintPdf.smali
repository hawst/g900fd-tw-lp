.class public Lcom/samsung/android/app/memo/print/GooglePrintPdf;
.super Ljava/lang/Object;
.source "GooglePrintPdf.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mFile:Ljava/io/File;

.field private mFileList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mJobName:Ljava/lang/String;

.field private pagesCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->pagesCount:I

    .line 55
    iput-object v1, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFileList:Ljava/util/List;

    .line 57
    iput-object v1, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFile:Ljava/io/File;

    .line 49
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)Ljava/util/List;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFileList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/print/GooglePrintPdf;I)V
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->pagesCount:I

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->pagesCount:I

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mJobName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)Ljava/io/File;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$5()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public printBitmap(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/print/PrintManager$PrintJobStateChangeListener;)Landroid/print/PrintJob;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "jobName"    # Ljava/lang/String;
    .param p3, "filePath"    # Ljava/lang/String;
    .param p4, "printJobStateChangeListener"    # Landroid/print/PrintManager$PrintJobStateChangeListener;

    .prologue
    const/4 v1, 0x0

    .line 63
    if-nez p3, :cond_0

    .line 76
    :goto_0
    return-object v1

    .line 66
    :cond_0
    if-nez p2, :cond_1

    .line 67
    const-string v2, "MobilePrint"

    iput-object v2, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mJobName:Ljava/lang/String;

    .line 71
    :goto_1
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFile:Ljava/io/File;

    .line 72
    iget-object v2, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFileList:Ljava/util/List;

    .line 74
    const-string v2, "print"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrintManager;

    .line 75
    .local v0, "printManager":Landroid/print/PrintManager;
    invoke-virtual {v0, p4}, Landroid/print/PrintManager;->addPrintJobStateChangeListener(Landroid/print/PrintManager$PrintJobStateChangeListener;)V

    .line 76
    iget-object v2, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mJobName:Ljava/lang/String;

    new-instance v3, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;

    invoke-direct {v3, p0, p1}, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;-><init>(Lcom/samsung/android/app/memo/print/GooglePrintPdf;Landroid/content/Context;)V

    invoke-virtual {v0, v2, v3, v1}, Landroid/print/PrintManager;->print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;

    move-result-object v1

    goto :goto_0

    .line 69
    .end local v0    # "printManager":Landroid/print/PrintManager;
    :cond_1
    iput-object p2, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mJobName:Ljava/lang/String;

    goto :goto_1
.end method
