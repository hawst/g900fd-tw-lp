.class public Lcom/samsung/android/app/memo/Main;
.super Landroid/app/Activity;
.source "Main.java"

# interfaces
.implements Landroid/view/KeyEvent$Callback;
.implements Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/Main$Mode;
    }
.end annotation


# static fields
.field private static final EXTRA_FROM_CALL_APP:Ljava/lang/String; = "_fromCallApp"

.field public static final EXTRA_MESSAGE_ID:Ljava/lang/String; = "MESSAGE_ID"

.field public static final EXTRA_OBJECT:Ljava/lang/String; = "OBJECT"

.field private static final EXTRA_PHONENUMBER:Ljava/lang/String; = "_phoneNum"

.field private static final EXTRA_TITLE:Ljava/lang/String; = "android.intent.extra.TITLE"

.field public static final KEY_DISPLAYNAME:Ljava/lang/String;

.field private static final KEY_DURING_CALL:Ljava/lang/String;

.field public static final KEY_MIMETYPE:Ljava/lang/String;

.field private static final KEY_MODE:Ljava/lang/String;

.field public static final KEY_ORIENTATION:Ljava/lang/String;

.field public static final KIES_INTENT_RESTORE_REQUEST:Ljava/lang/String; = "com.sec.android.intent.action.REQUEST_RESTORE_MEMO"

.field public static final KIES_INTENT_TMEMO1_REQUEST:Ljava/lang/String; = "com.sec.android.memo.KIES_RESTORES_AMEMO"

.field public static final MIME_TYPE_SBEAM:Ljava/lang/String; = "application/vnd.samsung.android.memo"

.field public static final MIME_TYPE_UNKNOWN:Ljava/lang/String; = "application/octet-stream"

.field public static final MIME_TYPE_VNOTE:Ljava/lang/String; = "text/x-vnote"

.field public static final MSG_SHOW_UNDOBAR:I = 0x0

.field private static final OPEN_MEMO:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field public static final URI_PATH:Ljava/lang/String; = "uri_path"

.field public static final VNT_FILE_EXTENSION:Ljava/lang/String; = ".vnt"

.field public static mIsUiPickerMode:Z


# instance fields
.field private final DELAY_COMMON:I

.field private mABDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

.field private mCategoryChangeHandler:Landroid/os/Handler;

.field private mCategoryChangeRunnable:Ljava/lang/Runnable;

.field private mCategoryListContainer:Landroid/widget/FrameLayout;

.field private volatile mCategoryListFragment:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

.field mCurrentFragment:Landroid/app/Fragment;

.field private mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

.field private mDuringCall:Landroid/os/Bundle;

.field private mFileUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field private mIsDrawerClosing:Z

.field private mIsDrawerOpen:Ljava/lang/Boolean;

.field private mIsFromDragListener:Z

.field private mIsFromVntFile:Z

.field public mIsFromWidget:Z

.field public volatile mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

.field private mMode:Lcom/samsung/android/app/memo/Main$Mode;

.field private mSession:Lcom/samsung/android/app/memo/Session;

.field private mStrBodyText:Ljava/lang/String;

.field private mStrTitleText:Ljava/lang/String;

.field private mStrVntContent:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;

.field public widgetId:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 93
    const-class v0, Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_mode"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Main;->KEY_MODE:Ljava/lang/String;

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_duringCall"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Main;->KEY_DURING_CALL:Ljava/lang/String;

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_orientation"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Main;->KEY_ORIENTATION:Ljava/lang/String;

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_displayname"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Main;->KEY_DISPLAYNAME:Ljava/lang/String;

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Main;->KEY_MIMETYPE:Ljava/lang/String;

    .line 200
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 92
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 118
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/Main;->mStrVntContent:Ljava/lang/String;

    .line 124
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/Main;->mIsFromVntFile:Z

    .line 126
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/Main;->mIsFromDragListener:Z

    .line 128
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/Main;->mIsFromWidget:Z

    .line 130
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/app/memo/Main;->widgetId:J

    .line 178
    iput-object v3, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    .line 179
    iput-object v3, p0, Lcom/samsung/android/app/memo/Main;->mCategoryListFragment:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    .line 186
    sget-object v0, Lcom/samsung/android/app/memo/Main$Mode;->UNKNOWN:Lcom/samsung/android/app/memo/Main$Mode;

    iput-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    .line 187
    iput-object v3, p0, Lcom/samsung/android/app/memo/Main;->mUri:Landroid/net/Uri;

    .line 194
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Main;->mIsDrawerOpen:Ljava/lang/Boolean;

    .line 195
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/Main;->mIsDrawerClosing:Z

    .line 205
    const/16 v0, 0x190

    iput v0, p0, Lcom/samsung/android/app/memo/Main;->DELAY_COMMON:I

    .line 92
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/Main;)Landroid/support/v4/widget/DrawerLayout;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/Main;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/Main;)Lcom/samsung/android/app/memo/Main$Mode;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/Main;Landroid/net/Uri;Ljava/lang/String;ZI)V
    .locals 0

    .prologue
    .line 774
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/app/memo/Main;->requestMemoContentFragment(Landroid/net/Uri;Ljava/lang/String;ZI)V

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/Main;)V
    .locals 0

    .prologue
    .line 913
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Main;->startCategoryManagementFragment()V

    return-void
.end method

.method private getCategoryListFragment()Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCategoryListFragment:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    if-nez v0, :cond_1

    .line 295
    monitor-enter p0

    .line 296
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCategoryListFragment:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    if-nez v0, :cond_0

    .line 297
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    invoke-direct {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCategoryListFragment:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    .line 295
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 301
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCategoryListFragment:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    return-object v0

    .line 295
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private getListFragment()Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    if-nez v0, :cond_1

    .line 284
    monitor-enter p0

    .line 285
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    if-nez v0, :cond_0

    .line 286
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-direct {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    .line 284
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    return-object v0

    .line 284
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private hasDrawerLayout(Landroid/app/FragmentTransaction;Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;)V
    .locals 4
    .param p1, "transaction"    # Landroid/app/FragmentTransaction;
    .param p2, "listFragment"    # Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    .param p3, "categoryListFragment"    # Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    .prologue
    const v3, 0x7f0e0027

    .line 981
    iget-object v1, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v1, :cond_0

    .line 983
    const v1, 0x7f0e0027

    :try_start_0
    invoke-virtual {p1, v1, p2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 990
    :cond_0
    :goto_0
    return-void

    .line 984
    :catch_0
    move-exception v0

    .line 985
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v1, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    const-string v2, "hasDrawerLayout"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 986
    invoke-virtual {p1, p2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 987
    invoke-virtual {p1, v3, p2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0
.end method

.method private hideApplicationIcon()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 996
    const v2, 0x102002c

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 997
    .local v0, "homeView":Landroid/view/View;
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 998
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 999
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1000
    .local v1, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1001
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1002
    return-void
.end method

.method private initFragments()V
    .locals 10

    .prologue
    const/16 v9, 0x1003

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 689
    sget-object v3, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "initFragments() for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    sget-object v3, Lcom/samsung/android/app/memo/Main$Mode;->VIEW:Lcom/samsung/android/app/memo/Main$Mode;

    iget-object v4, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/samsung/android/app/memo/Main$Mode;->EDIT:Lcom/samsung/android/app/memo/Main$Mode;

    iget-object v4, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 694
    :cond_0
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/samsung/android/app/memo/Main$4;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/memo/Main$4;-><init>(Lcom/samsung/android/app/memo/Main;)V

    .line 700
    const-wide/16 v6, 0x190

    .line 694
    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 772
    :cond_1
    :goto_0
    return-void

    .line 702
    :cond_2
    sget-object v3, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_IMAGE:Lcom/samsung/android/app/memo/Main$Mode;

    iget-object v4, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    sget-object v3, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_TEXT:Lcom/samsung/android/app/memo/Main$Mode;

    iget-object v4, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    sget-object v3, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_AUDIO:Lcom/samsung/android/app/memo/Main$Mode;

    iget-object v4, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 703
    :cond_3
    iget-boolean v3, p0, Lcom/samsung/android/app/memo/Main;->mIsFromDragListener:Z

    if-eqz v3, :cond_5

    .line 704
    iget-object v3, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->isInActionmode()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 705
    iget-object v3, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->finishActionMode()V

    .line 706
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/app/memo/Main;->mUri:Landroid/net/Uri;

    const/high16 v4, 0x100000

    invoke-direct {p0, v3, v6, v8, v4}, Lcom/samsung/android/app/memo/Main;->requestMemoContentFragment(Landroid/net/Uri;Ljava/lang/String;ZI)V

    .line 710
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Main;->getListFragment()Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->isAdded()Z

    move-result v3

    if-nez v3, :cond_6

    .line 711
    invoke-virtual {p0, v7, v7}, Lcom/samsung/android/app/memo/Main;->overridePendingTransition(II)V

    .line 712
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->finish()V

    goto :goto_0

    .line 708
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/app/memo/Main;->mUri:Landroid/net/Uri;

    invoke-direct {p0, v3, v6, v8, v7}, Lcom/samsung/android/app/memo/Main;->requestMemoContentFragment(Landroid/net/Uri;Ljava/lang/String;ZI)V

    goto :goto_1

    .line 715
    :cond_6
    sget-object v3, Lcom/samsung/android/app/memo/Main$Mode;->NORMAL:Lcom/samsung/android/app/memo/Main$Mode;

    iput-object v3, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    goto :goto_0

    .line 717
    :cond_7
    sget-object v3, Lcom/samsung/android/app/memo/Main$Mode;->CALLNOTE_VIEW:Lcom/samsung/android/app/memo/Main$Mode;

    iget-object v4, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 718
    iget-object v3, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/samsung/android/app/memo/Main;->mCategoryListFragment:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    if-eqz v3, :cond_8

    .line 719
    sget-object v3, Lcom/samsung/android/app/memo/Main$Mode;->NORMAL:Lcom/samsung/android/app/memo/Main$Mode;

    iget-object v4, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 724
    :cond_8
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Main;->getListFragment()Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v1

    .line 725
    .local v1, "listFragment":Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Main;->getCategoryListFragment()Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    move-result-object v0

    .line 726
    .local v0, "categoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 727
    invoke-virtual {v3, v9}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 729
    .local v2, "transaction":Landroid/app/FragmentTransaction;
    invoke-virtual {v2, v7}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 730
    invoke-direct {p0, v2, v1, v0}, Lcom/samsung/android/app/memo/Main;->hasDrawerLayout(Landroid/app/FragmentTransaction;Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;)V

    .line 732
    iget-object v3, p0, Lcom/samsung/android/app/memo/Main;->mUri:Landroid/net/Uri;

    sget-object v4, Lcom/samsung/android/app/memo/Main$Mode;->EDIT:Lcom/samsung/android/app/memo/Main$Mode;

    iget-object v5, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-direct {p0, v3, v6, v4, v7}, Lcom/samsung/android/app/memo/Main;->requestMemoContentFragment(Landroid/net/Uri;Ljava/lang/String;ZI)V

    .line 733
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    goto/16 :goto_0

    .line 736
    .end local v0    # "categoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    .end local v1    # "listFragment":Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    .end local v2    # "transaction":Landroid/app/FragmentTransaction;
    :cond_9
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Main;->getListFragment()Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v1

    .line 737
    .restart local v1    # "listFragment":Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    iget-object v3, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/samsung/android/app/memo/Main;->mCategoryListFragment:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    if-eqz v3, :cond_a

    .line 738
    sget-object v3, Lcom/samsung/android/app/memo/Main$Mode;->NORMAL:Lcom/samsung/android/app/memo/Main$Mode;

    iget-object v4, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 742
    :cond_a
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Main;->getCategoryListFragment()Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    move-result-object v0

    .line 743
    .restart local v0    # "categoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 744
    invoke-virtual {v3, v9}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 746
    .restart local v2    # "transaction":Landroid/app/FragmentTransaction;
    sget-object v3, Lcom/samsung/android/app/memo/Main$Mode;->NORMAL:Lcom/samsung/android/app/memo/Main$Mode;

    iget-object v4, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 748
    invoke-direct {p0, v2, v1, v0}, Lcom/samsung/android/app/memo/Main;->hasDrawerLayout(Landroid/app/FragmentTransaction;Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;)V

    .line 770
    :cond_b
    :goto_2
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    goto/16 :goto_0

    .line 750
    :cond_c
    sget-object v3, Lcom/samsung/android/app/memo/Main$Mode;->CALLNOTE:Lcom/samsung/android/app/memo/Main$Mode;

    iget-object v4, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 751
    invoke-virtual {v2, v7}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 752
    invoke-direct {p0, v2, v1, v0}, Lcom/samsung/android/app/memo/Main;->hasDrawerLayout(Landroid/app/FragmentTransaction;Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;)V

    .line 753
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v3

    if-lez v3, :cond_d

    .line 754
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->popBackStack()V

    .line 758
    :cond_d
    invoke-static {}, Lcom/samsung/android/app/memo/MemoApp;->isCreateMemo()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 759
    const v3, 0x10008000

    invoke-direct {p0, v6, v6, v8, v3}, Lcom/samsung/android/app/memo/Main;->requestMemoContentFragment(Landroid/net/Uri;Ljava/lang/String;ZI)V

    goto :goto_2

    .line 762
    :cond_e
    const/high16 v3, 0x24000000

    invoke-direct {p0, v6, v6, v8, v3}, Lcom/samsung/android/app/memo/Main;->requestMemoContentFragment(Landroid/net/Uri;Ljava/lang/String;ZI)V

    goto :goto_2

    .line 765
    :cond_f
    sget-object v3, Lcom/samsung/android/app/memo/Main$Mode;->PICK_ONE:Lcom/samsung/android/app/memo/Main$Mode;

    iget-object v4, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 766
    invoke-direct {p0, v2, v1, v0}, Lcom/samsung/android/app/memo/Main;->hasDrawerLayout(Landroid/app/FragmentTransaction;Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;)V

    .line 767
    sget-object v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_ONE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    invoke-virtual {v1, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->setActionModeStyle(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;)V

    goto :goto_2
.end method

.method private onIntentActionSend(Landroid/content/Intent;)Lcom/samsung/android/app/memo/Main$Mode;
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 508
    const-string v6, "_fromCallApp"

    invoke-virtual {p1, v6, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 511
    const-string v6, "phone"

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/memo/Main;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/telephony/TelephonyManager;

    .line 512
    invoke-virtual {v6}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v6

    .line 511
    if-nez v6, :cond_0

    const/4 v3, 0x1

    .line 513
    .local v3, "stopCallNote":Z
    :cond_0
    if-eqz v3, :cond_2

    .line 514
    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->NORMAL:Lcom/samsung/android/app/memo/Main$Mode;

    .line 570
    .end local v3    # "stopCallNote":Z
    :cond_1
    :goto_0
    return-object v2

    .line 516
    .restart local v3    # "stopCallNote":Z
    :cond_2
    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->CALLNOTE:Lcom/samsung/android/app/memo/Main$Mode;

    .line 517
    .local v2, "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    iget-object v6, p0, Lcom/samsung/android/app/memo/Main;->mDuringCall:Landroid/os/Bundle;

    const-string v8, "android.intent.extra.TITLE"

    const-string v9, "android.intent.extra.TITLE"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    iget-object v6, p0, Lcom/samsung/android/app/memo/Main;->mDuringCall:Landroid/os/Bundle;

    const-string v8, "_phoneNum"

    const-string v9, "_phoneNum"

    invoke-virtual {p1, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    iget-object v6, p0, Lcom/samsung/android/app/memo/Main;->mCategoryListFragment:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    if-eqz v6, :cond_3

    .line 521
    iput-object v7, p0, Lcom/samsung/android/app/memo/Main;->mCategoryListFragment:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    .line 523
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    if-eqz v6, :cond_1

    .line 524
    iput-object v7, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    goto :goto_0

    .line 526
    .end local v2    # "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    .end local v3    # "stopCallNote":Z
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_6

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v6

    const-string v8, "image"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 527
    iget-object v6, p0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    const-string v7, "android.intent.extra.STREAM"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 528
    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_IMAGE:Lcom/samsung/android/app/memo/Main$Mode;

    .line 529
    .restart local v2    # "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    const-string v6, "android.intent.extra.STREAM"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    if-nez v6, :cond_5

    const-string v6, "android.intent.extra.TEXT"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 531
    const-string v6, "android.intent.extra.TEXT"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    .line 532
    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_TEXT:Lcom/samsung/android/app/memo/Main$Mode;

    .line 533
    goto :goto_0

    .line 534
    :cond_5
    const-string v6, "android.intent.extra.STREAM"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 535
    const-string v6, "android.intent.extra.TEXT"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 536
    const-string v6, "android.intent.extra.TEXT"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    goto :goto_0

    .line 538
    .end local v2    # "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    :cond_6
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_8

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v6

    const-string v8, "text"

    invoke-virtual {v6, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 539
    const-string v6, "android.intent.extra.TEXT"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    .line 540
    iget-object v6, p0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    if-nez v6, :cond_7

    const-string v6, "android.intent.extra.STREAM"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 541
    iget-object v6, p0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {p1, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 544
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    .line 545
    const v8, 0x7f0b0059

    .line 544
    invoke-static {v6, v8}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 546
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->finish()V

    move-object v2, v7

    .line 547
    goto/16 :goto_0

    .line 549
    :cond_7
    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_TEXT:Lcom/samsung/android/app/memo/Main$Mode;

    .line 550
    .restart local v2    # "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    goto/16 :goto_0

    .end local v2    # "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    :cond_8
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v6

    const-string v7, "audio"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 551
    iget-object v6, p0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    const-string v7, "android.intent.extra.STREAM"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 552
    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_AUDIO:Lcom/samsung/android/app/memo/Main$Mode;

    .line 553
    .restart local v2    # "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    goto/16 :goto_0

    .end local v2    # "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    :cond_9
    invoke-virtual {p1}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v6

    if-eqz v6, :cond_d

    .line 554
    invoke-virtual {p1}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    .line 555
    .local v0, "cd":Landroid/content/ClipData;
    const-string v5, ""

    .line 556
    .local v5, "tmpBodyStr":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v6

    if-lt v1, v6, :cond_a

    .line 561
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_c

    .line 562
    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_TEXT:Lcom/samsung/android/app/memo/Main$Mode;

    .line 563
    .restart local v2    # "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    iput-object v5, p0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    goto/16 :goto_0

    .line 557
    .end local v2    # "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    :cond_a
    invoke-virtual {v0, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 558
    .local v4, "tmp":Ljava/lang/String;
    if-eqz v4, :cond_b

    .line 559
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 556
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 565
    .end local v4    # "tmp":Ljava/lang/String;
    :cond_c
    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->EDIT:Lcom/samsung/android/app/memo/Main$Mode;

    .line 566
    .restart local v2    # "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    goto/16 :goto_0

    .line 568
    .end local v0    # "cd":Landroid/content/ClipData;
    .end local v1    # "i":I
    .end local v2    # "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    .end local v5    # "tmpBodyStr":Ljava/lang/String;
    :cond_d
    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->EDIT:Lcom/samsung/android/app/memo/Main$Mode;

    .restart local v2    # "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    goto/16 :goto_0
.end method

.method private onIntentActionSendMultiple(Landroid/content/Intent;)Lcom/samsung/android/app/memo/Main$Mode;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 493
    sget-object v0, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_IMAGE:Lcom/samsung/android/app/memo/Main$Mode;

    .line 495
    .local v0, "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    .line 496
    iget-object v1, p0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 497
    iget-object v1, p0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 498
    sget-object v0, Lcom/samsung/android/app/memo/Main$Mode;->EDIT:Lcom/samsung/android/app/memo/Main$Mode;

    .line 500
    :cond_0
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    .line 502
    return-object v0
.end method

.method private readTextFileContent()Ljava/lang/String;
    .locals 13

    .prologue
    .line 574
    const-string v10, ""

    iput-object v10, p0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    .line 575
    const/4 v5, 0x0

    .line 576
    .local v5, "inputFileStream":Ljava/io/InputStream;
    const/4 v0, 0x0

    .line 577
    .local v0, "br":Ljava/io/BufferedReader;
    const/4 v7, 0x0

    .line 578
    .local v7, "totalContent":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 579
    .local v2, "contentLine":Ljava/lang/String;
    iget-object v10, p0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    if-eqz v10, :cond_0

    .line 580
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v10, p0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v4, v10, :cond_1

    .line 611
    .end local v4    # "i":I
    :cond_0
    iget-object v10, p0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    return-object v10

    .line 581
    .restart local v4    # "i":I
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/Uri;

    .line 582
    .local v9, "uri":Landroid/net/Uri;
    invoke-virtual {v9}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v10

    const-string v11, "file"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 583
    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/net/URLConnection;->guessContentTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 584
    .local v6, "mimeType":Ljava/lang/String;
    if-eqz v6, :cond_2

    const-string v10, "text"

    invoke-virtual {v6, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 587
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v5

    .line 588
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    invoke-direct {v10, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 589
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :try_start_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 590
    .end local v7    # "totalContent":Ljava/lang/StringBuilder;
    .local v8, "totalContent":Ljava/lang/StringBuilder;
    :try_start_2
    const-string v2, ""

    .line 591
    :goto_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    .line 594
    iget-object v10, p0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v11, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 596
    if-eqz v1, :cond_5

    .line 597
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    move-object v7, v8

    .end local v8    # "totalContent":Ljava/lang/StringBuilder;
    .restart local v7    # "totalContent":Ljava/lang/StringBuilder;
    move-object v0, v1

    .line 580
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v6    # "mimeType":Ljava/lang/String;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :cond_2
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 592
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v7    # "totalContent":Ljava/lang/StringBuilder;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v6    # "mimeType":Ljava/lang/String;
    .restart local v8    # "totalContent":Ljava/lang/StringBuilder;
    :cond_3
    :try_start_4
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 595
    :catchall_0
    move-exception v10

    move-object v7, v8

    .end local v8    # "totalContent":Ljava/lang/StringBuilder;
    .restart local v7    # "totalContent":Ljava/lang/StringBuilder;
    move-object v0, v1

    .line 596
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_3
    if-eqz v0, :cond_4

    .line 597
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 599
    :cond_4
    throw v10
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 600
    :catch_0
    move-exception v3

    .line 601
    .local v3, "e":Ljava/io/FileNotFoundException;
    :goto_4
    sget-object v10, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "File Not Found Exception :"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 602
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v3

    .line 603
    .local v3, "e":Ljava/io/IOException;
    :goto_5
    sget-object v10, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    .line 604
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "IOException exception during reading the data from the file :"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 605
    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    .line 604
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 603
    invoke-static {v10, v11}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 602
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v7    # "totalContent":Ljava/lang/StringBuilder;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "totalContent":Ljava/lang/StringBuilder;
    :catch_2
    move-exception v3

    move-object v7, v8

    .end local v8    # "totalContent":Ljava/lang/StringBuilder;
    .restart local v7    # "totalContent":Ljava/lang/StringBuilder;
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_5

    .line 600
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v7    # "totalContent":Ljava/lang/StringBuilder;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "totalContent":Ljava/lang/StringBuilder;
    :catch_3
    move-exception v3

    move-object v7, v8

    .end local v8    # "totalContent":Ljava/lang/StringBuilder;
    .restart local v7    # "totalContent":Ljava/lang/StringBuilder;
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_4

    .line 595
    :catchall_1
    move-exception v10

    goto :goto_3

    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catchall_2
    move-exception v10

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_3

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v7    # "totalContent":Ljava/lang/StringBuilder;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v8    # "totalContent":Ljava/lang/StringBuilder;
    :cond_5
    move-object v7, v8

    .end local v8    # "totalContent":Ljava/lang/StringBuilder;
    .restart local v7    # "totalContent":Ljava/lang/StringBuilder;
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method private requestMemoContentFragment(Landroid/net/Uri;Ljava/lang/String;ZI)V
    .locals 22
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "categoryUUID"    # Ljava/lang/String;
    .param p3, "editMode"    # Z
    .param p4, "flags"    # I

    .prologue
    .line 775
    sget-object v19, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "requestMemoContentFragment("

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x29

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 778
    .local v4, "args":Landroid/os/Bundle;
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_EDITMODE:Ljava/lang/String;

    move-object/from16 v0, v19

    move/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 779
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    sget-object v19, Lcom/samsung/android/app/memo/Main$Mode;->CALLNOTE:Lcom/samsung/android/app/memo/Main$Mode;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 781
    const-string v19, "phone"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/Main;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/telephony/TelephonyManager;

    invoke-virtual/range {v19 .. v19}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v19

    if-nez v19, :cond_c

    const/16 v18, 0x1

    .line 788
    .local v18, "stopCallNote":Z
    :goto_0
    if-eqz v18, :cond_d

    .line 789
    sget-object v19, Lcom/samsung/android/app/memo/Main$Mode;->NORMAL:Lcom/samsung/android/app/memo/Main$Mode;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    .line 799
    .end local v18    # "stopCallNote":Z
    :cond_0
    :goto_1
    new-instance v19, Landroid/content/Intent;

    invoke-direct/range {v19 .. v19}, Landroid/content/Intent;-><init>()V

    const-class v20, Lcom/samsung/android/app/memo/MemoContentActivity;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v12

    .line 800
    .local v12, "intent":Landroid/content/Intent;
    sget-object v19, Lcom/samsung/android/app/memo/MemoContentActivity;->KEY_BUNDLE:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v12, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 801
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/Main;->mIsFromWidget:Z

    move/from16 v19, v0

    if-eqz v19, :cond_1

    .line 802
    const-string v19, "from_widget"

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/Main;->mIsFromWidget:Z

    move/from16 v20, v0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 803
    const-string v19, "appWidgetId"

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/app/memo/Main;->widgetId:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v19

    move-wide/from16 v1, v20

    invoke-virtual {v12, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 805
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/Main;->mIsFromDragListener:Z

    move/from16 v19, v0

    if-eqz v19, :cond_2

    .line 806
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->PREF_CATEGORY:Ljava/lang/String;

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/memo/Main;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v15

    .line 807
    .local v15, "pref":Landroid/content/SharedPreferences;
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    const-string v20, ""

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v15, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 808
    .local v13, "mCategoryUUID":Ljava/lang/String;
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    .end local v13    # "mCategoryUUID":Ljava/lang/String;
    .end local v15    # "pref":Landroid/content/SharedPreferences;
    :cond_2
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v19

    const-string v20, "ExternalPicker"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 812
    const-string v19, "isUiPickerMode"

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 814
    :cond_3
    sget-object v19, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_IMAGE:Lcom/samsung/android/app/memo/Main$Mode;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_13

    .line 815
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mStrTitleText:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_4

    .line 816
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mStrTitleText:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    const/16 v20, 0x1e

    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_f

    .line 817
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TITLESTRINGEXTRA:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mStrTitleText:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 825
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_5

    .line 826
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_BODYSTRINGEXTRA:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 829
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    if-eqz v19, :cond_6

    .line 830
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 831
    .local v9, "gmailImageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v16

    .line 833
    .local v16, "receiveMultiUrisSize":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_3
    move/from16 v0, v16

    if-lt v10, v0, :cond_11

    .line 854
    if-eqz v9, :cond_6

    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v19

    if-nez v19, :cond_6

    .line 855
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    .line 856
    const-string v19, "isToBeDeleted"

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 859
    .end local v9    # "gmailImageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    .end local v10    # "i":I
    .end local v16    # "receiveMultiUrisSize":I
    :cond_6
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_ACTION_SEND_IMAGE_URI:Ljava/lang/String;

    .line 860
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    .line 859
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 861
    const-string v19, "isFromShareVia"

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 867
    :cond_7
    :goto_4
    sget-object v19, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_TEXT:Lcom/samsung/android/app/memo/Main$Mode;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 868
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mStrTitleText:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_8

    .line 869
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TITLESTRINGEXTRA:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mStrTitleText:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 871
    :cond_8
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_BODYSTRINGEXTRA:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 872
    const-string v19, "isFromShareVia"

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 874
    :cond_9
    sget-object v19, Lcom/samsung/android/app/memo/MemoContentActivity;->KEY_URI:Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 875
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->IS_FROM_DRAG_LISTENER:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/Main;->mIsFromDragListener:Z

    move/from16 v20, v0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 876
    move/from16 v0, p4

    invoke-virtual {v12, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 878
    sget-object v19, Lcom/samsung/android/app/memo/Main$Mode;->VIEW:Lcom/samsung/android/app/memo/Main$Mode;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_a

    sget-object v19, Lcom/samsung/android/app/memo/Main$Mode;->EDIT:Lcom/samsung/android/app/memo/Main$Mode;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_14

    .line 879
    :cond_a
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VNTFILE:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/Main;->mIsFromVntFile:Z

    move/from16 v20, v0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 880
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mStrVntContent:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_b

    .line 881
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VNTTEXT:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mStrVntContent:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 882
    :cond_b
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/samsung/android/app/memo/Main;->startActivity(Landroid/content/Intent;)V

    .line 883
    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/memo/Main;->overridePendingTransition(II)V

    .line 887
    :goto_5
    return-void

    .line 781
    .end local v12    # "intent":Landroid/content/Intent;
    :cond_c
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 791
    .restart local v18    # "stopCallNote":Z
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v19, v0

    if-eqz v19, :cond_e

    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->isInActionmode()Z

    move-result v19

    if-eqz v19, :cond_e

    .line 792
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->finishActionMode()V

    .line 793
    :cond_e
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_DURINGCALL:Ljava/lang/String;

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 794
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TITLEPREFIX:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mDuringCall:Landroid/os/Bundle;

    move-object/from16 v20, v0

    const-string v21, "android.intent.extra.TITLE"

    invoke-virtual/range {v20 .. v21}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_PHONENUMBER:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mDuringCall:Landroid/os/Bundle;

    move-object/from16 v20, v0

    const-string v21, "_phoneNum"

    invoke-virtual/range {v20 .. v21}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 819
    .end local v18    # "stopCallNote":Z
    .restart local v12    # "intent":Landroid/content/Intent;
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    move-object/from16 v19, v0

    if-eqz v19, :cond_10

    .line 820
    new-instance v19, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mStrTitleText:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, "\n"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    goto/16 :goto_2

    .line 822
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mStrTitleText:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/Main;->mStrBodyText:Ljava/lang/String;

    goto/16 :goto_2

    .line 834
    .restart local v9    # "gmailImageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    .restart local v10    # "i":I
    .restart local v16    # "receiveMultiUrisSize":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/net/Uri;

    .line 835
    .local v11, "image_uri":Landroid/net/Uri;
    if-eqz v11, :cond_12

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    const-string v20, "content://gmail"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_12

    .line 836
    invoke-static {v11}, Lcom/samsung/android/app/memo/util/ImageUtil;->lookupImageInfo(Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v6

    .line 837
    .local v6, "bundle":Landroid/os/Bundle;
    invoke-virtual {v12, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 839
    sget-object v19, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_ORIENTATION:Ljava/lang/String;

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v14

    .line 840
    .local v14, "orientation":I
    sget-object v19, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_DISPLAYNAME:Ljava/lang/String;

    const-string v20, "null"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 842
    .local v7, "displayName":Ljava/lang/String;
    :try_start_0
    new-instance v17, Lcom/samsung/android/app/memo/Session;

    const-wide/16 v20, -0x1

    move-object/from16 v0, v17

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/Session;-><init>(J)V

    .line 844
    .local v17, "ss":Lcom/samsung/android/app/memo/Session;
    const-string v19, "image/jpeg"

    .line 843
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v11, v7, v1, v14}, Lcom/samsung/android/app/memo/Session;->addFile(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v5

    .line 845
    .local v5, "attachmentUri":Landroid/net/Uri;
    if-eqz v5, :cond_12

    .line 846
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 833
    .end local v5    # "attachmentUri":Landroid/net/Uri;
    .end local v6    # "bundle":Landroid/os/Bundle;
    .end local v7    # "displayName":Ljava/lang/String;
    .end local v14    # "orientation":I
    .end local v17    # "ss":Lcom/samsung/android/app/memo/Session;
    :cond_12
    :goto_6
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_3

    .line 848
    .restart local v6    # "bundle":Landroid/os/Bundle;
    .restart local v7    # "displayName":Ljava/lang/String;
    .restart local v14    # "orientation":I
    :catch_0
    move-exception v8

    .line 849
    .local v8, "e":Lcom/samsung/android/app/memo/Session$SessionException;
    sget-object v19, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    const-string v20, "requestMemoContentFragment"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v8}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 862
    .end local v6    # "bundle":Landroid/os/Bundle;
    .end local v7    # "displayName":Ljava/lang/String;
    .end local v8    # "e":Lcom/samsung/android/app/memo/Session$SessionException;
    .end local v9    # "gmailImageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    .end local v10    # "i":I
    .end local v11    # "image_uri":Landroid/net/Uri;
    .end local v14    # "orientation":I
    .end local v16    # "receiveMultiUrisSize":I
    :cond_13
    sget-object v19, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_AUDIO:Lcom/samsung/android/app/memo/Main$Mode;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/android/app/memo/Main$Mode;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 863
    sget-object v19, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_ACTION_SEND_AUDIO_URI:Ljava/lang/String;

    .line 864
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    .line 863
    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 865
    const-string v19, "isFromShareVia"

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_4

    .line 885
    :cond_14
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v12, v1}, Lcom/samsung/android/app/memo/Main;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_5
.end method

.method private savePreferences(Ljava/lang/String;)V
    .locals 11
    .param p1, "mCategoryUUID"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x0

    .line 468
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    .line 469
    const-string v0, "_id"

    aput-object v0, v2, v9

    const/4 v0, 0x1

    const-string v4, "UUID"

    aput-object v4, v2, v0

    const-string v0, "_display_name"

    aput-object v0, v2, v10

    .line 471
    .local v2, "mProjection":[Ljava/lang/String;
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-static {v0, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 473
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 475
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 476
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->PREF_CATEGORY:Ljava/lang/String;

    invoke-virtual {p0, v0, v9}, Lcom/samsung/android/app/memo/Main;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    .line 477
    .local v8, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 478
    .local v7, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    invoke-interface {v7, v0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 479
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TITLE:Ljava/lang/String;

    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v7, v0, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 480
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_IS_CATEGORY_CHANGE:Ljava/lang/String;

    invoke-interface {v7, v0, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 482
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TYPE:Ljava/lang/String;

    .line 484
    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 483
    invoke-static {p0, p1, v3}, Lcom/samsung/android/app/memo/util/Utils;->getCategoryType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 481
    invoke-interface {v7, v0, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 485
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 487
    .end local v7    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v8    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    if-eqz v6, :cond_1

    .line 488
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 490
    :cond_1
    return-void
.end method

.method private setDrawerEnabled(Z)V
    .locals 7
    .param p1, "enabled"    # Z

    .prologue
    .line 931
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-nez v0, :cond_0

    .line 978
    :goto_0
    return-void

    .line 934
    :cond_0
    if-eqz p1, :cond_3

    .line 935
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mABDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    if-nez v0, :cond_1

    .line 936
    new-instance v0, Lcom/samsung/android/app/memo/Main$5;

    iget-object v3, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 937
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isFonbletHD()Z

    move-result v1

    if-eqz v1, :cond_2

    const v4, 0x7f020040

    :goto_1
    const v5, 0x7f0b0024

    const v6, 0x7f0b0025

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/app/memo/Main$5;-><init>(Lcom/samsung/android/app/memo/Main;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V

    .line 936
    iput-object v0, p0, Lcom/samsung/android/app/memo/Main;->mABDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    .line 969
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 970
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x7f02000d

    .line 971
    const/high16 v2, 0x800000

    .line 970
    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/DrawerLayout;->setDrawerShadow(II)V

    .line 972
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Main;->mABDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 973
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mABDrawerToggle:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0}, Landroid/support/v4/app/ActionBarDrawerToggle;->syncState()V

    goto :goto_0

    .line 937
    :cond_2
    const v4, 0x7f02003f

    goto :goto_1

    .line 975
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(I)V

    .line 976
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    goto :goto_0
.end method

.method private startCategoryManagementFragment()V
    .locals 5

    .prologue
    .line 914
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 915
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 916
    .local v1, "transaction":Landroid/app/FragmentTransaction;
    const/16 v2, 0x1001

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 917
    const v2, 0x7f0e0027

    new-instance v3, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    invoke-direct {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;-><init>()V

    invoke-virtual {v1, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 918
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 921
    :try_start_0
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 927
    .end local v1    # "transaction":Landroid/app/FragmentTransaction;
    :cond_0
    :goto_0
    return-void

    .line 922
    .restart local v1    # "transaction":Landroid/app/FragmentTransaction;
    :catch_0
    move-exception v0

    .line 923
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 924
    sget-object v2, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onClick   IllegalStateException :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private toggleTagsDrawer()V
    .locals 6

    .prologue
    const v5, 0x7f0e0004

    const/4 v4, 0x3

    .line 659
    iget-object v3, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v3, :cond_0

    .line 660
    iget-object v3, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v3, v4}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v1

    .line 661
    .local v1, "isOpened":Z
    if-eqz v1, :cond_1

    .line 662
    iget-object v3, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v3, v4}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(I)V

    .line 686
    .end local v1    # "isOpened":Z
    :cond_0
    :goto_0
    return-void

    .line 664
    .restart local v1    # "isOpened":Z
    :cond_1
    const v3, 0x7f0e0028

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/memo/Main;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewStub;

    .line 665
    .local v2, "vstub":Landroid/view/ViewStub;
    const/4 v0, 0x0

    .line 666
    .local v0, "fm":Landroid/widget/LinearLayout;
    if-eqz v2, :cond_2

    .line 667
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .end local v0    # "fm":Landroid/widget/LinearLayout;
    check-cast v0, Landroid/widget/LinearLayout;

    .line 668
    .restart local v0    # "fm":Landroid/widget/LinearLayout;
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 669
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/memo/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "fm":Landroid/widget/LinearLayout;
    check-cast v0, Landroid/widget/LinearLayout;

    .line 673
    .restart local v0    # "fm":Landroid/widget/LinearLayout;
    :goto_1
    if-eqz v0, :cond_0

    .line 674
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/samsung/android/app/memo/Main$3;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/memo/Main$3;-><init>(Lcom/samsung/android/app/memo/Main;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 672
    :cond_2
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/memo/Main;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "fm":Landroid/widget/LinearLayout;
    check-cast v0, Landroid/widget/LinearLayout;

    .restart local v0    # "fm":Landroid/widget/LinearLayout;
    goto :goto_1
.end method


# virtual methods
.method public closeDrawer()V
    .locals 2

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-nez v0, :cond_0

    .line 1042
    :goto_0
    return-void

    .line 1041
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(I)V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1194
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 1195
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 1196
    .local v1, "keyCode":I
    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 1197
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 1198
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1199
    :cond_0
    const/4 v2, 0x0

    .line 1203
    :goto_0
    return v2

    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method public finishActionMode()V
    .locals 1

    .prologue
    .line 1163
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    if-eqz v0, :cond_0

    .line 1164
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->finishActionMode()V

    .line 1166
    :cond_0
    return-void
.end method

.method public getRealPathFromURI(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 9
    .param p1, "contentURI"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 1135
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1137
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1138
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 1139
    .local v7, "idx":I
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/samsung/android/app/memo/util/Utils;->getAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1143
    .end local v7    # "idx":I
    .local v8, "path":Ljava/lang/String;
    :goto_0
    if-eqz v6, :cond_0

    .line 1144
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1145
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 1141
    .end local v8    # "path":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "path":Ljava/lang/String;
    goto :goto_0
.end method

.method public getSession()Lcom/samsung/android/app/memo/Session;
    .locals 1

    .prologue
    .line 1105
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mSession:Lcom/samsung/android/app/memo/Session;

    return-object v0
.end method

.method public hideCategoryList()V
    .locals 2

    .prologue
    .line 1150
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz v0, :cond_0

    .line 1151
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(I)V

    .line 1153
    :cond_0
    return-void
.end method

.method public initDrawerClosing()V
    .locals 1

    .prologue
    .line 1242
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/Main;->mIsDrawerClosing:Z

    .line 1243
    return-void
.end method

.method public isDrawerClosing()Z
    .locals 1

    .prologue
    .line 1232
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/Main;->mIsDrawerClosing:Z

    return v0
.end method

.method public isNeedDrawerOpen()Z
    .locals 1

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mIsDrawerOpen:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 891
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 892
    packed-switch p1, :pswitch_data_0

    .line 904
    :cond_0
    :goto_0
    packed-switch p2, :pswitch_data_1

    .line 911
    :cond_1
    :goto_1
    return-void

    .line 894
    :pswitch_0
    if-eqz p3, :cond_0

    const-string v2, "MESSAGE_ID"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 895
    const-string v2, "MESSAGE_ID"

    const/4 v3, -0x1

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 896
    .local v0, "message_Id":I
    const-string v2, "OBJECT"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    .line 897
    .local v1, "obj":Ljava/io/Serializable;
    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/memo/Main;->sendMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 906
    .end local v0    # "message_Id":I
    .end local v1    # "obj":Ljava/io/Serializable;
    :pswitch_1
    iget-boolean v2, p0, Lcom/samsung/android/app/memo/Main;->mIsFromWidget:Z

    if-eqz v2, :cond_1

    .line 907
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->finish()V

    goto :goto_1

    .line 892
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    .line 904
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_1
    .end packed-switch
.end method

.method public onAttachFragment(Landroid/app/Fragment;)V
    .locals 0
    .param p1, "fragment"    # Landroid/app/Fragment;

    .prologue
    .line 1270
    iput-object p1, p0, Lcom/samsung/android/app/memo/Main;->mCurrentFragment:Landroid/app/Fragment;

    .line 1271
    invoke-super {p0, p1}, Landroid/app/Activity;->onAttachFragment(Landroid/app/Fragment;)V

    .line 1272
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 1248
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1249
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->closeDrawer()V

    .line 1266
    :goto_0
    return-void

    .line 1253
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1254
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0e0027

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCurrentFragment:Landroid/app/Fragment;

    .line 1256
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCurrentFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCurrentFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    if-eqz v0, :cond_2

    .line 1258
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 1259
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->openDrawer()V

    .line 1260
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCurrentFragment:Landroid/app/Fragment;

    goto :goto_0

    .line 1264
    :cond_2
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCategroyChanged(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "mCancel"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1072
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCategoryChangeHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    .line 1073
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCategoryChangeHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Main;->mCategoryChangeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1091
    :cond_0
    :goto_0
    return-void

    .line 1076
    :cond_1
    iput-object v1, p0, Lcom/samsung/android/app/memo/Main;->mCategoryChangeHandler:Landroid/os/Handler;

    .line 1077
    iput-object v1, p0, Lcom/samsung/android/app/memo/Main;->mCategoryChangeRunnable:Ljava/lang/Runnable;

    .line 1078
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCategoryChangeHandler:Landroid/os/Handler;

    .line 1080
    new-instance v0, Lcom/samsung/android/app/memo/Main$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/Main$6;-><init>(Lcom/samsung/android/app/memo/Main;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCategoryChangeRunnable:Ljava/lang/Runnable;

    .line 1086
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCategoryChangeHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Main;->mCategoryChangeRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1087
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    if-eqz v0, :cond_0

    .line 1088
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->reloadedCursor(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 218
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 219
    const-string v2, "VerificationLog"

    const-string v3, "onCreate"

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    sget-object v2, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    const-string v3, "onCreate()"

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 223
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 224
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v2, v2, 0x400

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 225
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 226
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 227
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 229
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/app/memo/Main;->mSession:Lcom/samsung/android/app/memo/Session;

    .line 231
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isFonbletHD()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 232
    const v2, 0x7f0c001f

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/Main;->setTheme(I)V

    .line 235
    :cond_0
    const v2, 0x7f040018

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/Main;->setContentView(I)V

    .line 237
    const v2, 0x7f0e0026

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/Main;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/widget/DrawerLayout;

    iput-object v2, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 238
    sget-object v2, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mDrawerLayout ::: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/app/memo/Main;->mDuringCall:Landroid/os/Bundle;

    .line 244
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/samsung/android/app/memo/Main$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/memo/Main$1;-><init>(Lcom/samsung/android/app/memo/Main;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 249
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 253
    if-eqz p1, :cond_1

    .line 254
    invoke-static {p1}, Lcom/samsung/android/app/memo/Session;->createInstanceFrom(Landroid/os/Bundle;)Lcom/samsung/android/app/memo/Session;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/memo/Main;->mSession:Lcom/samsung/android/app/memo/Session;

    .line 256
    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->values:[Lcom/samsung/android/app/memo/Main$Mode;

    sget-object v3, Lcom/samsung/android/app/memo/Main;->KEY_MODE:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/app/memo/Main$Mode;->UNKNOWN:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/Main$Mode;->ordinal()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    .line 257
    sget-object v2, Lcom/samsung/android/app/memo/Main;->KEY_DURING_CALL:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/memo/Main;->mDuringCall:Landroid/os/Bundle;

    .line 258
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Main;->initFragments()V

    .line 280
    :goto_0
    return-void

    .line 263
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/Main;->onNewIntent(Landroid/content/Intent;)V

    .line 265
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->setOverFlowMenu()V

    .line 266
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/samsung/android/app/memo/Main$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/memo/Main$2;-><init>(Lcom/samsung/android/app/memo/Main;)V

    .line 279
    const-wide/16 v4, 0x0

    .line 266
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 637
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->flushCache()V

    .line 638
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 639
    sget-object v0, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    invoke-static {p0}, Lcom/samsung/android/app/migration/utils/Utils;->resetSyncDialogsState(Landroid/content/Context;)V

    .line 643
    return-void
.end method

.method public onFragmentSuicide(Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 1096
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 1097
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 1101
    :goto_0
    return-void

    .line 1099
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->onBackPressed()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    .line 1181
    const/16 v1, 0x70

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 1182
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0e0027

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    .line 1183
    .local v0, "mFragment":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    if-eqz v1, :cond_0

    .line 1184
    check-cast v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    .end local v0    # "mFragment":Landroid/app/Fragment;
    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1185
    const/4 v1, 0x1

    .line 1189
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    .line 1207
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    .line 1212
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1217
    sparse-switch p1, :sswitch_data_0

    .line 1225
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 1223
    :sswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 1217
    :sswitch_data_0
    .sparse-switch
        0x14 -> :sswitch_0
        0x1d -> :sswitch_0
        0x22 -> :sswitch_0
        0x2a -> :sswitch_0
        0xa0 -> :sswitch_0
    .end sparse-switch
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 1
    .param p1, "featureId"    # I
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 647
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 654
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 649
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Main;->toggleTagsDrawer()V

    goto :goto_0

    .line 647
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 321
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 322
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/samsung/android/app/memo/MemoApp;->setIsSaveOnDetach(Z)V

    .line 323
    sget-object v10, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "onNewIntent() "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->isTaskRoot()Z

    move-result v10

    if-nez v10, :cond_0

    .line 325
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 326
    .local v4, "i":Landroid/content/Intent;
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    .line 327
    .local v5, "intentAction":Ljava/lang/String;
    const-string v10, "android.intent.category.LAUNCHER"

    invoke-virtual {v4, v10}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    if-eqz v5, :cond_0

    .line 328
    const-string v10, "android.intent.action.MAIN"

    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 329
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->finish()V

    .line 464
    .end local v4    # "i":Landroid/content/Intent;
    .end local v5    # "intentAction":Ljava/lang/String;
    :goto_0
    return-void

    .line 334
    :cond_0
    const-string v10, "isFromDragListener"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    iput-boolean v10, p0, Lcom/samsung/android/app/memo/Main;->mIsFromDragListener:Z

    .line 335
    const-string v10, "from_widget"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    iput-boolean v10, p0, Lcom/samsung/android/app/memo/Main;->mIsFromWidget:Z

    .line 336
    const-string v10, "appWidgetId"

    const-wide/16 v12, -0x1

    invoke-virtual {p1, v10, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    iput-wide v10, p0, Lcom/samsung/android/app/memo/Main;->widgetId:J

    .line 338
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "ExternalPicker"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 339
    const/4 v10, 0x1

    sput-boolean v10, Lcom/samsung/android/app/memo/Main;->mIsUiPickerMode:Z

    .line 340
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/memo/Main;->setFinishOnTouchOutside(Z)V

    .line 346
    :goto_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/android/app/memo/Main;->mUri:Landroid/net/Uri;

    .line 347
    const/4 v6, 0x0

    .line 348
    .local v6, "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iput-object v10, p0, Lcom/samsung/android/app/memo/Main;->mFileUris:Ljava/util/ArrayList;

    .line 349
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 350
    .local v0, "action":Ljava/lang/String;
    const-string v10, "android.intent.extra.SUBJECT"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 351
    const-string v10, "android.intent.extra.SUBJECT"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 350
    :goto_2
    iput-object v10, p0, Lcom/samsung/android/app/memo/Main;->mStrTitleText:Ljava/lang/String;

    .line 352
    iget-object v10, p0, Lcom/samsung/android/app/memo/Main;->mStrTitleText:Ljava/lang/String;

    if-nez v10, :cond_1

    .line 353
    const-string v10, "android.intent.extra.TITLE"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 354
    const-string v10, "android.intent.extra.TITLE"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 353
    :goto_3
    iput-object v10, p0, Lcom/samsung/android/app/memo/Main;->mStrTitleText:Ljava/lang/String;

    .line 360
    :cond_1
    const-string v10, "android.intent.action.VIEW"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_12

    .line 362
    const/4 v3, 0x0

    .line 363
    .local v3, "fileVnt":Ljava/io/File;
    const-string v8, ""

    .line 364
    .local v8, "sFilepath":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 365
    .local v2, "fileUri":Landroid/net/Uri;
    if-eqz v2, :cond_2

    const-string v10, "file"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 366
    new-instance v3, Ljava/io/File;

    .end local v3    # "fileVnt":Ljava/io/File;
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 367
    .restart local v3    # "fileVnt":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 368
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 371
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_8

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v10

    const-string v11, "application/vnd.samsung.android.memo"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v10

    const-string v11, "application/octet-stream"

    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 372
    :cond_3
    iget-object v10, p0, Lcom/samsung/android/app/memo/Main;->mUri:Landroid/net/Uri;

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/memo/Main;->getRealPathFromURI(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/android/app/memo/Main;->mUri:Landroid/net/Uri;

    .line 373
    sget-object v6, Lcom/samsung/android/app/memo/Main$Mode;->EDIT:Lcom/samsung/android/app/memo/Main$Mode;

    .line 462
    .end local v2    # "fileUri":Landroid/net/Uri;
    .end local v3    # "fileVnt":Ljava/io/File;
    .end local v8    # "sFilepath":Ljava/lang/String;
    :cond_4
    :goto_4
    iput-object v6, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    .line 463
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Main;->initFragments()V

    goto/16 :goto_0

    .line 343
    .end local v0    # "action":Ljava/lang/String;
    .end local v6    # "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    :cond_5
    const/4 v10, 0x0

    sput-boolean v10, Lcom/samsung/android/app/memo/Main;->mIsUiPickerMode:Z

    goto/16 :goto_1

    .line 351
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v6    # "nextMode":Lcom/samsung/android/app/memo/Main$Mode;
    :cond_6
    const/4 v10, 0x0

    goto :goto_2

    .line 354
    :cond_7
    const/4 v10, 0x0

    goto :goto_3

    .line 374
    .restart local v2    # "fileUri":Landroid/net/Uri;
    .restart local v3    # "fileVnt":Ljava/io/File;
    .restart local v8    # "sFilepath":Ljava/lang/String;
    :cond_8
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_9

    const-string v10, "text/x-vnote"

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_a

    .line 375
    :cond_9
    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    const-string v11, ".vnt"

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 376
    :cond_a
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/samsung/android/app/memo/Main;->mUri:Landroid/net/Uri;

    .line 377
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/samsung/android/app/memo/Main;->mIsFromVntFile:Z

    .line 378
    new-instance v9, Lcom/samsung/android/app/memo/uiwidget/VNTReader;

    invoke-direct {v9, p0, p1}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    .line 379
    .local v9, "vr":Lcom/samsung/android/app/memo/uiwidget/VNTReader;
    iget-boolean v10, v9, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIsVntCorrupt:Z

    if-eqz v10, :cond_b

    .line 380
    sget-object v6, Lcom/samsung/android/app/memo/Main$Mode;->UNKNOWN:Lcom/samsung/android/app/memo/Main$Mode;

    .line 381
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->finish()V

    goto/16 :goto_0

    .line 384
    :cond_b
    iget-boolean v10, v9, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIsMultiVnt:Z

    if-nez v10, :cond_c

    .line 385
    invoke-virtual {v9}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->getVntText()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/android/app/memo/Main;->mStrVntContent:Ljava/lang/String;

    .line 386
    sget-object v6, Lcom/samsung/android/app/memo/Main$Mode;->EDIT:Lcom/samsung/android/app/memo/Main$Mode;

    .line 387
    goto :goto_4

    .line 388
    :cond_c
    sget-object v6, Lcom/samsung/android/app/memo/Main$Mode;->UNKNOWN:Lcom/samsung/android/app/memo/Main$Mode;

    .line 389
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->finish()V

    goto/16 :goto_0

    .line 393
    .end local v9    # "vr":Lcom/samsung/android/app/memo/uiwidget/VNTReader;
    :cond_d
    if-eqz v2, :cond_10

    const-string v10, "appto"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 395
    const/4 v7, 0x0

    .line 397
    .local v7, "params":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    :try_start_0
    new-instance v10, Ljava/net/URI;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    const-string v11, "UTF-8"

    invoke-static {v10, v11}, Lorg/apache/http/client/utils/URLEncodedUtils;->parse(Ljava/net/URI;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 402
    :goto_5
    if-eqz v7, :cond_f

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v10

    const-string v11, "memo"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 403
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/http/NameValuePair;

    invoke-interface {v10}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "UUID"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 404
    sget-object v11, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    const/4 v10, 0x0

    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/http/NameValuePair;

    invoke-interface {v10}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-static {v11, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/android/app/memo/Main;->mUri:Landroid/net/Uri;

    .line 405
    iget-object v10, p0, Lcom/samsung/android/app/memo/Main;->mUri:Landroid/net/Uri;

    invoke-static {p0, v10}, Lcom/samsung/android/app/memo/MemoDataHandler;->isFileExists(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 406
    sget-object v6, Lcom/samsung/android/app/memo/Main$Mode;->VIEW:Lcom/samsung/android/app/memo/Main$Mode;

    goto/16 :goto_4

    .line 398
    :catch_0
    move-exception v1

    .line 399
    .local v1, "e":Ljava/net/URISyntaxException;
    sget-object v10, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "URISyntaxException :"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 408
    .end local v1    # "e":Ljava/net/URISyntaxException;
    :cond_e
    sget-object v6, Lcom/samsung/android/app/memo/Main$Mode;->NORMAL:Lcom/samsung/android/app/memo/Main$Mode;

    .line 410
    goto/16 :goto_4

    .line 411
    :cond_f
    if-eqz v7, :cond_4

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v10

    const-string v11, "list"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 412
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/http/NameValuePair;

    invoke-interface {v10}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "c"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 413
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/http/NameValuePair;

    invoke-interface {v10}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/samsung/android/app/memo/Main;->savePreferences(Ljava/lang/String;)V

    .line 414
    sget-object v6, Lcom/samsung/android/app/memo/Main$Mode;->NORMAL:Lcom/samsung/android/app/memo/Main$Mode;

    .line 417
    goto/16 :goto_4

    .line 420
    .end local v7    # "params":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    :cond_10
    const-string v10, "isCallMode"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 421
    sget-object v6, Lcom/samsung/android/app/memo/Main$Mode;->CALLNOTE_VIEW:Lcom/samsung/android/app/memo/Main$Mode;

    .line 422
    goto/16 :goto_4

    .line 424
    :cond_11
    sget-object v6, Lcom/samsung/android/app/memo/Main$Mode;->VIEW:Lcom/samsung/android/app/memo/Main$Mode;

    .line 427
    goto/16 :goto_4

    .end local v2    # "fileUri":Landroid/net/Uri;
    .end local v3    # "fileVnt":Ljava/io/File;
    .end local v8    # "sFilepath":Ljava/lang/String;
    :cond_12
    const-string v10, "android.intent.action.EDIT"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_13

    .line 430
    sget-object v6, Lcom/samsung/android/app/memo/Main$Mode;->EDIT:Lcom/samsung/android/app/memo/Main$Mode;

    .line 431
    goto/16 :goto_4

    :cond_13
    const-string v10, "android.intent.action.SEND"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_15

    .line 432
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->checkLowMemory()Z

    move-result v10

    if-eqz v10, :cond_14

    .line 433
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    .line 434
    const v11, 0x7f0b00b9

    .line 433
    invoke-static {v10, v11}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 435
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->finish()V

    goto/16 :goto_4

    .line 438
    :cond_14
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/Main;->onIntentActionSend(Landroid/content/Intent;)Lcom/samsung/android/app/memo/Main$Mode;

    move-result-object v6

    .line 440
    goto/16 :goto_4

    :cond_15
    const-string v10, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_17

    .line 441
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->checkLowMemory()Z

    move-result v10

    if-eqz v10, :cond_16

    .line 442
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    .line 443
    const v11, 0x7f0b00b9

    .line 442
    invoke-static {v10, v11}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 444
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->finish()V

    goto/16 :goto_4

    .line 447
    :cond_16
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/Main;->onIntentActionSendMultiple(Landroid/content/Intent;)Lcom/samsung/android/app/memo/Main$Mode;

    move-result-object v6

    .line 449
    goto/16 :goto_4

    .line 450
    :cond_17
    const-string v10, "android.intent.action.PICK"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_18

    .line 451
    const-string v10, "android.intent.action.GET_CONTENT"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_19

    .line 455
    :cond_18
    sget-object v6, Lcom/samsung/android/app/memo/Main$Mode;->PICK_ONE:Lcom/samsung/android/app/memo/Main$Mode;

    .line 456
    goto/16 :goto_4

    .line 459
    :cond_19
    sget-object v6, Lcom/samsung/android/app/memo/Main$Mode;->NORMAL:Lcom/samsung/android/app/memo/Main$Mode;

    goto/16 :goto_4
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 630
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 631
    sget-object v0, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/Migration;->setIsInBackground(Z)V

    .line 633
    return-void
.end method

.method protected onPostResume()V
    .locals 2

    .prologue
    .line 623
    invoke-super {p0}, Landroid/app/Activity;->onPostResume()V

    .line 624
    sget-object v0, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    const-string v1, "onPostResume()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/migration/Migration;->startMigration(Lcom/samsung/android/app/memo/Main;)V

    .line 626
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 616
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 617
    const-string v0, "VerificationLog"

    const-string v1, "ON-RESUME"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    sget-object v0, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 210
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mSession:Lcom/samsung/android/app/memo/Session;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/memo/Session;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 212
    :cond_0
    sget-object v0, Lcom/samsung/android/app/memo/Main;->KEY_MODE:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Main;->mMode:Lcom/samsung/android/app/memo/Main$Mode;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Main$Mode;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 213
    sget-object v0, Lcom/samsung/android/app/memo/Main;->KEY_DURING_CALL:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/Main;->mDuringCall:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 214
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 1173
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    if-eqz v0, :cond_0

    .line 1174
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->onSearchRequested()V

    .line 1176
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public openContent(JLjava/lang/String;Z)V
    .locals 3
    .param p1, "memoId"    # J
    .param p3, "categoryUUID"    # Ljava/lang/String;
    .param p4, "isEditMode"    # Z

    .prologue
    .line 1065
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 1066
    const/4 v1, 0x0

    .line 1065
    invoke-direct {p0, v0, p3, p4, v1}, Lcom/samsung/android/app/memo/Main;->requestMemoContentFragment(Landroid/net/Uri;Ljava/lang/String;ZI)V

    .line 1067
    return-void
.end method

.method public openDrawer()V
    .locals 2

    .prologue
    .line 1046
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-nez v0, :cond_0

    .line 1051
    :goto_0
    return-void

    .line 1049
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->openDrawer(I)V

    goto :goto_0
.end method

.method public sendMessage(ILjava/lang/Object;)V
    .locals 2
    .param p1, "messageId"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1124
    packed-switch p1, :pswitch_data_0

    .line 1132
    .end local p2    # "obj":Ljava/lang/Object;
    :goto_0
    return-void

    .line 1126
    .restart local p2    # "obj":Ljava/lang/Object;
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "obj":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->showUndoBar(I)V

    goto :goto_0

    .line 1124
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public setActionBarStyle(Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;)V
    .locals 5
    .param p1, "style"    # Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    .prologue
    .line 1005
    sget-object v2, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setActionBarStyle()"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1006
    iget-object v2, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-nez v2, :cond_0

    .line 1017
    :goto_0
    return-void

    .line 1009
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 1011
    .local v0, "ab":Landroid/app/ActionBar;
    invoke-virtual {p1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;->isAbHomeAsUpRequired()Z

    move-result v1

    .line 1012
    .local v1, "homeEnabled":Z
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1013
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 1014
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 1015
    const v2, 0x7f080008

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setIcon(I)V

    .line 1016
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;->LIST_VIEW:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    if-ne v2, p1, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/Main;->setDrawerEnabled(Z)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public setActionBarTitle(I)V
    .locals 3
    .param p1, "resId"    # I

    .prologue
    .line 1030
    sget-object v0, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setActionBarStyle()"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1031
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1032
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 1033
    return-void
.end method

.method public setActionBarTitle(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 1021
    sget-object v0, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setActionBarTitle()"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1023
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/Main;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 1024
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1025
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Main;->hideApplicationIcon()V

    .line 1026
    :cond_0
    return-void
.end method

.method public setDrawerLock(Z)V
    .locals 2
    .param p1, "lock"    # Z

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-nez v0, :cond_0

    .line 1120
    :goto_0
    return-void

    .line 1119
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setNeedDrawerOpen(Z)V
    .locals 1
    .param p1, "isOpen"    # Z

    .prologue
    .line 1055
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/Main;->mIsDrawerOpen:Ljava/lang/Boolean;

    .line 1056
    return-void
.end method

.method public setOverFlowMenu()V
    .locals 5

    .prologue
    .line 308
    :try_start_0
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 309
    .local v0, "config":Landroid/view/ViewConfiguration;
    const-class v3, Landroid/view/ViewConfiguration;

    const-string v4, "sHasPermanentMenuKey"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 310
    .local v2, "menuKeyField":Ljava/lang/reflect/Field;
    if-eqz v2, :cond_0

    .line 311
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 312
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 317
    .end local v0    # "config":Landroid/view/ViewConfiguration;
    .end local v2    # "menuKeyField":Ljava/lang/reflect/Field;
    :cond_0
    :goto_0
    return-void

    .line 314
    :catch_0
    move-exception v1

    .line 315
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/samsung/android/app/memo/Main;->TAG:Ljava/lang/String;

    const-string v4, "setOverFlowMenu exception"

    invoke-static {v3, v4, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public showCategoryList()V
    .locals 2

    .prologue
    .line 1155
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-nez v0, :cond_0

    .line 1156
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCategoryListContainer:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 1157
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main;->mCategoryListContainer:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1159
    :cond_0
    return-void
.end method

.method public showGeneralErrorMessage()V
    .locals 2

    .prologue
    .line 1110
    const v0, 0x7f0b000a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1111
    return-void
.end method

.method public startDrawerClosing()V
    .locals 1

    .prologue
    .line 1237
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/Main;->mIsDrawerClosing:Z

    .line 1238
    return-void
.end method
