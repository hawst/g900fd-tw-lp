.class public Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;
.super Ljava/lang/Object;
.source "JSONParser.java"


# instance fields
.field private mColumns:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;->mColumns:[Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;
    .locals 7
    .param p1, "jsonObject"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 51
    const/4 v2, 0x0

    .line 52
    .local v2, "value":Ljava/lang/String;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 53
    .local v1, "contentValues":Landroid/content/ContentValues;
    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;->mColumns:[Ljava/lang/String;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_0

    .line 63
    return-object v1

    .line 53
    :cond_0
    aget-object v0, v4, v3

    .line 55
    .local v0, "column":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 56
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 57
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 59
    :catch_0
    move-exception v6

    goto :goto_1
.end method

.method public setColumnsList([Ljava/lang/String;)V
    .locals 0
    .param p1, "columnList"    # [Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;->mColumns:[Ljava/lang/String;

    .line 48
    return-void
.end method
