.class public abstract Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
.super Ljava/lang/Object;
.source "VoiceRecorderBase.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;,
        Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;
    }
.end annotation


# static fields
.field public static final COMMAND:Ljava/lang/String; = "command"

.field public static final ENTERPRISEMANAGER:Ljava/lang/String; = "android.sec.enterprise.EnterpriseDeviceManager"

.field public static final MUSIC_COMMAND_PAUSE:Ljava/lang/String; = "pause"

.field public static final MUSIC_SERVICE_COMMAND:Ljava/lang/String; = "com.android.music.musicservicecommand"

.field public static final PLAYER_NOT_INITIALIZED:I = -0x1

.field public static final PLAYER_NOT_STOPED:I = 0x3

.field public static final PLAYER_NOW_PLAYING:I = 0x1

.field public static final PLAYER_PAUSED:I = 0x2

.field public static final PLAYER_PREPARED:I = 0x0

.field public static final SECMEDIALIB:Ljava/lang/String; = "com.sec.android.secmediarecorder.SecMediaRecorder"

.field private static TAG:Ljava/lang/String;

.field public static isSecMediaLibraryEnabled:Z

.field public static mToastMicrophoneIsDisabled:Landroid/widget/Toast;


# instance fields
.field public mActivity:Landroid/content/Context;

.field public mCallListener:Landroid/telephony/PhoneStateListener;

.field public mCurVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

.field public mPlayer:Landroid/media/MediaPlayer;

.field public mPlayerState:I

.field public mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

.field public mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

.field public mTelMan:Landroid/telephony/TelephonyManager;

.field private pauseDuringCall:Z

.field public playerWasPlaying:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 44
    const-string v2, "VoiceRecorderBase"

    sput-object v2, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->TAG:Ljava/lang/String;

    .line 64
    sput-boolean v4, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z

    .line 66
    const/4 v2, 0x0

    sput-object v2, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mToastMicrophoneIsDisabled:Landroid/widget/Toast;

    .line 112
    :try_start_0
    const-string v2, "com.sec.android.secmediarecorder.SecMediaRecorder"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 113
    .local v1, "medialib":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v2, 0x1

    sput-boolean v2, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_4

    .line 129
    :goto_0
    sget-boolean v2, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z

    if-eqz v2, :cond_0

    .line 131
    :try_start_1
    const-string v2, "android.sec.enterprise.EnterpriseDeviceManager"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 132
    const/4 v2, 0x1

    sput-boolean v2, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_7

    .line 145
    :cond_0
    :goto_1
    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    sput-boolean v4, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z

    .line 116
    sget-object v2, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->TAG:Ljava/lang/String;

    const-string v3, "VoiceRecorderProxy"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 117
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/SecurityException;
    sput-boolean v4, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z

    .line 119
    sget-object v2, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->TAG:Ljava/lang/String;

    const-string v3, "VoiceRecorderProxy"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 120
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sput-boolean v4, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z

    .line 122
    sget-object v2, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->TAG:Ljava/lang/String;

    const-string v3, "VoiceRecorderProxy"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 123
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 124
    .local v0, "e":Ljava/lang/Exception;
    sput-boolean v4, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z

    .line 125
    sget-object v2, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->TAG:Ljava/lang/String;

    const-string v3, "VoiceRecorderProxy"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 126
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    sput-boolean v4, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z

    goto :goto_0

    .line 133
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_5
    move-exception v0

    .line 134
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    sput-boolean v4, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z

    .line 135
    sget-object v2, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->TAG:Ljava/lang/String;

    const-string v3, "VoiceRecorderProxy"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 136
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_6
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/SecurityException;
    sput-boolean v4, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z

    .line 138
    sget-object v2, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->TAG:Ljava/lang/String;

    const-string v3, "VoiceRecorderProxy"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 139
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_7
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sput-boolean v4, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z

    .line 141
    sget-object v2, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->TAG:Ljava/lang/String;

    const-string v3, "VoiceRecorderProxy"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-boolean v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseDuringCall:Z

    .line 86
    iput-boolean v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->playerWasPlaying:Z

    .line 432
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$1;-><init>(Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mCallListener:Landroid/telephony/PhoneStateListener;

    .line 149
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mActivity:Landroid/content/Context;

    .line 150
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    .line 151
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mActivity:Landroid/content/Context;

    .line 152
    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 151
    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mTelMan:Landroid/telephony/TelephonyManager;

    .line 153
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mTelMan:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mCallListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 154
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;Z)V
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseDuringCall:Z

    return-void
.end method


# virtual methods
.method public abandonAudioFocus()V
    .locals 3

    .prologue
    .line 367
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mActivity:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 372
    :goto_0
    return-void

    .line 369
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mActivity:Landroid/content/Context;

    .line 370
    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 369
    check-cast v0, Landroid/media/AudioManager;

    .line 371
    .local v0, "audioManager":Landroid/media/AudioManager;
    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    goto :goto_0
.end method

.method public freeResources()V
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mTelMan:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mTelMan:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mCallListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mTelMan:Landroid/telephony/TelephonyManager;

    .line 161
    :cond_0
    return-void
.end method

.method public abstract getCurRecordingTime()J
.end method

.method public getCurrentPlayPosition()J
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 174
    const-wide/16 v0, -0x1

    .line 176
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public getPlayerState()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    return v0
.end method

.method public getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;
    .locals 10
    .param p1, "contentURI"    # Landroid/net/Uri;

    .prologue
    .line 229
    const/4 v6, 0x0

    .line 230
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 232
    .local v8, "realPathFromUri":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mActivity:Landroid/content/Context;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mActivity:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    .line 233
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mActivity:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 234
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 235
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 237
    .local v7, "idx":I
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mActivity:Landroid/content/Context;

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 236
    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Utils;->getAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 247
    .end local v7    # "idx":I
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    .line 248
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 250
    :cond_1
    :goto_1
    return-object v8

    .line 239
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 241
    goto :goto_0

    :cond_3
    if-eqz p1, :cond_0

    .line 242
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    goto :goto_0

    .line 244
    :catch_0
    move-exception v9

    .line 245
    .local v9, "sqle":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    sget-object v0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SQLiteException: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 247
    if-eqz v6, :cond_1

    .line 248
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 246
    .end local v9    # "sqle":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v0

    .line 247
    if-eqz v6, :cond_4

    .line 248
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 249
    :cond_4
    throw v0
.end method

.method public getVoiceRecordingTime(Landroid/net/Uri;)J
    .locals 7
    .param p1, "vUri"    # Landroid/net/Uri;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v6, 0x0

    .line 199
    const/4 v1, 0x0

    .line 200
    .local v1, "recording_time":I
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v4, :cond_0

    .line 201
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->release()V

    .line 202
    :cond_0
    new-instance v4, Landroid/media/MediaPlayer;

    invoke-direct {v4}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    .line 203
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v4, :cond_1

    .line 225
    :goto_0
    return-wide v2

    .line 207
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 208
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->prepare()V

    .line 209
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v1

    .line 224
    iput-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    .line 225
    int-to-long v2, v1

    goto :goto_0

    .line 210
    :catch_0
    move-exception v0

    .line 211
    .local v0, "e":Ljava/io/IOException;
    iput-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    goto :goto_0

    .line 213
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 214
    .local v0, "e":Ljava/lang/IllegalStateException;
    iput-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    goto :goto_0

    .line 216
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 217
    .local v0, "e":Ljava/lang/NullPointerException;
    iput-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    goto :goto_0

    .line 219
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_3
    move-exception v0

    .line 220
    .local v0, "e":Ljava/lang/SecurityException;
    iput-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    goto :goto_0
.end method

.method public isNowPaused()Z
    .locals 2

    .prologue
    .line 192
    iget v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 193
    const/4 v0, 0x1

    .line 195
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNowPlaying()Z
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    .line 188
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract isNowRecording()Z
.end method

.method public abstract isNowStopped()Z
.end method

.method public abstract isRecordActive(Landroid/content/Context;)Z
.end method

.method public abstract isRecording()Z
.end method

.method public onAudioFocusChange(I)V
    .locals 4
    .param p1, "focusChange"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 387
    packed-switch p1, :pswitch_data_0

    .line 430
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 389
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 391
    iget v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->playerWasPlaying:Z

    if-nez v1, :cond_2

    .line 392
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseDuringCall:Z

    if-eqz v1, :cond_0

    .line 394
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 395
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->playerWasPlaying:Z

    .line 396
    const/4 v1, 0x1

    iput v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    .line 397
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    if-eqz v1, :cond_0

    .line 398
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;->onStartPlaying()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 399
    :catch_0
    move-exception v0

    .line 400
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onAudioFocusChange Exception :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 405
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :pswitch_2
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->processAudioFocusLoss()V

    goto :goto_0

    .line 409
    :pswitch_3
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 410
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 411
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;->onForceStop()V

    .line 414
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 415
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseSound(Z)V

    .line 416
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->playerWasPlaying:Z

    goto :goto_0

    .line 421
    :pswitch_4
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 422
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 423
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseSound(Z)V

    .line 424
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->playerWasPlaying:Z

    goto :goto_0

    .line 387
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public abstract pauseRecording()V
.end method

.method public pauseSound(Z)V
    .locals 2
    .param p1, "isSeeking"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 333
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 334
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 335
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    .line 336
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 337
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;->onPausePlaying()V

    .line 339
    :cond_0
    return-void
.end method

.method public pauseSoundPlay()V
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 293
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 294
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    .line 295
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;->onPausePlaying()V

    .line 298
    :cond_0
    return-void
.end method

.method public playSound(Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/media/MediaPlayer$OnCompletionListener;Landroid/media/MediaPlayer$OnErrorListener;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;)V
    .locals 4
    .param p1, "vInfo"    # Lcom/samsung/android/app/memo/voice/VoiceInfo;
    .param p2, "complete"    # Landroid/media/MediaPlayer$OnCompletionListener;
    .param p3, "error"    # Landroid/media/MediaPlayer$OnErrorListener;
    .param p4, "listner"    # Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    .prologue
    .line 302
    iput-object p4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    .line 303
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 304
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 306
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 307
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopSoundPlay()V

    .line 309
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->requestAudioFocus()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 330
    :cond_2
    :goto_0
    return-void

    .line 312
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.musicservicecommand"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 313
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "command"

    const-string v2, "pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 314
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mActivity:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 316
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mCurVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 317
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_4

    if-eqz p1, :cond_4

    .line 318
    iget-object v1, p1, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    invoke-virtual {p0, v1, p2, p3}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->preParePlayVoiceR(Landroid/net/Uri;Landroid/media/MediaPlayer$OnCompletionListener;Landroid/media/MediaPlayer$OnErrorListener;)J

    move-result-wide v2

    iput-wide v2, p1, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTotalTime:J

    .line 320
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_2

    .line 321
    iget v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 322
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 323
    const/4 v1, 0x1

    iput v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    .line 324
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    if-eqz v1, :cond_2

    .line 325
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;->onStartPlaying()V

    goto :goto_0
.end method

.method public playerSeekTo(I)Z
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 166
    const/4 v0, 0x1

    .line 168
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public preParePlayVoiceR(Landroid/net/Uri;Landroid/media/MediaPlayer$OnCompletionListener;Landroid/media/MediaPlayer$OnErrorListener;)J
    .locals 4
    .param p1, "vUri"    # Landroid/net/Uri;
    .param p2, "complete"    # Landroid/media/MediaPlayer$OnCompletionListener;
    .param p3, "error"    # Landroid/media/MediaPlayer$OnErrorListener;

    .prologue
    .line 254
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 255
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 256
    :cond_0
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    .line 259
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 260
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 261
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p3}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 262
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    .line 270
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    int-to-long v2, v1

    :goto_0
    return-wide v2

    .line 263
    :catch_0
    move-exception v0

    .line 264
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 265
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    .line 266
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    .line 267
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public processAudioFocusLoss()V
    .locals 1

    .prologue
    .line 375
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 377
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;->onForceStop()V

    .line 379
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 380
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 381
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseSound(Z)V

    .line 383
    :cond_1
    return-void
.end method

.method public requestAudioFocus()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 351
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mActivity:Landroid/content/Context;

    if-nez v4, :cond_1

    .line 362
    :cond_0
    :goto_0
    return v2

    .line 353
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mActivity:Landroid/content/Context;

    .line 354
    const-string v5, "audio"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 353
    check-cast v0, Landroid/media/AudioManager;

    .line 356
    .local v0, "audioManager":Landroid/media/AudioManager;
    const/4 v4, 0x3

    .line 355
    invoke-virtual {v0, p0, v4, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    .line 357
    .local v1, "result":I
    if-eq v1, v3, :cond_0

    move v2, v3

    .line 358
    goto :goto_0
.end method

.method public abstract resumeRecording()V
.end method

.method public abstract startRecording(Lcom/samsung/android/app/memo/voice/VoiceInfo;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;)Z
.end method

.method public startSound()V
    .locals 2

    .prologue
    .line 342
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 343
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 344
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    .line 345
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;->onStartPlaying()V

    .line 348
    :cond_0
    return-void
.end method

.method public abstract stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;
.end method

.method public stopSoundPlay()V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 276
    iget v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 278
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 279
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayer:Landroid/media/MediaPlayer;

    .line 280
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    if-eqz v0, :cond_1

    .line 281
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;->onStopPlaying()V

    .line 283
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->mPlayerState:I

    .line 285
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->abandonAudioFocus()V

    .line 289
    :cond_2
    return-void
.end method
