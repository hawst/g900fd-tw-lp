.class Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;
.super Ljava/lang/Object;
.source "BeamHelper.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/share/beam/BeamHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SbeamPushCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getFileList([Ljava/io/File;)Lorg/json/JSONArray;
    .locals 10
    .param p1, "files"    # [Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 249
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    .line 250
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 251
    .local v4, "rootPath":Ljava/lang/String;
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 253
    .local v3, "retList":Lorg/json/JSONArray;
    array-length v6, p1

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v6, :cond_0

    .line 263
    return-object v3

    .line 253
    :cond_0
    aget-object v1, p1, v5

    .line 254
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 255
    .local v0, "absolutePath":Ljava/lang/String;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 256
    .local v2, "obj":Lorg/json/JSONObject;
    const-string v7, "fileName"

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 257
    const-string v7, "fileLen"

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-virtual {v2, v7, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 258
    const-string v7, "filepath"

    invoke-virtual {v2, v7, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 259
    const-string v7, "subPath"

    const-string v8, ""

    invoke-virtual {v0, v4, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 260
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 253
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method private getJSONObject([Ljava/io/File;)[B
    .locals 5
    .param p1, "files"    # [Ljava/io/File;

    .prologue
    .line 235
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 237
    .local v1, "obj":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "mac"

    invoke-direct {p0}, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->getMacAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 238
    const-string v2, "mimeType"

    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mMimeType:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$4(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 239
    const-string v2, "list"

    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->getFileList([Ljava/io/File;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    return-object v2

    .line 240
    :catch_0
    move-exception v0

    .line 241
    .local v0, "e":Lorg/json/JSONException;
    const-string v2, "BeamHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BeamHelper.SbeamPushCallback.getJSONObject : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 242
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mMimeType:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$4(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 241
    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getMacAddress()Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 267
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$3(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Landroid/content/Context;

    move-result-object v4

    .line 268
    const-string v5, "wifi"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 267
    check-cast v3, Landroid/net/wifi/WifiManager;

    .line 269
    .local v3, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    .line 270
    .local v2, "wifiInfo":Landroid/net/wifi/WifiInfo;
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 271
    .local v0, "firstMac":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v4

    .line 272
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    .line 271
    invoke-virtual {v4, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 274
    .local v1, "lastMac":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-virtual {v0, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 275
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 277
    invoke-static {v0, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    or-int/lit8 v5, v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    .line 276
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 277
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 275
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 278
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 281
    :goto_0
    return-object v4

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    or-int/lit8 v5, v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 282
    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 7
    .param p1, "arg0"    # Landroid/nfc/NfcEvent;

    .prologue
    const/4 v6, 0x1

    .line 176
    const-string v3, "BeamHelper"

    const-string v4, "SbeamPushCallback.createNdefMessage"

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->createMemoFile()V

    .line 180
    const-wide/16 v4, 0x3e8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # invokes: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->getFiles()[Ljava/io/File;
    invoke-static {v3}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$0(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)[Ljava/io/File;

    move-result-object v1

    .line 187
    .local v1, "files":[Ljava/io/File;
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->createRecord([Ljava/io/File;)Landroid/nfc/NdefRecord;

    move-result-object v2

    .line 188
    .local v2, "record":Landroid/nfc/NdefRecord;
    if-nez v2, :cond_0

    .line 189
    const-string v3, "BeamHelper"

    .line 190
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SbeamPushCallback.createNdefMessage : fail to crate["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 191
    iget-object v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I
    invoke-static {v5}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$1(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 190
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 189
    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const/4 v3, 0x0

    .line 196
    :goto_1
    return-object v3

    .line 181
    .end local v1    # "files":[Ljava/io/File;
    .end local v2    # "record":Landroid/nfc/NdefRecord;
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v3, "BeamHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "InterruptedException :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 195
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .restart local v1    # "files":[Ljava/io/File;
    .restart local v2    # "record":Landroid/nfc/NdefRecord;
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$2(Lcom/samsung/android/app/memo/share/beam/BeamHelper;I)V

    .line 196
    new-instance v3, Landroid/nfc/NdefMessage;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/nfc/NdefRecord;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const-string v5, "com.sec.android.directshare"

    invoke-static {v5}, Landroid/nfc/NdefRecord;->createApplicationRecord(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-direct {v3, v4}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    goto :goto_1
.end method

.method createRecord([Ljava/io/File;)Landroid/nfc/NdefRecord;
    .locals 7
    .param p1, "files"    # [Ljava/io/File;

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 200
    const/4 v2, 0x0

    .line 201
    .local v2, "payload":[B
    const/4 v1, 0x0

    .line 203
    .local v1, "mediaMime":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.sec.android.directshare.DirectSharePopUp"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 204
    .local v0, "i":Landroid/content/Intent;
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I
    invoke-static {v4}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$1(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 226
    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mMimeType:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$4(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Ljava/lang/String;

    move-result-object v1

    .line 227
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->getJSONObject([Ljava/io/File;)[B

    move-result-object v2

    .line 230
    new-instance v3, Landroid/nfc/NdefRecord;

    const/4 v4, 0x2

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    new-array v6, v6, [B

    invoke-direct {v3, v4, v5, v6, v2}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    :goto_0
    return-object v3

    .line 206
    :pswitch_0
    const-string v4, "POPUP_MODE"

    const-string v5, "no_file_selected"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 207
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$3(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 208
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-static {v4, v6}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$2(Lcom/samsung/android/app/memo/share/beam/BeamHelper;I)V

    goto :goto_0

    .line 211
    :pswitch_1
    const-string v4, "POPUP_MODE"

    const-string v5, "from_cloud_file"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$3(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 213
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-static {v4, v6}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$2(Lcom/samsung/android/app/memo/share/beam/BeamHelper;I)V

    goto :goto_0

    .line 216
    :pswitch_2
    const-string v4, "POPUP_MODE"

    const-string v5, "from_drm_file"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 217
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$3(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 218
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-static {v4, v6}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$2(Lcom/samsung/android/app/memo/share/beam/BeamHelper;I)V

    goto :goto_0

    .line 221
    :pswitch_3
    const-string v4, "POPUP_MODE"

    const-string v5, "does_not_saved"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 222
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$3(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 223
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-static {v4, v6}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$2(Lcom/samsung/android/app/memo/share/beam/BeamHelper;I)V

    goto :goto_0

    .line 204
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
