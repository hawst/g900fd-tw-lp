.class public Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;
.super Landroid/app/Activity;
.source "OpenSourceLicenseActivity.java"


# instance fields
.field mWebView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 19
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 21
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 22
    .local v1, "window":Landroid/view/Window;
    if-eqz v1, :cond_0

    .line 23
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 24
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v2, v2, 0x400

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 25
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 26
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 28
    .end local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    const v2, 0x7f040023

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;->setContentView(I)V

    .line 30
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 31
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00b7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 32
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 33
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 35
    const v2, 0x7f0e004a

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    iput-object v2, p0, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;->mWebView:Landroid/webkit/WebView;

    .line 36
    iget-object v2, p0, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v3, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity$1;-><init>(Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 43
    iget-object v2, p0, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;->mWebView:Landroid/webkit/WebView;

    const-string v3, "file:///android_asset/legal/NOTICE"

    invoke-virtual {v2, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 44
    iget-object v2, p0, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 50
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/OpenSourceLicenseActivity;->finish()V

    .line 53
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
