.class Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$9;
.super Landroid/os/Handler;
.source "MemoContentFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    .line 2505
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    .line 2507
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 2508
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_1

    .line 2531
    :cond_0
    :goto_0
    return-void

    .line 2510
    :cond_1
    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 2509
    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 2511
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 2513
    :pswitch_0
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2514
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0

    .line 2519
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$7(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V

    .line 2520
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2521
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$16(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2522
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$16(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0

    .line 2524
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 2525
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->registerClipboard()V

    goto :goto_0

    .line 2511
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
