.class Lcom/samsung/android/app/migration/dialog/ImportDialog$5;
.super Ljava/lang/Object;
.source "ImportDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/migration/dialog/ImportDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/migration/dialog/ImportDialog;

.field private final synthetic val$ctx:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/migration/dialog/ImportDialog;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog$5;->this$0:Lcom/samsung/android/app/migration/dialog/ImportDialog;

    iput-object p2, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog$5;->val$ctx:Landroid/app/Activity;

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog$5;->val$ctx:Landroid/app/Activity;

    iget-object v1, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog$5;->this$0:Lcom/samsung/android/app/migration/dialog/ImportDialog;

    # getter for: Lcom/samsung/android/app/migration/dialog/ImportDialog;->mDoNotShowAgain:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/samsung/android/app/migration/dialog/ImportDialog;->access$0(Lcom/samsung/android/app/migration/dialog/ImportDialog;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->setImportDoNotShowFlag(Landroid/content/Context;Z)V

    .line 119
    iget-object v0, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog$5;->val$ctx:Landroid/app/Activity;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemoImportDialogState(Landroid/content/Context;I)V

    .line 120
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getInstance()Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    move-result-object v0

    # getter for: Lcom/samsung/android/app/migration/dialog/ImportDialog;->mKeys:Ljava/util/List;
    invoke-static {}, Lcom/samsung/android/app/migration/dialog/ImportDialog;->access$1()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->downloadTMemo(Ljava/util/List;)V

    .line 121
    iget-object v0, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog$5;->this$0:Lcom/samsung/android/app/migration/dialog/ImportDialog;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/dialog/ImportDialog;->dismiss()V

    .line 122
    return-void
.end method
