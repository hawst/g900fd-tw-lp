.class Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;
.super Ljava/lang/Object;
.source "MemoContentFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final MAX_CLICK_DURATION:I = 0x190


# instance fields
.field final MAX_CLICK_DISTANCE:I

.field dx:F

.field dy:F

.field startClickTime:J

.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

.field x1:F

.field x2:F

.field y1:F

.field y2:F


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    .line 2239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2240
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->startClickTime:J

    .line 2244
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->MAX_CLICK_DISTANCE:I

    .line 2246
    iput v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->x1:F

    .line 2248
    iput v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->y1:F

    .line 2250
    iput v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->x2:F

    .line 2252
    iput v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->y2:F

    .line 2254
    iput v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->dx:F

    .line 2256
    iput v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->dy:F

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    const/high16 v6, 0x41200000    # 10.0f

    .line 2260
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 2295
    :cond_0
    :goto_0
    return v7

    .line 2262
    :pswitch_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->startClickTime:J

    .line 2263
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->x1:F

    .line 2264
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->y1:F

    goto :goto_0

    .line 2268
    :pswitch_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->startClickTime:J

    sub-long v0, v2, v4

    .line 2269
    .local v0, "clickDuration":J
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->x2:F

    .line 2270
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->y2:F

    .line 2271
    iget v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->x2:F

    iget v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->x1:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->dx:F

    .line 2272
    iget v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->y2:F

    iget v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->y1:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->dy:F

    .line 2273
    const-wide/16 v2, 0x190

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    iget v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->dx:F

    cmpg-float v2, v2, v6

    if-gez v2, :cond_1

    .line 2274
    iget v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->dy:F

    cmpg-float v2, v2, v6

    if-gez v2, :cond_1

    .line 2275
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->removeLinks()V

    .line 2277
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2278
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 2283
    .end local v0    # "clickDuration":J
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2284
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$4(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/ScrollView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v2

    if-nez v2, :cond_2

    .line 2285
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2287
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$4(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/ScrollView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$5(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2288
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2260
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
