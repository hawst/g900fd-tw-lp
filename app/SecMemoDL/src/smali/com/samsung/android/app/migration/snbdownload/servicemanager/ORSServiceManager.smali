.class public Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;
.super Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;
.source "ORSServiceManager.java"


# instance fields
.field private mFileBuffer:[B

.field mORSMetaResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "authManager"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;
    .param p3, "cid"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;-><init>(Landroid/content/Context;Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;Ljava/lang/String;)V

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->mFileBuffer:[B

    .line 55
    new-instance v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->mORSMetaResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;

    .line 59
    return-void
.end method

.method private setBuffer([B)V
    .locals 0
    .param p1, "buffer"    # [B

    .prologue
    .line 107
    iput-object p1, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->mFileBuffer:[B

    .line 108
    return-void
.end method


# virtual methods
.method public downloadFile(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;
    .locals 9
    .param p1, "serverFilePath"    # Ljava/lang/String;
    .param p2, "localFilePath"    # Ljava/lang/String;
    .param p3, "writeToByteArray"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    .line 63
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    invoke-virtual {v5}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "baseUrl":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 65
    new-instance v5, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v6, 0x3

    invoke-direct {v5, v6}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v5

    .line 67
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 68
    .local v4, "url":Ljava/lang/StringBuilder;
    const-string v5, "/ors"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    const-string v5, "?action=download"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->mCid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->getPutApiParams(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    const/4 v5, 0x4

    .line 74
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->mORSMetaResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;

    .line 73
    invoke-virtual {p0, v5, v6, v7}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    move-result-object v3

    .line 75
    .local v3, "networkResponse":Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    invoke-virtual {v3}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;->isFileStream()Z
    :try_end_0
    .catch Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v5

    if-eqz v5, :cond_1

    .line 77
    :try_start_1
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->mORSMetaResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;

    invoke-virtual {v5}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;->clear()V

    .line 78
    if-eqz p3, :cond_2

    .line 79
    invoke-virtual {v3}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;->getFileInStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/app/memo/util/FileHelper;->getByteArr(Ljava/io/InputStream;)[B

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->setBuffer([B)V

    .line 83
    :goto_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 84
    .local v2, "json":Lorg/json/JSONObject;
    const-string v5, "rcode"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 85
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->mORSMetaResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;->fromJSON(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException; {:try_start_1 .. :try_end_1} :catch_1

    .line 99
    .end local v2    # "json":Lorg/json/JSONObject;
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->mORSMetaResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;

    return-object v5

    .line 81
    :cond_2
    :try_start_2
    invoke-virtual {v3}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;->getFileInStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-static {v5, p2}, Lcom/samsung/android/app/memo/util/FileHelper;->writeToFile(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 86
    :catch_0
    move-exception v1

    .line 87
    .local v1, "e":Ljava/io/IOException;
    :try_start_3
    new-instance v5, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v6, 0x4

    invoke-direct {v5, v6}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v5
    :try_end_3
    .catch Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException; {:try_start_3 .. :try_end_3} :catch_1

    .line 92
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "networkResponse":Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    :catch_1
    move-exception v1

    .line 93
    .local v1, "e":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
    invoke-virtual {v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;->getmExceptionCode()I

    move-result v5

    if-ne v5, v8, :cond_3

    .line 94
    new-instance v5, Ljava/io/IOException;

    invoke-direct {v5}, Ljava/io/IOException;-><init>()V

    throw v5

    .line 88
    .end local v1    # "e":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
    .restart local v3    # "networkResponse":Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    :catch_2
    move-exception v1

    .line 89
    .local v1, "e":Lorg/json/JSONException;
    :try_start_4
    new-instance v5, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v6, 0x2

    invoke-direct {v5, v6}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v5
    :try_end_4
    .catch Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException; {:try_start_4 .. :try_end_4} :catch_1

    .line 96
    .end local v3    # "networkResponse":Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    .local v1, "e":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
    :cond_3
    new-instance v5, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    invoke-direct {v5, v8}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v5
.end method

.method public getBuffer()[B
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->mFileBuffer:[B

    return-object v0
.end method
