.class public Lcom/samsung/android/app/memo/FromSConnectReceiver;
.super Landroid/content/BroadcastReceiver;
.source "FromSConnectReceiver.java"


# static fields
.field private static final CID_ID:I

.field private static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mId:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 106
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 107
    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 108
    const-string v2, "UUID"

    aput-object v2, v0, v1

    .line 106
    sput-object v0, Lcom/samsung/android/app/memo/FromSConnectReceiver;->PROJECTION:[Ljava/lang/String;

    .line 110
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/FromSConnectReceiver;->mContext:Landroid/content/Context;

    .line 45
    const-string v0, "FromSConnectReceiver"

    iput-object v0, p0, Lcom/samsung/android/app/memo/FromSConnectReceiver;->TAG:Ljava/lang/String;

    .line 42
    return-void
.end method

.method private fromCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 114
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/FromSConnectReceiver;->mId:J

    .line 116
    return-void
.end method

.method private getMemoIdsFromUri(Landroid/net/Uri;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "uriList"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "\\s*,\\s*"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 64
    .local v3, "strFileUriData":[Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .local v1, "mMemoIdsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    array-length v5, v3

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_0

    .line 73
    return-object v1

    .line 65
    :cond_0
    aget-object v2, v3, v4

    .line 67
    .local v2, "strFileUri":Ljava/lang/String;
    :try_start_0
    const-string v6, "/"

    invoke-virtual {v2, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    const-string v7, "."

    invoke-virtual {v2, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    const-wide/16 v8, -0x1

    invoke-direct {p0, v6, v8, v9}, Lcom/samsung/android/app/memo/FromSConnectReceiver;->loadMemo(Ljava/lang/String;J)V

    .line 68
    iget-wide v6, p0, Lcom/samsung/android/app/memo/FromSConnectReceiver;->mId:J

    long-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    .local v0, "e":Lcom/samsung/android/app/memo/Session$SessionException;
    iget-object v6, p0, Lcom/samsung/android/app/memo/FromSConnectReceiver;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "SessionException : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private loadMemo(Ljava/lang/String;J)V
    .locals 8
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/Session$SessionException;
        }
    .end annotation

    .prologue
    .line 78
    if-nez p1, :cond_1

    .line 79
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-static {v0, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 83
    .local v1, "uri":Landroid/net/Uri;
    :goto_0
    const/4 v6, 0x0

    .line 85
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/FromSConnectReceiver;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/android/app/memo/FromSConnectReceiver;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 86
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 87
    new-instance v0, Landroid/database/SQLException;

    const-string v2, "empty"

    invoke-direct {v0, v2}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :catch_0
    move-exception v7

    .line 90
    .local v7, "sqle":Landroid/database/SQLException;
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/FromSConnectReceiver;->TAG:Ljava/lang/String;

    const-string v2, "insertNew()"

    invoke-static {v0, v2, v7}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 91
    new-instance v0, Lcom/samsung/android/app/memo/Session$SessionException;

    const-string v2, "fail to load"

    invoke-direct {v0, v2, v7}, Lcom/samsung/android/app/memo/Session$SessionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    .end local v7    # "sqle":Landroid/database/SQLException;
    :catchall_0
    move-exception v0

    .line 93
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 94
    :cond_0
    throw v0

    .line 81
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_1
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    invoke-static {v0, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .restart local v1    # "uri":Landroid/net/Uri;
    goto :goto_0

    .line 88
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_2
    :try_start_2
    invoke-direct {p0, v6}, Lcom/samsung/android/app/memo/FromSConnectReceiver;->fromCursor(Landroid/database/Cursor;)V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 93
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 95
    :cond_3
    return-void
.end method


# virtual methods
.method public getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/android/app/memo/FromSConnectReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x1

    .line 49
    iput-object p1, p0, Lcom/samsung/android/app/memo/FromSConnectReceiver;->mContext:Landroid/content/Context;

    .line 50
    iget-object v4, p0, Lcom/samsung/android/app/memo/FromSConnectReceiver;->TAG:Ljava/lang/String;

    const-string v5, "in onReceive"

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-string v4, "PRINTER"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 52
    .local v2, "printerInfo":Landroid/os/Bundle;
    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 53
    .local v3, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    if-eqz v3, :cond_0

    .line 54
    new-instance v0, Lcom/samsung/android/app/memo/print/AsyncPrintTask;

    iget-object v4, p0, Lcom/samsung/android/app/memo/FromSConnectReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x0

    invoke-direct {v0, v4, v7, v5, v6}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;-><init>(Landroid/content/Context;ZILcom/samsung/android/app/memo/Session;)V

    .line 55
    .local v0, "asyncPrintTask":Lcom/samsung/android/app/memo/print/AsyncPrintTask;
    invoke-virtual {v0, v7}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->setFromSConnectPrint(Z)V

    .line 56
    invoke-virtual {v0, v2}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->setPrinterInfo(Landroid/os/Bundle;)V

    .line 57
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    invoke-direct {p0, v4}, Lcom/samsung/android/app/memo/FromSConnectReceiver;->getMemoIdsFromUri(Landroid/net/Uri;)Ljava/util/ArrayList;

    move-result-object v1

    .line 58
    .local v1, "memoUUIDList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/Integer;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Integer;

    invoke-virtual {v0, v4}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 60
    .end local v0    # "asyncPrintTask":Lcom/samsung/android/app/memo/print/AsyncPrintTask;
    .end local v1    # "memoUUIDList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_0
    return-void
.end method
