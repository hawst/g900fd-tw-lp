.class Lcom/samsung/android/app/memo/uiwidget/CustLinkify$4;
.super Ljava/lang/Object;
.source "CustLinkify.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->pruneOverlaps(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public final compare(Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;)I
    .locals 4
    .param p1, "a"    # Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;
    .param p2, "b"    # Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 464
    iget v2, p1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->start:I

    iget v3, p2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->start:I

    if-ge v2, v3, :cond_1

    .line 480
    :cond_0
    :goto_0
    return v0

    .line 468
    :cond_1
    iget v2, p1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->start:I

    iget v3, p2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->start:I

    if-le v2, v3, :cond_2

    move v0, v1

    .line 469
    goto :goto_0

    .line 472
    :cond_2
    iget v2, p1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->end:I

    iget v3, p2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->end:I

    if-ge v2, v3, :cond_3

    move v0, v1

    .line 473
    goto :goto_0

    .line 476
    :cond_3
    iget v1, p1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->end:I

    iget v2, p2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;->end:I

    if-gt v1, v2, :cond_0

    .line 480
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;

    check-cast p2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify$4;->compare(Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;Lcom/samsung/android/app/memo/uiwidget/MemoLinkSpec;)I

    move-result v0

    return v0
.end method
