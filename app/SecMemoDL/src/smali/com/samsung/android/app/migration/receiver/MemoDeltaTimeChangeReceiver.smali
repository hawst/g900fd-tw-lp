.class public Lcom/samsung/android/app/migration/receiver/MemoDeltaTimeChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MemoDeltaTimeChangeReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MemoDeltaTimeChangeReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 16
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 17
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.sec.android.samsungcloudsync.DELTA_TIME_CHANGED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 18
    const-string v2, "TimeDifference"

    const-wide/16 v4, 0x0

    invoke-virtual {p2, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 19
    .local v0, "timeDifference":J
    const-string v2, "MemoDeltaTimeChangeReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Delta Time has changed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Utils;->setDeltaTime(J)V

    .line 23
    .end local v0    # "timeDifference":J
    :cond_0
    return-void
.end method
