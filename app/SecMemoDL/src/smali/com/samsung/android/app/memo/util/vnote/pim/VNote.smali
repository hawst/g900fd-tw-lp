.class public Lcom/samsung/android/app/memo/util/vnote/pim/VNote;
.super Ljava/lang/Object;
.source "VNote.java"


# static fields
.field public static final BEGIN:Ljava/lang/String; = "BEGIN:VNOTE"

.field public static final BODY:Ljava/lang/String; = "BODY;CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:"

.field public static final DCREATED:Ljava/lang/String; = "DCREATED:"

.field public static final END:Ljava/lang/String; = "END:VNOTE"

.field public static final LAST_MODIFIED:Ljava/lang/String; = "LAST-MODIFIED:"

.field public static final PARSE_ERROR:I = -0x1

.field public static final TAG:Ljava/lang/String;

.field private static final TIME_FORMAT_STRING:Ljava/lang/String; = "yyyyMMdd\'T\'HHmmss"

.field public static final VERSION:Ljava/lang/String; = "VERSION:1.1"

.field public static mNewLine:Ljava/lang/String; = null

.field public static vntTag:I = 0x0

.field public static final vntTagRequired:I = 0x5


# instance fields
.field public KEY_CONTENT:Ljava/lang/String;

.field public KEY_CREATE:Ljava/lang/String;

.field public KEY_MODIFY:Ljava/lang/String;

.field public KEY_TITLE:Ljava/lang/String;

.field private mCreateTime:Ljava/lang/String;

.field private mModifyTime:Ljava/lang/String;

.field mStrBuilder:Ljava/lang/StringBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->TAG:Ljava/lang/String;

    .line 62
    const-string v0, "\r\n"

    sput-object v0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->mNewLine:Ljava/lang/String;

    .line 161
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vntTag:I

    .line 163
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const-string v0, "title"

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->KEY_TITLE:Ljava/lang/String;

    .line 56
    const-string v0, "content"

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->KEY_CONTENT:Ljava/lang/String;

    .line 58
    const-string v0, "modify_t"

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->KEY_MODIFY:Ljava/lang/String;

    .line 60
    const-string v0, "create_t"

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->KEY_CREATE:Ljava/lang/String;

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->mStrBuilder:Ljava/lang/StringBuilder;

    .line 37
    return-void
.end method

.method public static vNoteBodyParsing(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "body"    # Ljava/lang/String;

    .prologue
    .line 199
    move-object v1, p0

    .line 200
    .local v1, "tmpBody":Ljava/lang/String;
    const/16 v2, 0x3b

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 201
    .local v0, "sep":I
    if-ltz v0, :cond_0

    .line 202
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 204
    :cond_0
    return-object v1
.end method


# virtual methods
.method public checkPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 166
    const-string v1, "BEGIN:VNOTE"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 167
    sget v1, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vntTag:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vntTag:I

    move-object p1, v0

    .line 195
    .end local p1    # "prefix":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 169
    .restart local p1    # "prefix":Ljava/lang/String;
    :cond_1
    const-string v1, "VERSION:1.1"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 170
    sget v1, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vntTag:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vntTag:I

    move-object p1, v0

    .line 171
    goto :goto_0

    .line 172
    :cond_2
    const-string v1, "BODY"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 173
    invoke-static {p1}, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vNoteBodyParsing(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 174
    :cond_3
    const-string v1, "DCREATED:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 175
    sget v0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vntTag:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vntTag:I

    .line 176
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vNoteTimeParsing(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 177
    :cond_4
    const-string v1, "X-IRMC-LUID"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    move-object p1, v0

    .line 178
    goto :goto_0

    .line 179
    :cond_5
    const-string v1, "SUMMARY"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    move-object p1, v0

    .line 180
    goto :goto_0

    .line 181
    :cond_6
    const-string v1, "CATEGORIES"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    move-object p1, v0

    .line 182
    goto :goto_0

    .line 183
    :cond_7
    const-string v1, "CLASS"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    move-object p1, v0

    .line 184
    goto :goto_0

    .line 185
    :cond_8
    const-string v1, "LAST-MODIFIED:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 186
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vNoteTimeParsing(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 187
    :cond_9
    const-string v1, "END:VNOTE"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 188
    sget v0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vntTag:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vntTag:I

    .line 189
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vNoteTime()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 190
    :cond_a
    const-string v1, "X-SH-CATEGORIES:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    move-object p1, v0

    .line 191
    goto/16 :goto_0

    .line 192
    :cond_b
    const-string v1, "X-DCM-DATALINKID"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object p1, v0

    .line 193
    goto/16 :goto_0
.end method

.method public getFileLineCount(Landroid/net/Uri;)I
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    const/4 v6, 0x0

    .line 103
    .local v6, "itemCount":I
    const/4 v0, 0x0

    .line 104
    .local v0, "beginCount":I
    const/4 v3, 0x0

    .line 105
    .local v3, "endCount":I
    new-instance v8, Ljava/net/URL;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v5

    .line 106
    .local v5, "is":Ljava/io/InputStream;
    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 107
    .local v4, "ir":Ljava/io/InputStreamReader;
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 110
    .local v1, "br":Ljava/io/BufferedReader;
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .local v7, "tmp":Ljava/lang/String;
    if-nez v7, :cond_1

    .line 120
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V

    .line 121
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 122
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 124
    .end local v7    # "tmp":Ljava/lang/String;
    :goto_1
    if-ne v0, v3, :cond_3

    .line 125
    move v6, v0

    move v8, v6

    .line 128
    :goto_2
    return v8

    .line 111
    .restart local v7    # "tmp":Ljava/lang/String;
    :cond_1
    :try_start_1
    const-string v8, "BEGIN:VNOTE"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 112
    add-int/lit8 v0, v0, 0x1

    .line 113
    goto :goto_0

    :cond_2
    const-string v8, "END:VNOTE"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    if-eqz v8, :cond_0

    .line 114
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 117
    .end local v7    # "tmp":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 118
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    sget-object v8, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->TAG:Ljava/lang/String;

    const-string v9, "readFile()"

    invoke-static {v8, v9, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 120
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V

    .line 121
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 122
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    goto :goto_1

    .line 119
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 120
    invoke-virtual {v4}, Ljava/io/InputStreamReader;->close()V

    .line 121
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 122
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 123
    throw v8

    .line 128
    :cond_3
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public readFile(Landroid/net/Uri;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    new-instance v6, Ljava/net/URL;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v4

    .line 74
    .local v4, "is":Ljava/io/InputStream;
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 75
    .local v3, "ir":Ljava/io/InputStreamReader;
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 76
    .local v1, "br":Ljava/io/BufferedReader;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 79
    .local v0, "arrString":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .local v5, "tmp":Ljava/lang/String;
    if-nez v5, :cond_1

    .line 87
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    .line 88
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 89
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 91
    .end local v5    # "tmp":Ljava/lang/String;
    :goto_1
    return-object v0

    .line 80
    .restart local v5    # "tmp":Ljava/lang/String;
    :cond_1
    :try_start_1
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->checkPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 81
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->checkPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 84
    .end local v5    # "tmp":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 85
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    sget-object v6, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->TAG:Ljava/lang/String;

    const-string v7, "readFile()"

    invoke-static {v6, v7, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 87
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    .line 88
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 89
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    goto :goto_1

    .line 86
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 87
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    .line 88
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 89
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 90
    throw v6
.end method

.method public readFile(Landroid/net/Uri;I)Ljava/util/ArrayList;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    new-instance v6, Ljava/net/URL;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v4

    .line 141
    .local v4, "is":Ljava/io/InputStream;
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 142
    .local v3, "ir":Ljava/io/InputStreamReader;
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 143
    .local v1, "br":Ljava/io/BufferedReader;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 146
    .local v0, "arrString":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .local v5, "tmp":Ljava/lang/String;
    if-nez v5, :cond_1

    .line 154
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    .line 155
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 156
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 158
    .end local v5    # "tmp":Ljava/lang/String;
    :goto_1
    return-object v0

    .line 147
    .restart local v5    # "tmp":Ljava/lang/String;
    :cond_1
    :try_start_1
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->checkPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 148
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->checkPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 151
    .end local v5    # "tmp":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 152
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    sget-object v6, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->TAG:Ljava/lang/String;

    const-string v7, "readFile()"

    invoke-static {v6, v7, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 154
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    .line 155
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 156
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    goto :goto_1

    .line 153
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 154
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    .line 155
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 156
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 157
    throw v6
.end method

.method public vNoteTime()Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v3, 0x3a

    .line 218
    const/4 v1, 0x0

    .line 219
    .local v1, "time":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->mModifyTime:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 220
    iget-object v1, p0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->mCreateTime:Ljava/lang/String;

    .line 221
    if-nez v1, :cond_1

    .line 222
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMdd\'T\'HHmmss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 237
    :cond_0
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DCREATED:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 224
    :cond_1
    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 225
    .local v0, "sep":I
    if-ltz v0, :cond_0

    .line 226
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 229
    goto :goto_0

    .line 230
    .end local v0    # "sep":I
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->mModifyTime:Ljava/lang/String;

    .line 231
    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 232
    .restart local v0    # "sep":I
    if-ltz v0, :cond_0

    .line 233
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public vNoteTimeParsing(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 209
    const-string v0, "LAST-MODIFIED:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->mModifyTime:Ljava/lang/String;

    .line 214
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 212
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->mCreateTime:Ljava/lang/String;

    goto :goto_0
.end method
