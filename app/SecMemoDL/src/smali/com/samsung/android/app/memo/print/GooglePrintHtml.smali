.class public Lcom/samsung/android/app/memo/print/GooglePrintHtml;
.super Ljava/lang/Object;
.source "GooglePrintHtml.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mPrintContext:Landroid/content/Context;

.field private mTitle:Ljava/lang/String;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mPrintContext:Landroid/content/Context;

    .line 44
    iput-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mTitle:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mWebView:Landroid/webkit/WebView;

    .line 39
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/print/GooglePrintHtml;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->createWebPrintJob()V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/print/GooglePrintHtml;)Landroid/webkit/WebView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/print/GooglePrintHtml;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/print/GooglePrintHtml;Landroid/webkit/WebView;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mWebView:Landroid/webkit/WebView;

    return-void
.end method

.method private createWebPrintJob()V
    .locals 5

    .prologue
    .line 80
    iget-object v3, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mPrintContext:Landroid/content/Context;

    .line 81
    const-string v4, "print"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 80
    check-cast v2, Landroid/print/PrintManager;

    .line 83
    .local v2, "printManager":Landroid/print/PrintManager;
    new-instance v0, Lcom/samsung/android/app/memo/print/GooglePrintHtml$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/print/GooglePrintHtml$2;-><init>(Lcom/samsung/android/app/memo/print/GooglePrintHtml;)V

    .line 128
    .local v0, "adapter":Landroid/print/PrintDocumentAdapter;
    iget-object v1, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mTitle:Ljava/lang/String;

    .line 129
    .local v1, "jobName":Ljava/lang/String;
    new-instance v3, Landroid/print/PrintAttributes$Builder;

    invoke-direct {v3}, Landroid/print/PrintAttributes$Builder;-><init>()V

    invoke-virtual {v3}, Landroid/print/PrintAttributes$Builder;->build()Landroid/print/PrintAttributes;

    move-result-object v3

    invoke-virtual {v2, v1, v0, v3}, Landroid/print/PrintManager;->print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;

    .line 130
    return-void
.end method

.method private doWebViewPrint(Ljava/lang/String;)V
    .locals 8
    .param p1, "htmlContent"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 56
    new-instance v0, Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mPrintContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mWebView:Landroid/webkit/WebView;

    .line 57
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Lcom/samsung/android/app/memo/print/GooglePrintHtml$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/memo/print/GooglePrintHtml$1;-><init>(Lcom/samsung/android/app/memo/print/GooglePrintHtml;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 68
    const-string v0, "<p></p>"

    const-string v2, "<br>"

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 69
    .local v7, "htmlDocument":Ljava/lang/String;
    const-string v0, "</p><p><img"

    const-string v2, "<br><br><img"

    invoke-virtual {v7, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 70
    const-string v0, "<p>"

    const-string v2, ""

    invoke-virtual {v7, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 71
    const-string v0, "</p>"

    const-string v2, ""

    invoke-virtual {v7, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 73
    const-string v6, "<head><style>img {max-width:90%  ;max-height: 90%;;height: auto;width: auto;}</style></head>"

    .line 75
    .local v6, "header":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mWebView:Landroid/webkit/WebView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<head><style>img {max-width:90%  ;max-height: 90%;;height: auto;width: auto;}</style></head>"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/HTML"

    const-string v4, "UTF-8"

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    return-void
.end method


# virtual methods
.method public print(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "htmlContent"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mPrintContext:Landroid/content/Context;

    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mPrintContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0b0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mTitle:Ljava/lang/String;

    .line 51
    invoke-direct {p0, p2}, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->doWebViewPrint(Ljava/lang/String;)V

    .line 52
    return-void
.end method
