.class public Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "ProxyDrawable.java"


# instance fields
.field private mMutated:Z

.field private mProxy:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "target"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    .line 32
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 49
    :cond_0
    return-void
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public getProxy()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mMutated:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mMutated:Z

    .line 100
    :cond_0
    return-object p0
.end method

.method public setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 92
    :cond_0
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1, "colorFilter"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 85
    :cond_0
    return-void
.end method

.method public setDither(Z)V
    .locals 1
    .param p1, "dither"    # Z

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 78
    :cond_0
    return-void
.end method

.method public setFilterBitmap(Z)V
    .locals 1
    .param p1, "filter"    # Z

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 71
    :cond_0
    return-void
.end method

.method public setProxy(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "proxy"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 39
    if-eq p1, p0, :cond_0

    .line 40
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;->mProxy:Landroid/graphics/drawable/Drawable;

    .line 42
    :cond_0
    return-void
.end method
