.class Lcom/samsung/android/app/memo/uifragment/MemoListFragment$10;
.super Ljava/lang/Object;
.source "MemoListFragment.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    .line 1330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Z

    .prologue
    .line 1335
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$12(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1348
    :cond_0
    :goto_0
    return-void

    .line 1337
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$12(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/app/Activity;

    move-result-object v1

    .line 1338
    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1337
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1339
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 1341
    if-nez p2, :cond_2

    .line 1343
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    .line 1342
    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0

    .line 1346
    :cond_2
    const/4 v1, 0x1

    .line 1345
    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0
.end method
