.class public interface abstract Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/Constants$Files;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Files"
.end annotation


# static fields
.field public static final CONTENT:Ljava/lang/String; = "content"

.field public static final CREATE_TIME:Ljava/lang/String; = "CreateTime"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final MODIFIED_TIME:Ljava/lang/String; = "ModifiedTime"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PATH:Ljava/lang/String; = "path"

.field public static final SYNC_NAME:Ljava/lang/String; = "syncname"

.field public static final SYNC_PATH:Ljava/lang/String; = "syncpath"

.field public static final TEMPLATE_TYPE:Ljava/lang/String; = "TemplateType"
