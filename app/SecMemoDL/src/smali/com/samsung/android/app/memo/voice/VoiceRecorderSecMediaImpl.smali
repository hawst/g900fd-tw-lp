.class public Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;
.super Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
.source "VoiceRecorderSecMediaImpl.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mNowRecording:Z

.field private mRecordStartTime:J

.field private mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

.field private mRecording:Z

.field private recordTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-string v0, "VoiceRecorderSecMediaImpl"

    sput-object v0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;-><init>(Landroid/content/Context;)V

    .line 45
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->recordTime:J

    .line 49
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;J)V
    .locals 1

    .prologue
    .line 45
    iput-wide p1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->recordTime:J

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public RecordStartFail(Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;)V
    .locals 4
    .param p1, "mListner"    # Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    .prologue
    const/4 v3, 0x0

    .line 72
    if-eqz p1, :cond_0

    .line 73
    invoke-interface {p1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;->onStopRecording()V

    .line 76
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->reset()V

    .line 77
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    iput-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    .line 83
    :goto_0
    if-eqz p1, :cond_1

    .line 84
    invoke-interface {p1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;->onFailRecording()V

    .line 85
    :cond_1
    return-void

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->TAG:Ljava/lang/String;

    const-string v2, "VoiceRecorderProxy"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    iput-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    goto :goto_0

    .line 80
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    .line 81
    iput-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    .line 82
    throw v1
.end method

.method public getCurRecordingTime()J
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->recordTime:J

    return-wide v0
.end method

.method public isNowRecording()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mNowRecording:Z

    return v0
.end method

.method public isNowStopped()Z
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public isRecordActive(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 184
    const/4 v0, 0x0

    .line 192
    .local v0, "bRecording":Z
    return v0
.end method

.method public isRecording()Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecording:Z

    return v0
.end method

.method public pauseRecording()V
    .locals 3

    .prologue
    .line 198
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->pause()V

    .line 199
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecording:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :goto_0
    return-void

    .line 200
    :catch_0
    move-exception v0

    .line 201
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->TAG:Ljava/lang/String;

    const-string v2, "pauseRecording exception"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public resumeRecording()V
    .locals 3

    .prologue
    .line 207
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    if-nez v1, :cond_0

    .line 217
    :goto_0
    return-void

    .line 211
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->resume()V

    .line 212
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecording:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->TAG:Ljava/lang/String;

    const-string v2, "resumeRecording exception"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public startRecording(Lcom/samsung/android/app/memo/voice/VoiceInfo;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;)Z
    .locals 8
    .param p1, "voiceFile"    # Lcom/samsung/android/app/memo/voice/VoiceInfo;
    .param p2, "mListner"    # Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 94
    iget-boolean v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mNowRecording:Z

    if-eqz v6, :cond_1

    .line 179
    :cond_0
    :goto_0
    return v4

    .line 96
    :cond_1
    if-eqz p1, :cond_0

    .line 98
    const/4 v2, 0x1

    .line 100
    .local v2, "isMicrophoneEnabled":Z
    :try_start_0
    invoke-static {}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getInstance()Landroid/sec/enterprise/EnterpriseDeviceManager;

    move-result-object v6

    .line 101
    invoke-virtual {v6}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/sec/enterprise/RestrictionPolicy;

    move-result-object v3

    .line 102
    .local v3, "rp":Landroid/sec/enterprise/RestrictionPolicy;
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Landroid/sec/enterprise/RestrictionPolicy;->isMicrophoneEnabled(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 107
    .end local v3    # "rp":Landroid/sec/enterprise/RestrictionPolicy;
    :goto_1
    if-nez v2, :cond_3

    .line 108
    sget-object v5, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mToastMicrophoneIsDisabled:Landroid/widget/Toast;

    if-nez v5, :cond_2

    .line 109
    iget-object v5, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mActivity:Landroid/content/Context;

    .line 110
    const v6, 0x7f0b0099

    .line 109
    invoke-static {v5, v6, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    sput-object v5, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mToastMicrophoneIsDisabled:Landroid/widget/Toast;

    .line 111
    :cond_2
    sget-object v5, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mToastMicrophoneIsDisabled:Landroid/widget/Toast;

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 103
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->TAG:Ljava/lang/String;

    const-string v7, "VoiceRecorderProxy"

    invoke-static {v6, v7, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 114
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->requestAudioFocus()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 115
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    goto :goto_0

    .line 119
    :cond_4
    new-instance v1, Landroid/content/Intent;

    const-string v6, "com.android.music.musicservicecommand"

    invoke-direct {v1, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 120
    .local v1, "i":Landroid/content/Intent;
    const-string v6, "command"

    const-string v7, "pause"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 121
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mActivity:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 122
    new-instance v6, Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-direct {v6}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;-><init>()V

    iput-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    .line 123
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v6, v5}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioSource(I)V

    .line 124
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v6, v5}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setOutputFormat(I)V

    .line 125
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioEncoder(I)V

    .line 126
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    iget-object v7, p1, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 127
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const/16 v7, 0xc8

    invoke-virtual {v6, v7}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setDurationInterval(I)V

    .line 128
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const v7, 0x1f400

    invoke-virtual {v6, v7}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioEncodingBitRate(I)V

    .line 129
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    const v7, 0xac44

    invoke-virtual {v6, v7}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioSamplingRate(I)V

    .line 130
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    new-instance v7, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl$1;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl$1;-><init>(Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;)V

    invoke-virtual {v6, v7}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setOnInfoListener(Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;)V

    .line 150
    :try_start_1
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v6}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->prepare()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 163
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecordStartTime:J

    .line 164
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->recordTime:J

    .line 165
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v6}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->start()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 172
    iput-boolean v5, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mNowRecording:Z

    .line 173
    iput-boolean v5, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecording:Z

    .line 174
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mCurVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 175
    iput-object p2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    .line 176
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    if-eqz v4, :cond_5

    .line 177
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mCurVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    invoke-interface {v4, v6}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;->onStartRecording(Lcom/samsung/android/app/memo/voice/VoiceInfo;)V

    :cond_5
    move v4, v5

    .line 179
    goto/16 :goto_0

    .line 151
    :catch_1
    move-exception v0

    .line 152
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v5, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->TAG:Ljava/lang/String;

    const-string v6, "StartRecording exception"

    invoke-static {v5, v6, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 153
    invoke-virtual {p0, p2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->RecordStartFail(Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;)V

    goto/16 :goto_0

    .line 155
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_2
    move-exception v0

    .line 156
    .local v0, "e":Ljava/io/IOException;
    sget-object v5, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->TAG:Ljava/lang/String;

    const-string v6, "StartRecording exception"

    invoke-static {v5, v6, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 157
    invoke-virtual {p0, p2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->RecordStartFail(Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;)V

    goto/16 :goto_0

    .line 166
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 167
    .local v0, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->TAG:Ljava/lang/String;

    const-string v6, "StartRecording exception"

    invoke-static {v5, v6, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 168
    invoke-virtual {p0, p2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->RecordStartFail(Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;)V

    goto/16 :goto_0
.end method

.method public stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 221
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mCurVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mNowRecording:Z

    if-nez v3, :cond_1

    :cond_0
    move-object v1, v2

    .line 249
    :goto_0
    return-object v1

    .line 224
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v3}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->stop()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mCurVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 229
    iget-wide v6, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecordStartTime:J

    sub-long/2addr v4, v6

    .line 228
    iput-wide v4, v3, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTotalTime:J

    .line 230
    iput-boolean v8, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mNowRecording:Z

    .line 231
    iput-boolean v8, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecording:Z

    .line 232
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mCurVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-wide v4, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecordStartTime:J

    iput-wide v4, v3, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mRecordStartTime:J

    .line 234
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    invoke-virtual {v3}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->release()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 238
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    .line 241
    :goto_2
    const/4 v3, -0x1

    iput v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mPlayerState:I

    .line 242
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    if-eqz v3, :cond_2

    .line 243
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    invoke-interface {v3}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;->onStopRecording()V

    .line 245
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mCurVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 246
    .local v1, "vInfo":Lcom/samsung/android/app/memo/voice/VoiceInfo;
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mCurVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 247
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->abandonAudioFocus()V

    goto :goto_0

    .line 225
    .end local v1    # "vInfo":Lcom/samsung/android/app/memo/voice/VoiceInfo;
    :catch_0
    move-exception v0

    .line 226
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v3, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->TAG:Ljava/lang/String;

    const-string v4, "stopRecording exception"

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 235
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v0

    .line 236
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v3, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->TAG:Ljava/lang/String;

    const-string v4, "stopRecording exception"

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 238
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    goto :goto_2

    .line 237
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    .line 238
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->mRecorder:Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    .line 239
    throw v3
.end method
