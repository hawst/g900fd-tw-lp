.class public final Lcom/samsung/android/app/memo/util/ImageUtil;
.super Ljava/lang/Object;
.source "ImageUtil.java"


# static fields
.field public static final DEFAULT_JPEG_COMPRESS_LEVEL:I = 0x50

.field public static final KEY_DISPLAYNAME:Ljava/lang/String;

.field public static final KEY_MIMETYPE:Ljava/lang/String;

.field public static final KEY_ORIENTATION:Ljava/lang/String;

.field public static final MAX_IMAGE_SAVE_SIZE:I = 0x1000

.field public static final MIME_TYPE_JPEG:Ljava/lang/String; = "image/jpeg"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 64
    const-class v0, Lcom/samsung/android/app/memo/util/ImageUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_orientation"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_ORIENTATION:Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_displayname"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_DISPLAYNAME:Ljava/lang/String;

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_mimetype"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_MIMETYPE:Ljava/lang/String;

    .line 74
    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 499
    sget-object v0, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    return-void
.end method

.method private static bitmapRecycle(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "part"    # Landroid/graphics/Bitmap;

    .prologue
    .line 733
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 734
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 735
    const/4 p0, 0x0

    .line 737
    :cond_0
    return-object p0
.end method

.method public static final calculateInSampleSize(II)I
    .locals 3
    .param p0, "sourceWith"    # I
    .param p1, "maxWidth"    # I

    .prologue
    .line 347
    const/4 v0, 0x1

    .line 349
    .local v0, "inSampleSize":I
    :goto_0
    mul-int v2, p1, v0

    div-int v1, p0, v2

    .line 350
    .local v1, "temp":I
    if-nez v1, :cond_0

    .line 351
    div-int/lit8 v0, v0, 0x2

    .line 356
    return v0

    .line 354
    :cond_0
    mul-int/lit8 v0, v0, 0x2

    .line 348
    goto :goto_0
.end method

.method public static containVideoUri(Ljava/util/ArrayList;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 741
    .local p0, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v1, 0x0

    .line 743
    .local v1, "mContainVideo":Z
    :try_start_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 764
    :goto_0
    return v1

    .line 743
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 744
    .local v3, "uri":Landroid/net/Uri;
    const/4 v2, 0x0

    .line 745
    .local v2, "mime_type":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 748
    const-string v5, "content"

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "video"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 749
    const/4 v1, 0x1

    .line 750
    goto :goto_0

    .line 751
    :cond_2
    const-string v5, "file"

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 752
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/net/URLConnection;->guessContentTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 753
    if-eqz v2, :cond_0

    const-string v5, "video"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_0

    .line 754
    const/4 v1, 0x1

    .line 755
    goto :goto_0

    .line 761
    .end local v2    # "mime_type":Ljava/lang/String;
    .end local v3    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 762
    .local v0, "cme":Ljava/util/ConcurrentModificationException;
    sget-object v4, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    const-string v5, "containVideoUri"

    invoke-static {v4, v5, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static createRecycleBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "bm"    # Landroid/graphics/Bitmap;
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    const/4 v1, 0x0

    .line 261
    const/4 v7, 0x0

    .line 262
    .local v7, "scaledBm":Landroid/graphics/Bitmap;
    if-eqz p0, :cond_0

    .line 263
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    move v2, v1

    move-object v5, p1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 264
    sget-object v0, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    const-string v1, "decodeImageScaledIf() passed through post processing"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 266
    const/4 p0, 0x0

    .line 268
    :cond_0
    return-object v7
.end method

.method public static createThumbnail(Landroid/content/Context;Ljava/lang/String;)V
    .locals 29
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
        }
    .end annotation

    .prologue
    .line 610
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isFromDeleteMemo()Z

    move-result v25

    if-eqz v25, :cond_1

    .line 611
    const/16 v25, 0x0

    invoke-static/range {v25 .. v25}, Lcom/samsung/android/app/memo/util/Utils;->SetIsFromDeleteMemo(Z)V

    .line 730
    :cond_0
    :goto_0
    return-void

    .line 616
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v12

    .line 617
    .local v12, "f":Ljava/io/File;
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 618
    .local v24, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 619
    .local v17, "orientationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static/range {p0 .. p1}, Lcom/samsung/android/app/memo/MemoDataHandler;->getContent(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 620
    .local v5, "Content":Ljava/lang/String;
    move-object/from16 v0, v24

    move-object/from16 v1, v17

    invoke-static {v5, v0, v1}, Lcom/samsung/android/app/memo/util/ImageUtil;->customHtmlParser(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)I

    .line 622
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v16

    .line 623
    .local v16, "listSize":I
    if-nez v16, :cond_2

    .line 624
    sget-object v25, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    const-string v26, "createThumbnail() delete thumbnail"

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    invoke-static/range {p0 .. p1}, Lcom/samsung/android/app/memo/MemoDataHandler;->clearDataFor(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 628
    :cond_2
    const v25, 0x7f090093

    invoke-static/range {v25 .. v25}, Lcom/samsung/android/app/memo/util/Utils;->getDimension(I)F

    move-result v25

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v23, v0

    .line 629
    .local v23, "thumbSize":I
    new-instance v20, Landroid/graphics/Paint;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Paint;-><init>()V

    .line 630
    .local v20, "paint":Landroid/graphics/Paint;
    const/16 v25, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 631
    sget-object v25, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v23

    move/from16 v1, v23

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 632
    .local v6, "bmThumb":Landroid/graphics/Bitmap;
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 633
    .local v7, "canvas":Landroid/graphics/Canvas;
    new-instance v22, Landroid/graphics/Rect;

    invoke-direct/range {v22 .. v22}, Landroid/graphics/Rect;-><init>()V

    .line 634
    .local v22, "srcRect":Landroid/graphics/Rect;
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 635
    .local v8, "dstRect":Landroid/graphics/Rect;
    const/16 v21, 0x0

    .line 636
    .local v21, "part":Landroid/graphics/Bitmap;
    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v23

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 638
    packed-switch v16, :pswitch_data_0

    .line 684
    const/16 v25, 0x0

    :try_start_0
    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/net/Uri;

    const/16 v26, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v26

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v23

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->decodeCustom(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 685
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v25

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v26

    const/16 v27, 0x0

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v23

    move/from16 v3, v27

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->getRect(IIIZ)Landroid/graphics/Rect;

    move-result-object v22

    .line 686
    const/16 v25, 0x0

    const/16 v26, 0x0

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v23

    move/from16 v3, v23

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 687
    if-eqz v21, :cond_3

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v25

    if-nez v25, :cond_3

    .line 688
    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-virtual {v7, v0, v1, v8, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 689
    :cond_3
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/app/memo/util/ImageUtil;->bitmapRecycle(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v21

    .line 697
    :cond_4
    :goto_1
    invoke-virtual {v12}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/io/File;->mkdirs()Z

    .line 699
    :try_start_1
    invoke-virtual {v12}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 705
    const/16 v18, 0x0

    .line 707
    .local v18, "outStream":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v19, Ljava/io/FileOutputStream;

    move-object/from16 v0, v19

    invoke-direct {v0, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 708
    .end local v18    # "outStream":Ljava/io/FileOutputStream;
    .local v19, "outStream":Ljava/io/FileOutputStream;
    :try_start_3
    sget-object v25, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v26, 0x50

    move-object/from16 v0, v25

    move/from16 v1, v26

    move-object/from16 v2, v19

    invoke-virtual {v6, v0, v1, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 714
    if-eqz v19, :cond_5

    .line 716
    :try_start_4
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 721
    :cond_5
    :goto_2
    sget-object v25, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "createThumbnail() set thumbnail: "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    new-instance v14, Landroid/content/Intent;

    const-string v25, "com.samsung.android.app.memo.UPDATE_THUMB_IMAGE"

    move-object/from16 v0, v25

    invoke-direct {v14, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 724
    .local v14, "intent":Landroid/content/Intent;
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 725
    .local v11, "extras":Landroid/os/Bundle;
    const-string v25, "_data"

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v11, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    invoke-virtual {v14, v11}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 727
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 729
    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/MemoDataHandler;->setDataFor(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 640
    .end local v11    # "extras":Landroid/os/Bundle;
    .end local v14    # "intent":Landroid/content/Intent;
    .end local v19    # "outStream":Ljava/io/FileOutputStream;
    :pswitch_0
    :try_start_5
    rem-int/lit8 v25, v23, 0x2

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_6

    .line 641
    add-int/lit8 v23, v23, 0x1

    .line 642
    :cond_6
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_3
    move/from16 v0, v16

    if-ge v13, v0, :cond_4

    .line 643
    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/net/Uri;

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v26

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v23

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->decodeCustom(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 644
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v25

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v23

    move/from16 v3, v27

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->getRect(IIIZ)Landroid/graphics/Rect;

    move-result-object v22

    .line 645
    const/16 v25, 0x0

    mul-int v26, v13, v23

    div-int/lit8 v26, v26, 0x2

    div-int/lit8 v27, v23, 0x2

    mul-int v28, v23, v13

    div-int/lit8 v28, v28, 0x2

    add-int v27, v27, v28

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v23

    move/from16 v3, v27

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 646
    if-eqz v21, :cond_7

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v25

    if-nez v25, :cond_7

    .line 647
    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-virtual {v7, v0, v1, v8, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 648
    :cond_7
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/app/memo/util/ImageUtil;->bitmapRecycle(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 642
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    .line 652
    .end local v13    # "i":I
    :pswitch_1
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_4
    move/from16 v0, v16

    if-ge v13, v0, :cond_4

    .line 654
    if-lez v13, :cond_9

    .line 655
    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/net/Uri;

    .line 656
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v26

    div-int/lit8 v27, v23, 0x2

    .line 655
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->decodeCustom(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 657
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v25

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v26

    const/16 v27, 0x0

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v23

    move/from16 v3, v27

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->getRect(IIIZ)Landroid/graphics/Rect;

    move-result-object v22

    .line 658
    if-eqz v21, :cond_8

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v25

    if-nez v25, :cond_8

    .line 659
    div-int/lit8 v25, v23, 0x2

    add-int/lit8 v26, v13, 0x1

    rem-int/lit8 v26, v26, 0x2

    mul-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    div-int/lit8 v26, v23, 0x2

    .line 660
    add-int/lit8 v27, v13, 0x1

    div-int/lit8 v27, v27, 0x2

    mul-int v26, v26, v27

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    .line 659
    move-object/from16 v0, v21

    move/from16 v1, v25

    move/from16 v2, v26

    move-object/from16 v3, v20

    invoke-virtual {v7, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 669
    :cond_8
    :goto_5
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/app/memo/util/ImageUtil;->bitmapRecycle(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 652
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 662
    :cond_9
    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/net/Uri;

    .line 663
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v26

    .line 662
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v23

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->decodeCustom(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 664
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v25

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v26

    const/16 v27, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v23

    move/from16 v3, v27

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->getRect(IIIZ)Landroid/graphics/Rect;

    move-result-object v22

    .line 665
    const/16 v25, 0x0

    const/16 v26, 0x0

    div-int/lit8 v27, v23, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v23

    move/from16 v3, v27

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 666
    if-eqz v21, :cond_8

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v25

    if-nez v25, :cond_8

    .line 667
    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-virtual {v7, v0, v1, v8, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_5

    .line 692
    .end local v13    # "i":I
    :catch_0
    move-exception v10

    .line 693
    .local v10, "e1":Ljava/io/IOException;
    sget-object v25, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    const-string v26, "createThumbnail()"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v10}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 673
    .end local v10    # "e1":Ljava/io/IOException;
    :pswitch_2
    const/4 v13, 0x0

    .restart local v13    # "i":I
    :goto_6
    const/16 v25, 0x4

    move/from16 v0, v25

    if-ge v13, v0, :cond_4

    .line 674
    :try_start_6
    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/net/Uri;

    .line 675
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/Integer;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Integer;->intValue()I

    move-result v26

    div-int/lit8 v27, v23, 0x2

    .line 674
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->decodeCustom(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 676
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v25

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v26

    div-int/lit8 v27, v23, 0x2

    const/16 v28, 0x0

    invoke-static/range {v25 .. v28}, Lcom/samsung/android/app/memo/util/ImageUtil;->getRect(IIIZ)Landroid/graphics/Rect;

    move-result-object v22

    .line 677
    if-eqz v21, :cond_a

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v25

    if-nez v25, :cond_a

    .line 678
    div-int/lit8 v25, v23, 0x2

    rem-int/lit8 v26, v13, 0x2

    mul-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    div-int/lit8 v26, v23, 0x2

    div-int/lit8 v27, v13, 0x2

    mul-int v26, v26, v27

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    move-object/from16 v0, v21

    move/from16 v1, v25

    move/from16 v2, v26

    move-object/from16 v3, v20

    invoke-virtual {v7, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 680
    :cond_a
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/app/memo/util/ImageUtil;->bitmapRecycle(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    move-result-object v21

    .line 673
    add-int/lit8 v13, v13, 0x1

    goto :goto_6

    .line 700
    .end local v13    # "i":I
    :catch_1
    move-exception v9

    .line 701
    .local v9, "e":Ljava/io/IOException;
    sget-object v25, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    const-string v26, "not able to create thumbnail"

    invoke-static/range {v25 .. v26}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    invoke-static/range {p0 .. p1}, Lcom/samsung/android/app/memo/MemoDataHandler;->clearDataFor(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 709
    .end local v9    # "e":Ljava/io/IOException;
    .restart local v18    # "outStream":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v15

    .line 710
    .local v15, "ioe":Ljava/io/IOException;
    :goto_7
    :try_start_7
    sget-object v25, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    const-string v26, "IO exception"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v15}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 711
    invoke-virtual {v12}, Ljava/io/File;->delete()Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 714
    if-eqz v18, :cond_0

    .line 716
    :try_start_8
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto/16 :goto_0

    .line 717
    :catch_3
    move-exception v9

    .line 718
    .restart local v9    # "e":Ljava/io/IOException;
    sget-object v25, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    const-string v26, "createThumbnail()"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 713
    .end local v9    # "e":Ljava/io/IOException;
    .end local v15    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v25

    .line 714
    :goto_8
    if-eqz v18, :cond_b

    .line 716
    :try_start_9
    invoke-virtual/range {v18 .. v18}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    .line 720
    :cond_b
    :goto_9
    throw v25

    .line 717
    :catch_4
    move-exception v9

    .line 718
    .restart local v9    # "e":Ljava/io/IOException;
    sget-object v26, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    const-string v27, "createThumbnail()"

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_9

    .line 717
    .end local v9    # "e":Ljava/io/IOException;
    .end local v18    # "outStream":Ljava/io/FileOutputStream;
    .restart local v19    # "outStream":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v9

    .line 718
    .restart local v9    # "e":Ljava/io/IOException;
    sget-object v25, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    const-string v26, "createThumbnail()"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 713
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v25

    move-object/from16 v18, v19

    .end local v19    # "outStream":Ljava/io/FileOutputStream;
    .restart local v18    # "outStream":Ljava/io/FileOutputStream;
    goto :goto_8

    .line 709
    .end local v18    # "outStream":Ljava/io/FileOutputStream;
    .restart local v19    # "outStream":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v15

    move-object/from16 v18, v19

    .end local v19    # "outStream":Ljava/io/FileOutputStream;
    .restart local v18    # "outStream":Ljava/io/FileOutputStream;
    goto :goto_7

    .line 638
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static createThumbnailOnDiscreteThread(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 604
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/memo/MemoAutoSaveService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 605
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "uuid"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 606
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 607
    return-void
.end method

.method public static customHtmlParser(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)I
    .locals 7
    .param p0, "content"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 546
    .local p1, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .local p2, "orientationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 547
    :cond_0
    const/4 v2, 0x0

    .line 564
    :cond_1
    return v2

    .line 549
    :cond_2
    const/4 v2, 0x0

    .line 550
    .local v2, "listVal":I
    const/4 v4, 0x0

    .line 551
    .local v4, "start":I
    const/4 v0, 0x0

    .line 552
    .local v0, "end":I
    const-string v1, "<img src="

    .line 553
    .local v1, "imageTag":Ljava/lang/String;
    const-string v3, " orientation="

    .line 554
    .local v3, "orientationTag":Ljava/lang/String;
    invoke-virtual {p0, v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v4

    .line 555
    :goto_0
    if-lez v4, :cond_1

    const/4 v5, 0x4

    if-ge v2, v5, :cond_1

    .line 556
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {p0, v3, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 557
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v4

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 558
    const-string v5, " "

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v4

    .line 559
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v0

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v6, v4, -0x1

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 560
    const-string v5, "<img src"

    invoke-virtual {p0, v5, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v4

    .line 561
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static final decodeCustom(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "orientation"    # I
    .param p3, "maxDimen"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/high16 v8, 0x42480000    # 50.0f

    const/high16 v11, 0x3f800000    # 1.0f

    .line 284
    invoke-static {p0, p1}, Lcom/samsung/android/app/memo/util/ImageUtil;->getImageSize(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v3

    .line 286
    .local v3, "opts":Landroid/graphics/BitmapFactory$Options;
    iget v9, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v10, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-ge v9, v10, :cond_1

    iget v7, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 288
    .local v7, "sourceDimen":I
    :goto_0
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    .end local v3    # "opts":Landroid/graphics/BitmapFactory$Options;
    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 289
    .restart local v3    # "opts":Landroid/graphics/BitmapFactory$Options;
    sget-object v9, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v9, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 290
    invoke-static {v7, p3}, Lcom/samsung/android/app/memo/util/ImageUtil;->calculateInSampleSize(II)I

    move-result v9

    iput v9, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 293
    const/4 v1, 0x0

    .line 295
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-virtual {v9, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 296
    const/4 v9, 0x0

    invoke-static {v1, v9, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 298
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 300
    :cond_0
    :goto_1
    if-nez v0, :cond_3

    .line 301
    new-instance v8, Ljava/io/IOException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "fail to decode: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 286
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v1    # "is":Ljava/io/InputStream;
    .end local v7    # "sourceDimen":I
    :cond_1
    iget v7, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    goto :goto_0

    .line 297
    .restart local v1    # "is":Ljava/io/InputStream;
    .restart local v7    # "sourceDimen":I
    :catchall_0
    move-exception v8

    .line 298
    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 299
    :cond_2
    :goto_2
    throw v8

    .line 303
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 304
    const/16 v9, 0x5a

    if-eq p2, v9, :cond_4

    const/16 v9, 0x10e

    if-ne p2, v9, :cond_c

    .line 305
    :cond_4
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-static {v9, v10}, Lcom/samsung/android/app/memo/util/ImageUtil;->isPanorama(II)Z

    move-result v9

    if-nez v9, :cond_5

    .line 306
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 312
    :cond_5
    :goto_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-static {v9, v10}, Lcom/samsung/android/app/memo/util/ImageUtil;->isPanorama(II)Z

    move-result v9

    if-nez v9, :cond_6

    .line 313
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    if-ge v9, v10, :cond_d

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 315
    :cond_6
    :goto_4
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 316
    .local v2, "matrix":Landroid/graphics/Matrix;
    int-to-float v9, p3

    int-to-float v10, v7

    div-float v4, v9, v10

    .line 317
    .local v4, "scaleFactor":F
    cmpl-float v9, v4, v8

    if-lez v9, :cond_7

    move v4, v8

    .line 319
    :cond_7
    move v6, v4

    .local v6, "scaleFactorY":F
    move v5, v4

    .line 321
    .local v5, "scaleFactorX":F
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v5

    int-to-float v9, p3

    cmpg-float v8, v8, v9

    if-gez v8, :cond_8

    .line 322
    int-to-float v8, p3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    div-float v5, v8, v9

    .line 323
    :cond_8
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v6

    int-to-float v9, p3

    cmpg-float v8, v8, v9

    if-gez v8, :cond_9

    .line 324
    int-to-float v8, p3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    div-float v6, v8, v9

    .line 326
    :cond_9
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v5

    cmpl-float v8, v8, v11

    if-lez v8, :cond_e

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v6

    cmpl-float v8, v8, v11

    if-lez v8, :cond_e

    .line 327
    invoke-virtual {v2, v5, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 334
    :goto_5
    int-to-float v8, p2

    invoke-virtual {v2, v8}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 335
    float-to-double v8, v4

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    cmpl-double v8, v8, v10

    if-nez v8, :cond_a

    if-eqz p2, :cond_b

    .line 336
    :cond_a
    invoke-static {v0, v2}, Lcom/samsung/android/app/memo/util/ImageUtil;->createRecycleBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 337
    :cond_b
    sget-object v8, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "decodeImageScaledIf() width="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    return-object v0

    .line 308
    .end local v2    # "matrix":Landroid/graphics/Matrix;
    .end local v4    # "scaleFactor":F
    .end local v5    # "scaleFactorX":F
    .end local v6    # "scaleFactorY":F
    :cond_c
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-static {v9, v10}, Lcom/samsung/android/app/memo/util/ImageUtil;->isPanorama(II)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 309
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    if-le v9, v10, :cond_5

    .line 310
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    goto/16 :goto_3

    .line 313
    :cond_d
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    goto/16 :goto_4

    .line 329
    .restart local v2    # "matrix":Landroid/graphics/Matrix;
    .restart local v4    # "scaleFactor":F
    .restart local v5    # "scaleFactorX":F
    .restart local v6    # "scaleFactorY":F
    :cond_e
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v6

    cmpl-float v8, v8, v11

    if-lez v8, :cond_f

    .line 330
    invoke-virtual {v2, v11, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_5

    .line 332
    :cond_f
    invoke-virtual {v2, v5, v11}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_5

    .line 298
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v2    # "matrix":Landroid/graphics/Matrix;
    .end local v4    # "scaleFactor":F
    .end local v5    # "scaleFactorX":F
    .end local v6    # "scaleFactorY":F
    :catch_0
    move-exception v9

    goto/16 :goto_2

    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v9

    goto/16 :goto_1
.end method

.method public static final decodeImageScaledForDB(Landroid/content/Context;Landroid/net/Uri;III)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "sourceWidth"    # I
    .param p3, "orientation"    # I
    .param p4, "maxWidth"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    .line 191
    const/4 v6, -0x1

    if-ne p2, v6, :cond_0

    .line 192
    invoke-static {p0, p1, p3}, Lcom/samsung/android/app/memo/util/ImageUtil;->getActualImageWidth(Landroid/content/Context;Landroid/net/Uri;I)I

    move-result p2

    .line 194
    :cond_0
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 195
    .local v4, "opts":Landroid/graphics/BitmapFactory$Options;
    sget-object v6, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v6, v4, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 196
    invoke-static {p2, p4}, Lcom/samsung/android/app/memo/util/ImageUtil;->calculateInSampleSize(II)I

    move-result v6

    iput v6, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 199
    const/4 v2, 0x0

    .line 201
    .local v2, "is":Ljava/io/InputStream;
    if-eqz p1, :cond_1

    if-eqz p0, :cond_1

    .line 202
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 203
    :cond_1
    if-eqz v2, :cond_2

    .line 204
    sget-object v6, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Uri="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", size in bytes: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/InputStream;->available()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_2
    const/4 v6, 0x0

    invoke-static {v2, v6, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 207
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_3

    .line 209
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 214
    :cond_3
    :goto_0
    if-nez v0, :cond_5

    .line 215
    new-instance v6, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "fail to decode: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 206
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v6

    .line 207
    if-eqz v2, :cond_4

    .line 209
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 212
    :cond_4
    :goto_1
    throw v6

    .line 218
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_5
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 219
    .local v1, "bmWidth":I
    const/16 v6, 0x5a

    if-eq p3, v6, :cond_6

    const/16 v6, 0x10e

    if-ne p3, v6, :cond_b

    .line 220
    :cond_6
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/ImageUtil;->isPanorama(II)Z

    move-result v6

    if-nez v6, :cond_7

    .line 221
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 229
    :cond_7
    :goto_2
    if-nez p3, :cond_8

    if-le v1, p4, :cond_e

    .line 230
    :cond_8
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 231
    .local v3, "matrix":Landroid/graphics/Matrix;
    if-le v1, p4, :cond_9

    .line 232
    int-to-float v6, p4

    int-to-float v7, v1

    div-float v5, v6, v7

    .line 233
    .local v5, "scaleFactor":F
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v5

    cmpl-float v6, v6, v9

    if-lez v6, :cond_c

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v5

    cmpl-float v6, v6, v9

    if-lez v6, :cond_c

    .line 234
    invoke-virtual {v3, v5, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 243
    .end local v5    # "scaleFactor":F
    :cond_9
    :goto_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/ImageUtil;->isPanorama(II)Z

    move-result v6

    if-nez v6, :cond_a

    .line 244
    invoke-static {v0, v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->createRecycleBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 255
    .end local v3    # "matrix":Landroid/graphics/Matrix;
    :cond_a
    :goto_4
    sget-object v6, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "decodeImageScaledIf() width="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    return-object v0

    .line 223
    :cond_b
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/ImageUtil;->isPanorama(II)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 224
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    if-le v6, v7, :cond_7

    .line 225
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    goto :goto_2

    .line 236
    .restart local v3    # "matrix":Landroid/graphics/Matrix;
    .restart local v5    # "scaleFactor":F
    :cond_c
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v5

    cmpl-float v6, v6, v9

    if-lez v6, :cond_d

    .line 237
    invoke-virtual {v3, v9, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_3

    .line 239
    :cond_d
    invoke-virtual {v3, v5, v9}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_3

    .line 245
    .end local v3    # "matrix":Landroid/graphics/Matrix;
    .end local v5    # "scaleFactor":F
    :cond_e
    if-ge v1, p4, :cond_a

    .line 246
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 247
    .restart local v3    # "matrix":Landroid/graphics/Matrix;
    int-to-float v6, p4

    int-to-float v7, v1

    div-float v5, v6, v7

    .line 248
    .restart local v5    # "scaleFactor":F
    invoke-virtual {v3, v5, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 252
    invoke-static {v0, v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->createRecycleBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_4

    .line 210
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v1    # "bmWidth":I
    .end local v3    # "matrix":Landroid/graphics/Matrix;
    .end local v5    # "scaleFactor":F
    :catch_0
    move-exception v7

    goto/16 :goto_1

    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v6

    goto/16 :goto_0
.end method

.method public static final decodeImageScaledIf(Landroid/content/Context;Landroid/net/Uri;III)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "sourceWidth"    # I
    .param p3, "orientation"    # I
    .param p4, "maxWidth"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    .line 124
    const/4 v6, -0x1

    if-ne p2, v6, :cond_0

    .line 125
    invoke-static {p0, p1, p3}, Lcom/samsung/android/app/memo/util/ImageUtil;->getActualImageWidth(Landroid/content/Context;Landroid/net/Uri;I)I

    move-result p2

    .line 127
    :cond_0
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 128
    .local v4, "opts":Landroid/graphics/BitmapFactory$Options;
    sget-object v6, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v6, v4, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 129
    invoke-static {p2, p4}, Lcom/samsung/android/app/memo/util/ImageUtil;->calculateInSampleSize(II)I

    move-result v6

    iput v6, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 132
    const/4 v2, 0x0

    .line 134
    .local v2, "is":Ljava/io/InputStream;
    if-eqz p1, :cond_1

    if-eqz p0, :cond_1

    .line 135
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 136
    :cond_1
    if-eqz v2, :cond_2

    .line 137
    sget-object v6, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Uri="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", size in bytes: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/io/InputStream;->available()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_2
    const/4 v6, 0x0

    invoke-static {v2, v6, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 140
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_3

    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 143
    :cond_3
    :goto_0
    if-nez v0, :cond_5

    .line 144
    new-instance v6, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "fail to decode: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 139
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v6

    .line 140
    if-eqz v2, :cond_4

    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 141
    :cond_4
    :goto_1
    throw v6

    .line 147
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_5
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 148
    .local v1, "bmWidth":I
    const/16 v6, 0x5a

    if-eq p3, v6, :cond_6

    const/16 v6, 0x10e

    if-ne p3, v6, :cond_b

    .line 149
    :cond_6
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/ImageUtil;->isPanorama(II)Z

    move-result v6

    if-nez v6, :cond_7

    .line 150
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 158
    :cond_7
    :goto_2
    if-nez p3, :cond_8

    if-le v1, p4, :cond_e

    .line 159
    :cond_8
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 160
    .local v3, "matrix":Landroid/graphics/Matrix;
    if-le v1, p4, :cond_9

    .line 161
    int-to-float v6, p4

    int-to-float v7, v1

    div-float v5, v6, v7

    .line 162
    .local v5, "scaleFactor":F
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v5

    cmpl-float v6, v6, v9

    if-lez v6, :cond_c

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v5

    cmpl-float v6, v6, v9

    if-lez v6, :cond_c

    .line 163
    invoke-virtual {v3, v5, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 171
    .end local v5    # "scaleFactor":F
    :cond_9
    :goto_3
    int-to-float v6, p3

    invoke-virtual {v3, v6}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 172
    invoke-static {v0, v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->createRecycleBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 184
    .end local v3    # "matrix":Landroid/graphics/Matrix;
    :cond_a
    :goto_4
    sget-object v6, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "decodeImageScaledIf() width="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    return-object v0

    .line 152
    :cond_b
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/ImageUtil;->isPanorama(II)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 153
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    if-le v6, v7, :cond_7

    .line 154
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    goto :goto_2

    .line 165
    .restart local v3    # "matrix":Landroid/graphics/Matrix;
    .restart local v5    # "scaleFactor":F
    :cond_c
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v5

    cmpl-float v6, v6, v9

    if-lez v6, :cond_d

    .line 166
    invoke-virtual {v3, v9, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_3

    .line 168
    :cond_d
    invoke-virtual {v3, v5, v9}, Landroid/graphics/Matrix;->postScale(FF)Z

    goto :goto_3

    .line 174
    .end local v3    # "matrix":Landroid/graphics/Matrix;
    .end local v5    # "scaleFactor":F
    :cond_e
    if-ge v1, p4, :cond_a

    .line 175
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 176
    .restart local v3    # "matrix":Landroid/graphics/Matrix;
    int-to-float v6, p4

    int-to-float v7, v1

    div-float v5, v6, v7

    .line 177
    .restart local v5    # "scaleFactor":F
    invoke-virtual {v3, v5, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 178
    if-eqz p3, :cond_f

    .line 179
    int-to-float v6, p3

    invoke-virtual {v3, v6}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 181
    :cond_f
    invoke-static {v0, v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->createRecycleBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_4

    .line 140
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v1    # "bmWidth":I
    .end local v3    # "matrix":Landroid/graphics/Matrix;
    .end local v5    # "scaleFactor":F
    :catch_0
    move-exception v7

    goto/16 :goto_1

    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v6

    goto/16 :goto_0
.end method

.method public static final getActualImageWidth(Landroid/content/Context;Landroid/net/Uri;I)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "orientation"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    invoke-static {p0, p1}, Lcom/samsung/android/app/memo/util/ImageUtil;->getImageSize(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    .line 107
    .local v1, "opts":Landroid/graphics/BitmapFactory$Options;
    iget v0, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 108
    .local v0, "imageWidth":I
    const/16 v2, 0x5a

    if-eq p2, v2, :cond_0

    const/16 v2, 0x10e

    if-ne p2, v2, :cond_1

    .line 109
    :cond_0
    iget v0, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 111
    :cond_1
    return v0
.end method

.method public static getImageContentUri(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "imageFile"    # Ljava/io/File;

    .prologue
    const/4 v11, 0x0

    .line 503
    const/4 v6, 0x0

    .line 505
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    .line 506
    .local v7, "filePath":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 507
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    .line 508
    const-string v3, "_data=? "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v7, v4, v5

    .line 509
    const/4 v5, 0x0

    .line 506
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 510
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 511
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 512
    .local v8, "id":I
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    int-to-long v2, v8

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 523
    if-eqz v6, :cond_0

    .line 524
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 526
    .end local v7    # "filePath":Ljava/lang/String;
    .end local v8    # "id":I
    :cond_0
    :goto_0
    return-object v0

    .line 514
    .restart local v7    # "filePath":Ljava/lang/String;
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 515
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 516
    .local v10, "values":Landroid/content/ContentValues;
    const-string v0, "_data"

    invoke-virtual {v10, v0, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 523
    if-eqz v6, :cond_0

    .line 524
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 520
    .end local v7    # "filePath":Ljava/lang/String;
    .end local v10    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v9

    .line 521
    .local v9, "sqle":Landroid/database/SQLException;
    :try_start_2
    sget-object v0, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    const-string v1, "getImageContentUri()"

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 523
    if-eqz v6, :cond_2

    .line 524
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .end local v9    # "sqle":Landroid/database/SQLException;
    :cond_2
    :goto_1
    move-object v0, v11

    .line 526
    goto :goto_0

    .line 522
    :catchall_0
    move-exception v0

    .line 523
    if-eqz v6, :cond_3

    .line 524
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 525
    :cond_3
    throw v0

    .line 523
    .restart local v7    # "filePath":Ljava/lang/String;
    :cond_4
    if-eqz v6, :cond_2

    .line 524
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method public static final getImageSize(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/BitmapFactory$Options;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contentUri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 84
    .local v1, "opts":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 86
    const/4 v0, 0x0

    .line 88
    .local v0, "is":Ljava/io/InputStream;
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 89
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 90
    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :cond_0
    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 95
    :cond_1
    :goto_0
    return-object v1

    .line 92
    :catchall_0
    move-exception v2

    .line 93
    if-eqz v0, :cond_2

    :try_start_2
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 94
    :cond_2
    :goto_1
    throw v2

    .line 93
    :catch_0
    move-exception v3

    goto :goto_1

    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method public static getImageUUIDList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .param p0, "content"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 584
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 585
    .local v2, "imageUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 586
    .local v4, "start":I
    const/4 v0, 0x0

    .line 587
    .local v0, "end":I
    const-string v1, "<img src="

    .line 588
    .local v1, "imageTag":Ljava/lang/String;
    const-string v3, " orientation="

    .line 589
    .local v3, "orientationTag":Ljava/lang/String;
    invoke-virtual {p0, v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v4

    .line 590
    :goto_0
    if-gtz v4, :cond_0

    .line 599
    return-object v2

    .line 591
    :cond_0
    add-int/lit8 v6, v4, 0x1

    invoke-virtual {p0, v3, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 592
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v4

    .line 593
    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v7, v0, -0x1

    .line 592
    invoke-virtual {p0, v6, v7}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 594
    .local v5, "uri":Landroid/net/Uri;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x2

    if-lt v6, v7, :cond_1

    .line 595
    invoke-virtual {v5}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 597
    :cond_1
    const-string v6, "<img src"

    invoke-virtual {p0, v6, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v4

    goto :goto_0
.end method

.method public static getImageUriList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p0, "content"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 568
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 569
    .local v2, "imageUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v4, 0x0

    .line 570
    .local v4, "start":I
    const/4 v0, 0x0

    .line 571
    .local v0, "end":I
    const-string v1, "<img src="

    .line 572
    .local v1, "imageTag":Ljava/lang/String;
    const-string v3, " orientation="

    .line 573
    .local v3, "orientationTag":Ljava/lang/String;
    invoke-virtual {p0, v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v4

    .line 574
    :goto_0
    if-gtz v4, :cond_0

    .line 580
    return-object v2

    .line 575
    :cond_0
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {p0, v3, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 577
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v4

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v6, v0, -0x1

    .line 576
    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 578
    const-string v5, "<img src"

    invoke-virtual {p0, v5, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v4

    goto :goto_0
.end method

.method public static getRect(IIIZ)Landroid/graphics/Rect;
    .locals 6
    .param p0, "height"    # I
    .param p1, "width"    # I
    .param p2, "thumbSize"    # I
    .param p3, "RectSize"    # Z

    .prologue
    const/4 v3, 0x0

    .line 530
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 532
    .local v0, "srcRect":Landroid/graphics/Rect;
    if-le p1, p0, :cond_1

    .line 533
    sub-int v1, p1, p2

    div-int/lit8 v1, v1, 0x2

    add-int v2, p1, p2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v3, v2, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 538
    :goto_0
    if-eqz p3, :cond_0

    .line 539
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    div-int/lit8 v3, p2, 0x3

    add-int/2addr v2, v3

    iget v3, v0, Landroid/graphics/Rect;->right:I

    iget v4, v0, Landroid/graphics/Rect;->top:I

    mul-int/lit8 v5, p2, 0x5

    div-int/lit8 v5, v5, 0x6

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 542
    :cond_0
    return-object v0

    .line 535
    :cond_1
    sub-int v1, p0, p2

    div-int/lit8 v1, v1, 0x2

    add-int v2, p0, p2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v3, v1, p2, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method public static isImageFile(Landroid/net/Uri;)Z
    .locals 14
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 814
    const/4 v13, 0x0

    .line 815
    .local v13, "returnVlaue":Z
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://gmail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 816
    const/4 v0, 0x1

    .line 838
    :goto_0
    return v0

    .line 818
    :cond_0
    const-string v11, ".*\\.(png|jpg|bmp|gif|jpeg|wbmp|PNG|JPG|GIF|BMP|JPEG|WBMP)"

    .line 819
    .local v11, "re":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 820
    .local v12, "returnCursor":Landroid/database/Cursor;
    if-eqz v12, :cond_3

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 821
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 822
    const-string v0, "_display_name"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 823
    .local v10, "nameIndex":I
    const-string v0, "mime_type"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 825
    .local v9, "mimeTypeIndex":I
    :try_start_0
    invoke-interface {v12, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 826
    .local v7, "fileName":Ljava/lang/String;
    invoke-interface {v12, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 827
    .local v8, "mimeType":Ljava/lang/String;
    if-eqz v7, :cond_1

    invoke-virtual {v7, v11}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 828
    :cond_1
    if-eqz v8, :cond_3

    const-string v0, "image/"

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_3

    .line 829
    :cond_2
    const/4 v13, 0x1

    .line 835
    .end local v7    # "fileName":Ljava/lang/String;
    .end local v8    # "mimeType":Ljava/lang/String;
    .end local v9    # "mimeTypeIndex":I
    .end local v10    # "nameIndex":I
    :cond_3
    :goto_1
    if-eqz v12, :cond_4

    .line 836
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_4
    move v0, v13

    .line 838
    goto :goto_0

    .line 831
    .restart local v9    # "mimeTypeIndex":I
    .restart local v10    # "nameIndex":I
    :catch_0
    move-exception v6

    .line 832
    .local v6, "e":Ljava/lang/IllegalStateException;
    sget-object v0, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    const-string v1, "isImageFile"

    invoke-static {v0, v1, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static isMediaDocument(Landroid/net/Uri;)Z
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 364
    const-string v0, "com.android.providers.media.documents"

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static isPanorama(II)Z
    .locals 3
    .param p0, "height"    # I
    .param p1, "width"    # I

    .prologue
    .line 273
    if-le p0, p1, :cond_0

    .line 274
    int-to-float v1, p0

    int-to-float v2, p1

    div-float v0, v1, v2

    .line 278
    .local v0, "aspectRatio":F
    :goto_0
    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 279
    const/4 v1, 0x1

    .line 280
    :goto_1
    return v1

    .line 276
    .end local v0    # "aspectRatio":F
    :cond_0
    int-to-float v1, p1

    int-to-float v2, p0

    div-float v0, v1, v2

    .restart local v0    # "aspectRatio":F
    goto :goto_0

    .line 280
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static isVideoUri(Landroid/net/Uri;)Z
    .locals 5
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 768
    const/4 v1, 0x0

    .line 770
    .local v1, "containVideo":Z
    const/4 v2, 0x0

    .line 771
    .local v2, "mime_type":Ljava/lang/String;
    :try_start_0
    const-string v3, "content"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "video"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 772
    const/4 v1, 0x1

    .line 782
    :cond_0
    :goto_0
    return v1

    .line 773
    :cond_1
    const-string v3, "file"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 774
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/net/URLConnection;->guessContentTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 775
    if-eqz v2, :cond_0

    const-string v3, "video"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 776
    const/4 v1, 0x1

    goto :goto_0

    .line 779
    :catch_0
    move-exception v0

    .line 780
    .local v0, "cme":Ljava/util/ConcurrentModificationException;
    sget-object v3, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    const-string v4, "isVideoUri"

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static final lookupImageInfo(Landroid/net/Uri;)Landroid/os/Bundle;
    .locals 18
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 396
    const/16 v16, -0x1

    .line 397
    .local v16, "orientation":I
    const/4 v12, 0x0

    .line 398
    .local v12, "displayName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 399
    .local v7, "_data":Ljava/lang/String;
    const/4 v14, 0x0

    .line 400
    .local v14, "image_mime_type":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v11

    .line 402
    .local v11, "context":Landroid/content/Context;
    const-string v1, "content"

    invoke-virtual/range {p0 .. p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    if-eqz v11, :cond_9

    .line 403
    invoke-static/range {p0 .. p0}, Lcom/samsung/android/app/memo/util/ImageUtil;->isMediaDocument(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 404
    invoke-static/range {p0 .. p0}, Lcom/samsung/android/app/memo/util/ImageUtil;->translateDocumentUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p0

    .line 407
    :cond_0
    const/4 v9, 0x0

    .line 410
    .local v9, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 411
    if-eqz v9, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 413
    const-string v1, "_data"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 414
    .local v10, "columnIdx":I
    const/4 v1, -0x1

    if-eq v10, v1, :cond_1

    .line 415
    invoke-interface {v9, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 418
    :cond_1
    const-string v1, "orientation"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 419
    const/4 v1, -0x1

    if-eq v10, v1, :cond_2

    .line 421
    :try_start_1
    invoke-interface {v9, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v16

    .line 427
    :cond_2
    :goto_0
    :try_start_2
    const-string v1, "_display_name"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 428
    const/4 v1, -0x1

    if-eq v10, v1, :cond_3

    .line 429
    invoke-interface {v9, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 431
    :cond_3
    const-string v1, "mime_type"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 432
    const/4 v1, -0x1

    if-eq v10, v1, :cond_4

    .line 433
    invoke-interface {v9, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v14

    .line 440
    .end local v10    # "columnIdx":I
    :cond_4
    if-eqz v9, :cond_5

    .line 441
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 447
    .end local v9    # "c":Landroid/database/Cursor;
    :cond_5
    :goto_1
    const/4 v1, -0x1

    move/from16 v0, v16

    if-ne v0, v1, :cond_6

    .line 448
    if-nez v7, :cond_a

    .line 449
    const/16 v16, 0x0

    .line 462
    :cond_6
    :goto_2
    if-nez v12, :cond_7

    .line 463
    invoke-virtual/range {p0 .. p0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v12

    .line 466
    :cond_7
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 467
    .local v8, "bundle":Landroid/os/Bundle;
    sget-object v1, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_ORIENTATION:Ljava/lang/String;

    move/from16 v0, v16

    invoke-virtual {v8, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 468
    sget-object v1, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_DISPLAYNAME:Ljava/lang/String;

    invoke-virtual {v8, v1, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    sget-object v1, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_MIMETYPE:Ljava/lang/String;

    invoke-virtual {v8, v1, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    return-object v8

    .line 422
    .end local v8    # "bundle":Landroid/os/Bundle;
    .restart local v9    # "c":Landroid/database/Cursor;
    .restart local v10    # "columnIdx":I
    :catch_0
    move-exception v15

    .line 423
    .local v15, "nfe":Ljava/lang/NumberFormatException;
    :try_start_3
    sget-object v1, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    const-string v2, "orientation error"

    invoke-static {v1, v2, v15}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 435
    .end local v10    # "columnIdx":I
    .end local v15    # "nfe":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v17

    .line 436
    .local v17, "sqle":Landroid/database/SQLException;
    :try_start_4
    sget-object v1, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "lookupImageInfo() uri: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 440
    if-eqz v9, :cond_5

    .line 441
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 437
    .end local v17    # "sqle":Landroid/database/SQLException;
    :catch_2
    move-exception v17

    .line 438
    .local v17, "sqle":Ljava/lang/SecurityException;
    :try_start_5
    sget-object v1, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "lookupImageInfo() uri: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 440
    if-eqz v9, :cond_5

    .line 441
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 439
    .end local v17    # "sqle":Ljava/lang/SecurityException;
    :catchall_0
    move-exception v1

    .line 440
    if-eqz v9, :cond_8

    .line 441
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 442
    :cond_8
    throw v1

    .line 443
    .end local v9    # "c":Landroid/database/Cursor;
    :cond_9
    const-string v1, "file"

    invoke-virtual/range {p0 .. p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 444
    invoke-virtual/range {p0 .. p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_1

    .line 452
    :cond_a
    :try_start_6
    invoke-static {v7}, Lcom/samsung/android/app/memo/util/ImageUtil;->setOrientation(Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    move-result v16

    goto/16 :goto_2

    .line 453
    :catch_3
    move-exception v13

    .line 456
    .local v13, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    const-string v2, "orientation-ExifInterface"

    invoke-static {v1, v2, v13}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 457
    const/16 v16, 0x0

    goto/16 :goto_2
.end method

.method private static setOrientation(Ljava/lang/String;)I
    .locals 3
    .param p0, "_data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 476
    new-instance v0, Landroid/media/ExifInterface;

    invoke-direct {v0, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 478
    .local v0, "exifIF":Landroid/media/ExifInterface;
    const-string v2, "Orientation"

    invoke-virtual {v0, v2}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 477
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 479
    .local v1, "orientation":I
    packed-switch v1, :pswitch_data_0

    .line 490
    :pswitch_0
    const/4 v1, 0x0

    .line 492
    :goto_0
    return v1

    .line 481
    :pswitch_1
    const/16 v1, 0x5a

    .line 482
    goto :goto_0

    .line 484
    :pswitch_2
    const/16 v1, 0xb4

    .line 485
    goto :goto_0

    .line 487
    :pswitch_3
    const/16 v1, 0x10e

    .line 488
    goto :goto_0

    .line 479
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static final translateDocumentUri(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 6
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 372
    move-object v0, p0

    .line 374
    .local v0, "contentUri":Landroid/net/Uri;
    invoke-static {p0}, Landroid/provider/DocumentsContract;->getDocumentId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 375
    .local v1, "docId":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v5, ":"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 376
    const-string v5, ":"

    invoke-virtual {v1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 377
    .local v3, "split":[Ljava/lang/String;
    const/4 v5, 0x0

    aget-object v4, v3, v5

    .line 378
    .local v4, "type":Ljava/lang/String;
    const/4 v5, 0x1

    aget-object v2, v3, v5

    .line 379
    .local v2, "id":Ljava/lang/String;
    const-string v5, "image"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 380
    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 386
    :cond_0
    :goto_0
    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 388
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "split":[Ljava/lang/String;
    .end local v4    # "type":Ljava/lang/String;
    :cond_1
    return-object v0

    .line 381
    .restart local v2    # "id":Ljava/lang/String;
    .restart local v3    # "split":[Ljava/lang/String;
    .restart local v4    # "type":Ljava/lang/String;
    :cond_2
    const-string v5, "video"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 382
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 383
    goto :goto_0

    :cond_3
    const-string v5, "audio"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 384
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0
.end method

.method public static updateExifInterface(Ljava/lang/String;I)V
    .locals 7
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "orientation"    # I

    .prologue
    .line 786
    invoke-static {p0}, Lcom/samsung/android/app/memo/util/FileHelper;->getPrivateFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 787
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 789
    .local v3, "path":Ljava/lang/String;
    :try_start_0
    new-instance v1, Landroid/media/ExifInterface;

    invoke-direct {v1, v3}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 790
    .local v1, "exifIF":Landroid/media/ExifInterface;
    const-string v4, "Orientation"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    sparse-switch p1, :sswitch_data_0

    .line 805
    const-string v4, "Orientation"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    :goto_0
    invoke-virtual {v1}, Landroid/media/ExifInterface;->saveAttributes()V

    .line 811
    .end local v1    # "exifIF":Landroid/media/ExifInterface;
    :goto_1
    return-void

    .line 793
    .restart local v1    # "exifIF":Landroid/media/ExifInterface;
    :sswitch_0
    const-string v4, "Orientation"

    .line 794
    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 793
    invoke-virtual {v1, v4, v5}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 808
    .end local v1    # "exifIF":Landroid/media/ExifInterface;
    :catch_0
    move-exception v0

    .line 809
    .local v0, "e":Ljava/io/IOException;
    sget-object v4, Lcom/samsung/android/app/memo/util/ImageUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "IOException :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 797
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "exifIF":Landroid/media/ExifInterface;
    :sswitch_1
    :try_start_1
    const-string v4, "Orientation"

    .line 798
    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 797
    invoke-virtual {v1, v4, v5}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 801
    :sswitch_2
    const-string v4, "Orientation"

    .line 802
    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    .line 801
    invoke-virtual {v1, v4, v5}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 791
    nop

    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch
.end method
