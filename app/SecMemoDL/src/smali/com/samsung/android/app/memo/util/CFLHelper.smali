.class public final Lcom/samsung/android/app/memo/util/CFLHelper;
.super Ljava/lang/Object;
.source "CFLHelper.java"


# static fields
.field private static final CFL_COL_APPID:Ljava/lang/String; = "app_id"

.field private static final CFL_COL_TYPE:Ljava/lang/String; = "type"

.field private static final CFL_COL_URI:Ljava/lang/String; = "uri"

.field private static final CFL_MIN_VER:I = 0x2

.field public static final CFL_OP_CREATE:I = 0x0

.field public static final CFL_OP_MODIFY:I = 0x1

.field private static final CFL_PKG_NAME:Ljava/lang/String; = "com.samsung.android.providers.context"

.field private static final CFL_WD_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsSkip:Z

.field private mStrPkgName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/samsung/android/app/memo/util/CFLHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/util/CFLHelper;->TAG:Ljava/lang/String;

    .line 37
    const-string v0, "content://com.samsung.android.providers.context.log.write_document"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/util/CFLHelper;->CFL_WD_URI:Landroid/net/Uri;

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/CFLHelper;->mContext:Landroid/content/Context;

    .line 51
    invoke-direct {p0}, Lcom/samsung/android/app/memo/util/CFLHelper;->getVersionOfContextProviders()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/app/memo/util/CFLHelper;->mIsSkip:Z

    .line 52
    sget-object v0, Lcom/samsung/android/app/memo/util/CFLHelper;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CFL mSkip:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/app/memo/util/CFLHelper;->mIsSkip:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/util/CFLHelper;->mIsSkip:Z

    if-eqz v0, :cond_1

    .line 59
    :goto_1
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/CFLHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/CFLHelper;->mStrPkgName:Ljava/lang/String;

    goto :goto_1
.end method

.method private getVersionOfContextProviders()I
    .locals 6

    .prologue
    .line 62
    const/4 v2, -0x1

    .line 64
    .local v2, "version":I
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/util/CFLHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.samsung.android.providers.context"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 65
    .local v0, "pInfo":Landroid/content/pm/PackageInfo;
    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    .end local v0    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 66
    :catch_0
    move-exception v1

    .line 67
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/samsung/android/app/memo/util/CFLHelper;->TAG:Ljava/lang/String;

    const-string v4, "getVersionOfContextProviders"

    invoke-static {v3, v4, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public created(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 90
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/memo/util/CFLHelper;->log(Ljava/lang/String;I)V

    .line 91
    return-void
.end method

.method public log(Ljava/lang/String;I)V
    .locals 6
    .param p1, "contentUri"    # Ljava/lang/String;
    .param p2, "operationType"    # I

    .prologue
    .line 73
    iget-boolean v3, p0, Lcom/samsung/android/app/memo/util/CFLHelper;->mIsSkip:Z

    if-eqz v3, :cond_0

    .line 87
    :goto_0
    return-void

    .line 76
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 77
    .local v0, "row":Landroid/content/ContentValues;
    const-string v3, "app_id"

    iget-object v4, p0, Lcom/samsung/android/app/memo/util/CFLHelper;->mStrPkgName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v3, "uri"

    invoke-virtual {v0, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v3, "type"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 82
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/util/CFLHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/app/memo/util/CFLHelper;->CFL_WD_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 83
    .local v2, "uri":Landroid/net/Uri;
    sget-object v3, Lcom/samsung/android/app/memo/util/CFLHelper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "loged as "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 84
    .end local v2    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 85
    .local v1, "sqle":Landroid/database/SQLException;
    sget-object v3, Lcom/samsung/android/app/memo/util/CFLHelper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "log() uri: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", operationType"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public modified(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 94
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/memo/util/CFLHelper;->log(Ljava/lang/String;I)V

    .line 95
    return-void
.end method
