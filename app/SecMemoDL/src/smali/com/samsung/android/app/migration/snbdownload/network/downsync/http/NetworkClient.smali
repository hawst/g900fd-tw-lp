.class public final Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;
.super Ljava/lang/Object;
.source "NetworkClient.java"

# interfaces
.implements Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientResponseListener;


# static fields
.field private static final APP_ID_HEADER:Ljava/lang/String; = "x-sc-appid"

.field private static final CONNECTION_TIMEOUT_DURATION:I = 0x7530

.field private static final DEFAULT_PORT:I = 0x50

.field private static final JSON_CONTENT:Ljava/lang/String;

.field private static final OCTET_STREAM:Ljava/lang/String;

.field private static final SECURE_PORT:I = 0x1bb

.field private static final SOCKET_BUFFER_SIZE:I = 0x80000

.field private static final USER_AGENT:Ljava/lang/String;


# instance fields
.field private mHttpClient:Lorg/apache/http/client/HttpClient;

.field private final mNetworkResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

.field private final mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->USER_AGENT:Ljava/lang/String;

    .line 83
    const-string v0, "application/octet-stream"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->OCTET_STREAM:Ljava/lang/String;

    .line 84
    const-string v0, "application/json;charset=utf-8"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->JSON_CONTENT:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mNetworkResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 101
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 148
    return-void
.end method

.method private execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    .locals 9
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 199
    const-string v6, "x-sc-appid"

    const-string v8, "kqq79c436g"

    invoke-interface {p1, v6, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 202
    .local v4, "requestID":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;

    invoke-direct {p0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v6

    invoke-direct {v2, p1, v6, p0, v4}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/HttpClient;Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientResponseListener;Ljava/lang/String;)V

    .line 203
    .local v2, "httpClientThread":Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;
    const/4 v3, 0x0

    .line 205
    .local v3, "httpResponse":Lorg/apache/http/HttpResponse;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    invoke-virtual {v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->start()V

    .line 207
    invoke-virtual {v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->getThreadLock()Ljava/lang/Object;

    move-result-object v5

    .line 208
    .local v5, "threadLock":Ljava/lang/Object;
    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :try_start_1
    invoke-virtual {v5}, Ljava/lang/Object;->wait()V

    .line 208
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    :try_start_2
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;

    move-object v2, v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 212
    if-nez v2, :cond_0

    move-object v6, v7

    .line 228
    .end local v5    # "threadLock":Ljava/lang/Object;
    :goto_0
    return-object v6

    .line 208
    .restart local v5    # "threadLock":Ljava/lang/Object;
    :catchall_0
    move-exception v6

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v6
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 217
    .end local v5    # "threadLock":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 218
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->close()V

    move-object v6, v7

    .line 219
    goto :goto_0

    .line 215
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v5    # "threadLock":Ljava/lang/Object;
    :cond_0
    :try_start_5
    invoke-virtual {v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->getHttpResponse()Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 216
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    .line 221
    if-nez v3, :cond_1

    .line 222
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Response is NULL."

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 224
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mNetworkResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    invoke-direct {p0, v3, v6}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->handleResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;)V

    .line 225
    invoke-virtual {v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->isClosed()Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v6, v7

    .line 226
    goto :goto_0

    .line 228
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mNetworkResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    goto :goto_0
.end method

.method private getHttpClient()Lorg/apache/http/client/HttpClient;
    .locals 8

    .prologue
    .line 122
    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    if-eqz v4, :cond_0

    .line 123
    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    .line 144
    :goto_0
    return-object v4

    .line 126
    :cond_0
    :try_start_0
    new-instance v2, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v2}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 127
    .local v2, "params":Lorg/apache/http/params/HttpParams;
    sget-object v4, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpProtocolParams;->setVersion(Lorg/apache/http/params/HttpParams;Lorg/apache/http/ProtocolVersion;)V

    .line 128
    const-string v4, "UTF-8"

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpProtocolParams;->setContentCharset(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 129
    sget-object v4, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->USER_AGENT:Ljava/lang/String;

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 130
    const/16 v4, 0x7530

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 131
    const/16 v4, 0x7530

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 132
    const/high16 v4, 0x80000

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 133
    const/4 v4, 0x0

    invoke-static {v2, v4}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    .line 134
    const/4 v4, 0x0

    invoke-static {v2, v4}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 135
    const-string v4, "http.protocol.expect-continue"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 136
    new-instance v3, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 137
    .local v3, "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v4, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v6

    const/16 v7, 0x50

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 138
    new-instance v4, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v6

    const/16 v7, 0x1bb

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 139
    new-instance v0, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v0, v2, v3}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 140
    .local v0, "ccm":Lorg/apache/http/conn/ClientConnectionManager;
    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4, v0, v2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    iput-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    .end local v0    # "ccm":Lorg/apache/http/conn/ClientConnectionManager;
    .end local v2    # "params":Lorg/apache/http/params/HttpParams;
    .end local v3    # "registry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    goto :goto_0

    .line 141
    :catch_0
    move-exception v1

    .line 142
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mHttpClient:Lorg/apache/http/client/HttpClient;

    goto :goto_1
.end method

.method private handleResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;)V
    .locals 8
    .param p1, "httpResponse"    # Lorg/apache/http/HttpResponse;
    .param p2, "networkResponse"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 170
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    .line 171
    .local v5, "status":I
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 173
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    invoke-virtual {p2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;->clear()V

    .line 174
    invoke-virtual {p2, v5}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;->setStatus(I)V

    .line 175
    const-string v6, "Content-Type"

    invoke-interface {p1, v6}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    .line 176
    .local v2, "header":Lorg/apache/http/Header;
    if-eqz v2, :cond_3

    .line 177
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 178
    .local v3, "headerString":Ljava/lang/String;
    sget-object v6, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->OCTET_STREAM:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 179
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v6

    invoke-virtual {p2, v6}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;->setFileInStream(Ljava/io/InputStream;)V

    .line 193
    :goto_0
    if-eqz v1, :cond_0

    const/16 v6, 0x194

    if-ne v5, v6, :cond_0

    .line 194
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 196
    :cond_0
    return-void

    .line 180
    :cond_1
    sget-object v6, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->JSON_CONTENT:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 182
    :try_start_0
    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v4

    .line 183
    .local v4, "response":Ljava/lang/String;
    invoke-virtual {p2, v4}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;->setBody(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 184
    .end local v4    # "response":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Converting HTTPEntity to String returns out of Memory"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 188
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :cond_2
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Incorrect Header"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 191
    .end local v3    # "headerString":Ljava/lang/String;
    :cond_3
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Header is Empty"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method private isUnsafe(C)Z
    .locals 2
    .param p1, "ch"    # C

    .prologue
    const/4 v0, 0x0

    .line 303
    const/16 v1, 0x30

    if-lt p1, v1, :cond_0

    const/16 v1, 0x39

    if-le p1, v1, :cond_2

    :cond_0
    const/16 v1, 0x41

    if-lt p1, v1, :cond_1

    const/16 v1, 0x5a

    if-le p1, v1, :cond_2

    :cond_1
    const/16 v1, 0x61

    if-lt p1, v1, :cond_3

    const/16 v1, 0x7a

    if-gt p1, v1, :cond_3

    .line 307
    :cond_2
    :goto_0
    return v0

    :cond_3
    const-string v1, " $+,;@[]"

    invoke-virtual {v1, p1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-ltz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private requestFile(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    .locals 9
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 241
    const-string v6, "x-sc-appid"

    const-string v8, "kqq79c436g"

    invoke-interface {p1, v6, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 243
    .local v4, "requestID":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;

    invoke-direct {p0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v6

    invoke-direct {v2, p1, v6, p0, v4}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/HttpClient;Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientResponseListener;Ljava/lang/String;)V

    .line 244
    .local v2, "httpClientThread":Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;
    const/4 v3, 0x0

    .line 246
    .local v3, "httpResponse":Lorg/apache/http/HttpResponse;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    invoke-virtual {v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->start()V

    .line 248
    invoke-virtual {v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->getThreadLock()Ljava/lang/Object;

    move-result-object v5

    .line 249
    .local v5, "threadLock":Ljava/lang/Object;
    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :try_start_1
    invoke-virtual {v5}, Ljava/lang/Object;->wait()V

    .line 249
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 252
    :try_start_2
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;

    move-object v2, v0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 253
    if-nez v2, :cond_0

    move-object v6, v7

    .line 269
    .end local v5    # "threadLock":Ljava/lang/Object;
    :goto_0
    return-object v6

    .line 249
    .restart local v5    # "threadLock":Ljava/lang/Object;
    :catchall_0
    move-exception v6

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v6
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 258
    .end local v5    # "threadLock":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 259
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->close()V

    move-object v6, v7

    .line 260
    goto :goto_0

    .line 256
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .restart local v5    # "threadLock":Ljava/lang/Object;
    :cond_0
    :try_start_5
    invoke-virtual {v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->getHttpResponse()Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 257
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    .line 262
    if-nez v3, :cond_1

    .line 263
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Response is NULL."

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 265
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mNetworkResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    invoke-direct {p0, v3, v6}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->handleResponse(Lorg/apache/http/HttpResponse;Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;)V

    .line 266
    invoke-virtual {v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->isClosed()Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v6, v7

    .line 267
    goto :goto_0

    .line 269
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mNetworkResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    goto :goto_0
.end method

.method private toHex(I)C
    .locals 1
    .param p1, "ch"    # I

    .prologue
    .line 312
    const/16 v0, 0xa

    if-ge p1, v0, :cond_0

    add-int/lit8 v0, p1, 0x30

    :goto_0
    int-to-char v0, v0

    return v0

    :cond_0
    add-int/lit8 v0, p1, 0x41

    add-int/lit8 v0, v0, -0xa

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 6

    .prologue
    .line 104
    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 105
    .local v0, "HttpClientThreadRequests":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 119
    return-void

    .line 105
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 106
    .local v2, "requestID":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;

    .line 107
    .local v1, "httpClientThread":Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;
    if-eqz v1, :cond_0

    .line 108
    invoke-virtual {v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->close()V

    .line 109
    invoke-virtual {v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->isAlive()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 110
    invoke-virtual {v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->interrupt()V

    .line 112
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    invoke-virtual {v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->getThreadLock()Ljava/lang/Object;

    move-result-object v3

    .line 114
    .local v3, "threadLock":Ljava/lang/Object;
    monitor-enter v3

    .line 115
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Object;->notify()V

    .line 114
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public encodeURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 289
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 290
    .local v1, "encodedUrl":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 299
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 290
    :cond_0
    aget-char v0, v3, v2

    .line 291
    .local v0, "ch":C
    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->isUnsafe(C)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 292
    const/16 v5, 0x25

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 293
    div-int/lit8 v5, v0, 0x10

    invoke-direct {p0, v5}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->toHex(I)C

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 294
    rem-int/lit8 v5, v0, 0x10

    invoke-direct {p0, v5}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->toHex(I)C

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 290
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 296
    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public get(Ljava/lang/String;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    .locals 2
    .param p1, "Url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 153
    .local v0, "get":Lorg/apache/http/client/methods/HttpGet;
    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    move-result-object v1

    return-object v1
.end method

.method public getFile(Ljava/lang/String;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    .locals 2
    .param p1, "Url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 235
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 236
    .local v0, "get":Lorg/apache/http/client/methods/HttpGet;
    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->requestFile(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    move-result-object v1

    return-object v1
.end method

.method public getWithHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    .locals 2
    .param p1, "Url"    # Ljava/lang/String;
    .param p2, "headerName"    # Ljava/lang/String;
    .param p3, "headerValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 275
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 276
    .local v0, "get":Lorg/apache/http/client/methods/HttpGet;
    invoke-virtual {v0, p2, p3}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    move-result-object v1

    return-object v1
.end method

.method public onResponse(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 90
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;

    .line 91
    .local v0, "httpClientThread":Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;
    iget v2, p1, Landroid/os/Message;->arg1:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 92
    iget v2, p1, Landroid/os/Message;->arg1:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    .line 93
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->mThreadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->getRequestID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/HttpClientThread;->getThreadLock()Ljava/lang/Object;

    move-result-object v1

    .line 96
    .local v1, "threadLock":Ljava/lang/Object;
    monitor-enter v1

    .line 97
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 96
    monitor-exit v1

    .line 99
    return-void

    .line 96
    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public post(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    .locals 5
    .param p1, "Url"    # Ljava/lang/String;
    .param p2, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 159
    .local v1, "post":Lorg/apache/http/client/methods/HttpPost;
    if-eqz p2, :cond_0

    .line 160
    new-instance v0, Lorg/apache/http/entity/StringEntity;

    const-string v2, "UTF-8"

    invoke-direct {v0, p2, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    .local v0, "entity":Lorg/apache/http/entity/StringEntity;
    new-instance v2, Lorg/apache/http/message/BasicHeader;

    const-string v3, "Content-Type"

    const-string v4, "Application/JSON;charset=UTF-8"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/http/entity/StringEntity;->setContentType(Lorg/apache/http/Header;)V

    .line 162
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 164
    .end local v0    # "entity":Lorg/apache/http/entity/StringEntity;
    :cond_0
    invoke-direct {p0, v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    move-result-object v2

    return-object v2
.end method

.method public postMultiPart(Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartEntity;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    .locals 2
    .param p1, "Url"    # Ljava/lang/String;
    .param p2, "requestEntity"    # Lorg/apache/http/entity/mime/MultipartEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 283
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->encodeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 284
    .local v0, "post":Lorg/apache/http/client/methods/HttpPost;
    invoke-virtual {v0, p2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 285
    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    move-result-object v1

    return-object v1
.end method
