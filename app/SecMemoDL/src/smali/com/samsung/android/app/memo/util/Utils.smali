.class public final Lcom/samsung/android/app/memo/util/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/util/Utils$MemoWidget;
    }
.end annotation


# static fields
.field private static final ERROR:I = -0x1

.field public static final HAVE_MOBILE_PRINT_APP:Z

.field public static final HAVE_SEC_CUSTOM_HOVER_API:Z

.field public static final HAVE_SLOOKBEZELINTERACTION_API:Z

.field public static final HAVE_SLOOKSMARTCLIP_API:Z

.field private static final HDPI_H_WIDTH:I = 0x21c

.field private static final HDPI_V_HEIGHT:I = 0x3c0

.field private static final HD_H_WIDTH:I = 0x2d0

.field private static final HD_V_WIDTH:I = 0x500

.field private static final HMULT:J = 0x6a5d39eae116586dL

.field private static final HSTART:J = -0x44bf19b25dfa4f9cL

.field public static final IS_MULTIWINDOW_DEVICE:Z

.field public static final IS_RUNNING_UNDER_KNOX:Z

.field private static final MDPI_H_WIDTH:I = 0x140

.field private static final MDPI_V_HEIGHT:I = 0x1e0

.field static final MIN_MEMORY_SIZE:J = 0x3200000L

.field public static final MTP_SCAN_ACTION:Ljava/lang/String; = "com.samsung.intent.action.MTP_FILE_SCAN"

.field public static NO_CATEGORY_POPUP:I

.field public static NO_MEMO_POPUP:I

.field private static final TAG:Ljava/lang/String;

.field private static final byteTable:[J

.field private static isFromDeleteMemo:Z

.field private static mTimeDifference:J

.field private static mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/16 v12, 0x100

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 93
    const-class v7, Lcom/samsung/android/app/memo/util/Utils;

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    sput-object v7, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    .line 129
    sput-boolean v9, Lcom/samsung/android/app/memo/util/Utils;->isFromDeleteMemo:Z

    .line 147
    sput v9, Lcom/samsung/android/app/memo/util/Utils;->NO_MEMO_POPUP:I

    .line 149
    sput v8, Lcom/samsung/android/app/memo/util/Utils;->NO_CATEGORY_POPUP:I

    .line 152
    const-wide/16 v10, -0x1

    sput-wide v10, Lcom/samsung/android/app/memo/util/Utils;->mTimeDifference:J

    .line 157
    const/4 v5, 0x0

    .line 159
    .local v5, "method":Ljava/lang/reflect/Method;
    :try_start_0
    const-string v7, "android.view.View"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const-string v10, "getHoverPopupWindow"

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v5

    .line 165
    :goto_0
    if-eqz v5, :cond_1

    move v7, v8

    :goto_1
    sput-boolean v7, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SEC_CUSTOM_HOVER_API:Z

    .line 169
    const/4 v5, 0x0

    .line 171
    .local v5, "method":[Ljava/lang/reflect/Method;
    :try_start_1
    const-string v7, "com.samsung.android.sdk.look.smartclip.SlookSmartClip"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v5

    .line 176
    :goto_2
    if-eqz v5, :cond_2

    move v7, v8

    :goto_3
    sput-boolean v7, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SLOOKSMARTCLIP_API:Z

    .line 180
    const/4 v5, 0x0

    .line 183
    :try_start_2
    const-string v7, "com.samsung.android.quickconnect.QuickConnectManager"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v5

    .line 189
    :goto_4
    if-eqz v5, :cond_3

    :goto_5
    sput-boolean v8, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SLOOKBEZELINTERACTION_API:Z

    .line 193
    const/4 v3, 0x0

    .line 195
    .local v3, "isMWSupported":Z
    :try_start_3
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 196
    .local v6, "packManager":Landroid/content/pm/PackageManager;
    if-eqz v6, :cond_0

    .line 197
    const-string v7, "com.sec.feature.multiwindow"

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 198
    const-string v7, "android.sec.multiwindow.MultiWindow"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    .line 199
    const/4 v3, 0x1

    .line 205
    .end local v6    # "packManager":Landroid/content/pm/PackageManager;
    :cond_0
    :goto_6
    sput-boolean v3, Lcom/samsung/android/app/memo/util/Utils;->IS_MULTIWINDOW_DEVICE:Z

    .line 209
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "sec_container_"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    sput-boolean v7, Lcom/samsung/android/app/memo/util/Utils;->IS_RUNNING_UNDER_KNOX:Z

    .line 213
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/app/memo/util/Utils;->isMobilePrintAppAvailable(Landroid/content/Context;)Z

    move-result v7

    sput-boolean v7, Lcom/samsung/android/app/memo/util/Utils;->HAVE_MOBILE_PRINT_APP:Z

    .line 632
    new-array v7, v12, [J

    sput-object v7, Lcom/samsung/android/app/memo/util/Utils;->byteTable:[J

    .line 633
    const-wide v0, 0x544b2fbacaaf1684L    # 1.1613978372692151E98

    .line 634
    .local v0, "h":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_7
    if-lt v2, v12, :cond_4

    .line 642
    return-void

    .end local v0    # "h":J
    .end local v2    # "i":I
    .end local v3    # "isMWSupported":Z
    .local v5, "method":Ljava/lang/reflect/Method;
    :cond_1
    move v7, v9

    .line 165
    goto :goto_1

    .local v5, "method":[Ljava/lang/reflect/Method;
    :cond_2
    move v7, v9

    .line 176
    goto :goto_3

    :cond_3
    move v8, v9

    .line 189
    goto :goto_5

    .line 635
    .restart local v0    # "h":J
    .restart local v2    # "i":I
    .restart local v3    # "isMWSupported":Z
    :cond_4
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_8
    const/16 v7, 0x1f

    if-lt v4, v7, :cond_5

    .line 640
    sget-object v7, Lcom/samsung/android/app/memo/util/Utils;->byteTable:[J

    aput-wide v0, v7, v2

    .line 634
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 636
    :cond_5
    const/4 v7, 0x7

    ushr-long v8, v0, v7

    xor-long/2addr v0, v8

    .line 637
    const/16 v7, 0xb

    shl-long v8, v0, v7

    xor-long/2addr v0, v8

    .line 638
    const/16 v7, 0xa

    ushr-long v8, v0, v7

    xor-long/2addr v0, v8

    .line 635
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 202
    .end local v0    # "h":J
    .end local v2    # "i":I
    .end local v4    # "j":I
    :catch_0
    move-exception v7

    goto :goto_6

    .line 185
    .end local v3    # "isMWSupported":Z
    :catch_1
    move-exception v7

    goto :goto_4

    .line 173
    :catch_2
    move-exception v7

    goto :goto_2

    .line 162
    .local v5, "method":Ljava/lang/reflect/Method;
    :catch_3
    move-exception v7

    goto/16 :goto_0
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450
    sget-object v0, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    return-void
.end method

.method public static IsChineseLanguage()Z
    .locals 4

    .prologue
    .line 848
    const/4 v2, 0x0

    .line 850
    .local v2, "result":Z
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v1, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 851
    .local v1, "locale":Ljava/util/Locale;
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 853
    .local v0, "language":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v3, "zh"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 854
    const/4 v2, 0x1

    .line 856
    :cond_0
    return v2
.end method

.method public static IsChineseModel()Z
    .locals 2

    .prologue
    .line 779
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 780
    .local v0, "sales_code":Ljava/lang/String;
    const-string v1, "CHN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 781
    const-string v1, "CHM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 782
    const-string v1, "CHU"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 783
    const-string v1, "CHC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 784
    const-string v1, "CTC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 785
    :cond_0
    const/4 v1, 0x1

    .line 787
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static IsJapanLanguage()Z
    .locals 4

    .prologue
    .line 860
    const/4 v2, 0x0

    .line 862
    .local v2, "result":Z
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v1, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 863
    .local v1, "locale":Ljava/util/Locale;
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 865
    .local v0, "language":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v3, "ja"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 866
    const/4 v2, 0x1

    .line 868
    :cond_0
    return v2
.end method

.method public static IsJapanModel()Z
    .locals 3

    .prologue
    .line 826
    const/4 v1, 0x0

    .line 827
    .local v1, "result":Z
    const-string v2, "ro.csc.country_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 829
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "JP"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 830
    const/4 v1, 0x1

    .line 832
    :cond_0
    return v1
.end method

.method public static IsKoreanLanguage()Z
    .locals 4

    .prologue
    .line 836
    const/4 v2, 0x0

    .line 838
    .local v2, "result":Z
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v1, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 839
    .local v1, "locale":Ljava/util/Locale;
    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 841
    .local v0, "language":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v3, "ko"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 842
    const/4 v2, 0x1

    .line 844
    :cond_0
    return v2
.end method

.method public static IsKoreanModel()Z
    .locals 3

    .prologue
    .line 816
    const/4 v1, 0x0

    .line 817
    .local v1, "result":Z
    const-string v2, "ro.csc.country_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 819
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "KOREA"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 820
    const/4 v1, 0x1

    .line 822
    :cond_0
    return v1
.end method

.method public static IsNoMemosPopupModel()Z
    .locals 1

    .prologue
    .line 812
    const/4 v0, 0x0

    return v0
.end method

.method public static IsUSAModel()Z
    .locals 2

    .prologue
    .line 792
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 793
    .local v0, "sales_code":Ljava/lang/String;
    const-string v1, "VZW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ATT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 794
    const-string v1, "SPR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "TMO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 795
    const-string v1, "TMB"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "USC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 796
    const-string v1, "BST"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "VMU"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 797
    const-string v1, "CRI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "BNN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 798
    const-string v1, "USA_WIFI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "AIO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 799
    const-string v1, "XAC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ZIG"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 800
    const-string v1, "MTR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "XAR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 801
    const-string v1, "XAS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SPI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 802
    const-string v1, "TFN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "N03"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 803
    const-string v1, "LRA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ACG"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 804
    const-string v1, "N02"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 805
    :cond_0
    const/4 v1, 0x1

    .line 807
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static SetIsFromDeleteMemo(Z)V
    .locals 0
    .param p0, "value"    # Z

    .prologue
    .line 1140
    sput-boolean p0, Lcom/samsung/android/app/memo/util/Utils;->isFromDeleteMemo:Z

    .line 1141
    return-void
.end method

.method private static addImageToNoMemosPopup(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 1071
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 1072
    .local v5, "sb":Ljava/lang/StringBuffer;
    const-string v6, "%s"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 1073
    .local v2, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 1074
    .local v1, "m":Ljava/util/regex/Matcher;
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1075
    .local v3, "res":Landroid/content/res/Resources;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    .line 1076
    .local v4, "result":Z
    :goto_0
    if-nez v4, :cond_0

    .line 1080
    invoke-virtual {v1, v5}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 1081
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1082
    .local v0, "finalString":Ljava/lang/String;
    return-object v0

    .line 1077
    .end local v0    # "finalString":Ljava/lang/String;
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "<img src=\"@drawable/"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v7, 0x7f020073

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\" align=\"middle\"/>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    .line 1078
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    goto :goto_0
.end method

.method public static checkLowMemory()Z
    .locals 4

    .prologue
    .line 1086
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAvailableExternalMemorySize()J

    move-result-wide v0

    .line 1087
    .local v0, "externalSpace":J
    const-wide/32 v2, 0x3200000

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 1088
    const/4 v2, 0x0

    .line 1090
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static containNotSupportedFormat(Ljava/util/ArrayList;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1122
    .local p0, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v0, 0x0

    .line 1123
    .local v0, "mNotSupported":Z
    const-string v1, ".*\\.(vts|vcs|VTS|VCS)"

    .line 1124
    .local v1, "re":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1132
    :goto_0
    return v0

    .line 1124
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 1125
    .local v2, "uri":Landroid/net/Uri;
    if-eqz v2, :cond_0

    const-string v4, "file"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1126
    invoke-virtual {v2}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1127
    const/4 v0, 0x1

    .line 1128
    goto :goto_0
.end method

.method public static externalMemoryAvailable()Z
    .locals 2

    .prologue
    .line 1109
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 1110
    const-string v1, "mounted"

    .line 1109
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static findCategoryUUIDofDuringCall(Landroid/content/Context;)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 660
    const-string v8, ""

    .line 661
    .local v8, "categoryUUID":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 662
    .local v0, "resolver":Landroid/content/ContentResolver;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 663
    .local v7, "categoryName":Ljava/lang/String;
    const/4 v6, 0x0

    .line 665
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "UUID"

    aput-object v4, v2, v3

    const-string v3, "_display_name IS ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 667
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 668
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 673
    :cond_0
    if-eqz v6, :cond_1

    .line 674
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 677
    :cond_1
    :goto_0
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 678
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object v8

    .line 680
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 681
    .local v9, "cv":Landroid/content/ContentValues;
    const-string v1, "UUID"

    invoke-virtual {v9, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    const-string v1, "_display_name"

    invoke-virtual {v9, v1, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    :try_start_1
    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1

    .line 692
    .end local v9    # "cv":Landroid/content/ContentValues;
    :cond_2
    :goto_1
    return-object v8

    .line 670
    :catch_0
    move-exception v10

    .line 671
    .local v10, "e":Landroid/database/SQLException;
    :try_start_2
    sget-object v1, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    const-string v2, "lookup category UUID for during call"

    invoke-static {v1, v2, v10}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 673
    if-eqz v6, :cond_1

    .line 674
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 672
    .end local v10    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v1

    .line 673
    if-eqz v6, :cond_3

    .line 674
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 675
    :cond_3
    throw v1

    .line 686
    .restart local v9    # "cv":Landroid/content/ContentValues;
    :catch_1
    move-exception v10

    .line 687
    .restart local v10    # "e":Landroid/database/SQLException;
    sget-object v1, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    const-string v2, "lookup category UUID for during call"

    invoke-static {v1, v2, v10}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 688
    const-string v8, ""

    goto :goto_1
.end method

.method public static getAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "relativePath"    # Ljava/lang/String;

    .prologue
    .line 1018
    move-object v0, p1

    .line 1019
    .local v0, "absolutePath":Ljava/lang/String;
    const-string v1, "com.samsung.android.app.memo"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1020
    const-string v1, "app_attach"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1021
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1022
    :cond_0
    return-object v0
.end method

.method public static getAppContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 443
    invoke-static {}, Lcom/samsung/android/app/memo/MemoApp;->getInstance()Lcom/samsung/android/app/memo/MemoApp;

    move-result-object v0

    return-object v0
.end method

.method public static getAvailableExternalMemorySize()J
    .locals 8

    .prologue
    .line 1095
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->externalMemoryAvailable()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1097
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    .line 1098
    .local v4, "path":Ljava/io/File;
    new-instance v5, Landroid/os/StatFs;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1099
    .local v5, "stat":Landroid/os/StatFs;
    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v2

    .line 1100
    .local v2, "blockSize":J
    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v0

    .line 1101
    .local v0, "availableBlocks":J
    mul-long v6, v0, v2

    .line 1104
    :goto_0
    return-wide v6

    .end local v0    # "availableBlocks":J
    .end local v2    # "blockSize":J
    .end local v4    # "path":Ljava/io/File;
    .end local v5    # "stat":Landroid/os/StatFs;
    :cond_0
    const-wide/16 v6, -0x1

    goto :goto_0
.end method

.method public static getCategoryCount(Landroid/content/Context;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 715
    const/4 v7, 0x0

    .line 716
    .local v7, "count":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 717
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 719
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 720
    if-eqz v6, :cond_0

    .line 721
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 725
    :cond_0
    if-eqz v6, :cond_1

    .line 726
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 728
    :cond_1
    :goto_0
    return v7

    .line 722
    :catch_0
    move-exception v8

    .line 723
    .local v8, "e":Landroid/database/SQLException;
    :try_start_1
    sget-object v1, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    const-string v2, "getCategoryCount "

    invoke-static {v1, v2, v8}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 725
    if-eqz v6, :cond_1

    .line 726
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 724
    .end local v8    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v1

    .line 725
    if-eqz v6, :cond_2

    .line 726
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 727
    :cond_2
    throw v1
.end method

.method public static getCategoryType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 732
    if-nez p0, :cond_1

    .line 742
    :cond_0
    :goto_0
    return v0

    .line 735
    :cond_1
    const-string v1, ""

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 736
    sget v1, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedPosition:I

    const/4 v2, -0x3

    if-ne v1, v2, :cond_0

    .line 737
    const/4 v0, 0x1

    goto :goto_0

    .line 742
    :cond_2
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static getCorrectedTime()J
    .locals 8

    .prologue
    .line 334
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 335
    .local v0, "correctedTime":J
    sget-wide v4, Lcom/samsung/android/app/memo/util/Utils;->mTimeDifference:J

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 337
    :try_start_0
    invoke-static {}, Lcom/samsung/android/app/memo/MemoApp;->getInstance()Lcom/samsung/android/app/memo/MemoApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/MemoApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "TIME_DIFFERENCE"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J

    move-result-wide v4

    sput-wide v4, Lcom/samsung/android/app/memo/util/Utils;->mTimeDifference:J
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    :cond_0
    :goto_0
    sget-wide v4, Lcom/samsung/android/app/memo/util/Utils;->mTimeDifference:J

    sub-long/2addr v0, v4

    .line 345
    return-wide v0

    .line 338
    :catch_0
    move-exception v2

    .line 340
    .local v2, "e":Landroid/provider/Settings$SettingNotFoundException;
    sget-object v3, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    const-string v4, "An exception was thrown while trying to access the TIME_DIFFERENCE"

    invoke-static {v3, v4, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 341
    const-wide/16 v4, 0x0

    sput-wide v4, Lcom/samsung/android/app/memo/util/Utils;->mTimeDifference:J

    goto :goto_0
.end method

.method public static getCurrentUTCTimeMillis()J
    .locals 2

    .prologue
    .line 326
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getDateFormat(J)Ljava/lang/String;
    .locals 2
    .param p0, "modifyTime"    # J

    .prologue
    .line 434
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDimension(I)F
    .locals 1
    .param p0, "id"    # I

    .prologue
    .line 313
    invoke-static {}, Lcom/samsung/android/app/memo/MemoApp;->getInstance()Lcom/samsung/android/app/memo/MemoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/MemoApp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    return v0
.end method

.method public static getInteger(I)I
    .locals 1
    .param p0, "id"    # I

    .prologue
    .line 317
    invoke-static {}, Lcom/samsung/android/app/memo/MemoApp;->getInstance()Lcom/samsung/android/app/memo/MemoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/MemoApp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method public static getIntentForUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 476
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 477
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.browser.application_id"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 478
    return-object v0
.end method

.method public static getQueryDateFormat(Landroid/content/Context;)Ljava/lang/String;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 396
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 397
    .local v6, "testDate":Ljava/util/Calendar;
    const/4 v8, 0x1

    const/16 v9, 0x7dd

    invoke-virtual {v6, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 398
    const/4 v8, 0x2

    const/16 v9, 0xb

    invoke-virtual {v6, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 399
    const/4 v8, 0x5

    const/16 v9, 0x19

    invoke-virtual {v6, v8, v9}, Ljava/util/Calendar;->set(II)V

    .line 401
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    .line 402
    .local v1, "format":Ljava/text/Format;
    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/text/Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 403
    .local v7, "testDateFormat":Ljava/lang/String;
    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 404
    .local v3, "parts":[Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 405
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/16 v8, 0x27

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 406
    array-length v9, v3

    const/4 v8, 0x0

    :goto_0
    if-lt v8, v9, :cond_0

    .line 427
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x1

    if-le v8, v9, :cond_4

    .line 428
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 430
    :goto_1
    return-object v8

    .line 406
    :cond_0
    aget-object v4, v3, v8

    .line 408
    .local v4, "s":Ljava/lang/String;
    const/4 v2, -0x1

    .line 411
    .local v2, "number":I
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 416
    :goto_2
    const/16 v10, 0x19

    if-ne v10, v2, :cond_1

    .line 417
    const-string v10, "%d/"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 419
    :cond_1
    const/16 v10, 0xc

    if-ne v10, v2, :cond_2

    .line 420
    const-string v10, "%m/"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    :cond_2
    const/16 v10, 0x7dd

    if-ne v10, v2, :cond_3

    .line 423
    const-string v10, "%Y/"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 412
    :catch_0
    move-exception v0

    .line 413
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v10, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Utils getQueryDateFormat s:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 430
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .end local v2    # "number":I
    .end local v4    # "s":Ljava/lang/String;
    :cond_4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_1
.end method

.method public static getRelativePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "absolutePath"    # Ljava/lang/String;

    .prologue
    .line 1009
    move-object v2, p0

    .line 1010
    .local v2, "relativePath":Ljava/lang/String;
    const-string v1, "memo"

    .line 1011
    .local v1, "memo":Ljava/lang/String;
    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1012
    .local v0, "i":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 1013
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1014
    :cond_0
    return-object v2
.end method

.method public static getScreenWidthAbs()I
    .locals 9

    .prologue
    .line 292
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 293
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 294
    .local v1, "displayMetrics":Landroid/util/DisplayMetrics;
    const-string v6, "window"

    invoke-virtual {v0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 297
    .local v5, "wm":Landroid/view/WindowManager;
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 299
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 300
    .local v3, "screenWidth":I
    iget v2, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 301
    .local v2, "screenHeight":I
    if-ge v3, v2, :cond_0

    move v4, v3

    .line 302
    .local v4, "width":I
    :goto_0
    sget-object v6, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "getScreenWidthAbs() ret="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    return v4

    .end local v4    # "width":I
    :cond_0
    move v4, v2

    .line 301
    goto :goto_0
.end method

.method public static getTimeFormat(JZ)Ljava/lang/String;
    .locals 6
    .param p0, "modifyTime"    # J
    .param p2, "mOnlyTime"    # Z

    .prologue
    .line 376
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 377
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v3

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 390
    :goto_0
    return-object v2

    .line 380
    :cond_0
    const-string v1, "h:mm aaa"

    .line 381
    .local v1, "timeFormat":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsKoreanModel()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsKoreanLanguage()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsChineseLanguage()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 382
    :cond_1
    const-string v1, "aaa h:mm"

    .line 383
    :cond_2
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsJapanLanguage()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 384
    const-string v1, "K:mm aaa"

    .line 386
    :cond_3
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 387
    .local v0, "simpleDateFormat":Ljava/text/SimpleDateFormat;
    if-eqz p2, :cond_4

    .line 388
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 390
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getTimeString(Landroid/content/Context;J)Ljava/lang/String;
    .locals 4
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "timeInMillis"    # J

    .prologue
    .line 570
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUniqueId()J
    .locals 20

    .prologue
    .line 576
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v12

    .line 579
    .local v12, "ctx":Landroid/content/Context;
    const-string v17, "phone"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/telephony/TelephonyManager;

    .line 580
    .local v16, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual/range {v16 .. v16}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    .line 581
    .local v3, "Imei":Ljava/lang/String;
    if-eqz v3, :cond_0

    const-string v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_0

    const-string v17, "0"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_0

    .line 582
    invoke-static {v3}, Lcom/samsung/android/app/memo/util/Utils;->hashCode(Ljava/lang/String;)J

    move-result-wide v6

    .line 583
    .local v6, "ImeiNum":J
    const-wide/16 v18, 0x0

    cmp-long v17, v6, v18

    if-eqz v17, :cond_0

    .line 622
    .end local v6    # "ImeiNum":J
    :goto_0
    return-wide v6

    .line 588
    :cond_0
    :try_start_0
    const-string v2, ""

    .line 589
    .local v2, "BTmac":Ljava/lang/String;
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v9

    .line 590
    .local v9, "bluetoothDefaultAdapter":Landroid/bluetooth/BluetoothAdapter;
    if-eqz v9, :cond_1

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v17

    if-eqz v17, :cond_1

    .line 591
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v2

    .line 593
    :cond_1
    if-eqz v2, :cond_2

    const-string v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2

    .line 595
    const-string v17, ":"

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 596
    const-string v17, "-"

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 597
    const/16 v17, 0x10

    move/from16 v0, v17

    invoke-static {v2, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v4

    .line 599
    .local v4, "BtLongVal":J
    const-wide/16 v18, 0x0

    cmp-long v17, v4, v18

    if-lez v17, :cond_2

    .line 600
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/android/app/memo/util/Utils;->hashCode(Ljava/lang/String;)J

    move-result-wide v6

    goto :goto_0

    .line 604
    .end local v4    # "BtLongVal":J
    :cond_2
    const-string v17, "wifi"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/net/wifi/WifiManager;

    .line 605
    .local v15, "manager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v15}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v14

    .line 606
    .local v14, "info":Landroid/net/wifi/WifiInfo;
    if-eqz v14, :cond_3

    .line 607
    invoke-virtual {v14}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v8

    .line 609
    .local v8, "WifiAddress":Ljava/lang/String;
    if-eqz v8, :cond_3

    const-string v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_3

    .line 610
    const-string v17, ":"

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 611
    const-string v17, "-"

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 612
    const/16 v17, 0x10

    move/from16 v0, v17

    invoke-static {v8, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v10

    .line 614
    .local v10, "WifiDecimalVal":J
    const-wide/16 v18, 0x0

    cmp-long v17, v10, v18

    if-lez v17, :cond_3

    .line 615
    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/android/app/memo/util/Utils;->hashCode(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    goto/16 :goto_0

    .line 618
    .end local v2    # "BTmac":Ljava/lang/String;
    .end local v8    # "WifiAddress":Ljava/lang/String;
    .end local v9    # "bluetoothDefaultAdapter":Landroid/bluetooth/BluetoothAdapter;
    .end local v10    # "WifiDecimalVal":J
    .end local v14    # "info":Landroid/net/wifi/WifiInfo;
    .end local v15    # "manager":Landroid/net/wifi/WifiManager;
    :catch_0
    move-exception v13

    .line 619
    .local v13, "e":Ljava/lang/NumberFormatException;
    sget-object v17, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    const-string v18, "getUniqueId() :"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v13}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 622
    .end local v13    # "e":Ljava/lang/NumberFormatException;
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    goto/16 :goto_0
.end method

.method private static hashCode(Ljava/lang/String;)J
    .locals 14
    .param p0, "cs"    # Ljava/lang/String;

    .prologue
    const-wide v12, 0x6a5d39eae116586dL    # 2.2908150473787247E204

    .line 645
    if-nez p0, :cond_1

    .line 646
    const-wide/16 v2, 0x1

    .line 655
    :cond_0
    return-wide v2

    .line 647
    :cond_1
    const-wide v2, -0x44bf19b25dfa4f9cL

    .line 648
    .local v2, "h":J
    const-wide v4, 0x6a5d39eae116586dL    # 2.2908150473787247E204

    .line 649
    .local v4, "hmult":J
    sget-object v1, Lcom/samsung/android/app/memo/util/Utils;->byteTable:[J

    .line 650
    .local v1, "ht":[J
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v6, v7, -0x1

    .local v6, "i":I
    :goto_0
    if-ltz v6, :cond_0

    .line 651
    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 652
    .local v0, "ch":C
    mul-long v8, v2, v12

    and-int/lit16 v7, v0, 0xff

    aget-wide v10, v1, v7

    xor-long v2, v8, v10

    .line 653
    mul-long v8, v2, v12

    ushr-int/lit8 v7, v0, 0x8

    and-int/lit16 v7, v7, 0xff

    aget-wide v10, v1, v7

    xor-long v2, v8, v10

    .line 650
    add-int/lit8 v6, v6, -0x1

    goto :goto_0
.end method

.method public static isAccessoryKeyboardState(Landroid/view/inputmethod/InputMethodManager;)I
    .locals 6
    .param p0, "imm"    # Landroid/view/inputmethod/InputMethodManager;

    .prologue
    .line 983
    const/4 v2, 0x0

    .line 984
    .local v2, "result":I
    const/4 v1, 0x0

    .line 986
    .local v1, "isAccessoryKeyboardState":Ljava/lang/reflect/Method;
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "isAccessoryKeyboardState"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 987
    if-eqz v1, :cond_0

    .line 988
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    move-result v2

    .line 999
    :cond_0
    :goto_0
    return v2

    .line 990
    :catch_0
    move-exception v0

    .line 991
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    sget-object v3, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    const-string v4, "Fail to invoke isAccessoryKeyboardState"

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 992
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 993
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    const-string v4, "Fail to invoke isAccessoryKeyboardState"

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 994
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 995
    .local v0, "e":Ljava/lang/IllegalAccessException;
    sget-object v3, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    const-string v4, "Fail to invoke isAccessoryKeyboardState"

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 996
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 997
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-object v3, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    const-string v4, "Fail to invoke isAccessoryKeyboardState"

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isCategoryExist(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 696
    const/4 v8, 0x0

    .line 697
    .local v8, "res":Z
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 698
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 700
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "UUID"

    aput-object v4, v2, v3

    .line 701
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "_display_name"

    :goto_0
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " IS ?"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 702
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v9, ""

    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .end local p2    # "name":Ljava/lang/String;
    :goto_1
    aput-object p2, v4, v5

    const/4 v5, 0x0

    .line 700
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 703
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 704
    const/4 v8, 0x1

    .line 708
    :cond_0
    if-eqz v6, :cond_1

    .line 709
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 711
    :cond_1
    :goto_2
    return v8

    .line 701
    .restart local p2    # "name":Ljava/lang/String;
    :cond_2
    :try_start_1
    const-string v3, "UUID"
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_3
    move-object p2, p1

    .line 702
    goto :goto_1

    .line 705
    .end local p2    # "name":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 706
    .local v7, "e":Landroid/database/SQLException;
    :try_start_2
    sget-object v1, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    const-string v2, "isCategoryExist "

    invoke-static {v1, v2, v7}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 708
    if-eqz v6, :cond_1

    .line 709
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 707
    .end local v7    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v1

    .line 708
    if-eqz v6, :cond_4

    .line 709
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 710
    :cond_4
    throw v1
.end method

.method public static isChinaCMCC()Z
    .locals 2

    .prologue
    .line 883
    const-string v0, "CHM"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 884
    const/4 v0, 0x1

    .line 886
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1145
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v0

    .line 1147
    .local v0, "enable":I
    const/4 v3, 0x2

    if-eq v3, v0, :cond_0

    .line 1148
    const/4 v3, 0x3

    if-ne v3, v0, :cond_1

    .line 1149
    :cond_0
    sget-object v3, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " is disabled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1157
    .end local v0    # "enable":I
    :goto_0
    return v2

    .line 1152
    .restart local v0    # "enable":I
    :cond_1
    sget-object v3, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " is enabled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1153
    const/4 v2, 0x1

    goto :goto_0

    .line 1155
    .end local v0    # "enable":I
    :catch_0
    move-exception v1

    .line 1156
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " is not installed"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isFonbletHD()Z
    .locals 3

    .prologue
    .line 924
    const/4 v0, 0x0

    .line 926
    .local v0, "res":Z
    const-string v1, "ro.build.scafe"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "capuccino"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 927
    const-string v1, "ro.build.scafe.size"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "short"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 928
    const-string v1, "ro.build.scafe.shot"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "double"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 929
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getScreenWidthAbs()I

    move-result v1

    const/16 v2, 0x2d0

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getScreenWidthAbs()I

    move-result v1

    const/16 v2, 0x500

    if-ne v1, v2, :cond_1

    .line 930
    :cond_0
    const/4 v0, 0x1

    .line 934
    :cond_1
    return v0
.end method

.method public static isFromDeleteMemo()Z
    .locals 1

    .prologue
    .line 1136
    sget-boolean v0, Lcom/samsung/android/app/memo/util/Utils;->isFromDeleteMemo:Z

    return v0
.end method

.method public static isGuestMode()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 768
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 771
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v1

    const/16 v2, 0x64

    if-ge v1, v2, :cond_0

    const/16 v1, 0xa

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v2

    if-gt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isHdpiDevice()Z
    .locals 7

    .prologue
    .line 952
    const/4 v2, 0x0

    .line 953
    .local v2, "res":Z
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 954
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 955
    .local v1, "displayMetrics":Landroid/util/DisplayMetrics;
    const-string v6, "window"

    invoke-virtual {v0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 956
    .local v5, "wm":Landroid/view/WindowManager;
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 957
    iget v4, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 958
    .local v4, "screenWidth":I
    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 959
    .local v3, "screenHeight":I
    const/16 v6, 0x21c

    if-ne v4, v6, :cond_0

    const/16 v6, 0x3c0

    if-ne v3, v6, :cond_0

    .line 960
    const/4 v2, 0x1

    .line 962
    :cond_0
    return v2
.end method

.method public static isIndianChar(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 1114
    const/16 v0, 0x900

    if-lt p0, v0, :cond_0

    const/16 v0, 0xdff

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKhmerChar(C)Z
    .locals 1
    .param p0, "c"    # C

    .prologue
    .line 1118
    const/16 v0, 0x1780

    if-lt p0, v0, :cond_0

    const/16 v0, 0x17f9

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKnoxMode()Z
    .locals 2

    .prologue
    .line 764
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isMassProduct()Z
    .locals 1

    .prologue
    .line 918
    const/4 v0, 0x0

    return v0
.end method

.method public static isMdpiDevice()Z
    .locals 7

    .prologue
    .line 938
    const/4 v2, 0x0

    .line 939
    .local v2, "res":Z
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 940
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 941
    .local v1, "displayMetrics":Landroid/util/DisplayMetrics;
    const-string v6, "window"

    invoke-virtual {v0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    .line 942
    .local v5, "wm":Landroid/view/WindowManager;
    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 943
    iget v4, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 944
    .local v4, "screenWidth":I
    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 945
    .local v3, "screenHeight":I
    const/16 v6, 0x140

    if-ne v4, v6, :cond_0

    const/16 v6, 0x1e0

    if-ne v3, v6, :cond_0

    .line 946
    const/4 v2, 0x1

    .line 948
    :cond_0
    return v2
.end method

.method public static isMemoSyncAvailable()Z
    .locals 4

    .prologue
    .line 907
    const/4 v0, 0x0

    .line 909
    .local v0, "res":Z
    :try_start_0
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.samsung.android.scloud.proxy.memo"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 910
    const/4 v0, 0x1

    .line 913
    :goto_0
    return v0

    .line 911
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static isMobilePrintAppAvailable(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1163
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.app.mobileprint"

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1168
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 1164
    :catch_0
    move-exception v0

    .line 1165
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v1, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    const-string v2, "isMobilePrintAppAvailable"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1166
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isMondrianOrPicassoInProduction()Z
    .locals 3

    .prologue
    .line 895
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 896
    .local v0, "model":Ljava/lang/String;
    const/4 v1, 0x0

    .line 897
    .local v1, "res":Z
    const-string v2, "T320"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 898
    const-string v2, "T321"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 899
    const-string v2, "T325"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 900
    const-string v2, "T520"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 901
    const-string v2, "T525"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 902
    :cond_0
    const/4 v1, 0x1

    .line 903
    :cond_1
    return v1
.end method

.method public static isNoOutgoingCall()Z
    .locals 3

    .prologue
    .line 1039
    const/4 v0, 0x0

    .line 1040
    .local v0, "mTm":Landroid/telephony/TelephonyManager;
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "mTm":Landroid/telephony/TelephonyManager;
    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1042
    .restart local v0    # "mTm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v1

    if-nez v1, :cond_0

    .line 1043
    const/4 v1, 0x1

    .line 1042
    :goto_0
    return v1

    .line 1043
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isPackageExists(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "targetPackage"    # Ljava/lang/String;

    .prologue
    .line 547
    if-eqz p0, :cond_0

    .line 548
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 550
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const/16 v1, 0x80

    :try_start_0
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 551
    const/4 v1, 0x1

    .line 555
    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return v1

    .line 552
    .restart local v0    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v1

    .line 555
    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSamsungAppsDownloadable()Z
    .locals 1

    .prologue
    .line 1048
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsKoreanModel()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsChineseModel()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isTModel()Z
    .locals 3

    .prologue
    .line 966
    const/4 v1, 0x0

    .line 974
    .local v1, "res":Z
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 975
    .local v0, "model":Ljava/lang/String;
    const-string v2, "N91"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 976
    const/4 v1, 0x1

    .line 979
    :goto_0
    return v1

    .line 978
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isTablet()Z
    .locals 2

    .prologue
    .line 891
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f070000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public static isTodayMemo(Ljava/lang/String;)Z
    .locals 4
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    .line 367
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getCurrentUTCTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Utils;->getDateFormat(J)Ljava/lang/String;

    move-result-object v0

    .line 369
    .local v0, "today":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 370
    const/4 v1, 0x1

    .line 372
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isUPSM()I
    .locals 5

    .prologue
    .line 1026
    const/4 v1, 0x0

    .line 1029
    .local v1, "setUPSM":I
    :try_start_0
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "ultra_powersaving_mode"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1034
    :goto_0
    return v1

    .line 1030
    :catch_0
    move-exception v0

    .line 1031
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    const-string v3, "IllegalArgumentException UPSM"

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isVoiceCallAvailabe(Landroid/content/Context;)Z
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 559
    .line 560
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 559
    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 561
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v2

    if-nez v2, :cond_1

    .line 567
    :cond_0
    :goto_0
    return v1

    .line 564
    :cond_1
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isWIFImodel()Z

    move-result v2

    if-nez v2, :cond_0

    .line 567
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isWIFImodel()Z
    .locals 4

    .prologue
    .line 872
    const/4 v1, 0x0

    .line 874
    .local v1, "result":Z
    const-string v2, "ro.product.name"

    const-string v3, "Unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 875
    .local v0, "product_name":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "wifi"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 876
    const/4 v1, 0x1

    .line 878
    :cond_0
    return v1
.end method

.method public static makePopUpWindow(Landroid/content/Context;Z)V
    .locals 9
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "isMemoContentActivity"    # Z

    .prologue
    const/4 v8, 0x5

    const/16 v7, 0x35

    const/4 v6, 0x2

    .line 247
    move-object v0, p0

    check-cast v0, Landroid/app/Activity;

    .line 248
    .local v0, "callingActivity":Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 284
    :goto_0
    return-void

    .line 250
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v4

    .line 251
    .local v4, "window":Landroid/view/Window;
    invoke-virtual {v4, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 252
    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 253
    .local v2, "params":Landroid/view/WindowManager$LayoutParams;
    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 254
    invoke-virtual {v4, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 255
    const/4 v3, 0x0

    .line 256
    .local v3, "width":I
    const/4 v1, 0x0

    .line 257
    .local v1, "height":I
    if-eqz p1, :cond_2

    .line 258
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    if-ne v5, v6, :cond_1

    .line 259
    iput v7, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 260
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0011

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 261
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0012

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 281
    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 282
    invoke-virtual {v4, v3, v1}, Landroid/view/Window;->setLayout(II)V

    .line 283
    const v5, 0x7f050001

    const/4 v6, -0x1

    invoke-virtual {v0, v5, v6}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto :goto_0

    .line 264
    :cond_1
    iput v7, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 265
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0014

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 266
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0015

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 268
    goto :goto_1

    .line 270
    :cond_2
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    if-ne v5, v6, :cond_3

    .line 271
    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 272
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0013

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 273
    const/4 v1, -0x1

    .line 274
    goto :goto_1

    .line 276
    :cond_3
    iput v7, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 277
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0016

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 278
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0017

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    goto :goto_1
.end method

.method public static performLinksContextMenuAction(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 10
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "isAllDay"    # Z

    .prologue
    const v8, 0x7f0b00c1

    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 493
    const-string v6, "mailto:"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 494
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isUPSM()I

    move-result v6

    if-ne v6, v7, :cond_1

    .line 495
    invoke-static {p0, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 544
    :cond_0
    :goto_0
    return-void

    .line 498
    :cond_1
    invoke-static {p0, p1}, Lcom/samsung/android/app/memo/util/Utils;->getIntentForUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 499
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0, v0}, Lcom/samsung/android/app/memo/util/Utils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    goto :goto_0

    .line 500
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    const-string v6, "tel:"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 501
    const-string v6, "com.android.contacts"

    invoke-static {p0, v6}, Lcom/samsung/android/app/memo/util/Utils;->isPackageExists(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 502
    invoke-static {p0}, Lcom/samsung/android/app/memo/util/Utils;->isVoiceCallAvailabe(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 503
    new-instance v0, Landroid/content/Intent;

    const-string v6, "android.intent.action.DIAL"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v0, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 505
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-static {p0, v0}, Lcom/samsung/android/app/memo/util/Utils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    goto :goto_0

    .line 507
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    const-string v6, "time:"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 508
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isUPSM()I

    move-result v6

    if-ne v6, v7, :cond_4

    .line 509
    invoke-static {p0, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 512
    :cond_4
    const-string v6, "time:"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 513
    .local v2, "mTime":Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->toDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 514
    .local v4, "time":J
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 515
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v6, "android.intent.action.EDIT"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 516
    const-string v6, "vnd.android.cursor.item/event"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 517
    const-string v6, "beginTime"

    invoke-virtual {v0, v6, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 518
    const-string v6, "allDay"

    invoke-virtual {v0, v6, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 519
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 521
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "mTime":Ljava/lang/String;
    .end local v4    # "time":J
    :cond_5
    const-string v6, "geo:"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 522
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isUPSM()I

    move-result v6

    if-eq v6, v7, :cond_6

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsUSAModel()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isKnoxMode()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 523
    :cond_6
    invoke-static {p0, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 526
    :cond_7
    const/16 v6, 0x20

    const/16 v7, 0x2b

    invoke-virtual {p2, v6, v7}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p2

    .line 527
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "geo:0,0?q="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 528
    .local v1, "linkText":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-direct {v0, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 532
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 533
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 535
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "linkText":Ljava/lang/String;
    :cond_8
    const-string v6, "http://"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_9

    const-string v6, "https://"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 536
    const-string v6, "rtsp://"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 537
    :cond_9
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 539
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 538
    invoke-virtual {v3, v6, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 539
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 540
    invoke-static {p0, p1}, Lcom/samsung/android/app/memo/util/Utils;->getIntentForUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 541
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-static {p0, v0}, Lcom/samsung/android/app/memo/util/Utils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    goto/16 :goto_0
.end method

.method public static refreshToast(Landroid/content/Context;)V
    .locals 1
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 775
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/memo/util/Utils;->mToast:Landroid/widget/Toast;

    .line 776
    return-void
.end method

.method public static removeBrokenEmojiChar(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1178
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1179
    const-string p0, ""

    .line 1193
    .end local p0    # "string":Ljava/lang/String;
    .local v0, "ch":C
    .local v1, "length":I
    :cond_0
    :goto_0
    return-object p0

    .line 1181
    .end local v0    # "ch":C
    .end local v1    # "length":I
    .restart local p0    # "string":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 1182
    .restart local v1    # "length":I
    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1183
    .restart local v0    # "ch":C
    invoke-static {v0}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1184
    add-int/lit8 v2, v1, -0x1

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 1186
    :cond_2
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1187
    const-string p0, ""

    goto :goto_0

    .line 1189
    :cond_3
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 1190
    invoke-static {v0}, Ljava/lang/Character;->isLowSurrogate(C)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1191
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static sendMediaScanEvent(Landroid/content/Context;)V
    .locals 2
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    .line 1172
    const-string v0, "KOREA"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1173
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.intent.action.MTP_FILE_SCAN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1175
    :cond_0
    return-void
.end method

.method public static sendUpdateAllWidgetBroadcastEvent(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1003
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.widget.memo.UPDATE_ALL_WIDGET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1004
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p0, :cond_0

    .line 1005
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1006
    :cond_0
    return-void
.end method

.method public static setDeltaTime(J)V
    .locals 0
    .param p0, "timeDifference"    # J

    .prologue
    .line 330
    sput-wide p0, Lcom/samsung/android/app/memo/util/Utils;->mTimeDifference:J

    .line 331
    return-void
.end method

.method public static setNoMemosPopupText(Landroid/view/View;I)V
    .locals 4
    .param p0, "v"    # Landroid/view/View;
    .param p1, "popup"    # I

    .prologue
    .line 1052
    new-instance v0, Lcom/samsung/android/app/memo/util/Utils$1;

    invoke-direct {v0}, Lcom/samsung/android/app/memo/util/Utils$1;-><init>()V

    .line 1063
    .local v0, "imgGetter":Landroid/text/Html$ImageGetter;
    sget v2, Lcom/samsung/android/app/memo/util/Utils;->NO_MEMO_POPUP:I

    if-ne p1, v2, :cond_1

    const v2, 0x7f0e0035

    :goto_0
    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1064
    .local v1, "noMemosPopup":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 1065
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v3

    sget v2, Lcom/samsung/android/app/memo/util/Utils;->NO_MEMO_POPUP:I

    if-ne p1, v2, :cond_2

    .line 1066
    const v2, 0x7f0b0016

    .line 1065
    :goto_1
    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/memo/util/Utils;->addImageToNoMemosPopup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1066
    const/4 v3, 0x0

    .line 1065
    invoke-static {v2, v0, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1067
    :cond_0
    return-void

    .line 1063
    .end local v1    # "noMemosPopup":Landroid/widget/TextView;
    :cond_1
    const v2, 0x7f0e000b

    goto :goto_0

    .line 1066
    .restart local v1    # "noMemosPopup":Landroid/widget/TextView;
    :cond_2
    const v2, 0x7f0b0015

    goto :goto_1
.end method

.method public static showLinksContextMenu(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "isAllDay"    # Z

    .prologue
    .line 483
    :try_start_0
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->newInstance(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/DialogFragment;

    move-result-object v1

    .line 484
    .local v1, "fg":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "link_action"

    invoke-virtual {v1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 488
    .end local v1    # "fg":Landroid/app/DialogFragment;
    :goto_0
    return-void

    .line 485
    :catch_0
    move-exception v0

    .line 486
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v2, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    const-string v3, "Utils"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static showToast(Landroid/content/Context;I)V
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    .line 747
    const/4 v0, 0x0

    .line 748
    .local v0, "text":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 749
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 750
    :cond_0
    if-nez v0, :cond_2

    .line 761
    :cond_1
    :goto_0
    return-void

    .line 753
    :cond_2
    sget-object v1, Lcom/samsung/android/app/memo/util/Utils;->mToast:Landroid/widget/Toast;

    if-nez v1, :cond_4

    .line 754
    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/util/Utils;->mToast:Landroid/widget/Toast;

    .line 759
    :cond_3
    :goto_1
    sget-object v1, Lcom/samsung/android/app/memo/util/Utils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-nez v1, :cond_1

    .line 760
    sget-object v1, Lcom/samsung/android/app/memo/util/Utils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 756
    :cond_4
    if-eqz p0, :cond_3

    .line 757
    sget-object v1, Lcom/samsung/android/app/memo/util/Utils;->mToast:Landroid/widget/Toast;

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public static slideInAnimation(Landroid/content/Context;)V
    .locals 8
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    const/4 v7, -0x1

    .line 218
    move-object v0, p0

    check-cast v0, Landroid/app/Activity;

    .line 219
    .local v0, "callingActivity":Landroid/app/Activity;
    if-nez v0, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v4

    .line 223
    .local v4, "window":Landroid/view/Window;
    if-eqz v4, :cond_0

    .line 225
    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 226
    .local v1, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a000e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 227
    .local v2, "slideInBottonHeight":I
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0010

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 229
    .local v3, "slideInLeftWidth":I
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    .line 230
    const/16 v5, 0x35

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 231
    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 232
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 234
    invoke-virtual {v4, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 235
    const v5, 0x7f050001

    invoke-virtual {v0, v5, v7}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto :goto_0

    .line 238
    :cond_2
    const/16 v5, 0x50

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 239
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 240
    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 241
    invoke-virtual {v4, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 242
    const/high16 v5, 0x7f050000

    invoke-virtual {v0, v5, v7}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method public static startActivity(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x1

    .line 455
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 472
    :goto_0
    return v2

    .line 456
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v3, Lcom/samsung/android/app/memo/util/Utils;->TAG:Ljava/lang/String;

    const-string v4, "Utils"

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 458
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.DIAL"

    if-ne v3, v4, :cond_0

    .line 459
    const-string v3, "android.intent.action.CALL"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 460
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 461
    invoke-static {p0, p1}, Lcom/samsung/android/app/memo/util/Utils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    goto :goto_0

    .line 464
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 465
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :catch_1
    move-exception v1

    .line 468
    .local v1, "ex":Ljava/lang/SecurityException;
    const-string v3, "android.intent.action.CALL"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 469
    invoke-static {p0, p1}, Lcom/samsung/android/app/memo/util/Utils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method public static toCurrentDTFull(J)Ljava/lang/String;
    .locals 2
    .param p0, "utcTimeMillis"    # J

    .prologue
    .line 353
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0, p1}, Lcom/samsung/android/app/memo/util/Utils;->getDateFormat(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-static {p0, p1, v1}, Lcom/samsung/android/app/memo/util/Utils;->getTimeFormat(JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toCurrentTimeOrDate(J)Ljava/lang/String;
    .locals 4
    .param p0, "utcTimeMillis"    # J

    .prologue
    .line 357
    invoke-static {p0, p1}, Lcom/samsung/android/app/memo/util/Utils;->getDateFormat(J)Ljava/lang/String;

    move-result-object v0

    .line 358
    .local v0, "date":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-static {p0, p1, v2}, Lcom/samsung/android/app/memo/util/Utils;->getTimeFormat(JZ)Ljava/lang/String;

    move-result-object v1

    .line 360
    .local v1, "time":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/memo/util/Utils;->isTodayMemo(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 363
    .end local v1    # "time":Ljava/lang/String;
    :goto_0
    return-object v1

    .restart local v1    # "time":Ljava/lang/String;
    :cond_0
    move-object v1, v0

    goto :goto_0
.end method
