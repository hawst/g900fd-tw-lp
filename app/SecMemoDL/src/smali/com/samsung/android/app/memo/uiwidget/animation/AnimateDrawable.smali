.class public Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;
.super Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;
.source "AnimateDrawable.java"


# instance fields
.field private mAnimation:Landroid/view/animation/Animation;

.field private final mTransformation:Landroid/view/animation/Transformation;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "target"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 29
    new-instance v0, Landroid/view/animation/Transformation;

    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->mTransformation:Landroid/view/animation/Transformation;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;Landroid/view/animation/Animation;)V
    .locals 1
    .param p1, "target"    # Landroid/graphics/drawable/Drawable;
    .param p2, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/animation/ProxyDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 29
    new-instance v0, Landroid/view/animation/Transformation;

    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->mTransformation:Landroid/view/animation/Transformation;

    .line 37
    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    .line 38
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->getProxy()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 59
    .local v1, "dr":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_1

    .line 60
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    .line 61
    .local v2, "sc":I
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    .line 62
    .local v0, "anim":Landroid/view/animation/Animation;
    if-eqz v0, :cond_0

    .line 63
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->mTransformation:Landroid/view/animation/Transformation;

    invoke-virtual {v0, v4, v5, v3}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    .line 64
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->mTransformation:Landroid/view/animation/Transformation;

    invoke-virtual {v3}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 66
    :cond_0
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 67
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 69
    .end local v0    # "anim":Landroid/view/animation/Animation;
    .end local v2    # "sc":I
    :cond_1
    return-void
.end method

.method public getAnimation()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method public hasEnded()Z
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hasStarted()Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAnimation(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "anim"    # Landroid/view/animation/Animation;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;->mAnimation:Landroid/view/animation/Animation;

    .line 46
    return-void
.end method
