.class Lcom/samsung/android/app/memo/uiwidget/RichEditor$5;
.super Landroid/os/Handler;
.source "RichEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/RichEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$5;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 1244
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1247
    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isAddPermissionRunning:Z
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$18()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1257
    :cond_0
    :goto_0
    return-void

    .line 1250
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x79

    if-ne v0, v1, :cond_0

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->revokeList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$19()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1252
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$5;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->revokeList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$19()Ljava/util/ArrayList;

    move-result-object v1

    # invokes: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->revokePermission(Ljava/util/ArrayList;)V
    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$20(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/util/ArrayList;)V

    .line 1253
    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->revokeList:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$19()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1254
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$21(Ljava/util/ArrayList;)V

    goto :goto_0
.end method
