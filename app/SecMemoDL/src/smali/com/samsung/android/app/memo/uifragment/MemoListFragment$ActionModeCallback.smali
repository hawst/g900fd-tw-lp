.class final Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;
.super Ljava/lang/Object;
.source "MemoListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AbsListView$MultiChoiceModeListener;
.implements Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ActionModeCallback"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;
    }
.end annotation


# instance fields
.field private isActionmodeStarted:Z

.field public final mMessageHandler:Landroid/os/Handler;

.field private mMultiSelectActionBarView:Landroid/view/View;

.field public mSelectAllCheckBox:Landroid/widget/CheckBox;

.field public mSelectionMenu:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;

.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V
    .locals 1

    .prologue
    .line 2088
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2097
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->isActionmodeStarted:Z

    .line 2369
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMessageHandler:Landroid/os/Handler;

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)V
    .locals 0

    .prologue
    .line 2088
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)V
    .locals 0

    .prologue
    .line 2412
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->updateSelectionMenu()V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2091
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    .locals 1

    .prologue
    .line 2088
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    return-object v0
.end method

.method private printMemos(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2160
    .local p1, "selectedIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Integer;

    invoke-interface {p1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    .line 2161
    .local v0, "ids":[Ljava/lang/Integer;
    new-instance v1, Lcom/samsung/android/app/memo/print/AsyncPrintTask;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x1

    .line 2162
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x0

    .line 2161
    invoke-direct {v1, v2, v3, v4, v5}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;-><init>(Landroid/content/Context;ZILcom/samsung/android/app/memo/Session;)V

    .line 2163
    .local v1, "printTask":Lcom/samsung/android/app/memo/print/AsyncPrintTask;
    if-eqz v1, :cond_0

    .line 2164
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2166
    :cond_0
    return-void
.end method

.method private shareMemos(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "selectedIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v11, 0x0

    .line 2169
    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    int-to-long v2, v9

    .line 2170
    .local v2, "id":J
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getTitle(J)Ljava/lang/String;
    invoke-static {v9, v2, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$22(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Ljava/lang/String;

    move-result-object v6

    .line 2171
    .local v6, "titleForTextPlain":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getText(J)Ljava/lang/String;
    invoke-static {v9, v2, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$23(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Ljava/lang/String;

    move-result-object v5

    .line 2172
    .local v5, "stripcontent":Ljava/lang/String;
    move-object v0, v5

    .line 2173
    .local v0, "content":Ljava/lang/String;
    new-instance v1, Lcom/samsung/android/app/memo/share/MemoShare;

    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v9}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-direct {v1, v9}, Lcom/samsung/android/app/memo/share/MemoShare;-><init>(Landroid/content/Context;)V

    .line 2174
    .local v1, "m":Lcom/samsung/android/app/memo/share/MemoShare;
    invoke-virtual {v1, v0}, Lcom/samsung/android/app/memo/share/MemoShare;->setText(Ljava/lang/String;)V

    .line 2175
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getHtmlContent(J)Ljava/lang/String;
    invoke-static {v9, v2, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$24(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/samsung/android/app/memo/share/MemoShare;->setHtmlText(Ljava/lang/String;)V

    .line 2176
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getImageUriList(J)Ljava/util/ArrayList;
    invoke-static {v9, v2, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$25(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/samsung/android/app/memo/share/MemoShare;->setImageAttachmentList(Ljava/util/ArrayList;)V

    .line 2177
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getVoiceUri(J)Landroid/net/Uri;
    invoke-static {v9, v2, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$26(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)Landroid/net/Uri;

    move-result-object v7

    .line 2178
    .local v7, "voiceUri":Landroid/net/Uri;
    sget-object v9, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    .line 2179
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    .line 2178
    invoke-static {v9, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 2180
    .local v8, "voiceUriToSend":Landroid/net/Uri;
    if-eqz v7, :cond_0

    sget-object v9, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    invoke-virtual {v7, v9}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 2181
    invoke-virtual {v1, v8}, Lcom/samsung/android/app/memo/share/MemoShare;->setVoiceAttachment(Landroid/net/Uri;)V

    .line 2183
    :cond_0
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v9}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v10, 0x7f0b0009

    invoke-virtual {v9, v10}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/samsung/android/app/memo/share/MemoShare;->setTitle(Ljava/lang/String;)V

    .line 2184
    const-string v9, ""

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v6, 0x0

    .end local v6    # "titleForTextPlain":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1, v6}, Lcom/samsung/android/app/memo/share/MemoShare;->setSubject(Ljava/lang/String;)V

    .line 2185
    new-instance v4, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v9}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-direct {v4, v1, v9, v2, v3}, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;-><init>(Lcom/samsung/android/app/memo/share/MemoShare;Landroid/app/Activity;J)V

    .line 2186
    .local v4, "shareTask":Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;
    sget-object v9, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v10, v11, [Ljava/lang/Void;

    invoke-virtual {v4, v9, v10}, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2187
    return-void
.end method

.method private showCategoryMoveDialog()V
    .locals 4

    .prologue
    .line 2152
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2153
    .local v0, "categoryMoveIDs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->getSelectedMemoIds()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2155
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$19(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->newInstance(Ljava/util/ArrayList;Ljava/lang/String;)Landroid/app/DialogFragment;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    invoke-static {v2, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$20(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;)V

    .line 2156
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryMoveDialog:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$21(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 2157
    return-void
.end method

.method private updateSelectionMenu()V
    .locals 9

    .prologue
    const v8, 0x7f0b003a

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2414
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->getSelectedMemoIds()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 2416
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuShareVia:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$43(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2417
    if-eq v0, v3, :cond_4

    .line 2418
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuShareVia:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$43(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2423
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuPrint:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$44(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2424
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuPrint:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$44(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v5

    sget-boolean v2, Lcom/samsung/android/app/memo/util/Utils;->HAVE_MOBILE_PRINT_APP:Z

    if-eqz v2, :cond_0

    if-gtz v0, :cond_5

    .line 2425
    :cond_0
    if-eq v0, v3, :cond_5

    move v2, v4

    .line 2424
    :goto_1
    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2431
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuMove:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$45(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v2

    sget-object v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->NORMAL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v2, v5, :cond_2

    if-lez v0, :cond_2

    .line 2432
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuMove:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$45(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2435
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v2

    sget-object v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->DELETE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v2, v5, :cond_3

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuDeleteButton:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$46(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2436
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuDeleteButton:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$46(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v5

    if-lez v0, :cond_6

    move v2, v3

    :goto_2
    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2439
    :cond_3
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$1()Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "count"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2440
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2441
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "%d/"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2442
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v3, [Ljava/lang/Object;

    .line 2443
    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v7}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/AbsListView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/AbsListView;->getCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    .line 2442
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 2441
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2447
    .local v1, "selectedMemos":Ljava/lang/String;
    :goto_3
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->getCount()I

    move-result v5

    if-ne v0, v5, :cond_8

    :goto_4
    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2448
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mSelectionMenu:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 2450
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->getSelectedMemoIds()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$47(Ljava/util/ArrayList;)V

    .line 2451
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->notifyDataSetChanged()V

    .line 2452
    return-void

    .line 2420
    .end local v1    # "selectedMemos":Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuShareVia:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$43(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2421
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuShareVia:Landroid/view/MenuItem;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$43(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_5
    move v2, v3

    .line 2425
    goto/16 :goto_1

    :cond_6
    move v2, v4

    .line 2436
    goto/16 :goto_2

    .line 2445
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "selectedMemos":Ljava/lang/String;
    goto :goto_3

    :cond_8
    move v3, v4

    .line 2447
    goto :goto_4
.end method


# virtual methods
.method public getSelectedMemoIds()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2192
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->getSelectedIds()Landroid/util/SparseBooleanArray;

    move-result-object v1

    .line 2193
    .local v1, "selected":Landroid/util/SparseBooleanArray;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2195
    .local v2, "selectedMemoIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_0

    .line 2200
    return-object v2

    .line 2196
    :cond_0
    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 2197
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v3

    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->getItemId(I)J

    move-result-wide v4

    long-to-int v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2195
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 10
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    const-wide/16 v8, 0x190

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2101
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->isActionmodeStarted:Z

    .line 2102
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    move v3, v4

    .line 2147
    :cond_0
    :goto_0
    return v3

    .line 2104
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->showCategoryMoveDialog()V

    goto :goto_0

    .line 2107
    :pswitch_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 2108
    .local v0, "currentTouchTime":J
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mLastMemoOpenClickTime:J
    invoke-static {v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$15(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)J

    move-result-wide v4

    sub-long v4, v0, v4

    const-wide/16 v6, 0x1f4

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    .line 2109
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->getSelectedMemoIds()Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->printMemos(Ljava/util/List;)V

    .line 2111
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$16(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)V

    .line 2112
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$2;

    invoke-direct {v5, p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$2;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;Landroid/view/ActionMode;)V

    invoke-virtual {v4, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 2120
    .end local v0    # "currentTouchTime":J
    :pswitch_2
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v5

    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_MULTIPLE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v5, v6, :cond_2

    .line 2121
    new-instance v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/app/Activity;)V

    .line 2122
    .local v2, "shareOnSConnect":Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;
    sget-object v5, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v5, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 2124
    .end local v2    # "shareOnSConnect":Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsForCacheDelete:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$7(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 2125
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIDsForCacheDelete:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$7(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 2126
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->getSelectedMemoIds()Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$17(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Ljava/util/ArrayList;)V

    .line 2128
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->getSelectedMemoIds()Ljava/util/ArrayList;

    move-result-object v5

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->deletingMemos(Ljava/util/List;)V
    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$18(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Ljava/util/List;)V

    .line 2130
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    .line 2131
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/AbsListView;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2132
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/AbsListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/AbsListView;->requestFocus()Z

    goto/16 :goto_0

    .line 2137
    :pswitch_3
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->getSelectedMemoIds()Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->shareMemos(Ljava/util/List;)V

    .line 2138
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$3;

    invoke-direct {v5, p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$3;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;Landroid/view/ActionMode;)V

    invoke-virtual {v4, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 2102
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e009f
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 2457
    return-void
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 7
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const v2, 0x7f0d0004

    const/4 v6, 0x1

    .line 2205
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$1()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onCreateActionMode "

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2206
    sget-boolean v0, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SLOOKBEZELINTERACTION_API:Z

    if-eqz v0, :cond_0

    .line 2207
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->initSConnect(Landroid/app/Activity;)V
    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$27(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/app/Activity;)V

    .line 2208
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$28(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/quickconnect/QuickConnectManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$28(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/quickconnect/QuickConnectManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$29(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->registerListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 2211
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2212
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0c0002

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTheme(I)V

    .line 2214
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    iput-object p1, v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActionMode:Landroid/view/ActionMode;

    .line 2215
    sput-boolean v6, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsActionmode:Z

    .line 2216
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-static {v0, v6}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$30(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V

    .line 2217
    iput-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->isActionmodeStarted:Z

    .line 2218
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFabButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$31(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2219
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->hideUndoBar()V

    .line 2221
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isKnoxMode()Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/samsung/android/app/memo/util/Utils;->IS_RUNNING_UNDER_KNOX:Z

    if-nez v0, :cond_2

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isUPSM()I

    move-result v0

    if-ne v0, v6, :cond_4

    .line 2222
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->DELETE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v0, v1, :cond_3

    .line 2223
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    invoke-virtual {v0, v2, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2232
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    const v1, 0x7f0e00a0

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$32(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/view/MenuItem;)V

    .line 2233
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    const v1, 0x7f0e00a1

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$33(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/view/MenuItem;)V

    .line 2234
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    const v1, 0x7f0e009f

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$34(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/view/MenuItem;)V

    .line 2235
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    const v1, 0x7f0e00a2

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$35(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/view/MenuItem;)V

    .line 2237
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$36(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v0

    invoke-interface {v0, v6}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setDrawerLock(Z)V

    .line 2238
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-static {v0, p2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$37(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/view/Menu;)V

    .line 2240
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2241
    const v1, 0x7f040010

    const/4 v2, 0x0

    .line 2240
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    .line 2243
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    const v1, 0x7f0e0014

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    .line 2245
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 2246
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    const v3, 0x7f0e0013

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 2247
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    const v4, 0x7f0e0015

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;Landroid/content/Context;Landroid/widget/LinearLayout;Landroid/widget/TextView;Landroid/widget/CheckBox;)V

    .line 2245
    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mSelectionMenu:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;

    .line 2249
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->updateSelectionMenu()V

    .line 2251
    return v6

    .line 2225
    :cond_3
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0d0006

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto/16 :goto_0

    .line 2227
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->DELETE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_MULTIPLE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v0, v1, :cond_6

    .line 2228
    :cond_5
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    invoke-virtual {v0, v2, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto/16 :goto_0

    .line 2230
    :cond_6
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0d0007

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto/16 :goto_0
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 4
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    const/4 v3, 0x0

    .line 2256
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$1()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onDestroyActionMode "

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2257
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_MULTIPLE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v1, v2, :cond_0

    .line 2259
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 2261
    :cond_0
    sget-boolean v1, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SLOOKBEZELINTERACTION_API:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$28(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/quickconnect/QuickConnectManager;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2262
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$28(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/quickconnect/QuickConnectManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$29(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 2264
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$38(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/quickconnect/QuickConnectManager;)V

    .line 2266
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryMoveDialog:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$21(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isDestroyed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2268
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryMoveDialog:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$21(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2273
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryMoveDialog:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$21(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryMoveDialog:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$21(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getCategoryNamingDialog()Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2274
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->isDestroyed()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2276
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryMoveDialog:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$21(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getCategoryNamingDialog()Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2281
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-static {v1, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$30(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V

    .line 2282
    sput-boolean v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsActionmode:Z

    .line 2283
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->isActionmodeStarted:Z

    .line 2284
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFabButton:Landroid/widget/ImageButton;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$31(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/ImageButton;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2285
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTModel()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2286
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0c0001

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setTheme(I)V

    .line 2288
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->removeSelection()V

    .line 2289
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$36(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v1

    invoke-interface {v1, v3}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setDrawerLock(Z)V

    .line 2290
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTModel()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2291
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectAllLayout:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$39(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/LinearLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2296
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->NORMAL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$40(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;)V

    .line 2297
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$41(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$41(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2298
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSearch:Landroid/view/MenuItem;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$41(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->collapseActionView()Z

    .line 2300
    :cond_6
    return-void

    .line 2269
    :catch_0
    move-exception v0

    .line 2270
    .local v0, "e":Ljava/lang/IllegalStateException;
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$1()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onDestroyActionMode"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 2277
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 2278
    .restart local v0    # "e":Ljava/lang/IllegalStateException;
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$1()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onDestroyActionMode"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1
.end method

.method public onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V
    .locals 8
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "position"    # I
    .param p3, "arg2"    # J
    .param p5, "arg3"    # Z

    .prologue
    const/4 v7, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2321
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$1()Ljava/lang/String;

    move-result-object v1

    const-string v4, "onItemCheckedStateChanged "

    invoke-static {v1, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2322
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->isActionmodeStarted:Z

    if-eqz v1, :cond_4

    .line 2323
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/AbsListView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->performHapticFeedback(I)Z

    .line 2327
    :cond_0
    :goto_0
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->isActionmodeStarted:Z

    .line 2328
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->getSelectedMemoIds()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 2329
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v1

    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_MULTIPLE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v1, v4, :cond_5

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectLimit:I
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$11(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)I

    move-result v1

    if-lt v0, v1, :cond_5

    .line 2330
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->getSelectedIds()Landroid/util/SparseBooleanArray;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2331
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$12(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/app/Activity;

    move-result-object v1

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00bf

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v2, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectLimit:I
    invoke-static {v6}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$11(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 2332
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2336
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSelectionMode:Landroid/view/Menu;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$14(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/Menu;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2337
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v1

    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->DELETE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-eq v1, v4, :cond_1

    .line 2338
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2339
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSelectionMode:Landroid/view/Menu;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$14(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/Menu;

    move-result-object v4

    .line 2340
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->getSelectedIds()Landroid/util/SparseBooleanArray;

    move-result-object v1

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I

    move-result v5

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/AbsListView;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    .line 2341
    invoke-virtual {v1, v7}, Landroid/widget/GridView;->isItemChecked(I)Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v2

    :goto_2
    if-le v5, v1, :cond_7

    move v1, v2

    .line 2339
    :goto_3
    invoke-interface {v4, v3, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 2349
    :cond_1
    :goto_4
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSelectAll:Z
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$42(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2350
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->updateSelectionMenu()V

    .line 2352
    :cond_2
    sget-boolean v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsActionmode:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/AbsListView;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/AbsListView;->isItemChecked(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2353
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/AbsListView;

    move-result-object v1

    invoke-virtual {v1, v7, v2}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 2356
    :cond_3
    return-void

    .line 2324
    .end local v0    # "count":I
    :cond_4
    if-ltz p2, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mIsSelectAll:Z
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$42(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2325
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/AbsListView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/AbsListView;->playSoundEffect(I)V

    goto/16 :goto_0

    .line 2334
    .restart local v0    # "count":I
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->toggleSelection(I)V

    goto/16 :goto_1

    :cond_6
    move v1, v3

    .line 2341
    goto :goto_2

    :cond_7
    move v1, v3

    goto :goto_3

    .line 2343
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSelectionMode:Landroid/view/Menu;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$14(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/Menu;

    move-result-object v4

    .line 2344
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->getSelectedIds()Landroid/util/SparseBooleanArray;

    move-result-object v1

    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I

    move-result v5

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/AbsListView;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 2345
    invoke-virtual {v1, v7}, Landroid/widget/ListView;->isItemChecked(I)Z

    move-result v1

    if-eqz v1, :cond_9

    move v1, v2

    :goto_5
    if-le v5, v1, :cond_a

    move v1, v2

    .line 2343
    :goto_6
    invoke-interface {v4, v3, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_4

    :cond_9
    move v1, v3

    .line 2345
    goto :goto_5

    :cond_a
    move v1, v3

    goto :goto_6
.end method

.method public onPopupItemClick(I)Z
    .locals 4
    .param p1, "itemId"    # I

    .prologue
    const-wide/16 v2, 0x64

    .line 2360
    const v0, 0x7f0e00a3

    if-ne p1, v0, :cond_0

    .line 2361
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMessageHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2366
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 2363
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMessageHandler:Landroid/os/Handler;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 6
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 2304
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$1()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPrepareActionMode "

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2305
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2306
    const v1, 0x7f040010

    const/4 v2, 0x0

    .line 2305
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    .line 2308
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    const v1, 0x7f0e0014

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    .line 2309
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 2310
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    const v3, 0x7f0e0013

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 2311
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    const v4, 0x7f0e0015

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mSelectAllCheckBox:Landroid/widget/CheckBox;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;Landroid/content/Context;Landroid/widget/LinearLayout;Landroid/widget/TextView;Landroid/widget/CheckBox;)V

    .line 2309
    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mSelectionMenu:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$SelectAllActionBar;

    .line 2313
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->updateSelectionMenu()V

    .line 2314
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->mMultiSelectActionBarView:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    .line 2316
    const/4 v0, 0x1

    return v0
.end method
