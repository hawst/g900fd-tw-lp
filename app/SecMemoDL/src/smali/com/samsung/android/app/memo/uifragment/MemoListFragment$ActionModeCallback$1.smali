.class Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;
.super Landroid/os/Handler;
.source "MemoListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    .line 2369
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2372
    const/4 v1, 0x0

    .line 2373
    .local v1, "itemCount":I
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 2409
    :cond_0
    :goto_0
    return-void

    .line 2375
    :sswitch_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V

    .line 2376
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/AbsListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getCount()I

    move-result v1

    .line 2377
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v2

    sget-object v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_MULTIPLE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v2, v5, :cond_5

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectLimit:I
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$11(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)I

    move-result v2

    if-le v1, v2, :cond_5

    .line 2378
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$12(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/app/Activity;

    move-result-object v2

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b00bf

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v3, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v7}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v7

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectLimit:I
    invoke-static {v7}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$11(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 2379
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2380
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v1, :cond_3

    .line 2389
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V

    .line 2390
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v2

    sget-object v5, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->DELETE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-eq v2, v5, :cond_2

    .line 2391
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSelectionMode:Landroid/view/Menu;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$14(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/Menu;

    move-result-object v2

    invoke-interface {v2, v4, v3}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 2393
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->updateSelectionMenu()V
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$0(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)V

    goto/16 :goto_0

    .line 2381
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v5

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mSelectLimit:I
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$11(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)I

    move-result v2

    if-ge v0, v2, :cond_4

    move v2, v3

    :goto_2
    invoke-virtual {v5, v0, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->selectView(IZ)V

    .line 2380
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v2, v4

    .line 2381
    goto :goto_2

    .line 2384
    .end local v0    # "i":I
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    if-ge v0, v1, :cond_1

    .line 2386
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v2

    invoke-virtual {v2, v0, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->selectView(IZ)V

    .line 2384
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2396
    .end local v0    # "i":I
    :sswitch_1
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V

    .line 2397
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAbsListView:Landroid/widget/AbsListView;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$9(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/widget/AbsListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getCount()I

    move-result v1

    .line 2398
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_4
    if-lt v0, v1, :cond_6

    .line 2402
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V

    .line 2403
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->updateSelectionMenu()V
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$0(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)V

    .line 2404
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->DELETE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-eq v2, v3, :cond_0

    .line 2405
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuSelectionMode:Landroid/view/Menu;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$14(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/view/Menu;

    move-result-object v2

    invoke-interface {v2, v4, v4}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto/16 :goto_0

    .line 2400
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;->access$3(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeCallback;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v2

    invoke-virtual {v2, v0, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->selectView(IZ)V

    .line 2398
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2373
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
    .end sparse-switch
.end method
