.class Lcom/samsung/android/app/memo/uiwidget/PopupList$2;
.super Ljava/lang/Object;
.source "PopupList.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/PopupList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/PopupList;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 119
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->access$0(Lcom/samsung/android/app/memo/uiwidget/PopupList;)Landroid/widget/PopupWindow;

    move-result-object v0

    if-nez v0, :cond_1

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->access$0(Lcom/samsung/android/app/memo/uiwidget/PopupList;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 123
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/PopupList;->mOnPopupItemClickListener:Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->access$4(Lcom/samsung/android/app/memo/uiwidget/PopupList;)Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/PopupList;->mOnPopupItemClickListener:Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->access$4(Lcom/samsung/android/app/memo/uiwidget/PopupList;)Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;

    move-result-object v0

    long-to-int v1, p4

    invoke-interface {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;->onPopupItemClick(I)Z

    goto :goto_0
.end method
