.class public Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;
.super Ljava/lang/Object;
.source "NetworkUtil.java"


# static fields
.field private static final CONNECTION_TIMEOUT:I = 0x4e20

.field public static final HTTP_CONNECTION_MANAGER:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

.field public static final HTTP_PARAMS:Lorg/apache/http/params/HttpParams;

.field private static final TAG:Ljava/lang/String; = "NetworkUtil"

.field private static final USER_AGENT:Ljava/lang/String;


# instance fields
.field private final mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v4, 0x4e20

    .line 53
    new-instance v2, Ljava/lang/StringBuilder;

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "; "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;->USER_AGENT:Ljava/lang/String;

    .line 61
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 62
    .local v0, "params":Lorg/apache/http/params/HttpParams;
    const/4 v2, 0x0

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    .line 63
    invoke-static {v0, v4}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 64
    invoke-static {v0, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 65
    const/4 v2, 0x1

    invoke-static {v0, v2}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 66
    sget-object v2, Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;->USER_AGENT:Ljava/lang/String;

    invoke-static {v0, v2}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 67
    sput-object v0, Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;->HTTP_PARAMS:Lorg/apache/http/params/HttpParams;

    .line 69
    new-instance v1, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 70
    .local v1, "schemeRegistry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v4

    const/16 v5, 0x50

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 71
    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v4

    const/16 v5, 0x1bb

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v1, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 73
    new-instance v2, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v2, v0, v1}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    sput-object v2, Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;->HTTP_CONNECTION_MANAGER:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    .line 74
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    sget-object v1, Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;->HTTP_CONNECTION_MANAGER:Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    sget-object v2, Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;->HTTP_PARAMS:Lorg/apache/http/params/HttpParams;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    .line 49
    return-void
.end method

.method private ExecuteRequest(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;)V
    .locals 9
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2, "resp"    # Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    new-instance v3, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;

    invoke-direct {v3}, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;-><init>()V

    .line 93
    .local v3, "jsonfactory":Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;
    const/4 v2, 0x0

    .line 95
    .local v2, "httpResponse":Lorg/apache/http/HttpResponse;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;->mHttpClient:Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-virtual {v6, p1}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 100
    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    .line 101
    .local v5, "status":I
    const-string v6, "NetworkUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "status = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const/16 v6, 0xc8

    if-ne v5, v6, :cond_1

    .line 103
    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 104
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v4

    .line 105
    .local v4, "response":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->getReq_type()I

    move-result v6

    invoke-virtual {p2}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->getAuthinfo()Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;

    move-result-object v7

    invoke-virtual {v3, v4, v6, v7}, Lcom/samsung/android/app/migration/snbdownload/network/JsonFactory;->fromJSONString(Ljava/lang/String;ILcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;)V

    .line 107
    if-eqz v1, :cond_0

    const/16 v6, 0x194

    if-ne v5, v6, :cond_0

    .line 108
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 116
    :cond_0
    return-void

    .line 96
    .end local v1    # "entity":Lorg/apache/http/HttpEntity;
    .end local v4    # "response":Ljava/lang/String;
    .end local v5    # "status":I
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Ljava/io/IOException;
    throw v0

    .line 111
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v5    # "status":I
    :cond_1
    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 112
    .restart local v1    # "entity":Lorg/apache/http/HttpEntity;
    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v4

    .line 113
    .restart local v4    # "response":Ljava/lang/String;
    const-string v6, "NetworkUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "NOK: response = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    new-instance v6, Ljava/io/IOException;

    invoke-direct {v6}, Ljava/io/IOException;-><init>()V

    throw v6
.end method


# virtual methods
.method public get(Ljava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;)V
    .locals 1
    .param p1, "Url"    # Ljava/lang/String;
    .param p2, "resp"    # Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;->ExecuteRequest(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;)V

    .line 79
    return-void
.end method

.method public post(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;)V
    .locals 2
    .param p1, "Url"    # Ljava/lang/String;
    .param p2, "json"    # Ljava/lang/String;
    .param p3, "contentType"    # Ljava/lang/String;
    .param p4, "resp"    # Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    new-instance v0, Lorg/apache/http/entity/StringEntity;

    invoke-direct {v0, p2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .line 84
    .local v0, "entity":Lorg/apache/http/entity/StringEntity;
    invoke-virtual {v0, p3}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V

    .line 85
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v1, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 86
    .local v1, "post":Lorg/apache/http/client/methods/HttpPost;
    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 87
    invoke-direct {p0, v1, p4}, Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;->ExecuteRequest(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;)V

    .line 88
    return-void
.end method
