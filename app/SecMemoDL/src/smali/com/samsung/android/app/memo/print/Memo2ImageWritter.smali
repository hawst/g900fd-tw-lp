.class public Lcom/samsung/android/app/memo/print/Memo2ImageWritter;
.super Ljava/lang/Object;
.source "Memo2ImageWritter.java"


# static fields
.field public static final BITMAP_QUALITY:I = 0x64

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final HIDE_HEADER_FOOTER_SPACE:I

.field private final IMAGE_WIDTH:I

.field private final ORIENTATION_TAG:Ljava/lang/String;

.field private final PRINT_HEIGHT:I

.field private final PRINT_MARGIN_HEIGHT:I

.field private final PRINT_MARGIN_WIDTH:I

.field private final PRINT_WIDTH:I

.field private final SEARCH_IMAGE:Ljava/lang/String;

.field private final SHOW_HEADER_FOOTER_SPACE:I

.field private final SPECIAL_STRING:[Ljava/lang/String;

.field private final SYMBOLS:[Ljava/lang/String;

.field private final TEXT_LINE:F

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCanvas:Landroid/graphics/Canvas;

.field private final mContext:Landroid/content/Context;

.field private mDirFile:Ljava/io/File;

.field private final mHeaderFooterHeight:I

.field private mIsPrintingImage:Z

.field private mPageCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->TAG:Ljava/lang/String;

    .line 30
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/io/File;Z)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parentDir"    # Ljava/io/File;
    .param p3, "showHeaderFooter"    # Z

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput v0, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->HIDE_HEADER_FOOTER_SPACE:I

    .line 34
    const/16 v1, 0x14

    iput v1, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->SHOW_HEADER_FOOTER_SPACE:I

    .line 36
    const/16 v1, 0x32

    iput v1, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->PRINT_MARGIN_WIDTH:I

    .line 38
    const/16 v1, 0x19

    iput v1, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->PRINT_MARGIN_HEIGHT:I

    .line 40
    const/16 v1, 0x1ef

    iput v1, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->PRINT_WIDTH:I

    .line 42
    const/16 v1, 0x14a

    iput v1, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->IMAGE_WIDTH:I

    .line 44
    const/16 v1, 0x318

    iput v1, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->PRINT_HEIGHT:I

    .line 46
    const v1, 0x3e4ccccd    # 0.2f

    iput v1, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->TEXT_LINE:F

    .line 48
    const-string v1, "<img src=\"([^\"]+)"

    iput-object v1, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->SEARCH_IMAGE:Ljava/lang/String;

    .line 50
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "&nbsp;"

    aput-object v2, v1, v0

    const-string v2, "&amp;"

    aput-object v2, v1, v5

    const-string v2, "&apos;"

    aput-object v2, v1, v6

    const-string v2, "&quot;"

    aput-object v2, v1, v7

    const/4 v2, 0x4

    const-string v3, "&lt;"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "&gt;"

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->SPECIAL_STRING:[Ljava/lang/String;

    .line 52
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, " "

    aput-object v2, v1, v0

    const-string v2, "&"

    aput-object v2, v1, v5

    const-string v2, "\'"

    aput-object v2, v1, v6

    const-string v2, "\""

    aput-object v2, v1, v7

    const/4 v2, 0x4

    const-string v3, "<"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, ">"

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->SYMBOLS:[Ljava/lang/String;

    .line 54
    const-string v1, "orientation="

    iput-object v1, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->ORIENTATION_TAG:Ljava/lang/String;

    .line 58
    iput-object v4, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mBitmap:Landroid/graphics/Bitmap;

    .line 60
    iput-object v4, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mCanvas:Landroid/graphics/Canvas;

    .line 62
    iput-object v4, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mDirFile:Ljava/io/File;

    .line 64
    iput-boolean v0, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mIsPrintingImage:Z

    .line 71
    iput-object p1, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mContext:Landroid/content/Context;

    .line 72
    iput-object p2, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mDirFile:Ljava/io/File;

    .line 73
    const/16 v1, 0x1ef

    const/16 v2, 0x318

    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mBitmap:Landroid/graphics/Bitmap;

    .line 74
    new-instance v1, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mCanvas:Landroid/graphics/Canvas;

    .line 76
    if-eqz p3, :cond_0

    const/16 v0, 0x14

    :cond_0
    iput v0, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mHeaderFooterHeight:I

    .line 77
    return-void
.end method

.method private initCanvas(Landroid/graphics/Paint;)V
    .locals 6
    .param p1, "paintBG"    # Landroid/graphics/Paint;

    .prologue
    const/4 v1, 0x0

    .line 224
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mCanvas:Landroid/graphics/Canvas;

    const v3, 0x43f78000    # 495.0f

    const/high16 v4, 0x44460000    # 792.0f

    move v2, v1

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 225
    return-void
.end method

.method private setPage(JLjava/lang/String;Ljava/lang/String;IFLandroid/graphics/Paint;)V
    .locals 11
    .param p1, "memoId"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "lastModifiedAt"    # Ljava/lang/String;
    .param p5, "pageNum"    # I
    .param p6, "lineSpace"    # F
    .param p7, "paintText"    # Landroid/graphics/Paint;

    .prologue
    .line 202
    :try_start_0
    iget v6, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mHeaderFooterHeight:I

    if-eqz v6, :cond_0

    .line 204
    iget-object v6, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mCanvas:Landroid/graphics/Canvas;

    const/4 v7, 0x0

    move/from16 v0, p6

    move-object/from16 v1, p7

    invoke-virtual {v6, p3, v7, v0, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 205
    iget-object v6, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mCanvas:Landroid/graphics/Canvas;

    const/high16 v7, 0x43b40000    # 360.0f

    move/from16 v0, p6

    move-object/from16 v1, p7

    invoke-virtual {v6, p4, v7, v0, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 207
    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b000f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " : %d"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mPageCount:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 208
    .local v3, "footer":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mCanvas:Landroid/graphics/Canvas;

    const/high16 v7, 0x43450000    # 197.0f

    const v8, 0x4444c000    # 787.0f

    move-object/from16 v0, p7

    invoke-virtual {v6, v3, v7, v8, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 209
    iget v6, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mPageCount:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mPageCount:I

    .line 211
    .end local v3    # "footer":Ljava/lang/String;
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "/"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p5

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".jpg"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 212
    .local v5, "imageName":Ljava/lang/String;
    new-instance v4, Ljava/io/FileOutputStream;

    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mDirFile:Ljava/io/File;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 213
    .local v4, "fosImage":Ljava/io/FileOutputStream;
    iget-object v6, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mBitmap:Landroid/graphics/Bitmap;

    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x64

    invoke-virtual {v6, v7, v8, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 214
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 221
    .end local v4    # "fosImage":Ljava/io/FileOutputStream;
    .end local v5    # "imageName":Ljava/lang/String;
    :goto_0
    return-void

    .line 216
    :catch_0
    move-exception v2

    .line 217
    .local v2, "e":Ljava/io/FileNotFoundException;
    sget-object v6, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->TAG:Ljava/lang/String;

    const-string v7, "createPage"

    invoke-static {v6, v7, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 218
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v2

    .line 219
    .local v2, "e":Ljava/io/IOException;
    sget-object v6, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->TAG:Ljava/lang/String;

    const-string v7, "createPage"

    invoke-static {v6, v7, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public convert2image(JLjava/lang/String;Lcom/samsung/android/app/memo/Session;)V
    .locals 35
    .param p1, "memoId"    # J
    .param p3, "memoUUID"    # Ljava/lang/String;
    .param p4, "session"    # Lcom/samsung/android/app/memo/Session;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/memo/Session$SessionException;
        }
    .end annotation

    .prologue
    .line 81
    if-nez p4, :cond_3

    .line 82
    if-eqz p3, :cond_2

    new-instance v31, Lcom/samsung/android/app/memo/Session;

    move-object/from16 v0, v31

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/Session;-><init>(Ljava/lang/String;)V

    .line 86
    .local v31, "ss":Lcom/samsung/android/app/memo/Session;
    :goto_0
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/app/memo/Session;->getTitle()Ljava/lang/String;

    move-result-object v8

    .line 87
    .local v8, "title":Ljava/lang/String;
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/app/memo/Session;->getContent()Ljava/lang/String;

    move-result-object v4

    .line 88
    .local v4, "content":Ljava/lang/String;
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/app/memo/Session;->getLastModifiedAt()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Utils;->toCurrentDTFull(J)Ljava/lang/String;

    move-result-object v9

    .line 89
    .local v9, "lastModifiedAt":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    new-array v13, v5, [C

    const/4 v5, 0x0

    const/16 v6, 0x20

    aput-char v6, v13, v5

    .line 91
    .local v13, "contentChar":[C
    :goto_1
    const/16 v21, 0x0

    .line 92
    .local v21, "imgBmp":Landroid/graphics/Bitmap;
    new-instance v28, Landroid/graphics/Paint;

    invoke-direct/range {v28 .. v28}, Landroid/graphics/Paint;-><init>()V

    .line 93
    .local v28, "paintBG":Landroid/graphics/Paint;
    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    .line 95
    .local v12, "paintText":Landroid/graphics/Paint;
    const/4 v5, -0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 96
    const/high16 v5, -0x1000000

    invoke-virtual {v12, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 98
    invoke-virtual {v12}, Landroid/graphics/Paint;->ascent()F

    move-result v5

    neg-float v5, v5

    invoke-virtual {v12}, Landroid/graphics/Paint;->descent()F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual {v12}, Landroid/graphics/Paint;->ascent()F

    move-result v6

    neg-float v6, v6

    invoke-virtual {v12}, Landroid/graphics/Paint;->descent()F

    move-result v7

    add-float/2addr v6, v7

    const v7, 0x3e4ccccd    # 0.2f

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    float-to-int v0, v5

    move/from16 v22, v0

    .line 99
    .local v22, "lineSpace":I
    const/4 v10, 0x1

    .line 100
    .local v10, "pageNum":I
    const/16 v19, 0x0

    .line 101
    .local v19, "imageHeight":I
    const/16 v20, 0x0

    .line 102
    .local v20, "imageWidth":I
    const/16 v29, 0x0

    .line 103
    .local v29, "position":I
    const/4 v14, 0x0

    .line 104
    .local v14, "cutImagePointY":I
    const/4 v15, 0x5

    .line 105
    .local v15, "drawPointX":I
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mHeaderFooterHeight:I

    add-int v16, v5, v22

    .line 106
    .local v16, "drawPointY":I
    const-string v30, ""

    .line 107
    .local v30, "printLineString":Ljava/lang/String;
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mPageCount:I

    .line 108
    new-instance v32, Landroid/graphics/Rect;

    invoke-direct/range {v32 .. v32}, Landroid/graphics/Rect;-><init>()V

    .line 111
    .local v32, "textBounds":Landroid/graphics/Rect;
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->initCanvas(Landroid/graphics/Paint;)V

    .line 114
    :cond_0
    array-length v5, v13

    move/from16 v0, v29

    if-ge v0, v5, :cond_1

    .line 115
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static/range {v30 .. v30}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-char v6, v13, v29

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    .line 120
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mIsPrintingImage:Z

    if-eqz v5, :cond_6

    if-eqz v21, :cond_6

    .line 121
    sub-int v5, v19, v14

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mHeaderFooterHeight:I

    rsub-int v6, v6, 0x318

    if-le v5, v6, :cond_5

    .line 122
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mCanvas:Landroid/graphics/Canvas;

    new-instance v6, Landroid/graphics/Rect;

    const/4 v7, 0x0

    add-int/lit16 v11, v14, 0x318

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mHeaderFooterHeight:I

    move/from16 v33, v0

    sub-int v11, v11, v33

    move/from16 v0, v20

    invoke-direct {v6, v7, v14, v0, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 123
    new-instance v7, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mHeaderFooterHeight:I

    add-int v33, v20, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mHeaderFooterHeight:I

    move/from16 v34, v0

    move/from16 v0, v34

    rsub-int v0, v0, 0x318

    move/from16 v34, v0

    move/from16 v0, v33

    move/from16 v1, v34

    invoke-direct {v7, v15, v11, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 122
    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v5, v0, v6, v7, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 124
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mHeaderFooterHeight:I

    rsub-int v5, v5, 0x318

    add-int/2addr v14, v5

    .line 125
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mHeaderFooterHeight:I

    add-int v16, v5, v22

    .line 191
    :cond_1
    :goto_3
    add-int/lit8 v27, v10, 0x1

    .end local v10    # "pageNum":I
    .local v27, "pageNum":I
    move/from16 v0, v22

    int-to-float v11, v0

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    invoke-direct/range {v5 .. v12}, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->setPage(JLjava/lang/String;Ljava/lang/String;IFLandroid/graphics/Paint;)V

    .line 194
    array-length v5, v13

    move/from16 v0, v29

    if-lt v0, v5, :cond_f

    .line 198
    return-void

    .line 82
    .end local v4    # "content":Ljava/lang/String;
    .end local v8    # "title":Ljava/lang/String;
    .end local v9    # "lastModifiedAt":Ljava/lang/String;
    .end local v12    # "paintText":Landroid/graphics/Paint;
    .end local v13    # "contentChar":[C
    .end local v14    # "cutImagePointY":I
    .end local v15    # "drawPointX":I
    .end local v16    # "drawPointY":I
    .end local v19    # "imageHeight":I
    .end local v20    # "imageWidth":I
    .end local v21    # "imgBmp":Landroid/graphics/Bitmap;
    .end local v22    # "lineSpace":I
    .end local v27    # "pageNum":I
    .end local v28    # "paintBG":Landroid/graphics/Paint;
    .end local v29    # "position":I
    .end local v30    # "printLineString":Ljava/lang/String;
    .end local v31    # "ss":Lcom/samsung/android/app/memo/Session;
    .end local v32    # "textBounds":Landroid/graphics/Rect;
    :cond_2
    new-instance v31, Lcom/samsung/android/app/memo/Session;

    move-object/from16 v0, v31

    move-wide/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/Session;-><init>(J)V

    goto/16 :goto_0

    .line 84
    :cond_3
    move-object/from16 v31, p4

    .restart local v31    # "ss":Lcom/samsung/android/app/memo/Session;
    goto/16 :goto_0

    .line 89
    .restart local v4    # "content":Ljava/lang/String;
    .restart local v8    # "title":Ljava/lang/String;
    .restart local v9    # "lastModifiedAt":Ljava/lang/String;
    :cond_4
    const-string v5, "</p><p>"

    const-string v6, "\n"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "<p>"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "</p>"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v13

    goto/16 :goto_1

    .line 128
    .restart local v10    # "pageNum":I
    .restart local v12    # "paintText":Landroid/graphics/Paint;
    .restart local v13    # "contentChar":[C
    .restart local v14    # "cutImagePointY":I
    .restart local v15    # "drawPointX":I
    .restart local v16    # "drawPointY":I
    .restart local v19    # "imageHeight":I
    .restart local v20    # "imageWidth":I
    .restart local v21    # "imgBmp":Landroid/graphics/Bitmap;
    .restart local v22    # "lineSpace":I
    .restart local v28    # "paintBG":Landroid/graphics/Paint;
    .restart local v29    # "position":I
    .restart local v30    # "printLineString":Ljava/lang/String;
    .restart local v32    # "textBounds":Landroid/graphics/Rect;
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mCanvas:Landroid/graphics/Canvas;

    new-instance v6, Landroid/graphics/Rect;

    const/4 v7, 0x0

    move/from16 v0, v20

    move/from16 v1, v19

    invoke-direct {v6, v7, v14, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 129
    new-instance v7, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mHeaderFooterHeight:I

    add-int v33, v20, v15

    sub-int v34, v19, v14

    move/from16 v0, v33

    move/from16 v1, v34

    invoke-direct {v7, v15, v11, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 128
    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v5, v0, v6, v7, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 130
    sub-int v5, v19, v14

    add-int v16, v16, v5

    .line 131
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mIsPrintingImage:Z

    .line 132
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->recycle()V

    .line 136
    :cond_6
    const/4 v5, 0x0

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v6

    move-object/from16 v0, v30

    move-object/from16 v1, v32

    invoke-virtual {v12, v0, v5, v6, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 138
    move-object/from16 v0, v32

    iget v5, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v6}, Landroid/graphics/Canvas;->getWidth()I

    move-result v6

    add-int/lit8 v6, v6, -0x32

    if-lt v5, v6, :cond_7

    add-int/lit8 v5, v29, 0x1

    rem-int/lit8 v5, v5, 0x2

    if-nez v5, :cond_7

    const-string v5, "<img src=\"([^\"]+)"

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 139
    :cond_7
    aget-char v5, v13, v29

    const/16 v6, 0xa

    if-eq v5, v6, :cond_8

    array-length v5, v13

    add-int/lit8 v5, v5, -0x1

    move/from16 v0, v29

    if-ne v0, v5, :cond_a

    .line 141
    :cond_8
    const-string v5, "<img src=\"([^\"]+)"

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v23

    .line 142
    .local v23, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual/range {v23 .. v23}, Ljava/util/regex/Matcher;->find()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 144
    :try_start_0
    const-string v5, "orientation="

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const-string v6, "orientation="

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    add-int/lit8 v26, v5, 0x1

    .line 145
    .local v26, "orientationStart":I
    const/16 v5, 0x22

    add-int/lit8 v6, v26, 0x1

    move-object/from16 v0, v30

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->indexOf(II)I

    move-result v25

    .line 146
    .local v25, "orientationEnd":I
    move-object/from16 v0, v30

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v24

    .line 147
    .local v24, "orientation":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mContext:Landroid/content/Context;

    const/4 v6, 0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, -0x1

    const/16 v11, 0x14a

    move/from16 v0, v24

    invoke-static {v5, v6, v7, v0, v11}, Lcom/samsung/android/app/memo/util/ImageUtil;->decodeImageScaledIf(Landroid/content/Context;Landroid/net/Uri;III)Landroid/graphics/Bitmap;

    move-result-object v21

    .line 148
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v19

    .line 149
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    .line 150
    const-string v30, ""

    .line 151
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 152
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v5}, Landroid/graphics/Canvas;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    div-int/lit8 v6, v20, 0x2

    sub-int v15, v5, v6

    .line 154
    :cond_9
    add-int v5, v19, v16

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mHeaderFooterHeight:I

    rsub-int v6, v6, 0x318

    if-ge v5, v6, :cond_c

    .line 155
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mCanvas:Landroid/graphics/Canvas;

    int-to-float v6, v15

    move/from16 v0, v16

    int-to-float v7, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v28

    invoke-virtual {v5, v0, v6, v7, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 156
    add-int v5, v19, v22

    add-int v16, v16, v5

    .line 157
    if-eqz v21, :cond_a

    .line 158
    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    .end local v23    # "matcher":Ljava/util/regex/Matcher;
    .end local v24    # "orientation":I
    .end local v25    # "orientationEnd":I
    .end local v26    # "orientationStart":I
    :cond_a
    :goto_4
    add-int/lit8 v29, v29, 0x1

    .line 185
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mHeaderFooterHeight:I

    rsub-int v5, v5, 0x318

    move/from16 v0, v16

    if-gt v0, v5, :cond_b

    array-length v5, v13

    move/from16 v0, v29

    if-lt v0, v5, :cond_0

    .line 186
    :cond_b
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mHeaderFooterHeight:I

    add-int v16, v5, v22

    goto/16 :goto_3

    .line 161
    .restart local v23    # "matcher":Ljava/util/regex/Matcher;
    .restart local v24    # "orientation":I
    .restart local v25    # "orientationEnd":I
    .restart local v26    # "orientationStart":I
    :cond_c
    const/4 v14, 0x0

    .line 164
    :try_start_1
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mHeaderFooterHeight:I

    add-int v16, v5, v22

    .line 165
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mIsPrintingImage:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3

    .line 169
    .end local v24    # "orientation":I
    .end local v25    # "orientationEnd":I
    .end local v26    # "orientationStart":I
    :catch_0
    move-exception v17

    .line 170
    .local v17, "e":Ljava/io/IOException;
    sget-object v5, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->TAG:Ljava/lang/String;

    const-string v6, "convert2image"

    move-object/from16 v0, v17

    invoke-static {v5, v6, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 173
    .end local v17    # "e":Ljava/io/IOException;
    :cond_d
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->SPECIAL_STRING:[Ljava/lang/String;

    array-length v5, v5

    move/from16 v0, v18

    if-lt v0, v5, :cond_e

    .line 176
    const/4 v15, 0x5

    .line 177
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mCanvas:Landroid/graphics/Canvas;

    int-to-float v6, v15

    move/from16 v0, v16

    int-to-float v7, v0

    move-object/from16 v0, v30

    invoke-virtual {v5, v0, v6, v7, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 178
    add-int v16, v16, v22

    .line 179
    const-string v30, ""

    goto :goto_4

    .line 174
    :cond_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->SPECIAL_STRING:[Ljava/lang/String;

    aget-object v5, v5, v18

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->SYMBOLS:[Ljava/lang/String;

    aget-object v6, v6, v18

    move-object/from16 v0, v30

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v30

    .line 173
    add-int/lit8 v18, v18, 0x1

    goto :goto_5

    .end local v10    # "pageNum":I
    .end local v18    # "i":I
    .end local v23    # "matcher":Ljava/util/regex/Matcher;
    .restart local v27    # "pageNum":I
    :cond_f
    move/from16 v10, v27

    .end local v27    # "pageNum":I
    .restart local v10    # "pageNum":I
    goto/16 :goto_2
.end method

.method public releaseAll()V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/Memo2ImageWritter;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 231
    :cond_0
    return-void
.end method
