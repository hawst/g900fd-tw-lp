.class public Lcom/samsung/android/app/migration/receiver/KiesReceiver;
.super Landroid/content/BroadcastReceiver;
.source "KiesReceiver.java"


# static fields
.field public static final CATEGORY_EXTRAS:Ljava/lang/String; = "category"

.field private static final CONTENT_EXTRAS:Ljava/lang/String; = "content"

.field public static final CREATE_TIME_EXTRAS:Ljava/lang/String; = "create_t"

.field public static final KIES_INTENT_BACKUP_REQUEST:Ljava/lang/String; = "com.sec.android.intent.action.REQUEST_BACKUP_MEMO"

.field public static final KIES_INTENT_BACKUP_RESPONSE:Ljava/lang/String; = "com.sec.android.intent.action.RESPONSE_BACKUP_MEMO"

.field public static final KIES_INTENT_RESTORE_REQUEST:Ljava/lang/String; = "com.sec.android.intent.action.REQUEST_RESTORE_MEMO"

.field public static final KIES_INTENT_RESTORE_RESPONSE:Ljava/lang/String; = "com.sec.android.intent.action.RESPONSE_RESTORE_MEMO"

.field public static final KIES_INTENT_TMEMO1_REQUEST:Ljava/lang/String; = "com.sec.android.memo.KIES_RESTORES_AMEMO"

.field public static final KIES_INTENT_TMEMO1_RESPONSE:Ljava/lang/String; = "com.sec.android.memo.KIES_RESPONSE_AMEMO"

.field private static final MIGRATION_TYPE_ERROR:I = 0x0

.field private static final MIGRATION_TYPE_MEMO:I = 0x2

.field private static final MIGRATION_TYPE_TMEMO2:I = 0x1

.field public static final MODIFY_TIME_EXTRAS:Ljava/lang/String; = "modify_t"

.field private static final PATH_EXTRAS:Ljava/lang/String; = "SAVE_PATH"

.field public static final SESSION_EXTRAS:Ljava/lang/String; = "SESSION_KEY"

.field public static final SOURCE_EXTRAS:Ljava/lang/String; = "SOURCE"

.field private static final TAG:Ljava/lang/String; = "KiesReceiver"

.field public static final TITLE_EXTRAS:Ljava/lang/String; = "title"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private backUpMemo(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 160
    const-string v2, ""

    .line 161
    .local v2, "targetPath":Ljava/lang/String;
    const-string v3, "SAVE_PATH"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 162
    const-string v3, "SAVE_PATH"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 163
    :cond_0
    const-string v0, ""

    .line 164
    .local v0, "sessionKey":Ljava/lang/String;
    const-string v3, "SESSION_KEY"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 165
    const-string v3, "SESSION_KEY"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 166
    :cond_1
    const-string v1, ""

    .line 167
    .local v1, "source":Ljava/lang/String;
    const-string v3, "SOURCE"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 168
    const-string v3, "SOURCE"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 169
    :cond_2
    const-string v3, "KiesReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "com.sec.android.intent.action.REQUEST_BACKUP_MEMO received from "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". Extra params: path=["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const-string v3, "/storage"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 171
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 172
    :cond_3
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v3

    invoke-virtual {v3, p1, v2, v1, v0}, Lcom/samsung/android/app/migration/Migration;->startKiesBackUpMemoMigration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method private restoreTMemo1(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v10, 0x0

    .line 81
    const-string v2, ""

    .line 82
    .local v2, "title":Ljava/lang/String;
    const-string v0, "title"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const-string v0, "title"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 84
    :cond_0
    const-string v3, ""

    .line 85
    .local v3, "content":Ljava/lang/String;
    const-string v0, "content"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    const-string v0, "content"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 87
    :cond_1
    const-string v4, ""

    .line 88
    .local v4, "category":Ljava/lang/String;
    const-string v0, "category"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    const-string v0, "category"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 90
    :cond_2
    const-wide/16 v6, 0x0

    .line 91
    .local v6, "create_t":J
    const-string v0, "create_t"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 92
    const-string v0, "create_t"

    invoke-virtual {p2, v0, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 93
    :cond_3
    const-wide/16 v8, 0x0

    .line 94
    .local v8, "modify_t":J
    const-string v0, "modify_t"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 95
    const-string v0, "modify_t"

    invoke-virtual {p2, v0, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    .line 96
    :cond_4
    const-string v5, ""

    .line 97
    .local v5, "source":Ljava/lang/String;
    const-string v0, "SOURCE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 98
    const-string v0, "SOURCE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 99
    :cond_5
    const-string v0, "KiesReceiver"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "com.sec.android.memo.KIES_RESTORES_AMEMO received from "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ". Extra params: title=["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "], content=["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 100
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "], category=["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "], create_t=["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "], modify_t=["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 99
    invoke-static {v0, v10}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    new-instance v1, Lcom/samsung/android/app/migration/task/TMemo1Data;

    invoke-direct/range {v1 .. v9}, Lcom/samsung/android/app/migration/task/TMemo1Data;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 103
    .local v1, "data":Lcom/samsung/android/app/migration/task/TMemo1Data;
    invoke-static {}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->getInstance()Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->isAlive()Z

    move-result v0

    if-nez v0, :cond_7

    .line 104
    invoke-static {}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->getInstance()Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;

    move-result-object v0

    const v10, 0x7f0b0052

    invoke-virtual {p1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->setTitle(Ljava/lang/String;)V

    .line 105
    invoke-static {}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->getInstance()Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->setContext(Landroid/content/Context;)V

    .line 106
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    if-eqz v0, :cond_6

    .line 107
    invoke-static {}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->getInstance()Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v10

    iget-object v10, v10, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v0, v10}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->setContext(Landroid/app/Activity;)V

    .line 108
    :cond_6
    invoke-static {}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->getInstance()Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->start()V

    .line 111
    :goto_0
    invoke-static {}, Lcom/samsung/android/app/migration/MemoConverter;->getInstance()Lcom/samsung/android/app/migration/MemoConverter;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/MemoConverter;->addToTMemo1Queue(Lcom/samsung/android/app/migration/task/TMemo1Data;)V

    .line 112
    return-void

    .line 110
    :cond_7
    invoke-static {}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->getInstance()Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->setContext(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private restoreTMemo2(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 115
    const-string v4, ""

    .line 116
    .local v4, "path":Ljava/lang/String;
    const-string v7, "SAVE_PATH"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 117
    const-string v7, "SAVE_PATH"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 118
    :cond_0
    const-string v5, ""

    .line 119
    .local v5, "sessionKey":Ljava/lang/String;
    const-string v7, "SESSION_KEY"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 120
    const-string v7, "SESSION_KEY"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 121
    :cond_1
    const-string v6, ""

    .line 122
    .local v6, "source":Ljava/lang/String;
    const-string v7, "SOURCE"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 123
    const-string v7, "SOURCE"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 124
    :cond_2
    const-string v7, "KiesReceiver"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "com.sec.android.intent.action.REQUEST_RESTORE_MEMO received from "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ". Extra params: path=["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string v7, "/storage"

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 126
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 128
    :cond_3
    const/4 v3, 0x0

    .line 129
    .local v3, "migrationType":I
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 130
    .local v2, "folder":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 131
    invoke-virtual {v2}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v8

    array-length v9, v8

    const/4 v7, 0x0

    :goto_0
    if-lt v7, v9, :cond_6

    .line 139
    :cond_4
    if-eqz v3, :cond_5

    .line 140
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 141
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    if-eqz v7, :cond_5

    .line 142
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    iget-object v7, v7, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    if-eqz v7, :cond_5

    .line 143
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v7

    iget-object v7, v7, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    iget-object v7, v7, Lcom/samsung/android/app/memo/Main;->mMemoListFragment:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v7}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->hideUndoBar()V

    .line 144
    :cond_5
    packed-switch v3, :pswitch_data_0

    .line 157
    :goto_1
    return-void

    .line 131
    :cond_6
    aget-object v0, v8, v7

    .line 132
    .local v0, "child":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 133
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const-string v11, ".snb"

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 134
    const/4 v3, 0x1

    .line 131
    :cond_7
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 135
    :cond_8
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const-string v11, ".bk"

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 136
    const/4 v3, 0x2

    goto :goto_2

    .line 146
    .end local v0    # "child":Ljava/lang/String;
    .end local v1    # "file":Ljava/io/File;
    :pswitch_0
    const-string v7, "KiesReceiver"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Neither .snb files, nor .bk file are detected in "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const v7, 0x7f0b0050

    const/4 v8, 0x0

    invoke-static {p1, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 148
    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-static {p1, v6, v7, v8}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->sendRestoreResponse(Landroid/content/Context;Ljava/lang/String;II)V

    goto :goto_1

    .line 151
    :pswitch_1
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v7

    invoke-virtual {v7, p1, v4, v6}, Lcom/samsung/android/app/migration/Migration;->startKiesRestoreTMemo2Migration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 154
    :pswitch_2
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v7

    invoke-virtual {v7, p1, v4, v6, v5}, Lcom/samsung/android/app/migration/Migration;->startKiesRestoreMemoMigration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 144
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static sendBackupResponse(Landroid/content/Context;Ljava/lang/String;II)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "result"    # I
    .param p3, "errCode"    # I

    .prologue
    .line 176
    const-string v1, "KiesReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "com.sec.android.intent.action.RESPONSE_BACKUP_MEMO: res = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " code = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " source = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.intent.action.RESPONSE_BACKUP_MEMO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 178
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "RESULT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 179
    const-string v1, "ERR_CODE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 180
    const-string v1, "REQ_SIZE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 181
    const-string v1, "SOURCE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 182
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 183
    return-void
.end method

.method public static sendRestoreResponse(Landroid/content/Context;Ljava/lang/String;II)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "result"    # I
    .param p3, "errCode"    # I

    .prologue
    .line 186
    const-string v1, "KiesReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "com.sec.android.intent.action.RESPONSE_RESTORE_MEMO: res = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " code = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " source = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.intent.action.RESPONSE_RESTORE_MEMO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 188
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "RESULT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 189
    const-string v1, "ERR_CODE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 190
    const-string v1, "REQ_SIZE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 191
    const-string v1, "SOURCE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 193
    return-void
.end method

.method public static sendTMemo1RestoreResponse(Landroid/content/Context;Ljava/lang/String;II)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "result"    # I
    .param p3, "errCode"    # I

    .prologue
    .line 196
    const-string v1, "KiesReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "com.sec.android.memo.KIES_RESPONSE_AMEMO: res = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " code = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " source = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.memo.KIES_RESPONSE_AMEMO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 198
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "RESULT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 199
    const-string v1, "ERR_CODE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 200
    const-string v1, "REQ_SIZE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 201
    const-string v1, "SOURCE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 202
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 203
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 64
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.memo.KIES_RESTORES_AMEMO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->restoreTMemo1(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 71
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.intent.action.REQUEST_RESTORE_MEMO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 72
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->restoreTMemo2(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 75
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.intent.action.REQUEST_BACKUP_MEMO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->backUpMemo(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method
