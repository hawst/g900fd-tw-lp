.class public final Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;
.super Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;
.source "ORSResponse.java"


# static fields
.field private static final KEY:Ljava/lang/String; = "key"

.field private static final METADATA:Ljava/lang/String; = "metadata"

.field private static final RCODE:Ljava/lang/String; = "rcode"

.field private static final VALUE:Ljava/lang/String; = "value"


# instance fields
.field private mORSResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordORSItem;

.field private mResponseCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;->mResponseCode:I

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;->mORSResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordORSItem;

    .line 39
    return-void
.end method

.method public fromJSON(Ljava/lang/Object;)V
    .locals 8
    .param p1, "json"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 51
    new-instance v2, Lorg/json/JSONObject;

    check-cast p1, Ljava/lang/String;

    .end local p1    # "json":Ljava/lang/Object;
    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 52
    .local v2, "jsonObj":Lorg/json/JSONObject;
    const-string v7, "rcode"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 53
    const-string v7, "rcode"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;->mResponseCode:I

    .line 54
    :cond_0
    const-string v7, "metadata"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 55
    const-string v7, "metadata"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 56
    .local v0, "dataitemarray":Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 57
    .local v4, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v4, :cond_2

    .line 65
    .end local v0    # "dataitemarray":Lorg/json/JSONArray;
    .end local v1    # "i":I
    .end local v4    # "length":I
    :cond_1
    return-void

    .line 58
    .restart local v0    # "dataitemarray":Lorg/json/JSONArray;
    .restart local v1    # "i":I
    .restart local v4    # "length":I
    :cond_2
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 59
    .local v5, "obj":Lorg/json/JSONObject;
    const-string v7, "key"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 60
    .local v3, "key":Ljava/lang/String;
    const-string v7, "value"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 62
    .local v6, "value":Ljava/lang/String;
    new-instance v7, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordORSItem;

    invoke-direct {v7, v3, v6}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordORSItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v7, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;->mORSResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordORSItem;

    .line 57
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getORSResponse()Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordORSItem;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;->mORSResponse:Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordORSItem;

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;->mResponseCode:I

    return v0
.end method

.method public toJSON()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 70
    const/4 v0, 0x0

    return-object v0
.end method
