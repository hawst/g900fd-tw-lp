.class Lcom/samsung/android/app/memo/print/GooglePrintHtml$2;
.super Landroid/print/PrintDocumentAdapter;
.source "GooglePrintHtml.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/print/GooglePrintHtml;->createWebPrintJob()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mWrappedInstance:Landroid/print/PrintDocumentAdapter;

.field final synthetic this$0:Lcom/samsung/android/app/memo/print/GooglePrintHtml;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/print/GooglePrintHtml;)V
    .locals 2

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml$2;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintHtml;

    .line 83
    invoke-direct {p0}, Landroid/print/PrintDocumentAdapter;-><init>()V

    .line 85
    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mWebView:Landroid/webkit/WebView;
    invoke-static {p1}, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->access$2(Lcom/samsung/android/app/memo/print/GooglePrintHtml;)Landroid/webkit/WebView;

    move-result-object v0

    .line 86
    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mTitle:Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->access$3(Lcom/samsung/android/app/memo/print/GooglePrintHtml;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->createPrintDocumentAdapter(Ljava/lang/String;)Landroid/print/PrintDocumentAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml$2;->mWrappedInstance:Landroid/print/PrintDocumentAdapter;

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 5

    .prologue
    .line 110
    iget-object v3, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml$2;->mWrappedInstance:Landroid/print/PrintDocumentAdapter;

    invoke-virtual {v3}, Landroid/print/PrintDocumentAdapter;->onFinish()V

    .line 113
    iget-object v3, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml$2;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintHtml;

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintHtml;->mWebView:Landroid/webkit/WebView;
    invoke-static {v3}, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->access$2(Lcom/samsung/android/app/memo/print/GooglePrintHtml;)Landroid/webkit/WebView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/webkit/WebView;->destroy()V

    .line 114
    iget-object v3, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml$2;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintHtml;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->access$4(Lcom/samsung/android/app/memo/print/GooglePrintHtml;Landroid/webkit/WebView;)V

    .line 115
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 116
    const-string v4, "/.tempMemoPrint"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 115
    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 117
    .local v2, "printFolder":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 118
    invoke-virtual {v2}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "children":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-lt v1, v3, :cond_1

    .line 122
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 124
    .end local v0    # "children":[Ljava/lang/String;
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 120
    .restart local v0    # "children":[Ljava/lang/String;
    .restart local v1    # "i":I
    :cond_1
    new-instance v3, Ljava/io/File;

    aget-object v4, v0, v1

    invoke-direct {v3, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 119
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "oldAttributes"    # Landroid/print/PrintAttributes;
    .param p2, "newAttributes"    # Landroid/print/PrintAttributes;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "callback"    # Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
    .param p5, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml$2;->mWrappedInstance:Landroid/print/PrintDocumentAdapter;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/print/PrintDocumentAdapter;->onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V

    .line 100
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml$2;->mWrappedInstance:Landroid/print/PrintDocumentAdapter;

    invoke-virtual {v0}, Landroid/print/PrintDocumentAdapter;->onStart()V

    .line 92
    return-void
.end method

.method public onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 1
    .param p1, "pages"    # [Landroid/print/PageRange;
    .param p2, "destination"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "callback"    # Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml$2;->mWrappedInstance:Landroid/print/PrintDocumentAdapter;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/print/PrintDocumentAdapter;->onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V

    .line 106
    return-void
.end method
