.class Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;
.super Ljava/lang/Object;
.source "RichEditor.java"

# interfaces
.implements Lcom/samsung/android/app/memo/uiwidget/ClipboardService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/RichEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mActivity:Landroid/content/Context;

.field private mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInit(Landroid/content/Context;Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V
    .locals 0
    .param p1, "mActivity"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .prologue
    .line 1094
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mActivity:Landroid/content/Context;

    .line 1095
    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 1096
    return-void
.end method

.method public onTextCopy(Ljava/lang/CharSequence;)V
    .locals 21
    .param p1, "str"    # Ljava/lang/CharSequence;

    .prologue
    .line 1043
    new-instance v14, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1044
    .local v14, "span":Landroid/text/SpannableStringBuilder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionEnd()I

    move-result v20

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->min(II)I

    move-result v19

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    move-result v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$12(Lcom/samsung/android/app/memo/uiwidget/RichEditor;I)V

    .line 1045
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionEnd()I

    move-result v20

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->max(II)I

    move-result v19

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    move-result v18

    invoke-static/range {v17 .. v18}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$13(Lcom/samsung/android/app/memo/uiwidget/RichEditor;I)V

    .line 1046
    new-instance v13, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v17

    .line 1047
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$14(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$15(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)I

    move-result v19

    invoke-interface/range {v17 .. v19}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v17

    check-cast v17, Landroid/text/Spanned;

    .line 1046
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/app/memo/util/HtmlUtil;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v13, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1048
    .local v13, "spHtml":Ljava/lang/StringBuilder;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1049
    .local v4, "copyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/16 v16, -0x1

    .local v16, "startTag":I
    const/4 v5, -0x1

    .local v5, "endTag":I
    const/4 v9, 0x0

    .line 1051
    .local v9, "lastEndPos":I
    const/16 v17, 0x0

    invoke-virtual {v14}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v18

    const-class v19, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    move/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v14, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v11

    .line 1052
    .local v11, "objArr":[Ljava/lang/Object;
    if-eqz v11, :cond_0

    .line 1053
    array-length v0, v11

    move/from16 v19, v0

    const/16 v17, 0x0

    move/from16 v18, v17

    :goto_0
    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_3

    .line 1078
    :cond_0
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    const-string v18, "<p>"

    const-string v19, ""

    invoke-virtual/range {v17 .. v19}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1079
    .local v15, "st":Ljava/lang/String;
    const-string v17, "</p>"

    const-string v18, "<br>"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1080
    const-string v17, "<br>"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    .line 1081
    .local v8, "ind":I
    const/16 v17, 0x0

    const/16 v18, -0x1

    move/from16 v0, v18

    if-ne v8, v0, :cond_1

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v8

    .end local v8    # "ind":I
    :cond_1
    move/from16 v0, v17

    invoke-virtual {v15, v0, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    .line 1082
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mActivity:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->newInstance(Landroid/content/Context;)Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    move-result-object v3

    .line 1083
    .local v3, "clipboardEXProxy":Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
    invoke-virtual {v3}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled()Z

    move-result v17

    if-eqz v17, :cond_2

    .line 1084
    new-instance v12, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;

    invoke-direct {v12}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;-><init>()V

    .line 1085
    .local v12, "proxyData":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    const/16 v17, 0x4

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setFormat(I)V

    .line 1086
    invoke-virtual {v12, v15}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setData(Ljava/lang/CharSequence;)V

    .line 1087
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    # invokes: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->addPermission(Ljava/util/ArrayList;)V
    invoke-static {v0, v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$16(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/util/ArrayList;)V

    .line 1088
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v12}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setData(Landroid/content/Context;Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;)V

    .line 1090
    .end local v3    # "clipboardEXProxy":Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
    .end local v12    # "proxyData":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    .end local v15    # "st":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 1053
    :cond_3
    aget-object v10, v11, v18

    .line 1054
    .local v10, "obj":Ljava/lang/Object;
    instance-of v0, v10, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    move/from16 v17, v0

    if-eqz v17, :cond_6

    move-object/from16 v17, v10

    .line 1055
    check-cast v17, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->getContentUri()Landroid/net/Uri;

    move-result-object v7

    .line 1056
    .local v7, "imageUri":Landroid/net/Uri;
    if-eqz v7, :cond_6

    .line 1057
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1059
    check-cast v10, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    .end local v10    # "obj":Ljava/lang/Object;
    invoke-virtual {v10}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->toHtmlForCopy()Ljava/lang/String;

    move-result-object v6

    .line 1062
    .local v6, "htmlImgTag":Ljava/lang/String;
    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v17

    const-string v20, "<img"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v17

    add-int v16, v17, v9

    .line 1063
    const-string v17, "/>"

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v13, v0, v1}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;I)I

    move-result v17

    add-int/lit8 v5, v17, 0x2

    .line 1065
    add-int/lit8 v9, v5, 0x1

    .line 1067
    if-ltz v16, :cond_4

    if-gez v5, :cond_5

    .line 1068
    :cond_4
    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$9()Ljava/lang/String;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Copy Failure : IndexOutofBounds start:"

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    .line 1069
    const-string v19, " end:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 1068
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1072
    :cond_5
    move/from16 v0, v16

    if-le v5, v0, :cond_6

    .line 1073
    move/from16 v0, v16

    invoke-virtual {v13, v0, v5, v6}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1053
    .end local v6    # "htmlImgTag":Ljava/lang/String;
    .end local v7    # "imageUri":Landroid/net/Uri;
    :cond_6
    add-int/lit8 v17, v18, 0x1

    move/from16 v18, v17

    goto/16 :goto_0
.end method

.method public onTextPaste(Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;Ljava/lang/CharSequence;II)V
    .locals 20
    .param p1, "clipdataTemp"    # Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    .param p2, "str"    # Ljava/lang/CharSequence;
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    .line 814
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    sub-int v6, p4, p3

    sub-int/2addr v3, v6

    sget v6, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mMaxCharSize:I

    if-ne v3, v6, :cond_2

    const/4 v13, 0x1

    .line 815
    .local v13, "isLimitReached":Z
    :goto_0
    const/4 v12, 0x1

    .line 816
    .local v12, "isImageFulled":Z
    const/4 v3, 0x0

    sput-boolean v3, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mImagePasteFailed:Z

    .line 817
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$5(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Z)V

    .line 818
    if-eqz p1, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isPasteOperationGoing()Z

    move-result v3

    if-nez v3, :cond_1

    .line 819
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPasteOperationState(Z)V

    .line 820
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getFormat()I

    move-result v3

    const/4 v6, 0x2

    if-ne v3, v6, :cond_9

    .line 821
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getData()Ljava/lang/CharSequence;

    move-result-object v17

    .line 822
    .local v17, "seq":Ljava/lang/CharSequence;
    if-eqz v17, :cond_0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 823
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPasteOperationState(Z)V

    .line 905
    .end local v17    # "seq":Ljava/lang/CharSequence;
    :cond_1
    :goto_1
    return-void

    .line 814
    .end local v12    # "isImageFulled":Z
    .end local v13    # "isLimitReached":Z
    :cond_2
    const/4 v13, 0x0

    goto :goto_0

    .line 826
    .restart local v12    # "isImageFulled":Z
    .restart local v13    # "isLimitReached":Z
    .restart local v17    # "seq":Ljava/lang/CharSequence;
    :cond_3
    if-nez v13, :cond_8

    .line 827
    move/from16 v0, p3

    move/from16 v1, p4

    if-ne v0, v1, :cond_6

    if-lez p3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    add-int/lit8 v6, p3, -0x1

    invoke-interface {v3, v6}, Landroid/text/Editable;->charAt(I)C

    move-result v3

    const/16 v6, 0x20

    if-eq v3, v6, :cond_6

    .line 828
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-interface {v3, v0, v1, v6}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 839
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPasteOperationState(Z)V

    .line 899
    .end local v17    # "seq":Ljava/lang/CharSequence;
    :cond_4
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getFormat()I

    move-result v3

    const/4 v6, 0x4

    if-eq v3, v6, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getFormat()I

    move-result v3

    const/4 v6, 0x3

    if-eq v3, v6, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getFormat()I

    move-result v3

    const/4 v6, 0x2

    if-ne v3, v6, :cond_5

    .line 900
    if-nez v13, :cond_1

    :cond_5
    if-eqz v12, :cond_1

    sget-boolean v3, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mImagePasteFailed:Z

    if-nez v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$10(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isCalledPasteFromClipboard()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 901
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v3

    const v6, 0x7f0b00b2

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 902
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$10(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->resetIsPastedFromTheClipboard()V

    goto/16 :goto_1

    .line 830
    .restart local v17    # "seq":Ljava/lang/CharSequence;
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getEditableText()Landroid/text/Editable;

    move-result-object v3

    .line 831
    const-class v6, Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-interface {v3, v0, v1, v6}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v18

    check-cast v18, [Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 832
    .local v18, "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    move-object/from16 v0, v18

    array-length v3, v0

    if-lez v3, :cond_7

    .line 833
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v19

    .line 834
    .local v19, "text":Landroid/text/Editable;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    const/4 v6, 0x0

    aget-object v6, v18, v6

    invoke-interface {v3, v6}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 835
    if-lez p3, :cond_7

    add-int/lit8 v3, p3, -0x1

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/text/Editable;->charAt(I)C

    move-result v3

    const/16 v6, 0xa

    if-ne v3, v6, :cond_7

    add-int/lit8 p3, p3, -0x1

    .line 837
    .end local v19    # "text":Landroid/text/Editable;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    move/from16 v0, p3

    move/from16 v1, p4

    move-object/from16 v2, v17

    invoke-interface {v3, v0, v1, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto/16 :goto_2

    .line 841
    .end local v18    # "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v3

    const v6, 0x7f0b0048

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 842
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPasteOperationState(Z)V

    goto/16 :goto_3

    .line 844
    .end local v17    # "seq":Ljava/lang/CharSequence;
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getFormat()I

    move-result v3

    const/4 v6, 0x3

    if-ne v3, v6, :cond_b

    .line 845
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "file://"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getData()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 846
    .local v11, "imagePath":Landroid/net/Uri;
    new-instance v5, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-direct {v5, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 847
    .local v5, "spn":Landroid/text/SpannableStringBuilder;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 848
    .local v4, "HtmlStr":Ljava/lang/StringBuilder;
    const-string v3, "<img src=\""

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 849
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "\">"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 851
    if-nez v13, :cond_a

    .line 852
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .end local v4    # "HtmlStr":Ljava/lang/StringBuilder;
    const/4 v8, 0x1

    move/from16 v6, p3

    move/from16 v7, p4

    # invokes: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->addHtmlText(Ljava/lang/String;Landroid/text/SpannableStringBuilder;IIZ)V
    invoke-static/range {v3 .. v8}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$6(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/lang/String;Landroid/text/SpannableStringBuilder;IIZ)V

    goto/16 :goto_3

    .line 854
    .restart local v4    # "HtmlStr":Ljava/lang/StringBuilder;
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v3

    const v6, 0x7f0b0048

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 855
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPasteOperationState(Z)V

    goto/16 :goto_3

    .line 857
    .end local v4    # "HtmlStr":Ljava/lang/StringBuilder;
    .end local v5    # "spn":Landroid/text/SpannableStringBuilder;
    .end local v11    # "imagePath":Landroid/net/Uri;
    :cond_b
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getFormat()I

    move-result v3

    const/4 v6, 0x4

    if-ne v3, v6, :cond_12

    .line 858
    new-instance v5, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-direct {v5, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 859
    .restart local v5    # "spn":Landroid/text/SpannableStringBuilder;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getData()Ljava/lang/CharSequence;

    move-result-object v17

    .line 860
    .restart local v17    # "seq":Ljava/lang/CharSequence;
    if-eqz v17, :cond_c

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 861
    :cond_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPasteOperationState(Z)V

    goto/16 :goto_1

    .line 864
    :cond_d
    invoke-interface/range {v17 .. v17}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 865
    .local v4, "HtmlStr":Ljava/lang/String;
    const/4 v9, 0x0

    .line 866
    .local v9, "count":I
    :goto_4
    const-string v3, "\n"

    invoke-virtual {v4, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 874
    :cond_e
    if-nez v13, :cond_11

    .line 875
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v8, 0x1

    move/from16 v6, p3

    move/from16 v7, p4

    # invokes: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->addHtmlText(Ljava/lang/String;Landroid/text/SpannableStringBuilder;IIZ)V
    invoke-static/range {v3 .. v8}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$6(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/lang/String;Landroid/text/SpannableStringBuilder;IIZ)V

    goto/16 :goto_3

    .line 867
    :cond_f
    const/4 v3, 0x2

    if-gt v9, v3, :cond_e

    .line 869
    const-string v3, "\n"

    invoke-virtual {v4, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    .line 870
    .local v10, "endLineIndex":I
    if-lez v10, :cond_10

    .line 871
    const/4 v3, 0x0

    invoke-virtual {v4, v3, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 872
    :cond_10
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 877
    .end local v10    # "endLineIndex":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v3

    const v6, 0x7f0b0048

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 878
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPasteOperationState(Z)V

    goto/16 :goto_3

    .line 880
    .end local v4    # "HtmlStr":Ljava/lang/String;
    .end local v5    # "spn":Landroid/text/SpannableStringBuilder;
    .end local v9    # "count":I
    .end local v17    # "seq":Ljava/lang/CharSequence;
    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getFormat()I

    move-result v3

    const/4 v6, 0x5

    if-ne v3, v6, :cond_4

    .line 881
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getData()Ljava/lang/CharSequence;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 882
    .local v15, "paste":Ljava/lang/String;
    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 883
    .restart local v11    # "imagePath":Landroid/net/Uri;
    const/4 v14, 0x0

    .line 884
    .local v14, "orientation":I
    if-eqz v11, :cond_1

    .line 886
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    const-string v6, "\n \n"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v3, v6

    const/16 v6, 0x1000

    if-le v3, v6, :cond_13

    .line 887
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v3

    const v6, 0x7f0b0048

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_1

    .line 891
    :cond_13
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getContext()Landroid/content/Context;

    move-result-object v6

    # invokes: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getRotatedDegree(Landroid/net/Uri;Landroid/content/Context;)I
    invoke-static {v3, v11, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$7(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Landroid/net/Uri;Landroid/content/Context;)I

    move-result v14

    .line 892
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSession:Lcom/samsung/android/app/memo/Session;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$8(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Lcom/samsung/android/app/memo/Session;

    move-result-object v3

    const-string v6, ""

    const-string v7, "image/jpeg"

    invoke-virtual {v3, v11, v6, v7, v14}, Lcom/samsung/android/app/memo/Session;->addFile(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 896
    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-virtual {v3, v11, v14, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->insertImage(Landroid/net/Uri;ILjava/lang/String;)Z

    move-result v12

    .line 897
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPasteOperationState(Z)V

    goto/16 :goto_3

    .line 893
    :catch_0
    move-exception v16

    .line 894
    .local v16, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$9()Ljava/lang/String;

    move-result-object v3

    const-string v6, "insertImage()"

    move-object/from16 v0, v16

    invoke-static {v3, v6, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5
.end method

.method public onTextPaste(Ljava/lang/CharSequence;II)V
    .locals 21
    .param p1, "str"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 909
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    sget v6, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mMaxCharSize:I

    if-ne v3, v6, :cond_2

    const/4 v14, 0x1

    .line 910
    .local v14, "isLimitReached":Z
    :goto_0
    const/4 v3, 0x0

    sput-boolean v3, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mImagePasteFailed:Z

    .line 911
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$5(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Z)V

    .line 912
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPasteOperationState(Z)V

    .line 913
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mActivity:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->newInstance(Landroid/content/Context;)Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    move-result-object v9

    .line 914
    .local v9, "clipboardEXProxy":Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
    if-eqz v9, :cond_4

    .line 915
    invoke-virtual {v9}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->getFrozenState()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 916
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mActivity:Landroid/content/Context;

    const/4 v6, 0x4

    invoke-virtual {v9, v3, v6}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->getData(Landroid/content/Context;I)Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;

    move-result-object v10

    .line 917
    .local v10, "clipdataTemp":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    if-eqz v10, :cond_4

    .line 918
    invoke-virtual {v10}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getFormat()I

    move-result v3

    const/4 v6, 0x2

    if-ne v3, v6, :cond_8

    .line 919
    invoke-virtual {v10}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getData()Ljava/lang/CharSequence;

    move-result-object v18

    .line 920
    .local v18, "seq":Ljava/lang/CharSequence;
    if-eqz v18, :cond_0

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 921
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPasteOperationState(Z)V

    .line 1039
    .end local v10    # "clipdataTemp":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    .end local v18    # "seq":Ljava/lang/CharSequence;
    :cond_1
    :goto_1
    return-void

    .line 909
    .end local v9    # "clipboardEXProxy":Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
    .end local v14    # "isLimitReached":Z
    :cond_2
    const/4 v14, 0x0

    goto :goto_0

    .line 924
    .restart local v9    # "clipboardEXProxy":Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
    .restart local v10    # "clipdataTemp":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    .restart local v14    # "isLimitReached":Z
    .restart local v18    # "seq":Ljava/lang/CharSequence;
    :cond_3
    if-nez v14, :cond_7

    .line 925
    move/from16 v0, p2

    move/from16 v1, p3

    if-ne v0, v1, :cond_5

    if-lez p2, :cond_5

    .line 926
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    add-int/lit8 v6, p2, -0x1

    invoke-interface {v3, v6}, Landroid/text/Editable;->charAt(I)C

    move-result v3

    const/16 v6, 0x20

    if-eq v3, v6, :cond_5

    .line 927
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v3, v0, v1, v6}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 1034
    .end local v10    # "clipdataTemp":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    .end local v18    # "seq":Ljava/lang/CharSequence;
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPasteOperationState(Z)V

    .line 1035
    if-nez v14, :cond_1

    sget-boolean v3, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mImagePasteFailed:Z

    if-nez v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isMaxImageReached:Z
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$11(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v9}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isCalledPasteFromClipboard()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1036
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v3

    const v6, 0x7f0b00b1

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 1037
    invoke-virtual {v9}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->resetIsPastedFromTheClipboard()V

    goto :goto_1

    .line 929
    .restart local v10    # "clipdataTemp":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    .restart local v18    # "seq":Ljava/lang/CharSequence;
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getEditableText()Landroid/text/Editable;

    move-result-object v3

    .line 930
    const-class v6, Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v3, v0, v1, v6}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v19

    check-cast v19, [Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 931
    .local v19, "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    move-object/from16 v0, v19

    array-length v3, v0

    if-lez v3, :cond_6

    .line 932
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v20

    .line 933
    .local v20, "text":Landroid/text/Editable;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    const/4 v6, 0x0

    aget-object v6, v19, v6

    invoke-interface {v3, v6}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 934
    if-lez p2, :cond_6

    add-int/lit8 v3, p2, -0x1

    move-object/from16 v0, v20

    invoke-interface {v0, v3}, Landroid/text/Editable;->charAt(I)C

    move-result v3

    const/16 v6, 0xa

    if-ne v3, v6, :cond_6

    .line 935
    add-int/lit8 p2, p2, -0x1

    .line 937
    .end local v20    # "text":Landroid/text/Editable;
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    move/from16 v0, p2

    move/from16 v1, p3

    move-object/from16 v2, v18

    invoke-interface {v3, v0, v1, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_2

    .line 940
    .end local v19    # "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v3

    .line 941
    const v6, 0x7f0b0048

    .line 940
    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_2

    .line 943
    .end local v18    # "seq":Ljava/lang/CharSequence;
    :cond_8
    invoke-virtual {v10}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getFormat()I

    move-result v3

    const/4 v6, 0x3

    if-ne v3, v6, :cond_a

    .line 944
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "file://"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getData()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 945
    .local v13, "imagePath":Landroid/net/Uri;
    const/4 v15, 0x0

    .line 946
    .local v15, "orientation":I
    if-eqz v13, :cond_1

    .line 949
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getContext()Landroid/content/Context;

    move-result-object v6

    # invokes: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getRotatedDegree(Landroid/net/Uri;Landroid/content/Context;)I
    invoke-static {v3, v13, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$7(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Landroid/net/Uri;Landroid/content/Context;)I

    move-result v15

    .line 950
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSession:Lcom/samsung/android/app/memo/Session;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$8(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Lcom/samsung/android/app/memo/Session;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 951
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSession:Lcom/samsung/android/app/memo/Session;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$8(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Lcom/samsung/android/app/memo/Session;

    move-result-object v3

    const-string v6, ""

    .line 952
    const-string v7, "image/jpeg"

    .line 951
    invoke-virtual {v3, v13, v6, v7, v15}, Lcom/samsung/android/app/memo/Session;->addFile(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 957
    :cond_9
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-virtual {v3, v13, v15, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->insertImage(Landroid/net/Uri;ILjava/lang/String;)Z

    goto/16 :goto_2

    .line 954
    :catch_0
    move-exception v17

    .line 955
    .local v17, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$9()Ljava/lang/String;

    move-result-object v3

    const-string v6, "insertImage()"

    move-object/from16 v0, v17

    invoke-static {v3, v6, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 958
    .end local v13    # "imagePath":Landroid/net/Uri;
    .end local v15    # "orientation":I
    .end local v17    # "se":Lcom/samsung/android/app/memo/Session$SessionException;
    :cond_a
    invoke-virtual {v10}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getFormat()I

    move-result v3

    const/4 v6, 0x4

    if-ne v3, v6, :cond_11

    .line 959
    new-instance v5, Landroid/text/SpannableStringBuilder;

    .line 960
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 959
    invoke-direct {v5, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 961
    .local v5, "spn":Landroid/text/SpannableStringBuilder;
    invoke-virtual {v10}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getData()Ljava/lang/CharSequence;

    move-result-object v18

    .line 962
    .restart local v18    # "seq":Ljava/lang/CharSequence;
    if-eqz v18, :cond_b

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 963
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPasteOperationState(Z)V

    goto/16 :goto_1

    .line 966
    :cond_c
    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 968
    .local v4, "HtmlStr":Ljava/lang/String;
    const/4 v11, 0x0

    .line 969
    .local v11, "count":I
    :goto_4
    const-string v3, "\n"

    invoke-virtual {v4, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 977
    :cond_d
    if-nez v14, :cond_10

    .line 978
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v8, 0x1

    move/from16 v6, p2

    move/from16 v7, p3

    # invokes: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->addHtmlText(Ljava/lang/String;Landroid/text/SpannableStringBuilder;IIZ)V
    invoke-static/range {v3 .. v8}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$6(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/lang/String;Landroid/text/SpannableStringBuilder;IIZ)V

    goto/16 :goto_2

    .line 970
    :cond_e
    const/4 v3, 0x2

    if-gt v11, v3, :cond_d

    .line 972
    const-string v3, "\n"

    invoke-virtual {v4, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v12

    .line 973
    .local v12, "endLineIndex":I
    if-lez v12, :cond_f

    .line 974
    const/4 v3, 0x0

    invoke-virtual {v4, v3, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 975
    :cond_f
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 980
    .end local v12    # "endLineIndex":I
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v3

    const v6, 0x7f0b0048

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_2

    .line 981
    .end local v4    # "HtmlStr":Ljava/lang/String;
    .end local v5    # "spn":Landroid/text/SpannableStringBuilder;
    .end local v11    # "count":I
    .end local v18    # "seq":Ljava/lang/CharSequence;
    :cond_11
    invoke-virtual {v10}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getFormat()I

    move-result v3

    const/4 v6, 0x5

    if-ne v3, v6, :cond_4

    .line 982
    invoke-virtual {v10}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getData()Ljava/lang/CharSequence;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 983
    .local v16, "paste":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 984
    .restart local v13    # "imagePath":Landroid/net/Uri;
    const/4 v15, 0x0

    .line 985
    .restart local v15    # "orientation":I
    if-eqz v13, :cond_1

    .line 987
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    const-string v6, "\n \n"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v3, v6

    const/16 v6, 0x1000

    if-le v3, v6, :cond_12

    .line 988
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v3

    const v6, 0x7f0b0048

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_1

    .line 992
    :cond_12
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getContext()Landroid/content/Context;

    move-result-object v6

    # invokes: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getRotatedDegree(Landroid/net/Uri;Landroid/content/Context;)I
    invoke-static {v3, v13, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$7(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Landroid/net/Uri;Landroid/content/Context;)I

    move-result v15

    .line 993
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSession:Lcom/samsung/android/app/memo/Session;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$8(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Lcom/samsung/android/app/memo/Session;

    move-result-object v3

    const-string v6, ""

    const-string v7, "image/jpeg"

    invoke-virtual {v3, v13, v6, v7, v15}, Lcom/samsung/android/app/memo/Session;->addFile(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;
    :try_end_1
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v13

    .line 997
    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v6, 0x0

    invoke-virtual {v3, v13, v15, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->insertImage(Landroid/net/Uri;ILjava/lang/String;)Z

    goto/16 :goto_2

    .line 994
    :catch_1
    move-exception v17

    .line 995
    .restart local v17    # "se":Lcom/samsung/android/app/memo/Session$SessionException;
    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$9()Ljava/lang/String;

    move-result-object v3

    const-string v6, "insertImage()"

    move-object/from16 v0, v17

    invoke-static {v3, v6, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 1003
    .end local v10    # "clipdataTemp":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    .end local v13    # "imagePath":Landroid/net/Uri;
    .end local v15    # "orientation":I
    .end local v16    # "paste":Ljava/lang/String;
    .end local v17    # "se":Lcom/samsung/android/app/memo/Session$SessionException;
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mActivity:Landroid/content/Context;

    const/4 v6, 0x4

    invoke-virtual {v9, v3, v6}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->getData(Landroid/content/Context;I)Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;

    move-result-object v10

    .line 1004
    .restart local v10    # "clipdataTemp":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    if-eqz v10, :cond_4

    .line 1005
    invoke-virtual {v10}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getFormat()I

    move-result v3

    const/4 v6, 0x2

    if-ne v3, v6, :cond_14

    .line 1006
    invoke-virtual {v10}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getData()Ljava/lang/CharSequence;

    move-result-object v18

    .line 1007
    .restart local v18    # "seq":Ljava/lang/CharSequence;
    if-eqz v18, :cond_1

    .line 1009
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    move/from16 v0, p2

    move/from16 v1, p3

    move-object/from16 v2, v18

    invoke-interface {v3, v0, v1, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto/16 :goto_2

    .line 1010
    .end local v18    # "seq":Ljava/lang/CharSequence;
    :cond_14
    invoke-virtual {v10}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getFormat()I

    move-result v3

    const/4 v6, 0x4

    if-ne v3, v6, :cond_4

    .line 1011
    new-instance v5, Landroid/text/SpannableStringBuilder;

    .line 1012
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 1011
    invoke-direct {v5, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1013
    .restart local v5    # "spn":Landroid/text/SpannableStringBuilder;
    invoke-virtual {v10}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->getData()Ljava/lang/CharSequence;

    move-result-object v18

    .line 1014
    .restart local v18    # "seq":Ljava/lang/CharSequence;
    if-eqz v18, :cond_1

    .line 1016
    invoke-interface/range {v18 .. v18}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1017
    .restart local v4    # "HtmlStr":Ljava/lang/String;
    const/4 v11, 0x0

    .line 1018
    .restart local v11    # "count":I
    :goto_6
    const-string v3, "\n"

    invoke-virtual {v4, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_16

    .line 1026
    :cond_15
    if-nez v14, :cond_18

    .line 1027
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v8, 0x1

    move/from16 v6, p2

    move/from16 v7, p3

    # invokes: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->addHtmlText(Ljava/lang/String;Landroid/text/SpannableStringBuilder;IIZ)V
    invoke-static/range {v3 .. v8}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$6(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/lang/String;Landroid/text/SpannableStringBuilder;IIZ)V

    goto/16 :goto_2

    .line 1019
    :cond_16
    const/4 v3, 0x2

    if-gt v11, v3, :cond_15

    .line 1021
    const-string v3, "\n"

    invoke-virtual {v4, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v12

    .line 1022
    .restart local v12    # "endLineIndex":I
    if-lez v12, :cond_17

    .line 1023
    const/4 v3, 0x0

    invoke-virtual {v4, v3, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1024
    :cond_17
    add-int/lit8 v11, v11, 0x1

    goto :goto_6

    .line 1029
    .end local v12    # "endLineIndex":I
    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v3

    const v6, 0x7f0b0048

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_2
.end method
