.class Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$3;
.super Ljava/lang/Object;
.source "SNBDownloader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->showNotification()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

.field private final synthetic val$lock:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$3;->this$0:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    iput-object p2, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$3;->val$lock:Ljava/lang/Object;

    .line 331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const v11, 0x7f0b005b

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 334
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/16 v6, 0x100

    invoke-virtual {v5, v6}, Landroid/view/Window;->addFlags(I)V

    .line 335
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/16 v6, 0x200

    invoke-virtual {v5, v6}, Landroid/view/Window;->addFlags(I)V

    .line 336
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "T "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v6

    const/high16 v7, 0x7f0b0000

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 337
    .local v0, "TMemo":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$3;->this$0:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    new-instance v6, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 339
    const v7, 0x7f0200b7

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 340
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 341
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v0, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    .line 337
    invoke-static {v5, v6}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->access$6(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;Landroid/support/v4/app/NotificationCompat$Builder;)V

    .line 342
    new-instance v2, Landroid/content/Intent;

    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v5

    const-class v6, Lcom/samsung/android/app/memo/Main;

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 343
    .local v2, "resultIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Landroid/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/app/TaskStackBuilder;

    move-result-object v4

    .line 344
    .local v4, "stackBuilder":Landroid/app/TaskStackBuilder;
    const-class v5, Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v4, v5}, Landroid/app/TaskStackBuilder;->addParentStack(Ljava/lang/Class;)Landroid/app/TaskStackBuilder;

    .line 345
    invoke-virtual {v4, v2}, Landroid/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/app/TaskStackBuilder;

    .line 346
    const/high16 v5, 0x8000000

    invoke-virtual {v4, v9, v5}, Landroid/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v3

    .line 347
    .local v3, "resultPendingIntent":Landroid/app/PendingIntent;
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$3;->this$0:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    # getter for: Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotifBuilder:Landroid/support/v4/app/NotificationCompat$Builder;
    invoke-static {v5}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->access$7(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 348
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$3;->this$0:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v5

    const-string v7, "notification"

    invoke-virtual {v5, v7}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    invoke-static {v6, v5}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->access$8(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;Landroid/app/NotificationManager;)V

    .line 349
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$3;->this$0:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    # getter for: Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotificationManager:Landroid/app/NotificationManager;
    invoke-static {v5}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->access$9(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;)Landroid/app/NotificationManager;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$3;->this$0:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    # getter for: Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotifBuilder:Landroid/support/v4/app/NotificationCompat$Builder;
    invoke-static {v6}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->access$7(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 351
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 352
    .local v1, "attrs":Landroid/view/WindowManager$LayoutParams;
    iget v5, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v5, v5, -0x401

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 353
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 355
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    new-instance v6, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$3$1;

    iget-object v7, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$3;->val$lock:Ljava/lang/Object;

    invoke-direct {v6, p0, v7}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$3$1;-><init>(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$3;Ljava/lang/Object;)V

    .line 365
    const-wide/16 v8, 0x7d0

    .line 355
    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 366
    return-void
.end method
