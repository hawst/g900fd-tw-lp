.class public final Lcom/samsung/android/app/memo/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Camera:I = 0x7f0b00b3

.field public static final IDS_MH_POP_FILE_DOES_NOT_EXIST:I = 0x7f0b005a

.field public static final SS_AUDIO_FORMAT_NOT_SUPPORTED_ABB:I = 0x7f0b00b0

.field public static final SS_MAXIMUM_NUMBER_OF_IMAGES_REACHED:I = 0x7f0b0042

.field public static final SS_VIDEO_FORMAT_NOT_SUPPORTED_ABB:I = 0x7f0b00af

.field public static final VNT_Corrupt:I = 0x7f0b0013

.field public static final about_this_service:I = 0x7f0b00b8

.field public static final account:I = 0x7f0b002a

.field public static final add_category:I = 0x7f0b001c

.field public static final add_event:I = 0x7f0b0065

.field public static final add_to_bookmark:I = 0x7f0b0061

.field public static final add_to_calendar_message:I = 0x7f0b0068

.field public static final add_to_calendar_title:I = 0x7f0b0067

.field public static final afternoon:I = 0x7f0b008e

.field public static final already_exists:I = 0x7f0b00a7

.field public static final app_description:I = 0x7f0b0010

.field public static final app_name:I = 0x7f0b0000

.field public static final backing_up_memos_for_kies:I = 0x7f0b0053

.field public static final cancel:I = 0x7f0b0006

.field public static final cancel_full:I = 0x7f0b0005

.field public static final cancel_recording:I = 0x7f0b009a

.field public static final cancel_voice_recording:I = 0x7f0b009b

.field public static final cannot_recording_incalling:I = 0x7f0b0097

.field public static final category_all:I = 0x7f0b0017

.field public static final category_already_in_use:I = 0x7f0b0020

.field public static final category_duringcall:I = 0x7f0b0018

.field public static final category_name:I = 0x7f0b0019

.field public static final check:I = 0x7f0b003d

.field public static final clipboard_saved_textmemo:I = 0x7f0b00a8

.field public static final completed:I = 0x7f0b0054

.field public static final converting_dot_dot_dot:I = 0x7f0b0051

.field public static final copy:I = 0x7f0b00be

.field public static final copy_text:I = 0x7f0b0066

.field public static final create_category:I = 0x7f0b001b

.field public static final create_memo:I = 0x7f0b0029

.field public static final day01:I = 0x7f0b0083

.field public static final day02:I = 0x7f0b0084

.field public static final day03:I = 0x7f0b0085

.field public static final day04:I = 0x7f0b0086

.field public static final day05:I = 0x7f0b0087

.field public static final day06:I = 0x7f0b0088

.field public static final day07:I = 0x7f0b0089

.field public static final delete:I = 0x7f0b0002

.field public static final delete_categories:I = 0x7f0b001f

.field public static final deleted:I = 0x7f0b005d

.field public static final discard:I = 0x7f0b0004

.field public static final discard_close:I = 0x7f0b00b6

.field public static final do_not_show_again:I = 0x7f0b002e

.field public static final done:I = 0x7f0b0008

.field public static final download:I = 0x7f0b002f

.field public static final drawer_close:I = 0x7f0b0025

.field public static final drawer_open:I = 0x7f0b0024

.field public static final drop_not_supported:I = 0x7f0b00ae

.field public static final edit_categories:I = 0x7f0b001d

.field public static final editor_hint:I = 0x7f0b0040

.field public static final empty_memo:I = 0x7f0b000b

.field public static final enter_category:I = 0x7f0b001a

.field public static final evening:I = 0x7f0b008f

.field public static final failed:I = 0x7f0b0050

.field public static final failed_to_insert_image:I = 0x7f0b0041

.field public static final filetype_not_supported:I = 0x7f0b00a9

.field public static final general_error:I = 0x7f0b000a

.field public static final goto_maps:I = 0x7f0b0064

.field public static final import_from_tmemo:I = 0x7f0b005b

.field public static final import_tmemo:I = 0x7f0b002b

.field public static final import_tmemo_dialog:I = 0x7f0b002c

.field public static final import_tmemo_dialog_additional:I = 0x7f0b002d

.field public static final insert:I = 0x7f0b0046

.field public static final invalid_pwd:I = 0x7f0b004a

.field public static final items_moved:I = 0x7f0b00bc

.field public static final low_batt_msg:I = 0x7f0b00bd

.field public static final manage_categories:I = 0x7f0b0021

.field public static final memo_send_not_supported:I = 0x7f0b0059

.field public static final memo_will_discard:I = 0x7f0b0092

.field public static final menu_add_to_contacts:I = 0x7f0b0062

.field public static final menu_call:I = 0x7f0b0060

.field public static final menu_deselect_all:I = 0x7f0b0039

.field public static final menu_select_all:I = 0x7f0b0037

.field public static final menu_select_none:I = 0x7f0b0038

.field public static final migrating_memos_from_kies:I = 0x7f0b0052

.field public static final month01:I = 0x7f0b006b

.field public static final month01_abb:I = 0x7f0b0077

.field public static final month02:I = 0x7f0b006c

.field public static final month02_abb:I = 0x7f0b0078

.field public static final month03:I = 0x7f0b006d

.field public static final month03_abb:I = 0x7f0b0079

.field public static final month04:I = 0x7f0b006e

.field public static final month04_abb:I = 0x7f0b007a

.field public static final month05:I = 0x7f0b006f

.field public static final month05_abb:I = 0x7f0b007b

.field public static final month06:I = 0x7f0b0070

.field public static final month06_abb:I = 0x7f0b007c

.field public static final month07:I = 0x7f0b0071

.field public static final month07_abb:I = 0x7f0b007d

.field public static final month08:I = 0x7f0b0072

.field public static final month08_abb:I = 0x7f0b007e

.field public static final month09:I = 0x7f0b0073

.field public static final month09_abb:I = 0x7f0b007f

.field public static final month10:I = 0x7f0b0074

.field public static final month10_abb:I = 0x7f0b0080

.field public static final month11:I = 0x7f0b0075

.field public static final month11_abb:I = 0x7f0b0081

.field public static final month12:I = 0x7f0b0076

.field public static final month12_abb:I = 0x7f0b0082

.field public static final morining:I = 0x7f0b008d

.field public static final move_btn:I = 0x7f0b0057

.field public static final move_category:I = 0x7f0b00ba

.field public static final move_completed:I = 0x7f0b00bb

.field public static final move_file:I = 0x7f0b0056

.field public static final moving_file_dialog:I = 0x7f0b0055

.field public static final network_connection_unavailable:I = 0x7f0b004f

.field public static final new_memo:I = 0x7f0b0043

.field public static final night:I = 0x7f0b0090

.field public static final no:I = 0x7f0b006a

.field public static final no_categories:I = 0x7f0b0014

.field public static final no_categories_popup:I = 0x7f0b0015

.field public static final no_free_space:I = 0x7f0b004e

.field public static final no_memos:I = 0x7f0b0026

.field public static final no_memos_popup:I = 0x7f0b0016

.field public static final no_text:I = 0x7f0b003c

.field public static final none:I = 0x7f0b000d

.field public static final not_supported:I = 0x7f0b0058

.field public static final ok:I = 0x7f0b0047

.field public static final open_source_licenses:I = 0x7f0b00b7

.field public static final open_url:I = 0x7f0b005f

.field public static final page:I = 0x7f0b000f

.field public static final pasted_from_clipboard:I = 0x7f0b00b1

.field public static final pick_image:I = 0x7f0b0044

.field public static final please_wait:I = 0x7f0b00ab

.field public static final print:I = 0x7f0b0036

.field public static final processing:I = 0x7f0b000e

.field public static final read_permission:I = 0x7f0b0011

.field public static final recording_failed:I = 0x7f0b0095

.field public static final recording_failed_popup_text:I = 0x7f0b0098

.field public static final recording_will_not_saved:I = 0x7f0b009c

.field public static final rename:I = 0x7f0b0003

.field public static final rename_category:I = 0x7f0b001e

.field public static final samsung_account:I = 0x7f0b0031

.field public static final samsung_account_text:I = 0x7f0b0032

.field public static final save:I = 0x7f0b0007

.field public static final saved:I = 0x7f0b000c

.field public static final search:I = 0x7f0b0027

.field public static final search_hint:I = 0x7f0b009d

.field public static final select:I = 0x7f0b0001

.field public static final selected:I = 0x7f0b003a

.field public static final send_email:I = 0x7f0b005e

.field public static final send_message:I = 0x7f0b0063

.field public static final settings_description:I = 0x7f0b009e

.field public static final share_via:I = 0x7f0b0009

.field public static final skip:I = 0x7f0b0030

.field public static final ss_maximum_number_of_characters_exceeded:I = 0x7f0b0048

.field public static final ss_not_enough_space_to_add_more_content:I = 0x7f0b0049

.field public static final ss_the_maximum_number_of_files_that_can_be_shared_msg:I = 0x7f0b00bf

.field public static final ss_unable_to_create_memo_not_enough_memory:I = 0x7f0b00b9

.field public static final ss_view_contact_info:I = 0x7f0b0035

.field public static final stms_appgroup:I = 0x7f0b00c0

.field public static final string_category:I = 0x7f0b00a0

.field public static final string_check:I = 0x7f0b00aa

.field public static final string_edit:I = 0x7f0b009f

.field public static final string_no_search_result:I = 0x7f0b0028

.field public static final string_pause:I = 0x7f0b00a5

.field public static final string_play:I = 0x7f0b00a4

.field public static final string_record:I = 0x7f0b00a3

.field public static final string_recording:I = 0x7f0b00a1

.field public static final string_select_all:I = 0x7f0b003e

.field public static final string_stop:I = 0x7f0b00a6

.field public static final string_unable_to_start_new_recording_desc:I = 0x7f0b0096

.field public static final sync_memo:I = 0x7f0b0033

.field public static final sync_memo_text:I = 0x7f0b0034

.field public static final take_picture:I = 0x7f0b0045

.field public static final tap_to_add_memo:I = 0x7f0b00ad

.field public static final tb_navigate_up:I = 0x7f0b0022

.field public static final tb_reorder_category:I = 0x7f0b0023

.field public static final text_pasted_from_clipboard:I = 0x7f0b00b2

.field public static final thumb_image:I = 0x7f0b003b

.field public static final time_am:I = 0x7f0b00b4

.field public static final time_pm:I = 0x7f0b00b5

.field public static final title_hint:I = 0x7f0b003f

.field public static final today:I = 0x7f0b008a

.field public static final tomorrow:I = 0x7f0b008c

.field public static final tonight:I = 0x7f0b008b

.field public static final unable_play:I = 0x7f0b0093

.field public static final unable_to_perform_this_action:I = 0x7f0b00c1

.field public static final unable_to_play_audio:I = 0x7f0b0094

.field public static final undo:I = 0x7f0b005c

.field public static final unlock:I = 0x7f0b004b

.field public static final unlocking_enter_pwd:I = 0x7f0b004c

.field public static final unlocking_pwd:I = 0x7f0b004d

.field public static final voice_memo:I = 0x7f0b00a2

.field public static final voice_recorded:I = 0x7f0b00ac

.field public static final voice_will_delete:I = 0x7f0b0091

.field public static final voiceerror_microphone:I = 0x7f0b0099

.field public static final write_permission:I = 0x7f0b0012

.field public static final yes:I = 0x7f0b0069


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
