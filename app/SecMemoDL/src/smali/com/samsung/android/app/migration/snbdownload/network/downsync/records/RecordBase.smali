.class public Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;
.super Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;
.source "RecordBase.java"


# static fields
.field protected static final KEY:Ljava/lang/String; = "key"

.field protected static final TIMESTAMP:Ljava/lang/String; = "timestamp"


# instance fields
.field protected mKey:Ljava/lang/String;

.field protected mTimeStamp:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "timeStamp"    # Ljava/lang/Long;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;->mKey:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;->mTimeStamp:Ljava/lang/Long;

    .line 43
    return-void
.end method


# virtual methods
.method public fromJSON(Ljava/lang/Object;)V
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 46
    move-object v0, p1

    check-cast v0, Lorg/json/JSONObject;

    .line 47
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "key"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    const-string v1, "key"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;->mKey:Ljava/lang/String;

    .line 49
    :cond_0
    const-string v1, "timestamp"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 50
    const-string v1, "timestamp"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;->mTimeStamp:Ljava/lang/Long;

    .line 51
    :cond_1
    return-void
.end method

.method public getKEY()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeStamp()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;->mTimeStamp:Ljava/lang/Long;

    return-object v0
.end method

.method public toJSON()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;->mKey:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;->mTimeStamp:Ljava/lang/Long;

    if-nez v1, :cond_1

    .line 55
    :cond_0
    new-instance v1, Lorg/json/JSONException;

    const-string v2, "RecordItemWithResponse :Input parsing error"

    invoke-direct {v1, v2}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 57
    :cond_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 58
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "key"

    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 59
    const-string v1, "timestamp"

    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;->mTimeStamp:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 60
    return-object v0
.end method
