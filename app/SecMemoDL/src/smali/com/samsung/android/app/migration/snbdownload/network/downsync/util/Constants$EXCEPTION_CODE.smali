.class public interface abstract Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/Constants$EXCEPTION_CODE;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EXCEPTION_CODE"
.end annotation


# static fields
.field public static final AUTHENTICATION:I = 0x3

.field public static final HTTP:I = 0x1

.field public static final IO_ERROR:I = 0x4

.field public static final JSON:I = 0x2

.field public static final OTHER:I = 0x6

.field public static final SDCARD_NOT_MOUNTED:I = 0x8

.field public static final SERVER_AUTHENTICATION:I = 0x7

.field public static final STORAGE_FULL:I = 0x5

.field public static final SYNC_CANCELLED:I = 0x9
