.class public interface abstract Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;
.super Ljava/lang/Object;
.source "VoiceRecorderBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RecordListener"
.end annotation


# virtual methods
.method public abstract onFailRecording()V
.end method

.method public abstract onForceStop()V
.end method

.method public abstract onPauseRecording()V
.end method

.method public abstract onStartRecording(Lcom/samsung/android/app/memo/voice/VoiceInfo;)V
.end method

.method public abstract onStopRecording()V
.end method
