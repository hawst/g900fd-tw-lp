.class public Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;
.super Ljava/lang/Thread;
.source "ConvertSNBClickTask.java"


# instance fields
.field private alertDialog:Landroid/app/AlertDialog;

.field private memoId:J

.field private snbPath:Ljava/lang/String;

.field private snbTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "snbTitle"    # Ljava/lang/String;
    .param p2, "snbPath"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbTitle:Ljava/lang/String;

    .line 49
    iput-object p2, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbPath:Ljava/lang/String;

    .line 50
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 51
    const v1, 0x7f0b0056

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b0055

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 52
    const v1, 0x7f0b0057

    new-instance v2, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask$1;-><init>(Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 59
    const v1, 0x7f0b0005

    new-instance v2, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask$2;-><init>(Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 64
    new-instance v1, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask$3;-><init>(Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 50
    iput-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->alertDialog:Landroid/app/AlertDialog;

    .line 70
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->alertDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->convertMemo()V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->openMemo()V

    return-void
.end method

.method private convertMemo()V
    .locals 3

    .prologue
    .line 73
    invoke-static {}, Lcom/samsung/android/app/migration/MemoConverter;->getInstance()Lcom/samsung/android/app/migration/MemoConverter;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/migration/MemoConverter;->convertParsedSNB(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->memoId:J

    .line 74
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/Migration;->resetPassword()V

    .line 75
    iget-wide v0, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->memoId:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->handleConvertSNBResult(J)V

    .line 76
    return-void
.end method

.method private handleConvertSNBResult(J)V
    .locals 3
    .param p1, "resId"    # J

    .prologue
    .line 79
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 80
    const v0, 0x7f0b0050

    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->toast(I)V

    .line 81
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/SNBClickActivity;->finish()V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    const-wide/16 v0, -0x2

    cmp-long v0, p1, v0

    if-nez v0, :cond_3

    .line 83
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbPath:Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/FileHelper;->deleteFileItem(Ljava/io/File;)V

    .line 85
    :cond_2
    const v0, 0x7f0b0058

    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->toast(I)V

    .line 86
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/SNBClickActivity;->finish()V

    goto :goto_0

    .line 87
    :cond_3
    const-wide/16 v0, -0x3

    cmp-long v0, p1, v0

    if-nez v0, :cond_4

    .line 88
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbPath:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/app/migration/utils/Utils;->getFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 89
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbPath:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/app/migration/utils/Utils;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 88
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/FileHelper;->deleteFileItem(Ljava/io/File;)V

    .line 90
    const v0, 0x7f0b004a

    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->toast(I)V

    .line 91
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/SNBClickActivity;->finish()V

    goto :goto_0

    .line 92
    :cond_4
    const-wide/16 v0, -0x4

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 93
    const v0, 0x7f0b004e

    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->toast(I)V

    .line 94
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/SNBClickActivity;->finish()V

    goto :goto_0
.end method

.method private isContextAlive(Landroid/app/Activity;)Z
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 141
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 142
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    const/4 v0, 0x1

    .line 145
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private openMemo()V
    .locals 4

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->memoId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 111
    invoke-static {}, Lcom/samsung/android/app/migration/MemoConverter;->getInstance()Lcom/samsung/android/app/migration/MemoConverter;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->memoId:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/migration/MemoConverter;->openMemo(Landroid/content/Context;J)V

    .line 112
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbPath:Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/FileHelper;->deleteFileItem(Ljava/io/File;)V

    .line 114
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/SNBClickActivity;->finish()V

    .line 116
    :cond_1
    return-void
.end method

.method private openMemoOrDialog()V
    .locals 4

    .prologue
    .line 119
    invoke-static {}, Lcom/samsung/android/app/migration/MemoConverter;->getInstance()Lcom/samsung/android/app/migration/MemoConverter;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/migration/MemoConverter;->parseSNB(Ljava/lang/String;)J

    move-result-wide v0

    .line 120
    .local v0, "resId":J
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->handleConvertSNBResult(J)V

    .line 121
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 122
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 123
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbPath:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/app/migration/utils/Utils;->getFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/app/migration/Migration;->SNB_CLICK_DEFAULT_PATH:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 124
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbPath:Ljava/lang/String;

    const-string v3, "com.google.android.apps.docs"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 125
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->convertMemo()V

    .line 126
    invoke-direct {p0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->openMemo()V

    .line 138
    :cond_1
    :goto_0
    return-void

    .line 128
    :cond_2
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->isContextAlive(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 129
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask$5;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask$5;-><init>(Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/migration/SNBClickActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private toast(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 99
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->isContextAlive(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask$4;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask$4;-><init>(Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;I)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/SNBClickActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 107
    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 150
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/MemoDataHandler;->isMemoExist(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->memoId:J

    .line 151
    iget-wide v2, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->memoId:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_3

    .line 152
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbPath:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsung/android/app/memo/util/FileHelper;->isEncrypted(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 154
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v1

    .line 155
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v2

    const v3, 0x7f0b004b

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/migration/SNBClickActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->snbTitle:Ljava/lang/String;

    .line 153
    invoke-static {v1, v2, v3}, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->newInstance(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/app/migration/dialog/PasswordDialog;

    move-result-object v0

    .line 156
    .local v0, "pwdDialog":Lcom/samsung/android/app/migration/dialog/PasswordDialog;
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->isContextAlive(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask$6;

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask$6;-><init>(Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;Lcom/samsung/android/app/migration/dialog/PasswordDialog;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/migration/SNBClickActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 166
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v1

    iget-object v2, v1, Lcom/samsung/android/app/migration/Migration;->passwordLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :try_start_1
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/app/migration/Migration;->passwordLock:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 166
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 171
    :goto_0
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/migration/Migration;->getPassword()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 172
    invoke-direct {p0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->openMemoOrDialog()V

    .line 179
    .end local v0    # "pwdDialog":Lcom/samsung/android/app/migration/dialog/PasswordDialog;
    :goto_1
    return-void

    .line 166
    .restart local v0    # "pwdDialog":Lcom/samsung/android/app/migration/dialog/PasswordDialog;
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 169
    :catch_0
    move-exception v1

    goto :goto_0

    .line 174
    :cond_1
    invoke-static {}, Lcom/samsung/android/app/migration/SNBClickActivity;->getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/migration/SNBClickActivity;->finish()V

    goto :goto_1

    .line 176
    .end local v0    # "pwdDialog":Lcom/samsung/android/app/migration/dialog/PasswordDialog;
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->openMemoOrDialog()V

    goto :goto_1

    .line 178
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->openMemo()V

    goto :goto_1
.end method
