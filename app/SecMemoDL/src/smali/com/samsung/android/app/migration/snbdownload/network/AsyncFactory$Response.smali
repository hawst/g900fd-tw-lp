.class public Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;
.super Ljava/lang/Object;
.source "AsyncFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Response"
.end annotation


# instance fields
.field private authinfo:Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;

.field isfailed:Z

.field private req_type:I


# direct methods
.method constructor <init>(Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;I)V
    .locals 1
    .param p1, "authinfo"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;
    .param p2, "req_type"    # I

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->authinfo:Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->isfailed:Z

    .line 92
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->setAuthinfo(Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;)V

    .line 93
    invoke-virtual {p0, p2}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->setReq_type(I)V

    .line 94
    return-void
.end method


# virtual methods
.method public getAuthinfo()Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->authinfo:Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;

    return-object v0
.end method

.method public getReq_type()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->req_type:I

    return v0
.end method

.method public final setAuthinfo(Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;)V
    .locals 0
    .param p1, "authinfo"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->authinfo:Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;

    .line 110
    return-void
.end method

.method public final setReq_type(I)V
    .locals 0
    .param p1, "req_type"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;->req_type:I

    .line 102
    return-void
.end method
