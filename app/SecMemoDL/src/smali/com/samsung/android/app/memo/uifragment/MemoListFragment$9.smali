.class Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;
.super Ljava/lang/Object;
.source "MemoListFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    .line 1264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 1267
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1289
    :goto_0
    return-void

    .line 1270
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0e0026

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    .line 1272
    .local v0, "drawerLayout":Landroid/support/v4/widget/DrawerLayout;
    if-eqz v0, :cond_2

    const/4 v2, 0x3

    :try_start_0
    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$36(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->isNeedDrawerOpen()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1273
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    const/high16 v4, 0x7f0b0000

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SetCategoryTitle(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$55(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1286
    :catch_0
    move-exception v1

    .line 1287
    .local v1, "e":Ljava/lang/NullPointerException;
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$1()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NPE occured!!"

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1279
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$19(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1280
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$12(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/app/Activity;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$19(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;

    move-result-object v4

    .line 1281
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$56(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;

    move-result-object v5

    .line 1280
    invoke-static {v2, v4, v5}, Lcom/samsung/android/app/memo/util/Utils;->getCategoryType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    .line 1281
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    const v4, 0x7f0b000d

    invoke-virtual {v2, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1280
    :goto_1
    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SetCategoryTitleWithNum(Ljava/lang/String;)V
    invoke-static {v3, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$57(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 1282
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    const v4, 0x7f0b0017

    invoke-virtual {v2, v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 1284
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$9;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryTitle:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$56(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->SetCategoryTitleWithNum(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$57(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
