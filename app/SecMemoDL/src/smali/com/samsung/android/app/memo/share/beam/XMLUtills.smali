.class public Lcom/samsung/android/app/memo/share/beam/XMLUtills;
.super Ljava/lang/Object;
.source "XMLUtills.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/share/beam/XMLUtills$XMLUtillsOnDoneParsing;
    }
.end annotation


# static fields
.field public static APP_FOLDER:Ljava/lang/String; = null

.field private static EXT:Ljava/lang/String; = null

.field public static NFC_DIR:Ljava/lang/String; = null

.field private static final PACKAGE_NAME_ABEAM:Ljava/lang/String; = "com.android.nfc"

.field private static final PACKAGE_NAME_SBEAM:Ljava/lang/String; = "com.sec.android.directshare"

.field private static final RESULT_FAIL:I = -0x1

.field private static final RESULT_SUCCESS:I

.field public static ROOTFOLDER:Ljava/lang/String;

.field private static TAG:Ljava/lang/String;

.field private static XML_TAG_CONTENTS:Ljava/lang/String;

.field private static XML_TAG_CONTENTS_CONTENT:Ljava/lang/String;

.field private static XML_TAG_CONTENTS_VOICE_CONTENT:Ljava/lang/String;

.field private static XML_TAG_HEADER:Ljava/lang/String;

.field private static XML_TAG_MEMO:Ljava/lang/String;

.field private static XML_TAG_MEMO_META:Ljava/lang/String;

.field private static XML_TAG_MEMO_META_CREATEDTIME:Ljava/lang/String;

.field private static XML_TAG_MEMO_META_TITLE:Ljava/lang/String;

.field private static XML_TAG_MEMO_META_UUID:Ljava/lang/String;

.field private static XML_TAG_MEMO_VERSION:Ljava/lang/String;


# instance fields
.field private mContent:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDestFile:Ljava/lang/String;

.field private mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

.field private mHandler:Landroid/os/Handler;

.field private mMediaTempath:Ljava/lang/String;

.field private mProgress:Landroid/app/ProgressDialog;

.field private mRootDir:Ljava/io/File;

.field private mRootTempDir:Ljava/io/File;

.field private mTempUnZipPath:Ljava/lang/String;

.field private mTempZipPath:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mUUID:Ljava/lang/String;

.field private mUnzipHandler:Landroid/os/Handler;

.field private mZipHandler:Landroid/os/Handler;

.field private memofile:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 66
    const-string v0, "XMLUtills"

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->ROOTFOLDER:Ljava/lang/String;

    .line 72
    const-string v0, "BeamMemo"

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->APP_FOLDER:Ljava/lang/String;

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->ROOTFOLDER:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->APP_FOLDER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->NFC_DIR:Ljava/lang/String;

    .line 74
    const-string v0, ".memo"

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->EXT:Ljava/lang/String;

    .line 78
    const-string v0, "memo"

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO:Ljava/lang/String;

    .line 79
    const-string v0, "Version"

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_VERSION:Ljava/lang/String;

    .line 80
    const-string v0, "header"

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_HEADER:Ljava/lang/String;

    .line 81
    const-string v0, "meta"

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META:Ljava/lang/String;

    .line 82
    const-string v0, "title"

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META_TITLE:Ljava/lang/String;

    .line 83
    const-string v0, "uuid"

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META_UUID:Ljava/lang/String;

    .line 84
    const-string v0, "createdTime"

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META_CREATEDTIME:Ljava/lang/String;

    .line 85
    const-string v0, "contents"

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_CONTENTS:Ljava/lang/String;

    .line 86
    const-string v0, "content"

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_CONTENTS_CONTENT:Ljava/lang/String;

    .line 87
    const-string v0, "voice_content"

    sput-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_CONTENTS_VOICE_CONTENT:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    new-instance v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/share/beam/XMLUtills$1;-><init>(Lcom/samsung/android/app/memo/share/beam/XMLUtills;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mUnzipHandler:Landroid/os/Handler;

    .line 146
    new-instance v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/share/beam/XMLUtills$2;-><init>(Lcom/samsung/android/app/memo/share/beam/XMLUtills;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mHandler:Landroid/os/Handler;

    .line 153
    new-instance v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/share/beam/XMLUtills$3;-><init>(Lcom/samsung/android/app/memo/share/beam/XMLUtills;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mZipHandler:Landroid/os/Handler;

    .line 107
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContext:Landroid/content/Context;

    move-object v0, p1

    .line 108
    check-cast v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    iput-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    .line 109
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mRootDir:Ljava/io/File;

    if-nez v0, :cond_0

    .line 110
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->NFC_DIR:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mRootDir:Ljava/io/File;

    .line 111
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mRootDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mRootDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mRootTempDir:Ljava/io/File;

    if-nez v0, :cond_1

    .line 115
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 116
    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->APP_FOLDER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 115
    iput-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mRootTempDir:Ljava/io/File;

    .line 117
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mRootTempDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mRootTempDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 120
    :cond_1
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/share/beam/XMLUtills;)V
    .locals 0

    .prologue
    .line 461
    invoke-direct {p0}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->parseXML()V

    return-void
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/share/beam/XMLUtills;Landroid/app/ProgressDialog;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mProgress:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/share/beam/XMLUtills;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mProgress:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/share/beam/XMLUtills;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTempUnZipPath:Ljava/lang/String;

    return-object v0
.end method

.method private closeInputStream(Ljava/io/InputStream;)V
    .locals 3
    .param p1, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 448
    if-eqz p1, :cond_0

    .line 450
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 455
    :cond_0
    :goto_0
    return-void

    .line 451
    :catch_0
    move-exception v0

    .line 452
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    const-string v2, "readXMLFile "

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private closeOutPutStream(Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "outputStream"    # Ljava/io/OutputStream;

    .prologue
    .line 439
    if-eqz p1, :cond_0

    .line 441
    :try_start_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 442
    :catch_0
    move-exception v0

    .line 443
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    const-string v2, "createXMLFile "

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private copyFileFromInputStream(Ljava/io/InputStream;Ljava/lang/String;)Ljava/io/File;
    .locals 11
    .param p1, "inputStream"    # Ljava/io/InputStream;
    .param p2, "fileName"    # Ljava/lang/String;

    .prologue
    .line 193
    const/4 v5, 0x0

    .line 195
    .local v5, "outputStream":Ljava/io/OutputStream;
    :try_start_0
    new-instance v4, Ljava/io/File;

    iget-object v7, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mMediaTempath:Ljava/lang/String;

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 196
    .local v4, "mDir":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v4, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 197
    .local v2, "f":Ljava/io/File;
    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-virtual {v2, v7, v8}, Ljava/io/File;->setWritable(ZZ)Z

    .line 198
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    .end local v5    # "outputStream":Ljava/io/OutputStream;
    .local v6, "outputStream":Ljava/io/OutputStream;
    const/16 v7, 0x400

    :try_start_1
    new-array v0, v7, [B

    .line 200
    .local v0, "buffer":[B
    const/4 v3, 0x0

    .line 202
    .local v3, "length":I
    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    if-gtz v3, :cond_1

    .line 211
    if-eqz v6, :cond_0

    .line 212
    :try_start_2
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    .line 213
    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    move-object v5, v6

    .line 218
    .end local v0    # "buffer":[B
    .end local v2    # "f":Ljava/io/File;
    .end local v3    # "length":I
    .end local v4    # "mDir":Ljava/io/File;
    .end local v6    # "outputStream":Ljava/io/OutputStream;
    .restart local v5    # "outputStream":Ljava/io/OutputStream;
    :goto_2
    return-object v2

    .line 203
    .end local v5    # "outputStream":Ljava/io/OutputStream;
    .restart local v0    # "buffer":[B
    .restart local v2    # "f":Ljava/io/File;
    .restart local v3    # "length":I
    .restart local v4    # "mDir":Ljava/io/File;
    .restart local v6    # "outputStream":Ljava/io/OutputStream;
    :cond_1
    const/4 v7, 0x0

    :try_start_3
    invoke-virtual {v6, v0, v7, v3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 207
    .end local v0    # "buffer":[B
    .end local v3    # "length":I
    :catch_0
    move-exception v1

    move-object v5, v6

    .line 208
    .end local v2    # "f":Ljava/io/File;
    .end local v4    # "mDir":Ljava/io/File;
    .end local v6    # "outputStream":Ljava/io/OutputStream;
    .local v1, "e":Ljava/io/IOException;
    .restart local v5    # "outputStream":Ljava/io/OutputStream;
    :goto_3
    :try_start_4
    sget-object v7, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "copyFileFromInputStream from "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 211
    if-eqz v5, :cond_2

    .line 212
    :try_start_5
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 213
    :cond_2
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 218
    :goto_4
    const/4 v2, 0x0

    goto :goto_2

    .line 214
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "outputStream":Ljava/io/OutputStream;
    .restart local v0    # "buffer":[B
    .restart local v2    # "f":Ljava/io/File;
    .restart local v3    # "length":I
    .restart local v4    # "mDir":Ljava/io/File;
    .restart local v6    # "outputStream":Ljava/io/OutputStream;
    :catch_1
    move-exception v1

    .line 215
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v7, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "copyFileFromInputStream from "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 214
    .end local v0    # "buffer":[B
    .end local v2    # "f":Ljava/io/File;
    .end local v3    # "length":I
    .end local v4    # "mDir":Ljava/io/File;
    .end local v6    # "outputStream":Ljava/io/OutputStream;
    .restart local v5    # "outputStream":Ljava/io/OutputStream;
    :catch_2
    move-exception v1

    .line 215
    sget-object v7, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "copyFileFromInputStream from "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 209
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 211
    :goto_5
    if-eqz v5, :cond_3

    .line 212
    :try_start_6
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 213
    :cond_3
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 217
    :goto_6
    throw v7

    .line 214
    :catch_3
    move-exception v1

    .line 215
    .restart local v1    # "e":Ljava/io/IOException;
    sget-object v8, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "copyFileFromInputStream from "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 209
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "outputStream":Ljava/io/OutputStream;
    .restart local v2    # "f":Ljava/io/File;
    .restart local v4    # "mDir":Ljava/io/File;
    .restart local v6    # "outputStream":Ljava/io/OutputStream;
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "outputStream":Ljava/io/OutputStream;
    .restart local v5    # "outputStream":Ljava/io/OutputStream;
    goto :goto_5

    .line 207
    .end local v2    # "f":Ljava/io/File;
    .end local v4    # "mDir":Ljava/io/File;
    :catch_4
    move-exception v1

    goto :goto_3
.end method

.method private createMemoFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 222
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mRootTempDir:Ljava/io/File;

    invoke-direct {p0, v1, p1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->createTempPath(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTempZipPath:Ljava/lang/String;

    .line 223
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTempZipPath:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTempZipPath:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 224
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTempZipPath:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTempZipPath:Ljava/lang/String;

    .line 226
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTempZipPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 228
    .local v0, "tempfile":Ljava/io/File;
    const-string v1, "media"

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->createTempPath(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mMediaTempath:Ljava/lang/String;

    .line 229
    return-void
.end method

.method private createTempPath(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .param p1, "mFile"    # Ljava/io/File;
    .param p2, "nFilename"    # Ljava/lang/String;

    .prologue
    .line 167
    if-nez p2, :cond_0

    .line 168
    const/4 v0, 0x0

    .line 171
    :goto_0
    return-object v0

    .line 169
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 170
    .local v0, "oFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    goto :goto_0
.end method

.method private customHtmlParser(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 241
    move-object v7, p1

    .line 242
    .local v7, "returnString":Ljava/lang/String;
    const/4 v8, 0x0

    .line 243
    .local v8, "start":I
    const/4 v1, 0x0

    .line 245
    .local v1, "end":I
    const-string v3, "<img src="

    .line 246
    .local v3, "imageTag":Ljava/lang/String;
    const-string v6, " orientation="

    .line 250
    .local v6, "orientationTag":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1, v3, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v8

    .line 251
    :goto_0
    if-gtz v8, :cond_0

    .line 271
    :goto_1
    return-object v7

    .line 252
    :cond_0
    add-int/lit8 v9, v8, 0x1

    invoke-virtual {p1, v6, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 254
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v9, v8

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v10, v1, -0x1

    .line 253
    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 255
    .local v5, "mUri":Landroid/net/Uri;
    iget-object v9, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContext:Landroid/content/Context;

    const-string v10, "com.sec.android.directshare"

    .line 256
    const/4 v11, 0x1

    .line 255
    invoke-virtual {v9, v10, v5, v11}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    .line 257
    iget-object v9, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContext:Landroid/content/Context;

    const-string v10, "com.android.nfc"

    .line 258
    const/4 v11, 0x1

    .line 257
    invoke-virtual {v9, v10, v5, v11}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    .line 259
    iget-object v9, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v4

    .line 260
    .local v4, "in":Ljava/io/InputStream;
    invoke-virtual {v5}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v10, 0x1

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 261
    .local v2, "imageName":Ljava/lang/String;
    invoke-direct {p0, v4, v2}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->copyFileFromInputStream(Ljava/io/InputStream;Ljava/lang/String;)Ljava/io/File;

    .line 263
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v9, v8

    .line 264
    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v10, v1, -0x1

    .line 263
    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 264
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "/media/"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 262
    invoke-virtual {v7, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    .line 265
    move v8, v1

    .line 266
    const-string v9, "<img src"

    invoke-virtual {p1, v9, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    goto :goto_0

    .line 268
    .end local v2    # "imageName":Ljava/lang/String;
    .end local v4    # "in":Ljava/io/InputStream;
    .end local v5    # "mUri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 269
    .local v0, "e":Ljava/io/FileNotFoundException;
    sget-object v9, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "FileNotFoundException :"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public static deleteDir(Ljava/lang/String;)V
    .locals 6
    .param p0, "a_strPath"    # Ljava/lang/String;

    .prologue
    .line 175
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 176
    .local v2, "oFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 177
    .local v1, "oChildFileList":[Ljava/io/File;
    if-nez v1, :cond_0

    .line 188
    :goto_0
    return-void

    .line 180
    :cond_0
    array-length v4, v1

    const/4 v3, 0x0

    :goto_1
    if-lt v3, v4, :cond_1

    .line 187
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 180
    :cond_1
    aget-object v0, v1, v3

    .line 181
    .local v0, "oChildFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 182
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->deleteDir(Ljava/lang/String;)V

    .line 180
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 184
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_2
.end method

.method private insertVoiceAttachment(Ljava/lang/String;)V
    .locals 7
    .param p1, "voiceFilePath"    # Ljava/lang/String;

    .prologue
    .line 534
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTempUnZipPath:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 535
    .local v0, "basefilepath":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v4}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v3

    .line 537
    .local v3, "ss":Lcom/samsung/android/app/memo/Session;
    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 538
    .local v2, "mFile":Ljava/io/File;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 540
    :try_start_0
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/Session;->setVoiceRecordedFile(Landroid/net/Uri;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 545
    :cond_0
    :goto_0
    return-void

    .line 541
    :catch_0
    move-exception v1

    .line 542
    .local v1, "e":Lcom/samsung/android/app/memo/Session$SessionException;
    sget-object v4, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "insertVoiceAttachment "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private memoUnzip(Ljava/lang/String;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 548
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mRootTempDir:Ljava/io/File;

    .line 549
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unzip_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 548
    invoke-direct {p0, v1, v2}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->createTempPath(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 549
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 548
    iput-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTempUnZipPath:Ljava/lang/String;

    .line 550
    new-instance v0, Lcom/samsung/android/app/memo/share/beam/MemoZipItem;

    invoke-direct {v0}, Lcom/samsung/android/app/memo/share/beam/MemoZipItem;-><init>()V

    .line 551
    .local v0, "oItem":Lcom/samsung/android/app/memo/share/beam/MemoZipItem;
    iput-object p1, v0, Lcom/samsung/android/app/memo/share/beam/MemoZipItem;->mstrZipFilename:Ljava/lang/String;

    .line 552
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTempUnZipPath:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/app/memo/share/beam/MemoZipItem;->m_strZipRootpath:Ljava/lang/String;

    .line 553
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContext:Landroid/content/Context;

    const-string v2, ""

    .line 554
    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00ab

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 553
    invoke-static {v1, v2, v3}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mProgress:Landroid/app/ProgressDialog;

    .line 555
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mProgress:Landroid/app/ProgressDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 556
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isIndeterminate()Z

    .line 557
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 558
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mUnzipHandler:Landroid/os/Handler;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper;->unZip(Lcom/samsung/android/app/memo/share/beam/MemoZipItem;Landroid/os/Handler;)V

    .line 559
    return-void
.end method

.method private memoZip(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 232
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mRootDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->EXT:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mDestFile:Ljava/lang/String;

    .line 233
    new-instance v0, Lcom/samsung/android/app/memo/share/beam/MemoZipItem;

    invoke-direct {v0}, Lcom/samsung/android/app/memo/share/beam/MemoZipItem;-><init>()V

    .line 234
    .local v0, "oItem":Lcom/samsung/android/app/memo/share/beam/MemoZipItem;
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mDestFile:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/app/memo/share/beam/MemoZipItem;->mstrZipFilename:Ljava/lang/String;

    .line 235
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTempZipPath:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/app/memo/share/beam/MemoZipItem;->m_strZipRootpath:Ljava/lang/String;

    .line 236
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mZipHandler:Landroid/os/Handler;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper;->zip(Lcom/samsung/android/app/memo/share/beam/MemoZipItem;Landroid/os/Handler;)V

    .line 237
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mDestFile:Ljava/lang/String;

    return-object v1
.end method

.method private parseXML()V
    .locals 28

    .prologue
    .line 462
    const/4 v10, 0x0

    .line 464
    .local v10, "inStream":Ljava/io/InputStream;
    const/16 v24, 0x0

    .line 466
    .local v24, "voiceFilePath":Ljava/lang/String;
    :try_start_0
    new-instance v7, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTempUnZipPath:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v26, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "memo_content.xml"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 468
    .local v7, "file":Ljava/io/File;
    new-instance v11, Ljava/io/FileInputStream;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 469
    .end local v10    # "inStream":Ljava/io/InputStream;
    .local v11, "inStream":Ljava/io/InputStream;
    :try_start_1
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v4

    .line 470
    .local v4, "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v4}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v3

    .line 471
    .local v3, "db":Ljavax/xml/parsers/DocumentBuilder;
    new-instance v25, Lorg/xml/sax/InputSource;

    move-object/from16 v0, v25

    invoke-direct {v0, v11}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljavax/xml/parsers/DocumentBuilder;->parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;

    move-result-object v5

    .line 472
    .local v5, "doc":Lorg/w3c/dom/Document;
    invoke-interface {v5}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Lorg/w3c/dom/Element;->normalize()V

    .line 473
    sget-object v25, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_HEADER:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-interface {v5, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v15

    .line 474
    .local v15, "nodeListHeader":Lorg/w3c/dom/NodeList;
    invoke-interface {v15}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v16

    .line 475
    .local v16, "nodeListHeaderLength":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    move/from16 v0, v16

    if-lt v9, v0, :cond_2

    .line 496
    sget-object v25, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_CONTENTS:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-interface {v5, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v14

    .line 497
    .local v14, "nodeList":Lorg/w3c/dom/NodeList;
    invoke-interface {v14}, Lorg/w3c/dom/NodeList;->getLength()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v17

    .line 498
    .local v17, "nodeListLength":I
    const/16 v23, 0x0

    .local v23, "temp":I
    :goto_1
    move/from16 v0, v23

    move/from16 v1, v17

    if-lt v0, v1, :cond_8

    .line 514
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->closeInputStream(Ljava/io/InputStream;)V

    move-object v10, v11

    .line 516
    .end local v3    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v4    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v5    # "doc":Lorg/w3c/dom/Document;
    .end local v7    # "file":Ljava/io/File;
    .end local v9    # "i":I
    .end local v11    # "inStream":Ljava/io/InputStream;
    .end local v14    # "nodeList":Lorg/w3c/dom/NodeList;
    .end local v15    # "nodeListHeader":Lorg/w3c/dom/NodeList;
    .end local v16    # "nodeListHeaderLength":I
    .end local v17    # "nodeListLength":I
    .end local v23    # "temp":I
    .restart local v10    # "inStream":Ljava/io/InputStream;
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContent:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContent:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->isEmpty()Z

    move-result v25

    if-nez v25, :cond_0

    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContent:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->translateHtmlContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContent:Ljava/lang/String;

    .line 519
    :cond_0
    if-eqz v24, :cond_1

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->isEmpty()Z

    move-result v25

    if-nez v25, :cond_1

    .line 520
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->insertVoiceAttachment(Ljava/lang/String;)V

    .line 523
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v22

    .line 524
    .local v22, "ss":Lcom/samsung/android/app/memo/Session;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContent:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/Session;->setContent(Ljava/lang/String;)V

    .line 525
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTitle:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/Session;->setTitle(Ljava/lang/String;)V

    .line 526
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    check-cast v25, Lcom/samsung/android/app/memo/share/beam/XMLUtills$XMLUtillsOnDoneParsing;

    invoke-interface/range {v25 .. v25}, Lcom/samsung/android/app/memo/share/beam/XMLUtills$XMLUtillsOnDoneParsing;->onDoneXMLParsing()V

    .line 527
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mHandler:Landroid/os/Handler;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 528
    return-void

    .line 476
    .end local v10    # "inStream":Ljava/io/InputStream;
    .end local v22    # "ss":Lcom/samsung/android/app/memo/Session;
    .restart local v3    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v4    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v5    # "doc":Lorg/w3c/dom/Document;
    .restart local v7    # "file":Ljava/io/File;
    .restart local v9    # "i":I
    .restart local v11    # "inStream":Ljava/io/InputStream;
    .restart local v15    # "nodeListHeader":Lorg/w3c/dom/NodeList;
    .restart local v16    # "nodeListHeaderLength":I
    :cond_2
    :try_start_2
    invoke-interface {v15, v9}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v20

    .line 477
    .local v20, "node_header":Lorg/w3c/dom/Node;
    invoke-interface/range {v20 .. v20}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_3

    .line 478
    move-object/from16 v0, v20

    check-cast v0, Lorg/w3c/dom/Element;

    move-object v8, v0

    .line 479
    .local v8, "headerParent":Lorg/w3c/dom/Element;
    sget-object v25, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-interface {v8, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v18

    .line 480
    .local v18, "nodeListmeta":Lorg/w3c/dom/NodeList;
    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v19

    .line 481
    .local v19, "nodeListmetaLength":I
    const/16 v23, 0x0

    .restart local v23    # "temp":I
    :goto_3
    move/from16 v0, v23

    move/from16 v1, v19

    if-lt v0, v1, :cond_4

    .line 475
    .end local v8    # "headerParent":Lorg/w3c/dom/Element;
    .end local v18    # "nodeListmeta":Lorg/w3c/dom/NodeList;
    .end local v19    # "nodeListmetaLength":I
    .end local v23    # "temp":I
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 482
    .restart local v8    # "headerParent":Lorg/w3c/dom/Element;
    .restart local v18    # "nodeListmeta":Lorg/w3c/dom/NodeList;
    .restart local v19    # "nodeListmetaLength":I
    .restart local v23    # "temp":I
    :cond_4
    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v21

    .line 483
    .local v21, "node_meta":Lorg/w3c/dom/Node;
    invoke-interface/range {v21 .. v21}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_5

    .line 484
    move-object/from16 v0, v21

    check-cast v0, Lorg/w3c/dom/Element;

    move-object v12, v0

    .line 485
    .local v12, "meta_Parent":Lorg/w3c/dom/Element;
    sget-object v25, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META_TITLE:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-interface {v12, v0}, Lorg/w3c/dom/Element;->hasAttribute(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 486
    sget-object v25, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META_TITLE:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-interface {v12, v0}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTitle:Ljava/lang/String;

    .line 481
    .end local v12    # "meta_Parent":Lorg/w3c/dom/Element;
    :cond_5
    :goto_4
    add-int/lit8 v23, v23, 0x1

    goto :goto_3

    .line 487
    .restart local v12    # "meta_Parent":Lorg/w3c/dom/Element;
    :cond_6
    sget-object v25, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META_UUID:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-interface {v12, v0}, Lorg/w3c/dom/Element;->hasAttribute(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 488
    sget-object v25, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META_UUID:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-interface {v12, v0}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mUUID:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 511
    .end local v3    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v4    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v5    # "doc":Lorg/w3c/dom/Document;
    .end local v8    # "headerParent":Lorg/w3c/dom/Element;
    .end local v9    # "i":I
    .end local v12    # "meta_Parent":Lorg/w3c/dom/Element;
    .end local v15    # "nodeListHeader":Lorg/w3c/dom/NodeList;
    .end local v16    # "nodeListHeaderLength":I
    .end local v18    # "nodeListmeta":Lorg/w3c/dom/NodeList;
    .end local v19    # "nodeListmetaLength":I
    .end local v20    # "node_header":Lorg/w3c/dom/Node;
    .end local v21    # "node_meta":Lorg/w3c/dom/Node;
    .end local v23    # "temp":I
    :catch_0
    move-exception v6

    move-object v10, v11

    .line 512
    .end local v7    # "file":Ljava/io/File;
    .end local v11    # "inStream":Ljava/io/InputStream;
    .local v6, "e":Ljava/lang/Exception;
    .restart local v10    # "inStream":Ljava/io/InputStream;
    :goto_5
    :try_start_3
    sget-object v25, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "parseXML "

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContent:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v6}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 514
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->closeInputStream(Ljava/io/InputStream;)V

    goto/16 :goto_2

    .line 489
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v10    # "inStream":Ljava/io/InputStream;
    .restart local v3    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v4    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v5    # "doc":Lorg/w3c/dom/Document;
    .restart local v7    # "file":Ljava/io/File;
    .restart local v8    # "headerParent":Lorg/w3c/dom/Element;
    .restart local v9    # "i":I
    .restart local v11    # "inStream":Ljava/io/InputStream;
    .restart local v12    # "meta_Parent":Lorg/w3c/dom/Element;
    .restart local v15    # "nodeListHeader":Lorg/w3c/dom/NodeList;
    .restart local v16    # "nodeListHeaderLength":I
    .restart local v18    # "nodeListmeta":Lorg/w3c/dom/NodeList;
    .restart local v19    # "nodeListmetaLength":I
    .restart local v20    # "node_header":Lorg/w3c/dom/Node;
    .restart local v21    # "node_meta":Lorg/w3c/dom/Node;
    .restart local v23    # "temp":I
    :cond_7
    :try_start_4
    sget-object v25, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META_CREATEDTIME:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-interface {v12, v0}, Lorg/w3c/dom/Element;->hasAttribute(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 490
    sget-object v25, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META_CREATEDTIME:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-interface {v12, v0}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4

    .line 513
    .end local v3    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v4    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v5    # "doc":Lorg/w3c/dom/Document;
    .end local v8    # "headerParent":Lorg/w3c/dom/Element;
    .end local v9    # "i":I
    .end local v12    # "meta_Parent":Lorg/w3c/dom/Element;
    .end local v15    # "nodeListHeader":Lorg/w3c/dom/NodeList;
    .end local v16    # "nodeListHeaderLength":I
    .end local v18    # "nodeListmeta":Lorg/w3c/dom/NodeList;
    .end local v19    # "nodeListmetaLength":I
    .end local v20    # "node_header":Lorg/w3c/dom/Node;
    .end local v21    # "node_meta":Lorg/w3c/dom/Node;
    .end local v23    # "temp":I
    :catchall_0
    move-exception v25

    move-object v10, v11

    .line 514
    .end local v7    # "file":Ljava/io/File;
    .end local v11    # "inStream":Ljava/io/InputStream;
    .restart local v10    # "inStream":Ljava/io/InputStream;
    :goto_6
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->closeInputStream(Ljava/io/InputStream;)V

    .line 515
    throw v25

    .line 499
    .end local v10    # "inStream":Ljava/io/InputStream;
    .restart local v3    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v4    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v5    # "doc":Lorg/w3c/dom/Document;
    .restart local v7    # "file":Ljava/io/File;
    .restart local v9    # "i":I
    .restart local v11    # "inStream":Ljava/io/InputStream;
    .restart local v14    # "nodeList":Lorg/w3c/dom/NodeList;
    .restart local v15    # "nodeListHeader":Lorg/w3c/dom/NodeList;
    .restart local v16    # "nodeListHeaderLength":I
    .restart local v17    # "nodeListLength":I
    .restart local v23    # "temp":I
    :cond_8
    :try_start_5
    move/from16 v0, v23

    invoke-interface {v14, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v13

    .line 500
    .local v13, "node":Lorg/w3c/dom/Node;
    invoke-interface {v13}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_9

    .line 501
    move-object v0, v13

    check-cast v0, Lorg/w3c/dom/Element;

    move-object v2, v0

    .line 502
    .local v2, "contentParent":Lorg/w3c/dom/Element;
    sget-object v25, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_CONTENTS_CONTENT:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-interface {v2, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v25

    const/16 v26, 0x0

    invoke-interface/range {v25 .. v26}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v25

    .line 503
    invoke-interface/range {v25 .. v25}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v25

    .line 502
    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContent:Ljava/lang/String;

    .line 505
    sget-object v25, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_CONTENTS_VOICE_CONTENT:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-interface {v2, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v25

    const/16 v26, 0x0

    invoke-interface/range {v25 .. v26}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v25

    .line 506
    invoke-interface/range {v25 .. v25}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v24

    .line 498
    .end local v2    # "contentParent":Lorg/w3c/dom/Element;
    :cond_9
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_1

    .line 513
    .end local v3    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v4    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v5    # "doc":Lorg/w3c/dom/Document;
    .end local v7    # "file":Ljava/io/File;
    .end local v9    # "i":I
    .end local v11    # "inStream":Ljava/io/InputStream;
    .end local v13    # "node":Lorg/w3c/dom/Node;
    .end local v14    # "nodeList":Lorg/w3c/dom/NodeList;
    .end local v15    # "nodeListHeader":Lorg/w3c/dom/NodeList;
    .end local v16    # "nodeListHeaderLength":I
    .end local v17    # "nodeListLength":I
    .end local v23    # "temp":I
    .restart local v10    # "inStream":Ljava/io/InputStream;
    :catchall_1
    move-exception v25

    goto :goto_6

    .line 511
    :catch_1
    move-exception v6

    goto/16 :goto_5
.end method

.method private translateHtmlContent(Ljava/lang/String;)Ljava/lang/String;
    .locals 22
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 275
    new-instance v19, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTempUnZipPath:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v20, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 277
    .local v5, "basefilepath":Ljava/lang/String;
    move-object/from16 v16, p1

    .line 279
    .local v16, "returnString":Ljava/lang/String;
    const/4 v14, 0x0

    .line 280
    .local v14, "orientation":I
    const/16 v18, 0x0

    .line 281
    .local v18, "start":I
    const/4 v7, 0x0

    .line 282
    .local v7, "end":I
    const/4 v9, 0x0

    .line 283
    .local v9, "endOrientation":I
    const/4 v8, 0x0

    .line 284
    .local v8, "endAltText":I
    const-string v10, "<img src="

    .line 285
    .local v10, "imageTag":Ljava/lang/String;
    const-string v15, " orientation="

    .line 286
    .local v15, "orientationTag":Ljava/lang/String;
    const-string v4, " altText="

    .line 287
    .local v4, "altTextTag":Ljava/lang/String;
    const-string v3, ""

    .line 288
    .local v3, "altText":Ljava/lang/String;
    const/4 v11, 0x0

    .line 289
    .local v11, "mUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v17

    .line 291
    .local v17, "ss":Lcom/samsung/android/app/memo/Session;
    :try_start_0
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v10, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v18

    .line 292
    :goto_0
    if-gtz v18, :cond_0

    .line 326
    :goto_1
    return-object v16

    .line 293
    :cond_0
    add-int/lit8 v19, v18, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v15, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v7

    .line 294
    add-int/lit8 v19, v7, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v9

    .line 295
    const-string v19, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v8

    .line 297
    add-int/lit8 v19, v8, -0x1

    :try_start_1
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v20

    add-int v20, v20, v9

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_1

    .line 298
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v19

    add-int v19, v19, v9

    add-int/lit8 v19, v19, 0x1

    add-int/lit8 v20, v8, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 299
    :cond_1
    if-eqz v3, :cond_2

    const-string v19, "\""

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 300
    const/16 v19, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v20

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 306
    :cond_2
    :goto_2
    :try_start_2
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v19

    add-int v19, v19, v18

    add-int/lit8 v19, v19, 0x1

    add-int/lit8 v20, v7, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v13

    check-cast v13, Ljava/lang/String;
    :try_end_2
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_2 .. :try_end_2} :catch_1

    .line 308
    .local v13, "mtempFilePath":Ljava/lang/String;
    :try_start_3
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v19

    add-int v19, v19, v7

    add-int/lit8 v19, v19, 0x1

    add-int/lit8 v20, v9, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_3 .. :try_end_3} :catch_1

    move-result v14

    .line 312
    :goto_3
    :try_start_4
    new-instance v12, Ljava/io/File;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 313
    .local v12, "mfile":Ljava/io/File;
    if-eqz v12, :cond_3

    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v19

    if-eqz v19, :cond_3

    .line 314
    invoke-static {v12}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v19

    const-string v20, "image/jpeg"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v3, v2, v14}, Lcom/samsung/android/app/memo/Session;->addFile(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v11

    .line 317
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v19

    add-int v19, v19, v18

    add-int/lit8 v19, v19, 0x1

    add-int/lit8 v20, v7, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 318
    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v20

    .line 316
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v16

    .line 320
    :cond_3
    move/from16 v18, v7

    .line 321
    const-string v19, "<img src"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v18

    goto/16 :goto_0

    .line 302
    .end local v12    # "mfile":Ljava/io/File;
    .end local v13    # "mtempFilePath":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 303
    .local v6, "e":Ljava/lang/StringIndexOutOfBoundsException;
    sget-object v19, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    const-string v20, "translateHtmlcontent "

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v6}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_2

    .line 323
    .end local v6    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :catch_1
    move-exception v6

    .line 324
    .local v6, "e":Lcom/samsung/android/app/memo/Session$SessionException;
    sget-object v19, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "translateHtmlContent from "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v6}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 309
    .end local v6    # "e":Lcom/samsung/android/app/memo/Session$SessionException;
    .restart local v13    # "mtempFilePath":Ljava/lang/String;
    :catch_2
    move-exception v6

    .line 310
    .local v6, "e":Ljava/lang/NumberFormatException;
    :try_start_5
    sget-object v19, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    const-string v20, "translateHtmlContent "

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v6}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_3
.end method


# virtual methods
.method public createXMLFile(J)Ljava/lang/String;
    .locals 37
    .param p1, "memoId"    # J

    .prologue
    .line 330
    const/16 v25, 0x0

    .line 331
    .local v25, "ss":Lcom/samsung/android/app/memo/Session;
    const-wide/16 v34, -0x1

    cmp-long v33, p1, v34

    if-nez v33, :cond_0

    .line 332
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v25

    .line 340
    :goto_0
    if-nez v25, :cond_1

    .line 341
    const/16 v33, 0x0

    .line 435
    :goto_1
    return-object v33

    .line 335
    :cond_0
    :try_start_0
    new-instance v26, Lcom/samsung/android/app/memo/Session;

    move-object/from16 v0, v26

    move-wide/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/Session;-><init>(J)V
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v25    # "ss":Lcom/samsung/android/app/memo/Session;
    .local v26, "ss":Lcom/samsung/android/app/memo/Session;
    move-object/from16 v25, v26

    .line 336
    .end local v26    # "ss":Lcom/samsung/android/app/memo/Session;
    .restart local v25    # "ss":Lcom/samsung/android/app/memo/Session;
    goto :goto_0

    :catch_0
    move-exception v12

    .line 337
    .local v12, "e":Lcom/samsung/android/app/memo/Session$SessionException;
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    new-instance v34, Ljava/lang/StringBuilder;

    const-string v35, "createXMLFile memoId:"

    invoke-direct/range {v34 .. v35}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v34

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    invoke-static {v0, v1, v12}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 342
    .end local v12    # "e":Lcom/samsung/android/app/memo/Session$SessionException;
    :cond_1
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/app/memo/Session;->getUUID()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->createMemoFile(Ljava/lang/String;)V

    .line 343
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/app/memo/Session;->getContent()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContent:Ljava/lang/String;

    .line 344
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/app/memo/Session;->getTitle()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTitle:Ljava/lang/String;

    .line 345
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/app/memo/Session;->getUUID()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mUUID:Ljava/lang/String;

    .line 346
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContent:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->customHtmlParser(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContent:Ljava/lang/String;

    .line 348
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v10

    .line 349
    .local v10, "docFactory":Ljavax/xml/parsers/DocumentBuilderFactory;
    const/16 v19, 0x0

    .line 352
    .local v19, "outputStream":Ljava/io/OutputStream;
    :try_start_1
    invoke-virtual {v10}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v9

    .line 355
    .local v9, "docBuilder":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v9}, Ljavax/xml/parsers/DocumentBuilder;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v8

    .line 356
    .local v8, "doc":Lorg/w3c/dom/Document;
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v24

    .line 357
    .local v24, "rootElement":Lorg/w3c/dom/Element;
    move-object/from16 v0, v24

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 359
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_VERSION:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->createAttribute(Ljava/lang/String;)Lorg/w3c/dom/Attr;

    move-result-object v32

    .line 360
    .local v32, "version_attr":Lorg/w3c/dom/Attr;
    const-string v33, "1.0"

    invoke-interface/range {v32 .. v33}, Lorg/w3c/dom/Attr;->setValue(Ljava/lang/String;)V

    .line 361
    move-object/from16 v0, v24

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->setAttributeNode(Lorg/w3c/dom/Attr;)Lorg/w3c/dom/Attr;

    .line 364
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_HEADER:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v14

    .line 365
    .local v14, "header":Lorg/w3c/dom/Element;
    move-object/from16 v0, v24

    invoke-interface {v0, v14}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 368
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v16

    .line 369
    .local v16, "metaTitle":Lorg/w3c/dom/Element;
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META_TITLE:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->createAttribute(Ljava/lang/String;)Lorg/w3c/dom/Attr;

    move-result-object v28

    .line 370
    .local v28, "title_attr":Lorg/w3c/dom/Attr;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTitle:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Lorg/w3c/dom/Attr;->setValue(Ljava/lang/String;)V

    .line 371
    move-object/from16 v0, v16

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->setAttributeNode(Lorg/w3c/dom/Attr;)Lorg/w3c/dom/Attr;

    .line 372
    move-object/from16 v0, v16

    invoke-interface {v14, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 375
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v17

    .line 376
    .local v17, "meta_uuid":Lorg/w3c/dom/Element;
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META_UUID:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->createAttribute(Ljava/lang/String;)Lorg/w3c/dom/Attr;

    move-result-object v31

    .line 377
    .local v31, "uuid_attr":Lorg/w3c/dom/Attr;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mUUID:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Lorg/w3c/dom/Attr;->setValue(Ljava/lang/String;)V

    .line 378
    move-object/from16 v0, v17

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->setAttributeNode(Lorg/w3c/dom/Attr;)Lorg/w3c/dom/Attr;

    .line 379
    move-object/from16 v0, v17

    invoke-interface {v14, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 381
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v6

    .line 383
    .local v6, "createdTime":Lorg/w3c/dom/Element;
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_MEMO_META_CREATEDTIME:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->createAttribute(Ljava/lang/String;)Lorg/w3c/dom/Attr;

    move-result-object v7

    .line 384
    .local v7, "createdTime_attr":Lorg/w3c/dom/Attr;
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/app/memo/Session;->getCreatedAt()J

    move-result-wide v34

    invoke-static/range {v34 .. v35}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v34

    invoke-direct/range {v33 .. v34}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-interface {v7, v0}, Lorg/w3c/dom/Attr;->setValue(Ljava/lang/String;)V

    .line 385
    invoke-interface {v6, v7}, Lorg/w3c/dom/Element;->setAttributeNode(Lorg/w3c/dom/Attr;)Lorg/w3c/dom/Attr;

    .line 386
    invoke-interface {v14, v6}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 388
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_CONTENTS:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v23

    .line 389
    .local v23, "rootContents":Lorg/w3c/dom/Element;
    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 391
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_CONTENTS_CONTENT:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v4

    .line 392
    .local v4, "content":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContent:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v0, v33

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-interface {v4, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 393
    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 395
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/app/memo/Session;->getVrFileUUID()Ljava/lang/String;

    move-result-object v15

    .line 396
    .local v15, "mVoiceId":Ljava/lang/String;
    if-eqz v15, :cond_2

    const-string v33, ""

    move-object/from16 v0, v33

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v33

    if-nez v33, :cond_2

    .line 398
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->XML_TAG_CONTENTS_VOICE_CONTENT:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v5

    .line 399
    .local v5, "contentVoice":Lorg/w3c/dom/Element;
    sget-object v33, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    move-object/from16 v0, v33

    invoke-static {v0, v15}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v30

    .line 400
    .local v30, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContext:Landroid/content/Context;

    move-object/from16 v33, v0

    const-string v34, "com.sec.android.directshare"

    .line 401
    const/16 v35, 0x1

    .line 400
    move-object/from16 v0, v33

    move-object/from16 v1, v34

    move-object/from16 v2, v30

    move/from16 v3, v35

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    .line 402
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContext:Landroid/content/Context;

    move-object/from16 v33, v0

    const-string v34, "com.android.nfc"

    .line 403
    const/16 v35, 0x1

    .line 402
    move-object/from16 v0, v33

    move-object/from16 v1, v34

    move-object/from16 v2, v30

    move/from16 v3, v35

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mContext:Landroid/content/Context;

    move-object/from16 v33, v0

    invoke-virtual/range {v33 .. v33}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v33

    .line 405
    move-object/from16 v0, v33

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v33

    .line 404
    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1, v15}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->copyFileFromInputStream(Ljava/io/InputStream;Ljava/lang/String;)Ljava/io/File;

    .line 406
    new-instance v33, Ljava/lang/StringBuilder;

    const-string v34, "/media/"

    invoke-direct/range {v33 .. v34}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 407
    move-object/from16 v0, v33

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    .line 406
    move-object/from16 v0, v33

    invoke-interface {v8, v0}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-interface {v5, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 408
    move-object/from16 v0, v23

    invoke-interface {v0, v5}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 411
    .end local v5    # "contentVoice":Lorg/w3c/dom/Element;
    .end local v30    # "uri":Landroid/net/Uri;
    :cond_2
    invoke-static {}, Ljavax/xml/transform/TransformerFactory;->newInstance()Ljavax/xml/transform/TransformerFactory;

    move-result-object v13

    .line 412
    .local v13, "factory":Ljavax/xml/transform/TransformerFactory;
    invoke-virtual {v13}, Ljavax/xml/transform/TransformerFactory;->newTransformer()Ljavax/xml/transform/Transformer;

    move-result-object v29

    .line 413
    .local v29, "transformer":Ljavax/xml/transform/Transformer;
    new-instance v18, Ljava/util/Properties;

    invoke-direct/range {v18 .. v18}, Ljava/util/Properties;-><init>()V

    .line 414
    .local v18, "outFormat":Ljava/util/Properties;
    const-string v33, "indent"

    const-string v34, "yes"

    move-object/from16 v0, v18

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 415
    const-string v33, "method"

    const-string v34, "xml"

    move-object/from16 v0, v18

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 416
    const-string v33, "omit-xml-declaration"

    const-string v34, "no"

    move-object/from16 v0, v18

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 417
    const-string v33, "version"

    const-string v34, "1.0"

    move-object/from16 v0, v18

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 418
    const-string v33, "encoding"

    const-string v34, "UTF-8"

    move-object/from16 v0, v18

    move-object/from16 v1, v33

    move-object/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    .line 419
    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljavax/xml/transform/Transformer;->setOutputProperties(Ljava/util/Properties;)V

    .line 420
    new-instance v11, Ljavax/xml/transform/dom/DOMSource;

    invoke-direct {v11, v8}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    .line 421
    .local v11, "domSource":Ljavax/xml/transform/dom/DOMSource;
    new-instance v33, Ljava/io/File;

    new-instance v34, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mTempZipPath:Ljava/lang/String;

    move-object/from16 v35, v0

    invoke-direct/range {v34 .. v35}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v35, "memo_content.xml"

    invoke-direct/range {v33 .. v35}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v33

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->memofile:Ljava/io/File;

    .line 422
    new-instance v20, Ljava/io/FileOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->memofile:Ljava/io/File;

    move-object/from16 v33, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljavax/xml/transform/TransformerException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423
    .end local v19    # "outputStream":Ljava/io/OutputStream;
    .local v20, "outputStream":Ljava/io/OutputStream;
    :try_start_2
    new-instance v22, Ljavax/xml/transform/stream/StreamResult;

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/io/OutputStream;)V

    .line 424
    .local v22, "result":Ljavax/xml/transform/stream/StreamResult;
    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-virtual {v0, v11, v1}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V
    :try_end_2
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljavax/xml/transform/TransformerException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 433
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->closeOutPutStream(Ljava/io/OutputStream;)V

    move-object/from16 v19, v20

    .line 435
    .end local v4    # "content":Lorg/w3c/dom/Element;
    .end local v6    # "createdTime":Lorg/w3c/dom/Element;
    .end local v7    # "createdTime_attr":Lorg/w3c/dom/Attr;
    .end local v8    # "doc":Lorg/w3c/dom/Document;
    .end local v9    # "docBuilder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v11    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .end local v13    # "factory":Ljavax/xml/transform/TransformerFactory;
    .end local v14    # "header":Lorg/w3c/dom/Element;
    .end local v15    # "mVoiceId":Ljava/lang/String;
    .end local v16    # "metaTitle":Lorg/w3c/dom/Element;
    .end local v17    # "meta_uuid":Lorg/w3c/dom/Element;
    .end local v18    # "outFormat":Ljava/util/Properties;
    .end local v20    # "outputStream":Ljava/io/OutputStream;
    .end local v22    # "result":Ljavax/xml/transform/stream/StreamResult;
    .end local v23    # "rootContents":Lorg/w3c/dom/Element;
    .end local v24    # "rootElement":Lorg/w3c/dom/Element;
    .end local v28    # "title_attr":Lorg/w3c/dom/Attr;
    .end local v29    # "transformer":Ljavax/xml/transform/Transformer;
    .end local v31    # "uuid_attr":Lorg/w3c/dom/Attr;
    .end local v32    # "version_attr":Lorg/w3c/dom/Attr;
    .restart local v19    # "outputStream":Ljava/io/OutputStream;
    :goto_2
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/app/memo/Session;->getUUID()Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->memoZip(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    goto/16 :goto_1

    .line 426
    :catch_1
    move-exception v21

    .line 427
    .local v21, "pce":Ljavax/xml/parsers/ParserConfigurationException;
    :goto_3
    :try_start_3
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    const-string v34, "createXMLFile "

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 433
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->closeOutPutStream(Ljava/io/OutputStream;)V

    goto :goto_2

    .line 428
    .end local v21    # "pce":Ljavax/xml/parsers/ParserConfigurationException;
    :catch_2
    move-exception v27

    .line 429
    .local v27, "tfe":Ljavax/xml/transform/TransformerException;
    :goto_4
    :try_start_4
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    const-string v34, "createXMLFile "

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    move-object/from16 v2, v27

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 433
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->closeOutPutStream(Ljava/io/OutputStream;)V

    goto :goto_2

    .line 430
    .end local v27    # "tfe":Ljavax/xml/transform/TransformerException;
    :catch_3
    move-exception v12

    .line 431
    .local v12, "e":Ljava/io/FileNotFoundException;
    :goto_5
    :try_start_5
    sget-object v33, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;

    const-string v34, "createXMLFile "

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    invoke-static {v0, v1, v12}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 433
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->closeOutPutStream(Ljava/io/OutputStream;)V

    goto :goto_2

    .line 432
    .end local v12    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v33

    .line 433
    :goto_6
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->closeOutPutStream(Ljava/io/OutputStream;)V

    .line 434
    throw v33

    .line 432
    .end local v19    # "outputStream":Ljava/io/OutputStream;
    .restart local v4    # "content":Lorg/w3c/dom/Element;
    .restart local v6    # "createdTime":Lorg/w3c/dom/Element;
    .restart local v7    # "createdTime_attr":Lorg/w3c/dom/Attr;
    .restart local v8    # "doc":Lorg/w3c/dom/Document;
    .restart local v9    # "docBuilder":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v11    # "domSource":Ljavax/xml/transform/dom/DOMSource;
    .restart local v13    # "factory":Ljavax/xml/transform/TransformerFactory;
    .restart local v14    # "header":Lorg/w3c/dom/Element;
    .restart local v15    # "mVoiceId":Ljava/lang/String;
    .restart local v16    # "metaTitle":Lorg/w3c/dom/Element;
    .restart local v17    # "meta_uuid":Lorg/w3c/dom/Element;
    .restart local v18    # "outFormat":Ljava/util/Properties;
    .restart local v20    # "outputStream":Ljava/io/OutputStream;
    .restart local v23    # "rootContents":Lorg/w3c/dom/Element;
    .restart local v24    # "rootElement":Lorg/w3c/dom/Element;
    .restart local v28    # "title_attr":Lorg/w3c/dom/Attr;
    .restart local v29    # "transformer":Ljavax/xml/transform/Transformer;
    .restart local v31    # "uuid_attr":Lorg/w3c/dom/Attr;
    .restart local v32    # "version_attr":Lorg/w3c/dom/Attr;
    :catchall_1
    move-exception v33

    move-object/from16 v19, v20

    .end local v20    # "outputStream":Ljava/io/OutputStream;
    .restart local v19    # "outputStream":Ljava/io/OutputStream;
    goto :goto_6

    .line 430
    .end local v19    # "outputStream":Ljava/io/OutputStream;
    .restart local v20    # "outputStream":Ljava/io/OutputStream;
    :catch_4
    move-exception v12

    move-object/from16 v19, v20

    .end local v20    # "outputStream":Ljava/io/OutputStream;
    .restart local v19    # "outputStream":Ljava/io/OutputStream;
    goto :goto_5

    .line 428
    .end local v19    # "outputStream":Ljava/io/OutputStream;
    .restart local v20    # "outputStream":Ljava/io/OutputStream;
    :catch_5
    move-exception v27

    move-object/from16 v19, v20

    .end local v20    # "outputStream":Ljava/io/OutputStream;
    .restart local v19    # "outputStream":Ljava/io/OutputStream;
    goto :goto_4

    .line 426
    .end local v19    # "outputStream":Ljava/io/OutputStream;
    .restart local v20    # "outputStream":Ljava/io/OutputStream;
    :catch_6
    move-exception v21

    move-object/from16 v19, v20

    .end local v20    # "outputStream":Ljava/io/OutputStream;
    .restart local v19    # "outputStream":Ljava/io/OutputStream;
    goto :goto_3
.end method

.method public getmUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mUUID:Ljava/lang/String;

    return-object v0
.end method

.method public readXMLFile(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 458
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->memoUnzip(Ljava/lang/String;)V

    .line 459
    return-void
.end method
