.class Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;
.super Lcom/samsung/android/app/memo/MemoServiceIF$Stub;
.source "MemoService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/MemoService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MemoServiceIFImpl"
.end annotation


# instance fields
.field private mContent:Ljava/lang/StringBuffer;

.field private mContext:Landroid/content/Context;

.field private mCreatedAt:J

.field private mLastModifiedAt:J

.field private mSession:Lcom/samsung/android/app/memo/Session;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0}, Lcom/samsung/android/app/memo/MemoServiceIF$Stub;-><init>()V

    .line 64
    iput-object v0, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    .line 65
    iput-object v0, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mContent:Ljava/lang/StringBuffer;

    .line 70
    iput-object p1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mContext:Landroid/content/Context;

    .line 71
    return-void
.end method


# virtual methods
.method public appendImageByBitmap(Landroid/graphics/Bitmap;)V
    .locals 7
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v5, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 125
    .local v0, "cacheDir":Ljava/io/File;
    const/4 v4, 0x0

    .line 126
    .local v4, "tempFile":Ljava/io/File;
    const/4 v2, 0x0

    .line 128
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    const-string v5, "img"

    const-string v6, ""

    invoke-static {v5, v6, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v4

    .line 129
    invoke-virtual {v4}, Ljava/io/File;->deleteOnExit()V

    .line 130
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .local v3, "fos":Ljava/io/FileOutputStream;
    if-eqz p1, :cond_0

    .line 132
    :try_start_1
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x50

    invoke-virtual {p1, v5, v6, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 134
    :cond_0
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->appendImageByUri(Landroid/net/Uri;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 139
    if-eqz v3, :cond_1

    .line 141
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 144
    :cond_1
    :goto_0
    if-eqz v4, :cond_2

    .line 145
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 147
    :cond_2
    return-void

    .line 135
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v1

    .line 136
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    # getter for: Lcom/samsung/android/app/memo/MemoService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/MemoService;->access$0()Ljava/lang/String;

    move-result-object v5

    const-string v6, "appendImageByBitmap"

    invoke-static {v5, v6, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 137
    new-instance v5, Landroid/os/RemoteException;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 138
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    .line 139
    :goto_2
    if-eqz v2, :cond_3

    .line 141
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 144
    :cond_3
    :goto_3
    if-eqz v4, :cond_4

    .line 145
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 146
    :cond_4
    throw v5

    .line 142
    :catch_1
    move-exception v6

    goto :goto_3

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v5

    goto :goto_0

    .line 138
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 135
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v1

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method public appendImageByUri(Landroid/net/Uri;)V
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 114
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "image/jpeg"

    const/4 v6, 0x0

    invoke-virtual {v3, p1, v4, v5, v6}, Lcom/samsung/android/app/memo/Session;->addFile(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v1

    .line 115
    .local v1, "imgSrc":Landroid/net/Uri;
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    iget-object v3, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v3, v1, v4, v5}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;-><init>(Landroid/content/Context;Landroid/net/Uri;ILjava/lang/String;)V

    .line 116
    .local v0, "imageSpan":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    iget-object v3, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mContent:Ljava/lang/StringBuffer;

    const-string v4, "<p>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->toHtml()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "</p>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    .end local v0    # "imageSpan":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    .end local v1    # "imgSrc":Landroid/net/Uri;
    :goto_0
    return-void

    .line 117
    :catch_0
    move-exception v2

    .line 118
    .local v2, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    # getter for: Lcom/samsung/android/app/memo/MemoService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/MemoService;->access$0()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "appendImageByUri() uri: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public appendText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mContent:Ljava/lang/StringBuffer;

    const-string v1, "<p>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "</p>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 109
    return-void
.end method

.method public createNew()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Session;->isDirty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    iget-object v1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Session;->cancelEditing()V

    .line 79
    :cond_0
    :try_start_0
    new-instance v1, Lcom/samsung/android/app/memo/Session;

    const-wide/16 v2, -0x1

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/app/memo/Session;-><init>(J)V

    iput-object v1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    iget-object v1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mContent:Ljava/lang/StringBuffer;

    if-nez v1, :cond_1

    .line 86
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mContent:Ljava/lang/StringBuffer;

    .line 91
    :goto_0
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mLastModifiedAt:J

    iput-wide v2, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mCreatedAt:J

    .line 93
    iget-object v1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Session;->getUUID()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 80
    :catch_0
    move-exception v0

    .line 81
    .local v0, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    # getter for: Lcom/samsung/android/app/memo/MemoService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/MemoService;->access$0()Ljava/lang/String;

    move-result-object v1

    const-string v2, "createNew()"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    new-instance v1, Landroid/os/RemoteException;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Session$SessionException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 88
    .end local v0    # "se":Lcom/samsung/android/app/memo/Session$SessionException;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mContent:Ljava/lang/StringBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_0
.end method

.method public getUriWidthUUID()Landroid/net/Uri;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 183
    sget-object v0, Lcom/samsung/android/provider/memo/MemoContract;->AUTHORITY_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Session;->getUUID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getUriWithId()Landroid/net/Uri;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 174
    iget-object v2, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v0

    .line 175
    .local v0, "id":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 176
    new-instance v2, Landroid/os/RemoteException;

    const-string v3, "current session wasn\'t saved"

    invoke-direct {v2, v3}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 178
    :cond_0
    sget-object v2, Lcom/samsung/android/provider/memo/MemoContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    return-object v2
.end method

.method public saveCurrent()J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 161
    iget-object v1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    iget-object v2, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mContent:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/memo/Session;->setContent(Ljava/lang/String;)V

    .line 163
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    iget-wide v2, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mCreatedAt:J

    iget-wide v4, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mLastModifiedAt:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/app/memo/Session;->overrideDT(JJ)V

    .line 164
    iget-object v1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Session;->saveMemo()J
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    iget-object v1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v2

    return-wide v2

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    # getter for: Lcom/samsung/android/app/memo/MemoService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/MemoService;->access$0()Ljava/lang/String;

    move-result-object v1

    const-string v2, "saveCurrent()"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 167
    new-instance v1, Landroid/os/RemoteException;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Session$SessionException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 1
    .param p1, "category"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/memo/Session;->setCategory(Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public setDate(JJ)V
    .locals 1
    .param p1, "createdAt"    # J
    .param p3, "lastModifiedAt"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 188
    iput-wide p1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mCreatedAt:J

    .line 189
    iput-wide p3, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mLastModifiedAt:J

    .line 190
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/memo/Session;->setTitle(Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method public setVRByUri(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 152
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/MemoService$MemoServiceIFImpl;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v1, p1}, Lcom/samsung/android/app/memo/Session;->setVoiceRecordedFile(Landroid/net/Uri;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    return-void

    .line 153
    :catch_0
    move-exception v0

    .line 154
    .local v0, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    # getter for: Lcom/samsung/android/app/memo/MemoService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/MemoService;->access$0()Ljava/lang/String;

    move-result-object v1

    const-string v2, "setVRByUri()"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 155
    new-instance v1, Landroid/os/RemoteException;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Session$SessionException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
