.class Lcom/samsung/android/app/memo/uiwidget/UndoBarController$1;
.super Ljava/lang/Object;
.source "UndoBarController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uiwidget/UndoBarController;-><init>(Landroid/view/View;Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$1;->this$0:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$1;->this$0:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->hideUndoBar(Z)V

    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$1;->this$0:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoListener:Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->access$0(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;)Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$1;->this$0:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoToken:Landroid/os/Parcelable;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->access$1(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;->onUndo(Landroid/os/Parcelable;)V

    .line 66
    return-void
.end method
