.class final enum Lcom/samsung/android/app/memo/Main$Mode;
.super Ljava/lang/Enum;
.source "Main.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/Main;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/memo/Main$Mode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACTION_SEND_AUDIO:Lcom/samsung/android/app/memo/Main$Mode;

.field public static final enum ACTION_SEND_IMAGE:Lcom/samsung/android/app/memo/Main$Mode;

.field public static final enum ACTION_SEND_TEXT:Lcom/samsung/android/app/memo/Main$Mode;

.field public static final enum CALLNOTE:Lcom/samsung/android/app/memo/Main$Mode;

.field public static final enum CALLNOTE_VIEW:Lcom/samsung/android/app/memo/Main$Mode;

.field public static final enum EDIT:Lcom/samsung/android/app/memo/Main$Mode;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/app/memo/Main$Mode;

.field public static final enum NORMAL:Lcom/samsung/android/app/memo/Main$Mode;

.field public static final enum PICK_ONE:Lcom/samsung/android/app/memo/Main$Mode;

.field public static final enum UNKNOWN:Lcom/samsung/android/app/memo/Main$Mode;

.field public static final enum VIEW:Lcom/samsung/android/app/memo/Main$Mode;

.field public static final values:[Lcom/samsung/android/app/memo/Main$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 149
    new-instance v0, Lcom/samsung/android/app/memo/Main$Mode;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/memo/Main$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/Main$Mode;->UNKNOWN:Lcom/samsung/android/app/memo/Main$Mode;

    .line 150
    new-instance v0, Lcom/samsung/android/app/memo/Main$Mode;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/memo/Main$Mode;-><init>(Ljava/lang/String;I)V

    .line 152
    sput-object v0, Lcom/samsung/android/app/memo/Main$Mode;->NORMAL:Lcom/samsung/android/app/memo/Main$Mode;

    .line 153
    new-instance v0, Lcom/samsung/android/app/memo/Main$Mode;

    const-string v1, "PICK_ONE"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/app/memo/Main$Mode;-><init>(Ljava/lang/String;I)V

    .line 156
    sput-object v0, Lcom/samsung/android/app/memo/Main$Mode;->PICK_ONE:Lcom/samsung/android/app/memo/Main$Mode;

    .line 157
    new-instance v0, Lcom/samsung/android/app/memo/Main$Mode;

    const-string v1, "VIEW"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/app/memo/Main$Mode;-><init>(Ljava/lang/String;I)V

    .line 159
    sput-object v0, Lcom/samsung/android/app/memo/Main$Mode;->VIEW:Lcom/samsung/android/app/memo/Main$Mode;

    .line 160
    new-instance v0, Lcom/samsung/android/app/memo/Main$Mode;

    const-string v1, "EDIT"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/app/memo/Main$Mode;-><init>(Ljava/lang/String;I)V

    .line 162
    sput-object v0, Lcom/samsung/android/app/memo/Main$Mode;->EDIT:Lcom/samsung/android/app/memo/Main$Mode;

    .line 163
    new-instance v0, Lcom/samsung/android/app/memo/Main$Mode;

    const-string v1, "CALLNOTE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/Main$Mode;-><init>(Ljava/lang/String;I)V

    .line 166
    sput-object v0, Lcom/samsung/android/app/memo/Main$Mode;->CALLNOTE:Lcom/samsung/android/app/memo/Main$Mode;

    .line 168
    new-instance v0, Lcom/samsung/android/app/memo/Main$Mode;

    const-string v1, "ACTION_SEND_TEXT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/Main$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_TEXT:Lcom/samsung/android/app/memo/Main$Mode;

    .line 170
    new-instance v0, Lcom/samsung/android/app/memo/Main$Mode;

    const-string v1, "ACTION_SEND_AUDIO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/Main$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_AUDIO:Lcom/samsung/android/app/memo/Main$Mode;

    .line 172
    new-instance v0, Lcom/samsung/android/app/memo/Main$Mode;

    const-string v1, "ACTION_SEND_IMAGE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/Main$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_IMAGE:Lcom/samsung/android/app/memo/Main$Mode;

    .line 173
    new-instance v0, Lcom/samsung/android/app/memo/Main$Mode;

    const-string v1, "CALLNOTE_VIEW"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/Main$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/Main$Mode;->CALLNOTE_VIEW:Lcom/samsung/android/app/memo/Main$Mode;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/samsung/android/app/memo/Main$Mode;

    sget-object v1, Lcom/samsung/android/app/memo/Main$Mode;->UNKNOWN:Lcom/samsung/android/app/memo/Main$Mode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/memo/Main$Mode;->NORMAL:Lcom/samsung/android/app/memo/Main$Mode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/app/memo/Main$Mode;->PICK_ONE:Lcom/samsung/android/app/memo/Main$Mode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/app/memo/Main$Mode;->VIEW:Lcom/samsung/android/app/memo/Main$Mode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/app/memo/Main$Mode;->EDIT:Lcom/samsung/android/app/memo/Main$Mode;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->CALLNOTE:Lcom/samsung/android/app/memo/Main$Mode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_TEXT:Lcom/samsung/android/app/memo/Main$Mode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_AUDIO:Lcom/samsung/android/app/memo/Main$Mode;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->ACTION_SEND_IMAGE:Lcom/samsung/android/app/memo/Main$Mode;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/app/memo/Main$Mode;->CALLNOTE_VIEW:Lcom/samsung/android/app/memo/Main$Mode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/memo/Main$Mode;->ENUM$VALUES:[Lcom/samsung/android/app/memo/Main$Mode;

    .line 174
    invoke-static {}, Lcom/samsung/android/app/memo/Main$Mode;->values()[Lcom/samsung/android/app/memo/Main$Mode;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/Main$Mode;->values:[Lcom/samsung/android/app/memo/Main$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/memo/Main$Mode;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/app/memo/Main$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/Main$Mode;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/memo/Main$Mode;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/app/memo/Main$Mode;->ENUM$VALUES:[Lcom/samsung/android/app/memo/Main$Mode;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/app/memo/Main$Mode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
