.class public Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;
.super Ljava/lang/Object;
.source "KVSItem.java"


# instance fields
.field private mDeleted:Z

.field private mId:J

.field private mSize:J

.field private mTimeStamp:J

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-wide v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mId:J

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mDeleted:Z

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mValue:Ljava/lang/String;

    .line 33
    iput-wide v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mSize:J

    .line 34
    return-void
.end method

.method public constructor <init>(JZJLjava/lang/String;J)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "bDeleted"    # Z
    .param p4, "timeStamp"    # J
    .param p6, "value"    # Ljava/lang/String;
    .param p7, "size"    # J

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-wide p1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mId:J

    .line 39
    iput-boolean p3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mDeleted:Z

    .line 40
    iput-wide p4, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mTimeStamp:J

    .line 41
    iput-object p6, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mValue:Ljava/lang/String;

    .line 42
    iput-wide p7, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mSize:J

    .line 43
    return-void
.end method


# virtual methods
.method public getID()J
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mId:J

    return-wide v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mSize:J

    return-wide v0
.end method

.method public getTimeStamp()J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mTimeStamp:J

    return-wide v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mDeleted:Z

    return v0
.end method

.method public setID(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mId:J

    .line 51
    return-void
.end method

.method public setTimeStamp(J)V
    .locals 1
    .param p1, "timeStamp"    # J

    .prologue
    .line 62
    iput-wide p1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mTimeStamp:J

    .line 63
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;->mValue:Ljava/lang/String;

    .line 71
    return-void
.end method
