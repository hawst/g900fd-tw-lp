.class public Lcom/samsung/android/app/memo/uiwidget/UndoBarController;
.super Ljava/lang/Object;
.source "UndoBarController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;
    }
.end annotation


# instance fields
.field private mBarAnimator:Landroid/view/ViewPropertyAnimator;

.field private mBarView:Landroid/view/View;

.field private mMessageView:Landroid/widget/TextView;

.field private mUndoListener:Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;

.field private mUndoMessage:Ljava/lang/CharSequence;

.field private mUndoToken:Landroid/os/Parcelable;

.field private mVisibility:Z


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;)V
    .locals 2
    .param p1, "undoBarView"    # Landroid/view/View;
    .param p2, "undoListener"    # Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mVisibility:Z

    .line 56
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarView:Landroid/view/View;

    .line 57
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarAnimator:Landroid/view/ViewPropertyAnimator;

    .line 58
    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoListener:Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;

    .line 60
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarView:Landroid/view/View;

    const v1, 0x7f0e003a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mMessageView:Landroid/widget/TextView;

    .line 61
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarView:Landroid/view/View;

    const v1, 0x7f0e003b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$1;-><init>(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->hideUndoBar(Z)V

    .line 70
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;)Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoListener:Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoToken:Landroid/os/Parcelable;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;)Landroid/view/View;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoMessage:Ljava/lang/CharSequence;

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoToken:Landroid/os/Parcelable;

    return-void
.end method


# virtual methods
.method public final hideUndoBar(Z)V
    .locals 4
    .param p1, "immediate"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mVisibility:Z

    .line 98
    if-eqz p1, :cond_0

    .line 99
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 101
    iput-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoMessage:Ljava/lang/CharSequence;

    .line 102
    iput-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoToken:Landroid/os/Parcelable;

    .line 121
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 106
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarAnimator:Landroid/view/ViewPropertyAnimator;

    .line 107
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 110
    const/high16 v2, 0x10e0000

    .line 109
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    .line 108
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 111
    new-instance v1, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$2;-><init>(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mVisibility:Z

    return v0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 129
    if-eqz p1, :cond_1

    .line 130
    const-string v0, "undo_message"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoMessage:Ljava/lang/CharSequence;

    .line 131
    const-string v0, "undo_token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoToken:Landroid/os/Parcelable;

    .line 133
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoToken:Landroid/os/Parcelable;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoMessage:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 134
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoMessage:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoToken:Landroid/os/Parcelable;

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->showUndoBar(ZLjava/lang/CharSequence;Landroid/os/Parcelable;)V

    .line 137
    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 124
    const-string v0, "undo_message"

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoMessage:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 125
    const-string v0, "undo_token"

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoToken:Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 126
    return-void
.end method

.method public final showUndoBar(ZLjava/lang/CharSequence;Landroid/os/Parcelable;)V
    .locals 4
    .param p1, "immediate"    # Z
    .param p2, "message"    # Ljava/lang/CharSequence;
    .param p3, "undoToken"    # Landroid/os/Parcelable;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mVisibility:Z

    .line 74
    iput-object p3, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoToken:Landroid/os/Parcelable;

    .line 75
    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoMessage:Ljava/lang/CharSequence;

    .line 76
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mMessageView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoMessage:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 83
    if-eqz p1, :cond_0

    .line 84
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 93
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarAnimator:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 87
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarAnimator:Landroid/view/ViewPropertyAnimator;

    .line 88
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 91
    const/high16 v2, 0x10e0000

    .line 90
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    .line 89
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 91
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method
