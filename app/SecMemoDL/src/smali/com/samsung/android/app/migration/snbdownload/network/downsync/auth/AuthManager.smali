.class public Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;
.super Ljava/lang/Object;
.source "AuthManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AuthManager"

.field private static mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field private mBaseUrlPrimary:Ljava/lang/String;

.field private mGetAPIparms:Ljava/lang/String;

.field private mPutAPIparms:Ljava/lang/String;

.field private mRegistrationID:Ljava/lang/String;

.field private mUserId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mUserId:Ljava/lang/String;

    .line 53
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mAccessToken:Ljava/lang/String;

    .line 54
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    .line 55
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    .line 56
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    .line 57
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mPutAPIparms:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public static declared-synchronized create()Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;
    .locals 2

    .prologue
    .line 63
    const-class v1, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;-><init>()V

    sput-object v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    .line 65
    :cond_0
    sget-object v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getAccessToken(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "expiredAccessToken"    # Ljava/lang/String;

    .prologue
    .line 101
    const/4 v0, 0x2

    invoke-static {p1, v0, p2}, Lcom/samsung/android/app/migration/utils/Utils;->sendRequestToSAService(Landroid/content/Context;ILjava/lang/String;)V

    .line 102
    return-void
.end method

.method public getBaseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    return-object v0
.end method

.method public getGetApiParams(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cid"    # Ljava/lang/String;

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPutApiParams(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "cid"    # Ljava/lang/String;

    .prologue
    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mPutAPIparms:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&cid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized updateAuthInformation(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p2, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;-><init>()V

    .line 86
    .local v0, "authinfo":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;
    invoke-virtual {v0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->getPushInfo(Landroid/content/Context;)V

    .line 87
    iget-object v1, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->baseUrl:Ljava/lang/String;

    .line 88
    .local v1, "url":Ljava/lang/String;
    const-string v2, "AuthManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateAuthInformation: baseUrl="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 90
    :cond_0
    const-string v2, "https://eu2-scloud-proxy.ssp.samsungosp.com"

    iput-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;

    .line 93
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mAccessToken:Ljava/lang/String;

    .line 94
    const/4 v2, 0x1

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    .line 95
    const/4 v2, 0x2

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mUserId:Ljava/lang/String;

    .line 96
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "&uid="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mUserId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&access_token="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    .line 97
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mGetAPIparms:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "&did="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mRegistrationID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mPutAPIparms:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    monitor-exit p0

    return-void

    .line 92
    :cond_1
    :try_start_1
    iget-object v2, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->baseUrl:Ljava/lang/String;

    iput-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->mBaseUrlPrimary:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 85
    .end local v0    # "authinfo":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;
    .end local v1    # "url":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
