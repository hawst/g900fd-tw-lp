.class Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;
.super Ljava/lang/Object;
.source "ZoomableImageViewBase.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->scrollBy(FFD)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field old_x:D

.field old_y:D

.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

.field private final synthetic val$durationMs:D

.field private final synthetic val$dx:D

.field private final synthetic val$dy:D

.field private final synthetic val$startTime:J


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;DJDD)V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    iput-wide p2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->val$durationMs:D

    iput-wide p4, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->val$startTime:J

    iput-wide p6, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->val$dx:D

    iput-wide p8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->val$dy:D

    .line 759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 761
    iput-wide v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->old_x:D

    .line 763
    iput-wide v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->old_y:D

    return-void
.end method


# virtual methods
.method public run()V
    .locals 20

    .prologue
    .line 767
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 768
    .local v14, "now":J
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->val$durationMs:D

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->val$startTime:J

    sub-long v10, v14, v10

    long-to-double v10, v10

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    .line 769
    .local v6, "currentMs":D
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->val$dx:D

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->val$durationMs:D

    invoke-virtual/range {v5 .. v13}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->easeOut(DDDD)D

    move-result-wide v16

    .line 770
    .local v16, "x":D
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    const-wide/16 v8, 0x0

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->val$dy:D

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->val$durationMs:D

    invoke-virtual/range {v5 .. v13}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->easeOut(DDDD)D

    move-result-wide v18

    .line 771
    .local v18, "y":D
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->old_x:D

    sub-double v8, v16, v8

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->old_y:D

    sub-double v10, v18, v10

    invoke-virtual {v5, v8, v9, v10, v11}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->panBy(DD)V

    .line 772
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->old_x:D

    .line 773
    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->old_y:D

    .line 774
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->val$durationMs:D

    cmpg-double v5, v6, v8

    if-gez v5, :cond_1

    .line 775
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    iget-object v5, v5, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 781
    :cond_0
    :goto_0
    return-void

    .line 777
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    iget-object v8, v8, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mSuppMatrix:Landroid/graphics/Matrix;

    const/4 v9, 0x1

    const/4 v10, 0x1

    invoke-virtual {v5, v8, v9, v10}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getCenter(Landroid/graphics/Matrix;ZZ)Landroid/graphics/RectF;

    move-result-object v4

    .line 778
    .local v4, "centerRect":Landroid/graphics/RectF;
    iget v5, v4, Landroid/graphics/RectF;->left:F

    const/4 v8, 0x0

    cmpl-float v5, v5, v8

    if-nez v5, :cond_2

    iget v5, v4, Landroid/graphics/RectF;->top:F

    const/4 v8, 0x0

    cmpl-float v5, v5, v8

    if-eqz v5, :cond_0

    .line 779
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    iget v8, v4, Landroid/graphics/RectF;->left:F

    iget v9, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v5, v8, v9}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->scrollBy(FF)V

    goto :goto_0
.end method
