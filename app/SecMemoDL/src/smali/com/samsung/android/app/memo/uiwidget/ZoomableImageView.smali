.class public Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;
.super Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;
.source "ZoomableImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;,
        Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchDoubleTapListener;,
        Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchSingleTapListener;,
        Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;
    }
.end annotation


# static fields
.field static final SCROLL_DELTA_THRESHOLD:F = 1.0f

.field private static final SHORT_PRESS_TIMEOUT:J = 0x190L


# instance fields
.field mActionBarHandler:Landroid/os/Handler;

.field protected mDoubleTapDirection:I

.field protected mDoubleTapEnabled:Z

.field private mDoubleTapListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchDoubleTapListener;

.field protected mGestureDetector:Landroid/view/GestureDetector;

.field protected mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

.field private mIsActionBarShowing:Z

.field private mLastDownMillis:J

.field protected mScaleDetector:Landroid/view/ScaleGestureDetector;

.field protected mScaleEnabled:Z

.field protected mScaleFactor:F

.field protected mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

.field protected mScrollEnabled:Z

.field private mSingleTapListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchSingleTapListener;

.field protected mTouchSlop:I

.field private move_action_bar:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 79
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;-><init>(Landroid/content/Context;)V

    .line 60
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapEnabled:Z

    .line 62
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleEnabled:Z

    .line 64
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScrollEnabled:Z

    .line 70
    const/16 v0, 0x4e91

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->move_action_bar:I

    .line 72
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mIsActionBarShowing:Z

    .line 74
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mLastDownMillis:J

    .line 231
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$1;-><init>(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mActionBarHandler:Landroid/os/Handler;

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x1

    .line 87
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapEnabled:Z

    .line 62
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleEnabled:Z

    .line 64
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScrollEnabled:Z

    .line 70
    const/16 v0, 0x4e91

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->move_action_bar:I

    .line 72
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mIsActionBarShowing:Z

    .line 74
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mLastDownMillis:J

    .line 231
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$1;-><init>(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mActionBarHandler:Landroid/os/Handler;

    .line 88
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;)Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mIsActionBarShowing:Z

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;)Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchSingleTapListener;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mSingleTapListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchSingleTapListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;)Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchDoubleTapListener;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchDoubleTapListener;

    return-object v0
.end method


# virtual methods
.method protected _setImageDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;FF)V
    .locals 2
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "initial_matrix"    # Landroid/graphics/Matrix;
    .param p3, "min_zoom"    # F
    .param p4, "max_zoom"    # F

    .prologue
    .line 138
    invoke-super {p0, p1, p2, p3, p4}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->_setImageDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;FF)V

    .line 139
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getMaxScale()F

    move-result v0

    const/high16 v1, 0x40400000    # 3.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleFactor:F

    .line 140
    return-void
.end method

.method public canScroll(I)Z
    .locals 8
    .param p1, "direction"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 259
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getBitmapRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 260
    .local v0, "bitmapRect":Landroid/graphics/RectF;
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScrollRect:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v6}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->updateRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 261
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 262
    .local v1, "imageViewRect":Landroid/graphics/Rect;
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 264
    if-nez v0, :cond_1

    .line 275
    :cond_0
    :goto_0
    return v4

    .line 268
    :cond_1
    iget v6, v0, Landroid/graphics/RectF;->right:F

    iget v7, v1, Landroid/graphics/Rect;->right:I

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-ltz v6, :cond_2

    .line 269
    if-gez p1, :cond_2

    .line 270
    iget v6, v0, Landroid/graphics/RectF;->right:F

    iget v7, v1, Landroid/graphics/Rect;->right:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    const/high16 v7, 0x3f800000    # 1.0f

    cmpl-float v6, v6, v7

    if-lez v6, :cond_0

    move v4, v5

    goto :goto_0

    .line 274
    :cond_2
    iget v6, v0, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScrollRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    float-to-double v2, v6

    .line 275
    .local v2, "bitmapScrollRectDelta":D
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v6, v2, v6

    if-lez v6, :cond_0

    move v4, v5

    goto :goto_0
.end method

.method public getDoubleTapEnabled()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapEnabled:Z

    return v0
.end method

.method protected getGestureListener()Landroid/view/GestureDetector$OnGestureListener;
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$GestureListener;-><init>(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;)V

    return-object v0
.end method

.method protected getScaleListener()Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$ScaleListener;-><init>(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;)V

    return-object v0
.end method

.method protected final init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x1

    .line 92
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 93
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mTouchSlop:I

    .line 94
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getGestureListener()Landroid/view/GestureDetector$OnGestureListener;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 95
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getScaleListener()Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    .line 97
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleListener:Landroid/view/ScaleGestureDetector$OnScaleGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 98
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 100
    iput v4, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapDirection:I

    .line 101
    return-void
.end method

.method protected onDoubleTapPost(FF)F
    .locals 2
    .param p1, "scale"    # F
    .param p2, "maxZoom"    # F

    .prologue
    const/4 v1, 0x1

    .line 167
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapDirection:I

    if-ne v0, v1, :cond_1

    .line 168
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleFactor:F

    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v0, v1

    add-float/2addr v0, p1

    cmpg-float v0, v0, p2

    if-gtz v0, :cond_0

    .line 169
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleFactor:F

    add-float p2, p1, v0

    .line 176
    .end local p2    # "maxZoom":F
    :goto_0
    return p2

    .line 171
    .restart local p2    # "maxZoom":F
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapDirection:I

    goto :goto_0

    .line 175
    :cond_1
    iput v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapDirection:I

    .line 176
    const/high16 p2, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 207
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 8
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v2, 0x1

    const/high16 v6, 0x44480000    # 800.0f

    const/high16 v5, 0x40000000    # 2.0f

    .line 194
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float v0, v3, v4

    .line 195
    .local v0, "diffX":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float v1, v3, v4

    .line 197
    .local v1, "diffY":F
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v6

    if-gtz v3, :cond_0

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v6

    if-lez v3, :cond_1

    .line 198
    :cond_0
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mUserScaled:Z

    .line 199
    div-float v3, v0, v5

    div-float v4, v1, v5

    const-wide v6, 0x4072c00000000000L    # 300.0

    invoke-virtual {p0, v3, v4, v6, v7}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->scrollBy(FFD)V

    .line 200
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->invalidate()V

    .line 203
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v0, 0x1

    .line 185
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getScale()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 186
    const/4 v0, 0x0

    .line 190
    :goto_0
    return v0

    .line 187
    :cond_0
    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mUserScaled:Z

    .line 188
    neg-float v1, p3

    neg-float v2, p4

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->scrollBy(FF)V

    .line 189
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->invalidate()V

    goto :goto_0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 181
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 219
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mLastDownMillis:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x190

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 221
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mLastDownMillis:J

    .line 226
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mActionBarHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->move_action_bar:I

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 227
    const/4 v0, 0x1

    return v0

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mActionBarHandler:Landroid/os/Handler;

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->move_action_bar:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 144
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 146
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v1

    if-nez v1, :cond_0

    .line 147
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 150
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 151
    .local v0, "action":I
    and-int/lit16 v1, v0, 0xff

    packed-switch v1, :pswitch_data_0

    .line 155
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 153
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->onUp(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0

    .line 151
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onUp(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getScale()F

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getMinScale()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 212
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getMinScale()F

    move-result v0

    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->zoomTo(FF)V

    .line 214
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onZoomAnimationCompleted(F)V
    .locals 2
    .param p1, "scale"    # F

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getMinScale()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 162
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->getMinScale()F

    move-result v0

    const/high16 v1, 0x42480000    # 50.0f

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->zoomTo(FF)V

    .line 164
    :cond_0
    return-void
.end method

.method public setActionBar(Z)V
    .locals 6
    .param p1, "show"    # Z

    .prologue
    .line 243
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mLastDownMillis:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x190

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 244
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mLastDownMillis:J

    .line 248
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 249
    .local v0, "a":Landroid/app/ActionBar;
    if-eqz p1, :cond_1

    .line 250
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mIsActionBarShowing:Z

    .line 251
    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 256
    .end local v0    # "a":Landroid/app/ActionBar;
    :cond_0
    :goto_0
    return-void

    .line 253
    .restart local v0    # "a":Landroid/app/ActionBar;
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mIsActionBarShowing:Z

    .line 254
    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    goto :goto_0
.end method

.method public setDoubleTapEnabled(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 112
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapEnabled:Z

    .line 113
    return-void
.end method

.method public setDoubleTapListener(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchDoubleTapListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchDoubleTapListener;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mDoubleTapListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchDoubleTapListener;

    .line 105
    return-void
.end method

.method public setScaleEnabled(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 116
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScaleEnabled:Z

    .line 117
    return-void
.end method

.method public setScrollEnabled(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 120
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mScrollEnabled:Z

    .line 121
    return-void
.end method

.method public setSingleTapListener(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchSingleTapListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchSingleTapListener;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView;->mSingleTapListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageView$OnImageViewTouchSingleTapListener;

    .line 109
    return-void
.end method
