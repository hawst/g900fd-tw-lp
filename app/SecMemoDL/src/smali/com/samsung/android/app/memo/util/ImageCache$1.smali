.class Lcom/samsung/android/app/memo/util/ImageCache$1;
.super Landroid/support/v4/util/LruCache;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/util/ImageCache;->init(Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/util/ImageCache;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/util/ImageCache;I)V
    .locals 0
    .param p2, "$anonymous0"    # I

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/ImageCache$1;->this$0:Lcom/samsung/android/app/memo/util/ImageCache;

    .line 79
    invoke-direct {p0, p2}, Landroid/support/v4/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/app/memo/util/ImageCache$1;->sizeOf(Ljava/lang/String;Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method

.method protected sizeOf(Ljava/lang/String;Landroid/graphics/Bitmap;)I
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 82
    invoke-static {p2}, Lcom/samsung/android/app/memo/util/ImageCache;->getBitmapSize(Landroid/graphics/Bitmap;)I

    move-result v1

    div-int/lit16 v0, v1, 0x400

    .line 83
    .local v0, "bitmapSize":I
    if-nez v0, :cond_0

    const/4 v0, 0x1

    .end local v0    # "bitmapSize":I
    :cond_0
    return v0
.end method
