.class Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$3;
.super Landroid/widget/SimpleCursorAdapter;
.source "CategoryChooserDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->getCursorAdapter()Landroid/widget/CursorAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V
    .locals 7
    .param p2, "$anonymous0"    # Landroid/content/Context;
    .param p3, "$anonymous1"    # I
    .param p4, "$anonymous2"    # Landroid/database/Cursor;
    .param p5, "$anonymous3"    # [Ljava/lang/String;
    .param p6, "$anonymous4"    # [I
    .param p7, "$anonymous5"    # I

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move v6, p7

    .line 169
    invoke-direct/range {v0 .. v6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "ctx"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 175
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleCursorAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 177
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 179
    const v1, 0x7f0e0002

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 181
    .local v0, "radioBtn":Landroid/widget/RadioButton;
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->access$3(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Session;->getCategoryUUID()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$3;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->headerRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 184
    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 191
    :goto_0
    return-void

    .line 188
    :cond_0
    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0
.end method
