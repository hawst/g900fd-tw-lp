.class public final Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;
.super Landroid/app/Fragment;
.source "MemoContentFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Lcom/samsung/android/app/memo/uifragment/MemoKeyEventCallback;
.implements Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;
.implements Lcom/samsung/android/app/memo/uiwidget/RichEditor$EditViewModeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;,
        Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;
    }
.end annotation


# static fields
.field private static final ACTIVITY_REQUEST_PICK_IMAGE:I = 0x4e21

.field private static final ACTIVITY_REQUEST_TAKE_PHOTO:I = 0x4e23

.field private static final ACTIVITY_REQUEST_VIEW_IMAGE:I = 0x4e22

.field public static final FILE_TYPE_IMAGE:Ljava/lang/String; = "image"

.field public static final IS_FROM_DRAG_LISTENER:Ljava/lang/String;

.field public static final KEY_ACTION_SEND_AUDIO_URI:Ljava/lang/String;

.field public static final KEY_ACTION_SEND_IMAGE_URI:Ljava/lang/String;

.field public static final KEY_BODYSTRINGEXTRA:Ljava/lang/String;

.field public static final KEY_CATEGORY_UUID:Ljava/lang/String;

.field public static final KEY_DURINGCALL:Ljava/lang/String;

.field public static final KEY_EDITMODE:Ljava/lang/String;

.field public static final KEY_HTML_STRING:Ljava/lang/String;

.field public static final KEY_INSERT_SHOWING:Ljava/lang/String;

.field public static final KEY_PHONENUMBER:Ljava/lang/String;

.field public static final KEY_REQUIRED_ONE_TIME_EXEC:Ljava/lang/String;

.field public static final KEY_TEMPURI:Ljava/lang/String;

.field public static final KEY_TITLEPREFIX:Ljava/lang/String;

.field public static final KEY_TITLESTRINGEXTRA:Ljava/lang/String;

.field public static final KEY_VNTFILE:Ljava/lang/String;

.field public static final KEY_VNTTEXT:Ljava/lang/String;

.field public static final KEY_VOICEMODE:Ljava/lang/String;

.field private static final MIMETYPE_IMAGE_ALL:Ljava/lang/String; = "image/*"

.field private static final TAG:Ljava/lang/String;

.field private static lastCategoryClick:J = 0x0L

.field private static lastInsertClick:J = 0x0L

.field private static mLastSpanClickPosition:I = 0x0

.field private static final minCategoryClickDiff:J = 0x1f4L


# instance fields
.field private final DELAY_COMMON:I

.field private final DELAY_DIALOG_KEYBOARD:I

.field private final DELAY_UPDATE:I

.field private final HIDE_KEYBOARD:I

.field private final IMAGE_PICKER_THEME:Ljava/lang/String;

.field private final LIGHT_THEME:I

.field private final MEMO_THRESHOLD_TIME:J

.field private final SHOW_KEYBOARD:I

.field dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

.field editTextFocusListener:Landroid/view/View$OnFocusChangeListener;

.field public inputFilter:Landroid/text/InputFilter;

.field private isDialogShowing:Z

.field private isFromShareVia:Z

.field isRecreated:Z

.field keypadHandler:Landroid/os/Handler;

.field public lengthFilter:Landroid/text/InputFilter$LengthFilter;

.field limit:I

.field private mAddImagesTask:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;

.field private mAddPictureDialog:Landroid/app/AlertDialog;

.field private mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

.field private mCancelDialog:Landroid/app/AlertDialog;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

.field private mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

.field private mFirstVisableCharacterOffset:I

.field private mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

.field private mInsertButton:Landroid/widget/ImageButton;

.field private mIsDialogConfigChange:Z

.field private mIsDiscardBackPressed:Z

.field private mIsEditMode:Z

.field private mIsFirstTime:Z

.field private mIsFromOnActivityResult:Z

.field private mIsFromVnt:Z

.field private mIsFromWidget:Z

.field private mIsInsertOptionsShowing:Z

.field private mIsMemohasVoicedata:Z

.field private mIsRequiredToexecuteOneTime:Z

.field private mIsSavingThenClose:Z

.field private mIsVoiceButtonEnable:Z

.field private mIsVoiceLayoutVisible:Z

.field private mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

.field private mMainLayout:Landroid/widget/FrameLayout;

.field private mMemoId:J

.field private mMenu:Landroid/view/Menu;

.field public final mMessageHandler:Landroid/os/Handler;

.field private mNeedToShowClipboard:Z

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private final mOnGLobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private mPhoneStateReceiver:Landroid/content/BroadcastReceiver;

.field public mProgress:Landroid/app/ProgressDialog;

.field mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

.field private mScrollView:Landroid/widget/ScrollView;

.field private mShadowImage:Landroid/widget/ImageView;

.field mShowImageNotFoundHandler:Landroid/os/Handler;

.field private mStrHtml:Ljava/lang/String;

.field private mStubInflate:Z

.field private mTempImageUri:Landroid/net/Uri;

.field private mTitle:Landroid/widget/EditText;

.field private mVoiceButton:Landroid/widget/ImageButton;

.field private mVoiceDeleteLayout:Landroid/widget/LinearLayout;

.field private mVoiceMainLayoutStub:Landroid/view/ViewStub;

.field private mVoiceRecorderProxy:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

.field private mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

.field private mWidgetID:J

.field private memoId:J

.field private onTouchViewListnerForScrollView:Landroid/view/View$OnTouchListener;

.field private thresholdImageAdd:Landroid/os/CountDownTimer;

.field private tv:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 149
    const-class v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_isEditMode"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_EDITMODE:Ljava/lang/String;

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_tempUri"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TEMPURI:Ljava/lang/String;

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_isVoiceMode"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VOICEMODE:Ljava/lang/String;

    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_categoryUUID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_duringcall"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_DURINGCALL:Ljava/lang/String;

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_titleprefix"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TITLEPREFIX:Ljava/lang/String;

    .line 163
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_phoneNum"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_PHONENUMBER:Ljava/lang/String;

    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_actionSendImageUri"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_ACTION_SEND_IMAGE_URI:Ljava/lang/String;

    .line 167
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_actionSendAudioUri"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_ACTION_SEND_AUDIO_URI:Ljava/lang/String;

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_stringSendExtra"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_BODYSTRINGEXTRA:Ljava/lang/String;

    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_titleSendExtra"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TITLESTRINGEXTRA:Ljava/lang/String;

    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_isInsertOptionsShowing"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_INSERT_SHOWING:Ljava/lang/String;

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "isFromDragListener"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->IS_FROM_DRAG_LISTENER:Ljava/lang/String;

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_isRequiredToexecuteOneTime"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_REQUIRED_ONE_TIME_EXEC:Ljava/lang/String;

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_isFromVNT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VNTFILE:Ljava/lang/String;

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_vntText"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VNTTEXT:Ljava/lang/String;

    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "htmlString"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_HTML_STRING:Ljava/lang/String;

    .line 207
    sput-wide v2, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->lastCategoryClick:J

    .line 209
    sput-wide v2, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->lastInsertClick:J

    .line 213
    const/4 v0, -0x1

    sput v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mLastSpanClickPosition:I

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v2, 0x7530

    const-wide/16 v6, -0x1

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 145
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 189
    const-string v0, "theme"

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->IMAGE_PICKER_THEME:Ljava/lang/String;

    .line 191
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->LIGHT_THEME:I

    .line 199
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsDialogConfigChange:Z

    .line 201
    iput-wide v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->memoId:J

    .line 226
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    .line 230
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z

    .line 232
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFirstTime:Z

    .line 238
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mNeedToShowClipboard:Z

    .line 242
    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    .line 248
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mStubInflate:Z

    .line 255
    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 257
    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTempImageUri:Landroid/net/Uri;

    .line 264
    iput-wide v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMemoId:J

    .line 274
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsMemohasVoicedata:Z

    .line 276
    iput-boolean v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceButtonEnable:Z

    .line 282
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsInsertOptionsShowing:Z

    .line 284
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsRequiredToexecuteOneTime:Z

    .line 288
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFromWidget:Z

    .line 290
    iput-wide v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mWidgetID:J

    .line 292
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFromVnt:Z

    .line 294
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFromOnActivityResult:Z

    .line 296
    iput-boolean v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsSavingThenClose:Z

    .line 302
    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;

    .line 308
    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 310
    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 314
    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mPhoneStateReceiver:Landroid/content/BroadcastReceiver;

    .line 316
    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

    .line 322
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->isDialogShowing:Z

    .line 327
    iput v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->HIDE_KEYBOARD:I

    .line 328
    iput v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->SHOW_KEYBOARD:I

    .line 330
    iput v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->DELAY_COMMON:I

    .line 331
    const/16 v0, 0x32

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->DELAY_DIALOG_KEYBOARD:I

    .line 332
    const/16 v0, 0xc8

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->DELAY_UPDATE:I

    .line 392
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->isRecreated:Z

    .line 476
    iput-wide v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->MEMO_THRESHOLD_TIME:J

    .line 754
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$1;

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$1;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;JJ)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->thresholdImageAdd:Landroid/os/CountDownTimer;

    .line 1472
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$2;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mOnGLobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1675
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$3;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->editTextFocusListener:Landroid/view/View$OnFocusChangeListener;

    .line 2047
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$4;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShowImageNotFoundHandler:Landroid/os/Handler;

    .line 2239
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$5;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->onTouchViewListnerForScrollView:Landroid/view/View$OnTouchListener;

    .line 2367
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->limit:I

    .line 2369
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$6;

    iget v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->limit:I

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$6;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;I)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->lengthFilter:Landroid/text/InputFilter$LengthFilter;

    .line 2381
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$7;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->inputFilter:Landroid/text/InputFilter;

    .line 2397
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$8;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMessageHandler:Landroid/os/Handler;

    .line 2505
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$9;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    .line 145
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddImagesTask:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V
    .locals 0

    .prologue
    .line 1488
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->restoreScrollViewPosition()V

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V
    .locals 0

    .prologue
    .line 2390
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->enableDisableVoiceButton(Z)V

    return-void
.end method

.method static synthetic access$11(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Lcom/samsung/android/app/memo/voice/VoiceInfo;)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V
    .locals 0

    .prologue
    .line 274
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsMemohasVoicedata:Z

    return-void
.end method

.method static synthetic access$13(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V
    .locals 0

    .prologue
    .line 230
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Z
    .locals 1

    .prologue
    .line 230
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z

    return v0
.end method

.method static synthetic access$15(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/view/Menu;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$16(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$17(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/os/CountDownTimer;
    .locals 1

    .prologue
    .line 754
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->thresholdImageAdd:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$18(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1061
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getDrawableFromLocalUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$19(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;)Z
    .locals 1

    .prologue
    .line 2053
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->insertImage(Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$21()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$22(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 2224
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getAttachmentList()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$23(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceInfo;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    return-object v0
.end method

.method static synthetic access$24(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->tv:Landroid/widget/TextView;

    return-void
.end method

.method static synthetic access$25(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->tv:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$26(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V
    .locals 0

    .prologue
    .line 1740
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->setRecordingPlay()V

    return-void
.end method

.method static synthetic access$27(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V
    .locals 0

    .prologue
    .line 1497
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->updateVoiceButtonState()V

    return-void
.end method

.method static synthetic access$28(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V
    .locals 0

    .prologue
    .line 2130
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->cancelCurrentContent(Z)V

    return-void
.end method

.method static synthetic access$29(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V
    .locals 0

    .prologue
    .line 296
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsSavingThenClose:Z

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$30(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V
    .locals 0

    .prologue
    .line 2074
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->openImagePicker()V

    return-void
.end method

.method static synthetic access$31(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 1868
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddPictureDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$32(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V
    .locals 0

    .prologue
    .line 2083
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->openImageTaker()V

    return-void
.end method

.method static synthetic access$33(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V
    .locals 0

    .prologue
    .line 282
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsInsertOptionsShowing:Z

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Z
    .locals 1

    .prologue
    .line 226
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V
    .locals 0

    .prologue
    .line 276
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceButtonEnable:Z

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Z)V
    .locals 0

    .prologue
    .line 226
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private cancelCurrentContent(Z)V
    .locals 3
    .param p1, "changeToView"    # Z

    .prologue
    .line 2131
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v2, "cancelCurrentContent()"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2132
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v0

    .line 2133
    .local v0, "ss":Lcom/samsung/android/app/memo/Session;
    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Session;->cancelEditing()V

    .line 2135
    if-nez p1, :cond_0

    .line 2138
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsDiscardBackPressed:Z

    .line 2139
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->onFragmentSuicide(Ljava/lang/String;)V

    .line 2142
    :cond_0
    return-void
.end method

.method private cancelOrSave()V
    .locals 8

    .prologue
    .line 2159
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v4}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v2

    .line 2161
    .local v2, "ss":Lcom/samsung/android/app/memo/Session;
    const-string v3, ""

    .line 2162
    .local v3, "title":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    if-eqz v4, :cond_1

    .line 2163
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2165
    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Session;->isDraft()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_0

    .line 2166
    const-string v3, ""

    .line 2168
    :cond_0
    invoke-virtual {v2, v3}, Lcom/samsung/android/app/memo/Session;->setTitle(Ljava/lang/String;)V

    .line 2171
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getHtml()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2173
    .local v0, "content":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Session;->isDraft()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_2

    .line 2174
    const-string v0, ""

    .line 2176
    :cond_2
    invoke-virtual {v2, v0}, Lcom/samsung/android/app/memo/Session;->setContent(Ljava/lang/String;)V

    .line 2178
    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Session;->isDraft()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Session;->isDirty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 2179
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "cancelOrSave() cancel: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2180
    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Session;->cancelEditing()V

    .line 2181
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f0b000b

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 2196
    :goto_0
    return-void

    .line 2182
    :cond_3
    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Session;->isDirty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2183
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "cancelOrSave() save: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2185
    :try_start_0
    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Session;->saveMemo()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->memoId:J

    .line 2186
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/app/memo/util/Utils;->refreshToast(Landroid/content/Context;)V

    .line 2187
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f0b000c

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2192
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->sendUpdateAllWidgetBroadcastEvent()V

    goto :goto_0

    .line 2188
    :catch_0
    move-exception v1

    .line 2189
    .local v1, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v5, "cancelOrSave()"

    invoke-static {v4, v5, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2194
    .end local v1    # "se":Lcom/samsung/android/app/memo/Session$SessionException;
    :cond_4
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v5, "cancelOrSave() does nothing"

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private checkVoiceRecord()V
    .locals 8

    .prologue
    .line 1814
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v3}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v1

    .line 1816
    .local v1, "ss":Lcom/samsung/android/app/memo/Session;
    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Session;->getVrFileUUID()Ljava/lang/String;

    move-result-object v0

    .line 1818
    .local v0, "mVoiceId":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1819
    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsMemohasVoicedata:Z

    .line 1820
    iget-boolean v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsMemohasVoicedata:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-nez v3, :cond_1

    .line 1821
    new-instance v3, Lcom/samsung/android/app/memo/voice/VoiceInfo;

    invoke-direct {v3}, Lcom/samsung/android/app/memo/voice/VoiceInfo;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 1831
    :cond_1
    :goto_0
    return-void

    .line 1824
    :cond_2
    sget-object v3, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_FILE:Landroid/net/Uri;

    invoke-static {v3, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1825
    .local v2, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-wide v4, v3, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTotalTime:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    .line 1826
    new-instance v3, Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-wide v4, v4, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTotalTime:J

    invoke-direct {v3, v2, v4, v5}, Lcom/samsung/android/app/memo/voice/VoiceInfo;-><init>(Landroid/net/Uri;J)V

    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 1829
    :goto_1
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsMemohasVoicedata:Z

    goto :goto_0

    .line 1828
    :cond_3
    new-instance v3, Lcom/samsung/android/app/memo/voice/VoiceInfo;

    invoke-direct {v3, v2}, Lcom/samsung/android/app/memo/voice/VoiceInfo;-><init>(Landroid/net/Uri;)V

    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    goto :goto_1
.end method

.method private clearObjects()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1407
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v1, " clearObjects()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1409
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 1410
    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceButton:Landroid/widget/ImageButton;

    .line 1412
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mInsertButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 1413
    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mInsertButton:Landroid/widget/ImageButton;

    .line 1415
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 1416
    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;

    .line 1418
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCancelDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    .line 1419
    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCancelDialog:Landroid/app/AlertDialog;

    .line 1421
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    if-eqz v0, :cond_4

    .line 1422
    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    .line 1424
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceDeleteLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_5

    .line 1425
    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceDeleteLayout:Landroid/widget/LinearLayout;

    .line 1427
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_6

    .line 1428
    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    .line 1430
    :cond_6
    return-void
.end method

.method private createImageFile()Ljava/io/File;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2100
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyyMMdd_HHmmss"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 2101
    .local v3, "timeStamp":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "JPEG_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2103
    .local v1, "imageFileName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v5, Landroid/os/Environment;->DIRECTORY_DCIM:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/Camera"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 2105
    .local v2, "storageDir":Ljava/io/File;
    const-string v4, ".jpg"

    .line 2104
    invoke-static {v1, v4, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    .line 2108
    .local v0, "image":Ljava/io/File;
    return-object v0
.end method

.method private deletingCurrentContent()V
    .locals 8

    .prologue
    .line 2199
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v5, "deletingCurrentContent()"

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v4}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v3

    .line 2201
    .local v3, "ss":Lcom/samsung/android/app/memo/Session;
    invoke-virtual {v3}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_2

    .line 2202
    invoke-virtual {v3}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->memoId:J

    .line 2204
    :try_start_0
    iget-boolean v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v4, :cond_0

    .line 2205
    invoke-virtual {v3}, Lcom/samsung/android/app/memo/Session;->saveMemo()J

    .line 2206
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/samsung/android/app/memo/util/Utils;->SetIsFromDeleteMemo(Z)V

    .line 2208
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2209
    .local v1, "mIDsBeingDeleted":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v3}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2210
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/samsung/android/app/memo/MemoDataHandler;->deletingMemos(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 2211
    iget-boolean v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFromWidget:Z

    if-eqz v4, :cond_1

    .line 2212
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/samsung/android/app/memo/MemoDataHandler;->deleteMemos(Landroid/content/Context;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2219
    .end local v1    # "mIDsBeingDeleted":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    const/4 v5, 0x0

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v6

    long-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->sendMessage(ILjava/lang/Object;)V

    .line 2221
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getTag()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->onFragmentSuicide(Ljava/lang/String;)V

    .line 2222
    return-void

    .line 2214
    :catch_0
    move-exception v0

    .line 2215
    .local v0, "e":Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v5, "deletingCurrentContent()"

    invoke-static {v4, v5, v0}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2216
    .end local v0    # "e":Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
    :catch_1
    move-exception v2

    .line 2217
    .local v2, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v5, "deletingCurrentContent()"

    invoke-static {v4, v5, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private enableDisableVoiceButton(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 2391
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 2392
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2393
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceButtonEnable:Z

    .line 2395
    :cond_0
    return-void
.end method

.method private galleryAddPic(Landroid/net/Uri;)V
    .locals 2
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 2042
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2043
    .local v0, "mediaScanIntent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2044
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2045
    return-void
.end method

.method private getAttachmentList()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2225
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2226
    .local v4, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v3, Landroid/text/SpannableStringBuilder;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2227
    .local v3, "span":Landroid/text/SpannableStringBuilder;
    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    const-class v7, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    invoke-virtual {v3, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    .line 2228
    .local v1, "objArr":[Ljava/lang/Object;
    if-eqz v1, :cond_0

    .line 2229
    array-length v6, v1

    :goto_0
    if-lt v5, v6, :cond_1

    .line 2236
    :cond_0
    return-object v4

    .line 2229
    :cond_1
    aget-object v0, v1, v5

    .line 2230
    .local v0, "obj":Ljava/lang/Object;
    instance-of v7, v0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    if-eqz v7, :cond_2

    move-object v2, v0

    .line 2231
    check-cast v2, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    .line 2232
    .local v2, "sp":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->getContentUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2229
    .end local v2    # "sp":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method private getDrawableFromLocalUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "mContentUri"    # Landroid/net/Uri;

    .prologue
    .line 1062
    sget-object v5, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getDrawableFromLocalUri() uri: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063
    const/4 v0, 0x0

    .line 1066
    .local v0, "bm":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-static {p1}, Lcom/samsung/android/app/memo/util/ImageUtil;->lookupImageInfo(Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v1

    .line 1067
    .local v1, "bundle":Landroid/os/Bundle;
    sget-object v5, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_ORIENTATION:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 1068
    .local v4, "orientation":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5, p1, v4}, Lcom/samsung/android/app/memo/util/ImageUtil;->getActualImageWidth(Landroid/content/Context;Landroid/net/Uri;I)I

    move-result v2

    .line 1069
    .local v2, "imageWidth":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 1070
    sget v6, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->MAX_WIDTH:I

    .line 1069
    invoke-static {v5, p1, v2, v4, v6}, Lcom/samsung/android/app/memo/util/ImageUtil;->decodeImageScaledIf(Landroid/content/Context;Landroid/net/Uri;III)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1075
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "imageWidth":I
    .end local v4    # "orientation":I
    :goto_0
    return-object v0

    .line 1071
    :catch_0
    move-exception v3

    .line 1072
    .local v3, "ioe":Ljava/io/IOException;
    sget-object v5, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Failed to loaded content "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getUiPickerMode()Z
    .locals 2

    .prologue
    .line 2553
    const/4 v0, 0x0

    .line 2554
    .local v0, "ispicker":Z
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/samsung/android/app/memo/MemoContentActivity;

    if-eqz v1, :cond_0

    .line 2555
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/memo/MemoContentActivity;

    iget-boolean v0, v1, Lcom/samsung/android/app/memo/MemoContentActivity;->isUiPickerMode:Z

    .line 2557
    :cond_0
    return v0
.end method

.method private hideApplicationIcon()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 618
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x102002c

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 619
    .local v0, "homeView":Landroid/view/View;
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 620
    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 621
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 622
    .local v1, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 623
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 624
    return-void
.end method

.method private hideNotification()V
    .locals 2

    .prologue
    .line 1525
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/android/app/memo/MemoApp;->setShowNotificationEnabled(Z)V

    .line 1526
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mNotificationManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_0

    .line 1527
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mNotificationManager:Landroid/app/NotificationManager;

    const v1, 0x5411111

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1528
    :cond_0
    return-void
.end method

.method private initSConnect(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 410
    sget-boolean v0, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SLOOKBEZELINTERACTION_API:Z

    if-eqz v0, :cond_0

    .line 412
    const-string v0, "quickconnect"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/quickconnect/QuickConnectManager;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 413
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 441
    :cond_0
    return-void
.end method

.method private initToolbar()V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 546
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 547
    .local v0, "ab":Landroid/app/ActionBar;
    iget-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v6, :cond_6

    .line 549
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 551
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v6

    if-nez v6, :cond_3

    .line 552
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f04001a

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 553
    .local v4, "v":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 569
    :cond_0
    :goto_0
    const/4 v6, 0x3

    new-array v2, v6, [I

    fill-array-data v2, :array_0

    .line 572
    .local v2, "btns":[I
    array-length v6, v2

    :goto_1
    if-lt v5, v6, :cond_5

    .line 574
    invoke-static {v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getInsertButtonView(Landroid/view/View;)V

    .line 575
    const v5, 0x7f0e0033

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceButton:Landroid/widget/ImageButton;

    .line 576
    const v5, 0x7f0e0032

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mInsertButton:Landroid/widget/ImageButton;

    .line 578
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->updateInsertButton()V

    .line 580
    iget-boolean v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsMemohasVoicedata:Z

    if-eqz v5, :cond_1

    iget-boolean v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceButtonEnable:Z

    if-nez v5, :cond_1

    .line 581
    iget-boolean v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceButtonEnable:Z

    invoke-direct {p0, v5}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->enableDisableVoiceButton(Z)V

    .line 613
    .end local v2    # "btns":[I
    .end local v4    # "v":Landroid/view/View;
    :cond_1
    :goto_2
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v5

    if-nez v5, :cond_2

    .line 614
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->hideApplicationIcon()V

    .line 615
    :cond_2
    return-void

    .line 555
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f0e002e

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 556
    .restart local v4    # "v":Landroid/view/View;
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 557
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f0e0049

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 558
    .local v3, "title":Landroid/widget/TextView;
    if-eqz v3, :cond_4

    .line 559
    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 560
    :cond_4
    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 561
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 562
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 563
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 564
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v6}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/Session;->isDraft()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 565
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f0b0043

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 572
    .end local v3    # "title":Landroid/widget/TextView;
    .restart local v2    # "btns":[I
    :cond_5
    aget v1, v2, v5

    .line 573
    .local v1, "btn":I
    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 572
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 584
    .end local v1    # "btn":I
    .end local v2    # "btns":[I
    .end local v4    # "v":Landroid/view/View;
    :cond_6
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v6

    if-nez v6, :cond_7

    .line 585
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 586
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    new-instance v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$11;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    .line 608
    const-wide/16 v8, 0x0

    .line 586
    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    .line 610
    :cond_7
    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 611
    invoke-virtual {v0, v8}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto/16 :goto_2

    .line 569
    :array_0
    .array-data 4
        0x7f0e0031
        0x7f0e0032
        0x7f0e0033
    .end array-data
.end method

.method private initView(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 482
    const v0, 0x7f0e002c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 483
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->editTextFocusListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 484
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSpanOnClickListener(Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;)V

    .line 485
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setEditViewModeChangeListener(Lcom/samsung/android/app/memo/uiwidget/RichEditor$EditViewModeListener;)V

    .line 486
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScrollView:Landroid/widget/ScrollView;

    new-instance v1, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 487
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    new-instance v1, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    .line 488
    invoke-interface {v4}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;-><init>(Landroid/content/Context;Lcom/samsung/android/app/memo/uiwidget/RichEditor;Lcom/samsung/android/app/memo/Session;)V

    .line 487
    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 491
    const v0, 0x7f0e002b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    .line 492
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/text/InputFilter;

    .line 493
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->inputFilter:Landroid/text/InputFilter;

    aput-object v2, v1, v5

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->lengthFilter:Landroid/text/InputFilter$LengthFilter;

    aput-object v2, v1, v6

    .line 492
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 496
    const v0, 0x7f0e006e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceDeleteLayout:Landroid/widget/LinearLayout;

    .line 498
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 499
    const v0, 0x7f0e0029

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;

    .line 501
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->editTextFocusListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 502
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Session;->isDraft()Z

    move-result v0

    if-nez v0, :cond_2

    .line 503
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->requestFocus()Z

    .line 504
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0, v5}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setFocusable(Z)V

    .line 505
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 506
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 507
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 508
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 509
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 510
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setHint(Ljava/lang/CharSequence;)V

    .line 511
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, p0}, Landroid/widget/ScrollView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 517
    :goto_0
    iput-boolean v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsDiscardBackPressed:Z

    .line 518
    sget-boolean v0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z

    if-eqz v0, :cond_4

    .line 519
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/memo/voice/VoiceRecorderSecMediaImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceRecorderProxy:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    .line 523
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->requestFocus()Z

    .line 524
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsInsertOptionsShowing:Z

    if-eqz v0, :cond_1

    .line 525
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 526
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->showAddPictureDialog()V

    .line 529
    :cond_1
    return-void

    .line 513
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 514
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 515
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setEditMode(Z)V

    goto :goto_0

    .line 521
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceRecorderProxy:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    goto :goto_1
.end method

.method private insertImage(Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;)Z
    .locals 2
    .param p1, "attachmentUri"    # Landroid/net/Uri;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "orientation"    # I
    .param p4, "altText"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 2056
    if-nez p2, :cond_0

    .line 2057
    const-string p2, "image/jpeg"

    .line 2059
    :cond_0
    if-nez p1, :cond_2

    .line 2071
    :cond_1
    :goto_0
    return v0

    .line 2063
    :cond_2
    const-string v1, "image"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2071
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0, p1, p3, p4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->insertImage(Landroid/net/Uri;ILjava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private makeNewVoiceControl()V
    .locals 8

    .prologue
    .line 1792
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceRecorderProxy:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 1793
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMessageHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;-><init>(Landroid/content/Context;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/view/View;ZLandroid/os/Handler;Landroid/app/NotificationManager;)V

    .line 1792
    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 1794
    return-void
.end method

.method private makeVoiceControlLayout(Lcom/samsung/android/app/memo/voice/VoiceInfo;)V
    .locals 9
    .param p1, "mCurrentVoiceInfo2"    # Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .prologue
    const/4 v5, 0x0

    .line 1797
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-nez v0, :cond_1

    .line 1798
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsMemohasVoicedata:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v0, :cond_0

    .line 1799
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceRecorderProxy:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    .line 1800
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMessageHandler:Landroid/os/Handler;

    .line 1801
    iget-object v8, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-direct/range {v0 .. v8}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;-><init>(Landroid/content/Context;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/view/View;ZZLandroid/os/Handler;Landroid/app/NotificationManager;)V

    .line 1799
    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 1811
    :goto_0
    return-void

    .line 1803
    :cond_0
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceRecorderProxy:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    .line 1804
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMessageHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;-><init>(Landroid/content/Context;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/view/View;ZLandroid/os/Handler;Landroid/app/NotificationManager;)V

    .line 1803
    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    goto :goto_0

    .line 1807
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceRecorderProxy:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 1808
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    .line 1807
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setDioVoiceViewStatus(Landroid/content/Context;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/view/View;)V

    .line 1809
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setContentHandler(Landroid/os/Handler;)V

    goto :goto_0
.end method

.method private openImagePicker()V
    .locals 4

    .prologue
    .line 2076
    const-string v0, "android.intent.action.GET_CONTENT"

    .line 2077
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2078
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "image/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2079
    const-string v2, "theme"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2080
    const/16 v2, 0x4e21

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2081
    return-void
.end method

.method private openImageTaker()V
    .locals 5

    .prologue
    .line 2087
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2089
    .local v0, "intent":Landroid/content/Intent;
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->createImageFile()Ljava/io/File;

    move-result-object v2

    .line 2090
    .local v2, "outFile":Ljava/io/File;
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTempImageUri:Landroid/net/Uri;

    .line 2091
    const-string v3, "output"

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTempImageUri:Landroid/net/Uri;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2095
    .end local v2    # "outFile":Ljava/io/File;
    :goto_0
    const/16 v3, 0x4e23

    invoke-virtual {p0, v0, v3}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2096
    return-void

    .line 2092
    :catch_0
    move-exception v1

    .line 2093
    .local v1, "ioe":Ljava/io/IOException;
    sget-object v3, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v4, "openImageTaker()"

    invoke-static {v3, v4, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private openImageView(Landroid/net/Uri;I)V
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "orientation"    # I

    .prologue
    .line 2115
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 2116
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 2118
    const-string v3, "input_method"

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 2117
    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 2119
    .local v1, "im":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    if-eqz v3, :cond_0

    .line 2120
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2123
    .end local v1    # "im":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lcom/samsung/android/app/memo/ImageViewActivity;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    .line 2124
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2125
    sget-object v3, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->KEY_ORIENTATION:Ljava/lang/String;

    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2126
    sget-object v3, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->KEY_CLICK_POS:Ljava/lang/String;

    sget v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mLastSpanClickPosition:I

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2127
    const/16 v3, 0x4e22

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2128
    return-void
.end method

.method private registerPhoneStateReceiver(Z)V
    .locals 3
    .param p1, "doRegister"    # Z

    .prologue
    .line 1433
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mPhoneStateReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 1434
    new-instance v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$14;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$14;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mPhoneStateReceiver:Landroid/content/BroadcastReceiver;

    .line 1442
    :cond_0
    if-eqz p1, :cond_1

    .line 1443
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PHONE_STATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1444
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mPhoneStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1448
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    :goto_0
    return-void

    .line 1446
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mPhoneStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0
.end method

.method private registerScreenOnOffReceiver(Z)V
    .locals 5
    .param p1, "doRegister"    # Z

    .prologue
    .line 1451
    const-string v2, "MemoContentFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "registerScreenOnOffReceiver() doRegister:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1452
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

    if-nez v2, :cond_0

    .line 1453
    new-instance v2, Lcom/samsung/android/app/memo/voice/ScreenReceiver;

    invoke-direct {v2}, Lcom/samsung/android/app/memo/voice/ScreenReceiver;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

    .line 1456
    :cond_0
    if-eqz p1, :cond_2

    .line 1457
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1458
    .local v1, "screenFilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1459
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->isDestroyed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1460
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1470
    .end local v1    # "screenFilter":Landroid/content/IntentFilter;
    :cond_1
    :goto_0
    return-void

    .line 1462
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    .line 1464
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScreenOnOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1465
    :catch_0
    move-exception v0

    .line 1466
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "MemoContentFragment"

    const-string v3, "registerScreenOnOffReceiver():"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private restoreScrollViewPosition()V
    .locals 4

    .prologue
    .line 1489
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mOnGLobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1491
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getLayout()Landroid/text/Layout;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFirstVisableCharacterOffset:I

    invoke-virtual {v2, v3}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    .line 1492
    .local v1, "visableLineOffset":I
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getLayout()Landroid/text/Layout;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/text/Layout;->getLineTop(I)I

    move-result v0

    .line 1493
    .local v0, "pixelOffset":I
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScrollView:Landroid/widget/ScrollView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 1495
    return-void
.end method

.method private saveCurrentContent(Z)V
    .locals 2
    .param p1, "changeToView"    # Z

    .prologue
    .line 2145
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v1, "saveCurrentContent()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2147
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->cancelOrSave()V

    .line 2149
    if-nez p1, :cond_0

    .line 2152
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsDiscardBackPressed:Z

    .line 2153
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->onFragmentSuicide(Ljava/lang/String;)V

    .line 2156
    :cond_0
    return-void
.end method

.method private saveScrollViewPosition()V
    .locals 3

    .prologue
    .line 1480
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getLayout()Landroid/text/Layout;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1481
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 1482
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v2

    .line 1481
    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v0

    .line 1483
    .local v0, "currentVisableLineOffset":I
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 1484
    invoke-virtual {v1, v0}, Landroid/text/Layout;->getLineStart(I)I

    move-result v1

    .line 1483
    iput v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFirstVisableCharacterOffset:I

    .line 1486
    .end local v0    # "currentVisableLineOffset":I
    :cond_0
    return-void
.end method

.method private setRecordingPlay()V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1742
    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mStubInflate:Z

    if-nez v1, :cond_0

    .line 1743
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceMainLayoutStub:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    .line 1744
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mStubInflate:Z

    .line 1747
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 1748
    const-string v4, "notification"

    .line 1747
    invoke-virtual {v1, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mNotificationManager:Landroid/app/NotificationManager;

    .line 1749
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-nez v1, :cond_4

    .line 1750
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->checkVoiceRecord()V

    .line 1751
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsMemohasVoicedata:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v1, :cond_2

    .line 1752
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    invoke-direct {p0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->makeVoiceControlLayout(Lcom/samsung/android/app/memo/voice/VoiceInfo;)V

    .line 1756
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 1757
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1758
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z

    .line 1773
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceDeleteLayout:Landroid/widget/LinearLayout;

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v1, :cond_6

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1777
    :goto_3
    return-void

    .line 1754
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->makeNewVoiceControl()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1774
    :catch_0
    move-exception v0

    .line 1775
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setRecordingPlay"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1760
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1761
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z

    goto :goto_1

    .line 1763
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceRecorderProxy:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    if-nez v1, :cond_5

    .line 1764
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1765
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z

    goto :goto_1

    .line 1766
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceRecorderProxy:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    if-eqz v1, :cond_1

    .line 1767
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->checkVoiceRecord()V

    .line 1768
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    invoke-direct {p0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->makeVoiceControlLayout(Lcom/samsung/android/app/memo/voice/VoiceInfo;)V

    .line 1769
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1770
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_6
    move v1, v3

    .line 1773
    goto :goto_2
.end method

.method private setVoiceLayoutInvisible()V
    .locals 2

    .prologue
    .line 1780
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mStubInflate:Z

    if-nez v0, :cond_0

    .line 1781
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceMainLayoutStub:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    .line 1782
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mStubInflate:Z

    .line 1785
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1786
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1787
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z

    .line 1789
    :cond_1
    return-void
.end method

.method private showAddPictureDialog()V
    .locals 11

    .prologue
    const v8, 0x7f0b00ae

    const/4 v10, 0x0

    .line 1872
    iget-boolean v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v7, :cond_0

    iget-boolean v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsInsertOptionsShowing:Z

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->isFinishing()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1964
    :cond_0
    :goto_0
    return-void

    .line 1874
    :cond_1
    iget-boolean v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsDialogConfigChange:Z

    if-eqz v7, :cond_2

    .line 1875
    iput-boolean v10, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsDialogConfigChange:Z

    goto :goto_0

    .line 1879
    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-direct {v0, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1881
    .local v0, "build":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 1882
    .local v4, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/content/Intent;

    const-string v7, "android.intent.action.GET_CONTENT"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1883
    .local v2, "gallery":Landroid/content/Intent;
    const-string v7, "image/*"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1884
    new-instance v1, Landroid/content/Intent;

    const-string v7, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v1, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1886
    .local v1, "cameraIntent":Landroid/content/Intent;
    invoke-virtual {v4, v2, v10}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-gtz v7, :cond_3

    .line 1887
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7, v8}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1891
    :cond_3
    invoke-virtual {v4, v1, v10}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-gtz v7, :cond_4

    .line 1892
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7, v8}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1896
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    .line 1897
    const v8, 0x7f040017

    const/4 v9, 0x0

    .line 1896
    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 1899
    .local v3, "lv":Landroid/widget/LinearLayout;
    const v7, 0x7f0e0022

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    .line 1900
    .local v5, "rv_camera":Landroid/widget/RelativeLayout;
    const v7, 0x7f0e0024

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    .line 1902
    .local v6, "rv_gallery":Landroid/widget/RelativeLayout;
    new-instance v7, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$21;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$21;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1911
    new-instance v7, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$22;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$22;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1921
    const v7, 0x7f0b0046

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1922
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1924
    new-instance v7, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$23;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$23;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 1942
    new-instance v7, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$24;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$24;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 1960
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddPictureDialog:Landroid/app/AlertDialog;

    .line 1961
    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddPictureDialog:Landroid/app/AlertDialog;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1962
    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddPictureDialog:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    .line 1963
    iput-boolean v10, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsInsertOptionsShowing:Z

    goto/16 :goto_0
.end method

.method private showCategoryChooseDialog()V
    .locals 4

    .prologue
    .line 1731
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$20;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$20;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    .line 1737
    const-wide/16 v2, 0xfa

    .line 1731
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1738
    return-void
.end method

.method private startService()V
    .locals 4

    .prologue
    .line 1507
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$16;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$16;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mConnection:Landroid/content/ServiceConnection;

    .line 1520
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/samsung/android/app/memo/KillVoiceNotificationService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1521
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    .line 1520
    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 1522
    return-void
.end method

.method private updateMarginLayout()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 532
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0e002f

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 533
    .local v0, "left_margin":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0e0030

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 534
    .local v1, "right_margin":Landroid/view/View;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 535
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 536
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 537
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 543
    :cond_0
    :goto_0
    return-void

    .line 539
    :cond_1
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 540
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateVoiceButtonState()V
    .locals 4

    .prologue
    .line 1498
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$15;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$15;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    .line 1503
    const-wide/16 v2, 0xc8

    .line 1498
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1504
    return-void
.end method


# virtual methods
.method public UpdateEditor()V
    .locals 5

    .prologue
    .line 2476
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v0

    .line 2477
    .local v0, "ss":Lcom/samsung/android/app/memo/Session;
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iget-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setEditMode(Z)V

    .line 2478
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Session;->getContent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setHtml(JLjava/lang/CharSequence;)V

    .line 2479
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->tv:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-nez v1, :cond_0

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2480
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->tv:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Session;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2481
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Session;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2482
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->checkVoiceRecord()V

    .line 2483
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceButtonEnable:Z

    .line 2484
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsMemohasVoicedata:Z

    if-eqz v1, :cond_1

    .line 2485
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->setRecordingPlay()V

    .line 2486
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceButtonEnable:Z

    .line 2488
    :cond_1
    return-void
.end method

.method public getEditor()Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    .locals 1

    .prologue
    .line 1575
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    return-object v0
.end method

.method getSpanPosition()V
    .locals 2

    .prologue
    .line 1844
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    if-eqz v0, :cond_0

    .line 1845
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v0

    sput v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mLastSpanClickPosition:I

    .line 1850
    sget v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mLastSpanClickPosition:I

    if-lez v0, :cond_0

    .line 1851
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v0

    sget v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mLastSpanClickPosition:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    const v1, 0xfffc

    if-ne v0, v1, :cond_0

    .line 1852
    sget v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mLastSpanClickPosition:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mLastSpanClickPosition:I

    .line 1856
    :cond_0
    return-void
.end method

.method public isEditorFocussed()Z
    .locals 1

    .prologue
    .line 2632
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    if-eqz v0, :cond_0

    .line 2633
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isFocused()Z

    move-result v0

    .line 2635
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMemoContentEmpty(Z)Z
    .locals 4
    .param p1, "isPossibleTitle"    # Z

    .prologue
    const/4 v2, 0x1

    .line 2300
    const/4 v0, 0x0

    .line 2301
    .local v0, "hasVoiceData":Z
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v3}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/Session;->getVrFileUUID()Ljava/lang/String;

    move-result-object v1

    .line 2302
    .local v1, "mVoiceId":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2303
    :cond_0
    const/4 v0, 0x0

    .line 2307
    :goto_0
    if-eqz p1, :cond_3

    .line 2308
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2309
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getAttachmentList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2310
    if-nez v0, :cond_4

    .line 2320
    :cond_1
    :goto_1
    return v2

    .line 2305
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 2315
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getAttachmentList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2316
    if-eqz v0, :cond_1

    .line 2320
    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v8, 0x1e

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 653
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 654
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v7, "onActivityCreated"

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    invoke-virtual {p0, v11}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->setHasOptionsMenu(Z)V

    .line 656
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->resetFragment(Landroid/os/Bundle;)V

    .line 659
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 661
    .local v1, "intent":Landroid/content/Intent;
    if-nez v1, :cond_1

    .line 752
    :cond_0
    :goto_0
    return-void

    .line 664
    :cond_1
    const-string v6, "isFromShareVia"

    invoke-virtual {v1, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->isFromShareVia:Z

    .line 666
    iget-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsRequiredToexecuteOneTime:Z

    if-nez v6, :cond_4

    .line 667
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_ACTION_SEND_IMAGE_URI:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 668
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TITLESTRINGEXTRA:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 669
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    sget-object v7, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TITLESTRINGEXTRA:Ljava/lang/String;

    invoke-virtual {v1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 671
    :cond_2
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_BODYSTRINGEXTRA:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 672
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_BODYSTRINGEXTRA:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 673
    .local v2, "mBodyText":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v6}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v4

    .line 674
    .local v4, "ss":Lcom/samsung/android/app/memo/Session;
    if-eqz v2, :cond_3

    .line 675
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9, v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setHtml(JLjava/lang/CharSequence;)V

    .line 676
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSelection(I)V

    .line 679
    .end local v2    # "mBodyText":Ljava/lang/String;
    .end local v4    # "ss":Lcom/samsung/android/app/memo/Session;
    :cond_3
    new-instance v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-direct {v6, p0, v7, v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/content/Context;Landroid/content/Intent;)V

    iput-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddImagesTask:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;

    .line 680
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddImagesTask:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;

    new-array v7, v10, [Ljava/lang/Void;

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 682
    iput-boolean v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsRequiredToexecuteOneTime:Z

    .line 743
    :cond_4
    :goto_1
    const-string v6, "from_widget"

    invoke-virtual {v1, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFromWidget:Z

    .line 744
    iget-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFromWidget:Z

    if-eqz v6, :cond_5

    .line 745
    const-string v6, "appWidgetId"

    const-wide/16 v8, -0x1

    invoke-virtual {v1, v6, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mWidgetID:J

    .line 748
    :cond_5
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isNoOutgoingCall()Z

    move-result v6

    if-nez v6, :cond_0

    .line 749
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/app/memo/util/Utils;->findCategoryUUIDofDuringCall(Landroid/content/Context;)Ljava/lang/String;

    .line 750
    const/4 v6, -0x3

    sput v6, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedPosition:I

    goto/16 :goto_0

    .line 683
    :cond_6
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_BODYSTRINGEXTRA:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 684
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_BODYSTRINGEXTRA:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 686
    .restart local v2    # "mBodyText":Ljava/lang/String;
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->IS_FROM_DRAG_LISTENER:Ljava/lang/String;

    invoke-virtual {v1, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 687
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6, v2, v10, v10}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->insertHtmlText(Ljava/lang/CharSequence;II)V

    .line 706
    :cond_7
    :goto_2
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSelection(I)V

    .line 707
    iput-boolean v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsRequiredToexecuteOneTime:Z

    goto :goto_1

    .line 689
    :cond_8
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TITLESTRINGEXTRA:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 690
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TITLESTRINGEXTRA:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v8, :cond_a

    .line 691
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    sget-object v7, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TITLESTRINGEXTRA:Ljava/lang/String;

    invoke-virtual {v1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v10, v8}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 697
    :cond_9
    :goto_3
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v6}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v4

    .line 698
    .restart local v4    # "ss":Lcom/samsung/android/app/memo/Session;
    if-eqz v2, :cond_7

    .line 699
    invoke-static {v2}, Lcom/samsung/android/app/memo/util/HtmlUtil;->isHtml(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_b

    .line 700
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v8

    .line 701
    new-instance v7, Landroid/text/SpannedString;

    invoke-direct {v7, v2}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v7}, Lcom/samsung/android/app/memo/util/HtmlUtil;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v7

    .line 700
    invoke-virtual {v6, v8, v9, v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setHtml(JLjava/lang/CharSequence;)V

    goto :goto_2

    .line 694
    .end local v4    # "ss":Lcom/samsung/android/app/memo/Session;
    :cond_a
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    sget-object v7, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TITLESTRINGEXTRA:Ljava/lang/String;

    invoke-virtual {v1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 703
    .restart local v4    # "ss":Lcom/samsung/android/app/memo/Session;
    :cond_b
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9, v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setHtml(JLjava/lang/CharSequence;)V

    goto :goto_2

    .line 708
    .end local v2    # "mBodyText":Ljava/lang/String;
    .end local v4    # "ss":Lcom/samsung/android/app/memo/Session;
    :cond_c
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VNTFILE:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 709
    iget-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-nez v6, :cond_d

    .line 710
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const-string v7, ""

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setHint(Ljava/lang/CharSequence;)V

    .line 711
    :cond_d
    const/4 v5, 0x0

    .line 712
    .local v5, "vntContent":Ljava/lang/String;
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VNTFILE:Ljava/lang/String;

    invoke-virtual {v1, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    iput-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFromVnt:Z

    .line 713
    iget-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFromVnt:Z

    if-eqz v6, :cond_e

    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VNTTEXT:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 714
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VNTTEXT:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 716
    :cond_e
    if-eqz v5, :cond_f

    .line 717
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6, v5}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setText(Ljava/lang/CharSequence;)V

    .line 720
    :goto_4
    iput-boolean v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsRequiredToexecuteOneTime:Z

    goto/16 :goto_1

    .line 719
    :cond_f
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v7, "VNT content null !!"

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 721
    .end local v5    # "vntContent":Ljava/lang/String;
    :cond_10
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_ACTION_SEND_AUDIO_URI:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 723
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_ACTION_SEND_AUDIO_URI:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 724
    .local v3, "mReceiveMultiUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v6}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v4

    .line 725
    .restart local v4    # "ss":Lcom/samsung/android/app/memo/Session;
    if-eqz v3, :cond_12

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ne v6, v11, :cond_12

    .line 727
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    invoke-virtual {v4, v6}, Lcom/samsung/android/app/memo/Session;->setVoiceRecordedFile(Landroid/net/Uri;)Landroid/net/Uri;

    .line 728
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->checkVoiceRecord()V

    .line 729
    iget-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsMemohasVoicedata:Z

    if-eqz v6, :cond_11

    .line 730
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFirstTime:Z

    .line 731
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->setRecordingPlay()V

    .line 732
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceButtonEnable:Z

    .line 734
    :cond_11
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceButtonEnable:Z
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 739
    :cond_12
    :goto_5
    iput-boolean v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsRequiredToexecuteOneTime:Z

    goto/16 :goto_1

    .line 735
    :catch_0
    move-exception v0

    .line 736
    .local v0, "e":Lcom/samsung/android/app/memo/Session$SessionException;
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "this fragment is net yet attached on"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 12
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1970
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFromOnActivityResult:Z

    .line 1971
    packed-switch p1, :pswitch_data_0

    .line 2036
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2037
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2036
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1974
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_4

    .line 1976
    if-eqz p3, :cond_2

    .line 1977
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 1978
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_1

    invoke-static {v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->isVideoUri(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1979
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b00af

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2039
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 1981
    .restart local v3    # "uri":Landroid/net/Uri;
    :cond_1
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {p3}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    .line 1982
    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 1981
    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddImagesTask:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;

    .line 1983
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddImagesTask:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1989
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTempImageUri:Landroid/net/Uri;

    if-eqz v0, :cond_3

    .line 1990
    new-instance v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTempImageUri:Landroid/net/Uri;

    .line 1991
    const-string v8, "image/jpeg"

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTempImageUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v9

    move-object v5, p0

    invoke-direct/range {v4 .. v9}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 1990
    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddImagesTask:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;

    .line 1992
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddImagesTask:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1996
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTempImageUri:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->galleryAddPic(Landroid/net/Uri;)V

    .line 1997
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTempImageUri:Landroid/net/Uri;

    goto :goto_0

    .line 1999
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0041

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 2003
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTempImageUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 2004
    new-instance v11, Ljava/io/File;

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTempImageUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2005
    .local v11, "tempFile":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2006
    invoke-virtual {v11}, Ljava/io/File;->delete()Z

    .line 2007
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/Utils;->sendMediaScanEvent(Landroid/content/Context;)V

    .line 2009
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTempImageUri:Landroid/net/Uri;

    goto :goto_0

    .line 2015
    .end local v11    # "tempFile":Ljava/io/File;
    :pswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2016
    if-eqz p3, :cond_0

    .line 2017
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->deleteImage(Landroid/net/Uri;)V

    .line 2025
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->KEY_CLICK_POS:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2026
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->KEY_CLICK_POS:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 2027
    .local v10, "pos":I
    const/4 v0, -0x1

    if-le v10, v0, :cond_0

    .line 2028
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-static {v10, v0}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 2029
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0, v10}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSelection(I)V

    goto/16 :goto_0

    .line 1971
    :pswitch_data_0
    .packed-switch 0x4e21
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 336
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onAttach"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 338
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/samsung/android/app/memo/MemoApp;->setIsSaveOnDetach(Z)V

    .line 342
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-object v2, v0

    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 348
    return-void

    .line 343
    :catch_0
    move-exception v1

    .line 344
    .local v1, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v3, "onAttach"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 345
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 346
    const-string v4, " must implement MemoListActionListener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 345
    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2324
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v1, "onBackPressed()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2326
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsInsertOptionsShowing:Z

    .line 2328
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v0, :cond_0

    .line 2329
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMessageHandler:Landroid/os/Handler;

    const/16 v1, 0xfc7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2331
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsDiscardBackPressed:Z

    if-eqz v0, :cond_1

    .line 2343
    :goto_0
    return-void

    .line 2334
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v0, :cond_3

    .line 2335
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 2336
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->save()V

    .line 2338
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2339
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->unregisterClipboard()V

    .line 2340
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->cancelOrSave()V

    .line 2342
    :cond_3
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->setOverFlowMenu(Z)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const-wide/16 v8, 0x1f4

    const/4 v7, 0x0

    .line 1687
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 1725
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onClick() unknown: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1726
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "not implemeted yet"

    invoke-static {v4, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1728
    :cond_0
    :goto_0
    return-void

    .line 1689
    :pswitch_0
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isImageMaximum()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1695
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1696
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1697
    .local v2, "currentClickTime1":J
    sget-wide v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->lastInsertClick:J

    sub-long v4, v2, v4

    cmp-long v4, v4, v8

    if-lez v4, :cond_2

    .line 1698
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsInsertOptionsShowing:Z

    .line 1699
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->showAddPictureDialog()V

    .line 1701
    :cond_2
    sput-wide v2, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->lastInsertClick:J

    goto :goto_0

    .line 1704
    .end local v2    # "currentClickTime1":J
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1705
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->unregisterClipboard()V

    .line 1706
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1707
    .local v0, "currentClickTime":J
    sget-wide v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->lastCategoryClick:J

    sub-long v4, v0, v4

    cmp-long v4, v4, v8

    if-lez v4, :cond_3

    .line 1708
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->showCategoryChooseDialog()V

    .line 1709
    :cond_3
    sput-wide v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->lastCategoryClick:J

    goto :goto_0

    .line 1712
    .end local v0    # "currentClickTime":J
    :pswitch_2
    iget-boolean v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z

    if-eqz v4, :cond_4

    .line 1713
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->setVoiceLayoutInvisible()V

    goto :goto_0

    .line 1715
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1716
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$19;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$19;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    .line 1721
    const-wide/16 v6, 0x0

    .line 1716
    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1687
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e0031
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;)V
    .locals 3
    .param p1, "span"    # Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .prologue
    .line 1835
    instance-of v1, p1, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 1836
    check-cast v0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    .line 1837
    .local v0, "imgSpan":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getSpanPosition()V

    .line 1838
    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->getOrientation()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->openImageView(Landroid/net/Uri;I)V

    .line 1840
    .end local v0    # "imgSpan":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 7
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v6, 0x0

    .line 1216
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mOnGLobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1217
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->saveScrollViewPosition()V

    .line 1219
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1221
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->getDialog()Landroid/widget/PopupWindow;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->getDialog()Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1222
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1223
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1236
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    if-eqz v1, :cond_1

    .line 1237
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090086

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v6, v2}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 1239
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->initToolbar()V

    .line 1240
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->updateMarginLayout()V

    .line 1241
    return-void

    .line 1227
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->dismiss()V

    .line 1228
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->showCategoryChooseDialog()V

    .line 1229
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1233
    :catch_0
    move-exception v0

    .line 1234
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v2, "onConfigurationChanged"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1865
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    .line 396
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->isRecreated:Z

    .line 397
    if-eqz p1, :cond_0

    .line 398
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_HTML_STRING:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mStrHtml:Ljava/lang/String;

    .line 399
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TEMPURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TEMPURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTempImageUri:Landroid/net/Uri;

    .line 403
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 404
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->setRetainInstance(Z)V

    .line 405
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->setOverFlowMenu(Z)V

    .line 406
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 1861
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    const v6, 0x7f0e0092

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1534
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 1535
    const v0, 0x7f0d0002

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1536
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMenu:Landroid/view/Menu;

    .line 1538
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->initToolbar()V

    .line 1540
    const v0, 0x7f0e0093

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1541
    const v0, 0x7f0e0094

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1542
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_2
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1543
    const v0, 0x7f0e0095

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_3
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1544
    const v0, 0x7f0e0096

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v3, :cond_0

    move v1, v2

    :cond_0
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1545
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v0

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 1546
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1547
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 1540
    goto :goto_0

    :cond_3
    move v0, v2

    .line 1541
    goto :goto_1

    :cond_4
    move v0, v1

    .line 1542
    goto :goto_2

    :cond_5
    move v0, v1

    .line 1543
    goto :goto_3
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 445
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v2, "onCreateView"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    if-eqz p3, :cond_0

    .line 448
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_EDITMODE:Ljava/lang/String;

    invoke-virtual {p3, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    .line 449
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VOICEMODE:Ljava/lang/String;

    invoke-virtual {p3, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z

    .line 450
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_INSERT_SHOWING:Ljava/lang/String;

    invoke-virtual {p3, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsInsertOptionsShowing:Z

    .line 451
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_REQUIRED_ONE_TIME_EXEC:Ljava/lang/String;

    invoke-virtual {p3, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsRequiredToexecuteOneTime:Z

    .line 453
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mIsEditMode is restored from savedInstanceState. value is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 454
    iget-boolean v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 453
    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    :cond_0
    const v1, 0x7f040019

    invoke-virtual {p1, v1, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 467
    .local v0, "v":Landroid/view/View;
    const v1, 0x7f0e002a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScrollView:Landroid/widget/ScrollView;

    .line 468
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mScrollView:Landroid/widget/ScrollView;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->onTouchViewListnerForScrollView:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 469
    const v1, 0x7f0e002d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceMainLayoutStub:Landroid/view/ViewStub;

    .line 470
    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->initView(Landroid/view/View;)V

    .line 471
    return-object v0
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1365
    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mStrHtml:Ljava/lang/String;

    .line 1366
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mConnection:Landroid/content/ServiceConnection;

    if-eqz v1, :cond_0

    .line 1368
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 1369
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mConnection:Landroid/content/ServiceConnection;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1374
    :cond_0
    :goto_0
    sget-boolean v1, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SLOOKBEZELINTERACTION_API:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v1, :cond_1

    .line 1376
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 1378
    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 1379
    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 1382
    :cond_1
    sput-boolean v4, Lcom/samsung/android/app/memo/voice/VRUtils;->isRecording:Z

    .line 1384
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceRecorderProxy:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    if-eqz v1, :cond_2

    .line 1385
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceRecorderProxy:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->freeResources()V

    .line 1386
    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceRecorderProxy:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    .line 1388
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v1, :cond_3

    .line 1389
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->cleanUp()V

    .line 1390
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    if-eqz v1, :cond_4

    sget-boolean v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsClipboardRegistered:Z

    if-eqz v1, :cond_4

    .line 1391
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->unregisterClipboard()V

    .line 1392
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/samsung/android/app/memo/MemoApp;->isCreateMemo()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1393
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsSavingThenClose:Z

    if-eqz v1, :cond_5

    .line 1394
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->cancelOrSave()V

    .line 1396
    :cond_5
    invoke-static {v4}, Lcom/samsung/android/app/memo/MemoApp;->setIsCreateMemo(Z)V

    .line 1399
    :cond_6
    invoke-direct {p0, v4}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->registerScreenOnOffReceiver(Z)V

    .line 1401
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 1403
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->clearObjects()V

    .line 1404
    return-void

    .line 1370
    :catch_0
    move-exception v0

    .line 1371
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v2, "onDestroy"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 352
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v1, "onDetach"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsSavingThenClose:Z

    if-eqz v0, :cond_2

    .line 354
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v0, v0, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->save()V

    .line 357
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->hideNotification()V

    .line 359
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/memo/MemoApp;->isSaveOnDetach()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 360
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->cancelOrSave()V

    .line 361
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/android/app/memo/MemoApp;->setIsSaveOnDetach(Z)V

    .line 368
    :cond_1
    :goto_0
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 369
    return-void

    .line 363
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v0, :cond_1

    .line 364
    sget-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->getState()Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 365
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleStopPlaying()V

    .line 366
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->hideNotification()V

    goto :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2492
    sparse-switch p2, :sswitch_data_0

    .line 2502
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 2495
    :sswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->removeLinks()V

    .line 2496
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_0

    .line 2492
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2576
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2628
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2581
    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    move v0, v1

    .line 2622
    :cond_1
    :goto_1
    return v0

    .line 2583
    :sswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    goto :goto_0

    .line 2588
    :sswitch_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    goto :goto_0

    .line 2592
    :sswitch_2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    goto :goto_0

    .line 2597
    :sswitch_3
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2598
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v2, :cond_2

    .line 2599
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v2, v2, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_2

    .line 2600
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->save()V

    .line 2602
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2603
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->unregisterClipboard()V

    .line 2604
    invoke-direct {p0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->saveCurrentContent(Z)V

    goto :goto_1

    .line 2610
    :sswitch_4
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2611
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->redo()V

    goto :goto_1

    .line 2615
    :sswitch_5
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2616
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undo()V

    goto :goto_1

    .line 2581
    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_0
        0x2f -> :sswitch_3
        0x35 -> :sswitch_4
        0x36 -> :sswitch_5
    .end sparse-switch
.end method

.method public onModeChanged(Z)V
    .locals 4
    .param p1, "isEditMode"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2347
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    .line 2348
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2349
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0e0029

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;

    .line 2350
    :cond_0
    if-eqz p1, :cond_2

    .line 2351
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 2352
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mShadowImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2353
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSelection(I)V

    .line 2354
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 2355
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 2356
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 2357
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2358
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b003f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2359
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0040

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setHint(Ljava/lang/CharSequence;)V

    .line 2360
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceDeleteLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 2361
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceDeleteLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2363
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 2364
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 2365
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 9
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 1580
    iput-boolean v8, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsInsertOptionsShowing:Z

    .line 1582
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 1633
    :cond_0
    :goto_0
    return v8

    .line 1585
    :sswitch_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->onBackPressed()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1586
    :catch_0
    move-exception v0

    .line 1587
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "IllegalStateException :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1591
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :sswitch_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->showDiscardDialog()V

    goto :goto_0

    .line 1594
    :sswitch_2
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v4, :cond_1

    .line 1595
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v4, v4, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    if-eqz v4, :cond_1

    .line 1596
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->save()V

    .line 1598
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v4, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1599
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->unregisterClipboard()V

    .line 1600
    invoke-direct {p0, v8}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->saveCurrentContent(Z)V

    .line 1601
    iput-boolean v8, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsSavingThenClose:Z

    goto :goto_0

    .line 1604
    :sswitch_3
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v4, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1605
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->unregisterClipboard()V

    .line 1606
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->deletingCurrentContent()V

    .line 1607
    iget-boolean v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFromWidget:Z

    if-eqz v4, :cond_0

    .line 1608
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->sendUpdateAllWidgetBroadcastEvent()V

    goto :goto_0

    .line 1611
    :sswitch_4
    new-instance v2, Lcom/samsung/android/app/memo/print/AsyncPrintTask;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    .line 1612
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v5}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v5

    .line 1611
    invoke-direct {v2, v4, v6, v6, v5}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;-><init>(Landroid/content/Context;ZILcom/samsung/android/app/memo/Session;)V

    .line 1613
    .local v2, "printTask":Lcom/samsung/android/app/memo/print/AsyncPrintTask;
    new-array v4, v6, [Ljava/lang/Integer;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v5}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v6

    long-to-int v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v2, v4}, Lcom/samsung/android/app/memo/print/AsyncPrintTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1616
    .end local v2    # "printTask":Lcom/samsung/android/app/memo/print/AsyncPrintTask;
    :sswitch_5
    iget-boolean v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v4, :cond_2

    .line 1617
    sget-object v4, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->getState()Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    move-result-object v5

    if-ne v4, v5, :cond_2

    .line 1618
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleStopPlaying()V

    .line 1620
    :cond_2
    new-instance v1, Lcom/samsung/android/app/memo/share/MemoShare;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/samsung/android/app/memo/share/MemoShare;-><init>(Landroid/content/Context;)V

    .line 1621
    .local v1, "m":Lcom/samsung/android/app/memo/share/MemoShare;
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getPlainText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/memo/share/MemoShare;->setText(Ljava/lang/String;)V

    .line 1622
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/app/memo/util/HtmlUtil;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/memo/share/MemoShare;->setHtmlText(Ljava/lang/String;)V

    .line 1623
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getAttachmentList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/memo/share/MemoShare;->setImageAttachmentList(Ljava/util/ArrayList;)V

    .line 1624
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v4, :cond_3

    .line 1625
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v4, v4, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/memo/share/MemoShare;->setVoiceAttachment(Landroid/net/Uri;)V

    .line 1627
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f0b0009

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/memo/share/MemoShare;->setTitle(Ljava/lang/String;)V

    .line 1628
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/memo/share/MemoShare;->setSubject(Ljava/lang/String;)V

    .line 1629
    new-instance v3, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-wide/16 v6, -0x1

    invoke-direct {v3, v1, v4, v6, v7}, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;-><init>(Lcom/samsung/android/app/memo/share/MemoShare;Landroid/app/Activity;J)V

    .line 1630
    .local v3, "shareTask":Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;
    sget-object v4, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v5, v8, [Ljava/lang/Void;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/app/memo/share/MemoShare$MemoShareTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 1582
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0e0092 -> :sswitch_3
        0x7f0e0093 -> :sswitch_1
        0x7f0e0094 -> :sswitch_2
        0x7f0e0095 -> :sswitch_5
        0x7f0e0096 -> :sswitch_4
    .end sparse-switch
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1325
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->isRecreated:Z

    .line 1327
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1329
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v0, :cond_0

    .line 1330
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMainLayout:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1331
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->unregisterClipboard()V

    .line 1332
    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->registerPhoneStateReceiver(Z)V

    .line 1333
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getHtml()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mStrHtml:Ljava/lang/String;

    .line 1334
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v0, :cond_3

    .line 1335
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1336
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->getState()Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 1337
    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->getState()Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    if-ne v0, v1, :cond_7

    .line 1338
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getUiPickerMode()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->isFromShareVia:Z

    if-eqz v0, :cond_6

    .line 1339
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->getState()Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    if-ne v0, v1, :cond_5

    .line 1340
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const v1, 0x7f0b00ac

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    .line 1341
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleRecordingStop()V

    .line 1345
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->removeDialogIfshowing()V

    .line 1356
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->getDialog()Landroid/widget/PopupWindow;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->getDialog()Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1357
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->dismiss()V

    .line 1358
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->isDialogShowing:Z

    .line 1360
    :cond_4
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 1362
    return-void

    .line 1343
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleStopPlaying()V

    goto :goto_0

    .line 1347
    :cond_6
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->startService()V

    .line 1348
    invoke-static {v3}, Lcom/samsung/android/app/memo/MemoApp;->setShowNotificationEnabled(Z)V

    goto :goto_1

    .line 1351
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isNotificationStart()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1352
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->hideNotification()V

    goto :goto_1
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v2, 0x7f0e0096

    const/4 v1, 0x1

    .line 1551
    invoke-super {p0, p1}, Landroid/app/Fragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1565
    const v0, 0x7f0e0095

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1566
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1569
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isKnoxMode()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/samsung/android/app/memo/util/Utils;->IS_RUNNING_UNDER_KNOX:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isUPSM()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1570
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1572
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1266
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->isRecreated:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mStrHtml:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1267
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iget-wide v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->memoId:J

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mStrHtml:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setHtml(JLjava/lang/CharSequence;)V

    .line 1268
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSelection(I)V

    .line 1271
    :cond_0
    iput-boolean v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFromOnActivityResult:Z

    .line 1272
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFirstTime:Z

    if-nez v1, :cond_1

    .line 1273
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$13;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$13;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    invoke-virtual {v1, v2, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1280
    :cond_1
    iput-boolean v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFirstTime:Z

    .line 1282
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 1285
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 1286
    const-string v2, "keyguard"

    .line 1285
    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 1287
    .local v0, "myKeyManager":Landroid/app/KeyguardManager;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1289
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isNotificationStart()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1290
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->hideNotification()V

    .line 1291
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->hideNotification()V

    .line 1296
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->updateMarginLayout()V

    .line 1297
    invoke-direct {p0, v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->registerPhoneStateReceiver(Z)V

    .line 1298
    invoke-direct {p0, v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->registerScreenOnOffReceiver(Z)V

    .line 1299
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->updateVoiceButtonState()V

    .line 1300
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/app/memo/util/Utils;->refreshToast(Landroid/content/Context;)V

    .line 1302
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->getDialog()Landroid/widget/PopupWindow;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->getDialog()Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1303
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->getIsDeleteDialogShowing()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1304
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x32

    invoke-virtual {v1, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1305
    :cond_5
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v1, :cond_a

    .line 1306
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFromOnActivityResult:Z

    if-nez v1, :cond_a

    .line 1307
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddPictureDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddPictureDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_a

    .line 1308
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->getDialog()Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-nez v1, :cond_a

    .line 1309
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCancelDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCancelDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_a

    .line 1310
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->getIsDeleteDialogShowing()Z

    move-result v1

    if-nez v1, :cond_a

    .line 1311
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1313
    :cond_a
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddPictureDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddPictureDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isUPSM()I

    move-result v1

    if-ne v1, v6, :cond_b

    .line 1314
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mAddPictureDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 1316
    :cond_b
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->isDialogShowing:Z

    if-eqz v1, :cond_c

    .line 1317
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->showCategoryChooseDialog()V

    .line 1318
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1319
    iput-boolean v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->isDialogShowing:Z

    .line 1321
    :cond_c
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 373
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v1, "onSaveInstanceState"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->getDialog()Landroid/widget/PopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->getDialog()Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->dialog:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->dismiss()V

    .line 378
    :cond_0
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_EDITMODE:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 379
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v0, :cond_1

    .line 380
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isVoiceViewLayoutVisible()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z

    .line 382
    :cond_1
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VOICEMODE:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 383
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_INSERT_SHOWING:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsInsertOptionsShowing:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 384
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_REQUIRED_ONE_TIME_EXEC:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsRequiredToexecuteOneTime:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 385
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_HTML_STRING:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mStrHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTempImageUri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 387
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TEMPURI:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTempImageUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 389
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->requestUpdateSession()V

    .line 390
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 391
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 1244
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 1245
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->initSConnect(Landroid/app/Activity;)V

    .line 1246
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_0

    .line 1247
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->registerListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 1249
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 1252
    sget-boolean v0, Lcom/samsung/android/app/memo/util/Utils;->HAVE_SLOOKBEZELINTERACTION_API:Z

    if-eqz v0, :cond_1

    .line 1254
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_0

    .line 1255
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 1258
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 1260
    :cond_1
    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    .line 1261
    return-void
.end method

.method public requestUpdateSession()V
    .locals 2

    .prologue
    .line 2470
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v0

    .line 2471
    .local v0, "ss":Lcom/samsung/android/app/memo/Session;
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getHtml()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/Session;->setContent(Ljava/lang/String;)V

    .line 2472
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/Session;->setTitle(Ljava/lang/String;)V

    .line 2473
    return-void
.end method

.method public resetFragment(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "args"    # Landroid/os/Bundle;

    .prologue
    .line 1079
    sget-object v9, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "resetFragment()"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1080
    if-nez p1, :cond_1

    .line 1081
    sget-object v9, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v10, "args bundle is null"

    invoke-static {v9, v10}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1156
    :cond_0
    :goto_0
    return-void

    .line 1085
    :cond_1
    sget-object v9, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_DURINGCALL:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1086
    .local v1, "duringCall":Z
    sget-object v9, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_TITLEPREFIX:Ljava/lang/String;

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1087
    .local v8, "titlePrefix":Ljava/lang/String;
    sget-object v9, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_PHONENUMBER:Ljava/lang/String;

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1088
    .local v5, "phoneNumber":Ljava/lang/String;
    sget-object v9, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1090
    .local v0, "categoryUUID":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 1091
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v9}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v9

    invoke-virtual {v9, v0}, Lcom/samsung/android/app/memo/Session;->setCategoryUUID(Ljava/lang/String;)V

    .line 1094
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getView()Landroid/view/View;

    move-result-object v9

    if-nez v9, :cond_3

    .line 1095
    sget-object v9, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v10, "this fragment is net yet attached on"

    invoke-static {v9, v10}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1099
    :cond_3
    sget-object v9, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "resetFragmentUI()"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v9}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v6

    .line 1102
    .local v6, "ss":Lcom/samsung/android/app/memo/Session;
    iget-boolean v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-nez v9, :cond_4

    .line 1103
    sget-object v9, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_EDITMODE:Ljava/lang/String;

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    .line 1106
    :cond_4
    invoke-virtual {v6}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v10

    iput-wide v10, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMemoId:J

    .line 1107
    iget-wide v10, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMemoId:J

    const-wide/16 v12, -0x1

    cmp-long v9, v10, v12

    if-eqz v9, :cond_5

    .line 1108
    new-instance v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;

    iget-wide v10, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mMemoId:J

    long-to-int v9, v10

    .line 1109
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    .line 1108
    invoke-direct {v4, p0, v9, v10}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;ILandroid/app/Activity;)V

    .line 1110
    .local v4, "imageCacheTask":Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;
    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Void;

    invoke-virtual {v4, v9}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1114
    .end local v4    # "imageCacheTask":Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;
    :cond_5
    iget-boolean v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-eqz v9, :cond_6

    if-eqz v1, :cond_6

    .line 1115
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1116
    .local v2, "currentTimeMillis":J
    if-nez v8, :cond_9

    const-string v7, ""

    .line 1118
    .local v7, "title":Ljava/lang/String;
    :goto_1
    invoke-virtual {v6, v7}, Lcom/samsung/android/app/memo/Session;->setTitle(Ljava/lang/String;)V

    .line 1119
    invoke-virtual {v6, v5}, Lcom/samsung/android/app/memo/Session;->setPhoneNumer(Ljava/lang/String;)V

    .line 1120
    new-instance v9, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$12;

    invoke-direct {v9, p0, v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$12;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Lcom/samsung/android/app/memo/Session;)V

    .line 1128
    invoke-virtual {v9}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$12;->run()V

    .line 1132
    .end local v2    # "currentTimeMillis":J
    .end local v7    # "title":Ljava/lang/String;
    :cond_6
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/Session;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1134
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v10

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/Session;->getContent()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v10, v11, v12}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setHtml(JLjava/lang/CharSequence;)V

    .line 1136
    iget-boolean v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsEditMode:Z

    if-nez v9, :cond_a

    .line 1137
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v9

    if-nez v9, :cond_7

    .line 1138
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1140
    :cond_7
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v9}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setLinks()V

    .line 1141
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->checkVoiceRecord()V

    .line 1142
    iget-boolean v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsMemohasVoicedata:Z

    if-eqz v9, :cond_8

    .line 1143
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->setRecordingPlay()V

    .line 1144
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceButtonEnable:Z

    .line 1152
    :cond_8
    :goto_2
    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v9, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setCurrentMemoId(Lcom/samsung/android/app/memo/Session;)V

    .line 1153
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    if-eqz v9, :cond_0

    .line 1154
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->setRecordingPlay()V

    goto/16 :goto_0

    .line 1116
    .restart local v2    # "currentTimeMillis":J
    :cond_9
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 1117
    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Utils;->getDateFormat(J)Ljava/lang/String;

    move-result-object v10

    const-string v11, "/"

    const-string v12, "_"

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 1147
    .end local v2    # "currentTimeMillis":J
    :cond_a
    iget-boolean v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceLayoutVisible:Z

    if-eqz v9, :cond_8

    .line 1148
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mStubInflate:Z

    .line 1149
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->setRecordingPlay()V

    goto :goto_2
.end method

.method public sendUpdateAllWidgetBroadcastEvent()V
    .locals 6

    .prologue
    .line 2536
    iget-wide v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->memoId:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 2537
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.samsung.android.widget.memo.UPDATE_ALL_WIDGET"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2542
    .local v0, "intent":Landroid/content/Intent;
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2543
    iget-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsFromWidget:Z

    if-eqz v2, :cond_0

    .line 2544
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.widgetapp.MEMO_SAVE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2545
    .local v1, "widgetIntent":Landroid/content/Intent;
    const-string v2, "memo_id"

    iget-wide v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->memoId:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2546
    const-string v2, "appWidgetId"

    iget-wide v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mWidgetID:J

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2547
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2548
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 2550
    .end local v1    # "widgetIntent":Landroid/content/Intent;
    :cond_0
    return-void

    .line 2539
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.samsung.android.widget.memo.UPDATE_SELECTED_WIDGET"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2540
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v2, "memo_id"

    iget-wide v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->memoId:J

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_0
.end method

.method public setOverFlowMenu(Z)V
    .locals 6
    .param p1, "flag"    # Z

    .prologue
    const/4 v3, 0x1

    .line 2562
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 2563
    .local v0, "config":Landroid/view/ViewConfiguration;
    const-class v4, Landroid/view/ViewConfiguration;

    const-string v5, "sHasPermanentMenuKey"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 2564
    .local v2, "menuKeyField":Ljava/lang/reflect/Field;
    if-eqz v2, :cond_1

    .line 2565
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 2566
    if-eqz p1, :cond_0

    const/4 v3, 0x0

    :cond_0
    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2571
    .end local v0    # "config":Landroid/view/ViewConfiguration;
    .end local v2    # "menuKeyField":Ljava/lang/reflect/Field;
    :cond_1
    :goto_0
    return-void

    .line 2568
    :catch_0
    move-exception v1

    .line 2569
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;

    const-string v4, "setOverFlowMenu exception"

    invoke-static {v3, v4, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public showDiscardDialog()V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 1637
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1638
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getHtml()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1640
    .local v4, "strContent":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v6}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v3

    .line 1641
    .local v3, "ss":Lcom/samsung/android/app/memo/Session;
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-interface {v6}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/samsung/android/app/memo/Session;->setTitle(Ljava/lang/String;)V

    .line 1642
    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/Session;->setContent(Ljava/lang/String;)V

    .line 1644
    invoke-virtual {v3}, Lcom/samsung/android/app/memo/Session;->isDirty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1645
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1646
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->unregisterClipboard()V

    .line 1647
    invoke-direct {p0, v7}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->cancelCurrentContent(Z)V

    .line 1648
    iput-boolean v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsSavingThenClose:Z

    .line 1673
    :goto_0
    return-void

    .line 1652
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 1653
    .local v2, "mContext":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0004

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1654
    .local v5, "title":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0092

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1655
    .local v1, "delMsg":Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1656
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1657
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1658
    const v6, 0x7f0b00b6

    new-instance v7, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$17;

    invoke-direct {v7, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$17;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    invoke-virtual {v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 1665
    const v7, 0x7f0b0005

    new-instance v8, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$18;

    invoke-direct {v8, p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$18;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1671
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCancelDialog:Landroid/app/AlertDialog;

    .line 1672
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCancelDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public updateInsertButton()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 627
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    if-eqz v0, :cond_0

    .line 636
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mInsertButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 637
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isNoOutgoingCall()Z

    move-result v0

    if-nez v0, :cond_1

    .line 638
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mInsertButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 639
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 649
    :cond_0
    :goto_0
    return-void

    .line 640
    :cond_1
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isUPSM()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 641
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mInsertButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 642
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceButton:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceButtonEnable:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 644
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mInsertButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 645
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceButton:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mIsVoiceButtonEnable:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method
