.class Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$ViewAction;
.super Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;
.source "LinkActionChooserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ViewAction"
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "titleId"    # I

    .prologue
    .line 250
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;-><init>(I)V

    .line 251
    return-void
.end method


# virtual methods
.method public execute(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/app/Activity;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 255
    invoke-static {p1, p2}, Lcom/samsung/android/app/memo/util/Utils;->getIntentForUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 256
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 257
    invoke-static {p1, v0}, Lcom/samsung/android/app/memo/util/Utils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)Z

    .line 258
    return-void
.end method
