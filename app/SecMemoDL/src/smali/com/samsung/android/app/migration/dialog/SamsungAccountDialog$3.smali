.class Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$3;
.super Ljava/lang/Object;
.source "SamsungAccountDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$3;->this$0:Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 89
    iget-object v2, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$3;->this$0:Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;

    invoke-virtual {v2}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$3;->this$0:Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;

    # getter for: Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->mDoNotShowAgain:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->access$0(Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/migration/utils/SharedPref;->setSADoNotShowFlag(Landroid/content/Context;Z)V

    .line 90
    iget-object v2, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$3;->this$0:Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;

    invoke-virtual {v2}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    .line 91
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_0

    .line 92
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 93
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "client_id"

    const-string v3, "kqq79c436g"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    const-string v2, "client_secret"

    const-string v3, "51957371B6C4D7552C91EF680479AAE2"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    const-string v2, "mypackage"

    iget-object v3, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$3;->this$0:Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;

    invoke-virtual {v3}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    const-string v2, "OSP_VER"

    const-string v3, "OSP_02"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    const-string v2, "MODE"

    const-string v3, "ADD_ACCOUNT"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    iget-object v2, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$3;->this$0:Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->startActivity(Landroid/content/Intent;)V

    .line 105
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$3;->this$0:Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;

    invoke-virtual {v2}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->hide()V

    .line 106
    return-void

    .line 100
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.ACCOUNT_SYNC_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 101
    .restart local v1    # "intent":Landroid/content/Intent;
    const/high16 v2, 0x4000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 102
    const-string v2, "account"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 103
    iget-object v2, p0, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog$3;->this$0:Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/migration/dialog/SamsungAccountDialog;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
