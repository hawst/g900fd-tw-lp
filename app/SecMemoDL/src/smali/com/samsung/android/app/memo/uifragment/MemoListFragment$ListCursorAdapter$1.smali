.class Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$1;
.super Ljava/lang/Object;
.source "MemoListFragment.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    .line 1737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x3

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1740
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->access$0(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    .line 1741
    const v6, 0x7f0e0026

    invoke-virtual {v3, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1740
    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    .line 1742
    .local v0, "drawerLayout":Landroid/support/v4/widget/DrawerLayout;
    if-eqz v0, :cond_0

    .line 1743
    invoke-virtual {v0, v7}, Landroid/support/v4/widget/DrawerLayout;->isDrawerOpen(I)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v4

    .line 1769
    :goto_0
    return v3

    .line 1746
    :cond_0
    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    .line 1748
    .local v2, "toolType":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 1767
    :pswitch_0
    invoke-virtual {p1, v5}, Landroid/view/View;->setHovered(Z)V

    :cond_1
    :goto_1
    move v3, v5

    .line 1769
    goto :goto_0

    .line 1751
    :pswitch_1
    if-ne v2, v7, :cond_2

    move v3, v4

    .line 1752
    goto :goto_0

    .line 1754
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter$1;->this$1:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->access$0(Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 1755
    const-string v6, "finger_air_view"

    .line 1754
    invoke-static {v3, v6, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 1753
    if-eqz v3, :cond_3

    move v1, v4

    .line 1756
    .local v1, "isEnableFingerAirview":Z
    :goto_2
    if-eqz v1, :cond_4

    .line 1757
    invoke-virtual {p1, v4}, Landroid/view/View;->setHovered(Z)V

    .line 1761
    :goto_3
    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 1762
    invoke-virtual {p1, v5}, Landroid/view/View;->setHovered(Z)V

    goto :goto_1

    .end local v1    # "isEnableFingerAirview":Z
    :cond_3
    move v1, v5

    .line 1753
    goto :goto_2

    .line 1759
    .restart local v1    # "isEnableFingerAirview":Z
    :cond_4
    invoke-virtual {p1, v5}, Landroid/view/View;->setHovered(Z)V

    goto :goto_3

    .line 1748
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
