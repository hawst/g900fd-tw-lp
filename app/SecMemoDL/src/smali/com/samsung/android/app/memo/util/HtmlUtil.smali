.class public final Lcom/samsung/android/app/memo/util/HtmlUtil;
.super Ljava/lang/Object;
.source "HtmlUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/util/HtmlUtil$HtmlParser;,
        Lcom/samsung/android/app/memo/util/HtmlUtil$ImageGetter;,
        Lcom/samsung/android/app/memo/util/HtmlUtil$TagHandler;
    }
.end annotation


# static fields
.field public static final HTML_DOC:Ljava/lang/String; = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">"

.field private static final STRIP_EMPTY_LINE:Ljava/lang/String; = "(?m)^\\s*$[\n\r]{1,}"

.field private static final TAG:Ljava/lang/String;

.field private static isImageSpan:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/samsung/android/app/memo/util/HtmlUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/util/HtmlUtil;->TAG:Ljava/lang/String;

    .line 264
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/memo/util/HtmlUtil;->isImageSpan:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    sget-object v0, Lcom/samsung/android/app/memo/util/HtmlUtil;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/memo/util/HtmlUtil;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    return-void
.end method

.method public static fromHtml(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;J)Landroid/text/Spanned;
    .locals 9
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;
    .param p3, "_ID"    # J

    .prologue
    const/4 v2, 0x0

    .line 135
    move-object v1, p0

    move-object v3, v2

    move-object v4, p1

    move-object v5, p2

    move-wide v6, p3

    invoke-static/range {v1 .. v7}, Lcom/samsung/android/app/memo/util/HtmlUtil;->fromHtml(Ljava/lang/String;Lcom/samsung/android/app/memo/util/HtmlUtil$ImageGetter;Lcom/samsung/android/app/memo/util/HtmlUtil$TagHandler;Landroid/content/Context;Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;J)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public static fromHtml(Ljava/lang/String;Lcom/samsung/android/app/memo/util/HtmlUtil$ImageGetter;Lcom/samsung/android/app/memo/util/HtmlUtil$TagHandler;Landroid/content/Context;Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;J)Landroid/text/Spanned;
    .locals 11
    .param p0, "source"    # Ljava/lang/String;
    .param p1, "imageGetter"    # Lcom/samsung/android/app/memo/util/HtmlUtil$ImageGetter;
    .param p2, "tagHandler"    # Lcom/samsung/android/app/memo/util/HtmlUtil$TagHandler;
    .param p3, "ctx"    # Landroid/content/Context;
    .param p4, "listener"    # Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;
    .param p5, "_ID"    # J

    .prologue
    .line 158
    new-instance v5, Lorg/ccil/cowan/tagsoup/Parser;

    invoke-direct {v5}, Lorg/ccil/cowan/tagsoup/Parser;-><init>()V

    .line 160
    .local v5, "parser":Lorg/ccil/cowan/tagsoup/Parser;
    :try_start_0
    const-string v2, "http://www.ccil.org/~cowan/tagsoup/properties/schema"

    # getter for: Lcom/samsung/android/app/memo/util/HtmlUtil$HtmlParser;->schema:Lorg/ccil/cowan/tagsoup/HTMLSchema;
    invoke-static {}, Lcom/samsung/android/app/memo/util/HtmlUtil$HtmlParser;->access$0()Lorg/ccil/cowan/tagsoup/HTMLSchema;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Lorg/ccil/cowan/tagsoup/Parser;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/xml/sax/SAXNotRecognizedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXNotSupportedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 169
    new-instance v1, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    move-object v7, p4

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;-><init>(Ljava/lang/String;Lcom/samsung/android/app/memo/util/HtmlUtil$ImageGetter;Lcom/samsung/android/app/memo/util/HtmlUtil$TagHandler;Lorg/ccil/cowan/tagsoup/Parser;Landroid/content/Context;Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;J)V

    .line 171
    .local v1, "converter":Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;
    invoke-virtual {v1}, Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;->convert()Landroid/text/Spanned;

    move-result-object v2

    return-object v2

    .line 161
    .end local v1    # "converter":Lcom/samsung/android/app/memo/util/HtmlToSpannedConverter;
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Lorg/xml/sax/SAXNotRecognizedException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 164
    .end local v0    # "e":Lorg/xml/sax/SAXNotRecognizedException;
    :catch_1
    move-exception v0

    .line 166
    .local v0, "e":Lorg/xml/sax/SAXNotSupportedException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static getPlainTextFromHTML(Ljava/lang/String;Landroid/content/Context;Ljava/lang/Object;J)Ljava/lang/String;
    .locals 3
    .param p0, "content"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "object"    # Ljava/lang/Object;
    .param p3, "l"    # J

    .prologue
    .line 391
    const/4 v1, 0x0

    invoke-static {p0, p1, v1, p3, p4}, Lcom/samsung/android/app/memo/util/HtmlUtil;->fromHtml(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;J)Landroid/text/Spanned;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Spanned;->toString()Ljava/lang/String;

    move-result-object v0

    .line 392
    .local v0, "text":Ljava/lang/String;
    const-string v1, "\n\ufffc\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 393
    const-string v1, "(\\n\\uFFFC\\n)+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 394
    :cond_0
    const-string v1, "\ufffc\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 395
    const-string v1, "(\\uFFFC\\n)+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 396
    :cond_1
    const-string v1, "(\\n\\uFFFC\\n)+"

    const-string v2, "\n"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 397
    const-string v1, "(\\uFFFC\\n)+"

    const-string v2, "\n"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 398
    const-string v1, "(\\uFFFC)+"

    const-string v2, "\n"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 399
    const/16 v1, 0xa0

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 400
    return-object v0
.end method

.method public static isHtml(Ljava/lang/String;)Z
    .locals 1
    .param p0, "mText"    # Ljava/lang/String;

    .prologue
    .line 186
    const-string v0, "<p>"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "<br>"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "<img"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    :cond_0
    const/4 v0, 0x1

    .line 190
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static processHtmlImage(Ljava/lang/StringBuilder;Landroid/text/Spanned;II)V
    .locals 7
    .param p0, "out"    # Ljava/lang/StringBuilder;
    .param p1, "text"    # Landroid/text/Spanned;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 352
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-lt v0, p3, :cond_0

    .line 363
    return-void

    .line 353
    :cond_0
    const-class v4, Landroid/text/SpannableString;

    invoke-interface {p1, v0, p3, v4}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v3

    .line 354
    .local v3, "next":I
    const-class v4, Landroid/text/SpannableString;

    invoke-interface {p1, v0, v3, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/SpannableString;

    .line 356
    .local v2, "images":[Landroid/text/SpannableString;
    array-length v5, v2

    const/4 v4, 0x0

    :goto_1
    if-lt v4, v5, :cond_1

    .line 360
    invoke-interface {p1}, Landroid/text/Spanned;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v0, v3}, Lcom/samsung/android/app/memo/util/HtmlUtil;->withinStyle(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    .line 352
    move v0, v3

    goto :goto_0

    .line 356
    :cond_1
    aget-object v1, v2, v4

    .line 357
    .local v1, "image":Landroid/text/SpannableString;
    invoke-virtual {v1}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public static final stripHtml(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 3
    .param p0, "htmlContent"    # Ljava/lang/CharSequence;

    .prologue
    .line 103
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Spanned;->toString()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "text":Ljava/lang/String;
    const v1, 0xfffc

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 105
    return-object v0
.end method

.method public static final stripHtmlToSingleLine(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 3
    .param p0, "htmlContent"    # Ljava/lang/CharSequence;

    .prologue
    .line 114
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Spanned;->toString()Ljava/lang/String;

    move-result-object v0

    .line 115
    .local v0, "text":Ljava/lang/String;
    const v1, 0xfffc

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 116
    const-string v1, "(?m)^\\s*$[\n\r]{1,}"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    return-object v0
.end method

.method public static toHtml(Landroid/text/Spanned;)Ljava/lang/String;
    .locals 5
    .param p0, "text"    # Landroid/text/Spanned;

    .prologue
    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 179
    .local v1, "out":Ljava/lang/StringBuilder;
    invoke-static {v1, p0}, Lcom/samsung/android/app/memo/util/HtmlUtil;->withinHtml(Ljava/lang/StringBuilder;Landroid/text/Spanned;)V

    .line 180
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\ufffc"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 181
    .local v0, "htmlStr":Ljava/lang/String;
    const-string v2, "<p> "

    const-string v3, "<p>&nbsp;"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 182
    return-object v0
.end method

.method private static withinBlockquote(Ljava/lang/StringBuilder;Landroid/text/Spanned;II)V
    .locals 8
    .param p0, "out"    # Ljava/lang/StringBuilder;
    .param p1, "text"    # Landroid/text/Spanned;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/16 v7, 0xa

    .line 246
    move v2, p2

    .local v2, "i":I
    :goto_0
    if-lt v2, p3, :cond_0

    .line 263
    return-void

    .line 247
    :cond_0
    invoke-static {p1, v7, v2, p3}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;CII)I

    move-result v6

    .line 248
    .local v6, "next":I
    if-gez v6, :cond_1

    .line 249
    move v6, p3

    .line 252
    :cond_1
    const/4 v4, 0x0

    .line 254
    .local v4, "nl":I
    :goto_1
    if-ge v6, p3, :cond_2

    invoke-interface {p1, v6}, Landroid/text/Spanned;->charAt(I)C

    move-result v0

    if-eq v0, v7, :cond_3

    .line 259
    :cond_2
    sub-int v3, v6, v4

    if-ne v6, p3, :cond_4

    const/4 v5, 0x1

    :goto_2
    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/app/memo/util/HtmlUtil;->withinParagraph(Ljava/lang/StringBuilder;Landroid/text/Spanned;IIIZ)V

    .line 246
    move v2, v6

    goto :goto_0

    .line 255
    :cond_3
    add-int/lit8 v4, v4, 0x1

    .line 256
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 259
    :cond_4
    const/4 v5, 0x0

    goto :goto_2
.end method

.method private static withinDiv(Ljava/lang/StringBuilder;Landroid/text/Spanned;II)V
    .locals 3
    .param p0, "out"    # Ljava/lang/StringBuilder;
    .param p1, "text"    # Landroid/text/Spanned;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 227
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-lt v0, p3, :cond_0

    .line 240
    return-void

    .line 228
    :cond_0
    const-class v2, Landroid/text/style/QuoteSpan;

    invoke-interface {p1, v0, p3, v2}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v1

    .line 234
    .local v1, "next":I
    invoke-static {p0, p1, v0, v1}, Lcom/samsung/android/app/memo/util/HtmlUtil;->withinBlockquote(Ljava/lang/StringBuilder;Landroid/text/Spanned;II)V

    .line 227
    move v0, v1

    goto :goto_0
.end method

.method private static withinHtml(Ljava/lang/StringBuilder;Landroid/text/Spanned;)V
    .locals 5
    .param p0, "out"    # Ljava/lang/StringBuilder;
    .param p1, "text"    # Landroid/text/Spanned;

    .prologue
    .line 194
    invoke-interface {p1}, Landroid/text/Spanned;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 223
    :goto_0
    return-void

    .line 197
    :cond_0
    invoke-interface {p1}, Landroid/text/Spanned;->length()I

    move-result v1

    .line 198
    .local v1, "len":I
    const-string v3, "<p>"

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {p1}, Landroid/text/Spanned;->length()I

    move-result v3

    if-lt v0, v3, :cond_1

    .line 222
    const-string v3, "</p>"

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 202
    :cond_1
    const-class v3, Landroid/text/style/ParagraphStyle;

    invoke-interface {p1, v0, v1, v3}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v2

    .line 216
    .local v2, "next":I
    invoke-static {p0, p1, v0, v2}, Lcom/samsung/android/app/memo/util/HtmlUtil;->withinDiv(Ljava/lang/StringBuilder;Landroid/text/Spanned;II)V

    .line 201
    move v0, v2

    goto :goto_1
.end method

.method private static withinParagraph(Ljava/lang/StringBuilder;Landroid/text/Spanned;IIIZ)V
    .locals 6
    .param p0, "out"    # Ljava/lang/StringBuilder;
    .param p1, "text"    # Landroid/text/Spanned;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "nl"    # I
    .param p5, "last"    # Z

    .prologue
    .line 269
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-lt v0, p3, :cond_0

    .line 329
    const-string v3, "</p>\n<p>"

    .line 330
    .local v3, "p":Ljava/lang/String;
    const/4 v0, 0x0

    :goto_1
    if-lt v0, p4, :cond_4

    .line 347
    return-void

    .line 270
    .end local v3    # "p":Ljava/lang/String;
    :cond_0
    const-class v5, Landroid/text/style/CharacterStyle;

    invoke-interface {p1, v0, p3, v5}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v2

    .line 271
    .local v2, "next":I
    const-class v5, Landroid/text/style/CharacterStyle;

    invoke-interface {p1, v0, v2, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/text/style/CharacterStyle;

    .line 273
    .local v4, "style":[Landroid/text/style/CharacterStyle;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    array-length v5, v4

    if-lt v1, v5, :cond_2

    .line 306
    sget-boolean v5, Lcom/samsung/android/app/memo/util/HtmlUtil;->isImageSpan:Z

    if-nez v5, :cond_1

    .line 307
    invoke-static {p0, p1, v0, v2}, Lcom/samsung/android/app/memo/util/HtmlUtil;->processHtmlImage(Ljava/lang/StringBuilder;Landroid/text/Spanned;II)V

    .line 308
    :cond_1
    const/4 v5, 0x0

    sput-boolean v5, Lcom/samsung/android/app/memo/util/HtmlUtil;->isImageSpan:Z

    .line 269
    move v0, v2

    goto :goto_0

    .line 290
    :cond_2
    aget-object v5, v4, v1

    instance-of v5, v5, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    if-eqz v5, :cond_3

    .line 291
    aget-object v5, v4, v1

    check-cast v5, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->toHtml()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    const/4 v5, 0x1

    sput-boolean v5, Lcom/samsung/android/app/memo/util/HtmlUtil;->isImageSpan:Z

    .line 273
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 331
    .end local v1    # "j":I
    .end local v2    # "next":I
    .end local v4    # "style":[Landroid/text/style/CharacterStyle;
    .restart local v3    # "p":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static withinStyle(Ljava/lang/StringBuilder;Ljava/lang/String;II)V
    .locals 4
    .param p0, "out"    # Ljava/lang/StringBuilder;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/16 v3, 0x20

    .line 366
    move v1, p2

    .local v1, "i":I
    :goto_0
    if-lt v1, p3, :cond_0

    .line 388
    return-void

    .line 367
    :cond_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 368
    .local v0, "c":C
    if-ne v0, v3, :cond_3

    .line 369
    :goto_1
    add-int/lit8 v2, v1, 0x1

    if-ge v2, p3, :cond_1

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v2, v3, :cond_2

    .line 373
    :cond_1
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 366
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 370
    :cond_2
    const-string v2, "&nbsp;"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 371
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 374
    :cond_3
    const/16 v2, 0x3c

    if-ne v0, v2, :cond_4

    .line 375
    const-string v2, "&lt;"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 376
    :cond_4
    const/16 v2, 0x3e

    if-ne v0, v2, :cond_5

    .line 377
    const-string v2, "&gt;"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 378
    :cond_5
    const/16 v2, 0x26

    if-ne v0, v2, :cond_6

    .line 379
    const-string v2, "&amp;"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 380
    :cond_6
    const/16 v2, 0x22

    if-ne v0, v2, :cond_7

    .line 381
    const-string v2, "&quot;"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 382
    :cond_7
    const/16 v2, 0x27

    if-ne v0, v2, :cond_8

    .line 383
    const-string v2, "&apos;"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 385
    :cond_8
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2
.end method
