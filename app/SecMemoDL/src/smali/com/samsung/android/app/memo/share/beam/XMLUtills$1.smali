.class Lcom/samsung/android/app/memo/share/beam/XMLUtills$1;
.super Landroid/os/Handler;
.source "XMLUtills.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/share/beam/XMLUtills;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/share/beam/XMLUtills;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/share/beam/XMLUtills;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills$1;->this$0:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    .line 126
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    .line 128
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 137
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills$1;->this$0:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    # getter for: Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->access$3(Lcom/samsung/android/app/memo/share/beam/XMLUtills;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills$1;->this$0:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    # getter for: Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->access$3(Lcom/samsung/android/app/memo/share/beam/XMLUtills;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills$1;->this$0:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    # getter for: Lcom/samsung/android/app/memo/share/beam/XMLUtills;->mProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->access$3(Lcom/samsung/android/app/memo/share/beam/XMLUtills;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills$1;->this$0:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    invoke-static {v1, v3}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->access$2(Lcom/samsung/android/app/memo/share/beam/XMLUtills;Landroid/app/ProgressDialog;)V

    .line 144
    :goto_1
    return-void

    .line 130
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills$1;->this$0:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    # invokes: Lcom/samsung/android/app/memo/share/beam/XMLUtills;->parseXML()V
    invoke-static {v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->access$0(Lcom/samsung/android/app/memo/share/beam/XMLUtills;)V

    goto :goto_0

    .line 133
    :pswitch_1
    # getter for: Lcom/samsung/android/app/memo/share/beam/XMLUtills;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->access$1()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mUnzipHandler fail "

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "ie":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 142
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills$1;->this$0:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    invoke-static {v1, v3}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->access$2(Lcom/samsung/android/app/memo/share/beam/XMLUtills;Landroid/app/ProgressDialog;)V

    goto :goto_1

    .line 141
    .end local v0    # "ie":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    .line 142
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/beam/XMLUtills$1;->this$0:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->access$2(Lcom/samsung/android/app/memo/share/beam/XMLUtills;Landroid/app/ProgressDialog;)V

    .line 143
    throw v1

    .line 128
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
