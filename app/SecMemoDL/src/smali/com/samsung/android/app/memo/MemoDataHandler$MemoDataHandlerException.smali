.class public Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
.super Lcom/samsung/android/app/memo/Session$SessionException;
.source "MemoDataHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/MemoDataHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MemoDataHandlerException"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x3b17008b1087987L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 429
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Session$SessionException;-><init>()V

    .line 430
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 437
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/Session$SessionException;-><init>(Ljava/lang/String;)V

    .line 438
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;
    .param p2, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 433
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/memo/Session$SessionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 434
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 441
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/Session$SessionException;-><init>(Ljava/lang/Throwable;)V

    .line 442
    return-void
.end method
