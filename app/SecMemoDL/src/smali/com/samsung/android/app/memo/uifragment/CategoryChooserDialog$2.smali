.class Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;
.super Ljava/lang/Object;
.source "CategoryChooserDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->onCreateDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;)Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    return-object v0
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "l":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v3, 0x0

    .line 131
    if-nez p3, :cond_2

    .line 132
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 133
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-static {v3, v3}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DialogFragment;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    iput-object v1, v2, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    .line 134
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    new-instance v2, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2$1;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->setOnDialogResultListener(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;)V

    .line 140
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->access$1(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 149
    :cond_1
    :goto_0
    return-void

    .line 144
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 145
    .local v0, "categoryUUID":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->access$3(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/memo/Session;->setCategoryUUID(Ljava/lang/String;)V

    .line 146
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->access$4(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_0
.end method
