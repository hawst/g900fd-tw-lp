.class public Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;
.super Landroid/app/DialogFragment;
.source "ProgressBarDialog.java"


# static fields
.field private static m_msg:Ljava/lang/String;

.field private static m_title:Ljava/lang/String;

.field private static m_type:Z


# instance fields
.field private m_dlg:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;
    .locals 1
    .param p0, "title"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "type"    # Z

    .prologue
    .line 39
    sput-object p0, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->m_title:Ljava/lang/String;

    .line 40
    sput-object p1, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->m_msg:Ljava/lang/String;

    .line 41
    sput-boolean p2, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->m_type:Z

    .line 42
    new-instance v0, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;-><init>()V

    .line 43
    .local v0, "f":Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;
    return-object v0
.end method


# virtual methods
.method public hide()V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->dismiss()V

    .line 57
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 66
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 67
    .local v0, "dlg":Landroid/app/ProgressDialog;
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 68
    sget-object v1, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->m_title:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 69
    sget-object v1, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->m_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 70
    :cond_0
    sget-object v1, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->m_msg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 71
    sget-boolean v1, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->m_type:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 73
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 74
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->setCancelable(Z)V

    .line 75
    iput-object v0, p0, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->m_dlg:Landroid/app/ProgressDialog;

    .line 76
    return-object v0

    .line 72
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setProgress(I)V
    .locals 1
    .param p1, "percent"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->m_dlg:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->m_dlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 62
    :cond_0
    return-void
.end method

.method public show(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 47
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "migrationDialog"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 48
    return-void
.end method
