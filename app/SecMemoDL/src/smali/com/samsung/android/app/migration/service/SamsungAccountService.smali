.class public Lcom/samsung/android/app/migration/service/SamsungAccountService;
.super Landroid/app/Service;
.source "SamsungAccountService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/migration/service/SamsungAccountService$SACallback;
    }
.end annotation


# static fields
.field public static final REQUEST_ACCESSTOKEN:I = 0x2

.field public static final REQUEST_CHECKLIST_VALIDATION:I = 0x1

.field private static final SA_SERVICE_NAME:Ljava/lang/String; = "com.msc.action.samsungaccount.REQUEST_SERVICE"

.field private static final TAG:Ljava/lang/String; = "SamsungAccountService"

.field private static mInstance:Lcom/samsung/android/app/migration/service/SamsungAccountService;


# instance fields
.field final additionalData:[Ljava/lang/String;

.field private mExpiredAccessToken:Ljava/lang/String;

.field private mISAService:Lcom/msc/sa/aidl/ISAService;

.field public mIsAwaitingResponse:Z

.field public mIsConnected:Z

.field private mRegistrationCode:Ljava/lang/String;

.field private mRequestID:I

.field private mRequestType:I

.field private mSACallback:Lcom/samsung/android/app/migration/service/SamsungAccountService$SACallback;

.field mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 41
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 53
    iput-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mSACallback:Lcom/samsung/android/app/migration/service/SamsungAccountService$SACallback;

    .line 54
    iput-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mISAService:Lcom/msc/sa/aidl/ISAService;

    .line 57
    iput v3, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mRequestID:I

    .line 59
    iput-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mRegistrationCode:Ljava/lang/String;

    .line 66
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "user_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "birthday"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mcc"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "cc"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "api_server_url"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 67
    const-string v2, "auth_server_url"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "device_physical_address_text"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "refresh_token"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "login_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "login_id_type"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->additionalData:[Ljava/lang/String;

    .line 71
    iput-boolean v3, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mIsConnected:Z

    .line 72
    iput-boolean v3, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mIsAwaitingResponse:Z

    .line 78
    new-instance v0, Lcom/samsung/android/app/migration/service/SamsungAccountService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/migration/service/SamsungAccountService$1;-><init>(Lcom/samsung/android/app/migration/service/SamsungAccountService;)V

    iput-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 41
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/migration/service/SamsungAccountService;Lcom/msc/sa/aidl/ISAService;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mISAService:Lcom/msc/sa/aidl/ISAService;

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/app/migration/service/SamsungAccountService;Lcom/samsung/android/app/migration/service/SamsungAccountService$SACallback;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mSACallback:Lcom/samsung/android/app/migration/service/SamsungAccountService$SACallback;

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/app/migration/service/SamsungAccountService;)Z
    .locals 1

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->registerCallback()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3()Lcom/samsung/android/app/migration/service/SamsungAccountService;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mInstance:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/app/migration/service/SamsungAccountService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mExpiredAccessToken:Ljava/lang/String;

    return-void
.end method

.method public static getInstance()Lcom/samsung/android/app/migration/service/SamsungAccountService;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mInstance:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    return-object v0
.end method

.method private registerCallback()Z
    .locals 9

    .prologue
    .line 140
    :try_start_0
    const-string v1, "kqq79c436g"

    .line 141
    .local v1, "serviceAPPClientID":Ljava/lang/String;
    const-string v3, "51957371B6C4D7552C91EF680479AAE2"

    .line 142
    .local v3, "serviceAPPClientSecret":Ljava/lang/String;
    const-string v2, "com.samsung.android.app.memo"

    .line 144
    .local v2, "serviceAPPClientPackageName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mISAService:Lcom/msc/sa/aidl/ISAService;

    const-string v5, "kqq79c436g"

    const-string v6, "51957371B6C4D7552C91EF680479AAE2"

    const-string v7, "com.samsung.android.app.memo"

    iget-object v8, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mSACallback:Lcom/samsung/android/app/migration/service/SamsungAccountService$SACallback;

    invoke-interface {v4, v5, v6, v7, v8}, Lcom/msc/sa/aidl/ISAService;->registerCallback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/aidl/ISACallback;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mRegistrationCode:Ljava/lang/String;

    .line 145
    iget-object v4, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mRegistrationCode:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 153
    const-string v4, "SamsungAccountService"

    const-string v5, "registerCallback failed!"

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    const/4 v4, 0x0

    .line 159
    .end local v1    # "serviceAPPClientID":Ljava/lang/String;
    .end local v2    # "serviceAPPClientPackageName":Ljava/lang/String;
    .end local v3    # "serviceAPPClientSecret":Ljava/lang/String;
    :goto_0
    return v4

    .line 156
    :catch_0
    move-exception v0

    .line 157
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "SamsungAccountService"

    const-string v5, "RemoteException in registerCallback(). "

    invoke-static {v4, v5, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 159
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private requestAccessToken()V
    .locals 8

    .prologue
    .line 201
    const-string v5, "SamsungAccountService"

    const-string v6, "requestAccessToken"

    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    .line 204
    .local v3, "manager":Landroid/accounts/AccountManager;
    const-string v5, "com.osp.app.signin"

    invoke-virtual {v3, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 206
    .local v0, "accountArr":[Landroid/accounts/Account;
    array-length v5, v0

    if-lez v5, :cond_2

    .line 207
    const-string v5, "SamsungAccountService"

    const-string v6, "AccountManager > 0"

    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :try_start_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 211
    .local v1, "data":Landroid/os/Bundle;
    iget-object v5, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mExpiredAccessToken:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 212
    const-string v5, "expired_access_token"

    iget-object v6, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mExpiredAccessToken:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mISAService:Lcom/msc/sa/aidl/ISAService;

    iget v6, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mRequestID:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mRequestID:I

    iget-object v7, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mRegistrationCode:Ljava/lang/String;

    invoke-interface {v5, v6, v7, v1}, Lcom/msc/sa/aidl/ISAService;->requestAccessToken(ILjava/lang/String;Landroid/os/Bundle;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 215
    .local v4, "result":Ljava/lang/Boolean;
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_1

    .line 222
    const-string v5, "SamsungAccountService"

    const-string v6, "requestAccessToken FAILED"

    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    .end local v1    # "data":Landroid/os/Bundle;
    .end local v4    # "result":Ljava/lang/Boolean;
    :goto_0
    return-void

    .line 224
    .restart local v1    # "data":Landroid/os/Bundle;
    .restart local v4    # "result":Ljava/lang/Boolean;
    :cond_1
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mIsAwaitingResponse:Z

    .line 225
    const-string v5, "SamsungAccountService"

    const-string v6, "requestAccessToken SUCCESS"

    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 227
    .end local v1    # "data":Landroid/os/Bundle;
    .end local v4    # "result":Ljava/lang/Boolean;
    :catch_0
    move-exception v2

    .line 228
    .local v2, "e":Landroid/os/RemoteException;
    const-string v5, "SamsungAccountService"

    const-string v6, "RemoteException in requestAccessToken(). "

    invoke-static {v5, v6, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 231
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_2
    const-string v5, "SamsungAccountService"

    const-string v6, "AccountManager < 0"

    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private requestChecklistValidation()V
    .locals 8

    .prologue
    .line 236
    const-string v4, "SamsungAccountService"

    const-string v5, "requestChecklistValidation"

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 239
    .local v2, "manager":Landroid/accounts/AccountManager;
    const-string v4, "com.osp.app.signin"

    invoke-virtual {v2, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 241
    .local v0, "accountArr":[Landroid/accounts/Account;
    array-length v4, v0

    if-lez v4, :cond_0

    .line 243
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mISAService:Lcom/msc/sa/aidl/ISAService;

    iget v5, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mRequestID:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mRequestID:I

    iget-object v6, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mRegistrationCode:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-interface {v4, v5, v6, v7}, Lcom/msc/sa/aidl/ISAService;->requestChecklistValidation(ILjava/lang/String;Landroid/os/Bundle;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 244
    .local v3, "result":Ljava/lang/Boolean;
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_1

    .line 251
    const-string v4, "SamsungAccountService"

    const-string v5, "requestChecklistValidation FAILED"

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    .end local v3    # "result":Ljava/lang/Boolean;
    :cond_0
    :goto_0
    return-void

    .line 253
    .restart local v3    # "result":Ljava/lang/Boolean;
    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mIsAwaitingResponse:Z

    .line 254
    const-string v4, "SamsungAccountService"

    const-string v5, "requestChecklistValidation SUCCESS"

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 256
    .end local v3    # "result":Ljava/lang/Boolean;
    :catch_0
    move-exception v1

    .line 257
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "SamsungAccountService"

    const-string v5, "Exception in requestChecklistValidation. "

    invoke-static {v4, v5, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private unregisterCallback()V
    .locals 4

    .prologue
    .line 163
    const-string v2, "SamsungAccountService"

    const-string v3, "unregisterCallback"

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v2, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mISAService:Lcom/msc/sa/aidl/ISAService;

    if-nez v2, :cond_1

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mISAService:Lcom/msc/sa/aidl/ISAService;

    iget-object v3, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mRegistrationCode:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/msc/sa/aidl/ISAService;->unregisterCallback(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 168
    .local v1, "result":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 175
    const-string v2, "SamsungAccountService"

    const-string v3, "unregisterCallback failed!"

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 177
    .end local v1    # "result":Ljava/lang/Boolean;
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "SamsungAccountService"

    const-string v3, "RemoteException in unregisterCallback(). "

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 105
    const-string v0, "SamsungAccountService"

    const-string v1, "onBind"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 128
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 129
    const-string v0, "SamsungAccountService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-direct {p0}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->unregisterCallback()V

    .line 131
    iget-object v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->unbindService(Landroid/content/ServiceConnection;)V

    .line 132
    iput-object v2, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 133
    iput-object v2, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mISAService:Lcom/msc/sa/aidl/ISAService;

    .line 134
    sput-object v2, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mInstance:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    .line 135
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mIsConnected:Z

    .line 136
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 111
    const-string v2, "SamsungAccountService"

    const-string v3, "onStartCommand"

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 113
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 114
    const-string v2, "requestType"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mRequestType:I

    .line 115
    const-string v2, "expiredAccessToken"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mExpiredAccessToken:Ljava/lang/String;

    .line 116
    const-string v2, "SamsungAccountService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "requestType = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mRequestType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", expiredAccessToken = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mExpiredAccessToken:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_0
    sput-object p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mInstance:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    .line 119
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 120
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 121
    const-string v2, "com.osp.app.signin"

    const-string v3, "com.msc.sa.service.RequestService"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    iget-object v2, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 123
    const/4 v2, 0x2

    return v2
.end method

.method public requestAIDLInterface()V
    .locals 2

    .prologue
    .line 183
    sget-object v0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mInstance:Lcom/samsung/android/app/migration/service/SamsungAccountService;

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 184
    iget v0, p0, Lcom/samsung/android/app/migration/service/SamsungAccountService;->mRequestType:I

    packed-switch v0, :pswitch_data_0

    .line 198
    :goto_0
    return-void

    .line 186
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->requestChecklistValidation()V

    goto :goto_0

    .line 189
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->requestAccessToken()V

    goto :goto_0

    .line 195
    :cond_0
    const-string v0, "SamsungAccountService"

    const-string v1, "SA is signed out abnormally."

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/service/SamsungAccountService;->stopSelf()V

    goto :goto_0

    .line 184
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
