.class Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;
.super Ljava/lang/Object;
.source "MemoContentFragment.java"

# interfaces
.implements Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->initSConnect(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

.field private final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iput-object p2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->val$activity:Landroid/app/Activity;

    .line 413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected()V
    .locals 3

    .prologue
    .line 418
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 419
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->getState()Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    if-ne v1, v2, :cond_2

    .line 420
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-result-object v1

    const v2, 0x7f0b00ac

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    .line 421
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleRecordingStop()V

    .line 427
    :cond_0
    :goto_0
    new-instance v0, Lcom/samsung/android/app/memo/share/MemoShare;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->val$activity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/share/MemoShare;-><init>(Landroid/content/Context;)V

    .line 428
    .local v0, "m":Lcom/samsung/android/app/memo/share/MemoShare;
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/share/MemoShare;->setText(Ljava/lang/String;)V

    .line 429
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/app/memo/util/HtmlUtil;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/share/MemoShare;->setHtmlText(Ljava/lang/String;)V

    .line 430
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getAttachmentList()Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$22(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/share/MemoShare;->setImageAttachmentList(Ljava/util/ArrayList;)V

    .line 431
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$23(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 432
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mCurrentVoiceInfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$23(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/share/MemoShare;->setVoiceAttachment(Landroid/net/Uri;)V

    .line 434
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->val$activity:Landroid/app/Activity;

    const v2, 0x7f0b0009

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/share/MemoShare;->setTitle(Ljava/lang/String;)V

    .line 435
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mTitle:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$16(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/share/MemoShare;->setSubject(Ljava/lang/String;)V

    .line 436
    invoke-virtual {v0}, Lcom/samsung/android/app/memo/share/MemoShare;->shareOnSConnect()V

    .line 437
    return-void

    .line 422
    .end local v0    # "m":Lcom/samsung/android/app/memo/share/MemoShare;
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->getState()Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    if-ne v1, v2, :cond_0

    .line 423
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$10;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mVoiceView:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$8(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleStopPlaying()V

    goto/16 :goto_0
.end method
