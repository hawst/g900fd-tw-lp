.class public final Lcom/samsung/android/app/memo/ExternalPicker;
.super Lcom/samsung/android/app/memo/Main;
.source "ExternalPicker.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/android/app/memo/Main;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/samsung/android/app/memo/Main;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 49
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-static {p0}, Lcom/samsung/android/app/memo/util/Utils;->slideInAnimation(Landroid/content/Context;)V

    .line 52
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/samsung/android/app/memo/Main;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-static {p0}, Lcom/samsung/android/app/memo/util/Utils;->slideInAnimation(Landroid/content/Context;)V

    .line 43
    :cond_0
    return-void
.end method
