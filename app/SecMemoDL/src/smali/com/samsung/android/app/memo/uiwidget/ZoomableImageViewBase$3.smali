.class Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;
.super Ljava/lang/Object;
.source "ZoomableImageViewBase.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->zoomTo(FFFF)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

.field private final synthetic val$deltaScale:F

.field private final synthetic val$destX:F

.field private final synthetic val$destY:F

.field private final synthetic val$durationMs:F

.field private final synthetic val$oldScale:F

.field private final synthetic val$startTime:J


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;FJFFFF)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    iput p2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->val$durationMs:F

    iput-wide p3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->val$startTime:J

    iput p5, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->val$deltaScale:F

    iput p6, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->val$oldScale:F

    iput p7, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->val$destX:F

    iput p8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->val$destY:F

    .line 801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v11, 0x1

    .line 805
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 806
    .local v12, "now":J
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->val$durationMs:F

    iget-wide v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->val$startTime:J

    sub-long v2, v12, v2

    long-to-float v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 807
    .local v0, "currentMs":F
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    float-to-double v2, v0

    const-wide/16 v4, 0x0

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->val$deltaScale:F

    float-to-double v6, v6

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->val$durationMs:F

    float-to-double v8, v8

    invoke-virtual/range {v1 .. v9}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->easeInOut(DDDD)D

    move-result-wide v2

    double-to-float v10, v2

    .line 808
    .local v10, "newScale":F
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->val$oldScale:F

    add-float/2addr v2, v10

    iget v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->val$destX:F

    iget v4, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->val$destY:F

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->zoomTo(FFF)V

    .line 809
    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->val$durationMs:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 810
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 815
    :goto_0
    return-void

    .line 812
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->onZoomAnimationCompleted(F)V

    .line 813
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;

    invoke-virtual {v1, v11, v11}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->center(ZZ)V

    goto :goto_0
.end method
