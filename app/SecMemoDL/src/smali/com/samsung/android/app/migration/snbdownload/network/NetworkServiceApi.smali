.class public Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;
.super Ljava/lang/Object;
.source "NetworkServiceApi.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NetworkServiceApi"

.field private static mNetworkServiceApi:Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDeviceCC:Ljava/lang/String;

.field public mDeviceIMSI:Ljava/lang/String;

.field private mDeviceMNC:Ljava/lang/String;

.field public mNetworktype:I

.field private mRegisteredCC:Ljava/lang/String;

.field private mRegisteredMNC:Ljava/lang/String;

.field public mRegisteredNetwork:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mNetworkServiceApi:Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mDeviceCC:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mDeviceMNC:Ljava/lang/String;

    .line 35
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mDeviceIMSI:Ljava/lang/String;

    .line 36
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredCC:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredMNC:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredNetwork:Ljava/lang/String;

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mNetworktype:I

    .line 42
    iput-object p1, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mContext:Landroid/content/Context;

    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->getDeviceInformation(Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method private getDeviceInformation(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x3

    .line 70
    const-string v3, "phone"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 71
    .local v1, "mTelephony":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v3

    if-eq v3, v7, :cond_0

    .line 72
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_0

    .line 73
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v5, :cond_2

    .line 74
    :cond_0
    const-string v3, ""

    iput-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mDeviceCC:Ljava/lang/String;

    .line 75
    const-string v3, ""

    iput-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mDeviceMNC:Ljava/lang/String;

    .line 76
    iput-object v8, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mDeviceIMSI:Ljava/lang/String;

    .line 83
    :goto_0
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v3

    if-eq v3, v7, :cond_1

    .line 84
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 85
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v5, :cond_3

    .line 86
    :cond_1
    const-string v3, ""

    iput-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredCC:Ljava/lang/String;

    .line 87
    const-string v3, ""

    iput-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredMNC:Ljava/lang/String;

    .line 88
    iput-object v8, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredNetwork:Ljava/lang/String;

    .line 95
    :goto_1
    return-void

    .line 78
    :cond_2
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "imsi":Ljava/lang/String;
    invoke-virtual {v0, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mDeviceCC:Ljava/lang/String;

    .line 80
    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mDeviceMNC:Ljava/lang/String;

    .line 81
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mDeviceIMSI:Ljava/lang/String;

    goto :goto_0

    .line 90
    .end local v0    # "imsi":Ljava/lang/String;
    :cond_3
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v2

    .line 91
    .local v2, "netopp":Ljava/lang/String;
    invoke-virtual {v2, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredCC:Ljava/lang/String;

    .line 92
    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredMNC:Ljava/lang/String;

    .line 93
    iput-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredNetwork:Ljava/lang/String;

    goto :goto_1
.end method

.method public static getNetworkServiceApi(Landroid/content/Context;)Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    sget-object v0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mNetworkServiceApi:Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mNetworkServiceApi:Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    .line 49
    :cond_0
    sget-object v0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mNetworkServiceApi:Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    return-object v0
.end method


# virtual methods
.method public getUserInfo(Ljava/lang/String;)V
    .locals 6
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 53
    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->getAsyncFactory(Landroid/content/Context;)Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;

    move-result-object v0

    .line 54
    .local v0, "asyncfactory":Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;
    const/4 v2, 0x0

    .line 55
    .local v2, "url":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredNetwork:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredNetwork:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v4, :cond_1

    .line 56
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https://sc-auth.samsungosp.com/auth/oauth2/getuserinfo?access_token="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 57
    const-string v4, "&client_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "kqq79c436g"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&mcc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mDeviceCC:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&mnc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mDeviceMNC:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 56
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 63
    :goto_0
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->executeURLs(Ljava/lang/String;IILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_1
    return-void

    .line 59
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https://sc-auth.samsungosp.com/auth/oauth2/getuserinfo?access_token="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 60
    const-string v4, "&client_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "kqq79c436g"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&mcc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredCC:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&mnc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mRegisteredMNC:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 59
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 64
    :catch_0
    move-exception v1

    .line 65
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "NetworkServiceApi"

    const-string v4, "Exception in getUserInfo. "

    invoke-static {v3, v4, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
