.class public Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;
.super Landroid/os/AsyncTask;
.source "MemoListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ShareQuickConnectTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field id:J

.field intent:Landroid/content/Intent;

.field mProgress:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

.field uri:Landroid/net/Uri;

.field xmlUtils:Lcom/samsung/android/app/memo/share/beam/XMLUtills;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/content/Intent;J)V
    .locals 3
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "id"    # J

    .prologue
    .line 2913
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2914
    iput-object p2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->intent:Landroid/content/Intent;

    .line 2915
    iput-wide p3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->id:J

    .line 2916
    new-instance v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    invoke-virtual {p1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->xmlUtils:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    .line 2917
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 2931
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->xmlUtils:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    iget-wide v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->id:J

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->createXMLFile(J)Ljava/lang/String;

    move-result-object v0

    .line 2932
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 2933
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->uri:Landroid/net/Uri;

    .line 2934
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const/4 v3, 0x0

    .line 2940
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2941
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2945
    :cond_0
    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->mProgress:Landroid/app/ProgressDialog;

    .line 2947
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->intent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->uri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2948
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2949
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$36(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->onFragmentSuicide(Ljava/lang/String;)V

    .line 2950
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2951
    return-void

    .line 2942
    :catch_0
    move-exception v0

    .line 2943
    .local v0, "ie":Ljava/lang/IllegalArgumentException;
    :try_start_1
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$1()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onPostExecute"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2945
    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->mProgress:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 2944
    .end local v0    # "ie":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    .line 2945
    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->mProgress:Landroid/app/ProgressDialog;

    .line 2946
    throw v1
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 2921
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$12(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/app/Activity;

    move-result-object v0

    const-string v1, ""

    .line 2922
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$12(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00ab

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2921
    invoke-static {v0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->mProgress:Landroid/app/ProgressDialog;

    .line 2923
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->mProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 2924
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isIndeterminate()Z

    .line 2925
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ShareQuickConnectTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 2926
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 2927
    return-void
.end method
