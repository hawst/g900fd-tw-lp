.class Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$2;
.super Ljava/lang/Object;
.source "CategoryMoveDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

.field private final synthetic val$context:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    iput-object p2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$2;->val$context:Landroid/app/Activity;

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "l":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->access$6(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$2;->val$context:Landroid/app/Activity;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mCategoryMoveIDs:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->access$3()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mSelectCategoryUUID:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->access$7(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/MemoDataHandler;->moveCategory(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->access$8(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 141
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->access$4(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 142
    sget v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 143
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->access$5(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->finishActionMode()V

    .line 145
    :cond_0
    return-void
.end method
