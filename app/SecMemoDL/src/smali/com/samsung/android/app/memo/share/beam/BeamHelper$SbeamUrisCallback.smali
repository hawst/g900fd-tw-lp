.class Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;
.super Ljava/lang/Object;
.source "BeamHelper.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/share/beam/BeamHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SbeamUrisCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)V
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createBeamUris(Landroid/nfc/NfcEvent;)[Landroid/net/Uri;
    .locals 7
    .param p1, "arg0"    # Landroid/nfc/NfcEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 315
    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->createMemoFile()V

    .line 316
    const-string v3, "BeamHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SbeamUrisCallback ["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I
    invoke-static {v5}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$1(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I
    invoke-static {v4}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$1(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 319
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.android.nfc.AndroidBeamPopUp"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 320
    .local v0, "i":Landroid/content/Intent;
    const-string v3, "POPUP_MODE"

    const-string v4, "no_file_selected"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 321
    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$3(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 322
    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$2(Lcom/samsung/android/app/memo/share/beam/BeamHelper;I)V

    .line 345
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-object v2

    .line 324
    :cond_0
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I
    invoke-static {v4}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$1(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)I

    move-result v4

    if-ne v3, v4, :cond_1

    .line 325
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.android.nfc.AndroidBeamPopUp"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 326
    .restart local v0    # "i":Landroid/content/Intent;
    const-string v3, "POPUP_MODE"

    const-string v4, "from_cloud_file"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 327
    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$3(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 328
    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$2(Lcom/samsung/android/app/memo/share/beam/BeamHelper;I)V

    goto :goto_0

    .line 330
    .end local v0    # "i":Landroid/content/Intent;
    :cond_1
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I
    invoke-static {v4}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$1(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 331
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.android.nfc.AndroidBeamPopUp"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 332
    .restart local v0    # "i":Landroid/content/Intent;
    const-string v3, "POPUP_MODE"

    const-string v4, "from_drm_file"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 333
    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$3(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 334
    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$2(Lcom/samsung/android/app/memo/share/beam/BeamHelper;I)V

    goto :goto_0

    .line 336
    .end local v0    # "i":Landroid/content/Intent;
    :cond_2
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I
    invoke-static {v4}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$1(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)I

    move-result v4

    if-ne v3, v4, :cond_3

    .line 337
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.android.nfc.AndroidBeamPopUp"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 338
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "POPUP_MODE"

    const-string v4, "does_not_saved"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 339
    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$3(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 340
    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-static {v3, v6}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$2(Lcom/samsung/android/app/memo/share/beam/BeamHelper;I)V

    goto :goto_0

    .line 344
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-static {v2, v6}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$2(Lcom/samsung/android/app/memo/share/beam/BeamHelper;I)V

    .line 345
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mUris:[Landroid/net/Uri;
    invoke-static {v2}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$5(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)[Landroid/net/Uri;

    move-result-object v2

    goto :goto_0
.end method
