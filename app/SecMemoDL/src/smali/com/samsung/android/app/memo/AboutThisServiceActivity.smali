.class public Lcom/samsung/android/app/memo/AboutThisServiceActivity;
.super Landroid/app/Activity;
.source "AboutThisServiceActivity.java"


# instance fields
.field private clickListener:Landroid/view/View$OnClickListener;

.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 51
    new-instance v0, Lcom/samsung/android/app/memo/AboutThisServiceActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/AboutThisServiceActivity$1;-><init>(Lcom/samsung/android/app/memo/AboutThisServiceActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/AboutThisServiceActivity;->clickListener:Landroid/view/View$OnClickListener;

    .line 14
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    .line 20
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/AboutThisServiceActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 23
    .local v2, "window":Landroid/view/Window;
    if-eqz v2, :cond_0

    .line 24
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 25
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v3, v3, 0x400

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 26
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 27
    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 29
    .end local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    const/high16 v3, 0x7f040000

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/memo/AboutThisServiceActivity;->setContentView(I)V

    .line 31
    iput-object p0, p0, Lcom/samsung/android/app/memo/AboutThisServiceActivity;->mContext:Landroid/content/Context;

    .line 33
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/AboutThisServiceActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 34
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/AboutThisServiceActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/AboutThisServiceActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00b8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 35
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/AboutThisServiceActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 36
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/AboutThisServiceActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 38
    const/high16 v3, 0x7f0e0000

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/memo/AboutThisServiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 39
    .local v1, "tv":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/samsung/android/app/memo/AboutThisServiceActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 45
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 46
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/AboutThisServiceActivity;->finish()V

    .line 48
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
