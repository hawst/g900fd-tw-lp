.class Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;
.super Landroid/os/AsyncTask;
.source "VoiceRecorderDefaultImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TimeCounterTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Long;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private startTime:J

.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;


# direct methods
.method private constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;)V
    .locals 0

    .prologue
    .line 170
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;-><init>(Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;)V

    return-void
.end method

.method private countTime()J
    .locals 4

    .prologue
    .line 174
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;->startTime:J

    sub-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 178
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;->startTime:J

    .line 180
    :goto_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Long;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;->countTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;->publishProgress([Ljava/lang/Object;)V

    .line 182
    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 183
    :catch_0
    move-exception v0

    .line 187
    const/4 v0, 0x0

    return-object v0
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Long;)V
    .locals 3
    .param p1, "values"    # [Ljava/lang/Long;

    .prologue
    .line 192
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 193
    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 194
    .local v0, "millis":J
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;->this$0:Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;

    invoke-static {v2, v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;->access$0(Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl;J)V

    .line 195
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderDefaultImpl$TimeCounterTask;->onProgressUpdate([Ljava/lang/Long;)V

    return-void
.end method
