.class public Lcom/samsung/android/app/migration/MemoConverter;
.super Ljava/lang/Object;
.source "MemoConverter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MemoConverter"

.field private static mInstance:Lcom/samsung/android/app/migration/MemoConverter;


# instance fields
.field private mContent:Ljava/lang/StringBuffer;

.field private mCreatedAt:J

.field private mLastModifiedAt:J

.field private mLock:Ljava/lang/Object;

.field private mSession:Lcom/samsung/android/app/memo/Session;

.field private mTMemo1Data:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/samsung/android/app/migration/task/TMemo1Data;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/MemoConverter;->mLock:Ljava/lang/Object;

    .line 57
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/MemoConverter;->mTMemo1Data:Ljava/util/concurrent/BlockingQueue;

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/migration/MemoConverter;->mSession:Lcom/samsung/android/app/memo/Session;

    .line 59
    return-void
.end method

.method private appendText(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/android/app/migration/MemoConverter;->mContent:Ljava/lang/StringBuffer;

    const-string v1, "<p>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "</p>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 107
    return-void
.end method

.method private createNew()Ljava/lang/String;
    .locals 4

    .prologue
    .line 68
    iget-object v1, p0, Lcom/samsung/android/app/migration/MemoConverter;->mSession:Lcom/samsung/android/app/memo/Session;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/migration/MemoConverter;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Session;->isDirty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/samsung/android/app/migration/MemoConverter;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Session;->cancelEditing()V

    .line 72
    :cond_0
    :try_start_0
    new-instance v1, Lcom/samsung/android/app/memo/Session;

    const-wide/16 v2, -0x1

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/app/memo/Session;-><init>(J)V

    iput-object v1, p0, Lcom/samsung/android/app/migration/MemoConverter;->mSession:Lcom/samsung/android/app/memo/Session;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    iget-object v1, p0, Lcom/samsung/android/app/migration/MemoConverter;->mContent:Ljava/lang/StringBuffer;

    if-nez v1, :cond_1

    .line 78
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/migration/MemoConverter;->mContent:Ljava/lang/StringBuffer;

    .line 81
    :goto_0
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/app/migration/MemoConverter;->mLastModifiedAt:J

    iput-wide v2, p0, Lcom/samsung/android/app/migration/MemoConverter;->mCreatedAt:J

    .line 82
    iget-object v1, p0, Lcom/samsung/android/app/migration/MemoConverter;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Session;->getUUID()Ljava/lang/String;

    move-result-object v1

    :goto_1
    return-object v1

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    const-string v1, "MemoConverter"

    const-string v2, "createMemo()"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const/4 v1, 0x0

    goto :goto_1

    .line 80
    .end local v0    # "se":Lcom/samsung/android/app/memo/Session$SessionException;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/migration/MemoConverter;->mContent:Ljava/lang/StringBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/samsung/android/app/migration/MemoConverter;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/samsung/android/app/migration/MemoConverter;->mInstance:Lcom/samsung/android/app/migration/MemoConverter;

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Lcom/samsung/android/app/migration/MemoConverter;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/MemoConverter;-><init>()V

    sput-object v0, Lcom/samsung/android/app/migration/MemoConverter;->mInstance:Lcom/samsung/android/app/migration/MemoConverter;

    .line 64
    :cond_0
    sget-object v0, Lcom/samsung/android/app/migration/MemoConverter;->mInstance:Lcom/samsung/android/app/migration/MemoConverter;

    return-object v0
.end method

.method private saveCurrent()J
    .locals 6

    .prologue
    .line 86
    iget-object v1, p0, Lcom/samsung/android/app/migration/MemoConverter;->mSession:Lcom/samsung/android/app/memo/Session;

    iget-object v2, p0, Lcom/samsung/android/app/migration/MemoConverter;->mContent:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/memo/Session;->setContent(Ljava/lang/String;)V

    .line 87
    iget-object v1, p0, Lcom/samsung/android/app/migration/MemoConverter;->mSession:Lcom/samsung/android/app/memo/Session;

    iget-wide v2, p0, Lcom/samsung/android/app/migration/MemoConverter;->mCreatedAt:J

    iget-wide v4, p0, Lcom/samsung/android/app/migration/MemoConverter;->mLastModifiedAt:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/app/memo/Session;->overrideDT(JJ)V

    .line 89
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/migration/MemoConverter;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Session;->saveMemo()J
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    iget-object v1, p0, Lcom/samsung/android/app/migration/MemoConverter;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/Session;->getId()J

    move-result-wide v2

    :goto_0
    return-wide v2

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    const-string v1, "MemoConverter"

    const-string v2, "saveCurrent()"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 92
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method private setCategory(Ljava/lang/String;)V
    .locals 1
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/android/app/migration/MemoConverter;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/memo/Session;->setCategory(Ljava/lang/String;)V

    .line 103
    return-void
.end method

.method private setDate(JJ)V
    .locals 1
    .param p1, "createdAt"    # J
    .param p3, "lastModifiedAt"    # J

    .prologue
    .line 110
    iput-wide p1, p0, Lcom/samsung/android/app/migration/MemoConverter;->mCreatedAt:J

    .line 111
    iput-wide p3, p0, Lcom/samsung/android/app/migration/MemoConverter;->mLastModifiedAt:J

    .line 112
    return-void
.end method

.method private setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/android/app/migration/MemoConverter;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/memo/Session;->setTitle(Ljava/lang/String;)V

    .line 99
    return-void
.end method


# virtual methods
.method public addToTMemo1Queue(Lcom/samsung/android/app/migration/task/TMemo1Data;)V
    .locals 2
    .param p1, "data"    # Lcom/samsung/android/app/migration/task/TMemo1Data;

    .prologue
    .line 215
    iget-object v1, p0, Lcom/samsung/android/app/migration/MemoConverter;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 216
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/migration/MemoConverter;->mTMemo1Data:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    .line 215
    monitor-exit v1

    .line 218
    return-void

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public convertParsedSNB(Landroid/content/Context;Ljava/lang/String;)J
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "snbPath"    # Ljava/lang/String;

    .prologue
    .line 171
    const-wide/16 v10, -0x1

    .line 172
    .local v10, "memoId":J
    invoke-static/range {p2 .. p2}, Lcom/samsung/android/app/migration/utils/Utils;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 173
    .local v3, "title":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/migration/xml/XMLParser;->getContent()Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, "content":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/migration/xml/XMLParser;->getTemplateType()Ljava/lang/String;

    move-result-object v14

    .line 175
    .local v14, "templateType":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/migration/xml/XMLParser;->getCreatedTime()J

    move-result-wide v6

    .line 176
    .local v6, "createdTime":J
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/migration/xml/XMLParser;->getModifiedTime()J

    move-result-wide v8

    .line 177
    .local v8, "modifiedTime":J
    const-string v1, "MemoConverter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "title = "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", templateType = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", content="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 178
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", createdTime = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", modifiedTime = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 177
    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    if-nez v0, :cond_0

    move-wide v12, v10

    .line 184
    .end local v10    # "memoId":J
    .local v12, "memoId":J
    :goto_0
    return-wide v12

    .line 181
    .end local v12    # "memoId":J
    .restart local v10    # "memoId":J
    :cond_0
    invoke-static {v0}, Lcom/samsung/android/app/migration/utils/Utils;->handleNewLines(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 182
    .local v4, "formattedContent":Ljava/lang/String;
    const-string v1, " "

    const-string v2, "&nbsp;"

    invoke-virtual {v4, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 183
    const/4 v5, 0x0

    move-object v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v9}, Lcom/samsung/android/app/migration/MemoConverter;->convertToNewMemo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)J

    move-result-wide v10

    move-wide v12, v10

    .line 184
    .end local v10    # "memoId":J
    .restart local v12    # "memoId":J
    goto :goto_0
.end method

.method public convertSNB(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)J
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "snbPath"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;

    .prologue
    .line 116
    const-wide/16 v12, -0x1

    .line 117
    .local v12, "memoId":J
    invoke-static/range {p2 .. p2}, Lcom/samsung/android/app/migration/utils/Utils;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 119
    .local v5, "title":Ljava/lang/String;
    sget-object v3, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_FOLDER_PATH:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 120
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p2}, Lcom/samsung/android/app/migration/utils/Utils;->getFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 123
    .local v15, "targetDir":Ljava/lang/String;
    :goto_0
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    invoke-direct {v0, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 124
    .local v17, "unzipFolder":Ljava/io/File;
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v0, v15, v1}, Lcom/samsung/android/app/memo/util/FileHelper;->unzip(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v14

    .line 125
    .local v14, "res":I
    if-nez v14, :cond_2

    .line 126
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v3

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/migration/xml/XMLParser;->parseXMLContent(Ljava/lang/String;)V

    .line 127
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v3

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/migration/xml/XMLParser;->parseXMLSettings(Ljava/lang/String;)V

    .line 128
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/migration/xml/XMLParser;->getContent()Ljava/lang/String;

    move-result-object v2

    .line 129
    .local v2, "content":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/migration/xml/XMLParser;->getTemplateType()Ljava/lang/String;

    move-result-object v16

    .line 130
    .local v16, "templateType":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/migration/xml/XMLParser;->getCreatedTime()J

    move-result-wide v8

    .line 131
    .local v8, "createdTime":J
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/migration/xml/XMLParser;->getModifiedTime()J

    move-result-wide v10

    .line 132
    .local v10, "modifiedTime":J
    const-string v3, "MemoConverter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "title = "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", templateType = "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", content="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 133
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", createdTime = "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", modifiedTime = "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 134
    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 132
    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/app/memo/util/FileHelper;->deleteFileItem(Ljava/io/File;)V

    .line 137
    const-string v3, "14"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 138
    invoke-static {v2}, Lcom/samsung/android/app/migration/utils/Utils;->handleNewLines(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 139
    .local v6, "formattedContent":Ljava/lang/String;
    const-string v3, " "

    const-string v4, "&nbsp;"

    invoke-virtual {v6, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    .line 140
    const/4 v7, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v11}, Lcom/samsung/android/app/migration/MemoConverter;->convertToNewMemo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)J

    move-result-wide v12

    move-wide/from16 v18, v12

    .line 145
    .end local v2    # "content":Ljava/lang/String;
    .end local v6    # "formattedContent":Ljava/lang/String;
    .end local v8    # "createdTime":J
    .end local v10    # "modifiedTime":J
    .end local v16    # "templateType":Ljava/lang/String;
    :goto_1
    return-wide v18

    .line 122
    .end local v14    # "res":I
    .end local v15    # "targetDir":Ljava/lang/String;
    .end local v17    # "unzipFolder":Ljava/io/File;
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_FOLDER_PATH:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "targetDir":Ljava/lang/String;
    goto/16 :goto_0

    .line 142
    .restart local v2    # "content":Ljava/lang/String;
    .restart local v8    # "createdTime":J
    .restart local v10    # "modifiedTime":J
    .restart local v14    # "res":I
    .restart local v16    # "templateType":Ljava/lang/String;
    .restart local v17    # "unzipFolder":Ljava/io/File;
    :cond_1
    const-wide/16 v18, -0x2

    goto :goto_1

    .line 144
    .end local v2    # "content":Ljava/lang/String;
    .end local v8    # "createdTime":J
    .end local v10    # "modifiedTime":J
    .end local v16    # "templateType":Ljava/lang/String;
    :cond_2
    int-to-long v0, v14

    move-wide/from16 v18, v0

    goto :goto_1
.end method

.method public convertToNewMemo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)J
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "category"    # Ljava/lang/String;
    .param p5, "createdTime"    # J
    .param p7, "modifiedTime"    # J

    .prologue
    const/16 v3, 0x1e

    .line 189
    const-wide/16 v0, -0x1

    .line 190
    .local v0, "memoId":J
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v3, :cond_0

    .line 191
    const/4 v2, 0x0

    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p2

    .end local p2    # "title":Ljava/lang/String;
    check-cast p2, Ljava/lang/String;

    .line 192
    .restart local p2    # "title":Ljava/lang/String;
    :cond_0
    invoke-static {p1, p2, p3}, Lcom/samsung/android/app/memo/MemoDataHandler;->isMemoExists(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 193
    invoke-direct {p0}, Lcom/samsung/android/app/migration/MemoConverter;->createNew()Ljava/lang/String;

    .line 194
    invoke-direct {p0, p2}, Lcom/samsung/android/app/migration/MemoConverter;->setTitle(Ljava/lang/String;)V

    .line 195
    invoke-direct {p0, p3}, Lcom/samsung/android/app/migration/MemoConverter;->appendText(Ljava/lang/String;)V

    .line 196
    invoke-direct {p0, p5, p6, p7, p8}, Lcom/samsung/android/app/migration/MemoConverter;->setDate(JJ)V

    .line 197
    invoke-direct {p0, p4}, Lcom/samsung/android/app/migration/MemoConverter;->setCategory(Ljava/lang/String;)V

    .line 198
    invoke-direct {p0}, Lcom/samsung/android/app/migration/MemoConverter;->saveCurrent()J

    move-result-wide v0

    .line 200
    :cond_1
    return-wide v0
.end method

.method public getLock()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/android/app/migration/MemoConverter;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method public getTMemo1DataQueue()Ljava/util/concurrent/BlockingQueue;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/samsung/android/app/migration/task/TMemo1Data;",
            ">;"
        }
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lcom/samsung/android/app/migration/MemoConverter;->mTMemo1Data:Ljava/util/concurrent/BlockingQueue;

    return-object v0
.end method

.method public openMemo(Landroid/content/Context;J)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "memoId"    # J

    .prologue
    .line 204
    sget-object v2, Lcom/samsung/android/provider/memo/MemoContract;->AUTHORITY_URI:Landroid/net/Uri;

    invoke-static {v2, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 205
    .local v1, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 206
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 207
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 208
    return-void
.end method

.method public parseSNB(Ljava/lang/String;)J
    .locals 8
    .param p1, "snbPath"    # Ljava/lang/String;

    .prologue
    .line 150
    invoke-static {p1}, Lcom/samsung/android/app/migration/utils/Utils;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 152
    .local v4, "title":Ljava/lang/String;
    sget-object v6, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 153
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/samsung/android/app/migration/utils/Utils;->getFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 156
    .local v2, "targetDir":Ljava/lang/String;
    :goto_0
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 157
    .local v5, "unzipFolder":Ljava/io/File;
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/app/migration/Migration;->getPassword()Ljava/lang/String;

    move-result-object v6

    invoke-static {p1, v2, v6}, Lcom/samsung/android/app/memo/util/FileHelper;->unzip(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    int-to-long v0, v6

    .line 158
    .local v0, "resId":J
    const-wide/16 v6, 0x0

    cmp-long v6, v0, v6

    if-nez v6, :cond_0

    .line 159
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v6

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/migration/xml/XMLParser;->parseXMLContent(Ljava/lang/String;)V

    .line 160
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v6

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/migration/xml/XMLParser;->parseXMLSettings(Ljava/lang/String;)V

    .line 161
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/app/migration/xml/XMLParser;->getTemplateType()Ljava/lang/String;

    move-result-object v3

    .line 162
    .local v3, "templateType":Ljava/lang/String;
    const-string v6, "14"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 163
    const-wide/16 v0, -0x2

    .line 165
    .end local v3    # "templateType":Ljava/lang/String;
    :cond_0
    invoke-static {v5}, Lcom/samsung/android/app/memo/util/FileHelper;->deleteFileItem(Ljava/io/File;)V

    .line 166
    return-wide v0

    .line 155
    .end local v0    # "resId":J
    .end local v2    # "targetDir":Ljava/lang/String;
    .end local v5    # "unzipFolder":Ljava/io/File;
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    sget-object v7, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_FOLDER_PATH:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "targetDir":Ljava/lang/String;
    goto :goto_0
.end method
