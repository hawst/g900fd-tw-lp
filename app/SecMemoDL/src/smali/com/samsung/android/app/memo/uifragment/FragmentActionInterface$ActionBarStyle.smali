.class public final enum Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;
.super Ljava/lang/Enum;
.source "FragmentActionInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActionBarStyle"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CATEGORY_VIEW:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

.field public static final enum LIST_VIEW:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    const-string v1, "LIST_VIEW"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;->LIST_VIEW:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    new-instance v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    const-string v1, "CATEGORY_VIEW"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;->CATEGORY_VIEW:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    .line 33
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;->LIST_VIEW:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;->CATEGORY_VIEW:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;->ENUM$VALUES:[Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;->ENUM$VALUES:[Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public isAbHomeAsUpRequired()Z
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;->LIST_VIEW:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
