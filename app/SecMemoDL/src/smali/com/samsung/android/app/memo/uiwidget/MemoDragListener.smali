.class public Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;
.super Ljava/lang/Object;
.source "MemoDragListener.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsForEditScreen:Z

.field private mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

.field private mSession:Lcom/samsung/android/app/memo/Session;

.field private mStart:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mIsForEditScreen:Z

    .line 50
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mContext:Landroid/content/Context;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/memo/uiwidget/RichEditor;Lcom/samsung/android/app/memo/Session;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "richEditor"    # Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    .param p3, "session"    # Lcom/samsung/android/app/memo/Session;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mIsForEditScreen:Z

    .line 54
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mContext:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 56
    iput-object p3, p0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mSession:Lcom/samsung/android/app/memo/Session;

    .line 57
    return-void
.end method

.method private createNewIntent(ZLjava/lang/StringBuilder;Ljava/util/ArrayList;)Z
    .locals 4
    .param p1, "result"    # Z
    .param p2, "content"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p3, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v3, 0x1

    .line 185
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 186
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 187
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mContext:Landroid/content/Context;

    const-string v2, "com.samsung.android.app.memo.Main"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 188
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    const-string v1, "text/html"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 191
    const-string v1, "isFromDragListener"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 192
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 193
    const/4 p1, 0x1

    .line 204
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    :goto_0
    return p1

    .line 194
    :cond_1
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 195
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 196
    .restart local v0    # "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mContext:Landroid/content/Context;

    const-string v2, "com.samsung.android.app.memo.Main"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 200
    const-string v1, "isFromDragListener"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 201
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 202
    const/4 p1, 0x1

    goto :goto_0
.end method

.method private filterImageUris(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 229
    .local p1, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 230
    .local v1, "filteredList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 244
    return-object v1

    .line 230
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 231
    .local v3, "uri":Landroid/net/Uri;
    const/4 v2, 0x0

    .line 232
    .local v2, "mime_type":Ljava/lang/String;
    const-string v5, "content"

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "image"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 233
    invoke-static {v3}, Lcom/samsung/android/app/memo/util/ImageUtil;->lookupImageInfo(Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v0

    .line 234
    .local v0, "bundle":Landroid/os/Bundle;
    sget-object v5, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_MIMETYPE:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 240
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_1
    if-eqz v2, :cond_0

    const-string v5, "image"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 241
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 235
    :cond_2
    const-string v5, "file"

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 236
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/net/URLConnection;->guessContentTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private updateContent(Landroid/content/ClipData;Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "clipData"    # Landroid/content/ClipData;
    .param p2, "content"    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ClipData;",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 208
    .local p3, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p1}, Landroid/content/ClipData;->getItemCount()I

    move-result v0

    .line 209
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 226
    return-void

    .line 210
    :cond_0
    invoke-virtual {p1, v1}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v2

    .line 211
    .local v2, "item":Landroid/content/ClipData$Item;
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getHtmlText()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 212
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getHtmlText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 209
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 213
    :cond_2
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 214
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 215
    :cond_3
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 216
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/app/memo/util/HtmlUtil;->stripHtml(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 217
    :cond_4
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 218
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 219
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    const-string v5, "image"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 220
    invoke-virtual {v2}, Landroid/content/ClipData$Item;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "android.intent.extra.STREAM"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 221
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_1

    .line 222
    invoke-virtual {p3, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 23
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 62
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v6

    .line 63
    .local v6, "clipData1":Landroid/content/ClipData;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v20

    const-string v21, "text/x-vcard"

    invoke-virtual/range {v20 .. v21}, Landroid/content/ClipDescription;->hasMimeType(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 64
    const/4 v11, 0x0

    .line 180
    :cond_0
    :goto_0
    return v11

    .line 69
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v4

    .line 70
    .local v4, "action":I
    const/4 v11, 0x0

    .line 73
    .local v11, "result":Z
    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 82
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v20, v0

    if-eqz v20, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isEditMode()Z

    move-result v20

    if-nez v20, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mSession:Lcom/samsung/android/app/memo/Session;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/memo/Session;->isDraft()Z

    move-result v20

    if-eqz v20, :cond_6

    .line 83
    :cond_2
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mIsForEditScreen:Z

    .line 84
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Landroid/text/Editable;->length()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mStart:I

    .line 86
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isFocused()Z

    move-result v20

    if-eqz v20, :cond_3

    .line 87
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v15

    .line 88
    .local v15, "selStart":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionEnd()I

    move-result v14

    .line 89
    .local v14, "selEnd":I
    const/16 v20, 0x0

    invoke-static {v15, v14}, Ljava/lang/Math;->min(II)I

    move-result v21

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->max(II)I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mStart:I

    .line 95
    .end local v14    # "selEnd":I
    .end local v15    # "selStart":I
    :cond_3
    :goto_1
    const/4 v11, 0x1

    goto :goto_0

    .line 75
    :pswitch_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mIsForEditScreen:Z

    move/from16 v20, v0

    if-eqz v20, :cond_5

    .line 76
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v20, v0

    if-eqz v20, :cond_4

    .line 77
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->recycleDragImange()V

    .line 78
    :cond_4
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mIsForEditScreen:Z

    .line 80
    :cond_5
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 93
    :cond_6
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mIsForEditScreen:Z

    goto :goto_1

    .line 97
    :pswitch_3
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v5

    .line 98
    .local v5, "clipData":Landroid/content/ClipData;
    if-eqz v5, :cond_0

    .line 99
    invoke-virtual {v5}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v20

    if-eqz v20, :cond_7

    const-string v20, "MultiWindow_DragDrop_Memo"

    .line 100
    invoke-virtual {v5}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 101
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 103
    :cond_7
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .local v7, "content":Ljava/lang/StringBuilder;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    .local v12, "richEditorData":Ljava/lang/StringBuilder;
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .local v19, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 107
    .local v8, "filteredUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v5, v7, v1}, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->updateContent(Landroid/content/ClipData;Ljava/lang/StringBuilder;Ljava/util/ArrayList;)V

    .line 108
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_9

    .line 109
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->filterImageUris(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    .line 110
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v20

    if-eqz v20, :cond_8

    .line 111
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f0b00ae

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 112
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 114
    :cond_8
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_9

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v20

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_9

    .line 115
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/app/memo/util/ImageUtil;->containVideoUri(Ljava/util/ArrayList;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 116
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f0b00af

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 119
    :cond_9
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_b

    .line 121
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v16

    .line 122
    .local v16, "size":I
    move/from16 v9, v16

    .local v9, "i":I
    :goto_2
    const/16 v20, 0xb

    move/from16 v0, v20

    if-gt v9, v0, :cond_e

    .line 129
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 130
    .local v13, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_a
    :goto_3
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-nez v21, :cond_f

    .line 136
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 137
    .end local v9    # "i":I
    .end local v13    # "sb":Ljava/lang/StringBuilder;
    .end local v16    # "size":I
    :cond_b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_c

    .line 143
    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 146
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mIsForEditScreen:Z

    move/from16 v20, v0

    if-eqz v20, :cond_14

    .line 147
    if-eqz v12, :cond_d

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_10

    .line 148
    :cond_d
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 127
    .restart local v9    # "i":I
    .restart local v16    # "size":I
    :cond_e
    add-int/lit8 v20, v9, -0x1

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 122
    add-int/lit8 v9, v9, -0x1

    goto :goto_2

    .line 130
    .restart local v13    # "sb":Ljava/lang/StringBuilder;
    :cond_f
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/net/Uri;

    .line 131
    .local v18, "uri":Landroid/net/Uri;
    if-eqz v18, :cond_a

    .line 132
    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "<img src=\'"

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    .line 133
    const-string v22, "\'></img><br>"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 132
    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 149
    .end local v9    # "i":I
    .end local v13    # "sb":Ljava/lang/StringBuilder;
    .end local v16    # "size":I
    .end local v18    # "uri":Landroid/net/Uri;
    :cond_10
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const-string v21, "<p>"

    const-string v22, ""

    invoke-virtual/range {v20 .. v22}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 150
    .local v17, "st":Ljava/lang/String;
    const-string v20, "</p>"

    const-string v21, "<br>"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 151
    const-string v20, "<img src="

    const-string v21, "<br><img src="

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 152
    const-string v20, "<br>"

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    .line 153
    .local v10, "ind":I
    const/16 v20, 0x0

    const/16 v21, -0x1

    move/from16 v0, v21

    if-ne v10, v0, :cond_11

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v10

    .end local v10    # "ind":I
    :cond_11
    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v20, v0

    if-eqz v20, :cond_12

    .line 155
    const-string v20, "MultiWindow_DragDrop_Memo"

    .line 156
    invoke-virtual {v5}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v21

    .line 155
    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    .line 156
    if-nez v20, :cond_13

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v20, v0

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v21

    .line 159
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v22

    .line 157
    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v12, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->insertHtmlText(Ljava/lang/CharSequence;II)V

    .line 165
    :cond_12
    :goto_4
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 161
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v20, v0

    .line 162
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mStart:I

    move/from16 v22, v0

    .line 161
    move-object/from16 v0, v20

    move-object/from16 v1, v17

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->replaceHtmlText(Ljava/lang/CharSequence;II)V

    goto :goto_4

    .line 167
    .end local v17    # "st":Ljava/lang/String;
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->mIsForEditScreen:Z

    move/from16 v20, v0

    if-nez v20, :cond_0

    .line 168
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v20

    const v21, 0x186a1

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_15

    .line 169
    new-instance v20, Ljava/lang/StringBuilder;

    const/16 v21, 0x0

    const v22, 0x186a0

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v11, v1, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->createNewIntent(ZLjava/lang/StringBuilder;Ljava/util/ArrayList;)Z

    move-result v11

    goto/16 :goto_0

    .line 171
    :cond_15
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v12, v8}, Lcom/samsung/android/app/memo/uiwidget/MemoDragListener;->createNewIntent(ZLjava/lang/StringBuilder;Ljava/util/ArrayList;)Z

    move-result v11

    .line 174
    goto/16 :goto_0

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
