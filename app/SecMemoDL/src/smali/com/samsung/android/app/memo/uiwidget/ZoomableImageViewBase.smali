.class public abstract Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;
.super Landroid/widget/ImageView;
.source "ZoomableImageViewBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;,
        Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnDrawableChangeListener;,
        Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnLayoutChangeListener;
    }
.end annotation


# static fields
.field public static final LOG_TAG:Ljava/lang/String; = "ImageViewTouchBase"

.field public static final ZOOM_INVALID:F = -1.0f


# instance fields
.field protected final DEFAULT_ANIMATION_DURATION:I

.field protected mBaseMatrix:Landroid/graphics/Matrix;

.field private mBitmapChanged:Z

.field protected mBitmapRect:Landroid/graphics/RectF;

.field private mCenter:Landroid/graphics/PointF;

.field protected mCenterRect:Landroid/graphics/RectF;

.field protected final mDisplayMatrix:Landroid/graphics/Matrix;

.field private mDrawableChangeListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnDrawableChangeListener;

.field protected mHandler:Landroid/os/Handler;

.field protected mLayoutRunnable:Ljava/lang/Runnable;

.field protected final mMatrixValues:[F

.field private mMaxZoom:F

.field private mMaxZoomDefined:Z

.field private mMinZoom:F

.field private mMinZoomDefined:Z

.field protected mNextMatrix:Landroid/graphics/Matrix;

.field private mOnLayoutChangeListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnLayoutChangeListener;

.field protected mScaleType:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

.field private mScaleTypeChanged:Z

.field protected mScrollRect:Landroid/graphics/RectF;

.field protected mSuppMatrix:Landroid/graphics/Matrix;

.field private mThisHeight:I

.field private mThisWidth:I

.field protected mUserScaled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 119
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 120
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, -0x1

    const/high16 v1, -0x40800000    # -1.0f

    .line 123
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBaseMatrix:Landroid/graphics/Matrix;

    .line 67
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mSuppMatrix:Landroid/graphics/Matrix;

    .line 71
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mHandler:Landroid/os/Handler;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mLayoutRunnable:Ljava/lang/Runnable;

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mUserScaled:Z

    .line 77
    iput v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMaxZoom:F

    .line 79
    iput v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMinZoom:F

    .line 86
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    .line 88
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMatrixValues:[F

    .line 90
    iput v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisWidth:I

    .line 92
    iput v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisHeight:I

    .line 94
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mCenter:Landroid/graphics/PointF;

    .line 96
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;->NONE:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleType:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    .line 102
    const/16 v0, 0xc8

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->DEFAULT_ANIMATION_DURATION:I

    .line 104
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapRect:Landroid/graphics/RectF;

    .line 106
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mCenterRect:Landroid/graphics/RectF;

    .line 108
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScrollRect:Landroid/graphics/RectF;

    .line 125
    return-void
.end method

.method private setImageMatrixForZoom(ZIIIIIILandroid/graphics/drawable/Drawable;)V
    .locals 12
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "deltaX"    # I
    .param p7, "deltaY"    # I
    .param p8, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 238
    const/high16 v7, 0x3f800000    # 1.0f

    .line 241
    .local v7, "scale":F
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleType:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getDefaultScale(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;)F

    move-result v3

    .line 242
    .local v3, "old_default_scale":F
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v4

    .line 243
    .local v4, "old_matrix_scale":F
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale()F

    move-result v6

    .line 244
    .local v6, "old_scale":F
    const/high16 v8, 0x3f800000    # 1.0f

    const/high16 v9, 0x3f800000    # 1.0f

    div-float/2addr v9, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 246
    .local v5, "old_min_scale":F
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBaseMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p8

    invoke-virtual {p0, v0, v8}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getProperBaseMatrix(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;)V

    .line 248
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v2

    .line 251
    .local v2, "new_matrix_scale":F
    iget-boolean v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapChanged:Z

    if-nez v8, :cond_0

    iget-boolean v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleTypeChanged:Z

    if-eqz v8, :cond_a

    .line 253
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mNextMatrix:Landroid/graphics/Matrix;

    if-eqz v8, :cond_9

    .line 254
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mSuppMatrix:Landroid/graphics/Matrix;

    iget-object v9, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mNextMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v8, v9}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 255
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mNextMatrix:Landroid/graphics/Matrix;

    .line 256
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale()F

    move-result v7

    .line 262
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 264
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale()F

    move-result v8

    cmpl-float v8, v7, v8

    if-eqz v8, :cond_1

    .line 265
    invoke-virtual {p0, v7}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->zoomTo(F)V

    .line 294
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getMaxScale()F

    move-result v8

    cmpl-float v8, v7, v8

    if-gtz v8, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getMinScale()F

    move-result v8

    cmpg-float v8, v7, v8

    if-gez v8, :cond_3

    .line 297
    :cond_2
    invoke-virtual {p0, v7}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->zoomTo(F)V

    .line 300
    :cond_3
    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-virtual {p0, v8, v9}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->center(ZZ)V

    .line 302
    iget-boolean v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapChanged:Z

    if-eqz v8, :cond_4

    .line 303
    move-object/from16 v0, p8

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->onDrawableChanged(Landroid/graphics/drawable/Drawable;)V

    .line 304
    :cond_4
    if-nez p1, :cond_5

    iget-boolean v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapChanged:Z

    if-nez v8, :cond_5

    iget-boolean v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleTypeChanged:Z

    if-eqz v8, :cond_6

    .line 305
    :cond_5
    move/from16 v0, p4

    move/from16 v1, p5

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->onLayoutChanged(IIII)V

    .line 307
    :cond_6
    iget-boolean v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleTypeChanged:Z

    if-eqz v8, :cond_7

    .line 308
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleTypeChanged:Z

    .line 309
    :cond_7
    iget-boolean v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapChanged:Z

    if-eqz v8, :cond_8

    .line 310
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapChanged:Z

    .line 311
    :cond_8
    return-void

    .line 258
    :cond_9
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v8}, Landroid/graphics/Matrix;->reset()V

    .line 259
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleType:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getDefaultScale(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;)F

    move-result v7

    goto :goto_0

    .line 268
    :cond_a
    if-eqz p1, :cond_1

    .line 272
    iget-boolean v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMinZoomDefined:Z

    if-nez v8, :cond_b

    .line 273
    const/high16 v8, -0x40800000    # -1.0f

    iput v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMinZoom:F

    .line 274
    :cond_b
    iget-boolean v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMaxZoomDefined:Z

    if-nez v8, :cond_c

    .line 275
    const/high16 v8, -0x40800000    # -1.0f

    iput v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMaxZoom:F

    .line 277
    :cond_c
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 278
    move/from16 v0, p6

    neg-int v8, v0

    int-to-float v8, v8

    move/from16 v0, p7

    neg-int v9, v0

    int-to-float v9, v9

    invoke-virtual {p0, v8, v9}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->postTranslate(FF)V

    .line 280
    iget-boolean v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mUserScaled:Z

    if-eqz v8, :cond_d

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale()F

    move-result v8

    cmpl-float v8, v8, v3

    if-nez v8, :cond_e

    .line 281
    :cond_d
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleType:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getDefaultScale(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;)F

    move-result v7

    .line 282
    invoke-virtual {p0, v7}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->zoomTo(F)V

    goto/16 :goto_1

    .line 284
    :cond_e
    sub-float v8, v6, v5

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    float-to-double v8, v8

    const-wide v10, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v8, v8, v10

    if-lez v8, :cond_f

    .line 285
    div-float v8, v4, v2

    mul-float v7, v8, v6

    .line 287
    :cond_f
    invoke-virtual {p0, v7}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->zoomTo(F)V

    goto/16 :goto_1
.end method


# virtual methods
.method protected _setImageDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;FF)V
    .locals 7
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "initial_matrix"    # Landroid/graphics/Matrix;
    .param p3, "min_zoom"    # F
    .param p4, "max_zoom"    # F

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/high16 v3, -0x40800000    # -1.0f

    .line 389
    if-eqz p1, :cond_4

    .line 391
    const-string v0, "ImageViewTouchBase"

    .line 392
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 391
    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 399
    :goto_0
    cmpl-float v0, p3, v3

    if-eqz v0, :cond_5

    cmpl-float v0, p4, v3

    if-eqz v0, :cond_5

    .line 400
    invoke-static {p3, p4}, Ljava/lang/Math;->min(FF)F

    move-result p3

    .line 401
    invoke-static {p3, p4}, Ljava/lang/Math;->max(FF)F

    move-result p4

    .line 403
    iput p3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMinZoom:F

    .line 404
    iput p4, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMaxZoom:F

    .line 406
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMinZoomDefined:Z

    .line 407
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMaxZoomDefined:Z

    .line 409
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleType:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;->FIT_TO_SCREEN:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleType:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;->FIT_IF_BIGGER:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    if-ne v0, v1, :cond_2

    .line 411
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMinZoom:F

    cmpl-float v0, v0, v6

    if-ltz v0, :cond_1

    .line 412
    iput-boolean v5, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMinZoomDefined:Z

    .line 413
    iput v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMinZoom:F

    .line 416
    :cond_1
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMaxZoom:F

    cmpg-float v0, v0, v6

    if-gtz v0, :cond_2

    .line 417
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMaxZoomDefined:Z

    .line 418
    iput v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMaxZoom:F

    .line 429
    :cond_2
    :goto_1
    if-eqz p2, :cond_3

    .line 430
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0, p2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mNextMatrix:Landroid/graphics/Matrix;

    .line 433
    :cond_3
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapChanged:Z

    .line 434
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->requestLayout()V

    .line 435
    return-void

    .line 395
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 396
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 422
    :cond_5
    iput v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMinZoom:F

    .line 423
    iput v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMaxZoom:F

    .line 425
    iput-boolean v5, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMinZoomDefined:Z

    .line 426
    iput-boolean v5, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMaxZoomDefined:Z

    goto :goto_1
.end method

.method protected center(ZZ)V
    .locals 4
    .param p1, "horizontal"    # Z
    .param p2, "vertical"    # Z

    .prologue
    const/4 v3, 0x0

    .line 629
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 630
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_1

    .line 639
    :cond_0
    :goto_0
    return-void

    .line 633
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v2, p1, p2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getCenter(Landroid/graphics/Matrix;ZZ)Landroid/graphics/RectF;

    move-result-object v1

    .line 635
    .local v1, "rect":Landroid/graphics/RectF;
    iget v2, v1, Landroid/graphics/RectF;->left:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget v2, v1, Landroid/graphics/RectF;->top:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 637
    :cond_2
    iget v2, v1, Landroid/graphics/RectF;->left:F

    iget v3, v1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->postTranslate(FF)V

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 153
    return-void
.end method

.method protected computeMaxZoom()F
    .locals 6

    .prologue
    .line 458
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 460
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_0

    .line 461
    const/high16 v3, 0x3f800000    # 1.0f

    .line 468
    :goto_0
    return v3

    .line 464
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisWidth:I

    int-to-float v5, v5

    div-float v2, v4, v5

    .line 465
    .local v2, "fw":F
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisHeight:I

    int-to-float v5, v5

    div-float v1, v4, v5

    .line 466
    .local v1, "fh":F
    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    mul-float v3, v4, v5

    .line 468
    .local v3, "scale":F
    goto :goto_0
.end method

.method protected computeMinZoom()F
    .locals 4

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 472
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 474
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_0

    move v1, v2

    .line 481
    :goto_0
    return v1

    .line 478
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v1

    .line 479
    .local v1, "scale":F
    div-float v3, v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 481
    goto :goto_0
.end method

.method public dispose()V
    .locals 0

    .prologue
    .line 820
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->clear()V

    .line 821
    return-void
.end method

.method public easeIn(DDDD)D
    .locals 3
    .param p1, "time"    # D
    .param p3, "start"    # D
    .param p5, "end"    # D
    .param p7, "duration"    # D

    .prologue
    .line 828
    div-double/2addr p1, p7

    mul-double v0, p5, p1

    mul-double/2addr v0, p1

    mul-double/2addr v0, p1

    add-double/2addr v0, p3

    return-wide v0
.end method

.method public easeInOut(DDDD)D
    .locals 7
    .param p1, "time"    # D
    .param p3, "start"    # D
    .param p5, "end"    # D
    .param p7, "duration"    # D

    .prologue
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 832
    div-double v0, p7, v4

    div-double/2addr p1, v0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 833
    div-double v0, p5, v4

    mul-double/2addr v0, p1

    mul-double/2addr v0, p1

    mul-double/2addr v0, p1

    add-double/2addr v0, p3

    .line 834
    :goto_0
    return-wide v0

    :cond_0
    div-double v0, p5, v4

    sub-double/2addr p1, v4

    mul-double v2, p1, p1

    mul-double/2addr v2, p1

    add-double/2addr v2, v4

    mul-double/2addr v0, v2

    add-double/2addr v0, p3

    goto :goto_0
.end method

.method public easeOut(DDDD)D
    .locals 5
    .param p1, "time"    # D
    .param p3, "start"    # D
    .param p5, "end"    # D
    .param p7, "duration"    # D

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 824
    div-double v0, p1, p7

    sub-double p1, v0, v2

    mul-double v0, p1, p1

    mul-double/2addr v0, p1

    add-double/2addr v0, v2

    mul-double/2addr v0, p5

    add-double/2addr v0, p3

    return-wide v0
.end method

.method protected fireOnDrawableChangeListener(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 448
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mDrawableChangeListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnDrawableChangeListener;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mDrawableChangeListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnDrawableChangeListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnDrawableChangeListener;->onDrawableChanged(Landroid/graphics/drawable/Drawable;)V

    .line 451
    :cond_0
    return-void
.end method

.method protected fireOnLayoutChangeListener(IIII)V
    .locals 6
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 442
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mOnLayoutChangeListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnLayoutChangeListener;

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mOnLayoutChangeListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnLayoutChangeListener;

    const/4 v1, 0x1

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnLayoutChangeListener;->onLayoutChanged(ZIIII)V

    .line 445
    :cond_0
    return-void
.end method

.method public getBaseScale()F
    .locals 1

    .prologue
    .line 625
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v0

    return v0
.end method

.method public getBitmapRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getBitmapRect(Landroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method protected getBitmapRect(Landroid/graphics/Matrix;)Landroid/graphics/RectF;
    .locals 6
    .param p1, "supportMatrix"    # Landroid/graphics/Matrix;

    .prologue
    const/4 v5, 0x0

    .line 601
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 603
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_0

    .line 604
    const/4 v2, 0x0

    .line 608
    :goto_0
    return-object v2

    .line 605
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getImageViewMatrix(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v1

    .line 606
    .local v1, "m":Landroid/graphics/Matrix;
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 607
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 608
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapRect:Landroid/graphics/RectF;

    goto :goto_0
.end method

.method protected getCenter()Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mCenter:Landroid/graphics/PointF;

    return-object v0
.end method

.method protected getCenter(Landroid/graphics/Matrix;ZZ)Landroid/graphics/RectF;
    .locals 12
    .param p1, "supportMatrix"    # Landroid/graphics/Matrix;
    .param p2, "horizontal"    # Z
    .param p3, "vertical"    # Z

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x0

    .line 642
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 644
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v2, :cond_0

    .line 645
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8, v10, v10, v10, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 673
    :goto_0
    return-object v8

    .line 647
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mCenterRect:Landroid/graphics/RectF;

    invoke-virtual {v8, v10, v10, v10, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 648
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getBitmapRect(Landroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object v4

    .line 649
    .local v4, "rect":Landroid/graphics/RectF;
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v3

    .line 650
    .local v3, "height":F
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    .line 651
    .local v7, "width":F
    const/4 v0, 0x0

    .local v0, "deltaX":F
    const/4 v1, 0x0

    .line 652
    .local v1, "deltaY":F
    if-eqz p3, :cond_1

    .line 653
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisHeight:I

    .line 654
    .local v5, "viewHeight":I
    int-to-float v8, v5

    cmpg-float v8, v3, v8

    if-gez v8, :cond_3

    .line 655
    int-to-float v8, v5

    sub-float/2addr v8, v3

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->top:F

    sub-float v1, v8, v9

    .line 662
    .end local v5    # "viewHeight":I
    :cond_1
    :goto_1
    if-eqz p2, :cond_2

    .line 663
    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisWidth:I

    .line 664
    .local v6, "viewWidth":I
    int-to-float v8, v6

    cmpg-float v8, v7, v8

    if-gez v8, :cond_5

    .line 665
    int-to-float v8, v6

    sub-float/2addr v8, v7

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->left:F

    sub-float v0, v8, v9

    .line 672
    .end local v6    # "viewWidth":I
    :cond_2
    :goto_2
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mCenterRect:Landroid/graphics/RectF;

    invoke-virtual {v8, v0, v1, v10, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 673
    iget-object v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mCenterRect:Landroid/graphics/RectF;

    goto :goto_0

    .line 656
    .restart local v5    # "viewHeight":I
    :cond_3
    iget v8, v4, Landroid/graphics/RectF;->top:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_4

    .line 657
    iget v8, v4, Landroid/graphics/RectF;->top:F

    neg-float v1, v8

    .line 658
    goto :goto_1

    :cond_4
    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v9, v5

    cmpg-float v8, v8, v9

    if-gez v8, :cond_1

    .line 659
    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisHeight:I

    int-to-float v8, v8

    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    sub-float v1, v8, v9

    goto :goto_1

    .line 666
    .end local v5    # "viewHeight":I
    .restart local v6    # "viewWidth":I
    :cond_5
    iget v8, v4, Landroid/graphics/RectF;->left:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_6

    .line 667
    iget v8, v4, Landroid/graphics/RectF;->left:F

    neg-float v0, v8

    .line 668
    goto :goto_2

    :cond_6
    iget v8, v4, Landroid/graphics/RectF;->right:F

    int-to-float v9, v6

    cmpg-float v8, v8, v9

    if-gez v8, :cond_2

    .line 669
    int-to-float v8, v6

    iget v9, v4, Landroid/graphics/RectF;->right:F

    sub-float v0, v8, v9

    goto :goto_2
.end method

.method protected getDefaultScale(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;)F
    .locals 2
    .param p1, "type"    # Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 334
    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;->FIT_TO_SCREEN:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    if-ne p1, v1, :cond_0

    .line 342
    :goto_0
    return v0

    .line 337
    :cond_0
    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;->FIT_IF_BIGGER:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    if-ne p1, v1, :cond_1

    .line 339
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v1

    div-float v1, v0, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0

    .line 342
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public getDisplayMatrix()Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 528
    new-instance v0, Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-direct {v0, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    return-object v0
.end method

.method public getDisplayType()Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleType:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    return-object v0
.end method

.method public getImageViewMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getImageViewMatrix(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public getImageViewMatrix(Landroid/graphics/Matrix;)Landroid/graphics/Matrix;
    .locals 2
    .param p1, "supportMatrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 503
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 504
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 505
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getMaxScale()F
    .locals 2

    .prologue
    .line 485
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMaxZoom:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 486
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->computeMaxZoom()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMaxZoom:F

    .line 488
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMaxZoom:F

    return v0
.end method

.method public getMinScale()F
    .locals 2

    .prologue
    .line 492
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMinZoom:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 493
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->computeMinZoom()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMinZoom:F

    .line 495
    :cond_0
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMinZoom:F

    return v0
.end method

.method protected getProperBaseMatrix(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;)V
    .locals 11
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    .line 532
    iget v9, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisWidth:I

    int-to-float v6, v9

    .line 533
    .local v6, "viewWidth":F
    iget v9, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisHeight:I

    int-to-float v5, v9

    .line 535
    .local v5, "viewHeight":F
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v9

    int-to-float v7, v9

    .line 536
    .local v7, "w":F
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v9

    int-to-float v0, v9

    .line 538
    .local v0, "h":F
    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    .line 541
    div-float v8, v6, v7

    .line 542
    .local v8, "widthScale":F
    div-float v1, v5, v0

    .line 543
    .local v1, "heightScale":F
    invoke-static {v8, v1}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 544
    .local v2, "scale":F
    invoke-virtual {p2, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 546
    mul-float v9, v7, v2

    sub-float v9, v6, v9

    div-float v4, v9, v10

    .line 547
    .local v4, "tw":F
    mul-float v9, v0, v2

    sub-float v9, v5, v9

    div-float v3, v9, v10

    .line 548
    .local v3, "th":F
    invoke-virtual {p2, v4, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 561
    return-void
.end method

.method protected getProperBaseMatrix2(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;)V
    .locals 10
    .param p1, "bitmap"    # Landroid/graphics/drawable/Drawable;
    .param p2, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    .line 565
    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisWidth:I

    int-to-float v4, v7

    .line 566
    .local v4, "viewWidth":F
    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisHeight:I

    int-to-float v3, v7

    .line 568
    .local v3, "viewHeight":F
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    int-to-float v5, v7

    .line 569
    .local v5, "w":F
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    int-to-float v0, v7

    .line 571
    .local v0, "h":F
    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    .line 573
    div-float v6, v4, v5

    .line 574
    .local v6, "widthScale":F
    div-float v1, v3, v0

    .line 576
    .local v1, "heightScale":F
    invoke-static {v6, v1}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 578
    .local v2, "scale":F
    invoke-virtual {p2, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 579
    mul-float v7, v5, v2

    sub-float v7, v4, v7

    div-float/2addr v7, v9

    mul-float v8, v0, v2

    sub-float v8, v3, v8

    div-float/2addr v8, v9

    invoke-virtual {p2, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 580
    return-void
.end method

.method public getRotation()F
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "Override"
        }
    .end annotation

    .prologue
    .line 617
    const/4 v0, 0x0

    return v0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 621
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v0

    return v0
.end method

.method protected getScale(Landroid/graphics/Matrix;)F
    .locals 1
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 612
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getValue(Landroid/graphics/Matrix;I)F

    move-result v0

    return v0
.end method

.method protected getValue(Landroid/graphics/Matrix;I)F
    .locals 1
    .param p1, "matrix"    # Landroid/graphics/Matrix;
    .param p2, "whichValue"    # I

    .prologue
    .line 583
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMatrixValues:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 584
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMatrixValues:[F

    aget v0, v0, p2

    return v0
.end method

.method protected onDrawableChanged(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 438
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->fireOnDrawableChangeListener(Landroid/graphics/drawable/Drawable;)V

    .line 439
    return-void
.end method

.method protected onImageMatrixChanged()V
    .locals 0

    .prologue
    .line 525
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 17
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 185
    const-string v5, "ImageViewTouchBase"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "onLayout: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", bitmapChanged: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapChanged:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 186
    const-string v7, ", scaleChanged: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleTypeChanged:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 185
    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 190
    const/4 v11, 0x0

    .line 191
    .local v11, "deltaX":I
    const/4 v12, 0x0

    .line 193
    .local v12, "deltaY":I
    if-eqz p1, :cond_0

    .line 194
    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisWidth:I

    .line 195
    .local v15, "oldw":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisHeight:I

    .line 197
    .local v14, "oldh":I
    sub-int v5, p4, p2

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisWidth:I

    .line 198
    sub-int v5, p5, p3

    move-object/from16 v0, p0

    iput v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisHeight:I

    .line 200
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisWidth:I

    sub-int v11, v5, v15

    .line 201
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisHeight:I

    sub-int v12, v5, v14

    .line 204
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mCenter:Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisWidth:I

    int-to-float v6, v6

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 205
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mCenter:Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisHeight:I

    int-to-float v6, v6

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    iput v6, v5, Landroid/graphics/PointF;->y:F

    .line 208
    .end local v14    # "oldh":I
    .end local v15    # "oldw":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mLayoutRunnable:Ljava/lang/Runnable;

    move-object/from16 v16, v0

    .line 210
    .local v16, "r":Ljava/lang/Runnable;
    if-eqz v16, :cond_1

    .line 211
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mLayoutRunnable:Ljava/lang/Runnable;

    .line 212
    invoke-interface/range {v16 .. v16}, Ljava/lang/Runnable;->run()V

    .line 215
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 217
    .local v13, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v13, :cond_4

    .line 218
    if-nez p1, :cond_2

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleTypeChanged:Z

    if-nez v5, :cond_2

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapChanged:Z

    if-eqz v5, :cond_3

    :cond_2
    move-object/from16 v5, p0

    move/from16 v6, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    .line 219
    invoke-direct/range {v5 .. v13}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->setImageMatrixForZoom(ZIIIIIILandroid/graphics/drawable/Drawable;)V

    .line 234
    :cond_3
    :goto_0
    return-void

    .line 223
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapChanged:Z

    if-eqz v5, :cond_5

    .line 224
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->onDrawableChanged(Landroid/graphics/drawable/Drawable;)V

    .line 225
    :cond_5
    if-nez p1, :cond_6

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapChanged:Z

    if-nez v5, :cond_6

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleTypeChanged:Z

    if-eqz v5, :cond_7

    .line 226
    :cond_6
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->onLayoutChanged(IIII)V

    .line 228
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapChanged:Z

    if-eqz v5, :cond_8

    .line 229
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapChanged:Z

    .line 230
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleTypeChanged:Z

    if-eqz v5, :cond_3

    .line 231
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleTypeChanged:Z

    goto :goto_0
.end method

.method protected onLayoutChanged(IIII)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 454
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->fireOnLayoutChangeListener(IIII)V

    .line 455
    return-void
.end method

.method protected onZoom(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 720
    return-void
.end method

.method protected onZoomAnimationCompleted(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 723
    return-void
.end method

.method protected panBy(DD)V
    .locals 7
    .param p1, "dx"    # D
    .param p3, "dy"    # D

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 730
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getBitmapRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 731
    .local v0, "rect":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScrollRect:Landroid/graphics/RectF;

    double-to-float v2, p1

    double-to-float v3, p3

    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 732
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScrollRect:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->updateRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 733
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScrollRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScrollRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->postTranslate(FF)V

    .line 734
    invoke-virtual {p0, v5, v5}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->center(ZZ)V

    .line 735
    return-void
.end method

.method protected postScale(FFF)V
    .locals 1
    .param p1, "scale"    # F
    .param p2, "centerX"    # F
    .param p3, "centerY"    # F

    .prologue
    .line 684
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 685
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 686
    return-void
.end method

.method protected postTranslate(FF)V
    .locals 2
    .param p1, "deltaX"    # F
    .param p2, "deltaY"    # F

    .prologue
    const/4 v1, 0x0

    .line 677
    cmpl-float v0, p1, v1

    if-nez v0, :cond_0

    cmpl-float v0, p2, v1

    if-eqz v0, :cond_1

    .line 678
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 679
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 681
    :cond_1
    return-void
.end method

.method public printMatrix(Landroid/graphics/Matrix;)V
    .locals 7
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 588
    const/4 v4, 0x0

    invoke-virtual {p0, p1, v4}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getValue(Landroid/graphics/Matrix;I)F

    move-result v0

    .line 589
    .local v0, "scalex":F
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getValue(Landroid/graphics/Matrix;I)F

    move-result v1

    .line 590
    .local v1, "scaley":F
    const/4 v4, 0x2

    invoke-virtual {p0, p1, v4}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getValue(Landroid/graphics/Matrix;I)F

    move-result v2

    .line 591
    .local v2, "tx":F
    const/4 v4, 0x5

    invoke-virtual {p0, p1, v4}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getValue(Landroid/graphics/Matrix;I)F

    move-result v3

    .line 592
    .local v3, "ty":F
    const-string v4, "ImageViewTouchBase"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "matrix: { x: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", y: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", scalex: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", scaley: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 593
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " }"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 592
    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    return-void
.end method

.method public resetDisplay()V
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mBitmapChanged:Z

    .line 315
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->requestLayout()V

    .line 316
    return-void
.end method

.method public resetMatrix()V
    .locals 4

    .prologue
    .line 319
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mSuppMatrix:Landroid/graphics/Matrix;

    .line 321
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleType:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getDefaultScale(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;)F

    move-result v0

    .line 322
    .local v0, "scale":F
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 324
    const-string v1, "ImageViewTouchBase"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "default scale: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", scale: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale()F

    move-result v1

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 327
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->zoomTo(F)V

    .line 330
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->postInvalidate()V

    .line 331
    return-void
.end method

.method public scrollBy(FF)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 726
    float-to-double v0, p1

    float-to-double v2, p2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->panBy(DD)V

    .line 727
    return-void
.end method

.method protected scrollBy(FFD)V
    .locals 11
    .param p1, "distanceX"    # F
    .param p2, "distanceY"    # F
    .param p3, "durationMs"    # D

    .prologue
    .line 756
    float-to-double v6, p1

    .line 757
    .local v6, "dx":D
    float-to-double v8, p2

    .line 758
    .local v8, "dy":D
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 759
    .local v4, "startTime":J
    iget-object v10, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;

    move-object v1, p0

    move-wide v2, p3

    invoke-direct/range {v0 .. v9}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$2;-><init>(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;DJDD)V

    invoke-virtual {v10, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 783
    return-void
.end method

.method public setDisplayType(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;)V
    .locals 3
    .param p1, "type"    # Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleType:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    if-eq p1, v0, :cond_0

    .line 160
    const-string v0, "ImageViewTouchBase"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setDisplayType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mUserScaled:Z

    .line 162
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleType:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$DisplayType;

    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mScaleTypeChanged:Z

    .line 164
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->requestLayout()V

    .line 166
    :cond_0
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 353
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v1, v1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->setImageBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;FF)V

    .line 354
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;FF)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "matrix"    # Landroid/graphics/Matrix;
    .param p3, "min_zoom"    # F
    .param p4, "max_zoom"    # F

    .prologue
    .line 357
    if-eqz p1, :cond_0

    .line 358
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableBitmapDrawable;

    invoke-direct {v0, p1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableBitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->setImageDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;FF)V

    .line 361
    :goto_0
    return-void

    .line 360
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->setImageDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;FF)V

    goto :goto_0
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 365
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v1, v1}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->setImageDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;FF)V

    .line 366
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;FF)V
    .locals 7
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "initial_matrix"    # Landroid/graphics/Matrix;
    .param p3, "min_zoom"    # F
    .param p4, "max_zoom"    # F

    .prologue
    .line 371
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getWidth()I

    move-result v6

    .line 373
    .local v6, "viewWidth":I
    if-gtz v6, :cond_0

    .line 374
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$1;-><init>(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;FF)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mLayoutRunnable:Ljava/lang/Runnable;

    .line 384
    :goto_0
    return-void

    .line 383
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->_setImageDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/Matrix;FF)V

    goto :goto_0
.end method

.method public setImageMatrix(Landroid/graphics/Matrix;)V
    .locals 3
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 511
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    .line 512
    .local v0, "current":Landroid/graphics/Matrix;
    const/4 v1, 0x0

    .line 514
    .local v1, "needUpdate":Z
    if-nez p1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 515
    :cond_1
    const/4 v1, 0x1

    .line 518
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 520
    if-eqz v1, :cond_3

    .line 521
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->onImageMatrixChanged()V

    .line 522
    :cond_3
    return-void
.end method

.method public setImageResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 348
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 349
    return-void
.end method

.method protected setMaxScale(F)V
    .locals 3
    .param p1, "value"    # F

    .prologue
    .line 178
    const-string v0, "ImageViewTouchBase"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setMaxZoom: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMaxZoom:F

    .line 180
    return-void
.end method

.method protected setMinScale(F)V
    .locals 3
    .param p1, "value"    # F

    .prologue
    .line 173
    const-string v0, "ImageViewTouchBase"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setMinZoom: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mMinZoom:F

    .line 175
    return-void
.end method

.method public setOnDrawableChangedListener(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnDrawableChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnDrawableChangeListener;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mDrawableChangeListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnDrawableChangeListener;

    .line 129
    return-void
.end method

.method public setOnLayoutChangeListener(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnLayoutChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnLayoutChangeListener;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mOnLayoutChangeListener:Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$OnLayoutChangeListener;

    .line 133
    return-void
.end method

.method public setScaleType(Landroid/widget/ImageView$ScaleType;)V
    .locals 2
    .param p1, "scaleType"    # Landroid/widget/ImageView$ScaleType;

    .prologue
    .line 141
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    if-ne p1, v0, :cond_0

    .line 142
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 146
    :goto_0
    return-void

    .line 144
    :cond_0
    const-string v0, "ImageViewTouchBase"

    const-string v1, "Unsupported scaletype. Only MATRIX can be used"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected updateRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 3
    .param p1, "bitmapRect"    # Landroid/graphics/RectF;
    .param p2, "scrollRect"    # Landroid/graphics/RectF;

    .prologue
    const/4 v2, 0x0

    .line 738
    if-nez p1, :cond_1

    .line 753
    :cond_0
    :goto_0
    return-void

    .line 741
    :cond_1
    iget v0, p1, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_2

    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisHeight:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_2

    .line 742
    iput v2, p2, Landroid/graphics/RectF;->top:F

    .line 743
    :cond_2
    iget v0, p1, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_3

    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisWidth:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_3

    .line 744
    iput v2, p2, Landroid/graphics/RectF;->left:F

    .line 745
    :cond_3
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_4

    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisHeight:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 746
    iget v0, p1, Landroid/graphics/RectF;->top:F

    sub-float v0, v2, v0

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p2, Landroid/graphics/RectF;->top:F

    .line 747
    :cond_4
    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget v1, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisHeight:I

    add-int/lit8 v1, v1, 0x0

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_5

    iget v0, p1, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_5

    .line 748
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisHeight:I

    add-int/lit8 v0, v0, 0x0

    int-to-float v0, v0

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p2, Landroid/graphics/RectF;->top:F

    .line 749
    :cond_5
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_6

    .line 750
    iget v0, p1, Landroid/graphics/RectF;->left:F

    sub-float v0, v2, v0

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p2, Landroid/graphics/RectF;->left:F

    .line 751
    :cond_6
    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget v1, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisWidth:I

    add-int/lit8 v1, v1, 0x0

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 752
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mThisWidth:I

    add-int/lit8 v0, v0, 0x0

    int-to-float v0, v0

    iget v1, p1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p2, Landroid/graphics/RectF;->left:F

    goto/16 :goto_0
.end method

.method protected zoomTo(F)V
    .locals 3
    .param p1, "scale"    # F

    .prologue
    .line 694
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getMaxScale()F

    move-result v1

    cmpl-float v1, p1, v1

    if-lez v1, :cond_0

    .line 695
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getMaxScale()F

    move-result p1

    .line 696
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getMinScale()F

    move-result v1

    cmpg-float v1, p1, v1

    if-gez v1, :cond_1

    .line 697
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getMinScale()F

    move-result p1

    .line 699
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getCenter()Landroid/graphics/PointF;

    move-result-object v0

    .line 700
    .local v0, "center":Landroid/graphics/PointF;
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v2, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0, p1, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->zoomTo(FFF)V

    .line 701
    return-void
.end method

.method public zoomTo(FF)V
    .locals 3
    .param p1, "scale"    # F
    .param p2, "durationMs"    # F

    .prologue
    .line 704
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getCenter()Landroid/graphics/PointF;

    move-result-object v0

    .line 705
    .local v0, "center":Landroid/graphics/PointF;
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v2, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0, p1, v1, v2, p2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->zoomTo(FFFF)V

    .line 706
    return-void
.end method

.method protected zoomTo(FFF)V
    .locals 4
    .param p1, "scale"    # F
    .param p2, "centerX"    # F
    .param p3, "centerY"    # F

    .prologue
    const/4 v3, 0x1

    .line 709
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getMaxScale()F

    move-result v2

    cmpl-float v2, p1, v2

    if-lez v2, :cond_0

    .line 710
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getMaxScale()F

    move-result p1

    .line 712
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale()F

    move-result v1

    .line 713
    .local v1, "oldScale":F
    div-float v0, p1, v1

    .line 714
    .local v0, "deltaScale":F
    invoke-virtual {p0, v0, p2, p3}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->postScale(FFF)V

    .line 715
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->onZoom(F)V

    .line 716
    invoke-virtual {p0, v3, v3}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->center(ZZ)V

    .line 717
    return-void
.end method

.method protected zoomTo(FFFF)V
    .locals 12
    .param p1, "scale"    # F
    .param p2, "centerX"    # F
    .param p3, "centerY"    # F
    .param p4, "durationMs"    # F

    .prologue
    .line 786
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getMaxScale()F

    move-result v1

    cmpl-float v1, p1, v1

    if-lez v1, :cond_0

    .line 787
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getMaxScale()F

    move-result p1

    .line 789
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 790
    .local v4, "startTime":J
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getScale()F

    move-result v7

    .line 792
    .local v7, "oldScale":F
    sub-float v6, p1, v7

    .line 794
    .local v6, "deltaScale":F
    new-instance v0, Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-direct {v0, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 795
    .local v0, "m":Landroid/graphics/Matrix;
    invoke-virtual {v0, p1, p1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 796
    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->getCenter(Landroid/graphics/Matrix;ZZ)Landroid/graphics/RectF;

    move-result-object v10

    .line 798
    .local v10, "rect":Landroid/graphics/RectF;
    iget v1, v10, Landroid/graphics/RectF;->left:F

    mul-float/2addr v1, p1

    add-float v8, p2, v1

    .line 799
    .local v8, "destX":F
    iget v1, v10, Landroid/graphics/RectF;->top:F

    mul-float/2addr v1, p1

    add-float v9, p3, v1

    .line 801
    .local v9, "destY":F
    iget-object v11, p0, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;

    move-object v2, p0

    move/from16 v3, p4

    invoke-direct/range {v1 .. v9}, Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase$3;-><init>(Lcom/samsung/android/app/memo/uiwidget/ZoomableImageViewBase;FJFFFF)V

    invoke-virtual {v11, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 817
    return-void
.end method
