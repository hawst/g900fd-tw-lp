.class public Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
.super Landroid/text/style/ReplacementSpan;
.source "HtmlImageSpan.java"

# interfaces
.implements Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;


# static fields
.field private static final EXTRA_BOTTOM_MARGIN:I

.field private static final EXTRA_TOP_MARGIN:I

.field public static final IMAGE_BOUND_PADDING_OFFSET:I = 0x1

.field private static final IMAGE_MARGIN:I

.field public static final MAX_WIDTH:I

.field private static final TAG:Ljava/lang/String;

.field private static final mCacheSize:I

.field private static final mImageNotFoundDrawable:Landroid/graphics/drawable/Drawable;

.field private static final mMaxMemory:I

.field public static mMemoryCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAltText:Ljava/lang/String;

.field private mContentUri:Landroid/net/Uri;

.field private mContext:Landroid/content/Context;

.field private mIsLoaded:Z

.field private mLeftBound:I

.field private mListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

.field private mOrientation:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 44
    const-class v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->TAG:Ljava/lang/String;

    .line 67
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v4

    long-to-int v4, v4

    sput v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMaxMemory:I

    .line 69
    sget v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMaxMemory:I

    div-int/lit8 v4, v4, 0x4

    sput v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mCacheSize:I

    .line 75
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 76
    const v5, 0x7f02002b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 77
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v0, v6, v6, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 78
    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mImageNotFoundDrawable:Landroid/graphics/drawable/Drawable;

    .line 81
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->getScreenWidthAbs()I

    move-result v3

    .line 82
    .local v3, "screenWidth":I
    const v4, 0x7f09008f

    invoke-static {v4}, Lcom/samsung/android/app/memo/util/Utils;->getDimension(I)F

    move-result v4

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v2, v4

    .line 84
    .local v2, "maxWidth":I
    const v4, 0x7f09008a

    invoke-static {v4}, Lcom/samsung/android/app/memo/util/Utils;->getDimension(I)F

    move-result v4

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 85
    const v5, 0x7f09008b

    invoke-static {v5}, Lcom/samsung/android/app/memo/util/Utils;->getDimension(I)F

    move-result v5

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v5, v6

    .line 84
    add-int v1, v4, v5

    .line 86
    .local v1, "marginLR":I
    add-int v4, v2, v1

    if-le v4, v3, :cond_0

    .line 87
    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "max width is too large: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    sub-int v2, v3, v1

    .line 95
    :cond_0
    sput v2, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->MAX_WIDTH:I

    .line 96
    const v4, 0x7f0a000a

    invoke-static {v4}, Lcom/samsung/android/app/memo/util/Utils;->getInteger(I)I

    move-result v4

    sput v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->IMAGE_MARGIN:I

    .line 98
    const v4, 0x7f090091

    invoke-static {v4}, Lcom/samsung/android/app/memo/util/Utils;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    sput v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->EXTRA_TOP_MARGIN:I

    .line 99
    const v4, 0x7f090092

    invoke-static {v4}, Lcom/samsung/android/app/memo/util/Utils;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    sput v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->EXTRA_BOTTOM_MARGIN:I

    .line 102
    new-instance v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan$1;

    sget v5, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mCacheSize:I

    invoke-direct {v4, v5}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan$1;-><init>(I)V

    sput-object v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMemoryCache:Landroid/util/LruCache;

    .line 116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;ILjava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentUri"    # Landroid/net/Uri;
    .param p3, "orientation"    # I
    .param p4, "altText"    # Ljava/lang/String;

    .prologue
    .line 141
    invoke-direct {p0}, Landroid/text/style/ReplacementSpan;-><init>()V

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    .line 142
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContext:Landroid/content/Context;

    .line 143
    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContentUri:Landroid/net/Uri;

    .line 144
    iput p3, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mOrientation:I

    .line 145
    invoke-static {p4}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mAltText:Ljava/lang/String;

    .line 147
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mOrientation:I

    sparse-switch v0, :sswitch_data_0

    .line 154
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to proceed given orientaion: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 155
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 154
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :sswitch_0
    return-void

    .line 147
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_0
        0x10e -> :sswitch_0
    .end sparse-switch
.end method

.method public static flushCache()V
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMemoryCache:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 124
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMemoryCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 126
    :cond_0
    return-void
.end method

.method private getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 196
    const/4 v1, 0x0

    .line 197
    .local v1, "d":Landroid/graphics/drawable/BitmapDrawable;
    const/4 v0, 0x0

    .line 198
    .local v0, "bm":Landroid/graphics/Bitmap;
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mIsLoaded:Z

    .line 200
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContentUri:Landroid/net/Uri;

    if-eqz v5, :cond_4

    .line 201
    sget-object v5, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMemoryCache:Landroid/util/LruCache;

    if-eqz v5, :cond_0

    .line 202
    sget-object v5, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMemoryCache:Landroid/util/LruCache;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContentUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bm":Landroid/graphics/Bitmap;
    check-cast v0, Landroid/graphics/Bitmap;

    .line 205
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_0
    if-eqz v0, :cond_1

    .line 206
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/BitmapDrawable;
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v1, v5, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 207
    .restart local v1    # "d":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/BitmapDrawable;->setFilterBitmap(Z)V

    .line 208
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 209
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    .line 208
    invoke-virtual {v1, v4, v4, v3, v5}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    move-object v3, v1

    .line 229
    :goto_0
    return-object v3

    .line 213
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContentUri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 214
    .local v2, "scheme":Ljava/lang/String;
    const-string v5, "content"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "file"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 215
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->getDrawableFromLocalUri()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 216
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "d":Landroid/graphics/drawable/BitmapDrawable;
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v1, v5, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 217
    .restart local v1    # "d":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/BitmapDrawable;->setFilterBitmap(Z)V

    .line 218
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v5

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v6

    invoke-virtual {v1, v4, v4, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 223
    :cond_3
    :goto_1
    if-eqz v1, :cond_7

    :goto_2
    iput-boolean v3, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mIsLoaded:Z

    .line 225
    iget-boolean v3, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mIsLoaded:Z

    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    .line 226
    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMemoryCache:Landroid/util/LruCache;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContentUri:Landroid/net/Uri;

    invoke-virtual {v3, v4, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    .end local v2    # "scheme":Ljava/lang/String;
    :cond_4
    iget-boolean v3, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mIsLoaded:Z

    if-eqz v3, :cond_8

    move-object v3, v1

    goto :goto_0

    .line 219
    .restart local v2    # "scheme":Ljava/lang/String;
    :cond_5
    const-string v5, "http"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "https"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 220
    :cond_6
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->getDrawableFromRemoteUri()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    :cond_7
    move v3, v4

    .line 223
    goto :goto_2

    .line 229
    .end local v2    # "scheme":Ljava/lang/String;
    :cond_8
    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mImageNotFoundDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private getDrawableFromLocalUri()Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    .line 233
    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getDrawableFromLocalUri() uri: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContentUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const/4 v0, 0x0

    .line 237
    .local v0, "bm":Landroid/graphics/Bitmap;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContentUri:Landroid/net/Uri;

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mOrientation:I

    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/memo/util/ImageUtil;->getActualImageWidth(Landroid/content/Context;Landroid/net/Uri;I)I

    move-result v1

    .line 238
    .local v1, "imageWidth":I
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContentUri:Landroid/net/Uri;

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mOrientation:I

    sget v6, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->MAX_WIDTH:I

    invoke-static {v3, v4, v1, v5, v6}, Lcom/samsung/android/app/memo/util/ImageUtil;->decodeImageScaledIf(Landroid/content/Context;Landroid/net/Uri;III)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 242
    .end local v1    # "imageWidth":I
    :goto_0
    return-object v0

    .line 239
    :catch_0
    move-exception v2

    .line 240
    .local v2, "ioe":Ljava/io/IOException;
    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to loaded content "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContentUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getDrawableFromRemoteUri()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x0

    return-object v0
.end method

.method public static trimMemory()V
    .locals 2

    .prologue
    .line 128
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMemoryCache:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 129
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMemoryCache:Landroid/util/LruCache;

    sget v1, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mCacheSize:I

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->trimToSize(I)V

    .line 131
    :cond_0
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Ljava/lang/CharSequence;IIFIIILandroid/graphics/Paint;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "start"    # I
    .param p4, "end"    # I
    .param p5, "x"    # F
    .param p6, "top"    # I
    .param p7, "y"    # I
    .param p8, "bottom"    # I
    .param p9, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 180
    .local v0, "b":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 182
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int v3, p7, v3

    sget v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->EXTRA_TOP_MARGIN:I

    sub-int/2addr v3, v4

    sget v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->EXTRA_BOTTOM_MARGIN:I

    add-int v2, v3, v4

    .line 183
    .local v2, "transY":I
    sget v3, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->IMAGE_MARGIN:I

    int-to-float v3, v3

    add-float/2addr v3, p5

    float-to-int v1, v3

    .line 184
    .local v1, "transX":I
    iput v1, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mLeftBound:I

    .line 185
    int-to-float v3, v1

    int-to-float v4, v2

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 186
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 187
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 188
    return-void
.end method

.method public getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContentUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getLeftBound()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mLeftBound:I

    return v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mOrientation:I

    return v0
.end method

.method public getSize(Landroid/graphics/Paint;Ljava/lang/CharSequence;IILandroid/graphics/Paint$FontMetricsInt;)I
    .locals 4
    .param p1, "paint"    # Landroid/graphics/Paint;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "start"    # I
    .param p4, "end"    # I
    .param p5, "fm"    # Landroid/graphics/Paint$FontMetricsInt;

    .prologue
    .line 161
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 162
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 164
    .local v1, "rect":Landroid/graphics/Rect;
    if-eqz p5, :cond_0

    .line 165
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    neg-int v2, v2

    sget v3, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->EXTRA_TOP_MARGIN:I

    sub-int/2addr v2, v3

    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 166
    sget v2, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->EXTRA_BOTTOM_MARGIN:I

    add-int/lit8 v2, v2, 0x0

    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 168
    iget v2, p5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 169
    iget v2, p5, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iput v2, p5, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 172
    :cond_0
    iget v2, v1, Landroid/graphics/Rect;->right:I

    sget v3, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->IMAGE_MARGIN:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    return v2
.end method

.method public isLoaded()Z
    .locals 1

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->getDrawable()Landroid/graphics/drawable/Drawable;

    .line 192
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mIsLoaded:Z

    return v0
.end method

.method public onClick(Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;)V
    .locals 1
    .param p1, "span"    # Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .prologue
    .line 286
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;->onClick(Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;)V

    .line 288
    :cond_0
    return-void
.end method

.method public setOnClickListener(Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    .line 282
    return-void
.end method

.method public toHtml()Ljava/lang/String;
    .locals 3

    .prologue
    .line 257
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<img src=\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContentUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 258
    const-string v2, "\" orientation=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" altText=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mAltText:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 259
    const-string v2, "\"/>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 257
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 261
    .local v0, "imgTag":Ljava/lang/String;
    return-object v0
.end method

.method public toHtmlForCopy()Ljava/lang/String;
    .locals 4

    .prologue
    .line 266
    sget v1, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->MAX_WIDTH:I

    .line 267
    .local v1, "width":I
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<img src=\""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mContentUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" orientation=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mOrientation:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 268
    const-string v3, "\" altText=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mAltText:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\"   width=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" />"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 267
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 269
    .local v0, "imgTag":Ljava/lang/String;
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->toHtml()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
