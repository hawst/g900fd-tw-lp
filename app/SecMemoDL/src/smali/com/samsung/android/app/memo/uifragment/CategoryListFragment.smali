.class public Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
.super Landroid/app/ListFragment;
.source "CategoryListFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/ListFragment;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field private static final CID_DISPLAY_NAME:I = 0x2

.field private static final CID_ID:I = 0x0

.field private static final CID_UUID:I = 0x1

.field private static final CID_V_COUNT:I = 0x3

.field private static final COUNT_LOADER:I = 0x1

.field private static final MAIN_LOADER:I = 0x0

.field private static final MAX_HEADER_COUNT:I = 0x2

.field private static final ORDERBY:Ljava/lang/String; = "orderBy ASC"

.field private static final PROJECTION_COUNT:Ljava/lang/String; = "(SELECT COUNT(*) FROM memo WHERE memo.isDeleted IS 0 AND memo.categoryUUID IS category.UUID) AS V_COUNT"

.field private static final PROJECTION_COUNTALL:[Ljava/lang/String;

.field private static final SELECTION:Ljava/lang/String; = "isDeleted IS 0"

.field public static final SELECT_ALL_CATEGORY:I = -0x3

.field public static final SELECT_NONE_CATEGORY:I = -0x2

.field public static final SELECT_OTHER_CATEGORY:I = -0x1

.field private static final TAG:Ljava/lang/String;

.field public static isNeededCategoryFragment:Z

.field private static mNumberOfAllCategory:I

.field private static mNumberOfCategory:I

.field private static mNumberOfNoneCategory:I

.field public static mSelectedPosition:I


# instance fields
.field private mAllHeader:Landroid/view/View;

.field private mCategoryAllCount:Landroid/widget/TextView;

.field private mCategoryAllText:Landroid/widget/TextView;

.field public mCategoryListCursorAdpater:Landroid/widget/CursorAdapter;

.field private mCategoryNoneCount:Landroid/widget/TextView;

.field private mCategoryNoneText:Landroid/widget/TextView;

.field private mCountSelection:Ljava/lang/String;

.field private mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

.field private mNoneHeader:Landroid/view/View;

.field private mProjection:[Ljava/lang/String;

.field private mSelectedCategoryType:I

.field private mSelectedCategoryUUID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 60
    const-class v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->TAG:Ljava/lang/String;

    .line 67
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    .line 68
    const-string v1, "COUNT(*)"

    aput-object v1, v0, v2

    .line 67
    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->PROJECTION_COUNTALL:[Ljava/lang/String;

    .line 99
    sput v2, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNumberOfAllCategory:I

    .line 100
    sput v2, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNumberOfNoneCategory:I

    .line 101
    sput v2, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNumberOfCategory:I

    .line 104
    const/4 v0, -0x1

    sput v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedPosition:I

    .line 490
    sput-boolean v2, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->isNeededCategoryFragment:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 58
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 105
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedCategoryUUID:Ljava/lang/String;

    .line 107
    iput v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedCategoryType:I

    .line 109
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    .line 110
    const-string v1, "_id"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "UUID"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "(SELECT COUNT(*) FROM memo WHERE memo.isDeleted IS 0 AND memo.categoryUUID IS category.UUID) AS V_COUNT"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mProjection:[Ljava/lang/String;

    .line 112
    const-string v0, "isDeleted IS 0"

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCountSelection:Ljava/lang/String;

    .line 58
    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedCategoryUUID:Ljava/lang/String;

    return-object v0
.end method

.method private getBackgroundResId(II)I
    .locals 1
    .param p1, "currentPosition"    # I
    .param p2, "selectedPosition"    # I

    .prologue
    .line 302
    if-ne p1, p2, :cond_0

    .line 303
    const v0, 0x7f020072

    .line 305
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f020006

    goto :goto_0
.end method

.method private isHeaderCountMax()Z
    .locals 2

    .prologue
    .line 460
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setSelectedPosition(I)V
    .locals 4
    .param p1, "newPosition"    # I

    .prologue
    const v3, 0x7f08000c

    const v2, 0x7f080009

    .line 270
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryAllText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 271
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryAllCount:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 272
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryNoneText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 273
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryNoneCount:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 281
    const/4 v0, -0x3

    if-ne p1, v0, :cond_1

    .line 283
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryAllText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 284
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryAllCount:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 296
    :cond_0
    :goto_0
    sput p1, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedPosition:I

    .line 297
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryListCursorAdpater:Landroid/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->notifyDataSetChanged()V

    .line 298
    return-void

    .line 288
    :cond_1
    const/4 v0, -0x2

    if-ne p1, v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryNoneText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 291
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryNoneCount:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method


# virtual methods
.method public focusHeader()V
    .locals 2

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 235
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryListCursorAdpater:Landroid/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->notifyDataSetChanged()V

    .line 236
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 237
    return-void
.end method

.method protected getCursorAdapter()Landroid/widget/CursorAdapter;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 240
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment$1;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isFonbletHD()Z

    move-result v1

    if-eqz v1, :cond_0

    const v3, 0x7f04000a

    :goto_0
    const/4 v4, 0x0

    .line 241
    new-array v5, v8, [Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mProjection:[Ljava/lang/String;

    const/4 v6, 0x2

    aget-object v1, v1, v6

    aput-object v1, v5, v7

    new-array v6, v8, [I

    const v1, 0x7f0e0009

    aput v1, v6, v7

    move-object v1, p0

    .line 240
    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment$1;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    return-object v0

    :cond_0
    const v3, 0x7f040009

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 207
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 208
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->TAG:Ljava/lang/String;

    const-string v1, "onActivityCreated()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isFonbletHD()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f040012

    :goto_0
    invoke-virtual {v1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mAllHeader:Landroid/view/View;

    .line 212
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mAllHeader:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 214
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isFonbletHD()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f040014

    :goto_1
    invoke-virtual {v1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNoneHeader:Landroid/view/View;

    .line 216
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNoneHeader:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 218
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getCursorAdapter()Landroid/widget/CursorAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryListCursorAdpater:Landroid/widget/CursorAdapter;

    .line 219
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryListCursorAdpater:Landroid/widget/CursorAdapter;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 221
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mAllHeader:Landroid/view/View;

    const v1, 0x7f0e0019

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryAllText:Landroid/widget/TextView;

    .line 222
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mAllHeader:Landroid/view/View;

    const v1, 0x7f0e001a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryAllCount:Landroid/widget/TextView;

    .line 224
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNoneHeader:Landroid/view/View;

    const v1, 0x7f0e001c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryNoneText:Landroid/widget/TextView;

    .line 225
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNoneHeader:Landroid/view/View;

    const v1, 0x7f0e001d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryNoneCount:Landroid/widget/TextView;

    .line 229
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 230
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 231
    return-void

    .line 210
    :cond_0
    const v0, 0x7f040011

    goto/16 :goto_0

    .line 214
    :cond_1
    const v0, 0x7f040013

    goto :goto_1
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 122
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onAttach(Landroid/app/Activity;)V

    .line 123
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->TAG:Ljava/lang/String;

    const-string v3, "onAttach()"

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-object v2, v0

    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    return-void

    .line 128
    :catch_0
    move-exception v1

    .line 129
    .local v1, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->TAG:Ljava/lang/String;

    const-string v3, "onAttach"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 130
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 131
    const-string v4, " must implement MemoListActionListener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 130
    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 475
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->isDrawerClosing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 476
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->startDrawerClosing()V

    .line 477
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->finishActionMode()V

    .line 478
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->hideCategoryList()V

    .line 480
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 481
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/samsung/android/app/memo/CategoryManagementActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 482
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->startActivity(Landroid/content/Intent;)V

    .line 488
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 485
    :cond_0
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->isNeededCategoryFragment:Z

    goto :goto_0

    .line 487
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->hideCategoryList()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 142
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 143
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 189
    const/4 v0, 0x0

    .line 191
    .local v0, "cursorloader":Landroid/content/CursorLoader;
    packed-switch p1, :pswitch_data_0

    .line 202
    :goto_0
    return-object v0

    .line 193
    :pswitch_0
    new-instance v0, Landroid/content/CursorLoader;

    .end local v0    # "cursorloader":Landroid/content/CursorLoader;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    .line 194
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mProjection:[Ljava/lang/String;

    const-string v4, "isDeleted IS 0"

    const-string v6, "orderBy ASC"

    .line 193
    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    .restart local v0    # "cursorloader":Landroid/content/CursorLoader;
    goto :goto_0

    .line 197
    :pswitch_1
    new-instance v0, Landroid/content/CursorLoader;

    .end local v0    # "cursorloader":Landroid/content/CursorLoader;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    .line 198
    sget-object v3, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->PROJECTION_COUNTALL:[Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCountSelection:Ljava/lang/String;

    move-object v6, v5

    .line 197
    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .restart local v0    # "cursorloader":Landroid/content/CursorLoader;
    goto :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 148
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->TAG:Ljava/lang/String;

    const-string v5, "onCreateView()"

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isFonbletHD()Z

    move-result v4

    if-eqz v4, :cond_2

    const v4, 0x7f040007

    .line 150
    :goto_0
    const/4 v5, 0x0

    .line 149
    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 151
    .local v3, "v":Landroid/view/View;
    const v4, 0x7f0e0006

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 152
    .local v0, "editCategory":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0021

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 155
    .local v2, "i":Landroid/content/Intent;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.PICK"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 156
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "android.intent.action.GET_CONTENT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 157
    :cond_0
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 158
    const v4, 0x7f0e0005

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 159
    .local v1, "editCategoryLayout":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 160
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 163
    .end local v1    # "editCategoryLayout":Landroid/view/View;
    :cond_1
    return-object v3

    .line 150
    .end local v0    # "editCategory":Landroid/widget/TextView;
    .end local v2    # "i":Landroid/content/Intent;
    .end local v3    # "v":Landroid/view/View;
    :cond_2
    const v4, 0x7f040006

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 179
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->TAG:Ljava/lang/String;

    const-string v1, "onDestroyView"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryListCursorAdpater:Landroid/widget/CursorAdapter;

    .line 182
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 184
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroyView()V

    .line 185
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 10
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const v4, 0x7f0b000d

    const/4 v3, -0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 310
    invoke-super/range {p0 .. p5}, Landroid/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 311
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onListItemClick pos="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->finishActionMode()V

    .line 314
    if-nez p3, :cond_2

    .line 315
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedCategoryUUID:Ljava/lang/String;

    .line 316
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->isHeaderCountMax()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 318
    const/4 v0, -0x3

    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setSelectedPosition(I)V

    .line 319
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    const-string v1, ""

    const v2, 0x7f0b0017

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2, v8}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->onCategroyChanged(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 365
    :cond_0
    :goto_0
    return-void

    .line 323
    :cond_1
    invoke-direct {p0, v3}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setSelectedPosition(I)V

    .line 324
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    const-string v1, ""

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2, v8}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->onCategroyChanged(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 327
    :cond_2
    if-ne p3, v7, :cond_3

    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->isHeaderCountMax()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 329
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedCategoryUUID:Ljava/lang/String;

    .line 330
    invoke-direct {p0, v3}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setSelectedPosition(I)V

    .line 331
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    const-string v1, ""

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2, v8}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->onCategroyChanged(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 335
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 336
    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mProjection:[Ljava/lang/String;

    const-string v3, "isDeleted IS 0"

    const/4 v4, 0x0

    const-string v5, "orderBy ASC"

    .line 335
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 338
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->isHeaderCountMax()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v7

    :goto_1
    add-int/2addr v0, v1

    if-le p3, v0, :cond_6

    .line 339
    :cond_4
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 340
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_5
    move v0, v8

    .line 338
    goto :goto_1

    .line 345
    :cond_6
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->isHeaderCountMax()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 347
    add-int/lit8 v0, p3, -0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setSelectedPosition(I)V

    .line 348
    add-int/lit8 v0, p3, -0x2

    invoke-interface {v6, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 356
    :goto_2
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedCategoryUUID:Ljava/lang/String;

    .line 357
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 358
    const/4 v2, 0x2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 357
    invoke-interface {v0, v1, v2, v8}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->onCategroyChanged(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 360
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 361
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 352
    :cond_7
    add-int/lit8 v0, p3, -0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setSelectedPosition(I)V

    .line 353
    add-int/lit8 v0, p3, -0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_2
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 12
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 372
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    .line 373
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 374
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 375
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    sput v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNumberOfAllCategory:I

    .line 376
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryAllCount:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget v5, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNumberOfAllCategory:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 419
    :cond_0
    :goto_0
    sget v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNumberOfCategory:I

    if-nez v0, :cond_e

    .line 421
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mAllHeader:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeHeaderView(Landroid/view/View;)Z

    .line 433
    :cond_1
    :goto_1
    const/4 v7, 0x0

    .line 435
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 436
    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_MEMO:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "categoryUUID"

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCountSelection:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 435
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 437
    if-eqz v7, :cond_4

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 439
    :cond_2
    const-string v0, "categoryUUID"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 440
    .local v6, "categoryUUID":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 441
    sget v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNumberOfNoneCategory:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNumberOfNoneCategory:I

    .line 444
    :cond_3
    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 450
    .end local v6    # "categoryUUID":Ljava/lang/String;
    :cond_4
    if-eqz v7, :cond_5

    .line 451
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 453
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryNoneCount:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget v5, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNumberOfNoneCategory:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 455
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNumberOfNoneCategory:I

    .line 456
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_6
    :goto_3
    return-void

    .line 378
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryListCursorAdpater:Landroid/widget/CursorAdapter;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryListCursorAdpater:Landroid/widget/CursorAdapter;

    invoke-virtual {v0, p2}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 381
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->PREF_CATEGORY:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 382
    .local v10, "pref":Landroid/content/SharedPreferences;
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_UUID:Ljava/lang/String;

    const-string v1, ""

    invoke-interface {v10, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedCategoryUUID:Ljava/lang/String;

    .line 383
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->KEY_CATEGORY_TYPE:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-interface {v10, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedCategoryType:I

    .line 384
    iget v11, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedCategoryType:I

    .line 386
    .local v11, "prevSelectedCategoryType":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 387
    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "isDeleted IS 0"

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 386
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 388
    .local v8, "cursorCategory":Landroid/database/Cursor;
    if-eqz v8, :cond_6

    .line 391
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    sput v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNumberOfCategory:I

    .line 392
    if-eqz v8, :cond_8

    .line 393
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 396
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedCategoryUUID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 398
    sget v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNumberOfCategory:I

    if-nez v0, :cond_9

    .line 401
    const/4 v0, -0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setSelectedPosition(I)V

    .line 404
    :cond_9
    sget v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedPosition:I

    const/4 v1, -0x3

    if-eq v0, v1, :cond_a

    iget v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedCategoryType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_b

    :cond_a
    const/4 v0, 0x1

    if-eq v11, v0, :cond_b

    .line 406
    const/4 v0, -0x3

    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setSelectedPosition(I)V

    .line 407
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    const-string v1, ""

    .line 408
    const v2, 0x7f0b0017

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 407
    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->onCategroyChanged(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 410
    :cond_b
    sget v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedPosition:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_c

    iget v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedCategoryType:I

    if-nez v0, :cond_0

    .line 412
    :cond_c
    const/4 v0, -0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setSelectedPosition(I)V

    goto/16 :goto_0

    .line 416
    :cond_d
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->setSelectedPosition(I)V

    goto/16 :goto_0

    .line 425
    .end local v8    # "cursorCategory":Landroid/database/Cursor;
    .end local v10    # "pref":Landroid/content/SharedPreferences;
    .end local v11    # "prevSelectedCategoryType":I
    :cond_e
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->isHeaderCountMax()Z

    move-result v0

    if-nez v0, :cond_1

    .line 427
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNoneHeader:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeHeaderView(Landroid/view/View;)Z

    .line 428
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mAllHeader:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 429
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNoneHeader:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 442
    .restart local v6    # "categoryUUID":Ljava/lang/String;
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :cond_f
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/samsung/android/app/memo/MemoDataHandler;->isCategoryExist(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 443
    sget v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNumberOfNoneCategory:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mNumberOfNoneCategory:I
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 446
    .end local v6    # "categoryUUID":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 447
    .local v9, "e":Landroid/database/SQLException;
    :try_start_2
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->TAG:Ljava/lang/String;

    const-string v1, "SQLException occurred in onLoadFinished(). "

    invoke-static {v0, v1, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 450
    if-eqz v7, :cond_6

    .line 451
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    .line 449
    .end local v9    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v0

    .line 450
    if-eqz v7, :cond_10

    .line 451
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 452
    :cond_10
    throw v0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Landroid/content/Loader;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 468
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryListCursorAdpater:Landroid/widget/CursorAdapter;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCategoryListCursorAdpater:Landroid/widget/CursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 471
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 175
    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    .line 176
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 168
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 170
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->TAG:Ljava/lang/String;

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 137
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 138
    return-void
.end method

.method public setPickOneFilter(Ljava/lang/String;)V
    .locals 4
    .param p1, "filterString"    # Ljava/lang/String;

    .prologue
    .line 493
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mProjection:[Ljava/lang/String;

    const/4 v1, 0x3

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "(SELECT COUNT(*) FROM memo WHERE memo.isDeleted IS 0 AND memo.categoryUUID IS category.UUID"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 496
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") AS V_COUNT"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 493
    aput-object v2, v0, v1

    .line 497
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "isDeleted IS 0"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mCountSelection:Ljava/lang/String;

    .line 498
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 501
    :cond_0
    return-void
.end method
