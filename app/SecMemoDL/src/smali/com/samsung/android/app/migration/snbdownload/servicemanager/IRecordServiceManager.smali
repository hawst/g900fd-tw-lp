.class public interface abstract Lcom/samsung/android/app/migration/snbdownload/servicemanager/IRecordServiceManager;
.super Ljava/lang/Object;
.source "IRecordServiceManager.java"


# virtual methods
.method public abstract close()V
.end method

.method public abstract getItems(Ljava/util/List;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
        }
    .end annotation
.end method

.method public abstract getKeys(Ljava/lang/String;I)Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
        }
    .end annotation
.end method
