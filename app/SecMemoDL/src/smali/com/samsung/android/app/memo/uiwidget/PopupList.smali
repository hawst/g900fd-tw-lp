.class public Lcom/samsung/android/app/memo/uiwidget/PopupList;
.super Ljava/lang/Object;
.source "PopupList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uiwidget/PopupList$Item;,
        Lcom/samsung/android/app/memo/uiwidget/PopupList$ItemDataAdapter;,
        Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;
    }
.end annotation


# instance fields
.field private final mAnchorView:Landroid/view/View;

.field private mContentList:Landroid/widget/ListView;

.field private final mContext:Landroid/content/Context;

.field private final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/memo/uiwidget/PopupList$Item;",
            ">;"
        }
    .end annotation
.end field

.field private final mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

.field private final mOnGLobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private final mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mOnPopupItemClickListener:Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;

.field private mPopupHeight:I

.field private mPopupOffsetX:I

.field private mPopupOffsetY:I

.field private mPopupWidth:I

.field private mPopupWindow:Landroid/widget/PopupWindow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "anchorView"    # Landroid/view/View;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mItems:Ljava/util/ArrayList;

    .line 98
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/PopupList$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/PopupList$1;-><init>(Lcom/samsung/android/app/memo/uiwidget/PopupList;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    .line 116
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/PopupList$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/PopupList$2;-><init>(Lcom/samsung/android/app/memo/uiwidget/PopupList;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 129
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/PopupList$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/PopupList$3;-><init>(Lcom/samsung/android/app/memo/uiwidget/PopupList;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mOnGLobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 82
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContext:Landroid/content/Context;

    .line 83
    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mAnchorView:Landroid/view/View;

    .line 84
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uiwidget/PopupList;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/uiwidget/PopupList;Landroid/widget/PopupWindow;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/app/memo/uiwidget/PopupList;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/app/memo/uiwidget/PopupList;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/uiwidget/PopupList;)Landroid/view/View;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mAnchorView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/uiwidget/PopupList;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mOnGLobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/uiwidget/PopupList;)Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mOnPopupItemClickListener:Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/app/memo/uiwidget/PopupList;)V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->updatePopupLayoutParams()V

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/app/memo/uiwidget/PopupList;)I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupOffsetX:I

    return v0
.end method

.method static synthetic access$7(Lcom/samsung/android/app/memo/uiwidget/PopupList;)I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupOffsetY:I

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/app/memo/uiwidget/PopupList;)I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWidth:I

    return v0
.end method

.method static synthetic access$9(Lcom/samsung/android/app/memo/uiwidget/PopupList;)I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupHeight:I

    return v0
.end method

.method private createPopupWindow()Landroid/widget/PopupWindow;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 175
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    .line 176
    .local v0, "popup":Landroid/widget/PopupWindow;
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mOnDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 179
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 180
    new-instance v1, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContext:Landroid/content/Context;

    const v3, 0x101006d

    invoke-direct {v1, v2, v5, v3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContentList:Landroid/widget/ListView;

    .line 181
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContentList:Landroid/widget/ListView;

    new-instance v2, Lcom/samsung/android/app/memo/uiwidget/PopupList$ItemDataAdapter;

    invoke-direct {v2, p0, v5}, Lcom/samsung/android/app/memo/uiwidget/PopupList$ItemDataAdapter;-><init>(Lcom/samsung/android/app/memo/uiwidget/PopupList;Lcom/samsung/android/app/memo/uiwidget/PopupList$ItemDataAdapter;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 182
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContentList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 183
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContentList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200f9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 184
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContentList:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 185
    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 186
    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 187
    return-object v0
.end method

.method private updatePopupLayoutParams()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 159
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContentList:Landroid/widget/ListView;

    .line 160
    .local v0, "content":Landroid/widget/ListView;
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 162
    .local v3, "popup":Landroid/widget/PopupWindow;
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 163
    .local v2, "p":Landroid/graphics/Rect;
    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 165
    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mAnchorView:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v4

    iget v5, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    iget v5, v2, Landroid/graphics/Rect;->bottom:I

    sub-int v1, v4, v5

    .line 166
    .local v1, "maxHeight":I
    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContentList:Landroid/widget/ListView;

    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 167
    const/high16 v6, -0x80000000

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 166
    invoke-virtual {v4, v5, v6}, Landroid/widget/ListView;->measure(II)V

    .line 168
    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mAnchorView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0006

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWidth:I

    .line 169
    invoke-virtual {v0}, Landroid/widget/ListView;->getMeasuredHeight()I

    move-result v4

    iget v5, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v5

    iget v5, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0007

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupHeight:I

    .line 170
    iget v4, v2, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090074

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    add-int/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupOffsetX:I

    .line 171
    iget v4, v2, Landroid/graphics/Rect;->top:I

    neg-int v4, v4

    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0005

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupOffsetY:I

    .line 172
    return-void
.end method


# virtual methods
.method public addItem(ILjava/lang/String;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mItems:Ljava/util/ArrayList;

    new-instance v1, Lcom/samsung/android/app/memo/uiwidget/PopupList$Item;

    invoke-direct {v1, p1, p2}, Lcom/samsung/android/app/memo/uiwidget/PopupList$Item;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    return-void
.end method

.method public clearItems()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 96
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 156
    :cond_0
    return-void
.end method

.method public findItem(I)Lcom/samsung/android/app/memo/uiwidget/PopupList$Item;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 191
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 196
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 191
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uiwidget/PopupList$Item;

    .line 192
    .local v0, "item":Lcom/samsung/android/app/memo/uiwidget/PopupList$Item;
    iget v2, v0, Lcom/samsung/android/app/memo/uiwidget/PopupList$Item;->id:I

    if-ne v2, p1, :cond_0

    goto :goto_0
.end method

.method public setOnPopupItemClickListener(Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mOnPopupItemClickListener:Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;

    .line 88
    return-void
.end method

.method public show()V
    .locals 4

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mOnGLobalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 147
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->createPopupWindow()Landroid/widget/PopupWindow;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 148
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->updatePopupLayoutParams()V

    .line 149
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWidth:I

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 150
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    iget v1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupHeight:I

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 151
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mAnchorView:Landroid/view/View;

    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupOffsetX:I

    iget v3, p0, Lcom/samsung/android/app/memo/uiwidget/PopupList;->mPopupOffsetY:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_0
.end method
