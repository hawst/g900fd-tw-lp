.class Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;
.super Landroid/text/method/ArrowKeyMovementMethod;
.source "RichEditor.java"


# static fields
.field private static final SHORT_PRESS_TIMEOUT:J = 0x190L

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final FOCUS_ON_EDITOR:I

.field private ViewMode:Z

.field isAllDay:Z

.field private isOtherSpanTap:Z

.field private mContext:Landroid/content/Context;

.field private mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

.field private mHandler:Landroid/os/Handler;

.field private mLastDownMillis:J

.field private mSelectedText:Ljava/lang/String;

.field private mSelectedUrl:Ljava/lang/String;

.field private mSelectedUrlSpanFC:Landroid/text/style/ForegroundColorSpan;

.field spanEnd:I

.field spanStart:I

.field private text:Landroid/text/Spannable;

.field private touchPoint:Landroid/graphics/Point;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2076
    const-class v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->TAG:Ljava/lang/String;

    .line 2078
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isEditmode"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2112
    invoke-direct {p0}, Landroid/text/method/ArrowKeyMovementMethod;-><init>()V

    .line 2084
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    const/high16 v3, -0x1000000

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedUrlSpanFC:Landroid/text/style/ForegroundColorSpan;

    .line 2086
    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->isAllDay:Z

    .line 2094
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->ViewMode:Z

    .line 2096
    const/16 v2, 0x1e

    iput v2, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->FOCUS_ON_EDITOR:I

    .line 2113
    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->TAG:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "()"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2114
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mContext:Landroid/content/Context;

    .line 2115
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->touchPoint:Landroid/graphics/Point;

    .line 2116
    if-eqz p2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->ViewMode:Z

    .line 2117
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mHandler:Landroid/os/Handler;

    .line 2118
    return-void

    :cond_0
    move v0, v1

    .line 2116
    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;)Z
    .locals 1

    .prologue
    .line 2094
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->ViewMode:Z

    return v0
.end method


# virtual methods
.method public onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .locals 32
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "spannable"    # Landroid/text/Spannable;
    .param p3, "evt"    # Landroid/view/MotionEvent;

    .prologue
    .line 2122
    move-object/from16 v27, p1

    check-cast v27, Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 2124
    sget-boolean v27, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsClipboardRegistered:Z

    if-nez v27, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->hasFocus()Z

    move-result v27

    if-eqz v27, :cond_0

    .line 2125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->registerClipboard()V

    .line 2127
    :cond_0
    invoke-virtual/range {p3 .. p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    .line 2128
    .local v4, "action":I
    invoke-virtual/range {p3 .. p3}, Landroid/view/MotionEvent;->getX()F

    move-result v21

    .line 2129
    .local v21, "touchPointX":F
    if-eqz v4, :cond_1

    const/16 v27, 0x2

    move/from16 v0, v27

    if-ne v4, v0, :cond_7

    .line 2130
    :cond_1
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->isOtherSpanTap:Z

    .line 2131
    move-object/from16 v23, p1

    .line 2132
    .local v23, "tv":Landroid/widget/TextView;
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    .line 2133
    .local v6, "csText":Ljava/lang/CharSequence;
    instance-of v0, v6, Landroid/text/Spannable;

    move/from16 v27, v0

    if-eqz v27, :cond_6

    .line 2134
    check-cast v6, Landroid/text/Spannable;

    .end local v6    # "csText":Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->text:Landroid/text/Spannable;

    .line 2135
    invoke-virtual/range {p3 .. p3}, Landroid/view/MotionEvent;->getX()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v25, v0

    .line 2136
    .local v25, "x":I
    invoke-virtual/range {p3 .. p3}, Landroid/view/MotionEvent;->getY()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v26, v0

    .line 2137
    .local v26, "y":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->touchPoint:Landroid/graphics/Point;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 2138
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v27

    if-eqz v27, :cond_6

    .line 2140
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getScrollX()I

    move-result v27

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v28

    sub-int v27, v27, v28

    add-int v25, v25, v27

    .line 2141
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getScrollY()I

    move-result v27

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v28

    sub-int v27, v27, v28

    add-int v26, v26, v27

    .line 2143
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v8

    .line 2144
    .local v8, "layout":Landroid/text/Layout;
    move/from16 v0, v26

    invoke-virtual {v8, v0}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v9

    .line 2145
    .local v9, "line":I
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 2146
    .local v5, "bounds":Landroid/graphics/Rect;
    invoke-virtual {v8, v9, v5}, Landroid/text/Layout;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 2147
    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    invoke-virtual {v8, v9, v0}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v15

    .line 2148
    .local v15, "offset":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->ViewMode:Z

    move/from16 v27, v0

    if-eqz v27, :cond_6

    .line 2150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->text:Landroid/text/Spannable;

    move-object/from16 v27, v0

    const-class v28, Landroid/text/style/ClickableSpan;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-interface {v0, v15, v15, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [Landroid/text/style/ClickableSpan;

    .line 2151
    .local v12, "links2":[Landroid/text/style/ClickableSpan;
    if-eqz v12, :cond_6

    array-length v0, v12

    move/from16 v27, v0

    if-lez v27, :cond_6

    .line 2152
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v22

    .line 2153
    .local v22, "tp":Landroid/text/TextPaint;
    invoke-virtual {v8, v9}, Landroid/text/Layout;->getLineStart(I)I

    move-result v11

    .line 2154
    .local v11, "lineStart":I
    invoke-virtual {v8, v9}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v10

    .line 2155
    .local v10, "lineEnd":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->text:Landroid/text/Spannable;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aget-object v28, v12, v28

    invoke-interface/range {v27 .. v28}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v27

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->spanStart:I

    .line 2156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->text:Landroid/text/Spannable;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aget-object v28, v12, v28

    invoke-interface/range {v27 .. v28}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v27

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->spanEnd:I

    .line 2158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->text:Landroid/text/Spannable;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-interface {v0, v11, v10}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v18

    .line 2159
    .local v18, "selectedLine":Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->text:Landroid/text/Spannable;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->spanStart:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->spanEnd:I

    move/from16 v29, v0

    invoke-interface/range {v27 .. v29}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v19

    .line 2161
    .local v19, "selectedText":Ljava/lang/CharSequence;
    new-instance v17, Landroid/graphics/Rect;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Rect;-><init>()V

    .line 2162
    .local v17, "r":Landroid/graphics/Rect;
    move-object/from16 v0, v17

    invoke-virtual {v8, v9, v0}, Landroid/text/Layout;->getLineBounds(ILandroid/graphics/Rect;)I

    .line 2164
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->spanStart:I

    move/from16 v27, v0

    move/from16 v0, v27

    if-lt v0, v11, :cond_3

    .line 2166
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v27, v0

    const/16 v28, 0x0

    .line 2167
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->spanStart:I

    move/from16 v29, v0

    sub-int v29, v29, v11

    .line 2166
    move-object/from16 v0, v18

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-interface {v0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v28

    .line 2167
    invoke-interface/range {v28 .. v28}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    add-int v27, v27, v28

    move/from16 v0, v27

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 2168
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v27, v0

    invoke-interface/range {v19 .. v19}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    add-int v27, v27, v28

    move/from16 v0, v27

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 2175
    :goto_0
    move-object/from16 v0, v17

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v27

    if-eqz v27, :cond_6

    move-object/from16 v27, v19

    .line 2176
    check-cast v27, Landroid/text/Spanned;

    const/16 v28, 0x0

    .line 2177
    invoke-interface/range {v19 .. v19}, Ljava/lang/CharSequence;->length()I

    move-result v29

    const-class v30, Landroid/text/style/URLSpan;

    .line 2176
    invoke-interface/range {v27 .. v30}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v24

    check-cast v24, [Landroid/text/style/URLSpan;

    .line 2178
    .local v24, "urls":[Landroid/text/style/URLSpan;
    if-eqz v24, :cond_4

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v27, v0

    if-lez v27, :cond_4

    .line 2179
    const/16 v27, 0x1

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->isOtherSpanTap:Z

    .line 2180
    if-nez v4, :cond_2

    .line 2181
    invoke-interface/range {v19 .. v19}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedText:Ljava/lang/String;

    .line 2182
    const/16 v27, 0x0

    aget-object v27, v24, v27

    invoke-virtual/range {v27 .. v27}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedUrl:Ljava/lang/String;

    .line 2183
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->text:Landroid/text/Spannable;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedUrlSpanFC:Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->spanStart:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->spanEnd:I

    move/from16 v30, v0

    const/16 v31, 0x0

    invoke-interface/range {v27 .. v31}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2184
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->performClick()Z

    .line 2186
    :cond_2
    const/16 v27, 0x1

    .line 2274
    .end local v5    # "bounds":Landroid/graphics/Rect;
    .end local v8    # "layout":Landroid/text/Layout;
    .end local v9    # "line":I
    .end local v10    # "lineEnd":I
    .end local v11    # "lineStart":I
    .end local v12    # "links2":[Landroid/text/style/ClickableSpan;
    .end local v15    # "offset":I
    .end local v17    # "r":Landroid/graphics/Rect;
    .end local v18    # "selectedLine":Ljava/lang/CharSequence;
    .end local v19    # "selectedText":Ljava/lang/CharSequence;
    .end local v22    # "tp":Landroid/text/TextPaint;
    .end local v23    # "tv":Landroid/widget/TextView;
    .end local v24    # "urls":[Landroid/text/style/URLSpan;
    .end local v25    # "x":I
    .end local v26    # "y":I
    .end local p1    # "textView":Landroid/widget/TextView;
    :goto_1
    return v27

    .line 2170
    .restart local v5    # "bounds":Landroid/graphics/Rect;
    .restart local v8    # "layout":Landroid/text/Layout;
    .restart local v9    # "line":I
    .restart local v10    # "lineEnd":I
    .restart local v11    # "lineStart":I
    .restart local v12    # "links2":[Landroid/text/style/ClickableSpan;
    .restart local v15    # "offset":I
    .restart local v17    # "r":Landroid/graphics/Rect;
    .restart local v18    # "selectedLine":Ljava/lang/CharSequence;
    .restart local v19    # "selectedText":Ljava/lang/CharSequence;
    .restart local v22    # "tp":Landroid/text/TextPaint;
    .restart local v23    # "tv":Landroid/widget/TextView;
    .restart local v25    # "x":I
    .restart local v26    # "y":I
    .restart local p1    # "textView":Landroid/widget/TextView;
    :cond_3
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v27, v0

    .line 2172
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->spanStart:I

    move/from16 v28, v0

    sub-int v28, v11, v28

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->spanEnd:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->spanStart:I

    move/from16 v30, v0

    sub-int v29, v29, v30

    .line 2171
    move-object/from16 v0, v19

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-interface {v0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v28

    .line 2173
    invoke-interface/range {v28 .. v28}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v28

    .line 2171
    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    add-int v27, v27, v28

    .line 2170
    move/from16 v0, v27

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->right:I

    goto/16 :goto_0

    .restart local v24    # "urls":[Landroid/text/style/URLSpan;
    :cond_4
    move-object/from16 v27, v19

    .line 2188
    check-cast v27, Landroid/text/Spanned;

    const/16 v28, 0x0

    .line 2189
    invoke-interface/range {v19 .. v19}, Ljava/lang/CharSequence;->length()I

    move-result v29

    const-class v30, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;

    .line 2188
    invoke-interface/range {v27 .. v30}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;

    .line 2191
    .local v7, "intUrls":[Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;
    if-eqz v7, :cond_6

    array-length v0, v7

    move/from16 v27, v0

    if-lez v27, :cond_6

    .line 2192
    const/16 v27, 0x1

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->isOtherSpanTap:Z

    .line 2193
    if-nez v4, :cond_5

    .line 2194
    invoke-interface/range {v19 .. v19}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedText:Ljava/lang/String;

    .line 2195
    const/16 v27, 0x0

    aget-object v27, v7, v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->getTime()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedUrl:Ljava/lang/String;

    .line 2196
    const/16 v27, 0x0

    aget-object v27, v7, v27

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;->getAllDay()Z

    move-result v27

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->isAllDay:Z

    .line 2197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->text:Landroid/text/Spannable;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedUrlSpanFC:Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->spanStart:I

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->spanEnd:I

    move/from16 v30, v0

    const/16 v31, 0x0

    invoke-interface/range {v27 .. v31}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2198
    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->performClick()Z

    .line 2200
    :cond_5
    const/16 v27, 0x1

    goto/16 :goto_1

    .line 2208
    .end local v5    # "bounds":Landroid/graphics/Rect;
    .end local v7    # "intUrls":[Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$InternalURLSpan;
    .end local v8    # "layout":Landroid/text/Layout;
    .end local v9    # "line":I
    .end local v10    # "lineEnd":I
    .end local v11    # "lineStart":I
    .end local v12    # "links2":[Landroid/text/style/ClickableSpan;
    .end local v15    # "offset":I
    .end local v17    # "r":Landroid/graphics/Rect;
    .end local v18    # "selectedLine":Ljava/lang/CharSequence;
    .end local v19    # "selectedText":Ljava/lang/CharSequence;
    .end local v22    # "tp":Landroid/text/TextPaint;
    .end local v24    # "urls":[Landroid/text/style/URLSpan;
    .end local v25    # "x":I
    .end local v26    # "y":I
    :cond_6
    if-nez v4, :cond_7

    .line 2209
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mLastDownMillis:J

    .line 2211
    .end local v23    # "tv":Landroid/widget/TextView;
    :cond_7
    const/16 v27, 0x1

    move/from16 v0, v27

    if-ne v4, v0, :cond_9

    .line 2212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->text:Landroid/text/Spannable;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedUrlSpanFC:Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v28, v0

    invoke-interface/range {v27 .. v28}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 2213
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedUrl:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v27

    if-nez v27, :cond_b

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->isOtherSpanTap:Z

    move/from16 v27, v0

    if-eqz v27, :cond_b

    .line 2214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->text:Landroid/text/Spannable;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedUrlSpanFC:Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v28, v0

    invoke-interface/range {v27 .. v28}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 2215
    const/16 v27, 0x1

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->isOtherSpanTap:Z

    .line 2216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v27, v0

    if-eqz v27, :cond_8

    .line 2217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-virtual/range {v27 .. v28}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->playSoundEffect(I)V

    .line 2218
    :cond_8
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsUSAModel()Z

    move-result v27

    if-eqz v27, :cond_a

    .line 2219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    check-cast v27, Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedUrl:Ljava/lang/String;

    move-object/from16 v28, v0

    .line 2220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedText:Ljava/lang/String;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    .line 2219
    invoke-static/range {v27 .. v30}, Lcom/samsung/android/app/memo/util/Utils;->performLinksContextMenuAction(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2221
    const/16 v27, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedUrl:Ljava/lang/String;

    .line 2274
    :cond_9
    :goto_2
    invoke-super/range {p0 .. p3}, Landroid/text/method/ArrowKeyMovementMethod;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v27

    goto/16 :goto_1

    .line 2223
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->text:Landroid/text/Spannable;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedUrlSpanFC:Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v28, v0

    invoke-interface/range {v27 .. v28}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 2224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    check-cast v27, Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedUrl:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mSelectedText:Ljava/lang/String;

    move-object/from16 v29, v0

    .line 2225
    const/16 v30, 0x0

    .line 2224
    invoke-static/range {v27 .. v30}, Lcom/samsung/android/app/memo/util/Utils;->showLinksContextMenu(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_2

    .line 2227
    :cond_b
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move/from16 v27, v0

    if-eqz v27, :cond_c

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->ViewMode:Z

    move/from16 v27, v0

    if-eqz v27, :cond_c

    move-object/from16 v27, p1

    .line 2228
    check-cast v27, Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->removeLinks()V

    move-object/from16 v27, p1

    .line 2229
    check-cast v27, Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual/range {p3 .. p3}, Landroid/view/MotionEvent;->getX()F

    move-result v28

    invoke-virtual/range {p3 .. p3}, Landroid/view/MotionEvent;->getY()F

    move-result v29

    invoke-virtual/range {v27 .. v29}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getOffsetForPosition(FF)I

    move-result v16

    .line 2230
    .local v16, "position":I
    check-cast p1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .end local p1    # "textView":Landroid/widget/TextView;
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSelection(I)V

    .line 2231
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->ViewMode:Z

    .line 2232
    const/16 v27, 0x1

    goto/16 :goto_1

    .line 2233
    .end local v16    # "position":I
    .restart local p1    # "textView":Landroid/widget/TextView;
    :cond_c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mLastDownMillis:J

    move-wide/from16 v30, v0

    sub-long v28, v28, v30

    const-wide/16 v30, 0x190

    cmp-long v27, v28, v30

    if-gez v27, :cond_9

    .line 2235
    invoke-virtual/range {p3 .. p3}, Landroid/view/MotionEvent;->getX()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v25, v0

    .line 2236
    .restart local v25    # "x":I
    invoke-virtual/range {p3 .. p3}, Landroid/view/MotionEvent;->getY()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v26, v0

    .line 2238
    .restart local v26    # "y":I
    invoke-virtual/range {p1 .. p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v27

    sub-int v25, v25, v27

    .line 2239
    invoke-virtual/range {p1 .. p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v27

    sub-int v26, v26, v27

    .line 2241
    invoke-virtual/range {p1 .. p1}, Landroid/widget/TextView;->getScrollX()I

    move-result v27

    add-int v25, v25, v27

    .line 2242
    invoke-virtual/range {p1 .. p1}, Landroid/widget/TextView;->getScrollY()I

    move-result v27

    add-int v26, v26, v27

    .line 2244
    invoke-virtual/range {p1 .. p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v8

    .line 2245
    .restart local v8    # "layout":Landroid/text/Layout;
    move/from16 v0, v26

    invoke-virtual {v8, v0}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v9

    .line 2246
    .restart local v9    # "line":I
    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    invoke-virtual {v8, v9, v0}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v14

    .line 2248
    .local v14, "off":I
    const-class v27, Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    move-object/from16 v0, p2

    move-object/from16 v1, v27

    invoke-interface {v0, v14, v14, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v13

    .line 2249
    .local v13, "objects":[Ljava/lang/Object;
    array-length v0, v13

    move/from16 v27, v0

    if-eqz v27, :cond_9

    move-object/from16 v20, v13

    .line 2250
    check-cast v20, [Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .line 2251
    .local v20, "spans":[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->mHandler:Landroid/os/Handler;

    move-object/from16 v27, v0

    new-instance v28, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod$1;

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod$1;-><init>(Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;[Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;F)V

    invoke-virtual/range {v27 .. v28}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2268
    const/16 v27, 0x1

    goto/16 :goto_1
.end method

.method public setViewMode(Z)V
    .locals 0
    .param p1, "viewMode"    # Z

    .prologue
    .line 2099
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->ViewMode:Z

    .line 2100
    return-void
.end method
