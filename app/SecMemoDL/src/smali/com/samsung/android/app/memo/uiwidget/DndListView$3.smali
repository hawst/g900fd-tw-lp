.class Lcom/samsung/android/app/memo/uiwidget/DndListView$3;
.super Ljava/lang/Object;
.source "DndListView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uiwidget/DndListView;->doAnimationForDrop()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/DndListView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    .line 355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "paramAnimation"    # Landroid/view/animation/Animation;

    .prologue
    .line 359
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->access$3(Lcom/samsung/android/app/memo/uiwidget/DndListView;Lcom/samsung/android/app/memo/uiwidget/animation/AnimateDrawable;)V

    .line 360
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOnMoveCompletedListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnMoveCompletedListener;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->access$4(Lcom/samsung/android/app/memo/uiwidget/DndListView;)Lcom/samsung/android/app/memo/uiwidget/DndListView$OnMoveCompletedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->access$5(Lcom/samsung/android/app/memo/uiwidget/DndListView;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->access$5(Lcom/samsung/android/app/memo/uiwidget/DndListView;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    # getter for: Lcom/samsung/android/app/memo/uiwidget/DndListView;->mSrcDragPos:I
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->access$6()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I
    invoke-static {v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->access$5(Lcom/samsung/android/app/memo/uiwidget/DndListView;)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 362
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/DndListView;->mOnMoveCompletedListener:Lcom/samsung/android/app/memo/uiwidget/DndListView$OnMoveCompletedListener;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->access$4(Lcom/samsung/android/app/memo/uiwidget/DndListView;)Lcom/samsung/android/app/memo/uiwidget/DndListView$OnMoveCompletedListener;

    move-result-object v0

    # getter for: Lcom/samsung/android/app/memo/uiwidget/DndListView;->mSrcDragPos:I
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->access$6()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/DndListView;->mDragPos:I
    invoke-static {v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->access$5(Lcom/samsung/android/app/memo/uiwidget/DndListView;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/DndListView$OnMoveCompletedListener;->onOrderChanged(II)V

    .line 366
    :goto_0
    const/4 v0, -0x1

    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->access$8(I)V

    .line 367
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    # invokes: Lcom/samsung/android/app/memo/uiwidget/DndListView;->endProcessingAnimation()V
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->access$9(Lcom/samsung/android/app/memo/uiwidget/DndListView;)V

    .line 368
    return-void

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/DndListView$3;->this$0:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    # invokes: Lcom/samsung/android/app/memo/uiwidget/DndListView;->clearChildViewsAnimation()V
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->access$7(Lcom/samsung/android/app/memo/uiwidget/DndListView;)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "paramAnimation"    # Landroid/view/animation/Animation;

    .prologue
    .line 372
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "paramAnimation"    # Landroid/view/animation/Animation;

    .prologue
    .line 376
    return-void
.end method
