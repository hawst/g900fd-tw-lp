.class Lcom/samsung/android/app/memo/uiwidget/RichEditor$2;
.super Ljava/lang/Object;
.source "RichEditor.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/RichEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "arg0"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 543
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-static {v1, v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$2(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Z)V

    .line 544
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 545
    const-string v2, "input_method"

    .line 544
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 546
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v4, :cond_0

    .line 547
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->spanStart:I
    invoke-static {v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$3(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->spanEnd:I
    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$4(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSelection(II)V

    .line 548
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->performLongClick()Z

    .line 550
    :cond_0
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v1

    if-eqz v1, :cond_1

    .line 554
    :goto_0
    return v4

    .line 553
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0
.end method
