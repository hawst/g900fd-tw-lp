.class Lcom/samsung/android/app/memo/uifragment/CategoryListFragment$1;
.super Landroid/widget/SimpleCursorAdapter;
.source "CategoryListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getCursorAdapter()Landroid/widget/CursorAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V
    .locals 7
    .param p2, "$anonymous0"    # Landroid/content/Context;
    .param p3, "$anonymous1"    # I
    .param p4, "$anonymous2"    # Landroid/database/Cursor;
    .param p5, "$anonymous3"    # [Ljava/lang/String;
    .param p6, "$anonymous4"    # [I
    .param p7, "$anonymous5"    # I

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move v6, p7

    .line 240
    invoke-direct/range {v0 .. v6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "ctx"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const v9, 0x7f08000c

    const v8, 0x7f080009

    const/4 v7, 0x1

    .line 245
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleCursorAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 246
    const v2, 0x7f0e000a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 247
    .local v0, "countView":Landroid/widget/TextView;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "%d"

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x3

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    const v2, 0x7f0e0009

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 251
    .local v1, "nameView":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedCategoryUUID:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->access$1(Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->mSelectedCategoryUUID:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->access$1(Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 253
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 254
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 264
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 259
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
