.class Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;
.super Ljava/lang/Object;
.source "VoiceViewLayout.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 1697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v6, 0x0

    .line 1699
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1701
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1702
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 1704
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1705
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPaused()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1706
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopSoundPlay()V

    .line 1708
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1709
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_3

    .line 1710
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    .line 1711
    invoke-virtual {v2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1713
    .local v0, "attachmentId":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$12(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v2

    .line 1714
    invoke-virtual {v2}, Lcom/samsung/android/app/memo/Session;->isDraft()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1715
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$12(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v2

    .line 1716
    invoke-virtual {v2, v0}, Lcom/samsung/android/app/memo/Session;->deletevoiceFile(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1727
    .end local v0    # "attachmentId":Ljava/lang/String;
    :cond_3
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$28(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xfc5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1729
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    sget-object v3, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$2(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;)V

    .line 1730
    invoke-static {v6}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$41(Z)V

    .line 1731
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const-wide/16 v4, 0x0

    invoke-static {v2, v4, v5}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$5(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;J)V

    .line 1732
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$6(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1733
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$28(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xfca

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1734
    return-void

    .line 1719
    .restart local v0    # "attachmentId":Ljava/lang/String;
    :cond_5
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$12(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v2

    .line 1720
    invoke-virtual {v2, v0}, Lcom/samsung/android/app/memo/Session;->removeVoiceFile(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1723
    :catch_0
    move-exception v1

    .line 1724
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "onClick"

    const-string v3, ""

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
