.class Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SharePopupList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/share/SharePopupList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectShareMenuAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/app/memo/share/SharePopupList;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/memo/share/SharePopupList;Landroid/content/Context;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resource"    # I

    .prologue
    .line 206
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;->this$0:Lcom/samsung/android/app/memo/share/SharePopupList;

    .line 207
    invoke-direct {p0, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 208
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 227
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;->this$0:Lcom/samsung/android/app/memo/share/SharePopupList;

    iget-object v2, v2, Lcom/samsung/android/app/memo/share/SharePopupList;->mActivity:Landroid/app/Activity;

    .line 228
    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 227
    check-cast v1, Landroid/view/LayoutInflater;

    .line 230
    .local v1, "mLayoutInflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_0

    .line 231
    const v2, 0x7f040028

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 232
    new-instance v0, Lcom/samsung/android/app/memo/share/SharePopupList$Holder;

    invoke-direct {v0}, Lcom/samsung/android/app/memo/share/SharePopupList$Holder;-><init>()V

    .line 233
    .local v0, "holder":Lcom/samsung/android/app/memo/share/SharePopupList$Holder;
    const v2, 0x7f0e0063

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/samsung/android/app/memo/share/SharePopupList$Holder;->appIcon:Landroid/widget/ImageView;

    .line 234
    const v2, 0x7f0e0064

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/samsung/android/app/memo/share/SharePopupList$Holder;->appName:Landroid/widget/TextView;

    .line 235
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 238
    .end local v0    # "holder":Lcom/samsung/android/app/memo/share/SharePopupList$Holder;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/share/SharePopupList$Holder;

    .line 239
    .restart local v0    # "holder":Lcom/samsung/android/app/memo/share/SharePopupList$Holder;
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;->this$0:Lcom/samsung/android/app/memo/share/SharePopupList;

    # getter for: Lcom/samsung/android/app/memo/share/SharePopupList;->mPm:Landroid/content/pm/PackageManager;
    invoke-static {v2}, Lcom/samsung/android/app/memo/share/SharePopupList;->access$0(Lcom/samsung/android/app/memo/share/SharePopupList;)Landroid/content/pm/PackageManager;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 240
    iget-object v3, v0, Lcom/samsung/android/app/memo/share/SharePopupList$Holder;->appName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    iget-object v4, p0, Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;->this$0:Lcom/samsung/android/app/memo/share/SharePopupList;

    # getter for: Lcom/samsung/android/app/memo/share/SharePopupList;->mPm:Landroid/content/pm/PackageManager;
    invoke-static {v4}, Lcom/samsung/android/app/memo/share/SharePopupList;->access$0(Lcom/samsung/android/app/memo/share/SharePopupList;)Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    iget-object v3, v0, Lcom/samsung/android/app/memo/share/SharePopupList$Holder;->appIcon:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    iget-object v4, p0, Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;->this$0:Lcom/samsung/android/app/memo/share/SharePopupList;

    # getter for: Lcom/samsung/android/app/memo/share/SharePopupList;->mPm:Landroid/content/pm/PackageManager;
    invoke-static {v4}, Lcom/samsung/android/app/memo/share/SharePopupList;->access$0(Lcom/samsung/android/app/memo/share/SharePopupList;)Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 244
    :cond_1
    return-object p2
.end method

.method public setAppList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 214
    .local p1, "mitems":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/SharePopupList$SelectShareMenuAdapter;->mAppList:Ljava/util/List;

    .line 215
    return-void
.end method
