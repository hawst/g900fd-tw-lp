.class public Lcom/samsung/android/app/migration/SNBClickActivity;
.super Landroid/app/Activity;
.source "SNBClickActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SNBClickActivity"

.field private static instance:Lcom/samsung/android/app/migration/SNBClickActivity;

.field private static path:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/migration/SNBClickActivity;->instance:Lcom/samsung/android/app/migration/SNBClickActivity;

    .line 46
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/samsung/android/app/migration/SNBClickActivity;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/samsung/android/app/migration/SNBClickActivity;->instance:Lcom/samsung/android/app/migration/SNBClickActivity;

    return-object v0
.end method

.method private openSNBContent(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 5
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "data"    # Landroid/net/Uri;

    .prologue
    .line 112
    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_FOLDER_PATH:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/app/migration/SNBClickActivity;->path:Ljava/lang/String;

    .line 113
    sget-object v3, Lcom/samsung/android/app/migration/SNBClickActivity;->path:Ljava/lang/String;

    invoke-static {p0, p2, v3}, Lcom/samsung/android/app/memo/util/FileHelper;->writeFileFromURI(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V

    .line 114
    sget-object v3, Lcom/samsung/android/app/migration/SNBClickActivity;->path:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 115
    .local v1, "snbClickedPath":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/Utils;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, "snbClickedTitle":Ljava/lang/String;
    new-instance v0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .local v0, "convertSNBClickTask":Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;
    invoke-virtual {v0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->start()V

    .line 118
    return-void
.end method

.method private openSNBFile(Landroid/net/Uri;)V
    .locals 4
    .param p1, "data"    # Landroid/net/Uri;

    .prologue
    .line 121
    invoke-virtual {p1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/app/migration/SNBClickActivity;->path:Ljava/lang/String;

    .line 122
    sget-object v3, Lcom/samsung/android/app/migration/SNBClickActivity;->path:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 123
    .local v1, "snbClickedPath":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/Utils;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 124
    .local v2, "snbClickedTitle":Ljava/lang/String;
    new-instance v0, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    .local v0, "convertSNBClickTask":Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;
    invoke-virtual {v0}, Lcom/samsung/android/app/migration/task/ConvertSNBClickTask;->start()V

    .line 127
    return-void
.end method

.method private openVNTContent(Landroid/content/Intent;Landroid/net/Uri;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "data"    # Landroid/net/Uri;

    .prologue
    const/4 v6, 0x1

    .line 84
    const/4 v2, 0x0

    .line 85
    .local v2, "vntContent":Ljava/lang/String;
    new-instance v3, Lcom/samsung/android/app/memo/uiwidget/VNTReader;

    invoke-direct {v3, p0, p1}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    .line 86
    .local v3, "vr":Lcom/samsung/android/app/memo/uiwidget/VNTReader;
    iget-boolean v4, v3, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIsVntCorrupt:Z

    if-eqz v4, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/SNBClickActivity;->finish()V

    .line 109
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-boolean v4, v3, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIsMultiVnt:Z

    if-nez v4, :cond_2

    .line 91
    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->getVntText()Ljava/lang/String;

    move-result-object v2

    .line 97
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 98
    .local v0, "args":Landroid/os/Bundle;
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_EDITMODE:Ljava/lang/String;

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 99
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-class v5, Lcom/samsung/android/app/memo/MemoContentActivity;

    invoke-virtual {v4, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    .line 100
    .local v1, "i":Landroid/content/Intent;
    sget-object v4, Lcom/samsung/android/app/memo/MemoContentActivity;->KEY_BUNDLE:Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 101
    sget-object v4, Lcom/samsung/android/app/memo/MemoContentActivity;->KEY_URI:Ljava/lang/String;

    invoke-virtual {v1, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 102
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->IS_FROM_DRAG_LISTENER:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 103
    const v4, 0x10008000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 104
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VNTFILE:Ljava/lang/String;

    invoke-virtual {v1, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 105
    if-eqz v2, :cond_1

    .line 106
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VNTTEXT:Ljava/lang/String;

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    :cond_1
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/migration/SNBClickActivity;->startActivity(Landroid/content/Intent;)V

    .line 108
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/SNBClickActivity;->finish()V

    goto :goto_0

    .line 93
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/SNBClickActivity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const-string v4, "SNBClickActivity"

    const-string v5, "onCreate"

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    sput-object p0, Lcom/samsung/android/app/migration/SNBClickActivity;->instance:Lcom/samsung/android/app/migration/SNBClickActivity;

    .line 57
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/SNBClickActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 58
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/SNBClickActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/android/app/migration/SNBClickActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02003f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setHomeAsUpIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 59
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/SNBClickActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 60
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    .line 61
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 62
    .local v0, "data":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    .line 64
    .local v3, "scheme":Ljava/lang/String;
    const-string v4, "content"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 66
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/SNBClickActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/samsung/android/app/memo/MemoDataHandler;->getContentName(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "fileName":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v4, ".snb"

    invoke-virtual {v1, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 68
    invoke-direct {p0, v1, v0}, Lcom/samsung/android/app/migration/SNBClickActivity;->openSNBContent(Ljava/lang/String;Landroid/net/Uri;)V

    .line 81
    .end local v0    # "data":Landroid/net/Uri;
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v3    # "scheme":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 69
    .restart local v0    # "data":Landroid/net/Uri;
    .restart local v1    # "fileName":Ljava/lang/String;
    .restart local v3    # "scheme":Ljava/lang/String;
    :cond_1
    if-eqz v1, :cond_2

    const-string v4, ".vnt"

    invoke-virtual {v1, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 70
    invoke-direct {p0, v2, v0}, Lcom/samsung/android/app/migration/SNBClickActivity;->openVNTContent(Landroid/content/Intent;Landroid/net/Uri;)V

    goto :goto_0

    .line 72
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/SNBClickActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00ae

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 73
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/SNBClickActivity;->finish()V

    goto :goto_0

    .line 77
    .end local v1    # "fileName":Ljava/lang/String;
    :cond_3
    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/SNBClickActivity;->openSNBFile(Landroid/net/Uri;)V

    goto :goto_0
.end method
