.class Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7$1;
.super Ljava/lang/Object;
.source "CategoryNamingDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7$1;->this$1:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;

    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 292
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7$1;->this$1:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;->access$0(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;)Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7$1;->this$1:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;->access$0(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;)Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    move-result-object v1

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->access$6(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)Landroid/widget/EditText;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 293
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7$1;->this$1:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;->access$0(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;)Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 294
    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 293
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 295
    .local v0, "Imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/Utils;->isAccessoryKeyboardState(Landroid/view/inputmethod/InputMethodManager;)I

    move-result v1

    if-nez v1, :cond_0

    .line 296
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7$1;->this$1:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;->access$0(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;)Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    move-result-object v1

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->access$6(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 297
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7$1;->this$1:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;->access$0(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;)Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    move-result-object v1

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->access$7(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 298
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7$1;->this$1:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;->access$0(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;)Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    move-result-object v1

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->access$7(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->unregisterClipboard()V

    .line 301
    .end local v0    # "Imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method
