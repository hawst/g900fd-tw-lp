.class public Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
.super Ljava/lang/Object;
.source "VoiceViewLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$android$app$memo$voice$VoiceViewLayout$State:[I = null

.field public static final DISABLE_VOICE_BUTTON:I = 0xfc9

.field public static final ENABLE_VOICE_BUTTON:I = 0xfc8

.field public static final KEYPAD_HIDE:I = 0xfca

.field public static final KEYPAD_SHOW:I = 0xfcb

.field public static final MEMO_VOICE_NOTIFICATION_ID:I = 0x5411111

.field public static final RECORDING_PAUSE:Ljava/lang/String; = "com.samsung.android.app.memo.voicerecorder.pause"

.field public static final RECORDING_PLAY:Ljava/lang/String; = "com.samsung.android.app.memo.voicerecorder.play"

.field public static final RECORDING_RESUME:Ljava/lang/String; = "com.samsung.android.app.memo.voicerecorder.resume"

.field public static final RECORDING_STOP:Ljava/lang/String; = "com.samsung.android.app.memo.voicerecorder.stop"

.field public static final TAG:Ljava/lang/String; = "VoiceViewLayout"

.field public static final VOICE_CLEAR:I = 0xfc7

.field public static final VOICE_DELETE:I = 0xfc5

.field public static final VOICE_LAYOUT_INVISIBLE:I = 0xfcc

.field public static final VOICE_SAVE:I = 0xfc6

.field private static isDeleteDialogShowing:Z


# instance fields
.field private final MAX_PROGRESS:I

.field private alarmReceiver:Landroid/content/BroadcastReceiver;

.field public batteryPercentage:F

.field private batteryReceiver:Landroid/content/BroadcastReceiver;

.field private cameraReceiver:Landroid/content/BroadcastReceiver;

.field private earphoneReceiver:Landroid/content/BroadcastReceiver;

.field private headSetStateNew:I

.field private headSetStateOld:I

.field private isCompletioning:Z

.field private isVoiceViewLayoutVisible:Z

.field private lastNewButtonClickTime:J

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBroadcastReceiverControl:Landroid/content/BroadcastReceiver;

.field private mContentHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/Dialog;

.field private mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

.field private mHandler:Landroid/os/Handler;

.field public mIsNotificationStart:Z

.field private mKnoxUserSwitchReceiver:Landroid/content/BroadcastReceiver;

.field private mMainView:Landroid/view/View;

.field private mMultiPlayingLayout:Landroid/widget/LinearLayout;

.field private mMultiRecordingLayout:Landroid/widget/LinearLayout;

.field private mMute:Landroid/os/Handler;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mOpenedView:Landroid/widget/LinearLayout;

.field private mPlayPauseSeparator:Landroid/view/View;

.field private mPlayingButton:Landroid/widget/ImageButton;

.field private mPlayingPauseButton:Landroid/widget/ImageButton;

.field private mPlayingSeekbar:Landroid/widget/SeekBar;

.field private mRecordButton:Landroid/widget/ImageButton;

.field public mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

.field private mRecordingDuration:Landroid/widget/TextView;

.field private mRecordingPauseButton:Landroid/widget/ImageButton;

.field private mRecordingStopButton:Landroid/widget/ImageButton;

.field private mRemoteViews:Landroid/widget/RemoteViews;

.field private mSingleRecordLayout:Landroid/widget/LinearLayout;

.field public mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

.field private mStartRecordDuration:Landroid/widget/TextView;

.field private mState:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

.field private mStopRecordingDuration:Landroid/widget/TextView;

.field mToast:Landroid/widget/Toast;

.field mVoiceClickListner:Landroid/view/View$OnClickListener;

.field private mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

.field private mVoiceNotification:Landroid/app/Notification;

.field private mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

.field private mVoiveDeleteButton:Landroid/widget/ImageButton;

.field private mplayingEndDuration:Landroid/widget/TextView;

.field private mplayingStartDuration:Landroid/widget/TextView;

.field private notificationTime:J

.field public playerCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field public playerErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private startRecording:Ljava/lang/Runnable;

.field private timeBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private updateTime:Ljava/lang/Runnable;

.field private voiceRecordingTime:J


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$android$app$memo$voice$VoiceViewLayout$State()[I
    .locals 3

    .prologue
    .line 71
    sget-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->$SWITCH_TABLE$com$samsung$android$app$memo$voice$VoiceViewLayout$State:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->values()[Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_PAUSE:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD_PAUSE:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->$SWITCH_TABLE$com$samsung$android$app$memo$voice$VoiceViewLayout$State:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isDeleteDialogShowing:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/view/View;ZLandroid/os/Handler;Landroid/app/NotificationManager;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "manager"    # Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    .param p3, "vInfo"    # Lcom/samsung/android/app/memo/voice/VoiceInfo;
    .param p4, "mainView"    # Landroid/view/View;
    .param p5, "isNewCreated"    # Z
    .param p6, "mContentHandler"    # Landroid/os/Handler;
    .param p7, "notificationManager"    # Landroid/app/NotificationManager;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->MAX_PROGRESS:I

    .line 147
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isCompletioning:Z

    .line 149
    iput-wide v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->voiceRecordingTime:J

    .line 173
    iput-wide v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->lastNewButtonClickTime:J

    .line 187
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mIsNotificationStart:Z

    .line 189
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->batteryPercentage:F

    .line 191
    iput v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->headSetStateOld:I

    .line 193
    iput v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->headSetStateNew:I

    .line 991
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    .line 1044
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    .line 1089
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->playerCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 1109
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$4;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->playerErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 1198
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceClickListner:Landroid/view/View$OnClickListener;

    .line 1491
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->timeBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1529
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$7;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->startRecording:Ljava/lang/Runnable;

    .line 1582
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$8;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTime:Ljava/lang/Runnable;

    .line 1651
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$9;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMute:Landroid/os/Handler;

    .line 226
    iput-object p6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;

    .line 227
    iput-object p7, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mNotificationManager:Landroid/app/NotificationManager;

    .line 228
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->initialize(Landroid/content/Context;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/view/View;Z)V

    .line 229
    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->initNotification()V

    .line 231
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/view/View;ZZLandroid/os/Handler;Landroid/app/NotificationManager;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "manager"    # Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    .param p3, "vInfo"    # Lcom/samsung/android/app/memo/voice/VoiceInfo;
    .param p4, "mainView"    # Landroid/view/View;
    .param p5, "isNewCreated"    # Z
    .param p6, "isUseForPreview"    # Z
    .param p7, "mContentHandler"    # Landroid/os/Handler;
    .param p8, "notificationManager"    # Landroid/app/NotificationManager;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->MAX_PROGRESS:I

    .line 147
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isCompletioning:Z

    .line 149
    iput-wide v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->voiceRecordingTime:J

    .line 173
    iput-wide v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->lastNewButtonClickTime:J

    .line 187
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mIsNotificationStart:Z

    .line 189
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->batteryPercentage:F

    .line 191
    iput v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->headSetStateOld:I

    .line 193
    iput v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->headSetStateNew:I

    .line 991
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;

    .line 1044
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$2;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mSoundListener:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$SoundListener;

    .line 1089
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$3;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->playerCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 1109
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$4;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->playerErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 1198
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$5;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceClickListner:Landroid/view/View$OnClickListener;

    .line 1491
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->timeBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1529
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$7;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->startRecording:Ljava/lang/Runnable;

    .line 1582
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$8;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTime:Ljava/lang/Runnable;

    .line 1651
    new-instance v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$9;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMute:Landroid/os/Handler;

    .line 214
    if-eqz p6, :cond_0

    .line 215
    sget-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mState:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    .line 217
    :cond_0
    iput-object p7, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;

    .line 218
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->initialize(Landroid/content/Context;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/view/View;Z)V

    .line 219
    iput-object p8, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mNotificationManager:Landroid/app/NotificationManager;

    .line 220
    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->initNotification()V

    .line 221
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V
    .locals 0

    .prologue
    .line 147
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isCompletioning:Z

    return-void
.end method

.method static synthetic access$11(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Z
    .locals 1

    .prologue
    .line 1180
    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->callCheck()Z

    move-result v0

    return v0
.end method

.method static synthetic access$12(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    return-object v0
.end method

.method static synthetic access$13(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$14(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 1529
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->startRecording:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$15(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingPauseButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$16(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V
    .locals 0

    .prologue
    .line 609
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateBatteryReceiver(Z)V

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V
    .locals 0

    .prologue
    .line 656
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateCameraReceiver(Z)V

    return-void
.end method

.method static synthetic access$18(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V
    .locals 0

    .prologue
    .line 688
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateKnoxSwitchReceiver(Z)V

    return-void
.end method

.method static synthetic access$19(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V
    .locals 0

    .prologue
    .line 775
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateAlarmReceiver(Z)V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mState:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    return-void
.end method

.method static synthetic access$20(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V
    .locals 0

    .prologue
    .line 730
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateEarphoneReceiver(Z)V

    return-void
.end method

.method static synthetic access$21(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Z
    .locals 1

    .prologue
    .line 1575
    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->fileCheck()Z

    move-result v0

    return v0
.end method

.method static synthetic access$22(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 945
    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->initLayout()V

    return-void
.end method

.method static synthetic access$23(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingPauseButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$24(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)J
    .locals 2

    .prologue
    .line 173
    iget-wide v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->lastNewButtonClickTime:J

    return-wide v0
.end method

.method static synthetic access$25(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/view/View;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$26(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$27(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Landroid/app/Dialog;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mDialog:Landroid/app/Dialog;

    return-void
.end method

.method static synthetic access$28(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$29(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;J)V
    .locals 1

    .prologue
    .line 173
    iput-wide p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->lastNewButtonClickTime:J

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceInfo;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    return-void
.end method

.method static synthetic access$30(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mState:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    return-object v0
.end method

.method static synthetic access$31(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 1582
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTime:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$32(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingStartDuration:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$33(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/media/AudioManager;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$34(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Z)V
    .locals 0

    .prologue
    .line 1625
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setAudioManagerStatus(Z)V

    return-void
.end method

.method static synthetic access$36(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;I)V
    .locals 0

    .prologue
    .line 193
    iput p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->headSetStateNew:I

    return-void
.end method

.method static synthetic access$37(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)I
    .locals 1

    .prologue
    .line 191
    iget v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->headSetStateOld:I

    return v0
.end method

.method static synthetic access$38(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->headSetStateNew:I

    return v0
.end method

.method static synthetic access$39(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 580
    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->pauseDuringAlarm()V

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isCompletioning:Z

    return v0
.end method

.method static synthetic access$40(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;I)V
    .locals 0

    .prologue
    .line 191
    iput p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->headSetStateOld:I

    return-void
.end method

.method static synthetic access$41(Z)V
    .locals 0

    .prologue
    .line 169
    sput-boolean p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isDeleteDialogShowing:Z

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;J)V
    .locals 1

    .prologue
    .line 149
    iput-wide p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->voiceRecordingTime:J

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/RemoteViews;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    return-object v0
.end method

.method private callCheck()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 1181
    iget-object v5, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    if-eqz v5, :cond_0

    .line 1182
    iget-object v5, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    .line 1183
    const-string v6, "phone"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 1182
    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 1184
    .local v3, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v2

    .line 1185
    .local v2, "state":I
    if-nez v2, :cond_0

    .line 1186
    iget-object v5, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    .line 1187
    const-string v6, "audio"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1186
    check-cast v0, Landroid/media/AudioManager;

    .line 1188
    .local v0, "am":Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v1

    .line 1189
    .local v1, "mode":I
    const/4 v5, 0x3

    if-ne v1, v5, :cond_1

    .line 1195
    .end local v0    # "am":Landroid/media/AudioManager;
    .end local v1    # "mode":I
    .end local v2    # "state":I
    .end local v3    # "tm":Landroid/telephony/TelephonyManager;
    :cond_0
    :goto_0
    return v4

    .line 1192
    .restart local v0    # "am":Landroid/media/AudioManager;
    .restart local v1    # "mode":I
    .restart local v2    # "state":I
    .restart local v3    # "tm":Landroid/telephony/TelephonyManager;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private createRemoteView()Landroid/widget/RemoteViews;
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 256
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    if-eqz v6, :cond_0

    .line 257
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    .line 290
    :goto_0
    return-object v6

    .line 259
    :cond_0
    new-instance v6, Landroid/widget/RemoteViews;

    iget-object v7, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f04002a

    invoke-direct {v6, v7, v8}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    .line 261
    new-instance v0, Landroid/content/Intent;

    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    const-class v7, Lcom/samsung/android/app/memo/Main;

    invoke-direct {v0, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 262
    .local v0, "intent":Landroid/content/Intent;
    const-string v6, "android.intent.action.MAIN"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 263
    const-string v6, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 267
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    invoke-static {v6, v10, v0, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 268
    .local v1, "intent_launch":Landroid/app/PendingIntent;
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    new-instance v7, Landroid/content/Intent;

    .line 269
    const-string v8, "com.samsung.android.app.memo.voicerecorder.resume"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 268
    invoke-static {v6, v10, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 270
    .local v4, "intent_recBtn":Landroid/app/PendingIntent;
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    new-instance v7, Landroid/content/Intent;

    .line 271
    const-string v8, "com.samsung.android.app.memo.voicerecorder.pause"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 270
    invoke-static {v6, v10, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 272
    .local v2, "intent_pauseBtn":Landroid/app/PendingIntent;
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    new-instance v7, Landroid/content/Intent;

    .line 273
    const-string v8, "com.samsung.android.app.memo.voicerecorder.stop"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 272
    invoke-static {v6, v10, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 274
    .local v5, "intent_stopBtn":Landroid/app/PendingIntent;
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    .line 275
    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.samsung.android.app.memo.voicerecorder.play"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 274
    invoke-static {v6, v10, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 277
    .local v3, "intent_playBtn":Landroid/app/PendingIntent;
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    const v7, 0x7f0e0065

    invoke-virtual {v6, v7, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 278
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    const v7, 0x7f0e0066

    invoke-virtual {v6, v7, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 279
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    const v7, 0x7f0e0068

    invoke-virtual {v6, v7, v4}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 280
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    const v7, 0x7f0e006a

    invoke-virtual {v6, v7, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 282
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    const v7, 0x7f0e006b

    invoke-virtual {v6, v7, v5}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 283
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    .line 284
    const v7, 0x7f0e0069

    .line 283
    invoke-virtual {v6, v7, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 286
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceNotification:Landroid/app/Notification;

    if-eqz v6, :cond_1

    .line 287
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceNotification:Landroid/app/Notification;

    iget-object v7, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    iput-object v7, v6, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 288
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceNotification:Landroid/app/Notification;

    const v7, 0x7f0200b8

    iput v7, v6, Landroid/app/Notification;->icon:I

    .line 290
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    goto/16 :goto_0
.end method

.method private fileCheck()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1576
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v2, v2, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    if-nez v2, :cond_1

    .line 1579
    :cond_0
    :goto_0
    return v1

    .line 1578
    :cond_1
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v3, v3, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getRealPathFromURI(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1579
    .local v0, "f":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    goto :goto_0
.end method

.method private getNewNotification()Landroid/app/Notification;
    .locals 6

    .prologue
    .line 244
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 245
    .local v0, "builder":Landroid/app/Notification$Builder;
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    const-class v4, Lcom/samsung/android/app/memo/Main;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 246
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 247
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 248
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v3, v4, v1, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 250
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 252
    .local v2, "tN":Landroid/app/Notification;
    return-object v2
.end method

.method private initLayout()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 946
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiveDeleteButton:Landroid/widget/ImageButton;

    const v1, 0x7f0e006d

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusLeftId(I)V

    .line 947
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMultiPlayingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 948
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMultiRecordingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 949
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mSingleRecordLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 950
    sput-boolean v2, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isDeleteDialogShowing:Z

    .line 953
    return-void
.end method

.method private initNotification()V
    .locals 4

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->getNewNotification()Landroid/app/Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceNotification:Landroid/app/Notification;

    .line 235
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceNotification:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x22

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 236
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceNotification:Landroid/app/Notification;

    const/4 v1, 0x4

    iput v1, v0, Landroid/app/Notification;->twQuickPanelEvent:I

    .line 237
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->notificationTime:J

    .line 238
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceNotification:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->priority:I

    .line 239
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceNotification:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/Notification;->when:J

    .line 240
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    .line 241
    return-void
.end method

.method private initView()V
    .locals 9

    .prologue
    const/high16 v8, 0x41700000    # 15.0f

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 370
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mHandler:Landroid/os/Handler;

    .line 372
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    const v3, 0x7f0e006c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mOpenedView:Landroid/widget/LinearLayout;

    .line 374
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    .line 375
    const v3, 0x7f0e006d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 374
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mSingleRecordLayout:Landroid/widget/LinearLayout;

    .line 377
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    .line 378
    const v3, 0x7f0e006f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 377
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordButton:Landroid/widget/ImageButton;

    .line 379
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    .line 380
    const v3, 0x7f0e0070

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 379
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mStartRecordDuration:Landroid/widget/TextView;

    .line 381
    const-string v0, "%02d:%02d:%02d"

    .line 382
    .local v0, "hourFormat":Ljava/lang/String;
    const-string v2, "%02d:%02d:%02d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    const/4 v4, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 383
    .local v1, "timeString":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mStartRecordDuration:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 384
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceClickListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 386
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    .line 387
    const v3, 0x7f0e0071

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 386
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMultiRecordingLayout:Landroid/widget/LinearLayout;

    .line 389
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    .line 390
    const v3, 0x7f0e0072

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 389
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingPauseButton:Landroid/widget/ImageButton;

    .line 391
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingPauseButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceClickListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 393
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    .line 394
    const v3, 0x7f0e0073

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 393
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingStopButton:Landroid/widget/ImageButton;

    .line 395
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingStopButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceClickListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 397
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    .line 398
    const v3, 0x7f0e0074

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 397
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingDuration:Landroid/widget/TextView;

    .line 399
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingDuration:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 400
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    .line 401
    const v3, 0x7f0e0075

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 400
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMultiPlayingLayout:Landroid/widget/LinearLayout;

    .line 402
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    const v3, 0x7f0e007a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    .line 403
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 404
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v6}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 405
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->timeBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 407
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    const v3, 0x7f0e0076

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;

    .line 408
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceClickListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 410
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    .line 411
    const v3, 0x7f0e0078

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 410
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingPauseButton:Landroid/widget/ImageButton;

    .line 412
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingPauseButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceClickListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 414
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    .line 415
    const v3, 0x7f0e007c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 414
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingStartDuration:Landroid/widget/TextView;

    .line 416
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    .line 417
    const v3, 0x7f0e007d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 416
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingEndDuration:Landroid/widget/TextView;

    .line 419
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    .line 420
    const v3, 0x7f0e0079

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 419
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mStopRecordingDuration:Landroid/widget/TextView;

    .line 422
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsJapanModel()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 423
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mStartRecordDuration:Landroid/widget/TextView;

    invoke-virtual {v2, v7, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 424
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingDuration:Landroid/widget/TextView;

    invoke-virtual {v2, v7, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 425
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingStartDuration:Landroid/widget/TextView;

    invoke-virtual {v2, v7, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 426
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingEndDuration:Landroid/widget/TextView;

    invoke-virtual {v2, v7, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 429
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    .line 430
    const v3, 0x7f0e007e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    .line 429
    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiveDeleteButton:Landroid/widget/ImageButton;

    .line 431
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiveDeleteButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceClickListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 432
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    const v3, 0x7f0e0077

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayPauseSeparator:Landroid/view/View;

    .line 433
    return-void
.end method

.method private initialize(Landroid/content/Context;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/view/View;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "manager"    # Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    .param p3, "vInfo"    # Lcom/samsung/android/app/memo/voice/VoiceInfo;
    .param p4, "mainView"    # Landroid/view/View;
    .param p5, "isNewCreated"    # Z

    .prologue
    .line 296
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    .line 297
    iput-object p4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMainView:Landroid/view/View;

    .line 298
    iput-object p2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    .line 299
    iput-object p3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 300
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setVoiceViewLayoutVisible(Z)V

    .line 301
    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->initView()V

    .line 303
    if-eqz p5, :cond_0

    .line 304
    sget-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mState:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    .line 306
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->$SWITCH_TABLE$com$samsung$android$app$memo$voice$VoiceViewLayout$State()[I

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mState:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 342
    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->initLayout()V

    .line 343
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTimerView()V

    .line 346
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    .line 347
    sget-boolean v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isDeleteDialogShowing:Z

    if-eqz v0, :cond_1

    .line 348
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showDialog()V

    .line 350
    :cond_1
    return-void

    .line 309
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->initLayout()V

    .line 310
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTimerView()V

    goto :goto_0

    .line 314
    :pswitch_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setRecordingLayout()V

    .line 315
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTimerView()V

    goto :goto_0

    .line 319
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    .line 320
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v1, v1, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getVoiceRecordingTime(Landroid/net/Uri;)J

    move-result-wide v0

    .line 319
    iput-wide v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->voiceRecordingTime:J

    .line 321
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setReadyToPlayLayout()V

    .line 322
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTimerView()V

    goto :goto_0

    .line 326
    :pswitch_3
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setPlayingLayout()V

    .line 327
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTimerView()V

    goto :goto_0

    .line 331
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    .line 332
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v1, v1, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getVoiceRecordingTime(Landroid/net/Uri;)J

    move-result-wide v0

    .line 331
    iput-wide v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->voiceRecordingTime:J

    .line 333
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setRecordingLayout()V

    .line 334
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTimerView()V

    goto :goto_0

    .line 338
    :pswitch_5
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setPlayingLayout()V

    .line 339
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTimerView()V

    goto :goto_0

    .line 306
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private pauseDuringAlarm()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 581
    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->fileCheck()Z

    move-result v1

    if-nez v1, :cond_0

    .line 582
    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->initLayout()V

    .line 607
    :goto_0
    return-void

    .line 585
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 586
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 588
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getPlayerState()I

    move-result v0

    .line 589
    .local v0, "state":I
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 591
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    if-eqz v1, :cond_2

    .line 592
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    const v2, 0x7f0e006a

    .line 593
    const/16 v3, 0x8

    .line 592
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 594
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    const v2, 0x7f0e0069

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 597
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v1, v4}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseSound(Z)V

    .line 598
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->playerWasPlaying:Z

    .line 599
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 600
    const v3, 0x7f0b00a4

    .line 599
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 601
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;

    const v2, 0x7f0200ad

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 589
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private setAudioManagerStatus(Z)V
    .locals 2
    .param p1, "status"    # Z

    .prologue
    .line 1626
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 1627
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 1628
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 1631
    :cond_0
    return-void
.end method

.method private setCurrentProgress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .param p1, "minFormat"    # Ljava/lang/String;
    .param p2, "timeString_played"    # Ljava/lang/String;
    .param p3, "timeString_end"    # Ljava/lang/String;

    .prologue
    .line 866
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-wide v8, v10, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTotalTime:J

    .line 869
    .local v8, "totalTime":J
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v10}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getCurrentPlayPosition()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long v4, v10, v12

    .line 870
    .local v4, "curTime":J
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v10}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getCurrentPlayPosition()J

    move-result-wide v2

    .line 871
    .local v2, "curProgressTime":J
    move-object/from16 v6, p1

    .line 872
    .local v6, "format":Ljava/lang/String;
    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    .line 873
    const-wide/16 v12, 0x3c

    div-long v12, v4, v12

    const-wide/16 v14, 0x3c

    div-long/2addr v12, v14

    const-wide/16 v14, 0x3c

    rem-long/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-wide/16 v12, 0x3c

    div-long v12, v4, v12

    const-wide/16 v14, 0x3c

    rem-long/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    const-wide/16 v12, 0x3c

    rem-long v12, v4, v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    .line 872
    invoke-static {v6, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 874
    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    .line 875
    const-wide/16 v12, 0x3e8

    div-long v12, v8, v12

    const-wide/16 v14, 0x3c

    div-long/2addr v12, v14

    const-wide/16 v14, 0x3c

    div-long/2addr v12, v14

    const-wide/16 v14, 0x3c

    rem-long/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-wide/16 v12, 0x3e8

    div-long v12, v8, v12

    const-wide/16 v14, 0x3c

    div-long/2addr v12, v14

    const-wide/16 v14, 0x3c

    rem-long/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    const-wide/16 v12, 0x3e8

    div-long v12, v8, v12

    const-wide/16 v14, 0x3c

    rem-long/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    .line 874
    invoke-static {v6, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 876
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingStartDuration:Landroid/widget/TextView;

    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 877
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingEndDuration:Landroid/widget/TextView;

    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 878
    const/high16 v10, 0x447a0000    # 1000.0f

    long-to-float v11, v2

    long-to-float v12, v8

    div-float/2addr v11, v12

    mul-float/2addr v10, v11

    float-to-int v7, v10

    .line 879
    .local v7, "progress":I
    cmp-long v10, v2, v8

    if-nez v10, :cond_0

    .line 880
    const/16 v7, 0x3e8

    .line 881
    :cond_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v10, v7}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 882
    return-void
.end method

.method private setPlayingDuration(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "timeString_played"    # Ljava/lang/String;
    .param p2, "minFormat"    # Ljava/lang/String;
    .param p3, "timeString_end"    # Ljava/lang/String;

    .prologue
    .line 839
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mState:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    sget-object v5, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v4, :cond_1

    .line 840
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-wide v2, v4, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTotalTime:J

    .line 846
    .local v2, "totalTime":J
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 847
    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    const-wide/16 v8, 0x3c

    div-long/2addr v6, v8

    const-wide/16 v8, 0x3c

    div-long/2addr v6, v8

    const-wide/16 v8, 0x3c

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    const-wide/16 v8, 0x3c

    div-long/2addr v6, v8

    const-wide/16 v8, 0x3c

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    const-wide/16 v8, 0x3c

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    .line 846
    invoke-static {p2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 850
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mStopRecordingDuration:Landroid/widget/TextView;

    invoke-virtual {v4, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 863
    .end local v2    # "totalTime":J
    :cond_0
    :goto_0
    return-void

    .line 851
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mState:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    sget-object v5, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_PAUSE:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v4, :cond_0

    .line 852
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getCurrentPlayPosition()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v0, v4, v6

    .line 853
    .local v0, "curTime":J
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 854
    const-wide/16 v6, 0x3c

    div-long v6, v0, v6

    const-wide/16 v8, 0x3c

    div-long/2addr v6, v8

    const-wide/16 v8, 0x3c

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-wide/16 v6, 0x3c

    div-long v6, v0, v6

    const-wide/16 v8, 0x3c

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-wide/16 v6, 0x3c

    rem-long v6, v0, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    .line 853
    invoke-static {p2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 855
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-wide v2, v4, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTotalTime:J

    .line 858
    .restart local v2    # "totalTime":J
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 859
    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    const-wide/16 v8, 0x3c

    div-long/2addr v6, v8

    const-wide/16 v8, 0x3c

    div-long/2addr v6, v8

    const-wide/16 v8, 0x3c

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    const-wide/16 v8, 0x3c

    div-long/2addr v6, v8

    const-wide/16 v8, 0x3c

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    const-wide/16 v8, 0x3c

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    .line 858
    invoke-static {p2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 860
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingStartDuration:Landroid/widget/TextView;

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 861
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingEndDuration:Landroid/widget/TextView;

    invoke-virtual {v4, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private setRecordingDuration(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "hourFormat"    # Ljava/lang/String;
    .param p2, "timeString_played"    # Ljava/lang/String;

    .prologue
    const-wide/16 v8, 0x3e8

    const-wide/16 v6, 0x3c

    .line 885
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getCurRecordingTime()J

    move-result-wide v2

    div-long v0, v2, v8

    .line 887
    .local v0, "curTime":J
    const-wide/16 v2, 0xe10

    cmp-long v2, v0, v2

    if-ltz v2, :cond_2

    .line 888
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 889
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v2, :cond_0

    .line 890
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    .line 891
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 890
    invoke-static {v3, v4, v5}, Lcom/samsung/android/app/memo/util/Utils;->getTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTitle:Ljava/lang/String;

    .line 892
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setReadyToPlayLayout()V

    .line 893
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v2, :cond_1

    .line 894
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    mul-long v4, v0, v8

    iput-wide v4, v2, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTotalTime:J

    .line 895
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTimerView()V

    .line 896
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;

    const/16 v3, 0xfc6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 898
    :cond_2
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 899
    div-long v4, v0, v6

    div-long/2addr v4, v6

    rem-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    div-long v4, v0, v6

    rem-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 900
    rem-long v4, v0, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 898
    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 901
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingDuration:Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 902
    return-void
.end method

.method private updateAlarmReceiver(Z)V
    .locals 6
    .param p1, "register"    # Z

    .prologue
    .line 776
    if-eqz p1, :cond_2

    .line 778
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->alarmReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    .line 818
    :cond_0
    :goto_0
    return-void

    .line 782
    :cond_1
    new-instance v2, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$15;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$15;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->alarmReceiver:Landroid/content/BroadcastReceiver;

    .line 802
    new-instance v0, Landroid/content/IntentFilter;

    .line 803
    const-string v2, "com.samsung.sec.android.clockpackage.alarm.ALARM_ALERT"

    .line 802
    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 804
    .local v0, "alarmfilter":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->alarmReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 805
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->alarmReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    .line 806
    const-string v5, "com.sec.android.app.voicecommand"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 805
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 809
    .end local v0    # "alarmfilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->alarmReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_0

    .line 811
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->alarmReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 812
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->alarmReceiver:Landroid/content/BroadcastReceiver;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 813
    :catch_0
    move-exception v1

    .line 814
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "VoiceViewLayout"

    const-string v3, "unregisterReceiver"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private updateBatteryReceiver(Z)V
    .locals 4
    .param p1, "register"    # Z

    .prologue
    .line 610
    if-eqz p1, :cond_2

    .line 612
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->batteryReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    .line 654
    :cond_0
    :goto_0
    return-void

    .line 616
    :cond_1
    new-instance v2, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$11;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$11;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->batteryReceiver:Landroid/content/BroadcastReceiver;

    .line 640
    new-instance v0, Landroid/content/IntentFilter;

    .line 641
    const-string v2, "android.intent.action.ACTION_POWER_DISCONNECTED"

    .line 640
    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 642
    .local v0, "batteryfilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 643
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->batteryReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 645
    .end local v0    # "batteryfilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->batteryReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_0

    .line 647
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->batteryReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 648
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->batteryReceiver:Landroid/content/BroadcastReceiver;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 649
    :catch_0
    move-exception v1

    .line 650
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "VoiceViewLayout"

    const-string v3, "unregisterReceiver BatteryReceiver"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private updateCameraReceiver(Z)V
    .locals 4
    .param p1, "register"    # Z

    .prologue
    .line 657
    if-eqz p1, :cond_2

    .line 658
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->cameraReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    .line 686
    :cond_0
    :goto_0
    return-void

    .line 661
    :cond_1
    new-instance v2, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$12;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$12;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->cameraReceiver:Landroid/content/BroadcastReceiver;

    .line 673
    new-instance v0, Landroid/content/IntentFilter;

    .line 674
    const-string v2, "com.sec.android.app.voicerecorder.rec_save"

    .line 673
    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 675
    .local v0, "camerafilter":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->cameraReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 677
    .end local v0    # "camerafilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->cameraReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_0

    .line 679
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->cameraReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 680
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->cameraReceiver:Landroid/content/BroadcastReceiver;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 681
    :catch_0
    move-exception v1

    .line 682
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "VoiceViewLayout"

    const-string v3, "unregisterReceiver"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private updateControlReceiver(Z)V
    .locals 4
    .param p1, "register"    # Z

    .prologue
    .line 494
    if-eqz p1, :cond_2

    .line 495
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mBroadcastReceiverControl:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    .line 578
    :cond_0
    :goto_0
    return-void

    .line 498
    :cond_1
    new-instance v2, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$10;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mBroadcastReceiverControl:Landroid/content/BroadcastReceiver;

    .line 560
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 561
    .local v1, "iFilter":Landroid/content/IntentFilter;
    const-string v2, "com.samsung.android.app.memo.voicerecorder.pause"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 562
    const-string v2, "com.samsung.android.app.memo.voicerecorder.resume"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 563
    const-string v2, "com.samsung.android.app.memo.voicerecorder.stop"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 564
    const-string v2, "com.samsung.android.app.memo.voicerecorder.play"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 565
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mBroadcastReceiverControl:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 568
    .end local v1    # "iFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mBroadcastReceiverControl:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_0

    .line 570
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mBroadcastReceiverControl:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 571
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mBroadcastReceiverControl:Landroid/content/BroadcastReceiver;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 572
    :catch_0
    move-exception v0

    .line 573
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "VoiceViewLayout"

    const-string v3, "unregisterReceiver"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private updateEarphoneReceiver(Z)V
    .locals 4
    .param p1, "register"    # Z

    .prologue
    .line 732
    if-eqz p1, :cond_2

    .line 734
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->earphoneReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    .line 772
    :cond_0
    :goto_0
    return-void

    .line 738
    :cond_1
    new-instance v2, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$14;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$14;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->earphoneReceiver:Landroid/content/BroadcastReceiver;

    .line 759
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 760
    .local v1, "receiverFilter":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->earphoneReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 763
    .end local v1    # "receiverFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->earphoneReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_0

    .line 765
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->earphoneReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 766
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->earphoneReceiver:Landroid/content/BroadcastReceiver;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 767
    :catch_0
    move-exception v0

    .line 768
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "VoiceViewLayout"

    const-string v3, "unregisterReceiver"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private updateKnoxSwitchReceiver(Z)V
    .locals 4
    .param p1, "register"    # Z

    .prologue
    .line 689
    if-eqz p1, :cond_2

    .line 690
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mKnoxUserSwitchReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    .line 728
    :cond_0
    :goto_0
    return-void

    .line 692
    :cond_1
    new-instance v2, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$13;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$13;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mKnoxUserSwitchReceiver:Landroid/content/BroadcastReceiver;

    .line 709
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 710
    .local v1, "knoxSwitchFilter":Landroid/content/IntentFilter;
    const-string v2, "com.sec.knox.container.INTENT_KNOX_USER_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 713
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mKnoxUserSwitchReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 714
    :catch_0
    move-exception v0

    .line 715
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 719
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .end local v1    # "knoxSwitchFilter":Landroid/content/IntentFilter;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mKnoxUserSwitchReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_0

    .line 721
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mKnoxUserSwitchReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 722
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mKnoxUserSwitchReceiver:Landroid/content/BroadcastReceiver;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 723
    :catch_1
    move-exception v0

    .line 724
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "VoiceViewLayout"

    const-string v3, "unregisterReceiver"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public cleanUp()V
    .locals 2

    .prologue
    .line 1561
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTime:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1562
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v0, :cond_0

    .line 1563
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 1566
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1567
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 1569
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPaused()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1570
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopSoundPlay()V

    .line 1572
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->initLayout()V

    .line 1573
    return-void
.end method

.method public disableSystemSound()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1609
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    .line 1610
    const-string v2, "statusbar"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1609
    check-cast v0, Landroid/app/StatusBarManager;

    .line 1611
    .local v0, "statusBar":Landroid/app/StatusBarManager;
    if-eqz v0, :cond_0

    .line 1612
    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1617
    .end local v0    # "statusBar":Landroid/app/StatusBarManager;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mAudioManager:Landroid/media/AudioManager;

    if-nez v1, :cond_1

    .line 1618
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    .line 1619
    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 1618
    iput-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mAudioManager:Landroid/media/AudioManager;

    .line 1621
    :cond_1
    invoke-direct {p0, v4}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setAudioManagerStatus(Z)V

    .line 1622
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMute:Landroid/os/Handler;

    const-wide/16 v2, 0x14

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1623
    return-void

    .line 1614
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public enableSystemSound()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1635
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    .line 1636
    const-string v2, "statusbar"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1635
    check-cast v0, Landroid/app/StatusBarManager;

    .line 1637
    .local v0, "statusBar":Landroid/app/StatusBarManager;
    if-eqz v0, :cond_0

    .line 1638
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1642
    .end local v0    # "statusBar":Landroid/app/StatusBarManager;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mAudioManager:Landroid/media/AudioManager;

    if-nez v1, :cond_1

    .line 1643
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    .line 1644
    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    .line 1643
    iput-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mAudioManager:Landroid/media/AudioManager;

    .line 1646
    :cond_1
    invoke-direct {p0, v4}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setAudioManagerStatus(Z)V

    .line 1647
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMute:Landroid/os/Handler;

    const-wide/16 v2, 0x14

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1649
    return-void

    .line 1640
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getIsDeleteDialogShowing()Z
    .locals 1

    .prologue
    .line 1750
    sget-boolean v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isDeleteDialogShowing:Z

    return v0
.end method

.method public getState()Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mState:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    return-object v0
.end method

.method public handleRecordPause()V
    .locals 4

    .prologue
    .line 1406
    sget-boolean v0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z

    if-eqz v0, :cond_0

    .line 1407
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isRecording()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1408
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/memo/voice/VRUtils;->isRecording:Z

    .line 1409
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$17;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$17;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    .line 1418
    const-wide/16 v2, 0xc8

    .line 1409
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1428
    :cond_0
    :goto_0
    return-void

    .line 1420
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingPauseButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200ac

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1421
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00a5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1422
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->disableSystemSound()V

    .line 1423
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->resumeRecording()V

    .line 1424
    sget-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mState:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    .line 1425
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/app/memo/voice/VRUtils;->isRecording:Z

    goto :goto_0
.end method

.method public handleRecordingStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1431
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 1432
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1433
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 1435
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getPlayerState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1436
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopSoundPlay()V

    .line 1437
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1438
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->voiceRecordingTime:J

    .line 1439
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTimerView()V

    .line 1441
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getPlayerState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 1442
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopSoundPlay()V

    .line 1443
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1451
    :goto_0
    return-void

    .line 1447
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->save()V

    .line 1448
    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateBatteryReceiver(Z)V

    .line 1449
    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateCameraReceiver(Z)V

    .line 1450
    invoke-direct {p0, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateKnoxSwitchReceiver(Z)V

    goto :goto_0
.end method

.method public handleStopPlaying()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 821
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopSoundPlay()V

    .line 822
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 823
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_0

    .line 824
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    const v1, 0x7f0e0069

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 825
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    const v1, 0x7f0e006a

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 827
    :cond_0
    invoke-direct {p0, v3}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateKnoxSwitchReceiver(Z)V

    .line 828
    return-void
.end method

.method public hideNotification()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 831
    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateControlReceiver(Z)V

    .line 832
    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateAlarmReceiver(Z)V

    .line 833
    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateEarphoneReceiver(Z)V

    .line 834
    invoke-static {v0}, Lcom/samsung/android/app/memo/MemoApp;->setShowNotificationEnabled(Z)V

    .line 835
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mNotificationManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_0

    .line 836
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mNotificationManager:Landroid/app/NotificationManager;

    const v1, 0x5411111

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 837
    :cond_0
    return-void
.end method

.method public isGoogleTalkVideoChatRunning()Z
    .locals 8

    .prologue
    .line 1160
    const/4 v5, 0x0

    .line 1161
    .local v5, "videoChat":Z
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    .line 1162
    const-string v7, "activity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 1161
    check-cast v2, Landroid/app/ActivityManager;

    .line 1163
    .local v2, "manager":Landroid/app/ActivityManager;
    const/16 v6, 0x32

    invoke-virtual {v2, v6}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v1

    .line 1165
    .local v1, "info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v1, :cond_0

    .line 1166
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    .line 1167
    .local v4, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v4, :cond_1

    .line 1177
    .end local v0    # "i":I
    .end local v4    # "size":I
    :cond_0
    :goto_1
    return v5

    .line 1168
    .restart local v0    # "i":I
    .restart local v4    # "size":I
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 1169
    .local v3, "service":Landroid/app/ActivityManager$RunningServiceInfo;
    const-string v6, "com.google.android.talk:videoChat"

    iget-object v7, v3, Landroid/app/ActivityManager$RunningServiceInfo;->process:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1170
    iget-boolean v6, v3, Landroid/app/ActivityManager$RunningServiceInfo;->started:Z

    if-eqz v6, :cond_2

    .line 1171
    const/4 v5, 0x1

    .line 1172
    goto :goto_1

    .line 1167
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public isNotificationStart()Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mIsNotificationStart:Z

    return v0
.end method

.method public isNowPlaying()Z
    .locals 1

    .prologue
    .line 956
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v0

    return v0
.end method

.method public isVoiceViewLayoutVisible()Z
    .locals 1

    .prologue
    .line 200
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isVoiceViewLayoutVisible:Z

    return v0
.end method

.method protected refreshForceStopTimeFormat()V
    .locals 6

    .prologue
    .line 1135
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v2, v2, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    .line 1136
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v3, v3, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getVoiceRecordingTime(Landroid/net/Uri;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->voiceRecordingTime:J

    .line 1137
    iget-wide v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->voiceRecordingTime:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 1138
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v2, v2, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1140
    .local v0, "attachmentId":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/memo/Session;->deletevoiceFile(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1144
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;

    const/16 v3, 0xfc5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1145
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$16;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$16;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    .line 1150
    const-wide/16 v4, 0xc8

    .line 1145
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1151
    const v2, 0x7f0b009a

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    .line 1157
    .end local v0    # "attachmentId":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 1141
    .restart local v0    # "attachmentId":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1142
    .local v1, "e":Lcom/samsung/android/app/memo/Session$SessionException;
    const-string v2, "VoiceMemo"

    const-string v3, "refreshForceStopTimeFormat"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1153
    .end local v0    # "attachmentId":Ljava/lang/String;
    .end local v1    # "e":Lcom/samsung/android/app/memo/Session$SessionException;
    :cond_1
    const v2, 0x7f0b00ac

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    .line 1154
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;

    const/16 v3, 0xfc6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method

.method public releaseVoiceViewResources()V
    .locals 2

    .prologue
    .line 1547
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTime:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1548
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTimerView()V

    .line 1549
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v0, :cond_0

    .line 1550
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 1552
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1553
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 1555
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPaused()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1556
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopSoundPlay()V

    .line 1558
    :cond_3
    return-void
.end method

.method public removeDialogIfshowing()V
    .locals 1

    .prologue
    .line 1753
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1754
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isDeleteDialogShowing:Z

    .line 1755
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 1758
    :cond_0
    return-void
.end method

.method public save()V
    .locals 6

    .prologue
    .line 1453
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mIsNotificationStart:Z

    .line 1454
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1455
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopRecording()Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 1457
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPaused()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1458
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->stopSoundPlay()V

    .line 1460
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v2, v2, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_4

    .line 1461
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    .line 1462
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v3, v3, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getVoiceRecordingTime(Landroid/net/Uri;)J

    move-result-wide v2

    .line 1461
    iput-wide v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->voiceRecordingTime:J

    .line 1466
    iget-wide v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->voiceRecordingTime:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-gez v2, :cond_5

    .line 1467
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-object v2, v2, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1469
    .local v0, "attachmentId":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/memo/Session;->deletevoiceFile(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1473
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;

    const/16 v3, 0xfc5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1474
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$18;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$18;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    .line 1480
    const-wide/16 v4, 0xc8

    .line 1474
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1481
    const v2, 0x7f0b009a

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    .line 1488
    .end local v0    # "attachmentId":Ljava/lang/String;
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->removeDialogIfshowing()V

    .line 1489
    :cond_4
    return-void

    .line 1470
    .restart local v0    # "attachmentId":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 1471
    .local v1, "e":Lcom/samsung/android/app/memo/Session$SessionException;
    const-string v2, "save"

    const-string v3, ""

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1483
    .end local v0    # "attachmentId":Ljava/lang/String;
    .end local v1    # "e":Lcom/samsung/android/app/memo/Session$SessionException;
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->getState()Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    if-ne v2, v3, :cond_3

    .line 1484
    const v2, 0x7f0b00ac

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    .line 1485
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;

    const/16 v3, 0xfc6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method

.method public setContentHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "mhandler"    # Landroid/os/Handler;

    .prologue
    .line 353
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;

    .line 354
    return-void
.end method

.method public setDioVoiceViewStatus(Landroid/content/Context;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/view/View;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "manager"    # Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    .param p3, "vInfo"    # Lcom/samsung/android/app/memo/voice/VoiceInfo;
    .param p4, "mainView"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 362
    iput-object p2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    .line 363
    iput-object p3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .line 364
    iput-boolean v5, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isCompletioning:Z

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 365
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->initialize(Landroid/content/Context;Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;Lcom/samsung/android/app/memo/voice/VoiceInfo;Landroid/view/View;Z)V

    .line 366
    return-void
.end method

.method public setPlayingLayout()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 924
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiveDeleteButton:Landroid/widget/ImageButton;

    const v1, 0x7f0e0078

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusLeftId(I)V

    .line 925
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMultiPlayingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 926
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMultiRecordingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 927
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mSingleRecordLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 928
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mStopRecordingDuration:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 929
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingStartDuration:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 930
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingEndDuration:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 931
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 932
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200ac

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 933
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 934
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 939
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayPauseSeparator:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 941
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;

    const/16 v1, 0xfc9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 943
    return-void

    .line 936
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200ad

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 937
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public setReadyToPlayLayout()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 906
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiveDeleteButton:Landroid/widget/ImageButton;

    const v1, 0x7f0e0076

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusLeftId(I)V

    .line 907
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMultiPlayingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 908
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMultiRecordingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 909
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mSingleRecordLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 910
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 911
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayPauseSeparator:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 912
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200ad

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 914
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 915
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00a4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 916
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingStartDuration:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 917
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingEndDuration:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 918
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 919
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mStopRecordingDuration:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 920
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;

    const/16 v1, 0xfc9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 921
    return-void
.end method

.method public setRecordingLayout()V
    .locals 7

    .prologue
    const v6, 0x7f0e006a

    const v5, 0x7f0e0068

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 960
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiveDeleteButton:Landroid/widget/ImageButton;

    const v1, 0x7f0e0073

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setNextFocusLeftId(I)V

    .line 961
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMultiPlayingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 962
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mMultiRecordingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 963
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mSingleRecordLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 964
    sget-boolean v0, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isSecMediaLibraryEnabled:Z

    if-eqz v0, :cond_2

    .line 965
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 966
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mState:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD_PAUSE:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    if-ne v0, v1, :cond_1

    .line 967
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingPauseButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200ae

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 968
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/android/app/memo/MemoApp;->isShowNotificationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 969
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v6, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 971
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v5, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 973
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showVoiceNotification(Z)V

    .line 988
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContentHandler:Landroid/os/Handler;

    const/16 v1, 0xfc9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 989
    return-void

    .line 976
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingPauseButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200ac

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 977
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/android/app/memo/MemoApp;->isShowNotificationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 978
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v6, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 980
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v5, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 981
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showVoiceNotification(Z)V

    goto :goto_0

    .line 985
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public setVoiceViewLayoutVisible(Z)V
    .locals 0
    .param p1, "isVoiceViewLayoutVisible"    # Z

    .prologue
    .line 204
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isVoiceViewLayoutVisible:Z

    .line 205
    return-void
.end method

.method public showDialog()V
    .locals 6

    .prologue
    const v3, 0x7f0b0002

    .line 1669
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mDialog:Landroid/app/Dialog;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->isShowing()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1670
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    .line 1671
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b009b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1673
    .local v2, "title":Ljava/lang/String;
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    .line 1674
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 1675
    const v5, 0x7f0b009c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1677
    .local v1, "delMsg":Ljava/lang/String;
    :goto_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1678
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1679
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1680
    new-instance v4, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$19;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$19;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 1696
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v4

    if-eqz v4, :cond_1

    const v3, 0x7f0b0047

    .line 1697
    :cond_1
    new-instance v4, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$20;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    .line 1696
    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 1735
    const v4, 0x7f0b0005

    .line 1736
    new-instance v5, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$21;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$21;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V

    .line 1735
    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1742
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mDialog:Landroid/app/Dialog;

    .line 1743
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    .line 1744
    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mDialog:Landroid/app/Dialog;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1745
    const/4 v3, 0x1

    sput-boolean v3, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->isDeleteDialogShowing:Z

    .line 1747
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v1    # "delMsg":Ljava/lang/String;
    .end local v2    # "title":Ljava/lang/String;
    :cond_2
    return-void

    .line 1672
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1675
    .restart local v2    # "title":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    .line 1676
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0091

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public showToast(I)V
    .locals 3
    .param p1, "resId"    # I

    .prologue
    .line 1591
    const/4 v0, 0x0

    .line 1592
    .local v0, "text":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 1593
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1594
    :cond_0
    if-nez v0, :cond_1

    .line 1595
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "showEmptyToast, resourceNotFound exception"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1605
    :goto_0
    return-void

    .line 1598
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mToast:Landroid/widget/Toast;

    if-nez v1, :cond_3

    .line 1599
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mToast:Landroid/widget/Toast;

    .line 1604
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1601
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_2

    .line 1602
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mToast:Landroid/widget/Toast;

    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public showVoiceNotification(Z)V
    .locals 11
    .param p1, "forced"    # Z

    .prologue
    const/4 v7, 0x0

    const v10, 0x5411111

    const/4 v6, 0x1

    const-wide/16 v4, 0x3e8

    const-wide/16 v8, 0x3c

    .line 468
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    if-nez v0, :cond_0

    .line 469
    invoke-direct {p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->createRemoteView()Landroid/widget/RemoteViews;

    .line 471
    :cond_0
    invoke-direct {p0, v6}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateControlReceiver(Z)V

    .line 472
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 473
    iget-wide v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->notificationTime:J

    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getCurRecordingTime()J

    move-result-wide v2

    div-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    if-eqz p1, :cond_3

    .line 474
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getCurRecordingTime()J

    move-result-wide v0

    div-long/2addr v0, v4

    iput-wide v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->notificationTime:J

    .line 475
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    const v1, 0x7f0e0067

    .line 476
    const-string v2, "%02d:%02d:%02d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->notificationTime:J

    div-long/2addr v4, v8

    div-long/2addr v4, v8

    rem-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v7

    .line 477
    iget-wide v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->notificationTime:J

    div-long/2addr v4, v8

    rem-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    iget-wide v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->notificationTime:J

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    .line 475
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 478
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mNotificationManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_2

    .line 479
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceNotification:Landroid/app/Notification;

    invoke-virtual {v0, v10, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 491
    :cond_2
    :goto_0
    return-void

    .line 481
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 482
    iget-wide v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->notificationTime:J

    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getCurrentPlayPosition()J

    move-result-wide v2

    div-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    if-eqz p1, :cond_5

    .line 483
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->getCurrentPlayPosition()J

    move-result-wide v0

    div-long/2addr v0, v4

    iput-wide v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->notificationTime:J

    .line 484
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRemoteViews:Landroid/widget/RemoteViews;

    const v1, 0x7f0e0067

    .line 485
    const-string v2, "%02d:%02d:%02d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->notificationTime:J

    div-long/2addr v4, v8

    div-long/2addr v4, v8

    rem-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v7

    .line 486
    iget-wide v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->notificationTime:J

    div-long/2addr v4, v8

    rem-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    iget-wide v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->notificationTime:J

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    .line 484
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 487
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceNotification:Landroid/app/Notification;

    invoke-virtual {v0, v10, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    .line 488
    :cond_5
    if-eqz p1, :cond_2

    .line 489
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceNotification:Landroid/app/Notification;

    invoke-virtual {v0, v10, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public updateCompletionTimerView()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x3c

    .line 1119
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    if-eqz v6, :cond_0

    .line 1120
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getProgress()I

    move-result v6

    const/16 v7, 0x3e8

    if-ge v6, v7, :cond_0

    .line 1122
    const-string v1, "%02d:%02d:%02d"

    .line 1124
    .local v1, "hourFormat":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-nez v6, :cond_1

    const-wide/16 v6, 0x0

    :goto_0
    const-wide/16 v8, 0x3e8

    div-long v4, v6, v8

    .line 1125
    .local v4, "totalTime":J
    const-string v0, "%02d:%02d:%02d"

    .line 1126
    .local v0, "format":Ljava/lang/String;
    const-string v6, "%02d:%02d:%02d"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 1127
    div-long v10, v4, v12

    div-long/2addr v10, v12

    rem-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    div-long v10, v4, v12

    rem-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    rem-long v10, v4, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    .line 1126
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .local v2, "timeStringEnd":Ljava/lang/String;
    move-object v3, v2

    .line 1128
    .local v3, "timeStringStart":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    iget-object v7, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getMax()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1129
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingStartDuration:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1130
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingEndDuration:Landroid/widget/TextView;

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1132
    .end local v0    # "format":Ljava/lang/String;
    .end local v1    # "hourFormat":Ljava/lang/String;
    .end local v2    # "timeStringEnd":Ljava/lang/String;
    .end local v3    # "timeStringStart":Ljava/lang/String;
    .end local v4    # "totalTime":J
    :cond_0
    return-void

    .line 1124
    .restart local v1    # "hourFormat":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-wide v6, v6, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTotalTime:J

    goto :goto_0
.end method

.method public updateTimerView()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 436
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v4, :cond_0

    iget-wide v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->voiceRecordingTime:J

    iget-object v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-wide v6, v6, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTotalTime:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 437
    iget-wide v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->voiceRecordingTime:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 438
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    iget-wide v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->voiceRecordingTime:J

    iput-wide v6, v4, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTotalTime:J

    .line 439
    :cond_0
    const/4 v3, 0x0

    .local v3, "timeString_played":Ljava/lang/String;
    const/4 v2, 0x0

    .line 440
    .local v2, "timeString_end":Ljava/lang/String;
    const-string v1, "%02d:%02d:%02d"

    .line 442
    .local v1, "hourFormat":Ljava/lang/String;
    const/16 v0, 0x64

    .line 444
    .local v0, "delayTime":I
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 445
    const-string v4, "%02d:%02d:%02d"

    invoke-direct {p0, v4, v3}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setRecordingDuration(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    invoke-static {}, Lcom/samsung/android/app/memo/MemoApp;->isShowNotificationEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 447
    invoke-virtual {p0, v8}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showVoiceNotification(Z)V

    .line 462
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowRecording()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 463
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTime:Ljava/lang/Runnable;

    int-to-long v6, v0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 465
    :cond_3
    return-void

    .line 450
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->isNowPlaying()Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;

    if-eqz v4, :cond_6

    .line 451
    invoke-static {}, Lcom/samsung/android/app/memo/MemoApp;->isShowNotificationEnabled()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 452
    invoke-virtual {p0, v8}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showVoiceNotification(Z)V

    .line 454
    :cond_5
    const-string v4, "%02d:%02d:%02d"

    invoke-direct {p0, v4, v3, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setCurrentProgress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 457
    :cond_6
    const-string v4, "%02d:%02d:%02d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 458
    const-string v4, "%02d:%02d:%02d"

    invoke-direct {p0, v3, v4, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setPlayingDuration(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
