.class Lcom/samsung/android/app/memo/voice/VoiceViewLayout$17;
.super Ljava/lang/Object;
.source "VoiceViewLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->handleRecordPause()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$17;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 1409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1412
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$17;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingPauseButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$15(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;

    move-result-object v0

    const v1, 0x7f0200ae

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1413
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$17;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mRecordingPauseButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$15(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$17;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1414
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$17;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseRecording()V

    .line 1415
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$17;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD_PAUSE:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$2(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;)V

    .line 1416
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$17;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->enableSystemSound()V

    .line 1417
    return-void
.end method
