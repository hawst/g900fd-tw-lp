.class Lcom/samsung/android/app/memo/print/GooglePrintHtml$1;
.super Landroid/webkit/WebViewClient;
.source "GooglePrintHtml.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/print/GooglePrintHtml;->doWebViewPrint(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/print/GooglePrintHtml;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/print/GooglePrintHtml;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintHtml;

    .line 57
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 64
    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintHtml;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->access$0()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "page finished loading "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/samsung/android/app/memo/print/GooglePrintHtml$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintHtml;

    # invokes: Lcom/samsung/android/app/memo/print/GooglePrintHtml;->createWebPrintJob()V
    invoke-static {v0}, Lcom/samsung/android/app/memo/print/GooglePrintHtml;->access$1(Lcom/samsung/android/app/memo/print/GooglePrintHtml;)V

    .line 66
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method
