.class Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "CategoryManagementFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/samsung/android/app/memo/uifragment/CategoryListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field delCategoryDialog:Landroid/app/AlertDialog;

.field private inflater:Landroid/view/LayoutInflater;

.field public isDraggingEnabled:Z

.field private isdelDialogshow:Z

.field mCategoryManagementFragment:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

.field mContext:Landroid/content/Context;

.field private mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

.field private viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 620
    const-class v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/memo/uifragment/CategoryListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "lists":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/memo/uifragment/CategoryListItem;>;"
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 640
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 622
    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    .line 624
    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 630
    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mContext:Landroid/content/Context;

    .line 632
    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->delCategoryDialog:Landroid/app/AlertDialog;

    .line 634
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->isdelDialogshow:Z

    .line 636
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->isDraggingEnabled:Z

    .line 641
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 643
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    invoke-direct {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mCategoryManagementFragment:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    .line 644
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mContext:Landroid/content/Context;

    .line 645
    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;)Lcom/samsung/android/app/memo/uiwidget/DndListView;
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;)Z
    .locals 1

    .prologue
    .line 634
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->isdelDialogshow:Z

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;Z)V
    .locals 0

    .prologue
    .line 634
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->isdelDialogshow:Z

    return-void
.end method


# virtual methods
.method public delete(I)V
    .locals 6
    .param p1, "pos"    # I

    .prologue
    const v5, 0x7f0b0002

    .line 717
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "position"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    iget-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->isdelDialogshow:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->isDraggingEnabled:Z

    if-eqz v2, :cond_1

    .line 796
    :cond_0
    :goto_0
    return-void

    .line 722
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->isdelDialogshow:Z

    .line 724
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 726
    .local v0, "delCategory":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b001f

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 727
    new-instance v3, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;

    invoke-direct {v3, p0, p1}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$3;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;I)V

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 776
    const v3, 0x7f0b0005

    new-instance v4, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$4;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$4;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 783
    new-instance v3, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$5;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$5;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 790
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->delCategoryDialog:Landroid/app/AlertDialog;

    .line 791
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->delCategoryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 792
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->delCategoryDialog:Landroid/app/AlertDialog;

    const v3, 0x102000b

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 793
    .local v1, "messageView":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 794
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0
.end method

.method public dismissDeleteDialog()V
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->delCategoryDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 711
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->delCategoryDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 713
    :cond_0
    return-void
.end method

.method public getItem(I)Lcom/samsung/android/app/memo/uifragment/CategoryListItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 649
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->getCount()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 650
    :cond_0
    const/4 v0, 0x0

    .line 652
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->getItem(I)Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 658
    move-object v0, p2

    .line 660
    .local v0, "v":Landroid/view/View;
    if-nez v0, :cond_1

    .line 661
    new-instance v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    invoke-direct {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    .line 662
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f04000c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 663
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    .line 664
    const v1, 0x7f0e000e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 663
    iput-object v1, v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;->categoryManagementItem:Landroid/widget/TextView;

    .line 665
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    .line 666
    const v1, 0x7f0e000d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 665
    iput-object v1, v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;->imageViewChangeOrder:Landroid/widget/ImageView;

    .line 667
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    .line 668
    const v1, 0x7f0e000f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 667
    iput-object v1, v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;->deleteIcon:Landroid/widget/ImageView;

    .line 669
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 674
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;->categoryManagementItem:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 675
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;->categoryManagementItem:Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->getItem(I)Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->getCategoryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 676
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;->categoryManagementItem:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 679
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;->imageViewChangeOrder:Landroid/widget/ImageView;

    new-instance v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$1;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 690
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;->imageViewChangeOrder:Landroid/widget/ImageView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 692
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;->deleteIcon:Landroid/widget/ImageView;

    new-instance v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$2;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 702
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;->deleteIcon:Landroid/widget/ImageView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setHoverPopupType(I)V

    .line 703
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    iget-object v1, v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;->deleteIcon:Landroid/widget/ImageView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 705
    return-object v0

    .line 671
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->viewHolder:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter$ViewHolder;

    goto :goto_0
.end method

.method public setListView(Lcom/samsung/android/app/memo/uiwidget/DndListView;)V
    .locals 0
    .param p1, "listView"    # Lcom/samsung/android/app/memo/uiwidget/DndListView;

    .prologue
    .line 808
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    .line 809
    return-void
.end method
