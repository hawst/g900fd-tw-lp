.class public Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;
.super Ljava/lang/Object;
.source "AsyncFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$NetworkRequests;,
        Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;,
        Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;
    }
.end annotation


# static fields
.field private static mAsyncFactory:Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mNetworkUtil:Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;

.field private mtask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;",
            "Ljava/lang/Integer;",
            "Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Response;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->mContext:Landroid/content/Context;

    .line 43
    new-instance v0, Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->mNetworkUtil:Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;

    .line 44
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;)Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->mNetworkUtil:Lcom/samsung/android/app/migration/snbdownload/network/util/NetworkUtil;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getAsyncFactory(Landroid/content/Context;)Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    sget-object v0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->mAsyncFactory:Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->mAsyncFactory:Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;

    .line 49
    :cond_0
    sget-object v0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->mAsyncFactory:Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;

    return-object v0
.end method


# virtual methods
.method public endtask()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->mtask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->mtask:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 66
    :cond_0
    return-void
.end method

.method public executeURLs(Ljava/lang/String;IILjava/lang/String;)V
    .locals 8
    .param p1, "mURL"    # Ljava/lang/String;
    .param p2, "mFunction"    # I
    .param p3, "mReqtype"    # I
    .param p4, "jsonstring"    # Ljava/lang/String;

    .prologue
    .line 53
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/ConnectivityManager;

    .line 54
    .local v6, "connMgr":Landroid/net/ConnectivityManager;
    invoke-virtual {v6}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v7

    .line 55
    .local v7, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;->getNetworkServiceInstance()Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    move-result-object v1

    invoke-virtual {v7}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    iput v2, v1, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->mNetworktype:I

    .line 57
    new-instance v0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;

    const-string v3, "application/json;Charset=utf-8"

    move-object v1, p1

    move-object v2, p4

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 58
    .local v0, "request":Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;
    new-instance v1, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$NetworkRequests;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$NetworkRequests;-><init>(Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$NetworkRequests;)V

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$NetworkRequests;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory;->mtask:Landroid/os/AsyncTask;

    .line 60
    .end local v0    # "request":Lcom/samsung/android/app/migration/snbdownload/network/AsyncFactory$Request;
    :cond_0
    return-void
.end method
