.class public Lcom/samsung/android/app/memo/util/ImageLoader;
.super Ljava/lang/Object;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/util/ImageLoader$AsyncDrawable;,
        Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;,
        Lcom/samsung/android/app/memo/util/ImageLoader$CacheAsyncTask;,
        Lcom/samsung/android/app/memo/util/ImageLoader$UpdateThumbImageReceiver;
    }
.end annotation


# static fields
.field private static final MESSAGE_CLEAR:I = 0x0

.field private static final MESSAGE_CLOSE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ImageLoader"

.field public static final _ACTION:Ljava/lang/String; = "com.samsung.android.app.memo.UPDATE_THUMB_IMAGE"

.field public static final _DATA:Ljava/lang/String; = "_data"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mImageCache:Lcom/samsung/android/app/memo/util/ImageCache;

.field private mImageCacheParams:Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;

.field private mLoadingBitmap:Landroid/graphics/Bitmap;

.field protected mPauseWork:Z

.field private final mPauseWorkLock:Ljava/lang/Object;

.field private mReciever:Lcom/samsung/android/app/memo/util/ImageLoader$UpdateThumbImageReceiver;

.field protected mResources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mPauseWork:Z

    .line 61
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mPauseWorkLock:Ljava/lang/Object;

    .line 90
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mContext:Landroid/content/Context;

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mResources:Landroid/content/res/Resources;

    .line 92
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.samsung.android.app.memo.UPDATE_THUMB_IMAGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 93
    .local v0, "filter":Landroid/content/IntentFilter;
    new-instance v1, Lcom/samsung/android/app/memo/util/ImageLoader$UpdateThumbImageReceiver;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/util/ImageLoader$UpdateThumbImageReceiver;-><init>(Lcom/samsung/android/app/memo/util/ImageLoader;)V

    iput-object v1, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mReciever:Lcom/samsung/android/app/memo/util/ImageLoader$UpdateThumbImageReceiver;

    .line 94
    iget-object v1, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mReciever:Lcom/samsung/android/app/memo/util/ImageLoader$UpdateThumbImageReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 96
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/util/ImageLoader;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mPauseWorkLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/util/ImageLoader;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 248
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/util/ImageLoader;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/util/ImageLoader;)Lcom/samsung/android/app/memo/util/ImageCache;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mImageCache:Lcom/samsung/android/app/memo/util/ImageCache;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/util/ImageLoader;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 270
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/memo/util/ImageLoader;->setImageBitmap(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$4(Landroid/widget/ImageView;)Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;
    .locals 1

    .prologue
    .line 166
    invoke-static {p0}, Lcom/samsung/android/app/memo/util/ImageLoader;->getBitmapWorkerTask(Landroid/widget/ImageView;)Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;

    move-result-object v0

    return-object v0
.end method

.method public static cancelPotentialWork(Ljava/lang/String;Landroid/widget/ImageView;)Z
    .locals 5
    .param p0, "data"    # Ljava/lang/String;
    .param p1, "imageView"    # Landroid/widget/ImageView;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 150
    invoke-static {p1}, Lcom/samsung/android/app/memo/util/ImageLoader;->getBitmapWorkerTask(Landroid/widget/ImageView;)Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;

    move-result-object v1

    .line 151
    .local v1, "bitmapWorkerTask":Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;
    if-eqz v1, :cond_3

    .line 152
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v2

    .line 155
    :cond_1
    # getter for: Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->mPath:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->access$3(Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;)Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, "bitmapData":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 157
    :cond_2
    invoke-virtual {v1, v3}, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->cancel(Z)Z

    .end local v0    # "bitmapData":Ljava/lang/String;
    :cond_3
    move v2, v3

    .line 163
    goto :goto_0
.end method

.method public static cancelWork(Landroid/widget/ImageView;)V
    .locals 2
    .param p0, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 143
    invoke-static {p0}, Lcom/samsung/android/app/memo/util/ImageLoader;->getBitmapWorkerTask(Landroid/widget/ImageView;)Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;

    move-result-object v0

    .line 144
    .local v0, "bitmapWorkerTask":Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;
    if-eqz v0, :cond_0

    .line 145
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->cancel(Z)Z

    .line 147
    :cond_0
    return-void
.end method

.method private decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 249
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 250
    const/4 v1, 0x0

    .line 254
    :goto_0
    return-object v1

    .line 251
    :cond_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 252
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 253
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 254
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method private static getBitmapWorkerTask(Landroid/widget/ImageView;)Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;
    .locals 3
    .param p0, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 167
    if-eqz p0, :cond_0

    .line 168
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 169
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    instance-of v2, v1, Lcom/samsung/android/app/memo/util/ImageLoader$AsyncDrawable;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 170
    check-cast v0, Lcom/samsung/android/app/memo/util/ImageLoader$AsyncDrawable;

    .line 171
    .local v0, "asyncDrawable":Lcom/samsung/android/app/memo/util/ImageLoader$AsyncDrawable;
    invoke-virtual {v0}, Lcom/samsung/android/app/memo/util/ImageLoader$AsyncDrawable;->getBitmapWorkerTask()Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;

    move-result-object v2

    .line 174
    .end local v0    # "asyncDrawable":Lcom/samsung/android/app/memo/util/ImageLoader$AsyncDrawable;
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private setImageBitmap(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 271
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 272
    return-void
.end method


# virtual methods
.method public addImageCache(Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;)V
    .locals 1
    .param p1, "cacheParams"    # Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mImageCacheParams:Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;

    .line 134
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mImageCacheParams:Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/ImageCache;->findOrCreateCache(Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;)Lcom/samsung/android/app/memo/util/ImageCache;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/util/ImageLoader;->setImageCache(Lcom/samsung/android/app/memo/util/ImageCache;)V

    .line 135
    return-void
.end method

.method public clearCache()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 317
    new-instance v0, Lcom/samsung/android/app/memo/util/ImageLoader$CacheAsyncTask;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/util/ImageLoader$CacheAsyncTask;-><init>(Lcom/samsung/android/app/memo/util/ImageLoader;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/util/ImageLoader$CacheAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 318
    return-void
.end method

.method protected clearCacheInternal()V
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mImageCache:Lcom/samsung/android/app/memo/util/ImageCache;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mImageCache:Lcom/samsung/android/app/memo/util/ImageCache;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/util/ImageCache;->clearCache()V

    .line 314
    :cond_0
    return-void
.end method

.method public closeCache()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 307
    new-instance v0, Lcom/samsung/android/app/memo/util/ImageLoader$CacheAsyncTask;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/util/ImageLoader$CacheAsyncTask;-><init>(Lcom/samsung/android/app/memo/util/ImageLoader;)V

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/util/ImageLoader$CacheAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 308
    return-void
.end method

.method protected closeCacheInternal()V
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mImageCache:Lcom/samsung/android/app/memo/util/ImageCache;

    if-eqz v0, :cond_0

    .line 302
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mImageCache:Lcom/samsung/android/app/memo/util/ImageCache;

    .line 304
    :cond_0
    return-void
.end method

.method public loadImage(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 7
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 99
    if-nez p1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    const/4 v1, 0x0

    .line 105
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mImageCache:Lcom/samsung/android/app/memo/util/ImageCache;

    if-eqz v4, :cond_2

    .line 106
    iget-object v4, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mImageCache:Lcom/samsung/android/app/memo/util/ImageCache;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/memo/util/ImageCache;->getBitmapFromMemCache(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 109
    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_3

    .line 111
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 112
    :cond_3
    invoke-static {p1, p2}, Lcom/samsung/android/app/memo/util/ImageLoader;->cancelPotentialWork(Ljava/lang/String;Landroid/widget/ImageView;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 113
    new-instance v3, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;

    invoke-direct {v3, p0, p2, p1}, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;-><init>(Lcom/samsung/android/app/memo/util/ImageLoader;Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 114
    .local v3, "task":Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;
    new-instance v0, Lcom/samsung/android/app/memo/util/ImageLoader$AsyncDrawable;

    iget-object v4, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mResources:Landroid/content/res/Resources;

    iget-object v5, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mLoadingBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v4, v5, v3}, Lcom/samsung/android/app/memo/util/ImageLoader$AsyncDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;)V

    .line 115
    .local v0, "asyncDrawable":Lcom/samsung/android/app/memo/util/ImageLoader$AsyncDrawable;
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 117
    :try_start_0
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_4

    .line 118
    sget-object v4, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 122
    :catch_0
    move-exception v2

    .line 123
    .local v2, "e":Ljava/util/concurrent/RejectedExecutionException;
    const-string v4, "ImageLoader"

    const-string v5, "loadImage "

    invoke-static {v4, v5, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 120
    .end local v2    # "e":Ljava/util/concurrent/RejectedExecutionException;
    :cond_4
    const/4 v4, 0x0

    :try_start_1
    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/util/ImageLoader$BitmapWorkerTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_1
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public removeCache(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 330
    const-string v0, "ImageLoader"

    const-string v1, "removeCache()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mImageCache:Lcom/samsung/android/app/memo/util/ImageCache;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/memo/util/ImageCache;->removeBitmapFromCache(Ljava/lang/String;)V

    .line 332
    return-void
.end method

.method public setImageCache(Lcom/samsung/android/app/memo/util/ImageCache;)V
    .locals 0
    .param p1, "imageCache"    # Lcom/samsung/android/app/memo/util/ImageCache;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mImageCache:Lcom/samsung/android/app/memo/util/ImageCache;

    .line 139
    return-void
.end method

.method public setLoadingImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mLoadingBitmap:Landroid/graphics/Bitmap;

    .line 130
    return-void
.end method

.method public setPauseWork(Z)V
    .locals 2
    .param p1, "pauseWork"    # Z

    .prologue
    .line 275
    iget-object v1, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mPauseWorkLock:Ljava/lang/Object;

    monitor-enter v1

    .line 276
    :try_start_0
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mPauseWork:Z

    .line 277
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mPauseWork:Z

    if-nez v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mPauseWorkLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 275
    :cond_0
    monitor-exit v1

    .line 281
    return-void

    .line 275
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unRegisteredReciever()V
    .locals 3

    .prologue
    .line 322
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mReciever:Lcom/samsung/android/app/memo/util/ImageLoader$UpdateThumbImageReceiver;

    if-eqz v1, :cond_0

    .line 323
    iget-object v1, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/app/memo/util/ImageLoader;->mReciever:Lcom/samsung/android/app/memo/util/ImageLoader$UpdateThumbImageReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 324
    :catch_0
    move-exception v0

    .line 325
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "ImageLoader"

    const-string v2, "unRegisteredReciever "

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
