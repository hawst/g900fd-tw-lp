.class public abstract Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;
.super Ljava/lang/Object;
.source "AbstractServiceManager.java"


# static fields
.field private static final RCODE:Ljava/lang/String; = "rcode"

.field private static final TAG:Ljava/lang/String; = "AbstractServiceManager"


# instance fields
.field protected mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

.field protected mCid:Ljava/lang/String;

.field protected mClientDeviceId:Ljava/lang/String;

.field private mNetClient:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "authManager"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;
    .param p3, "cid"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mCid:Ljava/lang/String;

    .line 61
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mNetClient:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;

    .line 62
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    .line 63
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mClientDeviceId:Ljava/lang/String;

    .line 66
    iput-object p2, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    .line 67
    iput-object p3, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mCid:Ljava/lang/String;

    .line 68
    new-instance v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;

    invoke-direct {v0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mNetClient:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "authManager"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;
    .param p3, "cid"    # Ljava/lang/String;
    .param p4, "clientDeviceId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mCid:Ljava/lang/String;

    .line 61
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mNetClient:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;

    .line 62
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    .line 63
    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mClientDeviceId:Ljava/lang/String;

    .line 73
    iput-object p2, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    .line 74
    iput-object p3, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mCid:Ljava/lang/String;

    .line 75
    new-instance v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;

    invoke-direct {v0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mNetClient:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;

    .line 76
    iput-object p4, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mClientDeviceId:Ljava/lang/String;

    .line 77
    return-void
.end method

.method private createMetaResponse(Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;)V
    .locals 3
    .param p1, "networkResponse"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    .param p2, "metaResponse"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;->clear()V

    .line 87
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;->getBody()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 88
    invoke-virtual {p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;->fromJSON(Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 95
    :cond_0
    return-void

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Lorg/json/JSONException;
    const-string v1, "AbstractServiceManager"

    const-string v2, "JSON PARSER Exception. "

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 91
    new-instance v1, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v1

    .line 92
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 93
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    new-instance v1, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v1
.end method

.method private httpStatusResponse(Ljava/lang/String;I)V
    .locals 6
    .param p1, "responseBody"    # Ljava/lang/String;
    .param p2, "status"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
        }
    .end annotation

    .prologue
    .line 152
    sparse-switch p2, :sswitch_data_0

    .line 174
    const-string v3, "AbstractServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "HTTPEXCEPTION : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    if-eqz p1, :cond_0

    .line 177
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 178
    .local v1, "json":Lorg/json/JSONObject;
    const-string v3, "rcode"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 179
    const-string v3, "rcode"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 180
    .local v2, "rcode":I
    const-string v3, "AbstractServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "HTTPEXCEPTION, rcode is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 186
    .end local v1    # "json":Lorg/json/JSONObject;
    .end local v2    # "rcode":I
    :cond_0
    :goto_0
    new-instance v3, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v3

    .line 157
    :sswitch_0
    if-eqz p1, :cond_1

    .line 159
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 160
    .restart local v1    # "json":Lorg/json/JSONObject;
    const-string v3, "rcode"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 161
    const-string v3, "rcode"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 162
    .restart local v2    # "rcode":I
    const-string v3, "AbstractServiceManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SC_BAD_REQUEST, rcode is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const/16 v3, 0x4a40

    if-ne v3, v2, :cond_1

    .line 164
    new-instance v3, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v4, 0x7

    invoke-direct {v3, v4}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 167
    .end local v1    # "json":Lorg/json/JSONObject;
    .end local v2    # "rcode":I
    :catch_0
    move-exception v0

    .line 168
    .local v0, "e":Lorg/json/JSONException;
    const-string v3, "AbstractServiceManager"

    const-string v4, "JSON PARSER Exception. "

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 169
    new-instance v3, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v3

    .line 182
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v0

    .line 183
    .restart local v0    # "e":Lorg/json/JSONException;
    const-string v3, "AbstractServiceManager"

    const-string v4, "JSON PARSER Exception."

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 188
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_1
    :sswitch_1
    return-void

    .line 152
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_1
        0x190 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mNetClient:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->close()V

    .line 81
    return-void
.end method

.method protected handleRequest(ILjava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    .locals 6
    .param p1, "requestType"    # I
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "responseMeta"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 99
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->handleRequest(ILjava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartEntity;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    move-result-object v0

    return-object v0
.end method

.method protected handleRequest(ILjava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartEntity;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    .locals 8
    .param p1, "requestType"    # I
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "responseMeta"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;
    .param p4, "Json"    # Ljava/lang/String;
    .param p5, "multipart"    # Lorg/apache/http/entity/mime/MultipartEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
        }
    .end annotation

    .prologue
    .line 106
    const/4 v1, 0x0

    .line 107
    .local v1, "networkResponse":Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mNetClient:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;

    monitor-enter v5

    .line 109
    packed-switch p1, :pswitch_data_0

    .line 131
    :pswitch_0
    :try_start_0
    const-string v4, "AbstractServiceManager"

    const-string v6, "Wrong Request."

    invoke-static {v4, v6}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    new-instance v4, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v6, 0x6

    invoke-direct {v4, v6}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    const-string v4, "AbstractServiceManager"

    const-string v6, "IOException occured while Http Request."

    invoke-static {v4, v6, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 136
    new-instance v4, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v6, 0x4

    invoke-direct {v4, v6}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v4

    .line 107
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 111
    :pswitch_1
    if-eqz p4, :cond_0

    .line 112
    :try_start_2
    const-string v4, "AbstractServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Getting Headers : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mNetClient:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;

    invoke-virtual {v4, p2, p4}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->post(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 138
    :goto_0
    if-nez v1, :cond_1

    .line 139
    :try_start_3
    const-string v4, "AbstractServiceManager"

    const-string v6, "There is no Network Response."

    invoke-static {v4, v6}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    new-instance v4, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/16 v6, 0x9

    invoke-direct {v4, v6}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 117
    :pswitch_2
    :try_start_4
    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mNetClient:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;

    invoke-virtual {v4, p2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->get(Ljava/lang/String;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    move-result-object v1

    .line 118
    goto :goto_0

    .line 120
    :pswitch_3
    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mNetClient:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;

    invoke-virtual {v4, p2, p5}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->postMultiPart(Ljava/lang/String;Lorg/apache/http/entity/mime/MultipartEntity;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    move-result-object v1

    .line 121
    goto :goto_0

    .line 123
    :pswitch_4
    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mNetClient:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;

    invoke-virtual {v4, p2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->getFile(Ljava/lang/String;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;

    move-result-object v1

    .line 124
    goto :goto_0

    .line 126
    :pswitch_5
    iget-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->mNetClient:Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;

    .line 127
    const-string v6, "Accept"

    .line 128
    const-string v7, "application/json"

    .line 126
    invoke-virtual {v4, p2, v6, v7}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkClient;->getWithHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v1

    .line 129
    goto :goto_0

    .line 142
    :cond_1
    :try_start_5
    invoke-virtual {v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 143
    .local v2, "responseBody":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;->getStatus()I

    move-result v3

    .line 144
    .local v3, "status":I
    invoke-direct {p0, v2, v3}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->httpStatusResponse(Ljava/lang/String;I)V

    .line 107
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 146
    invoke-direct {p0, v1, p3}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/AbstractServiceManager;->createMetaResponse(Lcom/samsung/android/app/migration/snbdownload/network/downsync/http/NetworkResponse;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/AbstractJSON;)V

    .line 147
    return-object v1

    .line 109
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
