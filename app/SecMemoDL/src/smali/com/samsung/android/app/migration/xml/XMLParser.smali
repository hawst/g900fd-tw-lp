.class public Lcom/samsung/android/app/migration/xml/XMLParser;
.super Ljava/lang/Object;
.source "XMLParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "XMLParser"

.field public static final XML_CONTENT_FILE:Ljava/lang/String; = "/snote/snote.xml"

.field public static final XML_SETTINGS_FILE:Ljava/lang/String; = "/snote/settings.xml"

.field private static instance:Lcom/samsung/android/app/migration/xml/XMLParser;


# instance fields
.field private content:Ljava/lang/String;

.field private createdTime:J

.field private modifiedTime:J

.field private templateType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/migration/xml/XMLParser;->instance:Lcom/samsung/android/app/migration/xml/XMLParser;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/samsung/android/app/migration/xml/XMLParser;->instance:Lcom/samsung/android/app/migration/xml/XMLParser;

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Lcom/samsung/android/app/migration/xml/XMLParser;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/xml/XMLParser;-><init>()V

    sput-object v0, Lcom/samsung/android/app/migration/xml/XMLParser;->instance:Lcom/samsung/android/app/migration/xml/XMLParser;

    .line 64
    :cond_0
    sget-object v0, Lcom/samsung/android/app/migration/xml/XMLParser;->instance:Lcom/samsung/android/app/migration/xml/XMLParser;

    return-object v0
.end method


# virtual methods
.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser;->content:Ljava/lang/String;

    return-object v0
.end method

.method public getCreatedTime()J
    .locals 2

    .prologue
    .line 130
    iget-wide v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser;->createdTime:J

    return-wide v0
.end method

.method public getModifiedTime()J
    .locals 2

    .prologue
    .line 134
    iget-wide v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser;->modifiedTime:J

    return-wide v0
.end method

.method public getTemplateType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/android/app/migration/xml/XMLParser;->templateType:Ljava/lang/String;

    return-object v0
.end method

.method public parseXMLContent(Ljava/lang/String;)V
    .locals 11
    .param p1, "folder"    # Ljava/lang/String;

    .prologue
    .line 68
    const/4 v2, 0x0

    .line 70
    .local v2, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v6

    .line 71
    .local v6, "spf":Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v6}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v5

    .line 72
    .local v5, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v5}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v7

    .line 73
    .local v7, "xr":Lorg/xml/sax/XMLReader;
    new-instance v4, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;

    const/4 v8, 0x0

    invoke-direct {v4, v8}, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;-><init>(Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;)V

    .line 74
    .local v4, "myXMLHandler":Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;
    invoke-interface {v7, v4}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 75
    new-instance v3, Ljava/io/FileInputStream;

    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "/snote/snote.xml"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    .end local v2    # "is":Ljava/io/InputStream;
    .local v3, "is":Ljava/io/InputStream;
    :try_start_1
    new-instance v1, Lorg/xml/sax/InputSource;

    invoke-direct {v1, v3}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    .line 77
    .local v1, "inStream":Lorg/xml/sax/InputSource;
    invoke-interface {v7, v1}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 78
    invoke-virtual {v4}, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->getContent()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/app/migration/xml/XMLParser;->content:Ljava/lang/String;
    :try_end_1
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 88
    if-eqz v3, :cond_2

    .line 90
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_9

    move-object v2, v3

    .line 95
    .end local v1    # "inStream":Lorg/xml/sax/InputSource;
    .end local v3    # "is":Ljava/io/InputStream;
    .end local v4    # "myXMLHandler":Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;
    .end local v5    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v6    # "spf":Ljavax/xml/parsers/SAXParserFactory;
    .end local v7    # "xr":Lorg/xml/sax/XMLReader;
    .restart local v2    # "is":Ljava/io/InputStream;
    :cond_0
    :goto_0
    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Lorg/xml/sax/SAXException;
    :goto_1
    :try_start_3
    const-string v8, "XMLParser"

    const-string v9, "Exception during SAX initialization."

    invoke-static {v8, v9, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 88
    if-eqz v2, :cond_0

    .line 90
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 91
    :catch_1
    move-exception v0

    .line 92
    .local v0, "e":Ljava/io/IOException;
    const-string v8, "XMLParser"

    const-string v9, "Exception during closing InputStream."

    invoke-static {v8, v9, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 81
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 82
    .local v0, "e":Ljavax/xml/parsers/ParserConfigurationException;
    :goto_2
    :try_start_5
    const-string v8, "XMLParser"

    const-string v9, "Exception during SAX initialization."

    invoke-static {v8, v9, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 88
    if-eqz v2, :cond_0

    .line 90
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 91
    :catch_3
    move-exception v0

    .line 92
    .local v0, "e":Ljava/io/IOException;
    const-string v8, "XMLParser"

    const-string v9, "Exception during closing InputStream."

    invoke-static {v8, v9, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 83
    .end local v0    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v0

    .line 84
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_7
    const-string v8, "XMLParser"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "/snote/snote.xml"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " was not found."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 88
    if-eqz v2, :cond_0

    .line 90
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_0

    .line 91
    :catch_5
    move-exception v0

    .line 92
    .local v0, "e":Ljava/io/IOException;
    const-string v8, "XMLParser"

    const-string v9, "Exception during closing InputStream."

    invoke-static {v8, v9, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 85
    .end local v0    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v0

    .line 86
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_9
    const-string v8, "XMLParser"

    const-string v9, "Exception during XML parsing."

    invoke-static {v8, v9, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 88
    if-eqz v2, :cond_0

    .line 90
    :try_start_a
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    goto :goto_0

    .line 91
    :catch_7
    move-exception v0

    .line 92
    const-string v8, "XMLParser"

    const-string v9, "Exception during closing InputStream."

    invoke-static {v8, v9, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 87
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 88
    :goto_5
    if-eqz v2, :cond_1

    .line 90
    :try_start_b
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    .line 94
    :cond_1
    :goto_6
    throw v8

    .line 91
    :catch_8
    move-exception v0

    .line 92
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v9, "XMLParser"

    const-string v10, "Exception during closing InputStream."

    invoke-static {v9, v10, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 91
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "inStream":Lorg/xml/sax/InputSource;
    .restart local v3    # "is":Ljava/io/InputStream;
    .restart local v4    # "myXMLHandler":Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;
    .restart local v5    # "sp":Ljavax/xml/parsers/SAXParser;
    .restart local v6    # "spf":Ljavax/xml/parsers/SAXParserFactory;
    .restart local v7    # "xr":Lorg/xml/sax/XMLReader;
    :catch_9
    move-exception v0

    .line 92
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v8, "XMLParser"

    const-string v9, "Exception during closing InputStream."

    invoke-static {v8, v9, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    move-object v2, v3

    .end local v3    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    goto/16 :goto_0

    .line 87
    .end local v1    # "inStream":Lorg/xml/sax/InputSource;
    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    :catchall_1
    move-exception v8

    move-object v2, v3

    .end local v3    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    goto :goto_5

    .line 85
    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    :catch_a
    move-exception v0

    move-object v2, v3

    .end local v3    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    goto :goto_4

    .line 83
    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    :catch_b
    move-exception v0

    move-object v2, v3

    .end local v3    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    goto :goto_3

    .line 81
    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    :catch_c
    move-exception v0

    move-object v2, v3

    .end local v3    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    goto/16 :goto_2

    .line 79
    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    :catch_d
    move-exception v0

    move-object v2, v3

    .end local v3    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    goto/16 :goto_1
.end method

.method public parseXMLSettings(Ljava/lang/String;)V
    .locals 10
    .param p1, "folder"    # Ljava/lang/String;

    .prologue
    .line 99
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v5

    .line 100
    .local v5, "spf":Ljavax/xml/parsers/SAXParserFactory;
    invoke-virtual {v5}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v4

    .line 101
    .local v4, "sp":Ljavax/xml/parsers/SAXParser;
    invoke-virtual {v4}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v6

    .line 102
    .local v6, "xr":Lorg/xml/sax/XMLReader;
    new-instance v3, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;

    const/4 v7, 0x0

    invoke-direct {v3, v7}, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;-><init>(Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;)V

    .line 103
    .local v3, "myXMLHandler":Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;
    invoke-interface {v6, v3}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 104
    new-instance v2, Ljava/io/FileInputStream;

    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/snote/settings.xml"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 105
    .local v2, "is":Ljava/io/InputStream;
    new-instance v1, Lorg/xml/sax/InputSource;

    invoke-direct {v1, v2}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    .line 106
    .local v1, "inStream":Lorg/xml/sax/InputSource;
    invoke-interface {v6, v1}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 107
    invoke-virtual {v3}, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->getTemplateType()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/app/migration/xml/XMLParser;->templateType:Ljava/lang/String;

    .line 108
    invoke-virtual {v3}, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->getCreatedTime()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/samsung/android/app/migration/xml/XMLParser;->createdTime:J

    .line 109
    invoke-virtual {v3}, Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;->getModifiedTime()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/samsung/android/app/migration/xml/XMLParser;->modifiedTime:J
    :try_end_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 119
    .end local v1    # "inStream":Lorg/xml/sax/InputSource;
    .end local v2    # "is":Ljava/io/InputStream;
    .end local v3    # "myXMLHandler":Lcom/samsung/android/app/migration/xml/XMLParser$XMLHandler;
    .end local v4    # "sp":Ljavax/xml/parsers/SAXParser;
    .end local v5    # "spf":Ljavax/xml/parsers/SAXParserFactory;
    .end local v6    # "xr":Lorg/xml/sax/XMLReader;
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Lorg/xml/sax/SAXException;
    const-string v7, "XMLParser"

    const-string v8, "Exception during SAX initialization."

    invoke-static {v7, v8, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 112
    .end local v0    # "e":Lorg/xml/sax/SAXException;
    :catch_1
    move-exception v0

    .line 113
    .local v0, "e":Ljavax/xml/parsers/ParserConfigurationException;
    const-string v7, "XMLParser"

    const-string v8, "Exception during SAX initialization."

    invoke-static {v7, v8, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 114
    .end local v0    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    :catch_2
    move-exception v0

    .line 115
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v7, "XMLParser"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "/snote/settings.xml"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " was not found."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 116
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v0

    .line 117
    .local v0, "e":Ljava/io/IOException;
    const-string v7, "XMLParser"

    const-string v8, "Exception during XML parsing."

    invoke-static {v7, v8, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
