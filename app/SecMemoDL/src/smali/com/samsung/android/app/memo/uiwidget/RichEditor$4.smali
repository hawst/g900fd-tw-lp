.class Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;
.super Ljava/lang/Object;
.source "RichEditor.java"

# interfaces
.implements Lcom/samsung/android/app/memo/uiwidget/ClipboardService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/RichEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mActivity:Landroid/content/Context;

.field private mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 1099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInit(Landroid/content/Context;Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V
    .locals 0
    .param p1, "mActivity"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .prologue
    .line 1202
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->mActivity:Landroid/content/Context;

    .line 1203
    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 1204
    return-void
.end method

.method public onTextCopy(Ljava/lang/CharSequence;)V
    .locals 18
    .param p1, "str"    # Ljava/lang/CharSequence;

    .prologue
    .line 1151
    const/4 v6, 0x1

    .line 1153
    .local v6, "isTextOnly":Z
    new-instance v11, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1154
    .local v11, "span":Landroid/text/SpannableStringBuilder;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionEnd()I

    move-result v17

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->min(II)I

    move-result v16

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(II)I

    move-result v15

    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$12(Lcom/samsung/android/app/memo/uiwidget/RichEditor;I)V

    .line 1155
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionEnd()I

    move-result v17

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->max(II)I

    move-result v16

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(II)I

    move-result v15

    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$13(Lcom/samsung/android/app/memo/uiwidget/RichEditor;I)V

    .line 1156
    new-instance v10, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v14

    .line 1157
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I
    invoke-static {v15}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$14(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v16, v0

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$15(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)I

    move-result v16

    invoke-interface/range {v14 .. v16}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v14

    check-cast v14, Landroid/text/Spanned;

    .line 1156
    invoke-static {v14}, Lcom/samsung/android/app/memo/util/HtmlUtil;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v10, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1158
    .local v10, "spHtml":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1159
    .local v2, "copyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v13, -0x1

    .local v13, "startTag":I
    const/4 v3, -0x1

    .local v3, "endTag":I
    const/4 v7, 0x0

    .line 1161
    .local v7, "lastEndPos":I
    const/4 v14, 0x0

    invoke-virtual {v11}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v15

    const-class v16, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    move-object/from16 v0, v16

    invoke-virtual {v11, v14, v15, v0}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v9

    .line 1162
    .local v9, "objArr":[Ljava/lang/Object;
    if-eqz v9, :cond_0

    .line 1163
    array-length v0, v9

    move/from16 v16, v0

    const/4 v14, 0x0

    move v15, v14

    :goto_0
    move/from16 v0, v16

    if-lt v15, v0, :cond_2

    .line 1189
    :cond_0
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const-string v15, "<p>"

    const-string v16, ""

    invoke-virtual/range {v14 .. v16}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1190
    .local v12, "st":Ljava/lang/String;
    const-string v14, "</p>"

    const-string v15, "<br>"

    invoke-virtual {v12, v14, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1191
    const-string v14, "<br>"

    invoke-virtual {v12, v14}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    .line 1192
    .local v5, "ind":I
    const/4 v14, 0x0

    const/4 v15, -0x1

    if-ne v5, v15, :cond_1

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v5

    .end local v5    # "ind":I
    :cond_1
    invoke-virtual {v12, v14, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 1194
    if-eqz v6, :cond_6

    const-string v14, "<br>"

    invoke-virtual {v12, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_6

    .line 1195
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v15, 0x0

    invoke-static {v15, v12}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v15

    # invokes: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPrimaryClip(Landroid/content/ClipData;)V
    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$17(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Landroid/content/ClipData;)V

    .line 1198
    .end local v12    # "st":Ljava/lang/String;
    :goto_1
    return-void

    .line 1163
    :cond_2
    aget-object v8, v9, v15

    .line 1164
    .local v8, "obj":Ljava/lang/Object;
    instance-of v14, v8, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    if-eqz v14, :cond_5

    move-object v14, v8

    .line 1165
    check-cast v14, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    invoke-virtual {v14}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    .line 1166
    .local v1, "ImageUri":Landroid/net/Uri;
    if-eqz v1, :cond_5

    .line 1167
    const/4 v6, 0x0

    .line 1168
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1170
    check-cast v8, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    .end local v8    # "obj":Ljava/lang/Object;
    invoke-virtual {v8}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->toHtmlForCopy()Ljava/lang/String;

    move-result-object v4

    .line 1173
    .local v4, "htmlImgTag":Ljava/lang/String;
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v14

    const-string v17, "<img"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v14

    add-int v13, v14, v7

    .line 1174
    const-string v14, "/>"

    invoke-virtual {v10, v14, v13}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;I)I

    move-result v14

    add-int/lit8 v3, v14, 0x2

    .line 1176
    add-int/lit8 v7, v3, 0x1

    .line 1178
    if-ltz v13, :cond_3

    if-gez v3, :cond_4

    .line 1179
    :cond_3
    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$9()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "Copy Failure : IndexOutofBounds start:"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 1180
    const-string v16, " end:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 1179
    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1183
    :cond_4
    if-le v3, v13, :cond_5

    .line 1184
    invoke-virtual {v10, v13, v3, v4}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1163
    .end local v1    # "ImageUri":Landroid/net/Uri;
    .end local v4    # "htmlImgTag":Ljava/lang/String;
    :cond_5
    add-int/lit8 v14, v15, 0x1

    move v15, v14

    goto/16 :goto_0

    .line 1197
    .restart local v12    # "st":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    const/4 v15, 0x0

    invoke-static {v15, v12, v12}, Landroid/content/ClipData;->newHtmlText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/content/ClipData;

    move-result-object v15

    # invokes: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPrimaryClip(Landroid/content/ClipData;)V
    invoke-static {v14, v15}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$17(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Landroid/content/ClipData;)V

    goto :goto_1
.end method

.method public onTextPaste(Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;Ljava/lang/CharSequence;II)V
    .locals 1
    .param p1, "dtat"    # Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    .param p2, "str"    # Ljava/lang/CharSequence;
    .param p3, "start"    # I
    .param p4, "end"    # I

    .prologue
    .line 1106
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mImagePasteFailed:Z

    .line 1107
    return-void
.end method

.method public onTextPaste(Ljava/lang/CharSequence;II)V
    .locals 12
    .param p1, "str"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1111
    const-string v1, ""

    .line 1112
    .local v1, "HtmlStr":Ljava/lang/String;
    sput-boolean v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mImagePasteFailed:Z

    .line 1113
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    sget v4, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mMaxCharSize:I

    if-ne v3, v4, :cond_2

    move v9, v5

    .line 1114
    .local v9, "isLimitReached":Z
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->mActivity:Landroid/content/Context;

    .line 1115
    const-string v4, "clipboard"

    .line 1114
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/ClipboardManager;

    .line 1116
    .local v6, "clipboard":Landroid/content/ClipboardManager;
    invoke-virtual {v6}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v11

    .line 1117
    .local v11, "mclip":Landroid/content/ClipData;
    new-instance v2, Landroid/text/SpannableStringBuilder;

    .line 1118
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->mRichEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 1117
    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1119
    .local v2, "spn":Landroid/text/SpannableStringBuilder;
    const/4 v10, 0x0

    .line 1120
    .local v10, "item":Landroid/content/ClipData$Item;
    if-eqz v11, :cond_0

    .line 1121
    invoke-virtual {v11, v0}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v10

    .line 1123
    :cond_0
    if-eqz v10, :cond_1

    .line 1125
    invoke-virtual {v10}, Landroid/content/ClipData$Item;->getHtmlText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1126
    invoke-virtual {v10}, Landroid/content/ClipData$Item;->getHtmlText()Ljava/lang/String;

    move-result-object v1

    .line 1131
    :cond_1
    :goto_1
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1147
    :goto_2
    return-void

    .end local v2    # "spn":Landroid/text/SpannableStringBuilder;
    .end local v6    # "clipboard":Landroid/content/ClipboardManager;
    .end local v9    # "isLimitReached":Z
    .end local v10    # "item":Landroid/content/ClipData$Item;
    .end local v11    # "mclip":Landroid/content/ClipData;
    :cond_2
    move v9, v0

    .line 1113
    goto :goto_0

    .line 1127
    .restart local v2    # "spn":Landroid/text/SpannableStringBuilder;
    .restart local v6    # "clipboard":Landroid/content/ClipboardManager;
    .restart local v9    # "isLimitReached":Z
    .restart local v10    # "item":Landroid/content/ClipData$Item;
    .restart local v11    # "mclip":Landroid/content/ClipData;
    :cond_3
    invoke-virtual {v10}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1128
    invoke-virtual {v10}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1134
    :cond_4
    const/4 v7, 0x0

    .line 1135
    .local v7, "count":I
    :goto_3
    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 1143
    :cond_5
    if-nez v9, :cond_8

    .line 1144
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move v3, p2

    move v4, p3

    # invokes: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->addHtmlText(Ljava/lang/String;Landroid/text/SpannableStringBuilder;IIZ)V
    invoke-static/range {v0 .. v5}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$6(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/lang/String;Landroid/text/SpannableStringBuilder;IIZ)V

    goto :goto_2

    .line 1136
    :cond_6
    const/4 v3, 0x2

    if-gt v7, v3, :cond_5

    .line 1138
    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    .line 1139
    .local v8, "endLineIndex":I
    if-lez v8, :cond_7

    .line 1140
    invoke-virtual {v1, v0, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1141
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 1146
    .end local v8    # "endLineIndex":I
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f0b0048

    invoke-static {v0, v3}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_2
.end method
