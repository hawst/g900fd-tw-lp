.class Lcom/samsung/android/app/memo/uifragment/MemoListFragment$1;
.super Ljava/lang/Object;
.source "MemoListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->initView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    .line 470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    .line 474
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->checkLowMemory()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 475
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$12(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Landroid/app/Activity;

    move-result-object v2

    .line 476
    const v3, 0x7f0b00b9

    .line 475
    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 488
    :goto_0
    return-void

    .line 479
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 480
    .local v0, "currentTouchTime":J
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mLastMemoOpenClickTime:J
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$15(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)J

    move-result-wide v2

    sub-long v2, v0, v2

    const-wide/16 v4, 0x1f4

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 481
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-static {v2, v6}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$52(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V

    .line 482
    invoke-static {v6}, Lcom/samsung/android/app/memo/MemoApp;->setIsCreateMemo(Z)V

    .line 483
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$36(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v2

    const-wide/16 v4, -0x1

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mStrCategoryUUID:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$19(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v4, v5, v3, v6}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->openContent(JLjava/lang/String;Z)V

    .line 486
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$16(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;J)V

    .line 487
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$1;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->getCount()I

    move-result v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$53(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;I)V

    goto :goto_0
.end method
