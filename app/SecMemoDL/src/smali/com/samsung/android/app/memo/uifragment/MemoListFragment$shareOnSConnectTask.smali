.class public Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;
.super Landroid/os/AsyncTask;
.source "MemoListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "shareOnSConnectTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field intent:Landroid/content/Intent;

.field mActivity:Landroid/app/Activity;

.field mProgress:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

.field xmlUtils:Lcom/samsung/android/app/memo/share/beam/XMLUtills;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/app/Activity;)V
    .locals 2
    .param p2, "activity"    # Landroid/app/Activity;

    .prologue
    .line 2664
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2665
    iput-object p2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->mActivity:Landroid/app/Activity;

    .line 2666
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->intent:Landroid/content/Intent;

    .line 2667
    new-instance v0, Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->xmlUtils:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    .line 2668
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 8
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 2686
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2687
    .local v3, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->getSelectedIds()Landroid/util/SparseBooleanArray;

    move-result-object v2

    .line 2689
    .local v2, "selected":Landroid/util/SparseBooleanArray;
    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_0

    .line 2695
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->intent:Landroid/content/Intent;

    const-string v5, "android.intent.extra.STREAM"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2696
    const/4 v4, 0x0

    return-object v4

    .line 2690
    :cond_0
    invoke-virtual {v2, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    .line 2691
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->xmlUtils:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mAdapter:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;
    invoke-static {v5}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$13(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;

    move-result-object v5

    invoke-virtual {v2, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ListCursorAdapter;->getItemId(I)J

    move-result-wide v6

    long-to-int v5, v6

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->createXMLFile(J)Ljava/lang/String;

    move-result-object v1

    .line 2692
    .local v1, "path":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2689
    .end local v1    # "path":Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 2701
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2702
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_MULTIPLE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v1, v2, :cond_0

    .line 2703
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2704
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$36(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->onFragmentSuicide(Ljava/lang/String;)V

    .line 2719
    :goto_0
    return-void

    .line 2707
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->intent:Landroid/content/Intent;

    const-string v2, "com.samsung.android.sconnect.START"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2708
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->intent:Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2711
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->mActivity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2716
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$51(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V

    .line 2717
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    goto :goto_0

    .line 2712
    :catch_0
    move-exception v0

    .line 2713
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$1()Ljava/lang/String;

    move-result-object v1

    const-string v2, "shareOnSConnect"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 2672
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 2673
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->mActivity:Landroid/app/Activity;

    const-string v1, ""

    .line 2674
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b00ab

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2673
    invoke-static {v0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->mProgress:Landroid/app/ProgressDialog;

    .line 2675
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->mProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 2676
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isIndeterminate()Z

    .line 2677
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 2679
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$51(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Z)V

    .line 2680
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 2681
    return-void
.end method
