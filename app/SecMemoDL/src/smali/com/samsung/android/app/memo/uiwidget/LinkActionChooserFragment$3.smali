.class Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$3;
.super Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;
.source "LinkActionChooserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "$anonymous0"    # I

    .prologue
    .line 301
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;-><init>(I)V

    .line 1
    return-void
.end method


# virtual methods
.method public execute(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/app/Activity;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 304
    # getter for: Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->access$0()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->newInstance(Landroid/content/Context;)Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    move-result-object v0

    .line 305
    .local v0, "clipboardEXProxy":Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
    invoke-virtual {v0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 306
    new-instance v2, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;

    invoke-direct {v2}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;-><init>()V

    .line 307
    .local v2, "proxyData":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setFormat(I)V

    .line 308
    invoke-virtual {v2, p3}, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->setData(Ljava/lang/CharSequence;)V

    .line 309
    # getter for: Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->access$0()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->setData(Landroid/content/Context;Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;)V

    .line 316
    .end local v2    # "proxyData":Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
    :goto_0
    return-void

    .line 313
    :cond_0
    const-string v3, "clipboard"

    invoke-virtual {p1, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 312
    check-cast v1, Landroid/content/ClipboardManager;

    .line 314
    .local v1, "clipboardManager":Landroid/content/ClipboardManager;
    const/4 v3, 0x0

    invoke-static {v3, p3}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    goto :goto_0
.end method
