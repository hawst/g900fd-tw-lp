.class Lcom/samsung/android/app/memo/uifragment/MemoListFragment$12;
.super Ljava/lang/Object;
.source "MemoListFragment.java"

# interfaces
.implements Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->initSConnect(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

.field private final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$12;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    iput-object p2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$12;->val$activity:Landroid/app/Activity;

    .line 2642
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected()V
    .locals 3

    .prologue
    .line 2646
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$12;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->isTaskRunning:Z
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$58(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2647
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$12;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mMenuType:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;
    invoke-static {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$10(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->PICK_MULTIPLE:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    if-ne v1, v2, :cond_0

    .line 2648
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$12;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    sget-object v2, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;->NORMAL:Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->access$40(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Lcom/samsung/android/app/memo/uifragment/MemoListFragment$ActionModeStyle;)V

    .line 2649
    :cond_0
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$12;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$12;->val$activity:Landroid/app/Activity;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;Landroid/app/Activity;)V

    .line 2650
    .local v0, "shareOnSConnect":Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2652
    .end local v0    # "shareOnSConnect":Lcom/samsung/android/app/memo/uifragment/MemoListFragment$shareOnSConnectTask;
    :cond_1
    return-void
.end method
