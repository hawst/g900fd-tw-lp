.class Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;
.super Ljava/lang/Object;
.source "MemoLinkify.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ResourceString"
.end annotation


# static fields
.field static mAfternoon:Ljava/lang/String;

.field static mContext:Landroid/content/Context;

.field static mDay01:Ljava/lang/String;

.field static mDay01_abb:Ljava/lang/String;

.field static mDay02:Ljava/lang/String;

.field static mDay02_abb:Ljava/lang/String;

.field static mDay03:Ljava/lang/String;

.field static mDay03_abb:Ljava/lang/String;

.field static mDay04:Ljava/lang/String;

.field static mDay04_abb:Ljava/lang/String;

.field static mDay05:Ljava/lang/String;

.field static mDay05_abb:Ljava/lang/String;

.field static mDay06:Ljava/lang/String;

.field static mDay06_abb:Ljava/lang/String;

.field static mDay07:Ljava/lang/String;

.field static mDay07_abb:Ljava/lang/String;

.field static mEvening:Ljava/lang/String;

.field static mMonth01:Ljava/lang/String;

.field static mMonth01_abb:Ljava/lang/String;

.field static mMonth02:Ljava/lang/String;

.field static mMonth02_abb:Ljava/lang/String;

.field static mMonth03:Ljava/lang/String;

.field static mMonth03_abb:Ljava/lang/String;

.field static mMonth04:Ljava/lang/String;

.field static mMonth04_abb:Ljava/lang/String;

.field static mMonth05:Ljava/lang/String;

.field static mMonth05_abb:Ljava/lang/String;

.field static mMonth06:Ljava/lang/String;

.field static mMonth06_abb:Ljava/lang/String;

.field static mMonth07:Ljava/lang/String;

.field static mMonth07_abb:Ljava/lang/String;

.field static mMonth08:Ljava/lang/String;

.field static mMonth08_abb:Ljava/lang/String;

.field static mMonth09:Ljava/lang/String;

.field static mMonth09_abb:Ljava/lang/String;

.field static mMonth10:Ljava/lang/String;

.field static mMonth10_abb:Ljava/lang/String;

.field static mMonth11:Ljava/lang/String;

.field static mMonth11_abb:Ljava/lang/String;

.field static mMonth12:Ljava/lang/String;

.field static mMonth12_abb:Ljava/lang/String;

.field static mMorning:Ljava/lang/String;

.field static mNight:Ljava/lang/String;

.field static mToday:Ljava/lang/String;

.field static mTomorrow:Ljava/lang/String;

.field static mTonight:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getEnglishWeekDay(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "weekday"    # Ljava/lang/String;

    .prologue
    .line 1726
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay01:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1727
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object p0, v0, v1

    .line 1742
    .end local p0    # "weekday":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 1728
    .restart local p0    # "weekday":Ljava/lang/String;
    :cond_1
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay02:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1729
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object p0, v0, v1

    goto :goto_0

    .line 1730
    :cond_2
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay03:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1731
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object p0, v0, v1

    goto :goto_0

    .line 1732
    :cond_3
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay04:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1733
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object p0, v0, v1

    goto :goto_0

    .line 1734
    :cond_4
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay05:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1735
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object p0, v0, v1

    goto :goto_0

    .line 1736
    :cond_5
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay06:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1737
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object p0, v0, v1

    goto :goto_0

    .line 1738
    :cond_6
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay07:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1739
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->mArrDayOfWeek:[Ljava/lang/String;

    const/4 v1, 0x6

    aget-object p0, v0, v1

    goto :goto_0
.end method

.method public static getMonth(Ljava/lang/String;)I
    .locals 3
    .param p0, "sMonth"    # Ljava/lang/String;

    .prologue
    .line 1680
    const/4 v0, -0x1

    .line 1682
    .local v0, "month":I
    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth01:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth01_abb:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1683
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth01_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth01_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1684
    :cond_0
    const/4 v0, 0x1

    .line 1720
    :cond_1
    :goto_0
    return v0

    .line 1685
    :cond_2
    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth02:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth02_abb:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1686
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth02_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth02_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1687
    :cond_3
    const/4 v0, 0x2

    .line 1688
    goto :goto_0

    :cond_4
    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth03:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth03_abb:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1689
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth03_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth03_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1690
    :cond_5
    const/4 v0, 0x3

    .line 1691
    goto/16 :goto_0

    :cond_6
    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth04:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth04_abb:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1692
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth04_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth04_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1693
    :cond_7
    const/4 v0, 0x4

    .line 1694
    goto/16 :goto_0

    :cond_8
    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth05:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth05_abb:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1695
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth05_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth05_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1696
    :cond_9
    const/4 v0, 0x5

    .line 1697
    goto/16 :goto_0

    :cond_a
    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth06:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth06_abb:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1698
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth06_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth06_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1699
    :cond_b
    const/4 v0, 0x6

    .line 1700
    goto/16 :goto_0

    :cond_c
    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth07:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth07_abb:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 1701
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth07_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth07_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1702
    :cond_d
    const/4 v0, 0x7

    .line 1703
    goto/16 :goto_0

    :cond_e
    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth08:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth08_abb:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 1704
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth08_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth08_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1705
    :cond_f
    const/16 v0, 0x8

    .line 1706
    goto/16 :goto_0

    :cond_10
    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth09:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth09_abb:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 1707
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth09_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth09_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1708
    :cond_11
    const/16 v0, 0x9

    .line 1709
    goto/16 :goto_0

    :cond_12
    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth10:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth10_abb:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 1710
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth10_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth10_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 1711
    :cond_13
    const/16 v0, 0xa

    .line 1712
    goto/16 :goto_0

    :cond_14
    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth11:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth11_abb:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 1713
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth11_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth11_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 1714
    :cond_15
    const/16 v0, 0xb

    .line 1715
    goto/16 :goto_0

    :cond_16
    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth12:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    sget-object v1, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth12_abb:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 1716
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth12_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth12_abb:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1717
    :cond_17
    const/16 v0, 0xc

    goto/16 :goto_0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1638
    sput-object p0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    .line 1639
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b006b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth01:Ljava/lang/String;

    .line 1640
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b006c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth02:Ljava/lang/String;

    .line 1641
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b006d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth03:Ljava/lang/String;

    .line 1642
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b006e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth04:Ljava/lang/String;

    .line 1643
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b006f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth05:Ljava/lang/String;

    .line 1644
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0070

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth06:Ljava/lang/String;

    .line 1645
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0071

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth07:Ljava/lang/String;

    .line 1646
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0072

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth08:Ljava/lang/String;

    .line 1647
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0073

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth09:Ljava/lang/String;

    .line 1648
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0074

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth10:Ljava/lang/String;

    .line 1649
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0075

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth11:Ljava/lang/String;

    .line 1650
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0076

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth12:Ljava/lang/String;

    .line 1651
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0077

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth01_abb:Ljava/lang/String;

    .line 1652
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0078

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth02_abb:Ljava/lang/String;

    .line 1653
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0079

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth03_abb:Ljava/lang/String;

    .line 1654
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b007a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth04_abb:Ljava/lang/String;

    .line 1655
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b007b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth05_abb:Ljava/lang/String;

    .line 1656
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b007c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth06_abb:Ljava/lang/String;

    .line 1657
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b007d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth07_abb:Ljava/lang/String;

    .line 1658
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b007e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth08_abb:Ljava/lang/String;

    .line 1659
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b007f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth09_abb:Ljava/lang/String;

    .line 1660
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0080

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth10_abb:Ljava/lang/String;

    .line 1661
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0081

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth11_abb:Ljava/lang/String;

    .line 1662
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0082

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMonth12_abb:Ljava/lang/String;

    .line 1663
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0083

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay01:Ljava/lang/String;

    .line 1664
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0084

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay02:Ljava/lang/String;

    .line 1665
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0085

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay03:Ljava/lang/String;

    .line 1666
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0086

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay04:Ljava/lang/String;

    .line 1667
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0087

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay05:Ljava/lang/String;

    .line 1668
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0088

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay06:Ljava/lang/String;

    .line 1669
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0089

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mDay07:Ljava/lang/String;

    .line 1670
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b008b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mTonight:Ljava/lang/String;

    .line 1671
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b008a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mToday:Ljava/lang/String;

    .line 1672
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b008c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mTomorrow:Ljava/lang/String;

    .line 1673
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b008d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mMorning:Ljava/lang/String;

    .line 1674
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b008e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mAfternoon:Ljava/lang/String;

    .line 1675
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b008f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mEvening:Ljava/lang/String;

    .line 1676
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0090

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify$ResourceString;->mNight:Ljava/lang/String;

    .line 1677
    return-void
.end method
