.class Lcom/samsung/android/app/memo/uiwidget/CustLinkify$2;
.super Ljava/lang/Object;
.source "CustLinkify.java"

# interfaces
.implements Lcom/samsung/android/app/memo/uiwidget/CustLinkify$MatchFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/CustLinkify;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public final acceptMatch(Ljava/lang/CharSequence;II)Z
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 114
    const/4 v0, 0x0

    .line 116
    .local v0, "digitCount":I
    move v1, p2

    .local v1, "i":I
    :goto_0
    if-lt v1, p3, :cond_0

    .line 124
    const/4 v2, 0x0

    :goto_1
    return v2

    .line 117
    :cond_0
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 118
    add-int/lit8 v0, v0, 0x1

    .line 119
    const/4 v2, 0x5

    if-lt v0, v2, :cond_1

    .line 120
    const/4 v2, 0x1

    goto :goto_1

    .line 116
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
