.class public Lcom/samsung/android/app/memo/ImageViewActivity;
.super Landroid/app/Activity;
.source "ImageViewActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/samsung/android/app/memo/ImageViewActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/ImageViewActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    sget-object v4, Lcom/samsung/android/app/memo/ImageViewActivity;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onCreate()"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/ImageViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Landroid/view/Window;->requestFeature(I)Z

    .line 42
    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/memo/ImageViewActivity;->setRequestedOrientation(I)V

    .line 43
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/ImageViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    .line 44
    .local v3, "window":Landroid/view/Window;
    if-eqz v3, :cond_0

    .line 45
    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 46
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v4, v4, 0x400

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 47
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 48
    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 50
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    if-nez p1, :cond_1

    .line 54
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/ImageViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 55
    .local v0, "args":Landroid/os/Bundle;
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->KEY_CONTENT_URI:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/ImageViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 56
    new-instance v1, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;

    invoke-direct {v1}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;-><init>()V

    .line 57
    .local v1, "contentFragment":Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;
    invoke-virtual {v1, v0}, Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;->setArguments(Landroid/os/Bundle;)V

    .line 59
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/ImageViewActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    const v5, 0x1020002

    invoke-virtual {v4, v5, v1}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    .line 61
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "contentFragment":Lcom/samsung/android/app/memo/uifragment/ImageViewFragment;
    :cond_1
    return-void
.end method
