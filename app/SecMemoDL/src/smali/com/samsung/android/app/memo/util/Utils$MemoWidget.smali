.class public interface abstract Lcom/samsung/android/app/memo/util/Utils$MemoWidget;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/util/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MemoWidget"
.end annotation


# static fields
.field public static final ACTION_APPWIDGET_UPDATE:Ljava/lang/String; = "com.samsung.android.widget.memo.UPDATE_SELECTED_WIDGET"

.field public static final ACTION_UPDATE_ALL_WIDGET:Ljava/lang/String; = "com.samsung.android.widget.memo.UPDATE_ALL_WIDGET"

.field public static final EXTRA_ITEM:Ljava/lang/String; = "com.samsung.android.widget.EXTRA_ITEM"

.field public static final EXTRA_KEY_FROM_WIDGET:Ljava/lang/String; = "from_widget"

.field public static final EXTRA_KEY_FROM_WIDGET_ID:Ljava/lang/String; = "appWidgetId"
