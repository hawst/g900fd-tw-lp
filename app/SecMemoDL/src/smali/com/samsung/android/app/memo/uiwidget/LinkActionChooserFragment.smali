.class public Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;
.super Landroid/app/DialogFragment;
.source "LinkActionChooserFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;,
        Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$ViewAction;
    }
.end annotation


# static fields
.field static final ACTION_ADD_EVENT:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

.field static final ACTION_ADD_TO_BOOKMARKS:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

.field static final ACTION_ADD_TO_CONTACTS:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

.field static final ACTION_CALL:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

.field static final ACTION_COPY_TEXT:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

.field static final ACTION_GOTO_MAPS:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

.field static final ACTION_OPEN_URL:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

.field static final ACTION_SEND_EMAIL:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

.field static final ACTION_SEND_TEXT_MESSAGE:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

.field static final ACTION_VIEW_CONTACT:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

.field private static final EXTRA_TEXT:Ljava/lang/String; = "text"

.field private static final EXTRA_URL:Ljava/lang/String; = "url"

.field public static final PKG_BROWSER:Ljava/lang/String; = "com.android.browser"

.field public static final PKG_CHROME:Ljava/lang/String; = "com.android.chrome"

.field public static final PKG_CONTACTS:Ljava/lang/String; = "com.android.contacts"

.field public static final PKG_SBROWSER:Ljava/lang/String; = "com.sec.android.app.sbrowser"

.field private static mContext:Landroid/content/Context;

.field private static mIsAllDay:Z


# instance fields
.field private mAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;",
            ">;"
        }
    .end annotation
.end field

.field private mStrText:Ljava/lang/String;

.field private mStrUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 261
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$ViewAction;

    const v1, 0x7f0b005e

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$ViewAction;-><init>(I)V

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_SEND_EMAIL:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    .line 263
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$ViewAction;

    const v1, 0x7f0b005f

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$ViewAction;-><init>(I)V

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_OPEN_URL:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    .line 265
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$1;

    const v1, 0x7f0b0060

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$1;-><init>(I)V

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_CALL:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    .line 280
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$2;

    const v1, 0x7f0b0061

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$2;-><init>(I)V

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_ADD_TO_BOOKMARKS:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    .line 301
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$3;

    const v1, 0x7f0b00be

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$3;-><init>(I)V

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_COPY_TEXT:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    .line 319
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$4;

    const v1, 0x7f0b0062

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$4;-><init>(I)V

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_ADD_TO_CONTACTS:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    .line 338
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$5;

    const v1, 0x7f0b0035

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$5;-><init>(I)V

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_VIEW_CONTACT:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    .line 359
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$6;

    const v1, 0x7f0b0063

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$6;-><init>(I)V

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_SEND_TEXT_MESSAGE:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    .line 370
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$7;

    const v1, 0x7f0b0065

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$7;-><init>(I)V

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_ADD_EVENT:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    .line 386
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$8;

    const v1, 0x7f0b0064

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$8;-><init>(I)V

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_GOTO_MAPS:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    .line 406
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$0()Landroid/content/Context;
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1()Z
    .locals 1

    .prologue
    .line 79
    sget-boolean v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mIsAllDay:Z

    return v0
.end method

.method public static getContactIDfromMail(Landroid/app/Activity;Ljava/lang/String;)I
    .locals 8
    .param p0, "_activity"    # Landroid/app/Activity;
    .param p1, "email"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v7, -0x1

    .line 433
    if-eqz p1, :cond_4

    .line 434
    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 435
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "contact_id"

    aput-object v5, v2, v3

    .line 436
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "data1=\'"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    .line 434
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 440
    .local v6, "cur":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 441
    const-string v0, "contact_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 444
    if-eqz v6, :cond_0

    .line 445
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 449
    .end local v6    # "cur":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return v0

    .line 443
    .restart local v6    # "cur":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    .line 444
    if-eqz v6, :cond_1

    .line 445
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 446
    :cond_1
    throw v0

    .line 444
    :cond_2
    if-eqz v6, :cond_3

    .line 445
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    move v0, v7

    .line 447
    goto :goto_0

    .end local v6    # "cur":Landroid/database/Cursor;
    :cond_4
    move v0, v7

    .line 449
    goto :goto_0
.end method

.method public static getContactIDfromNumber(Landroid/app/Activity;Ljava/lang/String;)I
    .locals 8
    .param p0, "_activity"    # Landroid/app/Activity;
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    const/4 v7, -0x1

    const/4 v3, 0x0

    .line 409
    if-eqz p1, :cond_4

    .line 411
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 412
    .local v1, "lookupUri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 413
    const-string v4, "_id"

    aput-object v4, v2, v0

    .line 415
    .local v2, "mPhoneNumberProjection":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 419
    .local v6, "cur":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 420
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 423
    if-eqz v6, :cond_0

    .line 424
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 428
    .end local v1    # "lookupUri":Landroid/net/Uri;
    .end local v2    # "mPhoneNumberProjection":[Ljava/lang/String;
    .end local v6    # "cur":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return v0

    .line 422
    .restart local v1    # "lookupUri":Landroid/net/Uri;
    .restart local v2    # "mPhoneNumberProjection":[Ljava/lang/String;
    .restart local v6    # "cur":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    .line 423
    if-eqz v6, :cond_1

    .line 424
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 425
    :cond_1
    throw v0

    .line 423
    :cond_2
    if-eqz v6, :cond_3

    .line 424
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    move v0, v7

    .line 426
    goto :goto_0

    .end local v1    # "lookupUri":Landroid/net/Uri;
    .end local v2    # "mPhoneNumberProjection":[Ljava/lang/String;
    .end local v6    # "cur":Landroid/database/Cursor;
    :cond_4
    move v0, v7

    .line 428
    goto :goto_0
.end method

.method public static final newInstance(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/DialogFragment;
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "isAllDay"    # Z

    .prologue
    .line 97
    new-instance v1, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;

    invoke-direct {v1}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;-><init>()V

    .line 98
    .local v1, "fg":Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 99
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "url"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v2, "text"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-virtual {v1, v0}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->setArguments(Landroid/os/Bundle;)V

    .line 102
    sput-object p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mContext:Landroid/content/Context;

    .line 103
    sput-boolean p3, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mIsAllDay:Z

    .line 104
    return-object v1
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 219
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, p2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    .line 220
    .local v0, "action":Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;
    if-nez v0, :cond_0

    .line 222
    :goto_0
    return-void

    .line 221
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrText:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;->execute(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x1

    .line 115
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 116
    .local v0, "args":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 117
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    .line 120
    :cond_0
    const-string v3, "url"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrUrl:Ljava/lang/String;

    .line 121
    const-string v3, "text"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrText:Ljava/lang/String;

    .line 123
    new-instance v3, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const v5, 0x7f040020

    .line 124
    const v6, 0x7f0e0048

    invoke-direct {v3, v4, v5, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 123
    iput-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    .line 128
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrUrl:Ljava/lang/String;

    const-string v4, "mailto:"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isGuestMode()Z

    move-result v3

    if-nez v3, :cond_4

    .line 129
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isUPSM()I

    move-result v3

    if-eq v3, v7, :cond_1

    .line 130
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_SEND_EMAIL:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 133
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_SEND_TEXT_MESSAGE:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 145
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrUrl:Ljava/lang/String;

    const-string v5, "mailto:"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->getContactIDfromMail(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v3

    if-ne v3, v8, :cond_3

    .line 146
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_ADD_TO_CONTACTS:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 194
    :cond_2
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_COPY_TEXT:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 196
    new-instance v3, Landroid/app/AlertDialog$Builder;

    new-instance v4, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 197
    const/high16 v6, 0x7f0c0000

    invoke-direct {v4, v5, v6}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 196
    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 197
    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3, v4, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 198
    .local v2, "result":Landroid/app/Dialog;
    new-instance v3, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$9;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$9;-><init>(Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;)V

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 205
    invoke-virtual {v2, v7}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 206
    return-object v2

    .line 148
    .end local v2    # "result":Landroid/app/Dialog;
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_VIEW_CONTACT:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 151
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrUrl:Ljava/lang/String;

    const-string v4, "tel:"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isGuestMode()Z

    move-result v3

    if-nez v3, :cond_7

    .line 152
    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mContext:Landroid/content/Context;

    const-string v4, "com.android.contacts"

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Utils;->isPackageExists(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/app/memo/util/Utils;->isVoiceCallAvailabe(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 153
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_CALL:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 156
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_SEND_TEXT_MESSAGE:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 163
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrUrl:Ljava/lang/String;

    const-string v5, "tel:"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->getContactIDfromNumber(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v3

    if-ne v3, v8, :cond_6

    .line 164
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_ADD_TO_CONTACTS:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 166
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_VIEW_CONTACT:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 173
    :cond_7
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrUrl:Ljava/lang/String;

    const-string v4, "time:"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isGuestMode()Z

    move-result v3

    if-nez v3, :cond_8

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isUPSM()I

    move-result v3

    if-eq v3, v7, :cond_8

    .line 174
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_ADD_EVENT:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 175
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrUrl:Ljava/lang/String;

    const-string v4, "geo:"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isGuestMode()Z

    move-result v3

    if-nez v3, :cond_9

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isUPSM()I

    move-result v3

    if-eq v3, v7, :cond_9

    .line 176
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isKnoxMode()Z

    move-result v3

    if-nez v3, :cond_9

    .line 177
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_GOTO_MAPS:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 178
    :cond_9
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrUrl:Ljava/lang/String;

    const-string v4, "http://"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrUrl:Ljava/lang/String;

    const-string v4, "https://"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 179
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrUrl:Ljava/lang/String;

    const-string v4, "rtsp://"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 180
    :cond_a
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 182
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/4 v4, 0x0

    .line 181
    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 182
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_b

    .line 183
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_OPEN_URL:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 186
    :cond_b
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mStrUrl:Ljava/lang/String;

    const-string v4, "rtsp://"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 187
    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_2

    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mContext:Landroid/content/Context;

    const-string v4, "com.android.browser"

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Utils;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 188
    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mContext:Landroid/content/Context;

    const-string v4, "com.sec.android.app.sbrowser"

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Utils;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 189
    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mContext:Landroid/content/Context;

    const-string v4, "com.android.chrome"

    .line 188
    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Utils;->isEnabledPkg(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    .line 189
    if-eqz v3, :cond_2

    .line 190
    :cond_c
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->ACTION_ADD_TO_BOOKMARKS:Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;

    invoke-virtual {v3, v4}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 211
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 214
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->dismiss()V

    .line 215
    :cond_0
    return-void
.end method
