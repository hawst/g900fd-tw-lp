.class public Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;
.super Ljava/lang/Thread;
.source "ConvertLocalTMemo1Task.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ConvertLocalTMemo1Task"


# instance fields
.field private context:Landroid/app/Activity;

.field private cursor:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "context"    # Landroid/app/Activity;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->context:Landroid/app/Activity;

    .line 45
    iput-object p2, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->cursor:Landroid/database/Cursor;

    .line 46
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->context:Landroid/app/Activity;

    return-object v0
.end method

.method private isContextAlive(Landroid/app/Activity;)Z
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 49
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 50
    const/4 v0, 0x1

    .line 52
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 57
    const/4 v1, 0x0

    .line 58
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->context:Landroid/app/Activity;

    const v5, 0x7f0b0051

    invoke-virtual {v2, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    .line 57
    invoke-static {v1, v2, v5}, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    move-result-object v12

    .line 59
    .local v12, "progressDialog":Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->context:Landroid/app/Activity;

    invoke-direct {p0, v1}, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->isContextAlive(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->context:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->context:Landroid/app/Activity;

    new-instance v2, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task$1;

    invoke-direct {v2, p0, v12}, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task$1;-><init>(Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 66
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->cursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 67
    const/4 v10, 0x0

    .line 68
    .local v10, "memosCounter":I
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->cursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v11

    .line 70
    .local v11, "memosTotalNumber":I
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->cursor:Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 71
    .local v3, "title":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->cursor:Landroid/database/Cursor;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "content":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->cursor:Landroid/database/Cursor;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 73
    .local v6, "createdTime":J
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->cursor:Landroid/database/Cursor;

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 74
    .local v8, "modifiedTime":J
    const-string v1, "ConvertLocalTMemo1Task"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "title="

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", content="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v4, ""

    .line 76
    .local v4, "formattedContent":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 77
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 78
    .local v13, "s":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-static {v13, v0, v1, v2}, Lcom/samsung/android/app/memo/util/HtmlUtil;->withinStyle(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    .line 79
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/Utils;->formatContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 80
    invoke-static {v4}, Lcom/samsung/android/app/migration/utils/Utils;->handleNewLines(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 82
    .end local v13    # "s":Ljava/lang/StringBuilder;
    :cond_2
    invoke-static {}, Lcom/samsung/android/app/migration/MemoConverter;->getInstance()Lcom/samsung/android/app/migration/MemoConverter;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->context:Landroid/app/Activity;

    .line 83
    const/4 v5, 0x0

    .line 82
    invoke-virtual/range {v1 .. v9}, Lcom/samsung/android/app/migration/MemoConverter;->convertToNewMemo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)J

    .line 84
    add-int/lit8 v10, v10, 0x1

    mul-int/lit8 v1, v10, 0x64

    div-int/2addr v1, v11

    invoke-virtual {v12, v1}, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->setProgress(I)V

    .line 85
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->cursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 86
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->cursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 88
    .end local v0    # "content":Ljava/lang/String;
    .end local v3    # "title":Ljava/lang/String;
    .end local v4    # "formattedContent":Ljava/lang/String;
    .end local v6    # "createdTime":J
    .end local v8    # "modifiedTime":J
    .end local v10    # "memosCounter":I
    .end local v11    # "memosTotalNumber":I
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->context:Landroid/app/Activity;

    invoke-direct {p0, v1}, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->isContextAlive(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 89
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->context:Landroid/app/Activity;

    new-instance v2, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task$2;

    invoke-direct {v2, p0, v12}, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task$2;-><init>(Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 98
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->context:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/samsung/android/app/migration/utils/SharedPref;->setLocalMemosMigrationInProcessFlag(Landroid/content/Context;Z)V

    .line 99
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->closeTmemo1Database()V

    .line 100
    return-void
.end method
