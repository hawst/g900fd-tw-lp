.class public Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;
.super Ljava/lang/Object;
.source "SelectionMenu.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "SelectionMenu"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mPopupList:Lcom/samsung/android/app/memo/uiwidget/PopupList;

.field private final mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/TextView;Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textview"    # Landroid/widget/TextView;
    .param p3, "listener"    # Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mContext:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mTextView:Landroid/widget/TextView;

    .line 43
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/PopupList;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mTextView:Landroid/widget/TextView;

    invoke-direct {v0, p1, v1}, Lcom/samsung/android/app/memo/uiwidget/PopupList;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mPopupList:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    .line 44
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mPopupList:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    invoke-virtual {v0, p3}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->setOnPopupItemClickListener(Lcom/samsung/android/app/memo/uiwidget/PopupList$OnPopupItemClickListener;)V

    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    return-void
.end method


# virtual methods
.method public dismissPopupList()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mPopupList:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->dismiss()V

    .line 75
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mPopupList:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->show()V

    .line 51
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    return-void
.end method

.method public updateSelectAllMode(II)V
    .locals 6
    .param p1, "selectedMemoCount"    # I
    .param p2, "totalMemoCount"    # I

    .prologue
    const v5, 0x7f0e00a4

    const v4, 0x7f0e00a3

    const v3, 0x7f0b0039

    const v2, 0x7f0b0037

    .line 54
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mPopupList:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->clearItems()V

    .line 55
    if-nez p1, :cond_0

    .line 56
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mPopupList:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    .line 57
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->addItem(ILjava/lang/String;)V

    .line 67
    :goto_0
    return-void

    .line 58
    :cond_0
    if-ne p1, p2, :cond_1

    .line 59
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mPopupList:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    .line 60
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 59
    invoke-virtual {v0, v5, v1}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->addItem(ILjava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mPopupList:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    .line 63
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->addItem(ILjava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mPopupList:Lcom/samsung/android/app/memo/uiwidget/PopupList;

    .line 65
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/SelectionMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 64
    invoke-virtual {v0, v5, v1}, Lcom/samsung/android/app/memo/uiwidget/PopupList;->addItem(ILjava/lang/String;)V

    goto :goto_0
.end method
