.class Lcom/samsung/android/app/memo/uiwidget/UndoBarController$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "UndoBarController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->hideUndoBar(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    .line 111
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v2, 0x0

    .line 114
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mBarView:Landroid/view/View;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->access$2(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    invoke-static {v0, v2}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->access$3(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;Ljava/lang/CharSequence;)V

    .line 116
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    invoke-static {v0, v2}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->access$4(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;Landroid/os/Parcelable;)V

    .line 117
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$2;->this$0:Lcom/samsung/android/app/memo/uiwidget/UndoBarController;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->mUndoListener:Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController;->access$0(Lcom/samsung/android/app/memo/uiwidget/UndoBarController;)Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/app/memo/uiwidget/UndoBarController$UndoListener;->onHideUndoBar()V

    .line 118
    return-void
.end method
