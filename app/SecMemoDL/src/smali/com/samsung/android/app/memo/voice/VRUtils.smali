.class public Lcom/samsung/android/app/memo/voice/VRUtils;
.super Ljava/lang/Object;
.source "VRUtils.java"


# static fields
.field private static final LED_NOTIF_ID:I = 0x4e1f

.field private static final LED_OFF_TIME:I = 0x1f4

.field private static final LED_ON_TIME:I = 0x1f4

.field public static isRecording:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/memo/voice/VRUtils;->isRecording:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static hasLED()Z
    .locals 6

    .prologue
    .line 25
    const-string v0, "/sys/class/sec/led/led_pattern"

    .line 26
    .local v0, "LED_FILE":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 27
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    .line 29
    .local v2, "result":Z
    const-string v3, "VRUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "hasLED() result:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    return v2
.end method

.method public static isEnabledLED(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 35
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 36
    const-string v2, "led_indicator_voice_recording"

    .line 35
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 37
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 38
    const-string v2, "led_indicator_missed_event"

    .line 37
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_1

    .line 39
    :cond_0
    const-string v0, "VRUtils"

    const-string v1, "isEnabledLED() false"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const/4 v0, 0x0

    .line 43
    :goto_0
    return v0

    .line 42
    :cond_1
    const-string v1, "VRUtils"

    const-string v2, "isEnabledLED() true"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static toggleMemoLed(ZLandroid/content/Context;)V
    .locals 9
    .param p0, "enable"    # Z
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    const/16 v8, 0x4e1f

    const/16 v7, 0x1f4

    const/16 v6, 0xff

    const/4 v5, 0x0

    .line 47
    const-string v2, "VRUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "toggleMemoLed() enable:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isRecording:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/samsung/android/app/memo/voice/VRUtils;->isRecording:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v2, "notification"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 49
    check-cast v1, Landroid/app/NotificationManager;

    .line 52
    .local v1, "notifMgr":Landroid/app/NotificationManager;
    sget-boolean v2, Lcom/samsung/android/app/memo/voice/VRUtils;->isRecording:Z

    if-eqz v2, :cond_0

    if-eqz p0, :cond_0

    .line 53
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    .line 54
    .local v0, "notif":Landroid/app/Notification;
    invoke-static {v6, v5, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    iput v2, v0, Landroid/app/Notification;->ledARGB:I

    .line 55
    iget v2, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/app/Notification;->flags:I

    .line 56
    iput v7, v0, Landroid/app/Notification;->ledOnMS:I

    .line 57
    iput v7, v0, Landroid/app/Notification;->ledOffMS:I

    .line 58
    invoke-virtual {v1, v8, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 62
    .end local v0    # "notif":Landroid/app/Notification;
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-virtual {v1, v8}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method
