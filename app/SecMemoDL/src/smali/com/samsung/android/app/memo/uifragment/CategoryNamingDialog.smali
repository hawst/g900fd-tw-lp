.class public Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;
.super Landroid/app/DialogFragment;
.source "CategoryNamingDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;
    }
.end annotation


# static fields
.field private static final CID_UUID:I = 0x1

.field private static final KEY_CATEGORYUUID:Ljava/lang/String;

.field private static final KEY_ORIGINALNAME:Ljava/lang/String;

.field private static final MAX_INPUT_LENGTH:I = 0x1e

.field private static final ORDERBY:Ljava/lang/String; = "orderBy ASC"

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final SELECTION:Ljava/lang/String; = "isDeleted IS 0"

.field private static final TAG:Ljava/lang/String;

.field private static mDlg:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

.field private static mIsAdd:Z

.field private static mIsRename:Z

.field private static mStrCategoryUUID:Ljava/lang/String;

.field private static mStrOriginalName:Ljava/lang/String;

.field private static onDialogResultListener:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;


# instance fields
.field private categoryDialogOnShowListener:Landroid/content/DialogInterface$OnShowListener;

.field private categoryTextWatcher:Landroid/text/TextWatcher;

.field public inputFilter:Landroid/text/InputFilter;

.field inputTextFocusListener:Landroid/view/View$OnFocusChangeListener;

.field public lengthFilter:Landroid/text/InputFilter$LengthFilter;

.field private mCategotyNamingDialog:Landroid/app/AlertDialog;

.field private mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

.field private mInputText:Landroid/widget/EditText;

.field private mIsShowing:Z

.field private mIsStatePaused:Z

.field private mMemocontentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 56
    const-class v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->TAG:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_categoryUUID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->KEY_CATEGORYUUID:Ljava/lang/String;

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_OriginalName"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->KEY_ORIGINALNAME:Ljava/lang/String;

    .line 62
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    .line 63
    const-string v1, "_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    .line 64
    const-string v2, "UUID"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 65
    const-string v2, "_display_name"

    aput-object v2, v0, v1

    .line 62
    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->PROJECTION:[Ljava/lang/String;

    .line 78
    sput-object v4, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrOriginalName:Ljava/lang/String;

    .line 79
    sput-object v4, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrCategoryUUID:Ljava/lang/String;

    .line 80
    sput-boolean v3, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsAdd:Z

    .line 81
    sput-boolean v3, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsRename:Z

    .line 90
    sput-object v4, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mDlg:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 60
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsStatePaused:Z

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mCategotyNamingDialog:Landroid/app/AlertDialog;

    .line 85
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsShowing:Z

    .line 157
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$1;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->inputTextFocusListener:Landroid/view/View$OnFocusChangeListener;

    .line 179
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$2;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->categoryTextWatcher:Landroid/text/TextWatcher;

    .line 260
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$3;

    const/16 v1, 0x1e

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$3;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;I)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->lengthFilter:Landroid/text/InputFilter$LengthFilter;

    .line 270
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$4;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->inputFilter:Landroid/text/InputFilter;

    .line 369
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$5;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->categoryDialogOnShowListener:Landroid/content/DialogInterface$OnShowListener;

    .line 93
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)V
    .locals 0

    .prologue
    .line 279
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->showIme()V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)V
    .locals 0

    .prologue
    .line 311
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->hideIme()V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 198
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->setPositiveButtonEnabled(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mCategotyNamingDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)V
    .locals 0

    .prologue
    .line 393
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->positiveBtnProcess()V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getDefaultCategoryName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    return-object v0
.end method

.method private checkExistCategoryName(Ljava/lang/String;)Z
    .locals 11
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v10, 0x0

    .line 239
    const v0, 0x7f0b000d

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 241
    .local v9, "strNoneCategoryName":Ljava/lang/String;
    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    :goto_0
    return v6

    .line 244
    :cond_0
    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 245
    .local v8, "strChangedCategory":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "isDeleted IS 0 AND _display_name IS \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 247
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    .line 248
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    .line 247
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 250
    .local v7, "cursorduplicate":Landroid/database/Cursor;
    if-nez v7, :cond_1

    move v6, v10

    .line 251
    goto :goto_0

    .line 253
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 255
    .local v6, "bExist":Z
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .end local v6    # "bExist":Z
    :cond_2
    move v6, v10

    .line 253
    goto :goto_1
.end method

.method private getDefaultCategoryName()Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v4, 0x0

    const v9, 0x7f0b00a0

    const v10, 0x7f0b0019

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 208
    sget-boolean v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsRename:Z

    if-eqz v0, :cond_0

    .line 209
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrOriginalName:Ljava/lang/String;

    .line 235
    :goto_0
    return-object v0

    .line 212
    :cond_0
    const/4 v6, 0x0

    .line 213
    .local v6, "count":I
    const/4 v8, 0x0

    .line 215
    .local v8, "findCategoryName":Z
    :cond_1
    :goto_1
    if-nez v8, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->isAdded()Z

    move-result v0

    if-nez v0, :cond_3

    .line 232
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 233
    if-nez v6, :cond_7

    :goto_2
    new-array v0, v12, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    invoke-virtual {p0, v9, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 216
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "isDeleted IS 0 AND _display_name IS \'"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v6, :cond_5

    move v0, v9

    :goto_3
    new-array v2, v12, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v11

    invoke-virtual {p0, v0, v2}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 218
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    .line 219
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    .line 218
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 221
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_6

    .line 222
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_4

    .line 223
    const/4 v8, 0x1

    .line 225
    :cond_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 229
    :goto_4
    if-nez v8, :cond_1

    .line 230
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .end local v3    # "selection":Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_5
    move v0, v10

    .line 216
    goto :goto_3

    .line 227
    .restart local v3    # "selection":Ljava/lang/String;
    .restart local v7    # "cursor":Landroid/database/Cursor;
    :cond_6
    const/4 v8, 0x1

    goto :goto_4

    .end local v3    # "selection":Ljava/lang/String;
    .end local v7    # "cursor":Landroid/database/Cursor;
    :cond_7
    move v9, v10

    .line 233
    goto :goto_2

    .line 235
    :cond_8
    const-string v0, ""

    goto :goto_0
.end method

.method private hideIme()V
    .locals 3

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 313
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 314
    .local v0, "Imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isActive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 315
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 319
    .end local v0    # "Imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DialogFragment;
    .locals 1
    .param p0, "originalName"    # Ljava/lang/String;
    .param p1, "mUUID"    # Ljava/lang/String;

    .prologue
    .line 98
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mDlg:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrOriginalName:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrOriginalName:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrCategoryUUID:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrCategoryUUID:Ljava/lang/String;

    if-eq v0, p1, :cond_1

    .line 99
    :cond_0
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    invoke-direct {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;-><init>()V

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mDlg:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    .line 100
    sput-object p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrOriginalName:Ljava/lang/String;

    .line 101
    sput-object p1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrCategoryUUID:Ljava/lang/String;

    .line 103
    :cond_1
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mDlg:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    return-object v0
.end method

.method private positiveBtnProcess()V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/16 v0, 0x1e

    .line 395
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 397
    .local v6, "categoryName":Ljava/lang/String;
    invoke-direct {p0, v6}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->checkExistCategoryName(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 398
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 399
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v0, :cond_0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/widget/EditText;->setSelection(II)V

    .line 400
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0020

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 446
    :goto_1
    return-void

    .line 399
    :cond_0
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    .line 404
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->hideIme()V

    .line 405
    const/4 v10, 0x0

    .line 406
    .local v10, "uuid":Ljava/lang/String;
    const/4 v8, 0x0

    .line 408
    .local v8, "cursor":Landroid/database/Cursor;
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 409
    .local v11, "values":Landroid/content/ContentValues;
    sget-boolean v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsAdd:Z

    if-eqz v0, :cond_6

    .line 411
    const/4 v7, 0x0

    .line 413
    .local v7, "cnt":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    sget-object v2, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->PROJECTION:[Ljava/lang/String;

    const-string v3, "isDeleted IS 0"

    const-string v5, "orderBy ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 414
    invoke-static {}, Lcom/samsung/android/app/memo/util/UUIDHelper;->newUUID()Ljava/lang/String;

    move-result-object v10

    .line 416
    if-eqz v8, :cond_2

    .line 417
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 420
    :cond_2
    if-eqz v11, :cond_3

    .line 421
    const-string v0, "UUID"

    invoke-virtual {v11, v0, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    const-string v0, "orderBy"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 423
    const-string v0, "_display_name"

    invoke-virtual {v11, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {v0, v1, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 429
    :cond_3
    if-eqz v8, :cond_4

    .line 430
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 442
    .end local v7    # "cnt":I
    :cond_4
    :goto_2
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->onDialogResultListener:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;

    if-eqz v0, :cond_5

    .line 443
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->onDialogResultListener:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;

    invoke-interface {v0, v10, v6}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;->onPositiveBtnClick(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mCategotyNamingDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_1

    .line 433
    :cond_6
    sget-boolean v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsRename:Z

    if-eqz v0, :cond_4

    .line 435
    sget-object v10, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrCategoryUUID:Ljava/lang/String;

    .line 436
    const-string v0, "_display_name"

    invoke-virtual {v11, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    sput-object v6, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrOriginalName:Ljava/lang/String;

    .line 438
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UUID IS \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 439
    .local v9, "selectionByUUID":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {v0, v1, v11, v9, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_2
.end method

.method private setNamingDialogType()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 169
    sput-boolean v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsAdd:Z

    .line 170
    sput-boolean v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsRename:Z

    .line 172
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrOriginalName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 173
    sput-boolean v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsAdd:Z

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_0
    sput-boolean v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsRename:Z

    goto :goto_0
.end method

.method private setPositiveButtonEnabled(Ljava/lang/String;)V
    .locals 2
    .param p1, "categoryName"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 200
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrOriginalName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mCategotyNamingDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 204
    :goto_0
    return-void

    .line 203
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mCategotyNamingDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method private showIme()V
    .locals 6

    .prologue
    .line 281
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsStatePaused:Z

    if-eqz v1, :cond_0

    .line 309
    :goto_0
    return-void

    .line 284
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 285
    .local v0, "mActivity":Landroid/app/Activity;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 286
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$7;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;Landroid/app/Activity;)V

    .line 304
    const-wide/16 v4, 0x64

    .line 286
    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 306
    :cond_1
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->TAG:Ljava/lang/String;

    const-string v2, "showIme error"

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getOnDialogResultListener()Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;
    .locals 1

    .prologue
    .line 509
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->onDialogResultListener:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;

    return-object v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 366
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsShowing:Z

    return v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 116
    if-eqz p1, :cond_0

    sget-boolean v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsRename:Z

    if-eqz v1, :cond_0

    .line 117
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->KEY_CATEGORYUUID:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrCategoryUUID:Ljava/lang/String;

    .line 118
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->KEY_ORIGINALNAME:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrOriginalName:Ljava/lang/String;

    .line 120
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v1

    if-nez v1, :cond_1

    .line 121
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const v2, 0x1020002

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mMemocontentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    .line 122
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mMemocontentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    if-eqz v1, :cond_1

    .line 123
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mMemocontentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getEditor()Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    .line 124
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    if-eqz v1, :cond_1

    .line 125
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->unregisterClipboard()V

    .line 128
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040008

    invoke-virtual {v1, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 129
    .local v0, "v":Landroid/view/View;
    const v1, 0x7f0e0007

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;

    .line 130
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;

    const-string v2, "inputType=PredictionOff;disableEmoticonInput=true"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 131
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;

    const/16 v2, 0x4001

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 132
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->categoryTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 133
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/text/InputFilter;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->inputFilter:Landroid/text/InputFilter;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->lengthFilter:Landroid/text/InputFilter$LengthFilter;

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 134
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->inputTextFocusListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 137
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->setNamingDialogType()V

    .line 139
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 140
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 141
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 142
    sget-boolean v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsAdd:Z

    if-eqz v1, :cond_2

    const v1, 0x7f0b0047

    :goto_0
    invoke-virtual {v2, v1, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 143
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$6;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$6;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 148
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 139
    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mCategotyNamingDialog:Landroid/app/AlertDialog;

    .line 150
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mCategotyNamingDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->categoryDialogOnShowListener:Landroid/content/DialogInterface$OnShowListener;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 151
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mCategotyNamingDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 152
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mCategotyNamingDialog:Landroid/app/AlertDialog;

    sget-boolean v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsAdd:Z

    if-eqz v1, :cond_3

    const v1, 0x7f0b001c

    :goto_1
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 154
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mCategotyNamingDialog:Landroid/app/AlertDialog;

    return-object v1

    .line 142
    :cond_2
    const v1, 0x7f0b0003

    goto :goto_0

    .line 152
    :cond_3
    const v1, 0x7f0b001e

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mMemocontentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->registerClipboard()V

    .line 363
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 364
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 356
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsShowing:Z

    .line 357
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 358
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsStatePaused:Z

    .line 324
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 325
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mDlg:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    if-eqz v0, :cond_0

    .line 326
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mDlg:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->dismiss()V

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mInputText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 328
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->hideIme()V

    .line 329
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 333
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsStatePaused:Z

    .line 334
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mDlg:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    .line 335
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 336
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->showIme()V

    .line 337
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 108
    sget-boolean v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsRename:Z

    if-eqz v0, :cond_0

    .line 109
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->KEY_CATEGORYUUID:Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrCategoryUUID:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->KEY_ORIGINALNAME:Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mStrOriginalName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 113
    return-void
.end method

.method public setOnDialogResultListener(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;

    .prologue
    .line 506
    sput-object p1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->onDialogResultListener:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;

    .line 507
    return-void
.end method

.method public show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    .locals 4
    .param p1, "manager"    # Landroid/app/FragmentManager;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 341
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsStatePaused:Z

    .line 342
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsShowing:Z

    if-nez v1, :cond_0

    .line 343
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->mIsShowing:Z

    .line 344
    if-eqz p1, :cond_0

    .line 346
    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 347
    :catch_0
    move-exception v0

    .line 348
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "IllegalStateException :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
