.class Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;
.super Ljava/lang/Object;
.source "VoiceViewLayout.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final format:Ljava/lang/String;

.field final hourFormat:Ljava/lang/String;

.field progressTime:J

.field selectedTime:J

.field state:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

.field timeString_played:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 1491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1493
    const-string v0, "%02d:%02d:%02d"

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->hourFormat:Ljava/lang/String;

    .line 1494
    const-string v0, "%02d:%02d:%02d"

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->format:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 10
    .param p1, "bar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromuser"    # Z

    .prologue
    const-wide/16 v6, 0x3e8

    const-wide/16 v8, 0x3c

    .line 1510
    if-eqz p3, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mPlayingSeekbar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$6(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SeekBar;->isPressed()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1511
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v2

    iget-wide v0, v2, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTotalTime:J

    .line 1512
    .local v0, "totalTime":J
    int-to-long v2, p2

    mul-long/2addr v2, v0

    div-long/2addr v2, v6

    iput-wide v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->progressTime:J

    .line 1513
    int-to-long v2, p2

    div-long v4, v0, v6

    mul-long/2addr v2, v4

    div-long/2addr v2, v6

    iput-wide v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->selectedTime:J

    .line 1514
    const-string v2, "%02d:%02d:%02d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 1515
    iget-wide v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->selectedTime:J

    div-long/2addr v6, v8

    div-long/2addr v6, v8

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-wide v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->selectedTime:J

    div-long/2addr v6, v8

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 1516
    iget-wide v6, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->selectedTime:J

    rem-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1514
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->timeString_played:Ljava/lang/String;

    .line 1517
    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mplayingStartDuration:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$32(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->timeString_played:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1519
    .end local v0    # "totalTime":J
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "bar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1502
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mState:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$30(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->state:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    .line 1503
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$13(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->updateTime:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$31(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1504
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->pauseSound(Z)V

    .line 1505
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4
    .param p1, "bar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1523
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->progressTime:J

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->playerSeekTo(I)Z

    .line 1524
    sget-object v0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->state:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    if-ne v0, v1, :cond_0

    .line 1525
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$6;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceManger:Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;
    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$9(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceRecorderBase;->startSound()V

    .line 1527
    :cond_0
    return-void
.end method
