.class public Lcom/samsung/android/app/memo/share/beam/BeamHelper;
.super Ljava/lang/Object;
.source "BeamHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/share/beam/BeamHelper$MemoContentInterfaceForBeam;,
        Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;,
        Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCompleteCallback;,
        Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;
    }
.end annotation


# static fields
.field private static final ACTION_ABEAM_POPUP:Ljava/lang/String; = "com.android.nfc.AndroidBeamPopUp"

.field private static final ACTION_POPUP:Ljava/lang/String; = "com.sec.android.directshare.DirectSharePopUp"

.field private static final ACTION_START:Ljava/lang/String; = "com.sec.android.directshare.DIRECT_SHARE_START_ACTION"

.field private static final EXTRA_KEY_POPUP_MODE:Ljava/lang/String; = "POPUP_MODE"

.field private static final EXTRA_VAL_CLOUD_FILE:Ljava/lang/String; = "from_cloud_file"

.field private static final EXTRA_VAL_DRM_FILE:Ljava/lang/String; = "from_drm_file"

.field private static final EXTRA_VAL_NOT_SAVED:Ljava/lang/String; = "does_not_saved"

.field private static final EXTRA_VAL_NO_FILE:Ljava/lang/String; = "no_file_selected"

.field private static final KEY_SBEAM_ONOFF:Ljava/lang/String; = "SBeam_on_off"

.field private static final KEY_SBEAM_SUPPORT:Ljava/lang/String; = "SBeam_support"

.field private static final PACKAGE_NAME_SBEAM:Ljava/lang/String; = "com.sec.android.directshare"

.field private static final PACKAGE_NAME_SETTINGS:Ljava/lang/String; = "com.android.settings"

.field private static final PREP_NAME_SBEAM:Ljava/lang/String; = "pref_sbeam"

.field private static final SCHEME_FILE:Ljava/lang/String; = "file"

.field private static final STATUS_IS_CLOUD_FILE:I = 0x3

.field private static final STATUS_IS_DRM:I = 0x4

.field private static final STATUS_NONE:I = 0x0

.field private static final STATUS_NO_FILE_SELECTED:I = 0x2

.field private static final STATUS_PUSH:I = 0x1

.field private static final TAG:Ljava/lang/String; = "BeamHelper"

.field private static final TAGClass:Ljava/lang/String; = "BeamHelper."


# instance fields
.field private final STATUS_NEED_SAVED:I

.field private mContext:Landroid/content/Context;

.field private mMimeType:Ljava/lang/String;

.field private mNdefStatus:I

.field private mSbeamCompleteCallbck:Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCompleteCallback;

.field private mSbeamPushCallbck:Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;

.field private mSbeamUrisCallbck:Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;

.field private mUris:[Landroid/net/Uri;

.field private mbSupportedSbeam:Z

.field private nfcAdapter:Landroid/nfc/NfcAdapter;

.field private xmlUtils:Lcom/samsung/android/app/memo/share/beam/XMLUtills;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appMime"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mMimeType:Ljava/lang/String;

    .line 50
    iput-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;

    .line 51
    iput-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mSbeamPushCallbck:Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;

    .line 52
    iput-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mSbeamCompleteCallbck:Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCompleteCallback;

    .line 53
    iput-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mSbeamUrisCallbck:Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;

    .line 54
    iput-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mUris:[Landroid/net/Uri;

    .line 55
    iput-boolean v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mbSupportedSbeam:Z

    .line 61
    iput v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I

    .line 67
    iput v6, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->STATUS_NEED_SAVED:I

    .line 88
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;

    .line 89
    iput-object p2, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mMimeType:Ljava/lang/String;

    .line 91
    const/4 v3, 0x0

    .line 93
    .local v3, "settingContext":Landroid/content/Context;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;

    const-string v5, "com.android.settings"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v3

    .line 94
    const-string v4, "pref_sbeam"

    .line 95
    const/4 v5, 0x5

    .line 94
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 97
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string v4, "SBeam_support"

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mbSupportedSbeam:Z

    .line 98
    const-string v5, "BeamHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v4, "BeamHelper.SBeam is "

    invoke-direct {v6, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mbSupportedSbeam:Z

    if-eqz v4, :cond_1

    const-string v4, "supported"

    :goto_0
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iget-boolean v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mbSupportedSbeam:Z

    if-eqz v4, :cond_0

    .line 101
    new-instance v4, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;-><init>(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)V

    iput-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mSbeamPushCallbck:Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;

    .line 102
    new-instance v4, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCompleteCallback;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCompleteCallback;-><init>(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)V

    iput-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mSbeamCompleteCallbck:Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCompleteCallback;

    .line 105
    :cond_0
    new-instance v4, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;-><init>(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)V

    iput-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mSbeamUrisCallbck:Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;

    .line 106
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->nfcAdapter:Landroid/nfc/NfcAdapter;

    .line 107
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->nfcAdapter:Landroid/nfc/NfcAdapter;

    if-nez v4, :cond_2

    .line 108
    const-string v4, "BeamHelper"

    const-string v5, "BeamHelper.setBeamUris > can\'t load nfcadpater"

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    .end local p1    # "context":Landroid/content/Context;
    :goto_1
    return-void

    .line 98
    .restart local v2    # "pref":Landroid/content/SharedPreferences;
    .restart local p1    # "context":Landroid/content/Context;
    :cond_1
    const-string v4, "not supported"

    goto :goto_0

    .line 112
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->isSbeamOn(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 113
    const-string v4, "BeamHelper"

    const-string v5, "setSbeam"

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->nfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v6, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mSbeamPushCallbck:Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCallback;

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    move-object v4, v0

    const/4 v7, 0x0

    new-array v7, v7, [Landroid/app/Activity;

    invoke-virtual {v5, v6, v4, v7}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 115
    iget-object v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->nfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v6, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mSbeamCompleteCallbck:Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCompleteCallback;

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    move-object v4, v0

    const/4 v7, 0x0

    new-array v7, v7, [Landroid/app/Activity;

    invoke-virtual {v5, v6, v4, v7}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 116
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->nfcAdapter:Landroid/nfc/NfcAdapter;

    const/4 v5, 0x0

    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {v4, v5, p1}, Landroid/nfc/NfcAdapter;->setBeamPushUrisCallback(Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;Landroid/app/Activity;)V

    .line 124
    :goto_2
    new-instance v4, Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    iget-object v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->xmlUtils:Lcom/samsung/android/app/memo/share/beam/XMLUtills;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 125
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :catch_0
    move-exception v1

    .line 126
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "BeamHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "BeamHelper.NameNotFoundException > "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 119
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "pref":Landroid/content/SharedPreferences;
    .restart local p1    # "context":Landroid/content/Context;
    :cond_3
    :try_start_1
    const-string v4, "BeamHelper"

    const-string v5, "setAbeam"

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    iget-object v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->nfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v6, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mSbeamUrisCallbck:Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamUrisCallback;

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    move-object v4, v0

    invoke-virtual {v5, v6, v4}, Landroid/nfc/NfcAdapter;->setBeamPushUrisCallback(Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;Landroid/app/Activity;)V

    .line 121
    iget-object v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->nfcAdapter:Landroid/nfc/NfcAdapter;

    const/4 v6, 0x0

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    move-object v4, v0

    const/4 v7, 0x0

    new-array v7, v7, [Landroid/app/Activity;

    invoke-virtual {v5, v6, v4, v7}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 122
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->nfcAdapter:Landroid/nfc/NfcAdapter;

    const/4 v5, 0x0

    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    const/4 v6, 0x0

    new-array v6, v6, [Landroid/app/Activity;

    invoke-virtual {v4, v5, p1, v6}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 127
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :catch_1
    move-exception v1

    .line 128
    .local v1, "e":Ljava/lang/SecurityException;
    const-string v4, "BeamHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "BeamHelper.SecurityException > "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)[Ljava/io/File;
    .locals 1

    .prologue
    .line 353
    invoke-direct {p0}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->getFiles()[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/share/beam/BeamHelper;I)V
    .locals 0

    .prologue
    .line 61
    iput p1, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)[Landroid/net/Uri;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mUris:[Landroid/net/Uri;

    return-object v0
.end method

.method private getFiles()[Ljava/io/File;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 354
    iget-object v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mUris:[Landroid/net/Uri;

    if-nez v5, :cond_1

    .line 355
    const/4 v5, 0x2

    iput v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I

    move-object v0, v4

    .line 381
    :cond_0
    :goto_0
    return-object v0

    .line 359
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mUris:[Landroid/net/Uri;

    array-length v5, v5

    new-array v0, v5, [Ljava/io/File;

    .line 361
    .local v0, "files":[Ljava/io/File;
    const/4 v1, 0x0

    .line 362
    .local v1, "i":I
    iget-object v6, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mUris:[Landroid/net/Uri;

    array-length v7, v6

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v7, :cond_0

    aget-object v3, v6, v5

    .line 363
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 365
    .local v2, "scheme":Ljava/lang/String;
    const-string v8, "file"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 366
    invoke-direct {p0, v3}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->isDrmFile(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 367
    const/4 v5, 0x4

    iput v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I

    move-object v0, v4

    .line 368
    goto :goto_0

    .line 369
    :cond_2
    invoke-direct {p0, v3}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->needSaved(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 370
    const/4 v5, 0x5

    iput v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I

    move-object v0, v4

    .line 371
    goto :goto_0

    .line 374
    :cond_3
    const/4 v5, 0x3

    iput v5, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I

    move-object v0, v4

    .line 375
    goto :goto_0

    .line 377
    :cond_4
    new-instance v8, Ljava/io/File;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v8, v0, v1

    .line 378
    add-int/lit8 v1, v1, 0x1

    .line 362
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method private isDrmFile(Landroid/net/Uri;)Z
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 393
    const/4 v0, 0x0

    return v0
.end method

.method private needSaved(Landroid/net/Uri;)Z
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 398
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public createMemoFile()V
    .locals 4

    .prologue
    .line 385
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/samsung/android/app/memo/share/beam/BeamHelper$MemoContentInterfaceForBeam;

    invoke-interface {v1}, Lcom/samsung/android/app/memo/share/beam/BeamHelper$MemoContentInterfaceForBeam;->UpdateContentToCreateMemo()V

    .line 386
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->xmlUtils:Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->createXMLFile(J)Ljava/lang/String;

    move-result-object v0

    .line 387
    .local v0, "path":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mUris:[Landroid/net/Uri;

    .line 388
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mUris:[Landroid/net/Uri;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->setBeamUris([Landroid/net/Uri;)V

    .line 389
    return-void
.end method

.method isSbeamOn(Landroid/content/Context;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 143
    iget-boolean v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mbSupportedSbeam:Z

    if-nez v4, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v3

    .line 147
    :cond_1
    const/4 v2, 0x0

    .line 149
    .local v2, "settingContext":Landroid/content/Context;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;

    .line 150
    const-string v5, "com.android.settings"

    const/4 v6, 0x0

    .line 149
    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 156
    :goto_1
    if-eqz v2, :cond_0

    .line 158
    const-string v4, "pref_sbeam"

    const/4 v5, 0x5

    .line 157
    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 161
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v4, "SBeam_on_off"

    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    goto :goto_0

    .line 151
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "BeamHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "BeamHelper.SbeamHelper:NameNotFoundException > "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 153
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 154
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v4, "BeamHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "BeamHelper.SbeamHelper:SecurityException > "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setBeamUris([Landroid/net/Uri;)V
    .locals 1
    .param p1, "uris"    # [Landroid/net/Uri;

    .prologue
    .line 138
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I

    .line 139
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mUris:[Landroid/net/Uri;

    .line 140
    return-void
.end method
