.class public Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;
.super Landroid/app/DialogFragment;
.source "CategoryMoveDialog.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/DialogFragment;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final CID_DISPLAY_NAME:I = 0x2

.field private static final CID_UUID:I = 0x1

.field private static final ORDERBY:Ljava/lang/String; = "orderBy ASC"

.field private static final PROJECTION:[Ljava/lang/String;

.field private static mCategoryMoveIDs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static mCurrentCategoryUUID:Ljava/lang/String;


# instance fields
.field private final CATEGORY_COUNT:I

.field private final MEMO_MOVE_COUNT:I

.field private mAdapter:Landroid/widget/CursorAdapter;

.field private mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

.field private mDialog:Landroid/app/Dialog;

.field private mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

.field private mPopuplist:Landroid/widget/ListView;

.field private mSelectCategoryUUID:Ljava/lang/String;

.field private mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 36
    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 37
    const-string v2, "UUID"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 38
    const-string v2, "_display_name"

    aput-object v2, v0, v1

    .line 35
    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->PROJECTION:[Ljava/lang/String;

    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mCurrentCategoryUUID:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mCategoryMoveIDs:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 44
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->MEMO_MOVE_COUNT:I

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->CATEGORY_COUNT:I

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mSelectCategoryUUID:Ljava/lang/String;

    .line 34
    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;)Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    return-object v0
.end method

.method static synthetic access$3()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mCategoryMoveIDs:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;)Landroid/widget/Toast;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mSelectCategoryUUID:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mSelectCategoryUUID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method public static newInstance(Ljava/util/ArrayList;Ljava/lang/String;)Landroid/app/DialogFragment;
    .locals 2
    .param p1, "currentCategoryUUID"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/app/DialogFragment;"
        }
    .end annotation

    .prologue
    .line 69
    .local p0, "memoIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    sput-object p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mCategoryMoveIDs:Ljava/util/ArrayList;

    .line 70
    sput-object p1, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mCurrentCategoryUUID:Ljava/lang/String;

    .line 72
    new-instance v1, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    invoke-direct {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;-><init>()V

    .line 73
    .local v1, "dlg":Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 74
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {v1, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->setArguments(Landroid/os/Bundle;)V

    .line 76
    return-object v1
.end method


# virtual methods
.method public getCategoryNamingDialog()Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    return-object v0
.end method

.method protected getCursorAdapter()Landroid/widget/CursorAdapter;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 164
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$3;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f04000d

    const/4 v4, 0x0

    .line 165
    new-array v5, v8, [Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x2

    aget-object v1, v1, v6

    aput-object v1, v5, v7

    .line 166
    new-array v6, v8, [I

    const v1, 0x7f0e0010

    aput v1, v6, v7

    move-object v1, p0

    .line 164
    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$3;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 153
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 154
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 155
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 178
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 179
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->setCategoryMoveDialog()V

    .line 180
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v12, 0x7f0e0012

    const/16 v8, 0x8

    const/4 v11, 0x0

    const/4 v7, 0x0

    const/4 v10, 0x1

    .line 80
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 82
    .local v1, "context":Landroid/app/Activity;
    :try_start_0
    move-object v0, v1

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-object v6, v0

    iput-object v6, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v9, 0x7f04000e

    invoke-virtual {v6, v9, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 88
    .local v5, "popupView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v9, 0x7f04000f

    invoke-virtual {v6, v9, v11, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 89
    .local v4, "headerNone":Landroid/view/View;
    const-string v6, ""

    invoke-virtual {v4, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 91
    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    iput-object v6, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mPopuplist:Landroid/widget/ListView;

    .line 93
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mCurrentCategoryUUID:Ljava/lang/String;

    const-string v9, ""

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 94
    sget v6, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    if-ne v6, v10, :cond_0

    .line 95
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mPopuplist:Landroid/widget/ListView;

    invoke-virtual {v6, v4}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 97
    :cond_0
    invoke-static {v1}, Lcom/samsung/android/app/memo/MemoDataHandler;->getCategoryCount(Landroid/content/Context;)I

    move-result v2

    .line 98
    .local v2, "count":I
    const v6, 0x7f0e0011

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-nez v2, :cond_2

    move v6, v7

    :goto_0
    invoke-virtual {v9, v6}, Landroid/view/View;->setVisibility(I)V

    .line 99
    invoke-virtual {v5, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-lez v2, :cond_1

    move v8, v7

    :cond_1
    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 104
    .end local v2    # "count":I
    :goto_1
    sget-object v6, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mCategoryMoveIDs:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-le v6, v10, :cond_4

    .line 105
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f0b00bc

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v8, v10, [Ljava/lang/Object;

    sget-object v9, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mCategoryMoveIDs:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v7

    invoke-static {v6, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mToast:Landroid/widget/Toast;

    .line 110
    :goto_2
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-direct {v6, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 111
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b00ba

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 112
    invoke-virtual {v6, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 113
    const v7, 0x7f0b001b

    new-instance v8, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1;

    invoke-direct {v8, p0, v1}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;Landroid/app/Activity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 131
    const v7, 0x7f0b0005

    invoke-virtual {v6, v7, v11}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    .line 110
    iput-object v6, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mDialog:Landroid/app/Dialog;

    .line 133
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getCursorAdapter()Landroid/widget/CursorAdapter;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mAdapter:Landroid/widget/CursorAdapter;

    .line 134
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mPopuplist:Landroid/widget/ListView;

    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 135
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mPopuplist:Landroid/widget/ListView;

    new-instance v7, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$2;

    invoke-direct {v7, p0, v1}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$2;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;Landroid/app/Activity;)V

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 148
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mDialog:Landroid/app/Dialog;

    return-object v6

    .line 83
    .end local v4    # "headerNone":Landroid/view/View;
    .end local v5    # "popupView":Landroid/view/View;
    :catch_0
    move-exception v3

    .line 84
    .local v3, "e":Ljava/lang/ClassCastException;
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 85
    const-string v8, " must implement MemoListActionListener"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 84
    invoke-direct {v6, v7, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    .end local v3    # "e":Ljava/lang/ClassCastException;
    .restart local v2    # "count":I
    .restart local v4    # "headerNone":Landroid/view/View;
    .restart local v5    # "popupView":Landroid/view/View;
    :cond_2
    move v6, v8

    .line 98
    goto/16 :goto_0

    .line 101
    .end local v2    # "count":I
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mPopuplist:Landroid/widget/ListView;

    invoke-virtual {v6, v4}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 107
    :cond_4
    const v6, 0x7f0b00bb

    invoke-static {v1, v6, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mToast:Landroid/widget/Toast;

    goto :goto_2
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UUID IS NOT \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mCurrentCategoryUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 160
    .local v4, "selection":Ljava/lang/String;
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    sget-object v3, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "orderBy ASC"

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 184
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mAdapter:Landroid/widget/CursorAdapter;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v0, p2}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 186
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->setCategoryMoveDialog()V

    .line 188
    :cond_0
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Landroid/content/Loader;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 211
    .local p1, "arg0":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mAdapter:Landroid/widget/CursorAdapter;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mAdapter:Landroid/widget/CursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 214
    :cond_0
    return-void
.end method

.method setCategoryMoveDialog()V
    .locals 7

    .prologue
    .line 191
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 192
    .local v4, "windowLayoutParams":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "positionX"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 193
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "positionY"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 195
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 197
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 198
    .local v3, "maxListLine":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090146

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v2, v5

    .line 200
    .local v2, "itemHeight":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090017

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v0, v5

    .line 201
    .local v0, "bubbleTopMargin":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v1, v5, Landroid/util/DisplayMetrics;->density:F

    .line 203
    .local v1, "density":F
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v5}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    if-le v5, v3, :cond_0

    .line 204
    mul-int v5, v2, v3

    add-int/2addr v5, v0

    int-to-float v6, v3

    mul-float/2addr v6, v1

    float-to-int v6, v6

    add-int/2addr v5, v6

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 206
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 207
    return-void
.end method
