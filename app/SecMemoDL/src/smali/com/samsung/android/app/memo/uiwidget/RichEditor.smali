.class public Lcom/samsung/android/app/memo/uiwidget/RichEditor;
.super Landroid/widget/EditText;
.source "RichEditor.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;
.implements Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uiwidget/RichEditor$EditViewModeListener;,
        Lcom/samsung/android/app/memo/uiwidget/RichEditor$OnSelectionChangedListener;,
        Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;
    }
.end annotation


# static fields
.field public static final IMAGESPAN_TEXT:Ljava/lang/String; = "\n \n"

.field public static final IMAGESPAN_TEXT_NOT_LINE_CHANGE:Ljava/lang/String; = " \n"

.field private static final MESSAGE_REVOKE_PERMISSION:I = 0x79

.field public static final MIME_TYPE_IMAGE_ALL:Ljava/lang/String; = "image/*"

.field public static final MIME_TYPE_TEXT_PLAIN:Ljava/lang/String; = "text/plain"

.field public static final MULTIWINDOW_CLIPDATA_LABEL:Ljava/lang/String; = "MultiWindow_DragDrop_Memo"

.field private static final REVOKE_DELAY:I = 0x186a0

.field private static final TAG:Ljava/lang/String;

.field private static isAddPermissionRunning:Z

.field public static isFromImageDelete:Z

.field public static mImagePasteFailed:Z

.field public static mIsClipboardRegistered:Z

.field public static mMaxCharSize:I

.field public static mToolbarView:Landroid/view/View;

.field public static ongoingAsynctask:Z

.field private static revokeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final ID_CLIP_BOARD:I

.field private final ID_COPY:I

.field private final ID_CUT:I

.field private final ID_DICTIONARY:I

.field private final ID_PASTE:I

.field private final ID_REDO:I

.field private final ID_SELECTALL:I

.field private final ID_UNDO:I

.field private MAX_IMAGE_LIMIT:I

.field private Redoable:Z

.field private final SPAN_INDEX_LIMIT:I

.field private Undoable:Z

.field private currentImageCount:I

.field filter:Landroid/text/InputFilter;

.field private isLongClick:Z

.field private isMaxImageReached:Z

.field keyboardShownHandler:Landroid/os/Handler;

.field private linkMovementMethod:Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;

.field private mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

.field mClipboardExServiceImpl:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

.field mClipboardServiceImpl:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

.field private mClipboardservice:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

.field private mContext:Landroid/content/Context;

.field private mDragBitmap:Landroid/graphics/Bitmap;

.field private mEditViewModeListener:Lcom/samsung/android/app/memo/uiwidget/RichEditor$EditViewModeListener;

.field private mIsEditMode:Z

.field private mIsTextPasteOperationGoing:Z

.field private mOnSelectionChangedListener:Lcom/samsung/android/app/memo/uiwidget/RichEditor$OnSelectionChangedListener;

.field private mRevokeHandler:Landroid/os/Handler;

.field private mSession:Lcom/samsung/android/app/memo/Session;

.field mShowImageNotFoundHandler:Landroid/os/Handler;

.field private mSpanOnClickListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

.field private mStrSelection:Ljava/lang/CharSequence;

.field private max:I

.field private min:I

.field private redoData:Ljava/lang/String;

.field private redoSelection:I

.field private spanEnd:I

.field private spanStart:I

.field private undoData:Ljava/lang/String;

.field private undoSelection:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 87
    const-class v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;

    .line 141
    const/16 v0, 0x1000

    sput v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mMaxCharSize:I

    .line 145
    sput-boolean v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsClipboardRegistered:Z

    .line 149
    sput-boolean v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isAddPermissionRunning:Z

    .line 153
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->revokeList:Ljava/util/ArrayList;

    .line 166
    sput-boolean v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isFromImageDelete:Z

    .line 168
    sput-boolean v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mImagePasteFailed:Z

    .line 170
    sput-boolean v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ongoingAsynctask:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 180
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 95
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsTextPasteOperationGoing:Z

    .line 103
    const v0, 0x102040a

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_CLIP_BOARD:I

    .line 105
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "cut"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_CUT:I

    .line 107
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "copy"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_COPY:I

    .line 109
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "paste"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_PASTE:I

    .line 111
    const v0, 0x102001f

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_SELECTALL:I

    .line 113
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "dictionary"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_DICTIONARY:I

    .line 115
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "redo"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_REDO:I

    .line 117
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "undo"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_UNDO:I

    .line 119
    const/16 v0, 0x1000

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->SPAN_INDEX_LIMIT:I

    .line 129
    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSpanOnClickListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    .line 131
    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mEditViewModeListener:Lcom/samsung/android/app/memo/uiwidget/RichEditor$EditViewModeListener;

    .line 135
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsEditMode:Z

    .line 139
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->MAX_IMAGE_LIMIT:I

    .line 158
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoData:Ljava/lang/String;

    .line 159
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->redoData:Ljava/lang/String;

    .line 160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Undoable:Z

    .line 161
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Redoable:Z

    .line 164
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isLongClick:Z

    .line 169
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isMaxImageReached:Z

    .line 171
    iput v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->currentImageCount:I

    .line 456
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$1;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->filter:Landroid/text/InputFilter;

    .line 540
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$2;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->keyboardShownHandler:Landroid/os/Handler;

    .line 807
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardExServiceImpl:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    .line 1099
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardServiceImpl:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    .line 1244
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$5;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mRevokeHandler:Landroid/os/Handler;

    .line 1447
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$6;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mShowImageNotFoundHandler:Landroid/os/Handler;

    .line 181
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->initialize()V

    .line 182
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 185
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 95
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsTextPasteOperationGoing:Z

    .line 103
    const v0, 0x102040a

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_CLIP_BOARD:I

    .line 105
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "cut"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_CUT:I

    .line 107
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "copy"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_COPY:I

    .line 109
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "paste"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_PASTE:I

    .line 111
    const v0, 0x102001f

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_SELECTALL:I

    .line 113
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "dictionary"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_DICTIONARY:I

    .line 115
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "redo"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_REDO:I

    .line 117
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "undo"

    const-string v2, "id"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_UNDO:I

    .line 119
    const/16 v0, 0x1000

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->SPAN_INDEX_LIMIT:I

    .line 129
    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSpanOnClickListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    .line 131
    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mEditViewModeListener:Lcom/samsung/android/app/memo/uiwidget/RichEditor$EditViewModeListener;

    .line 135
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsEditMode:Z

    .line 139
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->MAX_IMAGE_LIMIT:I

    .line 158
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoData:Ljava/lang/String;

    .line 159
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->redoData:Ljava/lang/String;

    .line 160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Undoable:Z

    .line 161
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Redoable:Z

    .line 164
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isLongClick:Z

    .line 169
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isMaxImageReached:Z

    .line 171
    iput v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->currentImageCount:I

    .line 456
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$1;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->filter:Landroid/text/InputFilter;

    .line 540
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$2;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->keyboardShownHandler:Landroid/os/Handler;

    .line 807
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$3;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardExServiceImpl:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    .line 1099
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$4;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardServiceImpl:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    .line 1244
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$5;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mRevokeHandler:Landroid/os/Handler;

    .line 1447
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$6;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mShowImageNotFoundHandler:Landroid/os/Handler;

    .line 186
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->initialize()V

    .line 187
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsEditMode:Z

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Z
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isMaxImageReached:Z

    return v0
.end method

.method static synthetic access$12(Lcom/samsung/android/app/memo/uiwidget/RichEditor;I)V
    .locals 0

    .prologue
    .line 99
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    return-void
.end method

.method static synthetic access$13(Lcom/samsung/android/app/memo/uiwidget/RichEditor;I)V
    .locals 0

    .prologue
    .line 99
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    return v0
.end method

.method static synthetic access$15(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    return v0
.end method

.method static synthetic access$16(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 1207
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->addPermission(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Landroid/content/ClipData;)V
    .locals 0

    .prologue
    .line 694
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setPrimaryClip(Landroid/content/ClipData;)V

    return-void
.end method

.method static synthetic access$18()Z
    .locals 1

    .prologue
    .line 149
    sget-boolean v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isAddPermissionRunning:Z

    return v0
.end method

.method static synthetic access$19()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->revokeList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Z)V
    .locals 0

    .prologue
    .line 135
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsEditMode:Z

    return-void
.end method

.method static synthetic access$20(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 1260
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->revokePermission(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$21(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 153
    sput-object p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->revokeList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$22(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;

    return-void
.end method

.method static synthetic access$23(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Lcom/samsung/android/app/memo/uiwidget/ClipboardService;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardservice:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    return-object v0
.end method

.method static synthetic access$24(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$25(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mDragBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->spanStart:I

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->spanEnd:I

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Z)V
    .locals 0

    .prologue
    .line 169
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isMaxImageReached:Z

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/lang/String;Landroid/text/SpannableStringBuilder;IIZ)V
    .locals 0

    .prologue
    .line 1453
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->addHtmlText(Ljava/lang/String;Landroid/text/SpannableStringBuilder;IIZ)V

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Landroid/net/Uri;Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 793
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getRotatedDegree(Landroid/net/Uri;Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Lcom/samsung/android/app/memo/Session;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSession:Lcom/samsung/android/app/memo/Session;

    return-object v0
.end method

.method static synthetic access$9()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private addAlignment()Landroid/text/style/AlignmentSpan$Standard;
    .locals 2

    .prologue
    .line 534
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 535
    new-instance v0, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_LEFT:Landroid/text/Layout$Alignment;

    invoke-direct {v0, v1}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 537
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v0, v1}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    goto :goto_0
.end method

.method private addHtmlText(Ljava/lang/String;Landroid/text/SpannableStringBuilder;IIZ)V
    .locals 9
    .param p1, "htmlStr"    # Ljava/lang/String;
    .param p2, "spn"    # Landroid/text/SpannableStringBuilder;
    .param p3, "min"    # I
    .param p4, "max"    # I
    .param p5, "fromClipboard"    # Z

    .prologue
    .line 1454
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    move-object v7, p0

    move v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/lang/String;Landroid/content/Context;Landroid/text/SpannableStringBuilder;IILcom/samsung/android/app/memo/uiwidget/RichEditor;Z)V

    .line 1455
    .local v0, "AddHtmlTextTask":Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;
    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1456
    return-void
.end method

.method private addPermission(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "copyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1209
    sput-boolean v9, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isAddPermissionRunning:Z

    .line 1211
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    if-eqz v6, :cond_0

    if-nez p1, :cond_1

    .line 1212
    :cond_0
    sput-boolean v8, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isAddPermissionRunning:Z

    .line 1242
    :goto_0
    return-void

    .line 1217
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 1218
    .local v4, "pm":Landroid/content/pm/PackageManager;
    const/4 v3, 0x0

    .line 1220
    .local v3, "packs":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    if-eqz v4, :cond_2

    .line 1221
    invoke-virtual {v4, v8}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v3

    .line 1223
    :cond_2
    if-eqz v3, :cond_3

    .line 1224
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    if-lt v0, v6, :cond_4

    .line 1240
    .end local v0    # "i":I
    :cond_3
    sput-boolean v8, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isAddPermissionRunning:Z

    goto :goto_0

    .line 1225
    .restart local v0    # "i":I
    :cond_4
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageInfo;

    .line 1227
    .local v1, "p":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_5

    iget-object v6, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-eqz v6, :cond_5

    iget-object v6, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-nez v6, :cond_6

    .line 1224
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1230
    :cond_6
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 1232
    .local v2, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    .line 1234
    .local v5, "uri":Landroid/net/Uri;
    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v2, v5, v9}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    goto :goto_2
.end method

.method public static exifOrientationToDegrees(I)F
    .locals 1
    .param p0, "exifOrientation"    # I

    .prologue
    .line 783
    const/4 v0, 0x6

    if-ne p0, v0, :cond_0

    .line 784
    const/high16 v0, 0x42b40000    # 90.0f

    .line 790
    :goto_0
    return v0

    .line 785
    :cond_0
    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    .line 786
    const/high16 v0, 0x43340000    # 180.0f

    goto :goto_0

    .line 787
    :cond_1
    const/16 v0, 0x8

    if-ne p0, v0, :cond_2

    .line 788
    const/high16 v0, 0x43870000    # 270.0f

    goto :goto_0

    .line 790
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getInsertButtonView(Landroid/view/View;)V
    .locals 0
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 1611
    sput-object p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mToolbarView:Landroid/view/View;

    .line 1612
    return-void
.end method

.method private getRotatedDegree(Landroid/net/Uri;Landroid/content/Context;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 794
    const/4 v0, 0x0

    .line 796
    .local v0, "degree":I
    if-eqz p1, :cond_0

    :try_start_0
    const-string v3, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 797
    new-instance v2, Landroid/media/ExifInterface;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 799
    .local v2, "exif":Landroid/media/ExifInterface;
    const-string v3, "Orientation"

    const/4 v4, 0x1

    .line 798
    invoke-virtual {v2, v3, v4}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->exifOrientationToDegrees(I)F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    float-to-int v0, v3

    .line 804
    .end local v2    # "exif":Landroid/media/ExifInterface;
    :cond_0
    :goto_0
    return v0

    .line 801
    :catch_0
    move-exception v1

    .line 802
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " getRotatedDegree Exception :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getScreenViewBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "v"    # Landroid/view/View;

    .prologue
    const/16 v7, 0x3e8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1768
    invoke-virtual {p0, v6}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 1769
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 1770
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 1769
    invoke-virtual {p0, v3, v4}, Landroid/view/View;->measure(II)V

    .line 1771
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-static {v7, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1772
    .local v1, "height":I
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    invoke-static {v7, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1774
    .local v2, "width":I
    invoke-virtual {p0, v5, v5, v2, v1}, Landroid/view/View;->layout(IIII)V

    .line 1775
    invoke-virtual {p0, v6}, Landroid/view/View;->buildDrawingCache(Z)V

    .line 1776
    invoke-virtual {p0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1777
    .local v0, "b":Landroid/graphics/Bitmap;
    invoke-virtual {p0, v5}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 1778
    return-object v0
.end method

.method private initialize()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 194
    sget-object v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;

    const-string v1, "initialize()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    invoke-virtual {p0, v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setLinksClickable(Z)V

    .line 196
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/AddNewLineFilter;-><init>(Landroid/widget/EditText;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 197
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsEditMode:Z

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->linkMovementMethod:Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;

    .line 198
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->linkMovementMethod:Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 199
    new-array v0, v3, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    .line 200
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->filter:Landroid/text/InputFilter;

    aput-object v2, v0, v1

    .line 199
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setFilters([Landroid/text/InputFilter;)V

    .line 202
    invoke-virtual {p0, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 204
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    .line 205
    return-void
.end method

.method private newHtmlImageSpan(Landroid/net/Uri;ILjava/lang/String;)Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "orientation"    # I
    .param p3, "alt"    # Ljava/lang/String;

    .prologue
    .line 527
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;-><init>(Landroid/content/Context;Landroid/net/Uri;ILjava/lang/String;)V

    .line 528
    .local v0, "span":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSpanOnClickListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    if-nez v1, :cond_0

    .end local p0    # "this":Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    :goto_0
    invoke-virtual {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->setOnClickListener(Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;)V

    .line 529
    return-object v0

    .line 528
    .restart local p0    # "this":Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    :cond_0
    iget-object p0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSpanOnClickListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    goto :goto_0
.end method

.method private revokePermission(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1262
    .local p1, "revokingList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    sget-boolean v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isAddPermissionRunning:Z

    if-eqz v1, :cond_1

    .line 1270
    :cond_0
    return-void

    .line 1265
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1266
    .local v0, "uri":Landroid/net/Uri;
    sget-boolean v2, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isAddPermissionRunning:Z

    if-nez v2, :cond_0

    .line 1268
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    goto :goto_0
.end method

.method private setPrimaryClip(Landroid/content/ClipData;)V
    .locals 3
    .param p1, "clip"    # Landroid/content/ClipData;

    .prologue
    .line 695
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 696
    const-string v2, "clipboard"

    .line 695
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 697
    .local v0, "clipboard":Landroid/content/ClipboardManager;
    invoke-virtual {v0, p1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 698
    return-void
.end method

.method private swapHtmlText(Ljava/lang/String;Landroid/text/SpannableStringBuilder;II)V
    .locals 8
    .param p1, "htmlStr"    # Ljava/lang/String;
    .param p2, "spn"    # Landroid/text/SpannableStringBuilder;
    .param p3, "min"    # I
    .param p4, "start"    # I

    .prologue
    .line 1458
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p0

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/lang/String;Landroid/content/Context;Landroid/text/SpannableStringBuilder;ILcom/samsung/android/app/memo/uiwidget/RichEditor;I)V

    .line 1459
    .local v0, "SwapHtmlTextTask":Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;
    sget-object v1, Landroid/os/AsyncTask;->SERIAL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$SpannableStringBuilderTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1460
    return-void
.end method

.method private totalImageCount()I
    .locals 13

    .prologue
    const/4 v9, 0x0

    .line 428
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v8

    .line 429
    .local v8, "textSeq":Ljava/lang/CharSequence;
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7, v8}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 430
    .local v7, "span":Landroid/text/SpannableStringBuilder;
    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v10

    const-class v11, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    invoke-virtual {v7, v9, v10, v11}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    .line 431
    .local v2, "objArrIMg":[Ljava/lang/Object;
    const/4 v6, 0x0

    .line 432
    .local v6, "selimageCount":I
    const/4 v0, 0x0

    .line 433
    .local v0, "imageCount":I
    if-eqz v2, :cond_0

    .line 434
    array-length v11, v2

    move v10, v9

    :goto_0
    if-lt v10, v11, :cond_2

    .line 440
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v5

    .line 441
    .local v5, "selectionStart":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionEnd()I

    move-result v4

    .line 442
    .local v4, "selectionEnd":I
    if-eq v5, v4, :cond_1

    .line 444
    const-class v10, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    invoke-virtual {v7, v5, v4, v10}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    .line 445
    .local v3, "selectArrImg":[Ljava/lang/Object;
    if-eqz v3, :cond_1

    .line 446
    array-length v10, v3

    :goto_1
    if-lt v9, v10, :cond_4

    .line 453
    .end local v3    # "selectArrImg":[Ljava/lang/Object;
    :cond_1
    sub-int v9, v0, v6

    return v9

    .line 434
    .end local v4    # "selectionEnd":I
    .end local v5    # "selectionStart":I
    :cond_2
    aget-object v1, v2, v10

    .line 435
    .local v1, "obj":Ljava/lang/Object;
    instance-of v12, v1, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    if-eqz v12, :cond_3

    .line 436
    add-int/lit8 v0, v0, 0x1

    .line 434
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 446
    .end local v1    # "obj":Ljava/lang/Object;
    .restart local v3    # "selectArrImg":[Ljava/lang/Object;
    .restart local v4    # "selectionEnd":I
    .restart local v5    # "selectionStart":I
    :cond_4
    aget-object v1, v3, v9

    .line 447
    .restart local v1    # "obj":Ljava/lang/Object;
    instance-of v11, v1, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    if-eqz v11, :cond_5

    .line 448
    add-int/lit8 v6, v6, 0x1

    .line 446
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method


# virtual methods
.method public deleteImage(Landroid/net/Uri;)V
    .locals 14
    .param p1, "mContentUri"    # Landroid/net/Uri;

    .prologue
    .line 481
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v9

    .line 482
    .local v9, "textSeq":Ljava/lang/CharSequence;
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6, v9}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 483
    .local v6, "span":Landroid/text/SpannableStringBuilder;
    const/4 v10, 0x0

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v11

    const-class v12, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    invoke-virtual {v6, v10, v11, v12}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    .line 484
    .local v5, "objArrIMg":[Ljava/lang/Object;
    if-eqz v5, :cond_0

    .line 485
    array-length v12, v5

    const/4 v10, 0x0

    move v11, v10

    :goto_0
    if-lt v11, v12, :cond_1

    .line 518
    :cond_0
    :goto_1
    sget-object v10, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p0, v6, v10}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 520
    :try_start_0
    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v10

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSelection(I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 524
    :goto_2
    return-void

    .line 485
    :cond_1
    aget-object v3, v5, v11

    .line 486
    .local v3, "obj":Ljava/lang/Object;
    instance-of v10, v3, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    if-eqz v10, :cond_4

    move-object v10, v3

    .line 487
    check-cast v10, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    invoke-virtual {v10}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->getContentUri()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    .line 488
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 489
    invoke-virtual {v6, v3}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    .line 490
    .local v7, "start":I
    invoke-virtual {v6, v3}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    .line 492
    .local v2, "end":I
    add-int/lit8 v10, v2, 0x1

    const-string v11, "\n"

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v11

    if-gt v10, v11, :cond_2

    .line 493
    add-int/lit8 v10, v2, 0x1

    .line 494
    add-int/lit8 v11, v2, 0x1

    const-string v12, "\n"

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    .line 493
    invoke-interface {v9, v10, v11}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    .line 495
    .local v8, "subSeq":Ljava/lang/CharSequence;
    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 496
    add-int/lit8 v10, v2, 0x1

    add-int/lit8 v11, v2, 0x1

    const-string v12, "\n"

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-virtual {v6, v10, v11}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    .line 505
    .end local v8    # "subSeq":Ljava/lang/CharSequence;
    :cond_2
    const-class v10, Landroid/text/style/AlignmentSpan$Standard;

    invoke-virtual {v6, v7, v2, v10}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    .line 506
    .local v4, "objArrAlign":[Ljava/lang/Object;
    if-eqz v4, :cond_3

    array-length v10, v4

    if-lez v10, :cond_3

    .line 507
    const/4 v10, 0x0

    aget-object v0, v4, v10

    check-cast v0, Landroid/text/style/AlignmentSpan$Standard;

    .line 508
    .local v0, "as":Landroid/text/style/AlignmentSpan$Standard;
    if-eqz v0, :cond_3

    .line 509
    invoke-virtual {v6, v0}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 511
    .end local v0    # "as":Landroid/text/style/AlignmentSpan$Standard;
    :cond_3
    invoke-virtual {v6, v3}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 512
    const/4 v10, 0x1

    sput-boolean v10, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isFromImageDelete:Z

    .line 513
    add-int/lit8 v10, v2, 0x1

    const-string v11, ""

    invoke-virtual {v6, v7, v10, v11}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_1

    .line 485
    .end local v2    # "end":I
    .end local v4    # "objArrAlign":[Ljava/lang/Object;
    .end local v7    # "start":I
    :cond_4
    add-int/lit8 v10, v11, 0x1

    move v11, v10

    goto/16 :goto_0

    .line 521
    .end local v3    # "obj":Ljava/lang/Object;
    :catch_0
    move-exception v1

    .line 522
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    const/16 v10, 0x1000

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSelection(I)V

    goto/16 :goto_2
.end method

.method protected deleteText_internal_internal(II)V
    .locals 1
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    .line 691
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 692
    return-void
.end method

.method public getCustomSpannableString(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/app/memo/Session;)Landroid/text/SpannableStringBuilder;
    .locals 37
    .param p1, "HtmlStr"    # Ljava/lang/String;
    .param p2, "mActivity"    # Landroid/content/Context;
    .param p3, "mSession"    # Lcom/samsung/android/app/memo/Session;

    .prologue
    .line 1274
    const/4 v11, 0x0

    .line 1275
    .local v11, "htmlStartWithImage":Z
    const/16 v18, 0x0

    .line 1276
    .local v18, "isHtmlEndwithImage":Z
    const-string v31, "<h[1-9] "

    const-string v32, "<p "

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1277
    const-string v31, "<[ba] "

    const-string v32, "<text "

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1278
    const-string v31, "<blockquote "

    const-string v32, "<text "

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1279
    const-string v31, "<span "

    const-string v32, "<text "

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1280
    const-string v31, "<font "

    const-string v32, "<text "

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1281
    const-string v31, "<ul "

    const-string v32, "<text "

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1282
    invoke-static/range {p1 .. p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v29

    .line 1283
    .local v29, "st":Landroid/text/Spanned;
    const/4 v8, 0x0

    .line 1284
    .local v8, "count":I
    new-instance v30, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1285
    .local v30, "stb":Landroid/text/SpannableStringBuilder;
    const/16 v31, 0x0

    invoke-interface/range {v29 .. v29}, Landroid/text/Spanned;->length()I

    move-result v32

    const-class v33, Landroid/text/style/ImageSpan;

    invoke-virtual/range {v30 .. v33}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v15

    check-cast v15, [Landroid/text/style/ImageSpan;

    .line 1286
    .local v15, "imgSpn":[Landroid/text/style/ImageSpan;
    if-eqz v15, :cond_0

    array-length v0, v15

    move/from16 v31, v0

    if-nez v31, :cond_0

    .line 1287
    const/4 v8, 0x0

    .line 1288
    :goto_0
    invoke-virtual/range {v30 .. v30}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    const-string v32, "\n"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v31

    if-nez v31, :cond_6

    .line 1300
    :cond_0
    sget-boolean v31, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ongoingAsynctask:Z

    if-nez v31, :cond_1

    .line 1301
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->totalImageCount()I

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->currentImageCount:I

    .line 1302
    :cond_1
    const/16 v31, 0x1

    sput-boolean v31, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ongoingAsynctask:Z

    .line 1303
    if-eqz v15, :cond_2

    .line 1304
    array-length v0, v15

    move/from16 v33, v0

    const/16 v31, 0x0

    move/from16 v32, v31

    :goto_1
    move/from16 v0, v32

    move/from16 v1, v33

    if-lt v0, v1, :cond_8

    .line 1417
    :cond_2
    const/16 v31, 0x0

    invoke-interface/range {v29 .. v29}, Landroid/text/Spanned;->length()I

    move-result v32

    const-class v33, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    invoke-virtual/range {v30 .. v33}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    .line 1418
    .local v10, "htSpn":[Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    if-eqz v10, :cond_3

    array-length v0, v10

    move/from16 v31, v0

    if-nez v31, :cond_3

    .line 1419
    const/4 v8, 0x0

    .line 1420
    :goto_2
    invoke-virtual/range {v30 .. v30}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    const-string v32, "\n"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v31

    if-nez v31, :cond_18

    .line 1435
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->totalImageCount()I

    move-result v31

    add-int/lit8 v31, v31, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->MAX_IMAGE_LIMIT:I

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    if-gt v0, v1, :cond_5

    .line 1436
    if-eqz v11, :cond_4

    .line 1437
    const/16 v31, 0x0

    const-string v32, "\n"

    invoke-virtual/range {v30 .. v32}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1439
    :cond_4
    if-eqz v18, :cond_5

    .line 1440
    const-string v31, "\n"

    invoke-virtual/range {v30 .. v31}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1443
    :cond_5
    return-object v30

    .line 1289
    .end local v10    # "htSpn":[Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    :cond_6
    const/16 v31, 0x2

    move/from16 v0, v31

    if-gt v8, v0, :cond_0

    .line 1291
    invoke-virtual/range {v30 .. v30}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    const-string v32, "\n"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    .line 1292
    .local v9, "endLineIndex":I
    if-lez v9, :cond_7

    .line 1293
    invoke-virtual/range {v30 .. v30}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v31

    const-string v32, ""

    move-object/from16 v0, v30

    move/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v9, v1, v2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1294
    :cond_7
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 1304
    .end local v9    # "endLineIndex":I
    :cond_8
    aget-object v27, v15, v32

    .line 1305
    .local v27, "span":Landroid/text/style/ImageSpan;
    const/16 v20, 0x0

    .line 1306
    .local v20, "mImageUriNull":Z
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->addAlignment()Landroid/text/style/AlignmentSpan$Standard;

    move-result-object v6

    .line 1307
    .local v6, "as":Landroid/text/style/AlignmentSpan$Standard;
    if-eqz v27, :cond_c

    .line 1308
    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v16

    .line 1309
    .local v16, "imgSt":I
    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v13

    .line 1310
    .local v13, "imgEd":I
    if-nez v16, :cond_9

    .line 1311
    const/4 v11, 0x1

    .line 1313
    :cond_9
    invoke-interface/range {v29 .. v29}, Landroid/text/Spanned;->length()I

    move-result v31

    move/from16 v0, v31

    if-ne v13, v0, :cond_a

    .line 1314
    const/16 v18, 0x1

    .line 1316
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->currentImageCount:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->MAX_IMAGE_LIMIT:I

    move/from16 v34, v0

    move/from16 v0, v31

    move/from16 v1, v34

    if-eq v0, v1, :cond_b

    .line 1317
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v31

    invoke-interface/range {v31 .. v31}, Landroid/text/Editable;->length()I

    move-result v31

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    move/from16 v35, v0

    sub-int v34, v34, v35

    sub-int v31, v31, v34

    const-string v34, "\n \n"

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->length()I

    move-result v34

    add-int v31, v31, v34

    const/16 v34, 0x1000

    move/from16 v0, v31

    move/from16 v1, v34

    if-le v0, v1, :cond_d

    .line 1318
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    check-cast v31, Landroid/app/Activity;

    new-instance v34, Lcom/samsung/android/app/memo/uiwidget/RichEditor$9;

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$9;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1326
    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 1327
    const-string v31, ""

    move-object/from16 v0, v30

    move/from16 v1, v16

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v13, v2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1304
    .end local v13    # "imgEd":I
    .end local v16    # "imgSt":I
    :cond_c
    :goto_3
    add-int/lit8 v31, v32, 0x1

    move/from16 v32, v31

    goto/16 :goto_1

    .line 1330
    .restart local v13    # "imgEd":I
    .restart local v16    # "imgSt":I
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->currentImageCount:I

    move/from16 v31, v0

    add-int/lit8 v31, v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->currentImageCount:I

    .line 1331
    invoke-virtual/range {v27 .. v27}, Landroid/text/style/ImageSpan;->getSource()Ljava/lang/String;

    move-result-object v26

    .line 1332
    .local v26, "source":Ljava/lang/String;
    const/4 v12, 0x0

    .line 1333
    .local v12, "imageUri":Landroid/net/Uri;
    const/4 v14, 0x0

    .line 1334
    .local v14, "imgPath":Ljava/lang/String;
    if-eqz v26, :cond_11

    const-string v31, "http://"

    move-object/from16 v0, v26

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v31

    if-nez v31, :cond_e

    .line 1335
    const-string v31, "https://"

    move-object/from16 v0, v26

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v31

    if-nez v31, :cond_e

    const-string v31, "data:image/"

    move-object/from16 v0, v26

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_11

    .line 1336
    :cond_e
    move-object/from16 v14, v26

    .line 1338
    :try_start_0
    invoke-static {v14}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v31

    const-string v34, ""

    .line 1339
    const-string v35, "image/jpeg"

    const/16 v36, 0x0

    .line 1338
    move-object/from16 v0, p3

    move-object/from16 v1, v31

    move-object/from16 v2, v34

    move-object/from16 v3, v35

    move/from16 v4, v36

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/app/memo/Session;->addFile(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 1343
    :goto_4
    const/16 v31, 0x0

    const-string v34, ""

    move-object/from16 v0, p0

    move/from16 v1, v31

    move-object/from16 v2, v34

    invoke-direct {v0, v12, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->newHtmlImageSpan(Landroid/net/Uri;ILjava/lang/String;)Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    move-result-object v17

    .line 1344
    .local v17, "is":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    const/16 v31, 0x21

    move-object/from16 v0, v30

    move-object/from16 v1, v17

    move/from16 v2, v16

    move/from16 v3, v31

    invoke-virtual {v0, v1, v2, v13, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1345
    invoke-virtual/range {v30 .. v30}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v31

    add-int/lit8 v34, v13, 0x1

    move/from16 v0, v31

    move/from16 v1, v34

    if-gt v0, v1, :cond_10

    .line 1346
    const/16 v31, 0x21

    move-object/from16 v0, v30

    move/from16 v1, v16

    move/from16 v2, v31

    invoke-virtual {v0, v6, v1, v13, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1410
    .end local v17    # "is":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    :cond_f
    :goto_5
    move-object/from16 v0, v30

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 1411
    if-eqz v20, :cond_c

    .line 1412
    const-string v31, ""

    move-object/from16 v0, v30

    move/from16 v1, v16

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v13, v2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_3

    .line 1340
    :catch_0
    move-exception v25

    .line 1341
    .local v25, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    sget-object v31, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;

    const-string v34, "getCustomSpannableString()"

    move-object/from16 v0, v31

    move-object/from16 v1, v34

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 1348
    .end local v25    # "se":Lcom/samsung/android/app/memo/Session$SessionException;
    .restart local v17    # "is":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    :cond_10
    add-int/lit8 v31, v13, 0x1

    const/16 v34, 0x21

    move-object/from16 v0, v30

    move/from16 v1, v16

    move/from16 v2, v31

    move/from16 v3, v34

    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_5

    .line 1349
    .end local v17    # "is":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    :cond_11
    if-eqz v26, :cond_f

    const-string v31, "file://"

    move-object/from16 v0, v26

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v31

    if-nez v31, :cond_12

    const-string v31, "content://"

    move-object/from16 v0, v26

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_f

    .line 1350
    :cond_12
    const-string v31, "content://"

    move-object/from16 v0, v26

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_15

    .line 1351
    invoke-virtual/range {v27 .. v27}, Landroid/text/style/ImageSpan;->getSource()Ljava/lang/String;

    move-result-object v14

    .line 1352
    invoke-static {v14}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    .line 1363
    :cond_13
    :goto_6
    if-eqz v12, :cond_f

    .line 1364
    invoke-static {v12}, Lcom/samsung/android/app/memo/util/ImageUtil;->lookupImageInfo(Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v7

    .line 1365
    .local v7, "bundle":Landroid/os/Bundle;
    sget-object v31, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_ORIENTATION:Ljava/lang/String;

    const/16 v34, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v34

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v23

    .line 1366
    .local v23, "orientation":I
    sget-object v31, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_DISPLAYNAME:Ljava/lang/String;

    .line 1367
    invoke-virtual {v12}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v34

    .line 1366
    move-object/from16 v0, v31

    move-object/from16 v1, v34

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1368
    .local v5, "altText":Ljava/lang/String;
    sget-object v31, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_MIMETYPE:Ljava/lang/String;

    .line 1369
    const-string v34, "image/jpeg"

    .line 1368
    move-object/from16 v0, v31

    move-object/from16 v1, v34

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 1370
    .local v21, "mime_type":Ljava/lang/String;
    const/16 v22, 0x0

    .line 1373
    .local v22, "newUri":Landroid/net/Uri;
    :try_start_1
    move-object/from16 v0, p3

    move-object/from16 v1, v21

    move/from16 v2, v23

    invoke-virtual {v0, v12, v5, v1, v2}, Lcom/samsung/android/app/memo/Session;->addFile(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;
    :try_end_1
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v22

    .line 1377
    :goto_7
    if-eqz v22, :cond_17

    .line 1378
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v1, v2, v5}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->newHtmlImageSpan(Landroid/net/Uri;ILjava/lang/String;)Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    move-result-object v17

    .line 1379
    .restart local v17    # "is":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->isLoaded()Z

    .line 1386
    const/16 v31, 0x21

    move-object/from16 v0, v30

    move-object/from16 v1, v17

    move/from16 v2, v16

    move/from16 v3, v31

    invoke-virtual {v0, v1, v2, v13, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1388
    add-int/lit8 v31, v16, -0x1

    if-ltz v31, :cond_14

    .line 1389
    invoke-virtual/range {v30 .. v30}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    add-int/lit8 v34, v16, -0x1

    move-object/from16 v0, v31

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v24

    .line 1390
    .local v24, "prevChar":C
    const/16 v31, 0xa

    move/from16 v0, v31

    move/from16 v1, v24

    if-eq v0, v1, :cond_14

    .line 1391
    const-string v31, "\n"

    move-object/from16 v0, v30

    move/from16 v1, v16

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1394
    .end local v24    # "prevChar":C
    :cond_14
    invoke-virtual/range {v30 .. v30}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v31

    add-int/lit8 v34, v13, 0x1

    move/from16 v0, v31

    move/from16 v1, v34

    if-gt v0, v1, :cond_16

    .line 1396
    const/16 v31, 0x21

    .line 1395
    move-object/from16 v0, v30

    move/from16 v1, v16

    move/from16 v2, v31

    invoke-virtual {v0, v6, v1, v13, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_5

    .line 1354
    .end local v5    # "altText":Ljava/lang/String;
    .end local v7    # "bundle":Landroid/os/Bundle;
    .end local v17    # "is":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    .end local v21    # "mime_type":Ljava/lang/String;
    .end local v22    # "newUri":Landroid/net/Uri;
    .end local v23    # "orientation":I
    :cond_15
    const-string v31, "file://"

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v19

    .line 1355
    .local v19, "len":I
    invoke-virtual/range {v27 .. v27}, Landroid/text/style/ImageSpan;->getSource()Ljava/lang/String;

    move-result-object v28

    .line 1356
    .local v28, "spanSource":Ljava/lang/String;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v31

    move/from16 v0, v19

    move/from16 v1, v31

    if-ge v0, v1, :cond_13

    .line 1357
    const-string v31, "file://"

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->length()I

    move-result v31

    move-object/from16 v0, v28

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 1360
    new-instance v31, Ljava/io/File;

    move-object/from16 v0, v31

    invoke-direct {v0, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v31 .. v31}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v12

    goto/16 :goto_6

    .line 1374
    .end local v19    # "len":I
    .end local v28    # "spanSource":Ljava/lang/String;
    .restart local v5    # "altText":Ljava/lang/String;
    .restart local v7    # "bundle":Landroid/os/Bundle;
    .restart local v21    # "mime_type":Ljava/lang/String;
    .restart local v22    # "newUri":Landroid/net/Uri;
    .restart local v23    # "orientation":I
    :catch_1
    move-exception v25

    .line 1375
    .restart local v25    # "se":Lcom/samsung/android/app/memo/Session$SessionException;
    sget-object v31, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;

    const-string v34, "insertImage()"

    move-object/from16 v0, v31

    move-object/from16 v1, v34

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_7

    .line 1398
    .end local v25    # "se":Lcom/samsung/android/app/memo/Session$SessionException;
    .restart local v17    # "is":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    :cond_16
    add-int/lit8 v31, v13, 0x1

    .line 1399
    const/16 v34, 0x21

    .line 1398
    move-object/from16 v0, v30

    move/from16 v1, v16

    move/from16 v2, v31

    move/from16 v3, v34

    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_5

    .line 1402
    .end local v17    # "is":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    :cond_17
    const/16 v17, 0x0

    .line 1403
    .restart local v17    # "is":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    const/16 v20, 0x1

    .line 1404
    const/16 v31, 0x1

    sput-boolean v31, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mImagePasteFailed:Z

    .line 1405
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mShowImageNotFoundHandler:Landroid/os/Handler;

    move-object/from16 v31, v0

    const/16 v34, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1406
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->currentImageCount:I

    move/from16 v31, v0

    add-int/lit8 v31, v31, -0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->currentImageCount:I

    goto/16 :goto_5

    .line 1421
    .end local v5    # "altText":Ljava/lang/String;
    .end local v6    # "as":Landroid/text/style/AlignmentSpan$Standard;
    .end local v7    # "bundle":Landroid/os/Bundle;
    .end local v12    # "imageUri":Landroid/net/Uri;
    .end local v13    # "imgEd":I
    .end local v14    # "imgPath":Ljava/lang/String;
    .end local v16    # "imgSt":I
    .end local v17    # "is":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    .end local v20    # "mImageUriNull":Z
    .end local v21    # "mime_type":Ljava/lang/String;
    .end local v22    # "newUri":Landroid/net/Uri;
    .end local v23    # "orientation":I
    .end local v26    # "source":Ljava/lang/String;
    .end local v27    # "span":Landroid/text/style/ImageSpan;
    .restart local v10    # "htSpn":[Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    :cond_18
    const/16 v31, 0x2

    move/from16 v0, v31

    if-gt v8, v0, :cond_3

    .line 1424
    invoke-virtual/range {v30 .. v30}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    const-string v32, "\n"

    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    .line 1425
    .restart local v9    # "endLineIndex":I
    if-lez v9, :cond_19

    .line 1426
    invoke-virtual/range {v30 .. v30}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v31

    const-string v32, ""

    move-object/from16 v0, v30

    move/from16 v1, v31

    move-object/from16 v2, v32

    invoke-virtual {v0, v9, v1, v2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1428
    :cond_19
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2
.end method

.method public getHtml()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/HtmlUtil;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlainText()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 324
    .local v0, "text":Ljava/lang/String;
    const-string v1, "\n\ufffc\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    .line 325
    const-string v1, "(\\n\\uFFFC\\n)+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 326
    :cond_0
    const-string v1, "\ufffc\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 327
    const-string v1, "(\\uFFFC\\n)+"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 328
    :cond_1
    const-string v1, "(\\n\\uFFFC\\n)+"

    const-string v2, "\n"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 329
    const-string v1, "(\\uFFFC\\n)+"

    const-string v2, "\n"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 330
    const-string v1, "(\\uFFFC)+"

    const-string v2, "\n"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 331
    const/16 v1, 0xa0

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    .line 332
    return-object v0
.end method

.method getTransformedText(II)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 701
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->removeSpans(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public hideClipboard()V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->dismissUIDataDialog()V

    .line 224
    :cond_0
    return-void
.end method

.method public insertHtmlText(Ljava/lang/CharSequence;II)V
    .locals 8
    .param p1, "seq"    # Ljava/lang/CharSequence;
    .param p2, "min"    # I
    .param p3, "max"    # I

    .prologue
    const/4 v5, 0x0

    .line 1782
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1783
    .local v2, "spn":Landroid/text/SpannableStringBuilder;
    if-nez p1, :cond_0

    .line 1797
    :goto_0
    return-void

    .line 1785
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1787
    .local v1, "htmlStr":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1788
    .local v6, "count1":I
    :goto_1
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move-object v0, p0

    move v3, p2

    move v4, p3

    .line 1796
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->addHtmlText(Ljava/lang/String;Landroid/text/SpannableStringBuilder;IIZ)V

    goto :goto_0

    .line 1789
    :cond_2
    const/4 v0, 0x2

    if-gt v6, v0, :cond_1

    .line 1791
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    .line 1792
    .local v7, "endLineIndex":I
    if-lez v7, :cond_3

    .line 1793
    invoke-virtual {v1, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1794
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method public insertImage(Landroid/net/Uri;ILjava/lang/String;)Z
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "orientation"    # I
    .param p3, "alt"    # Ljava/lang/String;

    .prologue
    .line 345
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->totalImageCount()I

    move-result v10

    iget v11, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->MAX_IMAGE_LIMIT:I

    if-ne v10, v11, :cond_0

    .line 346
    iget-object v10, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    const v11, 0x7f0b0042

    invoke-static {v10, v11}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 347
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isMaxImageReached:Z

    .line 348
    const/4 v10, 0x0

    .line 406
    :goto_0
    return v10

    .line 351
    :cond_0
    invoke-static/range {p3 .. p3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, p1, p2, v10}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->newHtmlImageSpan(Landroid/net/Uri;ILjava/lang/String;)Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    move-result-object v4

    .line 354
    .local v4, "imgSpan":Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v7

    .line 355
    .local v7, "selectS":I
    if-gez v7, :cond_1

    .line 356
    const/4 v7, 0x0

    .line 357
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionEnd()I

    move-result v6

    .line 358
    .local v6, "selectE":I
    if-gez v6, :cond_2

    .line 359
    const/4 v6, 0x0

    .line 360
    :cond_2
    if-le v7, v6, :cond_3

    .line 361
    move v9, v7

    .line 362
    .local v9, "tmp":I
    move v7, v6

    .line 363
    move v6, v9

    .line 366
    .end local v9    # "tmp":I
    :cond_3
    const/4 v5, 0x1

    .line 368
    .local v5, "isLineChange":Z
    if-nez v7, :cond_6

    .line 369
    const/4 v5, 0x0

    .line 382
    :cond_4
    :goto_1
    if-nez v5, :cond_7

    .line 383
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v10

    const-string v11, " \n"

    invoke-interface {v10, v7, v6, v11}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v3

    .line 384
    .local v3, "editable":Landroid/text/Editable;
    add-int/lit8 v10, v7, 0x1

    const/16 v11, 0x21

    invoke-interface {v3, v4, v7, v10, v11}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 385
    new-instance v0, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v10, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v0, v10}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 386
    .local v0, "as":Landroid/text/style/AlignmentSpan$Standard;
    add-int/lit8 v10, v7, 0x1

    const/16 v11, 0x21

    invoke-interface {v3, v0, v7, v10, v11}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 397
    :goto_2
    const-wide/16 v10, -0x1

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getHtml()Ljava/lang/CharSequence;

    move-result-object v12

    invoke-virtual {p0, v10, v11, v12}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setHtml(JLjava/lang/CharSequence;)V

    .line 399
    if-eqz v5, :cond_8

    .line 400
    const-string v10, "\n \n"

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int v1, v7, v10

    .line 403
    .local v1, "cursorPos":I
    :goto_3
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-interface {v10}, Landroid/text/Editable;->length()I

    move-result v10

    if-gt v1, v10, :cond_5

    .line 404
    invoke-virtual {p0, v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSelection(I)V

    .line 406
    :cond_5
    const/4 v10, 0x1

    goto :goto_0

    .line 375
    .end local v0    # "as":Landroid/text/style/AlignmentSpan$Standard;
    .end local v1    # "cursorPos":I
    .end local v3    # "editable":Landroid/text/Editable;
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v10

    add-int/lit8 v11, v7, -0x1

    invoke-interface {v10, v11, v7}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    .line 376
    .local v8, "subSeq":Ljava/lang/CharSequence;
    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 377
    const/4 v5, 0x0

    goto :goto_1

    .line 388
    .end local v8    # "subSeq":Ljava/lang/CharSequence;
    :cond_7
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v10

    const-string v11, "\n \n"

    invoke-interface {v10, v7, v6, v11}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v3

    .line 389
    .restart local v3    # "editable":Landroid/text/Editable;
    add-int/lit8 v10, v7, 0x1

    add-int/lit8 v11, v7, 0x2

    const/16 v12, 0x21

    invoke-interface {v3, v4, v10, v11, v12}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 390
    new-instance v0, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v10, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v0, v10}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 391
    .restart local v0    # "as":Landroid/text/style/AlignmentSpan$Standard;
    add-int/lit8 v10, v7, 0x1

    add-int/lit8 v11, v7, 0x2

    const/16 v12, 0x21

    invoke-interface {v3, v0, v10, v11, v12}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 393
    .end local v0    # "as":Landroid/text/style/AlignmentSpan$Standard;
    .end local v3    # "editable":Landroid/text/Editable;
    :catch_0
    move-exception v2

    .line 394
    .local v2, "e":Ljava/lang/IndexOutOfBoundsException;
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 402
    .end local v2    # "e":Ljava/lang/IndexOutOfBoundsException;
    .restart local v0    # "as":Landroid/text/style/AlignmentSpan$Standard;
    .restart local v3    # "editable":Landroid/text/Editable;
    :cond_8
    const-string v10, " \n"

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int v1, v7, v10

    .restart local v1    # "cursorPos":I
    goto :goto_3
.end method

.method public isClipboardShowing()Z
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isShowing()Z

    move-result v0

    .line 236
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEditMode()Z
    .locals 1

    .prologue
    .line 1818
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsEditMode:Z

    return v0
.end method

.method public isImageMaximum()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 411
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 424
    :cond_0
    :goto_0
    return v0

    .line 414
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    const-string v3, "\n \n"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    const/16 v3, 0x1000

    if-le v2, v3, :cond_2

    .line 415
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0049

    invoke-static {v0, v2}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    move v0, v1

    .line 416
    goto :goto_0

    .line 419
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->totalImageCount()I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->MAX_IMAGE_LIMIT:I

    if-ne v2, v3, :cond_0

    .line 420
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0042

    invoke-static {v0, v2}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    move v0, v1

    .line 421
    goto :goto_0
.end method

.method public isPasteOperationGoing()Z
    .locals 1

    .prologue
    .line 573
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsTextPasteOperationGoing:Z

    return v0
.end method

.method public onClick(Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;)V
    .locals 1
    .param p1, "span"    # Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;

    .prologue
    .line 295
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSpanOnClickListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSpanOnClickListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;->onClick(Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType;)V

    .line 297
    :cond_0
    return-void
.end method

.method public onClipboardPaste(Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;)V
    .locals 1
    .param p1, "proxyData"    # Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;

    .prologue
    .line 763
    new-instance v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->post(Ljava/lang/Runnable;)Z

    .line 780
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 30
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1642
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionEnd()I

    move-result v25

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_d

    .line 1643
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    .line 1644
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Landroid/text/Editable;->length()I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    .line 1645
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isFocused()Z

    move-result v25

    if-eqz v25, :cond_0

    .line 1646
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v19

    .line 1647
    .local v19, "selStart":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionEnd()I

    move-result v18

    .line 1648
    .local v18, "selEnd":I
    move/from16 v0, v19

    move/from16 v1, v18

    if-eq v0, v1, :cond_0

    .line 1649
    const/16 v25, 0x0

    move/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v26

    invoke-static/range {v25 .. v26}, Ljava/lang/Math;->max(II)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    .line 1650
    const/16 v25, 0x0

    move/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v26

    invoke-static/range {v25 .. v26}, Ljava/lang/Math;->max(II)I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    .line 1653
    .end local v18    # "selEnd":I
    .end local v19    # "selStart":I
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    move/from16 v27, v0

    invoke-interface/range {v25 .. v27}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;

    .line 1656
    new-instance v21, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;

    move-object/from16 v25, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1657
    .local v21, "span":Landroid/text/SpannableStringBuilder;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    move/from16 v27, v0

    invoke-interface/range {v25 .. v27}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v25

    check-cast v25, Landroid/text/Spanned;

    invoke-static/range {v25 .. v25}, Lcom/samsung/android/app/memo/util/HtmlUtil;->toHtml(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object v9

    .line 1658
    .local v9, "htmlStr":Ljava/lang/String;
    new-instance v20, Ljava/lang/StringBuilder;

    move-object/from16 v0, v20

    invoke-direct {v0, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1659
    .local v20, "spHtml":Ljava/lang/StringBuilder;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1660
    .local v4, "copyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/16 v22, -0x1

    .local v22, "startTag":I
    const/4 v7, -0x1

    .local v7, "endTag":I
    const/4 v11, 0x0

    .line 1662
    .local v11, "lastEndPos":I
    const/16 v25, 0x0

    invoke-virtual/range {v21 .. v21}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v26

    const-class v27, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    move-object/from16 v0, v21

    move/from16 v1, v25

    move/from16 v2, v26

    move-object/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v15

    .line 1663
    .local v15, "objArr":[Ljava/lang/Object;
    if-eqz v15, :cond_1

    .line 1664
    array-length v0, v15

    move/from16 v27, v0

    const/16 v25, 0x0

    move/from16 v26, v25

    :goto_0
    move/from16 v0, v26

    move/from16 v1, v27

    if-lt v0, v1, :cond_6

    .line 1686
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mRevokeHandler:Landroid/os/Handler;

    move-object/from16 v25, v0

    const/16 v26, 0x79

    invoke-virtual/range {v25 .. v26}, Landroid/os/Handler;->removeMessages(I)V

    .line 1688
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->addPermission(Ljava/util/ArrayList;)V

    .line 1690
    if-eqz v4, :cond_3

    .line 1691
    sget-object v25, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->revokeList:Ljava/util/ArrayList;

    if-eqz v25, :cond_2

    .line 1692
    sget-object v25, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->revokeList:Ljava/util/ArrayList;

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->clear()V

    .line 1693
    const/16 v25, 0x0

    sput-object v25, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->revokeList:Ljava/util/ArrayList;

    .line 1695
    :cond_2
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    sput-object v25, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->revokeList:Ljava/util/ArrayList;

    .line 1696
    sget-object v25, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->revokeList:Ljava/util/ArrayList;

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1697
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mRevokeHandler:Landroid/os/Handler;

    move-object/from16 v25, v0

    const/16 v26, 0x79

    const-wide/32 v28, 0x186a0

    move-object/from16 v0, v25

    move/from16 v1, v26

    move-wide/from16 v2, v28

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1700
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/samsung/android/app/memo/util/HtmlUtil;->stripHtml(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1702
    .local v17, "plainText":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1703
    .local v10, "htmlString":Ljava/lang/String;
    const/4 v13, 0x0

    .line 1705
    .local v13, "multiWindow":Landroid/sec/multiwindow/MultiWindow;
    sget-boolean v25, Lcom/samsung/android/app/memo/util/Utils;->IS_MULTIWINDOW_DEVICE:Z

    if-eqz v25, :cond_4

    .line 1706
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    check-cast v25, Landroid/app/Activity;

    invoke-static/range {v25 .. v25}, Landroid/sec/multiwindow/MultiWindow;->createInstance(Landroid/app/Activity;)Landroid/sec/multiwindow/MultiWindow;

    move-result-object v13

    .line 1708
    :cond_4
    if-eqz v13, :cond_b

    invoke-virtual {v13}, Landroid/sec/multiwindow/MultiWindow;->isMultiWindow()Z

    move-result v25

    if-eqz v25, :cond_b

    invoke-virtual {v13}, Landroid/sec/multiwindow/MultiWindow;->isNormalWindow()Z

    move-result v25

    if-nez v25, :cond_b

    .line 1709
    const-string v25, "MultiWindow_DragDrop_Memo"

    .line 1710
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 1709
    move-object/from16 v0, v25

    move-object/from16 v1, v17

    move-object/from16 v2, v26

    invoke-static {v0, v1, v2}, Landroid/content/ClipData;->newHtmlText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/content/ClipData;

    move-result-object v5

    .line 1711
    .local v5, "dragData":Landroid/content/ClipData;
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_1
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-nez v26, :cond_a

    .line 1713
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->stopSelectionActionMode()V

    .line 1735
    :goto_2
    if-eqz v5, :cond_5

    .line 1736
    new-instance v23, Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;-><init>(Landroid/content/Context;)V

    .line 1737
    .local v23, "test":Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    const-wide/16 v26, 0x0

    move-object/from16 v0, v23

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2, v10}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setHtml(JLjava/lang/CharSequence;)V

    .line 1738
    new-instance v12, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-direct {v12, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1739
    .local v12, "linearLayout":Landroid/widget/LinearLayout;
    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1740
    new-instance v16, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v25, -0x2

    .line 1741
    const/16 v26, -0x2

    .line 1740
    move-object/from16 v0, v16

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1742
    .local v16, "params":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1744
    invoke-static {v12}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getScreenViewBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mDragBitmap:Landroid/graphics/Bitmap;

    .line 1745
    new-instance v6, Lcom/samsung/android/app/memo/uiwidget/RichEditor$11;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v12}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$11;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Landroid/view/View;)V

    .line 1758
    .local v6, "dragShadow":Landroid/view/View$DragShadowBuilder;
    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 1760
    .end local v6    # "dragShadow":Landroid/view/View$DragShadowBuilder;
    .end local v12    # "linearLayout":Landroid/widget/LinearLayout;
    .end local v16    # "params":Landroid/view/ViewGroup$LayoutParams;
    .end local v23    # "test":Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    :cond_5
    const/16 v25, 0x1

    .line 1763
    .end local v4    # "copyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v5    # "dragData":Landroid/content/ClipData;
    .end local v7    # "endTag":I
    .end local v9    # "htmlStr":Ljava/lang/String;
    .end local v10    # "htmlString":Ljava/lang/String;
    .end local v11    # "lastEndPos":I
    .end local v13    # "multiWindow":Landroid/sec/multiwindow/MultiWindow;
    .end local v15    # "objArr":[Ljava/lang/Object;
    .end local v17    # "plainText":Ljava/lang/String;
    .end local v20    # "spHtml":Ljava/lang/StringBuilder;
    .end local v21    # "span":Landroid/text/SpannableStringBuilder;
    .end local v22    # "startTag":I
    :goto_3
    return v25

    .line 1664
    .restart local v4    # "copyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .restart local v7    # "endTag":I
    .restart local v9    # "htmlStr":Ljava/lang/String;
    .restart local v11    # "lastEndPos":I
    .restart local v15    # "objArr":[Ljava/lang/Object;
    .restart local v20    # "spHtml":Ljava/lang/StringBuilder;
    .restart local v21    # "span":Landroid/text/SpannableStringBuilder;
    .restart local v22    # "startTag":I
    :cond_6
    aget-object v14, v15, v26

    .line 1665
    .local v14, "obj":Ljava/lang/Object;
    instance-of v0, v14, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    move/from16 v25, v0

    if-eqz v25, :cond_9

    move-object/from16 v25, v14

    .line 1666
    check-cast v25, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->getContentUri()Landroid/net/Uri;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1667
    check-cast v14, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    .end local v14    # "obj":Ljava/lang/Object;
    invoke-virtual {v14}, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->toHtmlForCopy()Ljava/lang/String;

    move-result-object v8

    .line 1670
    .local v8, "htmlImgTag":Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v25

    const-string v28, "<img"

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v25

    add-int v22, v25, v11

    .line 1671
    const-string v25, "/>"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;I)I

    move-result v25

    add-int/lit8 v7, v25, 0x2

    .line 1673
    add-int/lit8 v11, v7, 0x1

    .line 1674
    if-ltz v22, :cond_7

    if-gez v7, :cond_8

    .line 1675
    :cond_7
    sget-object v25, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    const-string v27, "onLongClick Copy Failure : IndexOutofBounds start:"

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1676
    move-object/from16 v0, v26

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " end:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 1675
    invoke-static/range {v25 .. v26}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1677
    const/16 v25, 0x1

    goto :goto_3

    .line 1679
    :cond_8
    move/from16 v0, v22

    if-le v7, v0, :cond_9

    .line 1680
    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1, v7, v8}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1664
    .end local v8    # "htmlImgTag":Ljava/lang/String;
    :cond_9
    add-int/lit8 v25, v26, 0x1

    move/from16 v26, v25

    goto/16 :goto_0

    .line 1711
    .restart local v5    # "dragData":Landroid/content/ClipData;
    .restart local v10    # "htmlString":Ljava/lang/String;
    .restart local v13    # "multiWindow":Landroid/sec/multiwindow/MultiWindow;
    .restart local v17    # "plainText":Ljava/lang/String;
    :cond_a
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/net/Uri;

    .line 1712
    .local v24, "uri":Landroid/net/Uri;
    new-instance v26, Landroid/content/ClipData$Item;

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    goto/16 :goto_1

    .line 1714
    .end local v5    # "dragData":Landroid/content/ClipData;
    .end local v24    # "uri":Landroid/net/Uri;
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/samsung/android/app/memo/util/HtmlUtil;->stripHtml(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->isEmpty()Z

    move-result v25

    if-nez v25, :cond_c

    .line 1715
    move-object/from16 v10, v17

    .line 1716
    const-string v25, "MultiWindow_DragDrop_Memo"

    move-object/from16 v0, v25

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v5

    .line 1717
    .restart local v5    # "dragData":Landroid/content/ClipData;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->stopSelectionActionMode()V

    goto/16 :goto_2

    .line 1720
    .end local v5    # "dragData":Landroid/content/ClipData;
    :cond_c
    new-instance v12, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-direct {v12, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1721
    .restart local v12    # "linearLayout":Landroid/widget/LinearLayout;
    new-instance v6, Lcom/samsung/android/app/memo/uiwidget/RichEditor$10;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v12}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$10;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Landroid/view/View;)V

    .line 1730
    .restart local v6    # "dragShadow":Landroid/view/View$DragShadowBuilder;
    const-string v25, "MultiWindow_DragDrop_Memo"

    .line 1731
    const-string v26, ""

    invoke-static/range {v26 .. v26}, Lcom/samsung/android/app/memo/util/HtmlUtil;->stripHtml(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v26

    const-string v27, ""

    .line 1730
    invoke-static/range {v25 .. v27}, Landroid/content/ClipData;->newHtmlText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/content/ClipData;

    move-result-object v25

    .line 1731
    const/16 v26, 0x0

    const/16 v27, 0x0

    .line 1730
    move-object/from16 v0, p1

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v6, v2, v3}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 1732
    const/16 v25, 0x1

    goto/16 :goto_3

    .line 1763
    .end local v4    # "copyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v6    # "dragShadow":Landroid/view/View$DragShadowBuilder;
    .end local v7    # "endTag":I
    .end local v9    # "htmlStr":Ljava/lang/String;
    .end local v10    # "htmlString":Ljava/lang/String;
    .end local v11    # "lastEndPos":I
    .end local v12    # "linearLayout":Landroid/widget/LinearLayout;
    .end local v13    # "multiWindow":Landroid/sec/multiwindow/MultiWindow;
    .end local v15    # "objArr":[Ljava/lang/Object;
    .end local v17    # "plainText":Ljava/lang/String;
    .end local v20    # "spHtml":Ljava/lang/StringBuilder;
    .end local v21    # "span":Landroid/text/SpannableStringBuilder;
    .end local v22    # "startTag":I
    :cond_d
    const/16 v25, 0x0

    goto/16 :goto_3
.end method

.method protected onSelectionChanged(II)V
    .locals 8
    .param p1, "selStart"    # I
    .param p2, "selEnd"    # I

    .prologue
    const/4 v5, 0x0

    .line 582
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->updateUndoData(II)V

    .line 583
    iget-boolean v3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isLongClick:Z

    if-eqz v3, :cond_1

    .line 584
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 585
    .local v2, "textSeq":Ljava/lang/CharSequence;
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 586
    .local v1, "span":Landroid/text/SpannableStringBuilder;
    const-class v3, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    invoke-virtual {v1, p1, p1, v3}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    .line 587
    .local v0, "objArrIMg":[Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 588
    array-length v3, v0

    if-eqz v3, :cond_0

    aget-object v3, v0, v5

    instance-of v3, v3, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;

    if-eqz v3, :cond_0

    .line 589
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Lcom/samsung/android/app/memo/uiwidget/RichEditor$7;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$7;-><init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    .line 596
    const-wide/16 v6, 0x1

    .line 589
    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 599
    :cond_0
    iput-boolean v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isLongClick:Z

    .line 601
    .end local v0    # "objArrIMg":[Ljava/lang/Object;
    .end local v1    # "span":Landroid/text/SpannableStringBuilder;
    .end local v2    # "textSeq":Ljava/lang/CharSequence;
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mOnSelectionChangedListener:Lcom/samsung/android/app/memo/uiwidget/RichEditor$OnSelectionChangedListener;

    if-eqz v3, :cond_2

    .line 602
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mOnSelectionChangedListener:Lcom/samsung/android/app/memo/uiwidget/RichEditor$OnSelectionChangedListener;

    invoke-interface {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$OnSelectionChangedListener;->onSelectionChanged()V

    .line 603
    :cond_2
    return-void
.end method

.method public onTextContextMenuItem(I)Z
    .locals 9
    .param p1, "id"    # I

    .prologue
    const v8, 0x102040a

    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 607
    new-instance v1, Landroid/sec/enterprise/RestrictionPolicy;

    invoke-direct {v1}, Landroid/sec/enterprise/RestrictionPolicy;-><init>()V

    .line 608
    .local v1, "restrictionPolicy":Landroid/sec/enterprise/RestrictionPolicy;
    iput v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    .line 609
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    .line 611
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 612
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v3

    .line 613
    .local v3, "selStart":I
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionEnd()I

    move-result v2

    .line 615
    .local v2, "selEnd":I
    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    .line 616
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    .line 619
    .end local v2    # "selEnd":I
    .end local v3    # "selStart":I
    :cond_0
    const v5, 0x102001f

    if-eq p1, v5, :cond_c

    .line 621
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_DICTIONARY:I

    if-eq p1, v5, :cond_c

    invoke-virtual {v1, v4}, Landroid/sec/enterprise/RestrictionPolicy;->isClipboardAllowed(Z)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 622
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 623
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_CUT:I

    if-eq p1, v5, :cond_1

    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_COPY:I

    if-eq p1, v5, :cond_1

    .line 624
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_PASTE:I

    if-eq p1, v5, :cond_1

    if-ne p1, v8, :cond_6

    .line 625
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    invoke-interface {v5, v6, v7}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;

    .line 626
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_CUT:I

    if-ne p1, v5, :cond_3

    .line 627
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardservice:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;

    invoke-interface {v5, v6}, Lcom/samsung/android/app/memo/uiwidget/ClipboardService;->onTextCopy(Ljava/lang/CharSequence;)V

    .line 629
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getHtml()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoData:Ljava/lang/String;

    .line 630
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Undoable:Z

    .line 631
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoSelection:I

    .line 632
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    invoke-virtual {p0, v5, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->deleteText_internal(II)V

    .line 633
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->stopSelectionActionMode()V

    .line 634
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getHtml()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->redoData:Ljava/lang/String;

    .line 635
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->redoSelection:I

    .line 687
    :cond_2
    :goto_0
    return v4

    .line 636
    :cond_3
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_PASTE:I

    if-ne p1, v5, :cond_4

    .line 637
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardservice:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    invoke-interface {v5, v6, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/ClipboardService;->onTextPaste(Ljava/lang/CharSequence;II)V

    .line 638
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getHtml()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoData:Ljava/lang/String;

    .line 639
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Undoable:Z

    .line 640
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoSelection:I

    goto :goto_0

    .line 642
    :cond_4
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_COPY:I

    if-ne p1, v5, :cond_5

    .line 643
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardservice:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;

    invoke-interface {v5, v6}, Lcom/samsung/android/app/memo/uiwidget/ClipboardService;->onTextCopy(Ljava/lang/CharSequence;)V

    .line 644
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->stopSelectionActionMode()V

    goto :goto_0

    .line 645
    :cond_5
    if-ne p1, v8, :cond_2

    .line 646
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    invoke-virtual {v5, p0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->registerClipboardPasteService(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;)V

    .line 647
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->stopSelectionActionMode()V

    goto :goto_0

    .line 652
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    invoke-interface {v5, v6, v7}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;

    .line 653
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_CUT:I

    if-ne p1, v5, :cond_8

    .line 654
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardservice:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;

    invoke-interface {v5, v6}, Lcom/samsung/android/app/memo/uiwidget/ClipboardService;->onTextCopy(Ljava/lang/CharSequence;)V

    .line 656
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getHtml()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoData:Ljava/lang/String;

    .line 657
    iput-boolean v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Undoable:Z

    .line 658
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoSelection:I

    .line 659
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    if-le v5, v6, :cond_7

    .line 660
    new-instance v0, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_LEFT:Landroid/text/Layout$Alignment;

    invoke-direct {v0, v5}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 661
    .local v0, "as":Landroid/text/style/AlignmentSpan$Standard;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v5

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    add-int/lit8 v7, v7, 0x1

    const/16 v8, 0x21

    invoke-interface {v5, v0, v6, v7, v8}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 663
    .end local v0    # "as":Landroid/text/style/AlignmentSpan$Standard;
    :cond_7
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    iget v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    invoke-virtual {p0, v5, v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->deleteText_internal(II)V

    .line 664
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->stopSelectionActionMode()V

    .line 665
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getHtml()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->redoData:Ljava/lang/String;

    .line 666
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v5

    iput v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->redoSelection:I

    goto/16 :goto_0

    .line 668
    :cond_8
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_COPY:I

    if-ne p1, v5, :cond_9

    .line 669
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardservice:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;

    invoke-interface {v5, v6}, Lcom/samsung/android/app/memo/uiwidget/ClipboardService;->onTextCopy(Ljava/lang/CharSequence;)V

    .line 671
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->stopSelectionActionMode()V

    goto/16 :goto_0

    .line 673
    :cond_9
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_PASTE:I

    if-ne p1, v5, :cond_a

    .line 674
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardservice:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;

    iget v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I

    iget v8, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I

    invoke-interface {v5, v6, v7, v8}, Lcom/samsung/android/app/memo/uiwidget/ClipboardService;->onTextPaste(Ljava/lang/CharSequence;II)V

    goto/16 :goto_0

    .line 676
    :cond_a
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_UNDO:I

    if-ne p1, v5, :cond_b

    .line 677
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undo()V

    goto/16 :goto_0

    .line 679
    :cond_b
    iget v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->ID_REDO:I

    if-ne p1, v5, :cond_c

    .line 680
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->redo()V

    goto/16 :goto_0

    .line 687
    :cond_c
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTextContextMenuItem(I)Z

    move-result v4

    goto/16 :goto_0
.end method

.method public performLongClick()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 560
    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->isLongClick:Z

    .line 561
    iget-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsEditMode:Z

    if-eqz v1, :cond_0

    .line 562
    invoke-super {p0}, Landroid/widget/EditText;->performLongClick()Z

    move-result v0

    .line 564
    :cond_0
    return v0
.end method

.method public recycleDragImange()V
    .locals 1

    .prologue
    .line 1827
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mDragBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mDragBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1828
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mDragBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1830
    :cond_0
    return-void
.end method

.method public redo()V
    .locals 3

    .prologue
    .line 1844
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsEditMode:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Redoable:Z

    if-eqz v0, :cond_0

    .line 1845
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getHtml()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoData:Ljava/lang/String;

    .line 1846
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoSelection:I

    .line 1847
    const-wide/16 v0, -0x1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->redoData:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setHtml(JLjava/lang/CharSequence;)V

    .line 1848
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->redoSelection:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSelection(I)V

    .line 1849
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Undoable:Z

    .line 1850
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Redoable:Z

    .line 1853
    :cond_0
    return-void
.end method

.method public registerClipboard()V
    .locals 2

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->newInstance(Landroid/content/Context;)Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    .line 209
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsClipboardRegistered:Z

    .line 210
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isClipBoardExServiceEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->registerClipboardPasteService(Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy$IClipboardEXProxyPateListener;)V

    .line 212
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardExServiceImpl:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardservice:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    .line 213
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardservice:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/samsung/android/app/memo/uiwidget/ClipboardService;->onInit(Landroid/content/Context;Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    .line 218
    :goto_0
    return-void

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardServiceImpl:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardservice:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    .line 216
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardservice:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/samsung/android/app/memo/uiwidget/ClipboardService;->onInit(Landroid/content/Context;Lcom/samsung/android/app/memo/uiwidget/RichEditor;)V

    goto :goto_0
.end method

.method public removeLinks()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 261
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->keyboardShownHandler:Landroid/os/Handler;

    const/4 v6, 0x2

    const-wide/16 v8, 0xc8

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 262
    invoke-virtual {p0, v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setCursorVisible(Z)V

    .line 263
    invoke-virtual {p0, v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setEnabled(Z)V

    .line 264
    invoke-virtual {p0, v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setFocusable(Z)V

    .line 265
    invoke-virtual {p0, v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setFocusableInTouchMode(Z)V

    .line 266
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->requestFocus()Z

    .line 267
    iput-boolean v7, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsEditMode:Z

    .line 268
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 269
    .local v3, "text":Landroid/text/Editable;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->length()I

    move-result v5

    const-class v6, Landroid/text/style/ClickableSpan;

    invoke-interface {v3, v4, v5, v6}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/ClickableSpan;

    .line 270
    .local v1, "links":[Landroid/text/style/ClickableSpan;
    array-length v6, v1

    move v5, v4

    :goto_0
    if-lt v5, v6, :cond_0

    .line 273
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->length()I

    move-result v5

    const-class v6, Landroid/text/style/URLSpan;

    invoke-interface {v3, v4, v5, v6}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/text/style/URLSpan;

    .line 274
    .local v2, "links2":[Landroid/text/style/URLSpan;
    array-length v5, v2

    :goto_1
    if-lt v4, v5, :cond_1

    .line 277
    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mEditViewModeListener:Lcom/samsung/android/app/memo/uiwidget/RichEditor$EditViewModeListener;

    invoke-interface {v4, v7}, Lcom/samsung/android/app/memo/uiwidget/RichEditor$EditViewModeListener;->onModeChanged(Z)V

    .line 278
    return-void

    .line 270
    .end local v2    # "links2":[Landroid/text/style/URLSpan;
    :cond_0
    aget-object v0, v1, v5

    .line 271
    .local v0, "i":Landroid/text/style/ClickableSpan;
    invoke-interface {v3, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 270
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 274
    .end local v0    # "i":Landroid/text/style/ClickableSpan;
    .restart local v2    # "links2":[Landroid/text/style/URLSpan;
    :cond_1
    aget-object v0, v2, v4

    .line 275
    .local v0, "i":Landroid/text/style/URLSpan;
    invoke-interface {v3, v0}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    .line 274
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method removeSpans(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 11
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v10, 0x0

    .line 722
    const/4 v1, 0x0

    .line 723
    .local v1, "isImageExist":Z
    instance-of v8, p1, Landroid/text/Spanned;

    if-eqz v8, :cond_0

    .line 725
    instance-of v8, p1, Landroid/text/Spannable;

    if-eqz v8, :cond_3

    move-object v5, p1

    .line 726
    check-cast v5, Landroid/text/Spannable;

    .line 732
    .local v5, "spannable":Landroid/text/Spannable;
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v8

    const-class v9, Landroid/text/style/SuggestionSpan;

    invoke-interface {v5, v10, v8, v9}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/text/style/SuggestionSpan;

    .line 733
    .local v6, "spans":[Landroid/text/style/SuggestionSpan;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v8, v6

    if-lt v0, v8, :cond_4

    .line 736
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v8

    const-class v9, Landroid/text/style/ImageSpan;

    invoke-interface {v5, v10, v8, v9}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/text/style/ImageSpan;

    .line 737
    .local v4, "spanI":[Landroid/text/style/ImageSpan;
    const/4 v0, 0x0

    :goto_2
    array-length v8, v4

    if-lt v0, v8, :cond_5

    .line 741
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v8

    .line 742
    const-class v9, Landroid/text/style/AlignmentSpan$Standard;

    .line 741
    invoke-interface {v5, v10, v8, v9}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/text/style/AlignmentSpan$Standard;

    .line 743
    .local v3, "spanAS":[Landroid/text/style/AlignmentSpan$Standard;
    const/4 v0, 0x0

    :goto_3
    array-length v8, v4

    if-lt v0, v8, :cond_6

    .line 747
    .end local v0    # "i":I
    .end local v3    # "spanAS":[Landroid/text/style/AlignmentSpan$Standard;
    .end local v4    # "spanI":[Landroid/text/style/ImageSpan;
    .end local v5    # "spannable":Landroid/text/Spannable;
    .end local v6    # "spans":[Landroid/text/style/SuggestionSpan;
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    .line 748
    .local v7, "str":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 749
    :goto_4
    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 756
    :cond_1
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 758
    :cond_2
    return-object v7

    .line 728
    .end local v7    # "str":Ljava/lang/String;
    :cond_3
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 729
    .restart local v5    # "spannable":Landroid/text/Spannable;
    move-object p1, v5

    goto :goto_0

    .line 734
    .restart local v0    # "i":I
    .restart local v6    # "spans":[Landroid/text/style/SuggestionSpan;
    :cond_4
    aget-object v8, v6, v0

    invoke-interface {v5, v8}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 733
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 738
    .restart local v4    # "spanI":[Landroid/text/style/ImageSpan;
    :cond_5
    aget-object v8, v4, v0

    invoke-interface {v5, v8}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 739
    const/4 v1, 0x1

    .line 737
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 744
    .restart local v3    # "spanAS":[Landroid/text/style/AlignmentSpan$Standard;
    :cond_6
    aget-object v8, v3, v0

    invoke-interface {v5, v8}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 743
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 750
    .end local v0    # "i":I
    .end local v3    # "spanAS":[Landroid/text/style/AlignmentSpan$Standard;
    .end local v4    # "spanI":[Landroid/text/style/ImageSpan;
    .end local v5    # "spannable":Landroid/text/Spannable;
    .end local v6    # "spans":[Landroid/text/style/SuggestionSpan;
    .restart local v7    # "str":Ljava/lang/String;
    :cond_7
    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 751
    .local v2, "newLineIndex":I
    if-lez v2, :cond_1

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v2, v8, :cond_1

    .line 752
    invoke-virtual {v7, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    goto :goto_4
.end method

.method public replaceHtmlText(Ljava/lang/CharSequence;II)V
    .locals 5
    .param p1, "seq"    # Ljava/lang/CharSequence;
    .param p2, "min"    # I
    .param p3, "start"    # I

    .prologue
    .line 1800
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1801
    .local v3, "spn":Landroid/text/SpannableStringBuilder;
    if-nez p1, :cond_0

    .line 1815
    :goto_0
    return-void

    .line 1803
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1805
    .local v2, "htmlStr":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1806
    .local v0, "count1":I
    :goto_1
    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1814
    :cond_1
    invoke-direct {p0, v2, v3, p2, p3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->swapHtmlText(Ljava/lang/String;Landroid/text/SpannableStringBuilder;II)V

    goto :goto_0

    .line 1807
    :cond_2
    const/4 v4, 0x2

    if-gt v0, v4, :cond_1

    .line 1809
    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 1810
    .local v1, "endLineIndex":I
    if-lez v1, :cond_3

    .line 1811
    const/4 v4, 0x0

    invoke-virtual {v2, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1812
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public setContentSizeLimit(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 340
    sput p1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mMaxCharSize:I

    .line 341
    return-void
.end method

.method public setCurrentMemoId(Lcom/samsung/android/app/memo/Session;)V
    .locals 0
    .param p1, "s"    # Lcom/samsung/android/app/memo/Session;

    .prologue
    .line 569
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSession:Lcom/samsung/android/app/memo/Session;

    .line 570
    return-void
.end method

.method public setEditMode(Z)V
    .locals 2
    .param p1, "isEditMode"    # Z

    .prologue
    .line 1822
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsEditMode:Z

    .line 1823
    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->linkMovementMethod:Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/samsung/android/app/memo/uiwidget/MyLinkMovementMethod;->setViewMode(Z)V

    .line 1824
    return-void

    .line 1823
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setEditViewModeChangeListener(Lcom/samsung/android/app/memo/uiwidget/RichEditor$EditViewModeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/memo/uiwidget/RichEditor$EditViewModeListener;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mEditViewModeListener:Lcom/samsung/android/app/memo/uiwidget/RichEditor$EditViewModeListener;

    .line 291
    return-void
.end method

.method public setHtml(JLjava/lang/CharSequence;)V
    .locals 3
    .param p1, "baseId"    # J
    .param p3, "htmlContent"    # Ljava/lang/CharSequence;

    .prologue
    .line 306
    if-eqz p3, :cond_0

    .line 307
    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 308
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSpanOnClickListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    if-nez v0, :cond_1

    move-object v0, p0

    .line 307
    :goto_0
    invoke-static {v1, v2, v0, p1, p2}, Lcom/samsung/android/app/memo/util/HtmlUtil;->fromHtml(Ljava/lang/String;Landroid/content/Context;Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;J)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setText(Ljava/lang/CharSequence;)V

    .line 310
    :cond_0
    return-void

    .line 308
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSpanOnClickListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    goto :goto_0
.end method

.method public setLinks()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 247
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setCursorVisible(Z)V

    .line 248
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setFocusable(Z)V

    .line 249
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setFocusableInTouchMode(Z)V

    .line 250
    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsEditMode:Z

    .line 251
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsChineseModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    const/4 v0, 0x7

    invoke-static {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->addLinks(Landroid/widget/TextView;I)Z

    .line 256
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 257
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 256
    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/app/memo/uiwidget/MemoLinkify;->addLinks(Landroid/text/Spannable;ILandroid/content/Context;J)Z

    .line 258
    return-void

    .line 254
    :cond_0
    const/16 v0, 0xf

    invoke-static {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/CustLinkify;->addLinks(Landroid/widget/TextView;I)Z

    goto :goto_0
.end method

.method public setOnSelectionChangedListener(Lcom/samsung/android/app/memo/uiwidget/RichEditor$OnSelectionChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/memo/uiwidget/RichEditor$OnSelectionChangedListener;

    .prologue
    .line 282
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mOnSelectionChangedListener:Lcom/samsung/android/app/memo/uiwidget/RichEditor$OnSelectionChangedListener;

    .line 283
    return-void
.end method

.method public setPasteOperationState(Z)V
    .locals 0
    .param p1, "isGoing"    # Z

    .prologue
    .line 577
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsTextPasteOperationGoing:Z

    .line 578
    return-void
.end method

.method public setSpanOnClickListener(Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    .prologue
    .line 286
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mSpanOnClickListener:Lcom/samsung/android/app/memo/uiwidget/ClickableSpanType$OnClickListener;

    .line 287
    return-void
.end method

.method public showClipboard()V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->showUIDataDialog()V

    .line 230
    :cond_0
    return-void
.end method

.method protected stopSelectionActionModeInvoke()V
    .locals 5

    .prologue
    .line 706
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 707
    .local v1, "edittext":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/android/app/memo/uiwidget/RichEditor;>;"
    const-string v3, "stopSelectionActionMode"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 708
    .local v2, "stopSelectionActionMode":Ljava/lang/reflect/Method;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 709
    const/4 v3, 0x0

    invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 719
    .end local v1    # "edittext":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/android/app/memo/uiwidget/RichEditor;>;"
    .end local v2    # "stopSelectionActionMode":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 710
    :catch_0
    move-exception v0

    .line 711
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;

    const-string v4, "stopSelectionActionModeInvoke "

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 712
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 713
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;

    const-string v4, "stopSelectionActionModeInvoke "

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 714
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 715
    .local v0, "e":Ljava/lang/IllegalAccessException;
    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;

    const-string v4, "stopSelectionActionModeInvoke "

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 716
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 717
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-object v3, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->TAG:Ljava/lang/String;

    const-string v4, "stopSelectionActionModeInvoke "

    invoke-static {v3, v4, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public undo()V
    .locals 3

    .prologue
    .line 1833
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsEditMode:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Undoable:Z

    if-eqz v0, :cond_0

    .line 1834
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getHtml()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->redoData:Ljava/lang/String;

    .line 1835
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->redoSelection:I

    .line 1836
    const-wide/16 v0, -0x1

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoData:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setHtml(JLjava/lang/CharSequence;)V

    .line 1837
    iget v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoSelection:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->setSelection(I)V

    .line 1838
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Undoable:Z

    .line 1839
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Redoable:Z

    .line 1841
    :cond_0
    return-void
.end method

.method public unregisterClipboard()V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardEXProxy:Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/clipboardproxy/ClipboardEXProxy;->unregisterClipboardPasteService()V

    .line 242
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsClipboardRegistered:Z

    .line 244
    :cond_0
    return-void
.end method

.method public updateUndoData(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 1855
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mIsEditMode:Z

    if-eqz v0, :cond_1

    .line 1856
    if-eq p2, p1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getHtml()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1857
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getHtml()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoData:Ljava/lang/String;

    .line 1858
    if-le p1, p2, :cond_2

    .line 1859
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoSelection:I

    .line 1863
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Undoable:Z

    .line 1864
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->Redoable:Z

    .line 1866
    :cond_1
    return-void

    .line 1861
    :cond_2
    iput p2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->undoSelection:I

    goto :goto_0
.end method
