.class public final Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItemResponse;
.super Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;
.source "RecordItemResponse.java"


# static fields
.field private static final RCODE:Ljava/lang/String; = "rcode"


# instance fields
.field private mRcode:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;I)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "timeStamp"    # Ljava/lang/Long;
    .param p3, "rcode"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    .line 36
    iput p3, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItemResponse;->mRcode:I

    .line 37
    return-void
.end method


# virtual methods
.method public fromJSON(Ljava/lang/Object;)V
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;->fromJSON(Ljava/lang/Object;)V

    move-object v0, p1

    .line 41
    check-cast v0, Lorg/json/JSONObject;

    .line 43
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "rcode"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    const-string v1, "rcode"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItemResponse;->mRcode:I

    .line 45
    :cond_0
    return-void
.end method

.method public getRcode()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItemResponse;->mRcode:I

    return v0
.end method

.method public toJSON()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-super {p0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordBase;->toJSON()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 49
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "rcode"

    iget v2, p0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItemResponse;->mRcode:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 50
    return-object v0
.end method
