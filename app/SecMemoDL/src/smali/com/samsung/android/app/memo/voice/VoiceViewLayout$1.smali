.class Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;
.super Ljava/lang/Object;
.source "VoiceViewLayout.java"

# interfaces
.implements Lcom/samsung/android/app/memo/voice/VoiceRecorderBase$RecordListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    .line 991
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;)Lcom/samsung/android/app/memo/voice/VoiceViewLayout;
    .locals 1

    .prologue
    .line 991
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    return-object v0
.end method


# virtual methods
.method public onFailRecording()V
    .locals 2

    .prologue
    .line 1032
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->hideNotification()V

    .line 1033
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$2(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;)V

    .line 1034
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    const v1, 0x7f0b0095

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->showToast(I)V

    .line 1035
    return-void
.end method

.method public onForceStop()V
    .locals 2

    .prologue
    .line 1025
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$2(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;)V

    .line 1026
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setReadyToPlayLayout()V

    .line 1027
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->refreshForceStopTimeFormat()V

    .line 1028
    return-void
.end method

.method public onPauseRecording()V
    .locals 2

    .prologue
    .line 1039
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD_PAUSE:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$2(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;)V

    .line 1040
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setRecordingLayout()V

    .line 1041
    return-void
.end method

.method public onStartRecording(Lcom/samsung/android/app/memo/voice/VoiceInfo;)V
    .locals 2
    .param p1, "voiceInfo"    # Lcom/samsung/android/app/memo/voice/VoiceInfo;

    .prologue
    .line 1011
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->disableSystemSound()V

    .line 1012
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-static {v0, p1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$3(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceInfo;)V

    .line 1013
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    sget-object v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_RECORD:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$2(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;)V

    .line 1014
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1$1;-><init>(Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1021
    return-void
.end method

.method public onStopRecording()V
    .locals 6

    .prologue
    .line 995
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->enableSystemSound()V

    .line 996
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->hideNotification()V

    .line 997
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 998
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 999
    .local v0, "time":Ljava/lang/Long;
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mVoiceinfo:Lcom/samsung/android/app/memo/voice/VoiceInfo;
    invoke-static {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$0(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Lcom/samsung/android/app/memo/voice/VoiceInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    # getter for: Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$1(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/samsung/android/app/memo/util/Utils;->getTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/app/memo/voice/VoiceInfo;->mTitle:Ljava/lang/String;

    .line 1000
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    sget-object v2, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_PLAY_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$2(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;)V

    .line 1001
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->setReadyToPlayLayout()V

    .line 1006
    .end local v0    # "time":Ljava/lang/Long;
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->removeDialogIfshowing()V

    .line 1007
    return-void

    .line 1003
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$1;->this$0:Lcom/samsung/android/app/memo/voice/VoiceViewLayout;

    sget-object v2, Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;->STATE_STOP:Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/voice/VoiceViewLayout;->access$2(Lcom/samsung/android/app/memo/voice/VoiceViewLayout;Lcom/samsung/android/app/memo/voice/VoiceViewLayout$State;)V

    goto :goto_0
.end method
