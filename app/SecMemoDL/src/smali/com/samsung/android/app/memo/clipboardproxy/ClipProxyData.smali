.class public Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;
.super Ljava/lang/Object;
.source "ClipProxyData.java"


# instance fields
.field private data:Ljava/lang/CharSequence;

.field private format:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->format:I

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->data:Ljava/lang/CharSequence;

    .line 22
    return-void
.end method


# virtual methods
.method public getData()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->data:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getFormat()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->format:I

    return v0
.end method

.method public setData(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/CharSequence;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->data:Ljava/lang/CharSequence;

    .line 42
    return-void
.end method

.method public setFormat(I)V
    .locals 0
    .param p1, "format"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;->format:I

    .line 34
    return-void
.end method
