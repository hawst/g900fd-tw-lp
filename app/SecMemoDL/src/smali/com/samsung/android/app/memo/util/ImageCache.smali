.class public Lcom/samsung/android/app/memo/util/ImageCache;
.super Ljava/lang/Object;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;
    }
.end annotation


# static fields
.field private static final DEFAULT_COMPRESS_FORMAT:Landroid/graphics/Bitmap$CompressFormat;

.field private static final DEFAULT_COMPRESS_QUALITY:I = 0x46

.field private static final DEFAULT_DISK_CACHE_ENABLED:Z = true

.field private static final DEFAULT_DISK_CACHE_SIZE:I = 0x1400000

.field private static final DEFAULT_INIT_DISK_CACHE_ON_CREATE:Z = false

.field private static final DEFAULT_MEM_CACHE_ENABLED:Z = true

.field private static final DEFAULT_MEM_CACHE_SIZE:I = 0x2800

.field private static final TAG:Ljava/lang/String; = "ImageCache"

.field static imageCache:Lcom/samsung/android/app/memo/util/ImageCache;


# instance fields
.field private mCacheParams:Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;

.field private final mDiskCacheLock:Ljava/lang/Object;

.field private mMemoryCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    sput-object v0, Lcom/samsung/android/app/memo/util/ImageCache;->DEFAULT_COMPRESS_FORMAT:Landroid/graphics/Bitmap$CompressFormat;

    .line 57
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/memo/util/ImageCache;->imageCache:Lcom/samsung/android/app/memo/util/ImageCache;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;)V
    .locals 1
    .param p1, "cacheParams"    # Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mDiskCacheLock:Ljava/lang/Object;

    .line 60
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/util/ImageCache;->init(Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;)V

    .line 61
    return-void
.end method

.method static synthetic access$0()Landroid/graphics/Bitmap$CompressFormat;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/samsung/android/app/memo/util/ImageCache;->DEFAULT_COMPRESS_FORMAT:Landroid/graphics/Bitmap$CompressFormat;

    return-object v0
.end method

.method private static bytesToHexString([B)Ljava/lang/String;
    .locals 5
    .param p0, "bytes"    # [B

    .prologue
    .line 168
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 169
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p0

    if-lt v1, v3, :cond_0

    .line 176
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 170
    :cond_0
    aget-byte v3, p0, v1

    and-int/lit16 v3, v3, 0xff

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "hex":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 172
    const/16 v3, 0x30

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 174
    :cond_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static findOrCreateCache(Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;)Lcom/samsung/android/app/memo/util/ImageCache;
    .locals 1
    .param p0, "cacheParams"    # Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;

    .prologue
    .line 66
    sget-object v0, Lcom/samsung/android/app/memo/util/ImageCache;->imageCache:Lcom/samsung/android/app/memo/util/ImageCache;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Lcom/samsung/android/app/memo/util/ImageCache;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/memo/util/ImageCache;-><init>(Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;)V

    sput-object v0, Lcom/samsung/android/app/memo/util/ImageCache;->imageCache:Lcom/samsung/android/app/memo/util/ImageCache;

    .line 70
    :cond_0
    sget-object v0, Lcom/samsung/android/app/memo/util/ImageCache;->imageCache:Lcom/samsung/android/app/memo/util/ImageCache;

    return-object v0
.end method

.method public static getBitmapSize(Landroid/graphics/Bitmap;)I
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 192
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v0

    return v0
.end method

.method public static getExternalCacheDir(Landroid/content/Context;)Ljava/io/File;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 185
    invoke-virtual {p0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static getUsableSpace(Ljava/io/File;)J
    .locals 2
    .param p0, "path"    # Ljava/io/File;

    .prologue
    .line 188
    invoke-virtual {p0}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v0

    return-wide v0
.end method

.method public static hashKeyForDisk(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 154
    :try_start_0
    const-string v3, "MD5"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 156
    .local v2, "mDigest":Ljava/security/MessageDigest;
    :try_start_1
    const-string v3, "UTF-8"

    invoke-virtual {p0, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/security/MessageDigest;->update([B)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    .line 160
    :goto_0
    :try_start_2
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/app/memo/util/ImageCache;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v0

    .line 164
    .end local v2    # "mDigest":Ljava/security/MessageDigest;
    .local v0, "cacheKey":Ljava/lang/String;
    :goto_1
    return-object v0

    .line 157
    .end local v0    # "cacheKey":Ljava/lang/String;
    .restart local v2    # "mDigest":Ljava/security/MessageDigest;
    :catch_0
    move-exception v1

    .line 158
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    const-string v3, "ImageCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UnsupportedEncodingException :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 161
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v2    # "mDigest":Ljava/security/MessageDigest;
    :catch_1
    move-exception v1

    .line 162
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "cacheKey":Ljava/lang/String;
    goto :goto_1
.end method

.method private init(Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;)V
    .locals 3
    .param p1, "cacheParams"    # Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;

    .prologue
    .line 75
    iget-object v1, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mDiskCacheLock:Ljava/lang/Object;

    monitor-enter v1

    .line 76
    :try_start_0
    iput-object p1, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mCacheParams:Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;

    .line 78
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mCacheParams:Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;

    iget-boolean v0, v0, Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;->memoryCacheEnabled:Z

    if-eqz v0, :cond_0

    .line 79
    new-instance v0, Lcom/samsung/android/app/memo/util/ImageCache$1;

    iget-object v2, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mCacheParams:Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;

    iget v2, v2, Lcom/samsung/android/app/memo/util/ImageCache$ImageCacheParams;->memCacheSize:I

    invoke-direct {v0, p0, v2}, Lcom/samsung/android/app/memo/util/ImageCache$1;-><init>(Lcom/samsung/android/app/memo/util/ImageCache;I)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mMemoryCache:Landroid/support/v4/util/LruCache;

    .line 75
    :cond_0
    monitor-exit v1

    .line 88
    return-void

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static isExternalStorageRemovable()Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 181
    invoke-static {}, Landroid/os/Environment;->isExternalStorageRemovable()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public addBitmapToCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 92
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mMemoryCache:Landroid/support/v4/util/LruCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mMemoryCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mMemoryCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public clearCache()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mMemoryCache:Landroid/support/v4/util/LruCache;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mMemoryCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0}, Landroid/support/v4/util/LruCache;->evictAll()V

    .line 126
    :cond_0
    return-void
.end method

.method public getBitmapFromMemCache(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 112
    iget-object v1, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mMemoryCache:Landroid/support/v4/util/LruCache;

    if-eqz v1, :cond_0

    .line 113
    iget-object v1, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mMemoryCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 114
    .local v0, "memBitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 118
    .end local v0    # "memBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeBitmapFromCache(Ljava/lang/String;)V
    .locals 1
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 102
    if-nez p1, :cond_1

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mMemoryCache:Landroid/support/v4/util/LruCache;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/samsung/android/app/memo/util/ImageCache;->mMemoryCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/support/v4/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
