.class public Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;
.super Landroid/os/AsyncTask;
.source "MemoContentFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ImageCacheLoaderTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field id:I

.field invalidate:Z

.field mActivity:Landroid/app/Activity;

.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;ILandroid/app/Activity;)V
    .locals 1
    .param p2, "id"    # I
    .param p3, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1165
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;->invalidate:Z

    .line 1166
    iput p2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;->id:I

    .line 1167
    iput-object p3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;->mActivity:Landroid/app/Activity;

    .line 1168
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 9
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v8, 0x0

    .line 1173
    const/4 v1, 0x0

    .line 1175
    .local v1, "content":Ljava/lang/String;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;->mActivity:Landroid/app/Activity;

    iget v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;->id:I

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/MemoDataHandler;->getContent(Landroid/content/Context;I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1179
    :goto_0
    if-nez v1, :cond_1

    .line 1197
    :cond_0
    return-object v8

    .line 1176
    :catch_0
    move-exception v2

    .line 1177
    .local v2, "e":Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$21()Ljava/lang/String;

    move-result-object v6

    const-string v7, "getImageUris"

    invoke-static {v6, v7, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1181
    .end local v2    # "e":Lcom/samsung/android/app/memo/MemoDataHandler$MemoDataHandlerException;
    :cond_1
    invoke-static {v1}, Lcom/samsung/android/app/memo/util/ImageUtil;->getImageUriList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 1182
    .local v4, "imageList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v3, v6, -0x1

    .local v3, "i":I
    :goto_1
    if-ltz v3, :cond_0

    .line 1183
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    .line 1185
    .local v5, "uri":Landroid/net/Uri;
    sget-object v6, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMemoryCache:Landroid/util/LruCache;

    if-eqz v6, :cond_2

    .line 1186
    sget-object v6, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMemoryCache:Landroid/util/LruCache;

    invoke-virtual {v6, v5}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1187
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_3

    .line 1182
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    :goto_2
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 1189
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getDrawableFromLocalUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    invoke-static {v6, v5}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$18(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1190
    if-eqz v0, :cond_2

    .line 1191
    sget-object v6, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMemoryCache:Landroid/util/LruCache;

    invoke-virtual {v6, v5, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1192
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;->invalidate:Z

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 1203
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;->invalidate:Z

    if-eqz v0, :cond_0

    .line 1204
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->invalidate()V

    .line 1205
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$ImageCacheLoaderTask;->invalidate:Z

    .line 1208
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1210
    return-void
.end method
