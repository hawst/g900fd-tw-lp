.class public Lcom/samsung/android/app/memo/voice/ScreenReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ScreenReceiver.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/voice/ScreenReceiver;->mContext:Landroid/content/Context;

    .line 10
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 17
    const-string v0, "ScreenReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onReceive() intent:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 21
    const-string v0, "ScreenReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onReceive() action:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    iput-object p1, p0, Lcom/samsung/android/app/memo/voice/ScreenReceiver;->mContext:Landroid/content/Context;

    .line 25
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 26
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/voice/ScreenReceiver;->showHideLedNotification(Z)V

    .line 31
    :cond_0
    :goto_0
    return-void

    .line 27
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/voice/ScreenReceiver;->showHideLedNotification(Z)V

    goto :goto_0
.end method

.method showHideLedNotification(Z)V
    .locals 3
    .param p1, "show"    # Z

    .prologue
    .line 35
    const-string v0, "ScreenReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "showHideLedNotification() show:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    invoke-static {}, Lcom/samsung/android/app/memo/voice/VRUtils;->hasLED()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/ScreenReceiver;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/ScreenReceiver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/app/memo/voice/VRUtils;->isEnabledLED(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/samsung/android/app/memo/voice/ScreenReceiver;->mContext:Landroid/content/Context;

    invoke-static {p1, v0}, Lcom/samsung/android/app/memo/voice/VRUtils;->toggleMemoLed(ZLandroid/content/Context;)V

    .line 42
    :cond_0
    return-void
.end method
