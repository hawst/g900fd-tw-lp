.class Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;
.super Landroid/print/PrintDocumentAdapter;
.source "GooglePrintPdf.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/print/GooglePrintPdf;->printBitmap(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/print/PrintManager$PrintJobStateChangeListener;)Landroid/print/PrintJob;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mAttributes:Landroid/print/PrintAttributes;

.field private pdfDocument:Landroid/print/pdf/PrintedPdfDocument;

.field final synthetic this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

.field private final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/print/GooglePrintPdf;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    iput-object p2, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->val$context:Landroid/content/Context;

    .line 76
    invoke-direct {p0}, Landroid/print/PrintDocumentAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 227
    const/4 v0, 0x0

    .local v0, "p":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->pagesCount:I
    invoke-static {v1}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$2(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 231
    iget-object v1, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFile:Ljava/io/File;
    invoke-static {v1}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$4(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 232
    invoke-super {p0}, Landroid/print/PrintDocumentAdapter;->onFinish()V

    .line 233
    return-void

    .line 228
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFileList:Ljava/util/List;
    invoke-static {v1}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$0(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 227
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onLayout(Landroid/print/PrintAttributes;Landroid/print/PrintAttributes;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$LayoutResultCallback;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "oldPrintAttributes"    # Landroid/print/PrintAttributes;
    .param p2, "newPrintAttributes"    # Landroid/print/PrintAttributes;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "layoutResultCallback"    # Landroid/print/PrintDocumentAdapter$LayoutResultCallback;
    .param p5, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 86
    iput-object p2, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->mAttributes:Landroid/print/PrintAttributes;

    .line 94
    iget-object v1, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    iget-object v2, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFileList:Ljava/util/List;
    invoke-static {v2}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$0(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$1(Lcom/samsung/android/app/memo/print/GooglePrintPdf;I)V

    .line 95
    iget-object v1, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->pagesCount:I
    invoke-static {v1}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$2(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)I

    move-result v1

    if-lez v1, :cond_0

    .line 96
    new-instance v1, Landroid/print/PrintDocumentInfo$Builder;

    iget-object v2, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mJobName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$3(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/print/PrintDocumentInfo$Builder;-><init>(Ljava/lang/String;)V

    .line 97
    invoke-virtual {v1, v3}, Landroid/print/PrintDocumentInfo$Builder;->setContentType(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v1

    .line 98
    iget-object v2, p0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->pagesCount:I
    invoke-static {v2}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$2(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/print/PrintDocumentInfo$Builder;->setPageCount(I)Landroid/print/PrintDocumentInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/print/PrintDocumentInfo$Builder;->build()Landroid/print/PrintDocumentInfo;

    move-result-object v0

    .line 100
    .local v0, "info":Landroid/print/PrintDocumentInfo;
    invoke-virtual {p4, v0, v3}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFinished(Landroid/print/PrintDocumentInfo;Z)V

    .line 104
    .end local v0    # "info":Landroid/print/PrintDocumentInfo;
    :goto_0
    return-void

    .line 102
    :cond_0
    const-string v1, "failed"

    invoke-virtual {p4, v1}, Landroid/print/PrintDocumentAdapter$LayoutResultCallback;->onLayoutFailed(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onWrite([Landroid/print/PageRange;Landroid/os/ParcelFileDescriptor;Landroid/os/CancellationSignal;Landroid/print/PrintDocumentAdapter$WriteResultCallback;)V
    .locals 23
    .param p1, "pageRanges"    # [Landroid/print/PageRange;
    .param p2, "fileDescriptor"    # Landroid/os/ParcelFileDescriptor;
    .param p3, "cancellationSignal"    # Landroid/os/CancellationSignal;
    .param p4, "writeResultCallback"    # Landroid/print/PrintDocumentAdapter$WriteResultCallback;

    .prologue
    .line 109
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v3, "bitmapArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/CancellationSignal;->isCanceled()Z

    move-result v20

    if-eqz v20, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->pdfDocument:Landroid/print/pdf/PrintedPdfDocument;

    move-object/from16 v20, v0

    if-eqz v20, :cond_5

    .line 111
    invoke-virtual/range {p4 .. p4}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteCancelled()V

    .line 112
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_0

    .line 113
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v20

    if-lt v7, v0, :cond_2

    .line 120
    const/4 v13, 0x0

    .local v13, "p":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->pagesCount:I
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$2(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)I

    move-result v20

    move/from16 v0, v20

    if-lt v13, v0, :cond_4

    .line 123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFile:Ljava/io/File;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$4(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)Ljava/io/File;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z

    .line 125
    .end local v7    # "i":I
    .end local v13    # "p":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->pdfDocument:Landroid/print/pdf/PrintedPdfDocument;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 126
    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->pdfDocument:Landroid/print/pdf/PrintedPdfDocument;

    .line 222
    :cond_1
    :goto_2
    return-void

    .line 114
    .restart local v7    # "i":I
    :cond_2
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 115
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v20

    if-nez v20, :cond_3

    .line 116
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 113
    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 121
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v13    # "p":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFileList:Ljava/util/List;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$0(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)Ljava/util/List;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/io/File;

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->delete()Z

    .line 120
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 130
    .end local v7    # "i":I
    .end local v13    # "p":I
    :cond_5
    :try_start_0
    new-instance v20, Landroid/print/pdf/PrintedPdfDocument;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->val$context:Landroid/content/Context;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->mAttributes:Landroid/print/PrintAttributes;

    move-object/from16 v22, v0

    invoke-direct/range {v20 .. v22}, Landroid/print/pdf/PrintedPdfDocument;-><init>(Landroid/content/Context;Landroid/print/PrintAttributes;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->pdfDocument:Landroid/print/pdf/PrintedPdfDocument;

    .line 131
    const/4 v15, 0x0

    .line 132
    .local v15, "pageCount":I
    const/16 v17, 0x0

    .local v17, "start":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->pagesCount:I
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$2(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)I

    move-result v20

    add-int/lit8 v6, v20, -0x1

    .line 134
    .local v6, "end":I
    move/from16 v9, v17

    .local v9, "m":I
    :goto_3
    if-le v9, v6, :cond_7

    .line 142
    if-eqz p1, :cond_6

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v20, v0

    if-lez v20, :cond_6

    .line 144
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_4
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v20

    if-le v7, v0, :cond_9

    .line 193
    .end local v7    # "i":I
    :cond_6
    const/4 v13, 0x0

    .restart local v13    # "p":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->pagesCount:I
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$2(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v20

    add-int/lit8 v20, v20, -0x1

    move/from16 v0, v20

    if-le v13, v0, :cond_f

    .line 202
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->pdfDocument:Landroid/print/pdf/PrintedPdfDocument;

    move-object/from16 v20, v0

    new-instance v21, Ljava/io/FileOutputStream;

    invoke-virtual/range {p2 .. p2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    invoke-virtual/range {v20 .. v21}, Landroid/print/pdf/PrintedPdfDocument;->writeTo(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206
    :goto_6
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->pdfDocument:Landroid/print/pdf/PrintedPdfDocument;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/print/pdf/PrintedPdfDocument;->close()V

    .line 208
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/print/PrintDocumentAdapter$WriteResultCallback;->onWriteFinished([Landroid/print/PageRange;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 214
    if-eqz p2, :cond_1

    .line 216
    :try_start_3
    invoke-virtual/range {p2 .. p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_2

    .line 217
    :catch_0
    move-exception v8

    .line 218
    .local v8, "ioe":Ljava/io/IOException;
    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$5()Ljava/lang/String;

    move-result-object v20

    const-string v21, "onWrite"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v8}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 136
    .end local v8    # "ioe":Ljava/io/IOException;
    .end local v13    # "p":I
    :cond_7
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFileList:Ljava/util/List;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$0(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)Ljava/util/List;

    move-result-object v20

    if-eqz v20, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFileList:Ljava/util/List;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$0(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)Ljava/util/List;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    if-eqz v20, :cond_8

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->this$0:Lcom/samsung/android/app/memo/print/GooglePrintPdf;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->mFileList:Ljava/util/List;
    invoke-static/range {v20 .. v20}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$0(Lcom/samsung/android/app/memo/print/GooglePrintPdf;)Ljava/util/List;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/io/File;

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 138
    .local v11, "mbitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    .end local v11    # "mbitmap":Landroid/graphics/Bitmap;
    :cond_8
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_3

    .line 146
    .restart local v7    # "i":I
    :cond_9
    const/16 v20, 0x0

    aget-object v20, p1, v20

    sget-object v21, Landroid/print/PageRange;->ALL_PAGES:Landroid/print/PageRange;

    invoke-virtual/range {v20 .. v21}, Landroid/print/PageRange;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_a

    .line 148
    aget-object v20, p1, v7

    invoke-virtual/range {v20 .. v20}, Landroid/print/PageRange;->getStart()I

    move-result v17

    .line 149
    aget-object v20, p1, v7

    invoke-virtual/range {v20 .. v20}, Landroid/print/PageRange;->getEnd()I

    move-result v6

    .line 152
    :cond_a
    move/from16 v12, v17

    .local v12, "n":I
    :goto_7
    if-le v12, v6, :cond_b

    .line 144
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_4

    .line 153
    :cond_b
    invoke-virtual {v3, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 155
    .restart local v2    # "bitmap":Landroid/graphics/Bitmap;
    if-eqz v3, :cond_d

    if-eqz v2, :cond_d

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->pdfDocument:Landroid/print/pdf/PrintedPdfDocument;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Landroid/print/pdf/PrintedPdfDocument;->startPage(I)Landroid/graphics/pdf/PdfDocument$Page;

    move-result-object v14

    .line 158
    .local v14, "page":Landroid/graphics/pdf/PdfDocument$Page;
    new-instance v4, Landroid/graphics/RectF;

    invoke-virtual {v14}, Landroid/graphics/pdf/PdfDocument$Page;->getInfo()Landroid/graphics/pdf/PdfDocument$PageInfo;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/pdf/PdfDocument$PageInfo;->getContentRect()Landroid/graphics/Rect;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v4, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 159
    .local v4, "content":Landroid/graphics/RectF;
    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    .line 162
    .local v10, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v20

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    .line 163
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v21

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    div-float v21, v21, v22

    .line 162
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->min(FF)F

    move-result v16

    .line 164
    .local v16, "scale":F
    move/from16 v0, v16

    move/from16 v1, v16

    invoke-virtual {v10, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 168
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v20

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    .line 169
    mul-float v21, v21, v16

    .line 168
    sub-float v20, v20, v21

    .line 169
    const/high16 v21, 0x40000000    # 2.0f

    .line 168
    div-float v18, v20, v21

    .line 170
    .local v18, "translateX":F
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v20

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    .line 171
    mul-float v21, v21, v16

    .line 170
    sub-float v20, v20, v21

    .line 171
    const/high16 v21, 0x40000000    # 2.0f

    .line 170
    div-float v19, v20, v21

    .line 172
    .local v19, "translateY":F
    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 175
    invoke-virtual {v14}, Landroid/graphics/pdf/PdfDocument$Page;->getCanvas()Landroid/graphics/Canvas;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v2, v10, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/memo/print/GooglePrintPdf$1;->pdfDocument:Landroid/print/pdf/PrintedPdfDocument;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Landroid/print/pdf/PrintedPdfDocument;->finishPage(Landroid/graphics/pdf/PdfDocument$Page;)V

    .line 179
    if-eqz v2, :cond_c

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v20

    if-nez v20, :cond_c

    .line 180
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 183
    :cond_c
    const/4 v2, 0x0

    .line 184
    add-int/lit8 v15, v15, 0x1

    .line 152
    .end local v4    # "content":Landroid/graphics/RectF;
    .end local v10    # "matrix":Landroid/graphics/Matrix;
    .end local v14    # "page":Landroid/graphics/pdf/PdfDocument$Page;
    .end local v16    # "scale":F
    .end local v18    # "translateX":F
    .end local v19    # "translateY":F
    :goto_8
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_7

    .line 186
    :cond_d
    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$5()Ljava/lang/String;

    move-result-object v20

    const-string v21, "onWrite Invalid image received"

    invoke-static/range {v20 .. v21}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_8

    .line 213
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local v6    # "end":I
    .end local v7    # "i":I
    .end local v9    # "m":I
    .end local v12    # "n":I
    .end local v15    # "pageCount":I
    .end local v17    # "start":I
    :catchall_0
    move-exception v20

    .line 214
    if-eqz p2, :cond_e

    .line 216
    :try_start_5
    invoke-virtual/range {p2 .. p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 221
    :cond_e
    :goto_9
    throw v20

    .line 195
    .restart local v6    # "end":I
    .restart local v9    # "m":I
    .restart local v13    # "p":I
    .restart local v15    # "pageCount":I
    .restart local v17    # "start":I
    :cond_f
    if-eqz v3, :cond_10

    :try_start_6
    invoke-virtual {v3, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    if-eqz v20, :cond_10

    .line 196
    invoke-virtual {v3, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/graphics/Bitmap;

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v20

    if-nez v20, :cond_10

    .line 197
    invoke-virtual {v3, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/graphics/Bitmap;

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->recycle()V

    .line 193
    :cond_10
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_5

    .line 203
    :catch_1
    move-exception v5

    .line 204
    .local v5, "e":Ljava/io/IOException;
    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$5()Ljava/lang/String;

    move-result-object v20

    const-string v21, "onWrite"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v5}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_6

    .line 217
    .end local v5    # "e":Ljava/io/IOException;
    .end local v6    # "end":I
    .end local v9    # "m":I
    .end local v13    # "p":I
    .end local v15    # "pageCount":I
    .end local v17    # "start":I
    :catch_2
    move-exception v8

    .line 218
    .restart local v8    # "ioe":Ljava/io/IOException;
    # getter for: Lcom/samsung/android/app/memo/print/GooglePrintPdf;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/print/GooglePrintPdf;->access$5()Ljava/lang/String;

    move-result-object v21

    const-string v22, "onWrite"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-static {v0, v1, v8}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_9
.end method
