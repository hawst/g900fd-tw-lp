.class Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$3;
.super Ljava/lang/Object;
.source "MemoContentFragment.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$3;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    .line 1675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 1678
    instance-of v0, p1, Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 1679
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$3;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->registerClipboard()V

    .line 1682
    :goto_0
    return-void

    .line 1681
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$3;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->unregisterClipboard()V

    goto :goto_0
.end method
