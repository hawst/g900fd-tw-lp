.class public Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;
.super Landroid/app/ListFragment;
.source "CategoryManagementFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/ListFragment;",
        "Landroid/widget/AdapterView$OnItemLongClickListener;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final CID_DISPLAY_NAME:I = 0x2

.field private static final CID_ID:I = 0x0

.field private static final CID_UUID:I = 0x1

.field private static final ORDERBY:Ljava/lang/String; = "orderBy ASC"

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final SELECTION:Ljava/lang/String; = "isDeleted IS 0"

.field private static final TAG:Ljava/lang/String;

.field public static mIsAlive:Z

.field private static mListViewItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/memo/uifragment/CategoryListItem;",
            ">;"
        }
    .end annotation
.end field

.field private static mMenuActionBar:Landroid/view/Menu;

.field private static mNoCategoryPopupView:Landroid/widget/TextView;

.field private static mNoCategoryView:Landroid/widget/TextView;

.field private static mTempListViewItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/memo/uifragment/CategoryListItem;",
            ">;"
        }
    .end annotation
.end field

.field private static mlightNoItemCategory:Landroid/widget/ImageView;


# instance fields
.field public final TYPE_DELETED:I

.field public final TYPE_REORDERED:I

.field private mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

.field private mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

.field private mContainerView:Landroid/view/View;

.field private mCursorLoader:Landroid/content/CursorLoader;

.field private mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

.field private mIsFirstTime:Z

.field mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 81
    const-class v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->TAG:Ljava/lang/String;

    .line 83
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    .line 84
    const-string v1, "_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "UUID"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    .line 83
    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->PROJECTION:[Ljava/lang/String;

    .line 97
    sput-boolean v3, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mIsAlive:Z

    .line 124
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 79
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 99
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mIsFirstTime:Z

    .line 126
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->TYPE_DELETED:I

    .line 128
    iput v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->TYPE_REORDERED:I

    .line 79
    return-void
.end method

.method private InitView()V
    .locals 4

    .prologue
    .line 213
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    if-nez v0, :cond_0

    .line 214
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 215
    const v2, 0x7f04000c

    sget-object v3, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 214
    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mContainerView:Landroid/view/View;

    const v1, 0x7f0e000c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mNoCategoryView:Landroid/widget/TextView;

    .line 221
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mContainerView:Landroid/view/View;

    const v1, 0x7f0e000b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mNoCategoryPopupView:Landroid/widget/TextView;

    .line 222
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    if-nez v0, :cond_1

    .line 223
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mContainerView:Landroid/view/View;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uiwidget/DndListView;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    .line 224
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 225
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    invoke-virtual {v0, p0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 226
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    new-instance v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$1;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setDragListener(Lcom/samsung/android/app/memo/uiwidget/DndListView$OnDndDragListener;)V

    .line 237
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    new-instance v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$2;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setOnMoveCompletedListener(Lcom/samsung/android/app/memo/uiwidget/DndListView$OnMoveCompletedListener;)V

    .line 272
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->setListView(Lcom/samsung/android/app/memo/uiwidget/DndListView;)V

    .line 275
    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;)Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    return-object v0
.end method

.method static synthetic access$2()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;)Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mIsFirstTime:Z

    return v0
.end method

.method static synthetic access$4()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mTempListViewItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;Z)V
    .locals 0

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mIsFirstTime:Z

    return-void
.end method

.method static synthetic access$6()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private clearObjects()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 400
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->TAG:Ljava/lang/String;

    const-string v1, " clearObjects()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mTempListViewItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 402
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mTempListViewItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 403
    sput-object v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mTempListViewItems:Ljava/util/ArrayList;

    .line 405
    :cond_0
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 406
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 407
    sput-object v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    .line 409
    :cond_1
    return-void
.end method

.method private getOrderByUUID(ILandroid/content/ContentResolver;)I
    .locals 9
    .param p1, "pos"    # I
    .param p2, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 475
    const/4 v8, -0x1

    .line 476
    .local v8, "order":I
    const/4 v6, 0x0

    .line 478
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "orderBy"

    aput-object v3, v2, v0

    .line 479
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "UUID = \'"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->getCategoryUUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p2

    .line 478
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 480
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481
    const-string v0, "orderBy"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 485
    :cond_0
    if-eqz v6, :cond_1

    .line 486
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 488
    :cond_1
    :goto_0
    return v8

    .line 482
    :catch_0
    move-exception v7

    .line 483
    .local v7, "e":Landroid/database/SQLException;
    :try_start_1
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->TAG:Ljava/lang/String;

    const-string v1, "SQLException in getOrderByUUID(): "

    invoke-static {v0, v1, v7}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 485
    if-eqz v6, :cond_1

    .line 486
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 484
    .end local v7    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v0

    .line 485
    if-eqz v6, :cond_2

    .line 486
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 487
    :cond_2
    throw v0
.end method

.method private setEditCategoryMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 278
    sput-object p1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mMenuActionBar:Landroid/view/Menu;

    .line 279
    return-void
.end method


# virtual methods
.method public doReorderItems(IIILandroid/content/ContentResolver;)V
    .locals 7
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "Type"    # I
    .param p4, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v6, 0x0

    .line 422
    const/4 v2, 0x1

    .line 424
    .local v2, "offsetForOrder":I
    packed-switch p3, :pswitch_data_0

    .line 468
    :cond_0
    return-void

    .line 426
    :pswitch_0
    const/4 v2, -0x1

    .line 427
    const/4 v1, 0x0

    .line 429
    .local v1, "k":I
    sget-object v4, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge p1, v4, :cond_0

    .line 432
    if-ltz p1, :cond_0

    sget-object v4, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge p2, v4, :cond_0

    .line 433
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-gt v0, p2, :cond_0

    .line 434
    if-eq v0, p1, :cond_1

    .line 435
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 436
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "orderBy"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 438
    sget-object v4, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getSelectionByUUID(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p4, v4, v3, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 439
    add-int/lit8 v1, v1, 0x1

    .line 433
    .end local v3    # "values":Landroid/content/ContentValues;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 446
    .end local v0    # "i":I
    .end local v1    # "k":I
    :pswitch_1
    if-ltz p1, :cond_0

    sget-object v4, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge p2, v4, :cond_0

    .line 447
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 448
    .restart local v3    # "values":Landroid/content/ContentValues;
    const-string v4, "orderBy"

    invoke-direct {p0, p2, p4}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getOrderByUUID(ILandroid/content/ContentResolver;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 449
    sget-object v4, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getSelectionByUUID(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p4, v4, v3, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 450
    if-ge p1, p2, :cond_2

    .line 451
    add-int/lit8 v0, p1, 0x1

    .restart local v0    # "i":I
    :goto_1
    if-gt v0, p2, :cond_0

    .line 452
    const-string v4, "orderBy"

    invoke-direct {p0, v0, p4}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getOrderByUUID(ILandroid/content/ContentResolver;)I

    move-result v5

    sub-int/2addr v5, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 453
    sget-object v4, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getSelectionByUUID(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p4, v4, v3, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 451
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 455
    .end local v0    # "i":I
    :cond_2
    if-le p1, p2, :cond_0

    .line 456
    add-int/lit8 v0, p1, -0x1

    .restart local v0    # "i":I
    :goto_2
    if-lt v0, p2, :cond_0

    .line 457
    const-string v4, "orderBy"

    invoke-direct {p0, v0, p4}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getOrderByUUID(ILandroid/content/ContentResolver;)I

    move-result v5

    add-int/2addr v5, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 458
    sget-object v4, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getSelectionByUUID(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p4, v4, v3, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 456
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 424
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getCategoryManagementListViewItems()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/app/memo/uifragment/CategoryListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 492
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSelectionByUUID(I)Ljava/lang/String;
    .locals 2
    .param p1, "pos"    # I

    .prologue
    .line 471
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "UUID IS \'"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->getCategoryUUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 382
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 383
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->TAG:Ljava/lang/String;

    const-string v1, "onActivityCreated()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->setHasOptionsMenu(Z)V

    .line 385
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 136
    const/4 v2, 0x1

    sput-boolean v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mIsAlive:Z

    .line 137
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onAttach(Landroid/app/Activity;)V

    .line 140
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->TAG:Ljava/lang/String;

    const-string v3, "onAttach()"

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-object v2, v0

    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    return-void

    .line 143
    :catch_0
    move-exception v1

    .line 144
    .local v1, "e":Ljava/lang/ClassCastException;
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->TAG:Ljava/lang/String;

    const-string v3, "onAttach"

    invoke-static {v2, v3, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 145
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 146
    const-string v4, " must implement MemoListActionListener"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 145
    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 7
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v6, 0x7f0e0004

    .line 157
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 158
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 160
    .local v0, "activity":Landroid/app/Activity;
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 162
    .local v1, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getView()Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 164
    .local v4, "viewGroup":Landroid/view/ViewGroup;
    invoke-virtual {v4}, Landroid/view/ViewGroup;->removeAllViewsInLayout()V

    .line 165
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    .line 167
    const v5, 0x7f04000b

    invoke-virtual {v1, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mContainerView:Landroid/view/View;

    .line 168
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->InitView()V

    .line 169
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->getCount()I

    move-result v5

    if-nez v5, :cond_2

    const/4 v5, 0x1

    :goto_0
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->showNoCategory(Z)V

    .line 172
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v4    # "viewGroup":Landroid/view/ViewGroup;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 173
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    .line 174
    invoke-virtual {v5, v6}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v3

    .line 173
    check-cast v3, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;

    .line 175
    .local v3, "mCategoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 176
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 177
    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->getView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 179
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "mCategoryListFragment":Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;
    :cond_1
    return-void

    .line 169
    .restart local v1    # "inflater":Landroid/view/LayoutInflater;
    .restart local v4    # "viewGroup":Landroid/view/ViewGroup;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 183
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 184
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->TAG:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->setRetainInstance(Z)V

    .line 186
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    .line 187
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mTempListViewItems:Ljava/util/ArrayList;

    .line 188
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 190
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 549
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    .line 550
    sget-object v3, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->PROJECTION:[Ljava/lang/String;

    const-string v4, "isDeleted IS 0"

    const/4 v5, 0x0

    const-string v6, "orderBy ASC"

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCursorLoader:Landroid/content/CursorLoader;

    .line 552
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCursorLoader:Landroid/content/CursorLoader;

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 11
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    const v10, 0x7f0b0021

    const/4 v9, 0x1

    .line 290
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 291
    const/high16 v5, 0x7f0d0000

    invoke-virtual {p2, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 293
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v5, v9}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setDrawerLock(Z)V

    .line 294
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v5, v10}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setActionBarTitle(I)V

    .line 295
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 296
    .local v0, "actionBar":Landroid/app/ActionBar;
    const v5, 0x7f0200e5

    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 297
    const v5, 0x7f0b0022

    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setHomeActionContentDescription(I)V

    .line 298
    invoke-virtual {v0, v9}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 300
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v5

    const-string v6, "action_bar_title"

    const-string v7, "id"

    const-string v8, "android"

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 301
    .local v1, "actionBarTitleId":I
    if-lez v1, :cond_0

    .line 302
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 303
    .local v3, "titleView":Landroid/widget/TextView;
    if-eqz v3, :cond_0

    .line 304
    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setText(I)V

    .line 305
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    const-string v6, "or_IN"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 306
    invoke-virtual {v3}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 307
    .local v2, "textPaint":Landroid/graphics/Paint;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    float-to-int v4, v5

    .line 308
    .local v4, "w":I
    add-int/lit8 v5, v4, 0xa

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setWidth(I)V

    .line 312
    .end local v2    # "textPaint":Landroid/graphics/Paint;
    .end local v3    # "titleView":Landroid/widget/TextView;
    .end local v4    # "w":I
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->setEditCategoryMenu(Landroid/view/Menu;)V

    .line 313
    const v5, 0x7f0e0091

    invoke-interface {p1, v5}, Landroid/view/Menu;->removeItem(I)V

    .line 314
    const v5, 0x7f0e0090

    invoke-interface {p1, v5}, Landroid/view/Menu;->removeItem(I)V

    .line 316
    invoke-virtual {v0, v9}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 317
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 203
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->TAG:Ljava/lang/String;

    const-string v1, "onCreateView()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const v0, 0x7f04000b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mContainerView:Landroid/view/View;

    .line 206
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mContainerView:Landroid/view/View;

    sget v1, Lcom/samsung/android/app/memo/util/Utils;->NO_CATEGORY_POPUP:I

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Utils;->setNoMemosPopupText(Landroid/view/View;I)V

    .line 207
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->InitView()V

    .line 209
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mContainerView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mIsAlive:Z

    .line 392
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->dismissDeleteDialog()V

    .line 393
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v0}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->showCategoryList()V

    .line 394
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroy()V

    .line 396
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->clearObjects()V

    .line 397
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 413
    sput-boolean v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mIsAlive:Z

    .line 414
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 415
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 416
    invoke-super {p0}, Landroid/app/ListFragment;->onDestroyView()V

    .line 418
    return-void
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 498
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    move v0, p3

    .line 499
    .local v0, "pos":I
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 500
    const/4 v1, 0x0

    .line 534
    :goto_0
    return v1

    .line 503
    :cond_0
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->getCategoryName()Ljava/lang/String;

    move-result-object v2

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    .line 504
    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->getCategoryUUID()Ljava/lang/String;

    move-result-object v1

    .line 502
    invoke-static {v2, v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DialogFragment;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    iput-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    .line 505
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    new-instance v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$4;

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$4;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;I)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->setOnDialogResultListener(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;)V

    .line 532
    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 534
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 193
    const/16 v0, 0x70

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 194
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->delete(I)V

    .line 196
    :cond_0
    const/4 v0, 0x1

    .line 198
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 7
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "arg0":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 557
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    if-eqz p2, :cond_2

    .line 558
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 559
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 560
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 566
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->getCount()I

    move-result v2

    sget-object v3, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 567
    new-instance v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 568
    const v4, 0x7f04000c

    sget-object v5, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 567
    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    .line 569
    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 571
    :cond_1
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_4

    :goto_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->showNoCategory(Z)V

    .line 573
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->notifyDataSetChanged()V

    .line 575
    :cond_2
    return-void

    .line 561
    :cond_3
    sget-object v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    new-instance v3, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 562
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 561
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    move v0, v1

    .line 571
    goto :goto_1
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Landroid/content/Loader;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 579
    .local p1, "arg0":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 580
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 582
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 321
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 352
    :goto_0
    return v3

    .line 323
    :sswitch_0
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 324
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v0, v3}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->setDrawerLock(Z)V

    .line 329
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_0

    .line 334
    :sswitch_1
    invoke-static {v1, v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DialogFragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    .line 336
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    new-instance v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$3;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->setOnDialogResultListener(Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;)V

    .line 347
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 321
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0e008f -> :sswitch_1
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 371
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mIsAlive:Z

    .line 372
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 373
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 374
    const/16 v1, 0x10

    .line 373
    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 377
    :cond_0
    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    .line 378
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 357
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 358
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->getCount()I

    move-result v0

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 359
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 360
    const v2, 0x7f04000c

    sget-object v3, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 359
    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    .line 361
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListView:Lcom/samsung/android/app/memo/uiwidget/DndListView;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/uiwidget/DndListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->notifyDataSetChanged()V

    .line 364
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 365
    const/16 v1, 0x30

    .line 364
    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 367
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 152
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 153
    return-void
.end method

.method setDoneMenu(Z)V
    .locals 3
    .param p1, "set"    # Z

    .prologue
    .line 282
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mMenuActionBar:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 283
    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mMenuActionBar:Landroid/view/Menu;

    const v2, 0x7f0e0091

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 284
    .local v0, "item":Landroid/view/MenuItem;
    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 286
    .end local v0    # "item":Landroid/view/MenuItem;
    :cond_0
    return-void
.end method

.method public showNoCategory(Z)V
    .locals 4
    .param p1, "visible"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 538
    sget-object v3, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mNoCategoryView:Landroid/widget/TextView;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 539
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->IsNoMemosPopupModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    sget-object v3, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mNoCategoryPopupView:Landroid/widget/TextView;

    if-eqz p1, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 542
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mlightNoItemCategory:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 543
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mlightNoItemCategory:Landroid/widget/ImageView;

    if-eqz p1, :cond_4

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 545
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 538
    goto :goto_0

    :cond_3
    move v0, v2

    .line 540
    goto :goto_1

    :cond_4
    move v1, v2

    .line 543
    goto :goto_2
.end method
