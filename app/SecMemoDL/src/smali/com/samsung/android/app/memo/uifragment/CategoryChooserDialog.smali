.class public Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;
.super Ljava/lang/Object;
.source "CategoryChooserDialog.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final CID_DISPLAY_NAME:I = 0x2

.field private static final CID_UUID:I = 0x1

.field public static final KEY_SELECTEDUUID:Ljava/lang/String;

.field private static final ORDERBY:Ljava/lang/String; = "orderBy ASC"

.field private static final PROJECTION:[Ljava/lang/String;

.field private static final SELECTION:Ljava/lang/String; = "isDeleted IS 0"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final DELAY_TIME_SCROLL:I

.field private final SCROLL_MAX_POSITION:I

.field headerRadioButton:Landroid/widget/RadioButton;

.field private mActivity:Landroid/app/Activity;

.field public mAdapter:Landroid/widget/CursorAdapter;

.field private mCategoryBtn:Landroid/widget/ImageButton;

.field public mCategoryNamingDialog:Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog;

.field private mDialog:Landroid/app/Dialog;

.field private mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

.field private mPopuplist:Landroid/widget/ListView;

.field private mSelectedPosition:I

.field private pw:Landroid/widget/PopupWindow;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 43
    const-class v0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->TAG:Ljava/lang/String;

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_selectedUUID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->KEY_SELECTEDUUID:Ljava/lang/String;

    .line 46
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 47
    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 48
    const-string v2, "UUID"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 49
    const-string v2, "_display_name"

    aput-object v2, v0, v1

    .line 46
    sput-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->PROJECTION:[Ljava/lang/String;

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->SCROLL_MAX_POSITION:I

    .line 55
    const/16 v0, 0x32

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->DELAY_TIME_SCROLL:I

    .line 60
    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->headerRadioButton:Landroid/widget/RadioButton;

    .line 71
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mSelectedPosition:I

    move-object v0, p1

    .line 77
    check-cast v0, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    .line 78
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;

    .line 79
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0e0031

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mCategoryBtn:Landroid/widget/ImageButton;

    .line 80
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->onCreateDialog()Landroid/app/Dialog;

    .line 81
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 82
    sget-object v0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->TAG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mCategoryBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mPopuplist:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;)I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mSelectedPosition:I

    return v0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 164
    :cond_0
    return-void
.end method

.method protected getCursorAdapter()Landroid/widget/CursorAdapter;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 169
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$3;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;

    const v3, 0x7f040002

    const/4 v4, 0x0

    .line 170
    new-array v5, v8, [Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x2

    aget-object v1, v1, v6

    aput-object v1, v5, v7

    .line 171
    new-array v6, v8, [I

    const v1, 0x7f0e0001

    aput v1, v6, v7

    move-object v1, p0

    .line 169
    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$3;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    return-object v0
.end method

.method public getDialog()Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method public onCreateDialog()Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v8, -0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 87
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f040003

    invoke-virtual {v3, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 88
    .local v2, "popupView":Landroid/view/View;
    const v3, 0x7f0e0003

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mPopuplist:Landroid/widget/ListView;

    .line 90
    new-instance v3, Landroid/widget/PopupWindow;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;

    invoke-direct {v3, v4}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;

    .line 91
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 92
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v6}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 93
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v6}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 94
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v2}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 95
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v8}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 96
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v8}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 97
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mCategoryBtn:Landroid/widget/ImageButton;

    if-eqz v3, :cond_0

    .line 98
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mCategoryBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v4, v5, v5}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 99
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;

    new-instance v4, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$1;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;)V

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 111
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f040004

    invoke-virtual {v3, v4, v7, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 112
    .local v1, "headerNone":Landroid/view/View;
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f040001

    invoke-virtual {v3, v4, v7, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 114
    .local v0, "addCategory":Landroid/view/View;
    const-string v3, ""

    invoke-virtual {v1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 116
    const v3, 0x7f0e0002

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->headerRadioButton:Landroid/widget/RadioButton;

    .line 117
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v3}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/Session;->getCategoryUUID()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 118
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->headerRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3, v6}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 120
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mPopuplist:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 121
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mPopuplist:Landroid/widget/ListView;

    invoke-virtual {v3, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 123
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->getCursorAdapter()Landroid/widget/CursorAdapter;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mAdapter:Landroid/widget/CursorAdapter;

    .line 124
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mPopuplist:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 126
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mPopuplist:Landroid/widget/ListView;

    new-instance v4, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$2;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;)V

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 153
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mDialog:Landroid/app/Dialog;

    return-object v3
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    new-instance v0, Landroid/content/CursorLoader;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;

    sget-object v2, Lcom/samsung/android/provider/memo/MemoContract;->BASE_URI_CATEGORY:Landroid/net/Uri;

    sget-object v3, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->PROJECTION:[Ljava/lang/String;

    const-string v4, "isDeleted IS 0"

    const/4 v5, 0x0

    const-string v6, "orderBy ASC"

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 11
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, -0x1

    .line 205
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mAdapter:Landroid/widget/CursorAdapter;

    if-eqz v5, :cond_2

    if-eqz p2, :cond_2

    .line 206
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v6}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v6

    if-eq v5, v6, :cond_0

    .line 207
    iput v8, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mSelectedPosition:I

    .line 209
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v5, p2}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 212
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0002

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 213
    .local v4, "maxListLine":I
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090013

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v3, v5

    .line 216
    .local v3, "itemHeight":I
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090017

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v0, v5

    .line 217
    .local v0, "bubbleTopMargin":I
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v1, v5, Landroid/util/DisplayMetrics;->density:F

    .line 220
    .local v1, "density":F
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v5}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    if-le v5, v4, :cond_3

    .line 221
    mul-int v5, v3, v4

    add-int/2addr v5, v0

    int-to-float v6, v4

    mul-float/2addr v6, v1

    float-to-int v6, v6

    add-int v2, v5, v6

    .line 225
    .local v2, "height":I
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->pw:Landroid/widget/PopupWindow;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mCategoryBtn:Landroid/widget/ImageButton;

    const/4 v7, -0x2

    invoke-virtual {v5, v6, v7, v2}, Landroid/widget/PopupWindow;->update(Landroid/view/View;II)V

    .line 226
    iget v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mSelectedPosition:I

    if-ne v5, v8, :cond_2

    .line 227
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v5}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/Session;->getCategoryUUID()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 228
    iput v9, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mSelectedPosition:I

    .line 246
    :cond_1
    :goto_1
    iget v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mSelectedPosition:I

    const/16 v6, 0xa

    if-ge v5, v6, :cond_8

    .line 247
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mPopuplist:Landroid/widget/ListView;

    iget v6, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mSelectedPosition:I

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setSelection(I)V

    .line 258
    .end local v0    # "bubbleTopMargin":I
    .end local v1    # "density":F
    .end local v2    # "height":I
    .end local v3    # "itemHeight":I
    .end local v4    # "maxListLine":I
    :cond_2
    :goto_2
    return-void

    .line 223
    .restart local v0    # "bubbleTopMargin":I
    .restart local v1    # "density":F
    .restart local v3    # "itemHeight":I
    .restart local v4    # "maxListLine":I
    :cond_3
    const/4 v2, -0x2

    .restart local v2    # "height":I
    goto :goto_0

    .line 230
    :cond_4
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_6

    .line 231
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 233
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v5}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/Session;->getCategoryUUID()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 234
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    iput v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mSelectedPosition:I

    .line 239
    :cond_6
    :goto_3
    iget v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mSelectedPosition:I

    if-ne v5, v8, :cond_1

    .line 240
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    invoke-interface {v5}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/memo/Session;->setCategoryUUID(Ljava/lang/String;)V

    .line 241
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->headerRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v5, v10}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 242
    iput v9, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mSelectedPosition:I

    goto :goto_1

    .line 237
    :cond_7
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_5

    goto :goto_3

    .line 249
    :cond_8
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mPopuplist:Landroid/widget/ListView;

    new-instance v6, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$4;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog$4;-><init>(Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;)V

    .line 254
    const-wide/16 v8, 0x32

    .line 249
    invoke-virtual {v5, v6, v8, v9}, Landroid/widget/ListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Landroid/content/Loader;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 265
    .local p1, "arg0":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mAdapter:Landroid/widget/CursorAdapter;

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryChooserDialog;->mAdapter:Landroid/widget/CursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 268
    :cond_0
    return-void
.end method
