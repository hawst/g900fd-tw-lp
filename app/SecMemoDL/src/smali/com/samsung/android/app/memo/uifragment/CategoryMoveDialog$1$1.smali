.class Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1$1;
.super Ljava/lang/Object;
.source "CategoryMoveDialog.java"

# interfaces
.implements Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1;

.field private final synthetic val$context:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1$1;->this$1:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1;

    iput-object p2, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1$1;->val$context:Landroid/app/Activity;

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPositiveBtnClick(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "categoryName"    # Ljava/lang/String;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1$1;->val$context:Landroid/app/Activity;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mCategoryMoveIDs:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->access$3()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/samsung/android/app/memo/MemoDataHandler;->moveCategory(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1$1;->this$1:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1;->access$0(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1;)Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    move-result-object v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->access$4(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 122
    sget v0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->mCategoryType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 123
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1$1;->this$1:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1;->access$0(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog$1;)Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;

    move-result-object v0

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;->access$5(Lcom/samsung/android/app/memo/uifragment/CategoryMoveDialog;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->finishActionMode()V

    .line 125
    :cond_0
    return-void
.end method
