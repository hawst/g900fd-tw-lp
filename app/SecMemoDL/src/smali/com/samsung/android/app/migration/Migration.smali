.class public Lcom/samsung/android/app/migration/Migration;
.super Ljava/lang/Object;
.source "Migration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/migration/Migration$MigrationTask;
    }
.end annotation


# static fields
.field private static final LOCAL_TMEMO2_DEFAULT_PATH:Ljava/lang/String;

.field public static final SNB_CLICK_DEFAULT_PATH:Ljava/lang/String;

.field public static final STATE_SYNC_MEMO_DIALOG_DISPLAYED:I = 0x2

.field public static final STATE_SYNC_MEMO_DIALOG_DISPLAYING:I = 0x1

.field public static final STATE_SYNC_MEMO_DIALOG_READY_TO_DISPLAY:I = 0x0

.field public static final STATE_TMEMO_IMPORT_CHECK_SERVER:I = 0x1

.field public static final STATE_TMEMO_IMPORT_DIALOG_DISPLAYING:I = 0x2

.field public static final STATE_TMEMO_IMPORT_DISPLAYED:I = 0x3

.field public static final STATE_TMEMO_IMPORT_READY_TO_DISPLAY:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Migration"

.field private static TMEMO1_DB_PATH:Ljava/lang/String;

.field public static checkDB:Landroid/database/sqlite/SQLiteDatabase;

.field private static instance:Lcom/samsung/android/app/migration/Migration;


# instance fields
.field private TMemoApp:Ljava/lang/String;

.field public activityContext:Lcom/samsung/android/app/memo/Main;

.field private backupMemoTask:Lcom/samsung/android/app/migration/task/BackupMemoTask;

.field private context:Landroid/content/Context;

.field private convertTask:Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;

.field private cursor:Landroid/database/Cursor;

.field private isInBackground:Z

.field private kiesSnbPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private localSnbPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private password:Ljava/lang/String;

.field public passwordLock:Ljava/lang/Object;

.field private restoreMemoTask:Lcom/samsung/android/app/migration/task/RestoreMemoTask;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 61
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/Download/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/migration/Migration;->SNB_CLICK_DEFAULT_PATH:Ljava/lang/String;

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 63
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "TMemo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/migration/Migration;->LOCAL_TMEMO2_DEFAULT_PATH:Ljava/lang/String;

    .line 76
    new-instance v0, Lcom/samsung/android/app/migration/Migration;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/Migration;-><init>()V

    sput-object v0, Lcom/samsung/android/app/migration/Migration;->instance:Lcom/samsung/android/app/migration/Migration;

    .line 91
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/migration/Migration;->checkDB:Landroid/database/sqlite/SQLiteDatabase;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/migration/Migration;->password:Ljava/lang/String;

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/migration/Migration;->isInBackground:Z

    .line 89
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/Migration;->passwordLock:Ljava/lang/Object;

    .line 94
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/migration/Migration;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/android/app/migration/Migration;->TMemoApp:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/app/migration/Migration;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->TMemoApp:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/app/migration/Migration;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/app/migration/Migration;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/android/app/migration/Migration;->context:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/android/app/migration/Migration;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->kiesSnbPaths:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$13(Lcom/samsung/android/app/migration/Migration;Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/android/app/migration/Migration;->convertTask:Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/app/migration/Migration;)Lcom/samsung/android/app/migration/task/BackupMemoTask;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->backupMemoTask:Lcom/samsung/android/app/migration/task/BackupMemoTask;

    return-object v0
.end method

.method static synthetic access$15(Lcom/samsung/android/app/migration/Migration;)Lcom/samsung/android/app/migration/task/RestoreMemoTask;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->restoreMemoTask:Lcom/samsung/android/app/migration/task/RestoreMemoTask;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/migration/Migration;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/samsung/android/app/migration/Migration;->getTMemo1Data()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/migration/Migration;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/samsung/android/app/migration/Migration;->cursor:Landroid/database/Cursor;

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/app/migration/Migration;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->cursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$5()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/samsung/android/app/migration/Migration;->LOCAL_TMEMO2_DEFAULT_PATH:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/app/migration/Migration;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/Migration;->getTMemo2Data(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/app/migration/Migration;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/samsung/android/app/migration/Migration;->localSnbPaths:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/app/migration/Migration;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->localSnbPaths:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/app/migration/Migration;)Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->convertTask:Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;

    return-object v0
.end method

.method public static closeTmemo1Database()V
    .locals 1

    .prologue
    .line 157
    sget-object v0, Lcom/samsung/android/app/migration/Migration;->checkDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 158
    sget-object v0, Lcom/samsung/android/app/migration/Migration;->checkDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 159
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/migration/Migration;->checkDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 161
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/samsung/android/app/migration/Migration;
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/samsung/android/app/migration/Migration;->instance:Lcom/samsung/android/app/migration/Migration;

    return-object v0
.end method

.method private getTMemo1Data()Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 127
    :try_start_0
    sget-object v0, Lcom/samsung/android/app/migration/Migration;->TMEMO1_DB_PATH:Ljava/lang/String;

    const/4 v1, 0x0

    .line 128
    const/4 v2, 0x1

    .line 127
    invoke-static {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/migration/Migration;->checkDB:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :goto_0
    sget-object v0, Lcom/samsung/android/app/migration/Migration;->checkDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    .line 133
    sget-object v0, Lcom/samsung/android/app/migration/Migration;->checkDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "memo_content"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 134
    const-string v5, "c1title"

    aput-object v5, v2, v4

    const-string v4, "c2content"

    aput-object v4, v2, v6

    const/4 v4, 0x2

    const-string v5, "c4modify_t"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "c5create_t"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    .line 133
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 139
    :goto_1
    return-object v3

    .line 129
    :catch_0
    move-exception v8

    .line 130
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    const-string v0, "Migration"

    const-string v1, "DB doesn\'t exist."

    invoke-static {v0, v1, v8}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 137
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    :cond_0
    const-string v0, "Migration"

    const-string v1, "DB can\'t be opened."

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getTMemo2Data(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "folderPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 144
    .local v4, "pathsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 145
    .local v2, "folder":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 146
    .local v1, "files":[Ljava/io/File;
    if-eqz v1, :cond_0

    .line 147
    array-length v6, v1

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v6, :cond_2

    .line 153
    :cond_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_1

    const/4 v4, 0x0

    .end local v4    # "pathsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    return-object v4

    .line 147
    .restart local v4    # "pathsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    aget-object v0, v1, v5

    .line 148
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 149
    .local v3, "path":Ljava/lang/String;
    const-string v7, ".snb"

    invoke-virtual {v3, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 150
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method


# virtual methods
.method public downloadSNB()V
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    if-eqz v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v0}, Lcom/samsung/android/app/migration/utils/Utils;->isNetworkReachable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 373
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 374
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/AccountHelper;->getUnsyncedAccount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 375
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isMemoSyncAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->isSAVerified(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 378
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->getTMemoImportDialogState(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->isMemoRelaunchedForTMemoImportDialog(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->isImportDoNotShowFlagSet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 381
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getInstance()Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->checkSNBOnServer(Landroid/app/Activity;)V

    .line 383
    :cond_1
    return-void
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->password:Ljava/lang/String;

    return-object v0
.end method

.method public isInBackground()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/samsung/android/app/migration/Migration;->isInBackground:Z

    return v0
.end method

.method public resetPassword()V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/migration/Migration;->password:Ljava/lang/String;

    .line 102
    return-void
.end method

.method public setIsInBackground(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/samsung/android/app/migration/Migration;->isInBackground:Z

    .line 114
    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 2
    .param p1, "pwd"    # Ljava/lang/String;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/samsung/android/app/migration/Migration;->password:Ljava/lang/String;

    .line 214
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->passwordLock:Ljava/lang/Object;

    monitor-enter v1

    .line 215
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->passwordLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 214
    monitor-exit v1

    .line 217
    return-void

    .line 214
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized showSyncMemoDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 340
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    if-eqz v1, :cond_0

    .line 341
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/Utils;->isNetworkReachable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 342
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v1}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 343
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 344
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->isSAVerified(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 345
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isMemoSyncAvailable()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 346
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->isMemoRelaunchedForSyncMemoDialog(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 347
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->getSyncMemoDialogState(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_1

    .line 348
    invoke-static {}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->newInstance()Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/migration/dialog/SyncMemoDialog;->show(Landroid/app/Activity;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 368
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 349
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->isMemoRelaunchedForTMemoImportDialog(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 350
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->getSyncMemoDialogState(Landroid/content/Context;)I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 351
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->getTMemoImportDialogState(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_0

    .line 352
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/Migration;->downloadSNB()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 340
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 355
    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemoImportDialogState(Landroid/content/Context;I)V

    .line 356
    invoke-virtual {p0}, Lcom/samsung/android/app/migration/Migration;->downloadSNB()V

    goto :goto_0

    .line 359
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->isSAVerificationStarted(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 360
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v1}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    .line 361
    .local v0, "account":Landroid/accounts/Account;
    if-eqz v0, :cond_0

    .line 362
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/samsung/android/app/migration/utils/Utils;->updateSharedPrefWhenLoginToSA(Landroid/content/Context;Ljava/lang/String;)V

    .line 363
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/samsung/android/app/migration/utils/Utils;->sendRequestToSAService(Landroid/content/Context;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public startKiesBackUpMemoMigration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "targetDir"    # Ljava/lang/String;
    .param p3, "sourceApp"    # Ljava/lang/String;
    .param p4, "sessionKey"    # Ljava/lang/String;

    .prologue
    .line 201
    const-string v0, "Migration"

    const-string v1, "Backing up Memo data for Kies..."

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    new-instance v0, Lcom/samsung/android/app/migration/task/BackupMemoTask;

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/migration/task/BackupMemoTask;-><init>(Landroid/app/Activity;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/app/migration/Migration;->backupMemoTask:Lcom/samsung/android/app/migration/task/BackupMemoTask;

    .line 203
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->backupMemoTask:Lcom/samsung/android/app/migration/task/BackupMemoTask;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/task/BackupMemoTask;->start()V

    .line 204
    return-void
.end method

.method public startKiesRestoreMemoMigration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "targetDir"    # Ljava/lang/String;
    .param p3, "sourceApp"    # Ljava/lang/String;
    .param p4, "sessionKey"    # Ljava/lang/String;

    .prologue
    .line 207
    const-string v0, "Migration"

    const-string v1, "Restoring Memo data from Kies..."

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    new-instance v0, Lcom/samsung/android/app/migration/task/RestoreMemoTask;

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    const/4 v6, 0x0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;-><init>(Landroid/app/Activity;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/samsung/android/app/migration/Migration;->restoreMemoTask:Lcom/samsung/android/app/migration/task/RestoreMemoTask;

    .line 209
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->restoreMemoTask:Lcom/samsung/android/app/migration/task/RestoreMemoTask;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/task/RestoreMemoTask;->start()V

    .line 210
    return-void
.end method

.method public startKiesRestoreTMemo2Migration(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "folderPath"    # Ljava/lang/String;
    .param p3, "sourceApp"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 183
    const-string v0, "Migration"

    const-string v1, "Migrating TMemo2 data from Kies folder..."

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    sget-object v0, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_KIES_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_KIES_FOLDER_PATH:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/FileHelper;->copyFileItem(Ljava/io/File;Ljava/io/File;)I

    move-result v6

    .line 186
    .local v6, "res":I
    if-nez v6, :cond_1

    .line 187
    invoke-static {p1, p3, v3, v3}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->sendRestoreResponse(Landroid/content/Context;Ljava/lang/String;II)V

    .line 193
    .end local v6    # "res":I
    :cond_0
    :goto_0
    sget-object v0, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_KIES_FOLDER_PATH:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/Migration;->getTMemo2Data(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/migration/Migration;->kiesSnbPaths:Ljava/util/ArrayList;

    .line 194
    new-instance v0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    .line 195
    const v2, 0x7f0b0052

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/migration/Migration;->kiesSnbPaths:Ljava/util/ArrayList;

    move-object v2, p1

    .line 196
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;-><init>(Landroid/app/Activity;Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;Z)V

    .line 194
    iput-object v0, p0, Lcom/samsung/android/app/migration/Migration;->convertTask:Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;

    .line 197
    iget-object v0, p0, Lcom/samsung/android/app/migration/Migration;->convertTask:Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;

    invoke-virtual {v0}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->start()V

    .line 198
    return-void

    .line 188
    .restart local v6    # "res":I
    :cond_1
    const/4 v0, -0x1

    if-ne v6, v0, :cond_2

    .line 189
    invoke-static {p1, p3, v5, v5}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->sendRestoreResponse(Landroid/content/Context;Ljava/lang/String;II)V

    goto :goto_0

    .line 190
    :cond_2
    const/4 v0, -0x4

    if-ne v6, v0, :cond_0

    .line 191
    const/4 v0, 0x2

    invoke-static {p1, p3, v5, v0}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->sendRestoreResponse(Landroid/content/Context;Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public startLocalMemosMigration()V
    .locals 6

    .prologue
    .line 164
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->isLocalMemosMigrationCompleted(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 165
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->TMemoApp:Ljava/lang/String;

    const-string v2, "TMemo1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 166
    const-string v1, "Migration"

    const-string v2, "Migrating TMemo1..."

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    new-instance v0, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;

    .line 168
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    iget-object v2, p0, Lcom/samsung/android/app/migration/Migration;->cursor:Landroid/database/Cursor;

    .line 167
    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;-><init>(Landroid/app/Activity;Landroid/database/Cursor;)V

    .line 169
    .local v0, "convertTask":Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;
    invoke-virtual {v0}, Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;->start()V

    .line 178
    .end local v0    # "convertTask":Lcom/samsung/android/app/migration/task/ConvertLocalTMemo1Task;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->setLocalMemosMigrationCompletedFlag(Landroid/content/Context;)V

    .line 180
    :cond_1
    return-void

    .line 170
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->TMemoApp:Ljava/lang/String;

    const-string v2, "TMemo2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    const-string v1, "Migration"

    const-string v2, "Migrating TMemo2..."

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    new-instance v0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;

    iget-object v1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    .line 173
    iget-object v2, p0, Lcom/samsung/android/app/migration/Migration;->context:Landroid/content/Context;

    .line 174
    iget-object v3, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    const v4, 0x7f0b0051

    invoke-virtual {v3, v4}, Lcom/samsung/android/app/memo/Main;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 175
    iget-object v4, p0, Lcom/samsung/android/app/migration/Migration;->localSnbPaths:Ljava/util/ArrayList;

    const/4 v5, 0x0

    .line 172
    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;-><init>(Landroid/app/Activity;Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;Z)V

    .line 176
    .local v0, "convertTask":Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;
    invoke-virtual {v0}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->start()V

    goto :goto_0
.end method

.method public startMigration(Lcom/samsung/android/app/memo/Main;)V
    .locals 5
    .param p1, "activity"    # Lcom/samsung/android/app/memo/Main;

    .prologue
    const/4 v4, 0x0

    .line 117
    iput-object p1, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    .line 118
    new-instance v1, Ljava/lang/StringBuilder;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/samsung/android/app/migration/Migration;->activityContext:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/Main;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 119
    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "com.sec.android.app.memo"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/databases/Memo.db"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 118
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/app/migration/Migration;->TMEMO1_DB_PATH:Ljava/lang/String;

    .line 120
    invoke-virtual {p0, v4}, Lcom/samsung/android/app/migration/Migration;->setIsInBackground(Z)V

    .line 121
    new-instance v0, Lcom/samsung/android/app/migration/Migration$MigrationTask;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/migration/Migration$MigrationTask;-><init>(Lcom/samsung/android/app/migration/Migration;)V

    .line 122
    .local v0, "migrationTask":Lcom/samsung/android/app/migration/Migration$MigrationTask;
    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/migration/Migration$MigrationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 123
    return-void
.end method
