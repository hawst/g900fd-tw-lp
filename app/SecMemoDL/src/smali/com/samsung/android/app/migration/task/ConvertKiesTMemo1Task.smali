.class public Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;
.super Ljava/lang/Thread;
.source "ConvertKiesTMemo1Task.java"


# static fields
.field private static instance:Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;


# instance fields
.field private context:Landroid/app/Activity;

.field private ctx:Landroid/content/Context;

.field private isShown:Z

.field private msg:Ljava/lang/String;

.field private progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->isShown:Z

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    .line 50
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;)Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->context:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;Z)V
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->isShown:Z

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    return-void
.end method

.method private formatMemo(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 101
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    .local v1, "s":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-static {v1, p1, v2, v3}, Lcom/samsung/android/app/memo/util/HtmlUtil;->withinStyle(Ljava/lang/StringBuilder;Ljava/lang/String;II)V

    .line 103
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/migration/utils/Utils;->formatContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "formattedContent":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/app/migration/utils/Utils;->handleNewLines(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 105
    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;
    .locals 2

    .prologue
    .line 53
    const-class v1, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->instance:Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;-><init>()V

    sput-object v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->instance:Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;

    .line 55
    :cond_0
    sget-object v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->instance:Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isContextAlive(Landroid/app/Activity;)Z
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 81
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    const/4 v0, 0x1

    .line 84
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendRestoreResponse(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "source"    # Ljava/lang/String;
    .param p3, "res"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 72
    if-nez p3, :cond_1

    .line 73
    invoke-static {p1, p2, v0, v0}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->sendTMemo1RestoreResponse(Landroid/content/Context;Ljava/lang/String;II)V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    const/4 v0, -0x1

    if-ne p3, v0, :cond_2

    .line 75
    invoke-static {p1, p2, v1, v1}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->sendTMemo1RestoreResponse(Landroid/content/Context;Ljava/lang/String;II)V

    goto :goto_0

    .line 76
    :cond_2
    const/4 v0, -0x4

    if-ne p3, v0, :cond_0

    .line 77
    const/4 v0, 0x2

    invoke-static {p1, p2, v1, v0}, Lcom/samsung/android/app/migration/receiver/KiesReceiver;->sendTMemo1RestoreResponse(Landroid/content/Context;Ljava/lang/String;II)V

    goto :goto_0
.end method

.method private showDialog()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->context:Landroid/app/Activity;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->isContextAlive(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->context:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->context:Landroid/app/Activity;

    new-instance v1, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task$1;-><init>(Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 98
    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 28

    .prologue
    .line 110
    const/16 v21, 0x0

    .line 111
    .local v21, "tMemo1Data":Ljava/util/concurrent/BlockingQueue;, "Ljava/util/concurrent/BlockingQueue<Lcom/samsung/android/app/migration/task/TMemo1Data;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 112
    .local v18, "startTime":J
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    sub-long v24, v24, v18

    const-wide/16 v26, 0x1388

    cmp-long v3, v24, v26

    if-ltz v3, :cond_1

    .line 167
    const/4 v3, 0x0

    sput-object v3, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->instance:Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;

    .line 168
    return-void

    .line 113
    :cond_1
    invoke-static {}, Lcom/samsung/android/app/migration/MemoConverter;->getInstance()Lcom/samsung/android/app/migration/MemoConverter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/migration/MemoConverter;->getTMemo1DataQueue()Ljava/util/concurrent/BlockingQueue;

    move-result-object v21

    .line 114
    if-eqz v21, :cond_4

    invoke-interface/range {v21 .. v21}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 115
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 116
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    if-nez v3, :cond_5

    .line 117
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->msg:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-static {v3, v4, v6}, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    .line 118
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->showDialog()V

    .line 122
    :cond_2
    :goto_1
    const-wide/16 v16, -0x1

    .line 123
    .local v16, "res":J
    invoke-static {}, Lcom/samsung/android/app/migration/MemoConverter;->getInstance()Lcom/samsung/android/app/migration/MemoConverter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/migration/MemoConverter;->getLock()Ljava/lang/Object;

    move-result-object v23

    monitor-enter v23

    .line 124
    :try_start_0
    invoke-interface/range {v21 .. v21}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/android/app/migration/task/TMemo1Data;

    .line 125
    .local v12, "data":Lcom/samsung/android/app/migration/task/TMemo1Data;
    invoke-virtual {v12}, Lcom/samsung/android/app/migration/task/TMemo1Data;->getTitle()Ljava/lang/String;

    move-result-object v5

    .line 126
    .local v5, "title":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/app/migration/task/TMemo1Data;->getContent()Ljava/lang/String;

    move-result-object v2

    .line 127
    .local v2, "content":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/app/migration/task/TMemo1Data;->getCategory()Ljava/lang/String;

    move-result-object v7

    .line 128
    .local v7, "category":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/app/migration/task/TMemo1Data;->getSource()Ljava/lang/String;

    move-result-object v15

    .line 129
    .local v15, "source":Ljava/lang/String;
    invoke-virtual {v12}, Lcom/samsung/android/app/migration/task/TMemo1Data;->getCreateTime()J

    move-result-wide v8

    .line 130
    .local v8, "createdTime":J
    invoke-virtual {v12}, Lcom/samsung/android/app/migration/task/TMemo1Data;->getModifyTime()J

    move-result-wide v10

    .line 131
    .local v10, "modifiedTime":J
    if-eqz v2, :cond_3

    .line 132
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v22

    .line 133
    .local v22, "textLength":I
    sget v3, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mMaxCharSize:I

    move/from16 v0, v22

    if-le v0, v3, :cond_a

    .line 134
    sget v3, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mMaxCharSize:I

    div-int v4, v22, v3

    sget v3, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mMaxCharSize:I

    rem-int v3, v22, v3

    if-lez v3, :cond_6

    const/4 v3, 0x1

    :goto_2
    add-int v14, v4, v3

    .line 135
    .local v14, "memoNumber":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_3
    if-lt v13, v14, :cond_7

    .line 146
    .end local v13    # "i":I
    .end local v14    # "memoNumber":I
    .end local v22    # "textLength":I
    :cond_3
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->ctx:Landroid/content/Context;

    const-wide/16 v24, -0x1

    cmp-long v3, v16, v24

    if-nez v3, :cond_c

    const/4 v3, -0x1

    :goto_5
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v15, v3}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->sendRestoreResponse(Landroid/content/Context;Ljava/lang/String;I)V

    .line 123
    monitor-exit v23
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    .end local v2    # "content":Ljava/lang/String;
    .end local v5    # "title":Ljava/lang/String;
    .end local v7    # "category":Ljava/lang/String;
    .end local v8    # "createdTime":J
    .end local v10    # "modifiedTime":J
    .end local v12    # "data":Lcom/samsung/android/app/migration/task/TMemo1Data;
    .end local v15    # "source":Ljava/lang/String;
    .end local v16    # "res":J
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    if-eqz v3, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    sub-long v24, v24, v18

    const-wide/16 v26, 0x3e8

    cmp-long v3, v24, v26

    if-lez v3, :cond_0

    .line 150
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->context:Landroid/app/Activity;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->isContextAlive(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 151
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->context:Landroid/app/Activity;

    new-instance v4, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task$2;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task$2;-><init>(Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 119
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->isShown:Z

    if-nez v3, :cond_2

    .line 120
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->showDialog()V

    goto/16 :goto_1

    .line 134
    .restart local v2    # "content":Ljava/lang/String;
    .restart local v5    # "title":Ljava/lang/String;
    .restart local v7    # "category":Ljava/lang/String;
    .restart local v8    # "createdTime":J
    .restart local v10    # "modifiedTime":J
    .restart local v12    # "data":Lcom/samsung/android/app/migration/task/TMemo1Data;
    .restart local v15    # "source":Ljava/lang/String;
    .restart local v16    # "res":J
    .restart local v22    # "textLength":I
    :cond_6
    const/4 v3, 0x0

    goto :goto_2

    .line 136
    .restart local v13    # "i":I
    .restart local v14    # "memoNumber":I
    :cond_7
    :try_start_1
    sget v3, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mMaxCharSize:I

    mul-int v4, v13, v3

    if-nez v13, :cond_8

    const/4 v3, 0x0

    :goto_6
    add-int/2addr v3, v4

    .line 137
    add-int/lit8 v4, v13, 0x1

    sget v6, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mMaxCharSize:I

    mul-int/2addr v4, v6

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, v22

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 136
    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    .line 138
    .local v20, "subContent":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/migration/MemoConverter;->getInstance()Lcom/samsung/android/app/migration/MemoConverter;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->context:Landroid/app/Activity;

    if-nez v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->ctx:Landroid/content/Context;

    :goto_7
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->formatMemo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v3 .. v11}, Lcom/samsung/android/app/migration/MemoConverter;->convertToNewMemo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)J

    move-result-wide v16

    .line 135
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_3

    .line 136
    .end local v20    # "subContent":Ljava/lang/String;
    :cond_8
    const/4 v3, -0x1

    goto :goto_6

    .line 138
    .restart local v20    # "subContent":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->context:Landroid/app/Activity;

    goto :goto_7

    .line 142
    .end local v13    # "i":I
    .end local v14    # "memoNumber":I
    .end local v20    # "subContent":Ljava/lang/String;
    :cond_a
    invoke-static {}, Lcom/samsung/android/app/migration/MemoConverter;->getInstance()Lcom/samsung/android/app/migration/MemoConverter;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->context:Landroid/app/Activity;

    if-nez v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->ctx:Landroid/content/Context;

    :goto_8
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->formatMemo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v3 .. v11}, Lcom/samsung/android/app/migration/MemoConverter;->convertToNewMemo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)J

    move-result-wide v16

    goto/16 :goto_4

    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->context:Landroid/app/Activity;

    goto :goto_8

    .line 146
    .end local v22    # "textLength":I
    :cond_c
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 123
    .end local v2    # "content":Ljava/lang/String;
    .end local v5    # "title":Ljava/lang/String;
    .end local v7    # "category":Ljava/lang/String;
    .end local v8    # "createdTime":J
    .end local v10    # "modifiedTime":J
    .end local v12    # "data":Lcom/samsung/android/app/migration/task/TMemo1Data;
    .end local v15    # "source":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit v23
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 162
    .end local v16    # "res":J
    :cond_d
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    .line 163
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->isShown:Z

    goto/16 :goto_0
.end method

.method public setContext(Landroid/app/Activity;)V
    .locals 0
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->context:Landroid/app/Activity;

    .line 64
    invoke-direct {p0}, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->showDialog()V

    .line 65
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->ctx:Landroid/content/Context;

    .line 69
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/ConvertKiesTMemo1Task;->msg:Ljava/lang/String;

    .line 60
    return-void
.end method
