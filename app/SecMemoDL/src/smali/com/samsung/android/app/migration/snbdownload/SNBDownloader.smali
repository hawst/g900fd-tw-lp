.class public Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;
.super Ljava/lang/Object;
.source "SNBDownloader.java"

# interfaces
.implements Lcom/samsung/android/app/migration/snbdownload/DownloadResponseListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;
    }
.end annotation


# static fields
.field private static final FILEDETAIL:Ljava/lang/String; = "FILEDETAIL"

.field private static final FILEDETAIL_COLUMNS:[Ljava/lang/String;

.field private static final FILES:Ljava/lang/String; = "FILES"

.field private static final FILE_COLUMNS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "SNBDownloader"

.field private static mContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private static mInstance:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;


# instance fields
.field private mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

.field private final mFileDownloadMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;",
            ">;"
        }
    .end annotation
.end field

.field private mIsTMemo2DataExists:Z

.field private mKVSServiceManager:Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;

.field private mNotifBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mORSServiceManager:Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;

.field protected mServerChangedRecords:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 96
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 97
    const-string v1, "name"

    aput-object v1, v0, v2

    .line 98
    const-string v1, "syncname"

    aput-object v1, v0, v3

    .line 96
    sput-object v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->FILE_COLUMNS:[Ljava/lang/String;

    .line 101
    new-array v0, v3, [Ljava/lang/String;

    .line 102
    const-string v1, "path"

    aput-object v1, v0, v2

    .line 101
    sput-object v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->FILEDETAIL_COLUMNS:[Ljava/lang/String;

    .line 103
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mIsTMemo2DataExists:Z

    .line 117
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mFileDownloadMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 118
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;)V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->downsync()V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;)V
    .locals 0

    .prologue
    .line 329
    invoke-direct {p0}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->showNotification()V

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
        }
    .end annotation

    .prologue
    .line 274
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getItemsFromKeys(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
        }
    .end annotation

    .prologue
    .line 478
    invoke-direct {p0}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->downloadSNoteFile()V

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;Z)V
    .locals 0

    .prologue
    .line 376
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->hideNotification(Z)V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;Z)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->handleResult(Z)V

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;Landroid/support/v4/app/NotificationCompat$Builder;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotifBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;)Landroid/support/v4/app/NotificationCompat$Builder;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotifBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;Landroid/app/NotificationManager;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotificationManager:Landroid/app/NotificationManager;

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;)Landroid/app/NotificationManager;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotificationManager:Landroid/app/NotificationManager;

    return-object v0
.end method

.method private addToDownloadKeyList(Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;Ljava/util/Map;)Z
    .locals 13
    .param p1, "response"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 558
    .local p2, "serverChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;>;"
    invoke-virtual {p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->getKeyDetailList()Ljava/util/List;

    move-result-object v11

    .line 559
    .local v11, "updates":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;>;"
    if-eqz v11, :cond_0

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 560
    :cond_0
    const/4 v2, 0x0

    .line 569
    :goto_0
    return v2

    .line 562
    :cond_1
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_2
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 569
    const/4 v2, 0x1

    goto :goto_0

    .line 562
    :cond_3
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;

    .line 563
    .local v10, "record":Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;
    new-instance v1, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;

    const-wide/16 v2, -0x1

    invoke-virtual {v10}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;->isDeleted()Z

    move-result v4

    invoke-virtual {v10}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;->getTimeStamp()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const/4 v7, 0x0

    .line 564
    invoke-virtual {v10}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;->getSize()J

    move-result-wide v8

    .line 563
    invoke-direct/range {v1 .. v9}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;-><init>(JZJLjava/lang/String;J)V

    .line 565
    .local v1, "item":Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;
    invoke-virtual {v10}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;->getKEY()Ljava/lang/String;

    move-result-object v0

    .line 566
    .local v0, "key":Ljava/lang/String;
    const-string v2, "_TMEMO2"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v10}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;->isDeleted()Z

    move-result v2

    if-nez v2, :cond_2

    .line 567
    invoke-virtual {v10}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordKey;->getKEY()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private downloadFile(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;Ljava/lang/String;)V
    .locals 19
    .param p1, "snbData"    # Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 500
    :try_start_0
    const-string v14, ""

    .line 501
    .local v14, "localPath":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/samsung/android/app/migration/utils/Utils;->TMP_FOLDER_PATH:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "snote/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 502
    .local v15, "localPathFolder":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;->noteFilePath:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v3, v1}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getServerPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 503
    .local v16, "serverPath":Ljava/lang/String;
    if-eqz v16, :cond_0

    .line 504
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 505
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mORSServiceManager:Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v3, v0, v14, v4}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->downloadFile(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;

    .line 507
    :cond_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 508
    const-string v3, "SNBDownloader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "downloadFile(), file does not exist: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    .end local v14    # "localPath":Ljava/lang/String;
    .end local v15    # "localPathFolder":Ljava/lang/String;
    .end local v16    # "serverPath":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 511
    .restart local v14    # "localPath":Ljava/lang/String;
    .restart local v15    # "localPathFolder":Ljava/lang/String;
    .restart local v16    # "serverPath":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;->settingsFilePath:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v3, v1}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getServerPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 512
    if-eqz v16, :cond_3

    .line 513
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 514
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mORSServiceManager:Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;

    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v3, v0, v14, v4}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;->downloadFile(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/ORSResponse;

    .line 516
    :cond_3
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_4

    .line 517
    const-string v3, "SNBDownloader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "downloadFile(), file does not exist: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 536
    .end local v14    # "localPath":Ljava/lang/String;
    .end local v15    # "localPathFolder":Ljava/lang/String;
    .end local v16    # "serverPath":Ljava/lang/String;
    :catch_0
    move-exception v12

    .line 537
    .local v12, "e":Ljava/lang/Exception;
    const-string v3, "SNBDownloader"

    const-string v4, "Exception occurred in downloadFile()."

    invoke-static {v3, v4, v12}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 520
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v14    # "localPath":Ljava/lang/String;
    .restart local v15    # "localPathFolder":Ljava/lang/String;
    .restart local v16    # "serverPath":Ljava/lang/String;
    :cond_4
    :try_start_1
    const-string v3, "/snote/settings.xml"

    invoke-virtual {v14, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v17

    .line 521
    .local v17, "startIndex":I
    const/4 v3, 0x0

    move/from16 v0, v17

    invoke-virtual {v14, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 522
    .local v13, "folderPath":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v3

    invoke-virtual {v3, v13}, Lcom/samsung/android/app/migration/xml/XMLParser;->parseXMLContent(Ljava/lang/String;)V

    .line 523
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v3

    invoke-virtual {v3, v13}, Lcom/samsung/android/app/migration/xml/XMLParser;->parseXMLSettings(Ljava/lang/String;)V

    .line 524
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/migration/xml/XMLParser;->getContent()Ljava/lang/String;

    move-result-object v2

    .line 525
    .local v2, "content":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/migration/xml/XMLParser;->getTemplateType()Ljava/lang/String;

    move-result-object v18

    .line 526
    .local v18, "templateType":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/migration/xml/XMLParser;->getCreatedTime()J

    move-result-wide v8

    .line 527
    .local v8, "createdTime":J
    invoke-static {}, Lcom/samsung/android/app/migration/xml/XMLParser;->getInstance()Lcom/samsung/android/app/migration/xml/XMLParser;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/migration/xml/XMLParser;->getModifiedTime()J

    move-result-wide v10

    .line 528
    .local v10, "modifiedTime":J
    const-string v3, "SNBDownloader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "title = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;->snbName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", templateType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", content = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 529
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", createdTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", modifiedTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 528
    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/samsung/android/app/memo/util/FileHelper;->deleteFileItem(Ljava/io/File;)V

    .line 531
    const-string v3, "14"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 532
    invoke-static {v2}, Lcom/samsung/android/app/migration/utils/Utils;->formatContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 533
    .local v6, "formattedContent":Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/app/migration/utils/Utils;->handleNewLines(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 534
    invoke-static {}, Lcom/samsung/android/app/migration/MemoConverter;->getInstance()Lcom/samsung/android/app/migration/MemoConverter;

    move-result-object v3

    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;->snbName:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual/range {v3 .. v11}, Lcom/samsung/android/app/migration/MemoConverter;->convertToNewMemo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private downloadSNoteFile()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 479
    const/4 v0, 0x1

    .line 480
    .local v0, "count":I
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mFileDownloadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v3

    .line 481
    .local v3, "size":I
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mFileDownloadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 482
    .local v2, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 496
    return-void

    .line 482
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 483
    .local v1, "key":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/app/migration/utils/Utils;->isNetworkReachable(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 484
    new-instance v5, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    invoke-direct {v5, v10}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v5

    .line 485
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mFileDownloadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v6, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;

    .line 486
    .local v4, "snbData":Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mORSServiceManager:Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;

    if-eqz v6, :cond_3

    if-eqz v4, :cond_3

    .line 487
    invoke-direct {p0, v4, v1}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->downloadFile(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;Ljava/lang/String;)V

    .line 488
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotifBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotificationManager:Landroid/app/NotificationManager;

    if-eqz v6, :cond_0

    .line 489
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotifBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    const/16 v7, 0x64

    mul-int/lit8 v8, v0, 0x64

    div-int/2addr v8, v3

    invoke-virtual {v6, v7, v8, v10}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 490
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotifBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f0b0051

    invoke-virtual {v8, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 491
    const-string v8, " ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    mul-int/lit8 v8, v0, 0x64

    div-int/2addr v8, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%)"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 490
    invoke-virtual {v6, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 492
    iget-object v6, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v7, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotifBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v7}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v6, v10, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 493
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private downsync()V
    .locals 8

    .prologue
    .line 181
    :try_start_0
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 182
    .local v4, "serverChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 183
    .local v3, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, v4}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getAllkeys(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mServerChangedRecords:Ljava/util/Map;

    .line 184
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mServerChangedRecords:Ljava/util/Map;

    if-eqz v5, :cond_0

    .line 185
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mServerChangedRecords:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 188
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mServerChangedRecords:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 189
    invoke-static {v3}, Lcom/samsung/android/app/migration/dialog/ImportDialog;->newInstance(Ljava/util/List;)Lcom/samsung/android/app/migration/dialog/ImportDialog;

    move-result-object v5

    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/app/migration/dialog/ImportDialog;->show(Landroid/app/Activity;)V

    .line 190
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mIsTMemo2DataExists:Z

    .line 205
    .end local v3    # "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "serverChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;>;"
    :cond_0
    :goto_1
    return-void

    .line 185
    .restart local v3    # "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v4    # "serverChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;>;"
    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 186
    .local v2, "keyItem":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 193
    .end local v2    # "keyItem":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;>;"
    .end local v3    # "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "serverChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;>;"
    :catch_0
    move-exception v1

    .line 194
    .local v1, "e":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemoImportDialogState(Landroid/content/Context;I)V

    .line 195
    invoke-virtual {v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;->getmExceptionCode()I

    move-result v5

    const/4 v6, 0x7

    if-ne v5, v6, :cond_2

    .line 196
    new-instance v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;-><init>()V

    .line 197
    .local v0, "authinfo":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->getPushInfo(Landroid/content/Context;)V

    .line 198
    const-string v5, "SNBDownloader"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error occurred when syncing memos, access token "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 199
    iget-object v7, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->accessToken:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is not valid."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 198
    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v5, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v6

    iget-object v7, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->accessToken:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->getAccessToken(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 202
    .end local v0    # "authinfo":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;
    :cond_2
    const-string v5, "SNBDownloader"

    const-string v6, "Error occurred when syncing memos."

    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getAllkeys(Ljava/util/Map;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
        }
    .end annotation

    .prologue
    .local p1, "serverChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;>;"
    const/4 v5, 0x0

    .line 248
    const/4 v2, 0x0

    .line 249
    .local v2, "response":Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;
    const-string v4, ""

    .line 252
    .local v4, "startKey":Ljava/lang/String;
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mKVSServiceManager:Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;

    .line 253
    .local v1, "recordServiceManager":Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;
    if-nez v1, :cond_2

    move-object p1, v5

    .line 271
    .end local p1    # "serverChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;>;"
    :cond_1
    :goto_0
    return-object p1

    .line 256
    .restart local p1    # "serverChanges":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;>;"
    :cond_2
    const/16 v6, 0xfa

    invoke-virtual {v1, v4, v6}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->getKeys(Ljava/lang/String;I)Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;
    :try_end_0
    .catch Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 262
    invoke-virtual {v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->getResponseCode()I

    move-result v3

    .line 263
    .local v3, "ret_code":I
    if-nez v3, :cond_4

    .line 264
    invoke-direct {p0, v2, p1}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->addToDownloadKeyList(Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;Ljava/util/Map;)Z

    move-result v6

    if-nez v6, :cond_3

    move-object p1, v5

    .line 265
    goto :goto_0

    .line 257
    .end local v1    # "recordServiceManager":Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;
    .end local v3    # "ret_code":I
    :catch_0
    move-exception v0

    .line 258
    .local v0, "e":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
    const-string v5, "SNBDownloader"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SyncException :"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    throw v0

    .line 267
    .end local v0    # "e":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
    .restart local v1    # "recordServiceManager":Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;
    .restart local v3    # "ret_code":I
    :cond_3
    invoke-virtual {v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->getNextKey()Ljava/lang/String;

    move-result-object v4

    .line 269
    :cond_4
    invoke-virtual {v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->getNextKey()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->getNextKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_0

    goto :goto_0
.end method

.method public static getContext()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    return-object v0
.end method

.method private getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 550
    const/4 v0, 0x0

    .line 551
    .local v0, "fileName":Ljava/lang/String;
    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 552
    .local v1, "i":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 553
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 554
    :cond_0
    return-object v0
.end method

.method public static getInstance()Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mInstance:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    if-nez v0, :cond_0

    .line 126
    new-instance v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;-><init>()V

    sput-object v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mInstance:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    .line 127
    :cond_0
    sget-object v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mInstance:Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;

    return-object v0
.end method

.method private declared-synchronized getItemsFromKeys(Ljava/util/List;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
        }
    .end annotation

    .prologue
    .line 275
    .local p1, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    monitor-enter p0

    const/4 v6, 0x0

    .line 276
    .local v6, "itemResponse":Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;
    :try_start_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v11

    .line 277
    .local v11, "size":I
    const/4 v12, 0x0

    .line 278
    .local v12, "start":I
    const/16 v7, 0x19

    .line 279
    .local v7, "maxGetSize":I
    const/4 v3, 0x0

    .line 280
    .local v3, "end":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mFileDownloadMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v13}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 281
    :cond_0
    :goto_0
    if-gt v11, v12, :cond_2

    .line 327
    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    .line 282
    :cond_2
    add-int/lit8 v3, v12, 0x19

    .line 284
    if-ge v11, v3, :cond_3

    .line 285
    move v3, v11

    .line 286
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mKVSServiceManager:Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;

    .line 287
    .local v9, "recordServiceManager":Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;
    if-eqz v9, :cond_1

    .line 289
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/android/app/migration/utils/Utils;->isNetworkReachable(Landroid/content/Context;)Z

    move-result v13

    if-nez v13, :cond_4

    .line 290
    new-instance v13, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v14, 0x0

    invoke-direct {v13, v14}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v13
    :try_end_1
    .catch Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 295
    .end local v9    # "recordServiceManager":Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;
    :catch_0
    move-exception v2

    .line 296
    .local v2, "e":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
    :try_start_2
    const-string v13, "SNBDownloader"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "getItemsFromKeys: Sync exception recieved."

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 275
    .end local v2    # "e":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
    .end local v3    # "end":I
    .end local v7    # "maxGetSize":I
    .end local v11    # "size":I
    .end local v12    # "start":I
    :catchall_0
    move-exception v13

    monitor-exit p0

    throw v13

    .line 291
    .restart local v3    # "end":I
    .restart local v7    # "maxGetSize":I
    .restart local v9    # "recordServiceManager":Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;
    .restart local v11    # "size":I
    .restart local v12    # "start":I
    :cond_4
    :try_start_3
    move-object/from16 v0, p1

    invoke-interface {v0, v12, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v13

    invoke-virtual {v9, v13}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;->getItems(Ljava/util/List;)Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;

    move-result-object v6

    .line 292
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/android/app/migration/utils/Utils;->isNetworkReachable(Landroid/content/Context;)Z

    move-result v13

    if-nez v13, :cond_5

    .line 293
    new-instance v13, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v14, 0x0

    invoke-direct {v13, v14}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v13
    :try_end_3
    .catch Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 294
    :cond_5
    move v12, v3

    .line 299
    :try_start_4
    invoke-virtual {v6}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->getResponseCode()I

    move-result v10

    .line 300
    .local v10, "ret_code":I
    if-nez v10, :cond_0

    .line 301
    invoke-virtual {v6}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSResponse;->getItemDetailsList()Ljava/util/List;

    move-result-object v1

    .line 302
    .local v1, "dataRecords":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;>;"
    if-eqz v1, :cond_6

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_7

    .line 303
    :cond_6
    const-string v13, "SNBDownloader"

    const-string v14, "getItemsFromKeys: No records for KEY List"

    invoke-static {v13, v14}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 306
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mServerChangedRecords:Ljava/util/Map;

    if-eqz v13, :cond_9

    .line 307
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;>;"
    :cond_8
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-nez v13, :cond_a

    .line 324
    .end local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;>;"
    :cond_9
    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto/16 :goto_0

    .line 308
    .restart local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;>;"
    :cond_a
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;

    .line 309
    .local v8, "record":Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mServerChangedRecords:Ljava/util/Map;

    invoke-virtual {v8}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;

    .line 310
    .local v5, "item":Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;
    if-nez v5, :cond_b

    .line 311
    const-string v13, "SNBDownloader"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "GetItems retured a key which is not found in list of serverkeys. Key = "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 312
    invoke-virtual {v8}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 311
    invoke-static {v13, v14}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 315
    :cond_b
    invoke-virtual {v8}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;->isDeleted()Z

    move-result v13

    if-nez v13, :cond_c

    .line 317
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v8}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->parseJSON(Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 320
    :cond_c
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v13

    invoke-static {v13}, Lcom/samsung/android/app/migration/utils/Utils;->isNetworkReachable(Landroid/content/Context;)Z

    move-result v13

    if-nez v13, :cond_8

    .line 321
    new-instance v13, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;

    const/4 v14, 0x0

    invoke-direct {v13, v14}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;-><init>(I)V

    throw v13
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method private getServerPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 542
    const/4 v1, 0x0

    .line 543
    .local v1, "serverPath":Ljava/lang/String;
    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 544
    .local v0, "i":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 545
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/storage/sdcard0/SNote/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 546
    :cond_0
    return-object v1
.end method

.method private handleResult(Z)V
    .locals 5
    .param p1, "isOk"    # Z

    .prologue
    const/4 v4, 0x0

    .line 235
    if-eqz p1, :cond_2

    .line 236
    sget-object v1, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    .line 237
    .local v0, "account":Landroid/accounts/Account;
    sget-object v1, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_0
    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/AccountHelper;->saveAccount(Landroid/content/Context;Ljava/lang/String;)V

    .line 238
    iget-boolean v1, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mIsTMemo2DataExists:Z

    if-eqz v1, :cond_0

    .line 239
    sget-object v1, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    sget-object v2, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f0b0054

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 244
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    :goto_1
    sget-object v1, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemoImportDialogState(Landroid/content/Context;I)V

    .line 245
    return-void

    .line 237
    .restart local v0    # "account":Landroid/accounts/Account;
    :cond_1
    sget-object v2, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/app/migration/utils/SharedPref;->getSAName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 241
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_2
    sget-object v1, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    sget-object v2, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 242
    sget-object v1, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/samsung/android/app/migration/utils/SharedPref;->setImportDoNotShowFlag(Landroid/content/Context;Z)V

    goto :goto_1
.end method

.method private hideNotification(Z)V
    .locals 5
    .param p1, "isOk"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 377
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotifBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotificationManager:Landroid/app/NotificationManager;

    if-eqz v1, :cond_0

    .line 378
    const/4 v0, 0x0

    .line 379
    .local v0, "msg":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 380
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0b0054

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 383
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotifBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    invoke-virtual {v1, v3, v3, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->setProgress(IIZ)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 384
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotifBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v2}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 385
    iget-object v1, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v1, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 386
    iput-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotifBuilder:Landroid/support/v4/app/NotificationCompat$Builder;

    .line 387
    iput-object v4, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mNotificationManager:Landroid/app/NotificationManager;

    .line 388
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$4;-><init>(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 407
    .end local v0    # "msg":Ljava/lang/String;
    :cond_0
    return-void

    .line 382
    .restart local v0    # "msg":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private parseJSON(Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;)Z
    .locals 6
    .param p1, "item"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/KVSItem;
    .param p2, "record"    # Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SyncException;
        }
    .end annotation

    .prologue
    .line 411
    invoke-virtual {p2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 412
    .local v0, "data":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 413
    :cond_0
    const/4 v1, 0x0

    .line 415
    :goto_0
    return v1

    .line 414
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;->getKEY()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/records/RecordItem;->getTimeStamp()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {p0, v0, v2, v4, v5}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->parseJSON(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v1

    .line 415
    .local v1, "result":Z
    goto :goto_0
.end method

.method private parseJSON(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 15
    .param p1, "jsonString"    # Ljava/lang/String;
    .param p2, "Key"    # Ljava/lang/String;
    .param p3, "modifiedTime"    # J

    .prologue
    .line 419
    const/4 v7, 0x1

    .line 420
    .local v7, "res":Z
    const/4 v8, 0x0

    .line 422
    .local v8, "sNoteJson":Lorg/json/JSONObject;
    :try_start_0
    new-instance v13, Lorg/json/JSONTokener;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v13

    move-object v0, v13

    check-cast v0, Lorg/json/JSONObject;

    move-object v8, v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 426
    :goto_0
    if-nez v8, :cond_0

    .line 427
    const-string v13, "SNBDownloader"

    const-string v14, "parseJSON(): sNoteJson is null"

    invoke-static {v13, v14}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    const/4 v13, 0x0

    .line 475
    :goto_1
    return v13

    .line 423
    :catch_0
    move-exception v1

    .line 424
    .local v1, "e":Lorg/json/JSONException;
    const-string v13, "SNBDownloader"

    const-string v14, "Exception occurred when creating JSONTokener in parseJSON()."

    invoke-static {v13, v14, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 430
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_0
    const/4 v9, 0x0

    .line 431
    .local v9, "sNoteJsonObject":Lorg/json/JSONObject;
    const/4 v10, 0x0

    .line 432
    .local v10, "sNoteValues":Landroid/content/ContentValues;
    new-instance v5, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;

    invoke-direct {v5}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;-><init>()V

    .line 434
    .local v5, "mJsonParser":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;
    sget-object v13, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->FILE_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v5, v13}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;->setColumnsList([Ljava/lang/String;)V

    .line 436
    :try_start_1
    const-string v13, "FILES"

    invoke-virtual {v8, v13}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 437
    invoke-virtual {v5, v9}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;->fromJSON(Lorg/json/JSONObject;)Landroid/content/ContentValues;

    move-result-object v10

    .line 438
    const-string v13, "syncname"

    invoke-virtual {v10, v13}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_1

    .line 439
    const-string v13, "syncname"

    const-string v14, "name"

    invoke-virtual {v10, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    :cond_1
    const-string v13, "syncname"

    invoke-virtual {v10, v13}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 441
    .local v12, "title":Ljava/lang/String;
    const-string v13, ".snb"

    invoke-virtual {v12, v13}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 442
    .local v4, "index":I
    const/4 v13, -0x1

    if-eq v4, v13, :cond_2

    .line 443
    const/4 v13, 0x0

    invoke-virtual {v12, v13, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    .line 444
    :cond_2
    new-instance v11, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;

    invoke-direct {v11, v12}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;-><init>(Ljava/lang/String;)V

    .line 445
    .local v11, "snbData":Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;
    iget-object v13, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mFileDownloadMap:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v11}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 450
    const/4 v13, 0x0

    invoke-virtual {v5, v13}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;->setColumnsList([Ljava/lang/String;)V

    .line 453
    .end local v4    # "index":I
    .end local v11    # "snbData":Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;
    .end local v12    # "title":Ljava/lang/String;
    :goto_2
    const/4 v6, 0x0

    .line 454
    .local v6, "penJsonArray":Lorg/json/JSONArray;
    sget-object v13, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->FILEDETAIL_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v5, v13}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;->setColumnsList([Ljava/lang/String;)V

    .line 456
    :try_start_2
    const-string v13, "FILEDETAIL"

    invoke-virtual {v8, v13}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 457
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_3
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v13

    if-lt v3, v13, :cond_3

    .line 473
    const/4 v13, 0x0

    invoke-virtual {v5, v13}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;->setColumnsList([Ljava/lang/String;)V

    .end local v3    # "i":I
    :goto_4
    move v13, v7

    .line 475
    goto :goto_1

    .line 446
    .end local v6    # "penJsonArray":Lorg/json/JSONArray;
    :catch_1
    move-exception v1

    .line 447
    .restart local v1    # "e":Lorg/json/JSONException;
    :try_start_3
    const-string v13, "SNBDownloader"

    const-string v14, "Unable to parse FILES JSONObject in parseJSON()."

    invoke-static {v13, v14, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 448
    const/4 v7, 0x0

    .line 450
    const/4 v13, 0x0

    invoke-virtual {v5, v13}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;->setColumnsList([Ljava/lang/String;)V

    goto :goto_2

    .line 449
    .end local v1    # "e":Lorg/json/JSONException;
    :catchall_0
    move-exception v13

    .line 450
    const/4 v14, 0x0

    invoke-virtual {v5, v14}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;->setColumnsList([Ljava/lang/String;)V

    .line 451
    throw v13

    .line 458
    .restart local v3    # "i":I
    .restart local v6    # "penJsonArray":Lorg/json/JSONArray;
    :cond_3
    :try_start_4
    invoke-virtual {v6, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v13

    const-string v14, "path"

    invoke-virtual {v13, v14}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 459
    .local v2, "filePath":Ljava/lang/String;
    const-string v13, "snote.xml"

    invoke-virtual {v2, v13}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 460
    iget-object v13, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mFileDownloadMap:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;

    .line 461
    .restart local v11    # "snbData":Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;
    iput-object v2, v11, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;->noteFilePath:Ljava/lang/String;

    .line 462
    iget-object v13, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mFileDownloadMap:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v11}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    .end local v11    # "snbData":Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;
    :cond_4
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 463
    :cond_5
    const-string v13, "settings.xml"

    invoke-virtual {v2, v13}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 464
    iget-object v13, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mFileDownloadMap:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;

    .line 465
    .restart local v11    # "snbData":Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;
    iput-object v2, v11, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;->settingsFilePath:Ljava/lang/String;

    .line 466
    iget-object v13, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mFileDownloadMap:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v11}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_5

    .line 469
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v11    # "snbData":Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$SNBContract;
    :catch_2
    move-exception v1

    .line 470
    .restart local v1    # "e":Lorg/json/JSONException;
    :try_start_5
    const-string v13, "SNBDownloader"

    const-string v14, "Unable to parse FILEDETAIL JSONObject in parseJSON()."

    invoke-static {v13, v14, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 471
    const/4 v7, 0x0

    .line 473
    const/4 v13, 0x0

    invoke-virtual {v5, v13}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;->setColumnsList([Ljava/lang/String;)V

    goto :goto_4

    .line 472
    .end local v1    # "e":Lorg/json/JSONException;
    :catchall_1
    move-exception v13

    .line 473
    const/4 v14, 0x0

    invoke-virtual {v5, v14}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/JSONParser;->setColumnsList([Ljava/lang/String;)V

    .line 474
    throw v13
.end method

.method private showNotification()V
    .locals 3

    .prologue
    .line 330
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 331
    .local v0, "lock":Ljava/lang/Object;
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$3;

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$3;-><init>(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 369
    :try_start_0
    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 370
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    .line 369
    monitor-exit v0

    .line 374
    :goto_0
    return-void

    .line 369
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 372
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public checkSNBOnServer(Landroid/app/Activity;)V
    .locals 5
    .param p1, "ctx"    # Landroid/app/Activity;

    .prologue
    .line 131
    const/4 v2, 0x1

    invoke-static {p1, v2}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemoImportDialogState(Landroid/content/Context;I)V

    .line 132
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v2, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mContext:Ljava/lang/ref/WeakReference;

    .line 133
    new-instance v1, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;

    invoke-direct {v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;-><init>()V

    .line 134
    .local v1, "authinfo":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;
    invoke-virtual {v1, p1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->getPushInfo(Landroid/content/Context;)V

    .line 135
    iget-object v0, v1, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->accessToken:Ljava/lang/String;

    .line 136
    .local v0, "accessToken":Ljava/lang/String;
    const-string v2, "SNBDownloader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "accessToken = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->create()Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    .line 139
    new-instance v2, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    const-string v4, "a1QGNqwu27"

    invoke-direct {v2, p1, v3, v4}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;-><init>(Landroid/content/Context;Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mORSServiceManager:Lcom/samsung/android/app/migration/snbdownload/servicemanager/ORSServiceManager;

    .line 140
    new-instance v2, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;

    iget-object v3, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    const-string v4, "a1QGNqwu27"

    invoke-direct {v2, p1, v3, v4}, Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;-><init>(Landroid/content/Context;Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mKVSServiceManager:Lcom/samsung/android/app/migration/snbdownload/servicemanager/KVSServiceManager;

    .line 142
    if-eqz v0, :cond_0

    .line 143
    invoke-static {p0}, Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;->setListener(Lcom/samsung/android/app/migration/snbdownload/DownloadResponseListener;)V

    .line 144
    invoke-static {p1}, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->getNetworkServiceApi(Landroid/content/Context;)Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;->setNetworkServiceInstance(Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;)V

    .line 145
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/network/util/RelayUtil;->getNetworkServiceInstance()Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/app/migration/snbdownload/network/NetworkServiceApi;->getUserInfo(Ljava/lang/String;)V

    .line 148
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->getAccessToken(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public downloadTMemo(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 208
    .local p1, "keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 209
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/migration/utils/Utils;->isNetworkReachable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/memo/util/AccountHelper;->getRegisteredSamsungAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 211
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$2;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$2;-><init>(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 228
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 232
    :goto_0
    return-void

    .line 230
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->handleResult(Z)V

    goto :goto_0
.end method

.method public onResponse(IZ)V
    .locals 5
    .param p1, "requestType"    # I
    .param p2, "isFailed"    # Z

    .prologue
    const/4 v3, 0x0

    .line 152
    packed-switch p1, :pswitch_data_0

    .line 177
    :goto_0
    return-void

    .line 154
    :pswitch_0
    if-nez p2, :cond_0

    .line 155
    new-instance v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;

    invoke-direct {v0}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;-><init>()V

    .line 156
    .local v0, "authinfo":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->getPushInfo(Landroid/content/Context;)V

    .line 157
    const-string v2, "SNBDownloader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "userId = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->userId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 159
    .local v1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v2, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->accessToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    iget-object v2, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->regId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    iget-object v2, v0, Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;->userId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v2, p0, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->mAuthManager:Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;

    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/samsung/android/app/migration/snbdownload/network/downsync/auth/AuthManager;->updateAuthInformation(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 165
    new-instance v2, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$1;-><init>(Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;)V

    .line 170
    invoke-virtual {v2}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader$1;->start()V

    goto :goto_0

    .line 172
    .end local v0    # "authinfo":Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/SharedInfo;
    .end local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v3}, Lcom/samsung/android/app/migration/utils/SharedPref;->setImportDoNotShowFlag(Landroid/content/Context;Z)V

    .line 173
    invoke-static {}, Lcom/samsung/android/app/migration/snbdownload/SNBDownloader;->getContext()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v3}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemoImportDialogState(Landroid/content/Context;I)V

    goto :goto_0

    .line 152
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
