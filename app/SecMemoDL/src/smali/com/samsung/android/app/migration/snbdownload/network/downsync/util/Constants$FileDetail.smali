.class public interface abstract Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/Constants$FileDetail;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/migration/snbdownload/network/downsync/util/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FileDetail"
.end annotation


# static fields
.field public static final CHECKSUM:Ljava/lang/String; = "checksum"

.field public static final ID:Ljava/lang/String; = "_id"

.field public static final PATH:Ljava/lang/String; = "path"

.field public static final SNB_ID:Ljava/lang/String; = "snb_id"
