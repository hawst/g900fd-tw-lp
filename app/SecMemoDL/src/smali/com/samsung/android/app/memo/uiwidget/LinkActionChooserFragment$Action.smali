.class abstract Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;
.super Ljava/lang/Object;
.source "LinkActionChooserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "Action"
.end annotation


# instance fields
.field private final mTitleId:I


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1, "titleId"    # I

    .prologue
    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228
    iput p1, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;->mTitleId:I

    .line 229
    return-void
.end method


# virtual methods
.method abstract execute(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 233
    const-string v0, ""

    .line 235
    .local v0, "ret":Ljava/lang/String;
    :try_start_0
    # getter for: Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment;->access$0()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/memo/uiwidget/LinkActionChooserFragment$Action;->mTitleId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 240
    :goto_0
    return-object v0

    .line 236
    :catch_0
    move-exception v1

    goto :goto_0
.end method
