.class public Lcom/samsung/android/app/memo/MemoContentActivity;
.super Lcom/samsung/android/app/memo/MemoBaseActivity;
.source "MemoContentActivity.java"

# interfaces
.implements Landroid/view/KeyEvent$Callback;
.implements Lcom/samsung/android/app/memo/share/MemoShare$SessionUpdateInterfaceForMemoShare;
.implements Lcom/samsung/android/app/memo/share/beam/BeamHelper$MemoContentInterfaceForBeam;
.implements Lcom/samsung/android/app/memo/share/beam/XMLUtills$XMLUtillsOnDoneParsing;


# static fields
.field public static final KEY_BUNDLE:Ljava/lang/String;

.field public static final KEY_URI:Ljava/lang/String;

.field private static MIME_TYPE:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public isUiPickerMode:Z

.field private mBeamhelper:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

.field private mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49
    const-class v0, Lcom/samsung/android/app/memo/MemoContentActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/MemoContentActivity;->TAG:Ljava/lang/String;

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/MemoContentActivity;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_uri"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/MemoContentActivity;->KEY_URI:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/app/memo/MemoContentActivity;->TAG:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "_bundle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/MemoContentActivity;->KEY_BUNDLE:Ljava/lang/String;

    .line 55
    const-string v0, "text/DirectShareKMemo"

    sput-object v0, Lcom/samsung/android/app/memo/MemoContentActivity;->MIME_TYPE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/samsung/android/app/memo/MemoBaseActivity;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->isUiPickerMode:Z

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mBeamhelper:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    .line 47
    return-void
.end method

.method private clearObjects()V
    .locals 2

    .prologue
    .line 242
    sget-object v0, Lcom/samsung/android/app/memo/MemoContentActivity;->TAG:Ljava/lang/String;

    const-string v1, "clearObjects()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    if-eqz v0, :cond_0

    .line 244
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    .line 245
    :cond_0
    return-void
.end method

.method private handleMemoFile(Landroid/net/Uri;)V
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 117
    new-instance v4, Lcom/samsung/android/app/memo/share/beam/XMLUtills;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;-><init>(Landroid/content/Context;)V

    .line 118
    .local v4, "xmlUtils":Lcom/samsung/android/app/memo/share/beam/XMLUtills;
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 119
    .local v3, "path":Ljava/lang/String;
    const/4 v1, 0x0

    .line 121
    .local v1, "mFile":Ljava/io/File;
    :try_start_0
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/net/URI;

    invoke-direct {v5, v3}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/net/URI;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "mFile":Ljava/io/File;
    .local v2, "mFile":Ljava/io/File;
    move-object v1, v2

    .line 125
    .end local v2    # "mFile":Ljava/io/File;
    .restart local v1    # "mFile":Ljava/io/File;
    :goto_0
    if-eqz v1, :cond_0

    .line 126
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->readXMLFile(Ljava/lang/String;)V

    .line 127
    :cond_0
    return-void

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Ljava/net/URISyntaxException;
    sget-object v5, Lcom/samsung/android/app/memo/MemoContentActivity;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Memo file read "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public UpdateContentToCreateMemo()V
    .locals 0

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->updateCurrentContentToSession()V

    .line 156
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const v2, 0x1020002

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    .line 144
    .local v0, "fragment":Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;
    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->onBackPressed()V

    .line 145
    invoke-super {p0}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onBackPressed()V

    .line 146
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 215
    invoke-super {p0, p1}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 216
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->isUiPickerMode:Z

    if-eqz v0, :cond_0

    .line 217
    invoke-static {p0}, Lcom/samsung/android/app/memo/util/Utils;->slideInAnimation(Landroid/content/Context;)V

    .line 220
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    .line 60
    invoke-super {p0, p1}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 61
    sget-object v7, Lcom/samsung/android/app/memo/MemoContentActivity;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "onCreate()"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    .line 64
    .local v6, "window":Landroid/view/Window;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 65
    .local v3, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v7, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v7, v7, 0x400

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 66
    iget v7, v3, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v7, v7, 0x1

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 67
    iget v7, v3, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v7, v7, 0x2

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 68
    invoke-virtual {v6, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 69
    if-nez p1, :cond_1

    .line 71
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 72
    .local v2, "intent":Landroid/content/Intent;
    sget-object v7, Lcom/samsung/android/app/memo/MemoContentActivity;->KEY_URI:Ljava/lang/String;

    invoke-virtual {v2, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    .line 74
    .local v5, "uri":Landroid/net/Uri;
    sget-object v7, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VNTFILE:Ljava/lang/String;

    invoke-virtual {v2, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 75
    sget-object v7, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_VNTFILE:Ljava/lang/String;

    invoke-virtual {v2, v7, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 76
    const/4 v5, 0x0

    .line 79
    :cond_0
    const-string v7, "isUiPickerMode"

    invoke-virtual {v2, v7, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->isUiPickerMode:Z

    .line 81
    if-eqz v5, :cond_3

    const-string v7, "file"

    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 83
    const/4 v4, 0x0

    .line 84
    .local v4, "mUri":Landroid/net/Uri;
    :try_start_0
    new-instance v7, Lcom/samsung/android/app/memo/Session;

    invoke-direct {v7, v4}, Lcom/samsung/android/app/memo/Session;-><init>(Landroid/net/Uri;)V

    iput-object v7, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mSession:Lcom/samsung/android/app/memo/Session;

    .line 85
    invoke-direct {p0, v5}, Lcom/samsung/android/app/memo/MemoContentActivity;->handleMemoFile(Landroid/net/Uri;)V
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    .end local v4    # "mUri":Landroid/net/Uri;
    :goto_0
    sget-object v7, Lcom/samsung/android/app/memo/MemoContentActivity;->KEY_BUNDLE:Ljava/lang/String;

    invoke-virtual {v2, v7}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 104
    .local v0, "args":Landroid/os/Bundle;
    new-instance v7, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-direct {v7}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;-><init>()V

    iput-object v7, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    .line 105
    iget-object v7, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v7, v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->setArguments(Landroid/os/Bundle;)V

    .line 106
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v7

    const v8, 0x1020002

    iget-object v9, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v7, v8, v9}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 109
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_1
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v7

    if-eqz v7, :cond_2

    iget-boolean v7, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->isUiPickerMode:Z

    if-eqz v7, :cond_2

    .line 110
    invoke-static {p0}, Lcom/samsung/android/app/memo/util/Utils;->slideInAnimation(Landroid/content/Context;)V

    .line 114
    :cond_2
    :goto_1
    return-void

    .line 86
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local v4    # "mUri":Landroid/net/Uri;
    .restart local v5    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 87
    .local v1, "e":Lcom/samsung/android/app/memo/Session$SessionException;
    sget-object v7, Lcom/samsung/android/app/memo/MemoContentActivity;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "requestMemoContentFragment() memoId: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 88
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->showGeneralErrorMessage()V

    .line 89
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->finish()V

    goto :goto_1

    .line 94
    .end local v1    # "e":Lcom/samsung/android/app/memo/Session$SessionException;
    .end local v4    # "mUri":Landroid/net/Uri;
    :cond_3
    :try_start_1
    new-instance v7, Lcom/samsung/android/app/memo/Session;

    invoke-direct {v7, v5}, Lcom/samsung/android/app/memo/Session;-><init>(Landroid/net/Uri;)V

    iput-object v7, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mSession:Lcom/samsung/android/app/memo/Session;
    :try_end_1
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 95
    :catch_1
    move-exception v1

    .line 96
    .restart local v1    # "e":Lcom/samsung/android/app/memo/Session$SessionException;
    sget-object v7, Lcom/samsung/android/app/memo/MemoContentActivity;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "requestMemoContentFragment() memoId: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 97
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->showGeneralErrorMessage()V

    .line 98
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->finish()V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 236
    invoke-super {p0}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onDestroy()V

    .line 238
    invoke-direct {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->clearObjects()V

    .line 239
    return-void
.end method

.method public onDoneXMLParsing()V
    .locals 0

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->updateSessionToEditor()V

    .line 151
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    if-nez v0, :cond_0

    .line 164
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 165
    const v1, 0x1020002

    .line 164
    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iput-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    .line 167
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 177
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 169
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->isEditorFocussed()Z

    move-result v0

    if-nez v0, :cond_2

    .line 170
    const/4 v0, 0x0

    goto :goto_0

    .line 171
    :cond_2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    const/4 v0, 0x1

    goto :goto_0

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x36
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    .line 182
    invoke-super {p0, p1, p2}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/view/KeyEvent;

    .prologue
    .line 187
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    if-nez v0, :cond_0

    .line 193
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iput-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    .line 195
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 209
    invoke-super {p0, p1, p2}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 204
    :sswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->isEditorFocussed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 205
    const/4 v0, 0x0

    goto :goto_0

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 195
    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x22 -> :sswitch_0
        0x2a -> :sswitch_0
        0x2f -> :sswitch_0
        0x35 -> :sswitch_0
        0x36 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mBeamhelper:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    .line 225
    invoke-super {p0}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onPause()V

    .line 226
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 230
    new-instance v0, Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    sget-object v1, Lcom/samsung/android/app/memo/MemoContentActivity;->MIME_TYPE:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mBeamhelper:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    .line 231
    invoke-super {p0}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onResume()V

    .line 232
    return-void
.end method

.method public updateCurrentContentToSession()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->requestUpdateSession()V

    .line 133
    :cond_0
    return-void
.end method

.method public updateSessionOnMemoShare()V
    .locals 0

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoContentActivity;->updateCurrentContentToSession()V

    .line 161
    return-void
.end method

.method public updateSessionToEditor()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoContentActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->UpdateEditor()V

    .line 139
    :cond_0
    return-void
.end method
