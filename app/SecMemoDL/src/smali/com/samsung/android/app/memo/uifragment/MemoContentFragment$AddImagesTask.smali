.class public Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;
.super Landroid/os/AsyncTask;
.source "MemoContentFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AddImagesTask"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private fileDoesNoteExist:Z

.field private intent:Landroid/content/Intent;

.field private isContainVideo:Z

.field private mActivity:Landroid/content/Context;

.field private mContainsBrokenImage:Z

.field private mCount:I

.field mReceiveMultiUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mimeType:Ljava/lang/String;

.field private subject:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

.field private uri:Landroid/net/Uri;

.field uriMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/Uri;",
            "Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;",
            ">;"
        }
    .end annotation
.end field

.field urisToBeRemoved:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 813
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 784
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mContainsBrokenImage:Z

    .line 786
    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->bitmap:Landroid/graphics/Bitmap;

    .line 805
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->uriMap:Ljava/util/HashMap;

    .line 807
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->fileDoesNoteExist:Z

    .line 808
    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    .line 810
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->isContainVideo:Z

    .line 812
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->urisToBeRemoved:Ljava/util/ArrayList;

    .line 814
    iput-object p2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mActivity:Landroid/content/Context;

    .line 815
    iput-object p3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->intent:Landroid/content/Intent;

    .line 816
    iput v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mCount:I

    .line 817
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "mimeType"    # Ljava/lang/String;
    .param p5, "subject"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 818
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 784
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mContainsBrokenImage:Z

    .line 786
    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->bitmap:Landroid/graphics/Bitmap;

    .line 805
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->uriMap:Ljava/util/HashMap;

    .line 807
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->fileDoesNoteExist:Z

    .line 808
    iput-object v2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    .line 810
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->isContainVideo:Z

    .line 812
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->urisToBeRemoved:Ljava/util/ArrayList;

    .line 819
    iput-object p2, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mActivity:Landroid/content/Context;

    .line 820
    iput-object p3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->uri:Landroid/net/Uri;

    .line 821
    iput-object p4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mimeType:Ljava/lang/String;

    .line 822
    iput-object p5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->subject:Ljava/lang/String;

    .line 823
    iput v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mCount:I

    .line 824
    return-void
.end method

.method private addImage(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "altText"    # Ljava/lang/String;

    .prologue
    .line 1030
    const/4 v0, 0x0

    .line 1031
    .local v0, "attachmentUri":Landroid/net/Uri;
    const/4 v2, 0x0

    .line 1032
    .local v2, "orientation":I
    if-nez p2, :cond_0

    .line 1033
    const-string p2, "image/jpeg"

    .line 1036
    :cond_0
    if-eqz p1, :cond_2

    .line 1037
    invoke-static {p1}, Lcom/samsung/android/app/memo/util/ImageUtil;->lookupImageInfo(Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v1

    .line 1038
    .local v1, "bundle":Landroid/os/Bundle;
    sget-object v5, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_ORIENTATION:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 1039
    if-nez p3, :cond_1

    .line 1040
    sget-object v5, Lcom/samsung/android/app/memo/util/ImageUtil;->KEY_DISPLAYNAME:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 1045
    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v5}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$19(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v5

    invoke-interface {v5}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v5

    invoke-virtual {v5, p1, p3, p2, v2}, Lcom/samsung/android/app/memo/Session;->addFile(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/samsung/android/app/memo/Session$SessionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1052
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_2
    :goto_0
    new-instance v4, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;-><init>(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;)V

    .line 1053
    .local v4, "v":Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;
    invoke-virtual {v4, v2}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;->setOrientation(I)V

    .line 1054
    invoke-virtual {v4, v0}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;->setAttachmentUri(Landroid/net/Uri;)V

    .line 1055
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->uriMap:Ljava/util/HashMap;

    invoke-virtual {v5, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1057
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$21()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "addImage() from "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    return-void

    .line 1047
    .end local v4    # "v":Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;
    .restart local v1    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v3

    .line 1048
    .local v3, "se":Lcom/samsung/android/app/memo/Session$SessionException;
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$21()Ljava/lang/String;

    move-result-object v5

    const-string v6, "insertImage()"

    invoke-static {v5, v6, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private isImageFile(Landroid/net/Uri;)Z
    .locals 14
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 994
    const/4 v13, 0x0

    .line 995
    .local v13, "returnVlaue":Z
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->isContainVideo:Z

    .line 996
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://gmail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 997
    const/4 v0, 0x1

    .line 1025
    :goto_0
    return v0

    .line 999
    :cond_0
    const-string v11, ".*\\.(png|jpg|bmp|gif|jpeg|wbmp|PNG|JPG|GIF|BMP|JPEG|WBMP)"

    .line 1000
    .local v11, "re":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mActivity:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1001
    .local v12, "returnCursor":Landroid/database/Cursor;
    if-eqz v12, :cond_7

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_7

    .line 1002
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1003
    const-string v0, "_display_name"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 1004
    .local v10, "nameIndex":I
    const-string v0, "mime_type"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 1006
    .local v9, "mimeTypeIndex":I
    :try_start_0
    invoke-interface {v12, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1007
    .local v7, "fileName":Ljava/lang/String;
    invoke-interface {v12, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1008
    .local v8, "mimeType":Ljava/lang/String;
    if-eqz v7, :cond_1

    invoke-virtual {v7, v11}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1009
    :cond_1
    if-eqz v8, :cond_5

    const-string v0, "image/"

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_5

    .line 1010
    :cond_2
    const/4 v13, 0x1

    .line 1022
    .end local v7    # "fileName":Ljava/lang/String;
    .end local v8    # "mimeType":Ljava/lang/String;
    .end local v9    # "mimeTypeIndex":I
    .end local v10    # "nameIndex":I
    :cond_3
    :goto_1
    if-eqz v12, :cond_4

    .line 1023
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_4
    move v0, v13

    .line 1025
    goto :goto_0

    .line 1012
    .restart local v7    # "fileName":Ljava/lang/String;
    .restart local v8    # "mimeType":Ljava/lang/String;
    .restart local v9    # "mimeTypeIndex":I
    .restart local v10    # "nameIndex":I
    :cond_5
    if-nez v7, :cond_6

    .line 1013
    if-eqz v8, :cond_3

    :try_start_1
    const-string v0, "video/"

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1014
    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->isContainVideo:Z
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1016
    .end local v7    # "fileName":Ljava/lang/String;
    .end local v8    # "mimeType":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 1017
    .local v6, "e":Ljava/lang/IllegalStateException;
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$21()Ljava/lang/String;

    move-result-object v0

    const-string v1, "isImageFile"

    invoke-static {v0, v1, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1019
    .end local v6    # "e":Ljava/lang/IllegalStateException;
    .end local v9    # "mimeTypeIndex":I
    .end local v10    # "nameIndex":I
    :cond_7
    if-eqz v12, :cond_3

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 1020
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->fileDoesNoteExist:Z

    goto :goto_1
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 8
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v7, 0x1

    .line 838
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->intent:Landroid/content/Intent;

    if-eqz v4, :cond_2

    .line 839
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->intent:Landroid/content/Intent;

    sget-object v5, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->KEY_ACTION_SEND_IMAGE_URI:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    .line 844
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    .line 845
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    iget v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mCount:I

    const/16 v5, 0xa

    if-lt v4, v5, :cond_3

    .line 881
    :cond_0
    iget-boolean v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mContainsBrokenImage:Z

    if-nez v4, :cond_1

    .line 883
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 889
    .local v2, "receiveMultiUrisSize":I
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->intent:Landroid/content/Intent;

    if-eqz v4, :cond_e

    .line 890
    const/4 v0, 0x0

    :goto_2
    if-lt v0, v2, :cond_c

    .line 904
    .end local v0    # "i":I
    .end local v2    # "receiveMultiUrisSize":I
    :cond_1
    :goto_3
    const/4 v4, 0x0

    return-object v4

    .line 841
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    .line 842
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->uri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 846
    .restart local v0    # "i":I
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 847
    .local v3, "uri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->thresholdImageAdd:Landroid/os/CountDownTimer;
    invoke-static {v4}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$17(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/os/CountDownTimer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 848
    if-eqz v3, :cond_6

    .line 849
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    const-string v5, "file"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 850
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/net/URLConnection;->guessContentTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 851
    .local v1, "mimeType":Ljava/lang/String;
    if-eqz v1, :cond_4

    const-string v4, "image"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 852
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->urisToBeRemoved:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 854
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->thresholdImageAdd:Landroid/os/CountDownTimer;
    invoke-static {v4}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$17(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/os/CountDownTimer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/CountDownTimer;->cancel()V

    .line 845
    .end local v1    # "mimeType":Ljava/lang/String;
    :cond_6
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 855
    :cond_7
    invoke-direct {p0, v3}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->isImageFile(Landroid/net/Uri;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 856
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->urisToBeRemoved:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 857
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->thresholdImageAdd:Landroid/os/CountDownTimer;
    invoke-static {v4}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$17(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/os/CountDownTimer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/CountDownTimer;->cancel()V

    goto :goto_4

    .line 859
    :cond_8
    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMemoryCache:Landroid/util/LruCache;

    if-eqz v4, :cond_6

    .line 860
    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMemoryCache:Landroid/util/LruCache;

    invoke-virtual {v4, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->bitmap:Landroid/graphics/Bitmap;

    .line 861
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_9

    .line 862
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->thresholdImageAdd:Landroid/os/CountDownTimer;
    invoke-static {v4}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$17(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/os/CountDownTimer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/CountDownTimer;->cancel()V

    goto :goto_4

    .line 865
    :cond_9
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getDrawableFromLocalUri(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    invoke-static {v4, v3}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$18(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->bitmap:Landroid/graphics/Bitmap;

    .line 866
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->thresholdImageAdd:Landroid/os/CountDownTimer;
    invoke-static {v4}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$17(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Landroid/os/CountDownTimer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/CountDownTimer;->cancel()V

    .line 867
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_a

    .line 868
    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/HtmlImageSpan;->mMemoryCache:Landroid/util/LruCache;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v3, v5}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 869
    iget v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mCount:I

    goto :goto_4

    .line 871
    :cond_a
    iget-boolean v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mContainsBrokenImage:Z

    if-nez v4, :cond_b

    .line 872
    iput-boolean v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mContainsBrokenImage:Z

    .line 874
    :cond_b
    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->urisToBeRemoved:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 891
    .end local v3    # "uri":Landroid/net/Uri;
    .restart local v2    # "receiveMultiUrisSize":I
    :cond_c
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 892
    .restart local v3    # "uri":Landroid/net/Uri;
    const-string v4, "image/jpeg"

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->subject:Ljava/lang/String;

    invoke-direct {p0, v3, v4, v5}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->addImage(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->intent:Landroid/content/Intent;

    const-string v5, "isToBeDeleted"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 894
    if-eqz v3, :cond_d

    invoke-virtual {v3}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x2

    if-lt v4, v5, :cond_d

    .line 895
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v4}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$19(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v4

    invoke-interface {v4}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v5

    .line 896
    invoke-virtual {v3}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 895
    invoke-virtual {v5, v4}, Lcom/samsung/android/app/memo/Session;->removeFile(Ljava/lang/String;)Z

    .line 890
    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 900
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_e
    iget-object v4, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->uri:Landroid/net/Uri;

    const-string v5, "image/jpeg"

    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->subject:Ljava/lang/String;

    invoke-direct {p0, v4, v5, v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->addImage(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 11
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 909
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    if-nez v6, :cond_0

    .line 991
    :goto_0
    return-void

    .line 912
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    if-eqz v6, :cond_8

    .line 913
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->intent:Landroid/content/Intent;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    invoke-static {v6}, Lcom/samsung/android/app/memo/util/Utils;->containNotSupportedFormat(Ljava/util/ArrayList;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 914
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f0b0059

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 916
    :cond_1
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->intent:Landroid/content/Intent;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    invoke-static {v6}, Lcom/samsung/android/app/memo/util/ImageUtil;->containVideoUri(Ljava/util/ArrayList;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->isContainVideo:Z

    if-eqz v6, :cond_3

    .line 917
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f0b00af

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 919
    :cond_3
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->urisToBeRemoved:Ljava/util/ArrayList;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 920
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 922
    .local v3, "receiveMultiUrisSize":I
    iget-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mContainsBrokenImage:Z

    if-eqz v6, :cond_4

    .line 923
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f0b0041

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 926
    :cond_4
    const/16 v6, 0xa

    if-le v3, v6, :cond_5

    .line 927
    const/16 v3, 0xa

    .line 929
    :cond_5
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->intent:Landroid/content/Intent;

    if-eqz v6, :cond_d

    .line 930
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-lt v0, v3, :cond_9

    .line 948
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->requestFocus()Z

    .line 969
    .end local v0    # "i":I
    :goto_2
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iget-object v6, v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->keypadHandler:Landroid/os/Handler;

    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 972
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iget-object v6, v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iget-object v6, v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 973
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iget-object v6, v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v6}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 977
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    const/4 v7, 0x0

    iput-object v7, v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mProgress:Landroid/app/ProgressDialog;

    .line 980
    :goto_3
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/16 v7, 0xa

    if-le v6, v7, :cond_7

    .line 981
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f0b0042

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 983
    :cond_7
    iget-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->fileDoesNoteExist:Z

    if-eqz v6, :cond_8

    .line 984
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f0b005a

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 985
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->fileDoesNoteExist:Z

    .line 989
    .end local v3    # "receiveMultiUrisSize":I
    :cond_8
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 931
    .restart local v0    # "i":I
    .restart local v3    # "receiveMultiUrisSize":I
    :cond_9
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mReceiveMultiUris:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 932
    .local v4, "uri":Landroid/net/Uri;
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->uriMap:Ljava/util/HashMap;

    invoke-virtual {v6, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;

    .line 933
    .local v5, "v":Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;
    if-eqz v5, :cond_a

    .line 934
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;->getAttachmentUri()Landroid/net/Uri;

    move-result-object v7

    const-string v8, "image/jpeg"

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;->getOrientation()I

    move-result v9

    iget-object v10, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->subject:Ljava/lang/String;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->insertImage(Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;)Z
    invoke-static {v6, v7, v8, v9, v10}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$20(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;)Z

    move-result v2

    .line 935
    .local v2, "insertResult":Z
    if-nez v2, :cond_a

    .line 936
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mimeType:Ljava/lang/String;

    if-eqz v6, :cond_c

    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mimeType:Ljava/lang/String;

    const-string v7, "image"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_c

    .line 937
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f0b00a9

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    .line 942
    .end local v2    # "insertResult":Z
    :cond_a
    :goto_4
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->intent:Landroid/content/Intent;

    const-string v7, "isToBeDeleted"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 943
    if-eqz v4, :cond_b

    invoke-virtual {v4}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x2

    if-lt v6, v7, :cond_b

    .line 944
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mFragmentAction:Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;
    invoke-static {v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$19(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;

    move-result-object v6

    invoke-interface {v6}, Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;->getSession()Lcom/samsung/android/app/memo/Session;

    move-result-object v7

    .line 945
    invoke-virtual {v4}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    const/4 v8, 0x1

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 944
    invoke-virtual {v7, v6}, Lcom/samsung/android/app/memo/Session;->removeFile(Ljava/lang/String;)Z

    .line 930
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 939
    .restart local v2    # "insertResult":Z
    :cond_c
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f0b0041

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_4

    .line 950
    .end local v0    # "i":I
    .end local v2    # "insertResult":Z
    .end local v4    # "uri":Landroid/net/Uri;
    .end local v5    # "v":Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;
    :cond_d
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->uriMap:Ljava/util/HashMap;

    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->uri:Landroid/net/Uri;

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;

    .line 951
    .restart local v5    # "v":Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;
    const/4 v2, 0x0

    .line 952
    .restart local v2    # "insertResult":Z
    iget-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mContainsBrokenImage:Z

    if-nez v6, :cond_e

    .line 953
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;->getAttachmentUri()Landroid/net/Uri;

    move-result-object v7

    const-string v8, "image/jpeg"

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;->getOrientation()I

    move-result v9

    iget-object v10, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->subject:Ljava/lang/String;

    # invokes: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->insertImage(Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;)Z
    invoke-static {v6, v7, v8, v9, v10}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$20(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;)Z

    move-result v2

    .line 954
    :cond_e
    if-nez v2, :cond_11

    .line 955
    iget-boolean v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->isContainVideo:Z

    if-eqz v6, :cond_f

    .line 956
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 957
    const v7, 0x7f0b00af

    const/4 v8, 0x1

    .line 956
    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    .line 958
    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    .line 959
    :cond_f
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mimeType:Ljava/lang/String;

    if-eqz v6, :cond_10

    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mimeType:Ljava/lang/String;

    const-string v7, "image"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_10

    .line 960
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f0b00a9

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_2

    .line 962
    :cond_10
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const v7, 0x7f0b0041

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_2

    .line 964
    :cond_11
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mEditor:Lcom/samsung/android/app/memo/uiwidget/RichEditor;
    invoke-static {v6}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$2(Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;)Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->requestFocus()Z

    goto/16 :goto_2

    .line 974
    .end local v2    # "insertResult":Z
    .end local v5    # "v":Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask$Vector;
    :catch_0
    move-exception v1

    .line 975
    .local v1, "ie":Ljava/lang/IllegalArgumentException;
    :try_start_1
    # getter for: Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->access$21()Ljava/lang/String;

    move-result-object v6

    const-string v7, "onPostExecute"

    invoke-static {v6, v7, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 977
    iget-object v6, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    const/4 v7, 0x0

    iput-object v7, v6, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mProgress:Landroid/app/ProgressDialog;

    goto/16 :goto_3

    .line 976
    .end local v1    # "ie":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v6

    .line 977
    iget-object v7, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    const/4 v8, 0x0

    iput-object v8, v7, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mProgress:Landroid/app/ProgressDialog;

    .line 978
    throw v6
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    .line 828
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mActivity:Landroid/content/Context;

    const-string v2, ""

    .line 829
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->mActivity:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00ab

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 828
    invoke-static {v1, v2, v3}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mProgress:Landroid/app/ProgressDialog;

    .line 830
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 831
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isIndeterminate()Z

    .line 832
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment$AddImagesTask;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;

    iget-object v0, v0, Lcom/samsung/android/app/memo/uifragment/MemoContentFragment;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 833
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 834
    return-void
.end method
