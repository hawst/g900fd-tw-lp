.class Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$2;
.super Ljava/lang/Object;
.source "CategoryManagementFragment.java"

# interfaces
.implements Lcom/samsung/android/app/memo/uiwidget/DndListView$OnMoveCompletedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->InitView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onOrderChanged(II)V
    .locals 6
    .param p1, "source"    # I
    .param p2, "destination"    # I

    .prologue
    .line 242
    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 243
    .local v1, "listSize":I
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mIsFirstTime:Z
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$3(Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 244
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v1, :cond_3

    .line 246
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$5(Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;Z)V

    .line 248
    .end local v0    # "i":I
    :cond_0
    if-ge p1, v1, :cond_1

    if-ge p2, v1, :cond_1

    if-ltz p1, :cond_1

    if-gez p2, :cond_4

    .line 249
    :cond_1
    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$6()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "source: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", destination: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 250
    const-string v5, ", but listSize: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 249
    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    :cond_2
    :goto_1
    return-void

    .line 245
    .restart local v0    # "i":I
    :cond_3
    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mTempListViewItems:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$4()Ljava/util/ArrayList;

    move-result-object v4

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 244
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254
    .end local v0    # "i":I
    :cond_4
    if-eq p1, p2, :cond_2

    .line 255
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    invoke-virtual {v5}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 256
    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 255
    invoke-virtual {v3, p1, p2, v4, v5}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->doReorderItems(IIILandroid/content/ContentResolver;)V

    .line 258
    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    .line 259
    .local v2, "srcListViewItem":Lcom/samsung/android/app/memo/uifragment/CategoryListItem;
    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 260
    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p2, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 262
    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment$2;->this$0:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;
    invoke-static {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$1(Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;)Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->notifyDataSetChanged()V

    .line 264
    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$6()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "source::"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "destination::"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$6()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v3, "Orderchanged : "

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 266
    # getter for: Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->mListViewItems:Ljava/util/ArrayList;
    invoke-static {}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->access$2()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;->getCategoryName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 265
    invoke-static {v4, v3}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method
