.class Lcom/samsung/android/app/memo/Main$5;
.super Landroid/support/v4/app/ActionBarDrawerToggle;
.source "Main.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/Main;->setDrawerEnabled(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/Main;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/Main;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V
    .locals 6
    .param p2, "$anonymous0"    # Landroid/app/Activity;
    .param p3, "$anonymous1"    # Landroid/support/v4/widget/DrawerLayout;
    .param p4, "$anonymous2"    # I
    .param p5, "$anonymous3"    # I
    .param p6, "$anonymous4"    # I

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/Main$5;->this$0:Lcom/samsung/android/app/memo/Main;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    .line 936
    invoke-direct/range {v0 .. v5}, Landroid/support/v4/app/ActionBarDrawerToggle;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V

    return-void
.end method


# virtual methods
.method public onDrawerClosed(Landroid/view/View;)V
    .locals 1
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 949
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onDrawerClosed(Landroid/view/View;)V

    .line 950
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main$5;->this$0:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Main;->initDrawerClosing()V

    .line 951
    sget-boolean v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->isNeededCategoryFragment:Z

    if-eqz v0, :cond_0

    .line 952
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/app/memo/uifragment/CategoryListFragment;->isNeededCategoryFragment:Z

    .line 953
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main$5;->this$0:Lcom/samsung/android/app/memo/Main;

    # invokes: Lcom/samsung/android/app/memo/Main;->startCategoryManagementFragment()V
    invoke-static {v0}, Lcom/samsung/android/app/memo/Main;->access$4(Lcom/samsung/android/app/memo/Main;)V

    .line 957
    :goto_0
    return-void

    .line 956
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main$5;->this$0:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Main;->invalidateOptionsMenu()V

    goto :goto_0
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 1
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    .line 961
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main$5;->this$0:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Main;->initDrawerClosing()V

    .line 962
    invoke-super {p0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onDrawerOpened(Landroid/view/View;)V

    .line 963
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main$5;->this$0:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Main;->invalidateOptionsMenu()V

    .line 964
    return-void
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
    .locals 1
    .param p1, "drawerView"    # Landroid/view/View;
    .param p2, "slideOffset"    # F

    .prologue
    .line 942
    iget-object v0, p0, Lcom/samsung/android/app/memo/Main$5;->this$0:Lcom/samsung/android/app/memo/Main;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/Main;->closeOptionsMenu()V

    .line 943
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ActionBarDrawerToggle;->onDrawerSlide(Landroid/view/View;F)V

    .line 944
    return-void
.end method
