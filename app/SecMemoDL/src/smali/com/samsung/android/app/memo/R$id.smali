.class public final Lcom/samsung/android/app/memo/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final RelativeLayout1:I = 0x7f0e0081

.field public static final action_select_all:I = 0x7f0e00a3

.field public static final action_select_none:I = 0x7f0e00a4

.field public static final app_name_textview:I = 0x7f0e008c

.field public static final background_img:I = 0x7f0e0082

.field public static final btn_category:I = 0x7f0e0031

.field public static final btn_insert:I = 0x7f0e0032

.field public static final btn_voice:I = 0x7f0e0033

.field public static final button_pause:I = 0x7f0e0078

.field public static final button_play:I = 0x7f0e0076

.field public static final button_play_pause_sep:I = 0x7f0e0077

.field public static final button_with_multi_icon_recording:I = 0x7f0e0071

.field public static final button_with_sigle_icon_play:I = 0x7f0e0075

.field public static final button_with_sigle_icon_record:I = 0x7f0e006d

.field public static final camera:I = 0x7f0e0023

.field public static final category_all:I = 0x7f0e0019

.field public static final category_all_count:I = 0x7f0e001a

.field public static final category_all_layout:I = 0x7f0e0018

.field public static final category_chooser_item_name:I = 0x7f0e0001

.field public static final category_drawer:I = 0x7f0e0004

.field public static final category_drawer_stub:I = 0x7f0e0028

.field public static final category_edit:I = 0x7f0e0006

.field public static final category_edit_layout:I = 0x7f0e0005

.field public static final category_item:I = 0x7f0e0009

.field public static final category_item_count:I = 0x7f0e000a

.field public static final category_item_layout:I = 0x7f0e0008

.field public static final category_management_delete_button:I = 0x7f0e000f

.field public static final category_management_item:I = 0x7f0e000e

.field public static final category_management_reorder_button:I = 0x7f0e000d

.field public static final category_move_item_name:I = 0x7f0e0010

.field public static final category_none:I = 0x7f0e001c

.field public static final category_none_count:I = 0x7f0e001d

.field public static final category_none_layout:I = 0x7f0e001b

.field public static final cb_donotshowagain:I = 0x7f0e0021

.field public static final checked:I = 0x7f0e008d

.field public static final drawer_layout:I = 0x7f0e0026

.field public static final edit_voice_delete_layout:I = 0x7f0e006e

.field public static final editor:I = 0x7f0e002c

.field public static final editor_title:I = 0x7f0e002b

.field public static final empty_list_view:I = 0x7f0e0085

.field public static final empty_list_view_tablet:I = 0x7f0e0089

.field public static final et_password:I = 0x7f0e004b

.field public static final fab_btn:I = 0x7f0e0038

.field public static final fragment_container:I = 0x7f0e0027

.field public static final gallery:I = 0x7f0e0025

.field public static final image_view:I = 0x7f0e001e

.field public static final left_margin_layout:I = 0x7f0e002f

.field public static final license_webview:I = 0x7f0e004a

.field public static final light_no_item_memo:I = 0x7f0e003d

.field public static final linear_expandable_opened:I = 0x7f0e006c

.field public static final listView_main:I = 0x7f0e0084

.field public static final list_item_cb:I = 0x7f0e0040

.field public static final memo_grid:I = 0x7f0e003c

.field public static final memo_list:I = 0x7f0e0037

.field public static final memo_list_contents:I = 0x7f0e0043

.field public static final memo_list_date_time:I = 0x7f0e0044

.field public static final memo_list_hover_image:I = 0x7f0e003e

.field public static final memo_list_image:I = 0x7f0e0042

.field public static final memo_list_title:I = 0x7f0e0041

.field public static final memo_list_top_padding:I = 0x7f0e0045

.field public static final memo_lst_datetime:I = 0x7f0e0047

.field public static final memo_lst_rl:I = 0x7f0e003f

.field public static final memo_lst_rl2:I = 0x7f0e0046

.field public static final memo_quickpanel_voiceMemoPauseBtn:I = 0x7f0e006a

.field public static final memo_quickpanel_voiceMemoPlayBtn:I = 0x7f0e0069

.field public static final memo_quickpanel_voiceMemoRecBtn:I = 0x7f0e0068

.field public static final memo_quickpanel_voiceMemoStopBtn:I = 0x7f0e006b

.field public static final memo_select_dialog_text:I = 0x7f0e0048

.field public static final menu_category_add:I = 0x7f0e008f

.field public static final menu_category_cancel:I = 0x7f0e0090

.field public static final menu_category_done:I = 0x7f0e0091

.field public static final mitem_about:I = 0x7f0e009d

.field public static final mitem_account:I = 0x7f0e009c

.field public static final mitem_cancel:I = 0x7f0e0093

.field public static final mitem_category_add:I = 0x7f0e009a

.field public static final mitem_category_edit:I = 0x7f0e009b

.field public static final mitem_create_new:I = 0x7f0e009e

.field public static final mitem_delete:I = 0x7f0e0092

.field public static final mitem_delete_action_mode:I = 0x7f0e009f

.field public static final mitem_done:I = 0x7f0e0094

.field public static final mitem_move_action_mode:I = 0x7f0e00a1

.field public static final mitem_print:I = 0x7f0e0096

.field public static final mitem_print_action_mode:I = 0x7f0e00a2

.field public static final mitem_rename_categories:I = 0x7f0e0099

.field public static final mitem_search:I = 0x7f0e0097

.field public static final mitem_select:I = 0x7f0e0098

.field public static final mitem_share_action_mode:I = 0x7f0e00a0

.field public static final mitem_share_via:I = 0x7f0e0095

.field public static final move_no_categories:I = 0x7f0e0011

.field public static final move_popup_list:I = 0x7f0e0012

.field public static final no_category_popup:I = 0x7f0e000b

.field public static final no_memos_popup:I = 0x7f0e0035

.field public static final notifiation_icon:I = 0x7f0e0066

.field public static final notification_time:I = 0x7f0e0067

.field public static final open_source_licences:I = 0x7f0e0000

.field public static final outline:I = 0x7f0e0083

.field public static final popup_list:I = 0x7f0e0003

.field public static final radiobutton:I = 0x7f0e0002

.field public static final recording_button_pause:I = 0x7f0e0072

.field public static final recording_button_stop:I = 0x7f0e0073

.field public static final right_margin_layout:I = 0x7f0e0030

.field public static final rl:I = 0x7f0e0024

.field public static final rl2:I = 0x7f0e0022

.field public static final scroll_content:I = 0x7f0e002a

.field public static final select_all_cb:I = 0x7f0e0036

.field public static final select_all_checkbox:I = 0x7f0e0014

.field public static final select_all_layout:I = 0x7f0e0013

.field public static final select_all_text:I = 0x7f0e0015

.field public static final selection_back:I = 0x7f0e0016

.field public static final selection_menu:I = 0x7f0e0017

.field public static final seperator:I = 0x7f0e007b

.field public static final shadow_image:I = 0x7f0e0029

.field public static final shareAppIcon:I = 0x7f0e0063

.field public static final shareAppName:I = 0x7f0e0064

.field public static final show_record_porgress:I = 0x7f0e0080

.field public static final stop_recording_duration:I = 0x7f0e0079

.field public static final sw_voice_view:I = 0x7f0e007f

.field public static final text_input_dialog:I = 0x7f0e0007

.field public static final text_no_categories:I = 0x7f0e000c

.field public static final text_no_memos:I = 0x7f0e0034

.field public static final timeText_endtime:I = 0x7f0e007d

.field public static final timeText_playtime:I = 0x7f0e007c

.field public static final title:I = 0x7f0e0049

.field public static final toolbar:I = 0x7f0e002e

.field public static final tv_message:I = 0x7f0e001f

.field public static final tv_message_additional:I = 0x7f0e0020

.field public static final tv_record_button:I = 0x7f0e006f

.field public static final tv_record_duration:I = 0x7f0e0070

.field public static final tv_recording_duration:I = 0x7f0e0074

.field public static final unchecked:I = 0x7f0e008e

.field public static final undobar:I = 0x7f0e0039

.field public static final undobar_button:I = 0x7f0e003b

.field public static final undobar_message:I = 0x7f0e003a

.field public static final voice_delete_btn:I = 0x7f0e007e

.field public static final voice_layout:I = 0x7f0e002d

.field public static final voice_memo_list_view:I = 0x7f0e008a

.field public static final voicememo_quickpanel:I = 0x7f0e0065

.field public static final vr_progress:I = 0x7f0e007a

.field public static final widget_item_image1:I = 0x7f0e004f

.field public static final widget_item_image10:I = 0x7f0e0061

.field public static final widget_item_image2:I = 0x7f0e0051

.field public static final widget_item_image3:I = 0x7f0e0053

.field public static final widget_item_image4:I = 0x7f0e0055

.field public static final widget_item_image5:I = 0x7f0e0057

.field public static final widget_item_image6:I = 0x7f0e0059

.field public static final widget_item_image7:I = 0x7f0e005b

.field public static final widget_item_image8:I = 0x7f0e005d

.field public static final widget_item_image9:I = 0x7f0e005f

.field public static final widget_item_text1:I = 0x7f0e004e

.field public static final widget_item_text10:I = 0x7f0e0060

.field public static final widget_item_text11:I = 0x7f0e0062

.field public static final widget_item_text2:I = 0x7f0e0050

.field public static final widget_item_text3:I = 0x7f0e0052

.field public static final widget_item_text4:I = 0x7f0e0054

.field public static final widget_item_text5:I = 0x7f0e0056

.field public static final widget_item_text6:I = 0x7f0e0058

.field public static final widget_item_text7:I = 0x7f0e005a

.field public static final widget_item_text8:I = 0x7f0e005c

.field public static final widget_item_text9:I = 0x7f0e005e

.field public static final widget_item_title:I = 0x7f0e004d

.field public static final widget_ll:I = 0x7f0e004c

.field public static final widget_no_memo_bg:I = 0x7f0e0086

.field public static final widget_no_memo_layout:I = 0x7f0e0087

.field public static final widget_no_memo_text:I = 0x7f0e0088

.field public static final widget_voice_icon:I = 0x7f0e008b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
