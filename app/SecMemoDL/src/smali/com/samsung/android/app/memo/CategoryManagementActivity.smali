.class public Lcom/samsung/android/app/memo/CategoryManagementActivity;
.super Lcom/samsung/android/app/memo/MemoBaseActivity;
.source "CategoryManagementActivity.java"

# interfaces
.implements Landroid/view/KeyEvent$Callback;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mContentFragment:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/samsung/android/app/memo/CategoryManagementActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/CategoryManagementActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/android/app/memo/MemoBaseActivity;-><init>()V

    return-void
.end method

.method private clearObjects()V
    .locals 2

    .prologue
    .line 87
    sget-object v0, Lcom/samsung/android/app/memo/CategoryManagementActivity;->TAG:Ljava/lang/String;

    const-string v1, " clearObjects()"

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/samsung/android/app/memo/CategoryManagementActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/CategoryManagementActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    .line 90
    :cond_0
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 61
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/samsung/android/app/memo/util/Utils;->makePopUpWindow(Landroid/content/Context;Z)V

    .line 64
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/CategoryManagementActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 39
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/CategoryManagementActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 40
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v2, v2, 0x400

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 41
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 42
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 43
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 45
    new-instance v2, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    invoke-direct {v2}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/app/memo/CategoryManagementActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    .line 46
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/CategoryManagementActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/CategoryManagementActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x1020002

    iget-object v4, p0, Lcom/samsung/android/app/memo/CategoryManagementActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    invoke-virtual {v2, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 50
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/memo/util/Utils;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 51
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lcom/samsung/android/app/memo/util/Utils;->makePopUpWindow(Landroid/content/Context;Z)V

    .line 54
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/memo/CategoryManagementActivity;->setFinishOnTouchOutside(Z)V

    .line 56
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 81
    invoke-super {p0}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onDestroy()V

    .line 83
    invoke-direct {p0}, Lcom/samsung/android/app/memo/CategoryManagementActivity;->clearObjects()V

    .line 84
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    .line 68
    const/16 v0, 0x70

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 69
    iget-object v0, p0, Lcom/samsung/android/app/memo/CategoryManagementActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/samsung/android/app/memo/CategoryManagementActivity;->mContentFragment:Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementFragment;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    const/4 v0, 0x1

    .line 76
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
