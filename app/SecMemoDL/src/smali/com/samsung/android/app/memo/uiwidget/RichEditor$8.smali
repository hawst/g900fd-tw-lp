.class Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;
.super Ljava/lang/Object;
.source "RichEditor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uiwidget/RichEditor;->onClipboardPaste(Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

.field private final synthetic val$proxyData:Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->val$proxyData:Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;

    .line 763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 767
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-static {v2, v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$12(Lcom/samsung/android/app/memo/uiwidget/RichEditor;I)V

    .line 768
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$13(Lcom/samsung/android/app/memo/uiwidget/RichEditor;I)V

    .line 769
    sput-boolean v4, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mImagePasteFailed:Z

    .line 771
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionStart()I

    move-result v1

    .line 772
    .local v1, "selStart":I
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getSelectionEnd()I

    move-result v0

    .line 774
    .local v0, "selEnd":I
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$12(Lcom/samsung/android/app/memo/uiwidget/RichEditor;I)V

    .line 775
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$13(Lcom/samsung/android/app/memo/uiwidget/RichEditor;I)V

    .line 776
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    invoke-virtual {v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->getText()Landroid/text/Editable;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I
    invoke-static {v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$14(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I
    invoke-static {v5}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$15(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)I

    move-result v5

    invoke-interface {v3, v4, v5}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$22(Lcom/samsung/android/app/memo/uiwidget/RichEditor;Ljava/lang/CharSequence;)V

    .line 777
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mClipboardservice:Lcom/samsung/android/app/memo/uiwidget/ClipboardService;
    invoke-static {v2}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$23(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Lcom/samsung/android/app/memo/uiwidget/ClipboardService;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->val$proxyData:Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;

    iget-object v4, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->mStrSelection:Ljava/lang/CharSequence;
    invoke-static {v4}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$24(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)Ljava/lang/CharSequence;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->min:I
    invoke-static {v5}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$14(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/RichEditor$8;->this$0:Lcom/samsung/android/app/memo/uiwidget/RichEditor;

    # getter for: Lcom/samsung/android/app/memo/uiwidget/RichEditor;->max:I
    invoke-static {v6}, Lcom/samsung/android/app/memo/uiwidget/RichEditor;->access$15(Lcom/samsung/android/app/memo/uiwidget/RichEditor;)I

    move-result v6

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/samsung/android/app/memo/uiwidget/ClipboardService;->onTextPaste(Lcom/samsung/android/app/memo/clipboardproxy/ClipProxyData;Ljava/lang/CharSequence;II)V

    .line 778
    return-void
.end method
