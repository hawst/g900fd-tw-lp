.class public Lcom/samsung/android/app/migration/utils/SharedPref;
.super Ljava/lang/Object;
.source "SharedPref.java"


# static fields
.field private static final PREFERENCES_DO_NOT_SHOW_IMPORT_DIALOG:Ljava/lang/String; = "DO_NOT_SHOW_IMPORT_DIALOG"

.field private static final PREFERENCES_DO_NOT_SHOW_SAMSUNG_ACCOUNT_DIALOG:Ljava/lang/String; = "DO_NOT_SHOW_SAMSUNG_ACCOUNT_DIALOG"

.field private static final PREFERENCES_IS_MEMO_RELAUNCHED_FOR_SYNC_MEMO_DIALOG_FLAG:Ljava/lang/String; = "IS_MEMO_RELAUNCHED_FOR_SYNC_MEMO_DIALOG_FLAG"

.field private static final PREFERENCES_IS_MEMO_RELAUNCHED_FOR_TMEMO_IMPORT_DIALOG_FLAG:Ljava/lang/String; = "IS_MEMO_RELAUNCHED_FOR_TMEMO_IMPORT_DIALOG_FLAG"

.field private static final PREFERENCES_IS_SA_VERIFICATION_STARTED_FLAG:Ljava/lang/String; = "IS_SA_VERIFICATION_STARTED"

.field private static final PREFERENCES_KIES_MEMO_BACKING_UP_IN_PROCESS_FLAG:Ljava/lang/String; = "IS_KIES_MEMO_BACKING_UP_IN_PROCESS"

.field private static final PREFERENCES_KIES_MEMO_RESTORING_IN_PROCESS_FLAG:Ljava/lang/String; = "IS_KIES_MEMO_RESTORING_IN_PROCESS"

.field private static final PREFERENCES_KIES_TMEMO2_RESTORING_IN_PROCESS_FLAG:Ljava/lang/String; = "IS_KIES_TMEMO2_RESTORING_IN_PROCESS"

.field private static final PREFERENCES_LOCAL_MEMOS_MIGRATION_COMPLETED_FLAG:Ljava/lang/String; = "IS_LOCAL_MIGRATION_COMPLETED"

.field private static final PREFERENCES_LOCAL_MEMOS_MIGRATION_IN_PROCESS_FLAG:Ljava/lang/String; = "IS_LOCAL_MIGRATION_IN_PROCESS"

.field private static final PREFERENCES_MIGRATION:Ljava/lang/String; = "MIGRATION_PREFERENCES"

.field private static final PREFERENCES_SA_NAME:Ljava/lang/String; = "SA_NAME"

.field private static final PREFERENCES_SA_VERIFY_SETTING:Ljava/lang/String; = "IS_SA_VERIFIED"

.field private static final PREFERENCES_SYNC_MEMO_DIALOG_STATE:Ljava/lang/String; = "SYNC_MEMO_DIALOG_STATE"

.field private static final PREFERENCES_TMEMO_IMPORT_DIALOG_STATE:Ljava/lang/String; = "TMEMO_IMPORT_DIALOG_STATE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSAName(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 169
    const-string v1, "MIGRATION_PREFERENCES"

    .line 170
    const/4 v2, 0x0

    .line 169
    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 171
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "SA_NAME"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getSyncMemoDialogState(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 110
    const-string v1, "MIGRATION_PREFERENCES"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 112
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "SYNC_MEMO_DIALOG_STATE"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getTMemoImportDialogState(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 124
    const-string v1, "MIGRATION_PREFERENCES"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 126
    .local v0, "sharedPref":Landroid/content/SharedPreferences;
    const-string v1, "TMEMO_IMPORT_DIALOG_STATE"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static isImportDoNotShowFlagSet(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 183
    const-string v1, "MIGRATION_PREFERENCES"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 185
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "DO_NOT_SHOW_IMPORT_DIALOG"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isLocalMemosMigrationCompleted(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 40
    const-string v1, "MIGRATION_PREFERENCES"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 42
    .local v0, "sharedPref":Landroid/content/SharedPreferences;
    const-string v1, "IS_LOCAL_MIGRATION_COMPLETED"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isLocalMemosMigrationInProcess(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 54
    const-string v1, "MIGRATION_PREFERENCES"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 56
    .local v0, "sharedPref":Landroid/content/SharedPreferences;
    const-string v1, "IS_LOCAL_MIGRATION_IN_PROCESS"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isMemoBackingUpInProcess(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 96
    const-string v1, "MIGRATION_PREFERENCES"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 98
    .local v0, "sharedPref":Landroid/content/SharedPreferences;
    const-string v1, "IS_KIES_MEMO_BACKING_UP_IN_PROCESS"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isMemoRelaunchedForSyncMemoDialog(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 197
    const-string v1, "MIGRATION_PREFERENCES"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 199
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "IS_MEMO_RELAUNCHED_FOR_SYNC_MEMO_DIALOG_FLAG"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isMemoRelaunchedForTMemoImportDialog(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 211
    const-string v1, "MIGRATION_PREFERENCES"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 213
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "IS_MEMO_RELAUNCHED_FOR_TMEMO_IMPORT_DIALOG_FLAG"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isMemoRestoringInProcess(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 82
    const-string v1, "MIGRATION_PREFERENCES"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 84
    .local v0, "sharedPref":Landroid/content/SharedPreferences;
    const-string v1, "IS_KIES_MEMO_RESTORING_IN_PROCESS"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isSADoNotShowFlagSet(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 225
    const-string v1, "MIGRATION_PREFERENCES"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 227
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "DO_NOT_SHOW_SAMSUNG_ACCOUNT_DIALOG"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isSAVerificationStarted(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 155
    const-string v1, "MIGRATION_PREFERENCES"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 157
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "IS_SA_VERIFICATION_STARTED"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isSAVerified(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 141
    const-string v1, "MIGRATION_PREFERENCES"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 143
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "IS_SA_VERIFIED"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isTMemo2RestoringInProcess(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 68
    const-string v1, "MIGRATION_PREFERENCES"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 70
    .local v0, "sharedPref":Landroid/content/SharedPreferences;
    const-string v1, "IS_KIES_TMEMO2_RESTORING_IN_PROCESS"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static setImportDoNotShowFlag(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 189
    const-string v2, "MIGRATION_PREFERENCES"

    .line 190
    const/4 v3, 0x0

    .line 189
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 191
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 192
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "DO_NOT_SHOW_IMPORT_DIALOG"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 193
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 194
    return-void
.end method

.method public static setLocalMemosMigrationCompletedFlag(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    const-string v2, "MIGRATION_PREFERENCES"

    .line 47
    const/4 v3, 0x0

    .line 46
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 48
    .local v1, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 49
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "IS_LOCAL_MIGRATION_COMPLETED"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 50
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 51
    return-void
.end method

.method public static setLocalMemosMigrationInProcessFlag(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "val"    # Z

    .prologue
    .line 60
    const-string v2, "MIGRATION_PREFERENCES"

    .line 61
    const/4 v3, 0x0

    .line 60
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 62
    .local v1, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 63
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "IS_LOCAL_MIGRATION_IN_PROCESS"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 64
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 65
    return-void
.end method

.method public static setMemoBackingUpInProcessFlag(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "val"    # Z

    .prologue
    .line 102
    const-string v2, "MIGRATION_PREFERENCES"

    .line 103
    const/4 v3, 0x0

    .line 102
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 104
    .local v1, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 105
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "IS_KIES_MEMO_BACKING_UP_IN_PROCESS"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 106
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 107
    return-void
.end method

.method public static setMemoRelaunchedForSyncMemoDialogFlag(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 203
    const-string v2, "MIGRATION_PREFERENCES"

    .line 204
    const/4 v3, 0x0

    .line 203
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 205
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 206
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "IS_MEMO_RELAUNCHED_FOR_SYNC_MEMO_DIALOG_FLAG"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 207
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 208
    return-void
.end method

.method public static setMemoRelaunchedForTMemoImportDialogFlag(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 217
    const-string v2, "MIGRATION_PREFERENCES"

    .line 218
    const/4 v3, 0x0

    .line 217
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 219
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 220
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "IS_MEMO_RELAUNCHED_FOR_TMEMO_IMPORT_DIALOG_FLAG"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 221
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 222
    return-void
.end method

.method public static setMemoRestoringInProcessFlag(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "val"    # Z

    .prologue
    .line 88
    const-string v2, "MIGRATION_PREFERENCES"

    .line 89
    const/4 v3, 0x0

    .line 88
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 90
    .local v1, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 91
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "IS_KIES_MEMO_RESTORING_IN_PROCESS"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 92
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 93
    return-void
.end method

.method public static setSADoNotShowFlag(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 231
    const-string v2, "MIGRATION_PREFERENCES"

    .line 232
    const/4 v3, 0x0

    .line 231
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 233
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 234
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "DO_NOT_SHOW_SAMSUNG_ACCOUNT_DIALOG"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 235
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 236
    return-void
.end method

.method public static setSAName(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 175
    const-string v2, "MIGRATION_PREFERENCES"

    .line 176
    const/4 v3, 0x0

    .line 175
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 177
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 178
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "SA_NAME"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 179
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 180
    return-void
.end method

.method public static setSAVerificationStartedFlag(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 161
    const-string v2, "MIGRATION_PREFERENCES"

    .line 162
    const/4 v3, 0x0

    .line 161
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 163
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 164
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "IS_SA_VERIFICATION_STARTED"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 165
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 166
    return-void
.end method

.method public static setSyncMemoDialogState(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "val"    # I

    .prologue
    .line 116
    const-string v2, "MIGRATION_PREFERENCES"

    .line 117
    const/4 v3, 0x0

    .line 116
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 118
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 119
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "SYNC_MEMO_DIALOG_STATE"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 120
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 121
    return-void
.end method

.method public static setTMemo2RestoringInProcessFlag(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "val"    # Z

    .prologue
    .line 74
    const-string v2, "MIGRATION_PREFERENCES"

    .line 75
    const/4 v3, 0x0

    .line 74
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 76
    .local v1, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 77
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "IS_KIES_TMEMO2_RESTORING_IN_PROCESS"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 78
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 79
    return-void
.end method

.method public static setTMemoImportDialogState(Landroid/content/Context;I)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "val"    # I

    .prologue
    .line 130
    if-eqz p0, :cond_0

    .line 132
    const-string v2, "MIGRATION_PREFERENCES"

    .line 133
    const/4 v3, 0x0

    .line 132
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 134
    .local v1, "sharedPref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 135
    .local v0, "prefEditor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "TMEMO_IMPORT_DIALOG_STATE"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 136
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 138
    .end local v0    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "sharedPref":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method public static updateVerifiedSA(Landroid/content/Context;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "value"    # Z

    .prologue
    .line 147
    const-string v2, "MIGRATION_PREFERENCES"

    .line 148
    const/4 v3, 0x0

    .line 147
    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 149
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 150
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "IS_SA_VERIFIED"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 151
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 152
    return-void
.end method
