.class public Lcom/samsung/android/app/memo/MemoBaseActivity;
.super Landroid/app/Activity;
.source "MemoBaseActivity.java"

# interfaces
.implements Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected mIsDrawerOpen:Ljava/lang/Boolean;

.field protected mSession:Lcom/samsung/android/app/memo/Session;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/samsung/android/app/memo/MemoBaseActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/MemoBaseActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/MemoBaseActivity;->mSession:Lcom/samsung/android/app/memo/Session;

    .line 36
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/MemoBaseActivity;->mIsDrawerOpen:Ljava/lang/Boolean;

    .line 32
    return-void
.end method


# virtual methods
.method public closeDrawer()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public finishActionMode()V
    .locals 0

    .prologue
    .line 148
    return-void
.end method

.method public getSession()Lcom/samsung/android/app/memo/Session;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoBaseActivity;->mSession:Lcom/samsung/android/app/memo/Session;

    return-object v0
.end method

.method public hideCategoryList()V
    .locals 0

    .prologue
    .line 138
    return-void
.end method

.method public initDrawerClosing()V
    .locals 0

    .prologue
    .line 161
    return-void
.end method

.method public isDrawerClosing()Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method

.method public isNeedDrawerOpen()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoBaseActivity;->mIsDrawerOpen:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public onCategroyChanged(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "mCancel"    # Z

    .prologue
    .line 115
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    sget-object v0, Lcom/samsung/android/app/memo/MemoBaseActivity;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCreate()"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    if-eqz p1, :cond_0

    .line 51
    invoke-static {p1}, Lcom/samsung/android/app/memo/Session;->createInstanceFrom(Landroid/os/Bundle;)Lcom/samsung/android/app/memo/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/MemoBaseActivity;->mSession:Lcom/samsung/android/app/memo/Session;

    .line 53
    :cond_0
    return-void
.end method

.method public onFragmentSuicide(Ljava/lang/String;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoBaseActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoBaseActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 110
    :goto_0
    return-void

    .line 108
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/MemoBaseActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 41
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoBaseActivity;->mSession:Lcom/samsung/android/app/memo/Session;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/samsung/android/app/memo/MemoBaseActivity;->mSession:Lcom/samsung/android/app/memo/Session;

    invoke-virtual {v0, p1}, Lcom/samsung/android/app/memo/Session;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 43
    :cond_0
    return-void
.end method

.method public openContent(JLjava/lang/String;Z)V
    .locals 0
    .param p1, "memoId"    # J
    .param p3, "categoryUUID"    # Ljava/lang/String;
    .param p4, "editor"    # Z

    .prologue
    .line 100
    return-void
.end method

.method public openDrawer()V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public sendMessage(ILjava/lang/Object;)V
    .locals 2
    .param p1, "messageId"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 129
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 130
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "MESSAGE_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 131
    const-string v1, "OBJECT"

    check-cast p2, Ljava/io/Serializable;

    .end local p2    # "obj":Ljava/lang/Object;
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 132
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/app/memo/MemoBaseActivity;->setResult(ILandroid/content/Intent;)V

    .line 134
    return-void
.end method

.method public setActionBarStyle(Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;)V
    .locals 0
    .param p1, "style"    # Lcom/samsung/android/app/memo/uifragment/FragmentActionInterface$ActionBarStyle;

    .prologue
    .line 63
    return-void
.end method

.method public setActionBarTitle(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 73
    return-void
.end method

.method public setActionBarTitle(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 68
    return-void
.end method

.method public setDrawerLock(Z)V
    .locals 0
    .param p1, "lock"    # Z

    .prologue
    .line 125
    return-void
.end method

.method public setNeedDrawerOpen(Z)V
    .locals 0
    .param p1, "isOpen"    # Z

    .prologue
    .line 89
    return-void
.end method

.method public showCategoryList()V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method public showGeneralErrorMessage()V
    .locals 2

    .prologue
    .line 119
    const v0, 0x7f0b000a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 120
    return-void
.end method

.method public startDrawerClosing()V
    .locals 0

    .prologue
    .line 157
    return-void
.end method
