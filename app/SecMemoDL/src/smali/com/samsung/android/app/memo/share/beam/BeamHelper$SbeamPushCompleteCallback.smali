.class Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCompleteCallback;
.super Ljava/lang/Object;
.source "BeamHelper.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/share/beam/BeamHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SbeamPushCompleteCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCompleteCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNdefPushComplete(Landroid/nfc/NfcEvent;)V
    .locals 5
    .param p1, "arg0"    # Landroid/nfc/NfcEvent;

    .prologue
    const/4 v4, 0x0

    .line 293
    const-string v1, "BeamHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SbeamPushCompleteCallback ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCompleteCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I
    invoke-static {v3}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$1(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCompleteCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mNdefStatus:I
    invoke-static {v2}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$1(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 296
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCompleteCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-static {v1, v4}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$2(Lcom/samsung/android/app/memo/share/beam/BeamHelper;I)V

    .line 304
    :goto_0
    return-void

    .line 300
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.DIRECT_SHARE_START_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 301
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.android.directshare"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 302
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCompleteCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    # getter for: Lcom/samsung/android/app/memo/share/beam/BeamHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$3(Lcom/samsung/android/app/memo/share/beam/BeamHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 303
    iget-object v1, p0, Lcom/samsung/android/app/memo/share/beam/BeamHelper$SbeamPushCompleteCallback;->this$0:Lcom/samsung/android/app/memo/share/beam/BeamHelper;

    invoke-static {v1, v4}, Lcom/samsung/android/app/memo/share/beam/BeamHelper;->access$2(Lcom/samsung/android/app/memo/share/beam/BeamHelper;I)V

    goto :goto_0
.end method
