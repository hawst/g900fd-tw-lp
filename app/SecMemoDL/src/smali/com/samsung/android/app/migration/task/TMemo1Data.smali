.class public Lcom/samsung/android/app/migration/task/TMemo1Data;
.super Ljava/lang/Object;
.source "TMemo1Data.java"


# instance fields
.field private category:Ljava/lang/String;

.field private content:Ljava/lang/String;

.field private create_t:J

.field private modify_t:J

.field private source:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;
    .param p3, "category"    # Ljava/lang/String;
    .param p4, "source"    # Ljava/lang/String;
    .param p5, "create_t"    # J
    .param p7, "modify_t"    # J

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/TMemo1Data;->title:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/samsung/android/app/migration/task/TMemo1Data;->content:Ljava/lang/String;

    .line 14
    iput-object p3, p0, Lcom/samsung/android/app/migration/task/TMemo1Data;->category:Ljava/lang/String;

    .line 15
    iput-object p4, p0, Lcom/samsung/android/app/migration/task/TMemo1Data;->source:Ljava/lang/String;

    .line 16
    iput-wide p5, p0, Lcom/samsung/android/app/migration/task/TMemo1Data;->create_t:J

    .line 17
    iput-wide p7, p0, Lcom/samsung/android/app/migration/task/TMemo1Data;->modify_t:J

    .line 18
    return-void
.end method


# virtual methods
.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/TMemo1Data;->category:Ljava/lang/String;

    return-object v0
.end method

.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/TMemo1Data;->content:Ljava/lang/String;

    return-object v0
.end method

.method public getCreateTime()J
    .locals 2

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/samsung/android/app/migration/task/TMemo1Data;->create_t:J

    return-wide v0
.end method

.method public getModifyTime()J
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/samsung/android/app/migration/task/TMemo1Data;->modify_t:J

    return-wide v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/TMemo1Data;->source:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/TMemo1Data;->title:Ljava/lang/String;

    return-object v0
.end method
