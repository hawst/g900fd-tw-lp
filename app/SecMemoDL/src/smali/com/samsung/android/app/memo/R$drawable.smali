.class public final Lcom/samsung/android/app/memo/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/memo/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final action_mode_bg_selector:I = 0x7f020000

.field public static final action_mode_spinner_selector:I = 0x7f020001

.field public static final actionbar_check_box_animator_list:I = 0x7f020002

.field public static final card_bg_selector:I = 0x7f020003

.field public static final category_chooser_item_name_selector:I = 0x7f020004

.field public static final category_edit_bg:I = 0x7f020005

.field public static final category_fragment_list_selector:I = 0x7f020006

.field public static final category_fragment_text_selector:I = 0x7f020007

.field public static final category_header_item_selector:I = 0x7f020008

.field public static final category_list_bottom_selector:I = 0x7f020009

.field public static final category_list_item_selector:I = 0x7f02000a

.field public static final category_management_delete_btn:I = 0x7f02000b

.field public static final checked_bitmap_color_list:I = 0x7f02000c

.field public static final drawer_shadow:I = 0x7f02000d

.field public static final edit_bar_progress:I = 0x7f02000e

.field public static final edit_bar_progress_bg:I = 0x7f02000f

.field public static final edit_category_menu_item:I = 0x7f020010

.field public static final edit_toolbar_actionbaritem_background:I = 0x7f020011

.field public static final edit_toolbar_bg_center:I = 0x7f020012

.field public static final edit_toolbar_bg_center_focused:I = 0x7f020013

.field public static final edit_toolbar_bg_center_pressed:I = 0x7f020014

.field public static final edit_toolbar_bg_center_selected:I = 0x7f020015

.field public static final edit_toolbar_bg_left:I = 0x7f020016

.field public static final edit_toolbar_bg_left_focused:I = 0x7f020017

.field public static final edit_toolbar_bg_left_pressed:I = 0x7f020018

.field public static final edit_toolbar_bg_left_selected:I = 0x7f020019

.field public static final edit_toolbar_bg_right:I = 0x7f02001a

.field public static final edit_toolbar_bg_right_focused:I = 0x7f02001b

.field public static final edit_toolbar_bg_right_pressed:I = 0x7f02001c

.field public static final edit_toolbar_bg_right_selected:I = 0x7f02001d

.field public static final edit_toolbar_ic_category:I = 0x7f02001e

.field public static final edit_toolbar_ic_category_disable:I = 0x7f02001f

.field public static final edit_toolbar_ic_category_normal:I = 0x7f020020

.field public static final edit_toolbar_ic_picture:I = 0x7f020021

.field public static final edit_toolbar_ic_picture_disable:I = 0x7f020022

.field public static final edit_toolbar_ic_picture_normal:I = 0x7f020023

.field public static final edit_toolbar_ic_voice:I = 0x7f020024

.field public static final edit_toolbar_ic_voice_disable:I = 0x7f020025

.field public static final edit_toolbar_ic_voice_normal:I = 0x7f020026

.field public static final fab_btn_bg:I = 0x7f020027

.field public static final fairview_internet_folder_hover_bg_light:I = 0x7f020028

.field public static final header_press_bg:I = 0x7f020029

.field public static final ic_undobar_undo:I = 0x7f02002a

.field public static final image_not_found:I = 0x7f02002b

.field public static final images:I = 0x7f02002c

.field public static final insert_image_popup_item_selector:I = 0x7f02002d

.field public static final list_divider:I = 0x7f02002e

.field public static final memo_ab_transparent:I = 0x7f02002f

.field public static final memo_divider_ab:I = 0x7f020030

.field public static final memo_drawer_setting_bg:I = 0x7f020031

.field public static final memo_dropdown_popup:I = 0x7f020032

.field public static final memo_edit_action_item_background_focused:I = 0x7f020033

.field public static final memo_edit_action_item_background_pressed:I = 0x7f020034

.field public static final memo_edit_action_item_background_selected:I = 0x7f020035

.field public static final memo_edit_dv:I = 0x7f020036

.field public static final memo_edit_ic_menu:I = 0x7f020037

.field public static final memo_edit_ic_menu_disable:I = 0x7f020038

.field public static final memo_edit_ic_menu_normal:I = 0x7f020039

.field public static final memo_edit_title_selector:I = 0x7f02003a

.field public static final memo_edit_toolbar_bg_center_normal:I = 0x7f02003b

.field public static final memo_edit_toolbar_bg_left_normal:I = 0x7f02003c

.field public static final memo_edit_toolbar_bg_right_normal:I = 0x7f02003d

.field public static final memo_fab_add:I = 0x7f02003e

.field public static final memo_ic_ab_drawer:I = 0x7f02003f

.field public static final memo_ic_ab_drawer_fonblet:I = 0x7f020040

.field public static final memo_ic_add:I = 0x7f020041

.field public static final memo_ic_add_disable:I = 0x7f020042

.field public static final memo_ic_add_disable_fonblet:I = 0x7f020043

.field public static final memo_ic_add_fonblet:I = 0x7f020044

.field public static final memo_ic_add_normal:I = 0x7f020045

.field public static final memo_ic_add_normal_fonblet:I = 0x7f020046

.field public static final memo_ic_delete:I = 0x7f020047

.field public static final memo_ic_delete_disable:I = 0x7f020048

.field public static final memo_ic_delete_normal:I = 0x7f020049

.field public static final memo_ic_lock_normal:I = 0x7f02004a

.field public static final memo_ic_moreoverflow:I = 0x7f02004b

.field public static final memo_ic_moreoverflow_disable:I = 0x7f02004c

.field public static final memo_ic_moreoverflow_disable_fonblet:I = 0x7f02004d

.field public static final memo_ic_moreoverflow_fonblet:I = 0x7f02004e

.field public static final memo_ic_moreoverflow_normal:I = 0x7f02004f

.field public static final memo_ic_moreoverflow_normal_fonblet:I = 0x7f020050

.field public static final memo_ic_move:I = 0x7f020051

.field public static final memo_ic_move_disable:I = 0x7f020052

.field public static final memo_ic_move_normal:I = 0x7f020053

.field public static final memo_ic_print:I = 0x7f020054

.field public static final memo_ic_print_disable:I = 0x7f020055

.field public static final memo_ic_print_normal:I = 0x7f020056

.field public static final memo_ic_quick_panel:I = 0x7f020057

.field public static final memo_ic_quick_panel_pause:I = 0x7f020058

.field public static final memo_ic_quick_panel_play:I = 0x7f020059

.field public static final memo_ic_quick_panel_record:I = 0x7f02005a

.field public static final memo_ic_quick_panel_stop:I = 0x7f02005b

.field public static final memo_ic_search:I = 0x7f02005c

.field public static final memo_ic_search_disable:I = 0x7f02005d

.field public static final memo_ic_search_disable_fonblet:I = 0x7f02005e

.field public static final memo_ic_search_fonblet:I = 0x7f02005f

.field public static final memo_ic_search_normal:I = 0x7f020060

.field public static final memo_ic_search_normal_fonblet:I = 0x7f020061

.field public static final memo_ic_share_via:I = 0x7f020062

.field public static final memo_ic_share_via_disable:I = 0x7f020063

.field public static final memo_ic_share_via_normal:I = 0x7f020064

.field public static final memo_icon_category_popup_add:I = 0x7f020065

.field public static final memo_icon_voice:I = 0x7f020066

.field public static final memo_icon_voice_fonblet_hd:I = 0x7f020067

.field public static final memo_icon_voice_press_fonblet_hd:I = 0x7f020068

.field public static final memo_icon_voice_selector:I = 0x7f020069

.field public static final memo_icon_voice_selector_fonblet_hd:I = 0x7f02006a

.field public static final memo_list_card_bg:I = 0x7f02006b

.field public static final memo_list_card_bg_pressed:I = 0x7f02006c

.field public static final memo_list_card_focus_bg:I = 0x7f02006d

.field public static final memo_list_item_content_text_selector:I = 0x7f02006e

.field public static final memo_list_item_date_time_text_selector:I = 0x7f02006f

.field public static final memo_list_item_selector:I = 0x7f020070

.field public static final memo_list_item_title_text_selector:I = 0x7f020071

.field public static final memo_list_pressed:I = 0x7f020072

.field public static final memo_popup_icon_create:I = 0x7f020073

.field public static final memo_toolbar_bg:I = 0x7f020074

.field public static final memo_toolbar_bg_dv:I = 0x7f020075

.field public static final memo_view_ab_transparent:I = 0x7f020076

.field public static final memo_view_ab_transparent_shadow:I = 0x7f020077

.field public static final no_memo_widget:I = 0x7f020078

.field public static final notification_btn_bg:I = 0x7f020079

.field public static final notification_icon_bg:I = 0x7f02007a

.field public static final open_source_license_selector:I = 0x7f02007b

.field public static final quick_panel_icon_call_thumbnail:I = 0x7f02007c

.field public static final quick_panel_icon_call_thumbnail_focus:I = 0x7f02007d

.field public static final quick_panel_icon_call_thumbnail_memo:I = 0x7f02007e

.field public static final quick_panel_icon_call_thumbnail_press:I = 0x7f02007f

.field public static final quick_panel_icon_memo:I = 0x7f020080

.field public static final quick_panel_icon_mini_memo:I = 0x7f020081

.field public static final quick_panel_icon_thumbnail_memo:I = 0x7f020082

.field public static final quick_panel_memo_pause:I = 0x7f020083

.field public static final quick_panel_memo_pause_focus:I = 0x7f020084

.field public static final quick_panel_memo_pause_press:I = 0x7f020085

.field public static final quick_panel_memo_rec:I = 0x7f020086

.field public static final quick_panel_memo_rec_focus:I = 0x7f020087

.field public static final quick_panel_memo_rec_press:I = 0x7f020088

.field public static final quick_panel_memo_stop:I = 0x7f020089

.field public static final quick_panel_memo_stop_focus:I = 0x7f02008a

.field public static final quick_panel_memo_stop_press:I = 0x7f02008b

.field public static final quick_panel_music_pause_40:I = 0x7f02008c

.field public static final quick_panel_music_pause_focus_40:I = 0x7f02008d

.field public static final quick_panel_music_pause_press_40:I = 0x7f02008e

.field public static final quick_panel_music_play:I = 0x7f02008f

.field public static final quick_panel_music_play_40:I = 0x7f020090

.field public static final quick_panel_music_play_focus:I = 0x7f020091

.field public static final quick_panel_music_play_focus_40:I = 0x7f020092

.field public static final quick_panel_music_play_press:I = 0x7f020093

.field public static final quick_panel_music_play_press_40:I = 0x7f020094

.field public static final quick_panel_music_rec_40:I = 0x7f020095

.field public static final quick_panel_music_rec_focus_40:I = 0x7f020096

.field public static final quick_panel_music_rec_press_40:I = 0x7f020097

.field public static final quick_panel_player_stop_40:I = 0x7f020098

.field public static final quick_panel_player_stop_focus_40:I = 0x7f020099

.field public static final quick_panel_player_stop_press_40:I = 0x7f02009a

.field public static final quickpanel_btn:I = 0x7f02009b

.field public static final quickpanel_btn_focused:I = 0x7f02009c

.field public static final quickpanel_btn_pressed:I = 0x7f02009d

.field public static final record_2btn_left_bg_focus:I = 0x7f02009e

.field public static final record_2btn_left_bg_press:I = 0x7f02009f

.field public static final record_2btn_right_bg_focus:I = 0x7f0200a0

.field public static final record_2btn_right_bg_press:I = 0x7f0200a1

.field public static final record_bg:I = 0x7f0200a2

.field public static final record_btn_bg:I = 0x7f0200a3

.field public static final record_btn_bg_focus:I = 0x7f0200a4

.field public static final record_btn_bg_press:I = 0x7f0200a5

.field public static final record_delete_selector:I = 0x7f0200a6

.field public static final record_double_button_bg_left:I = 0x7f0200a7

.field public static final record_double_button_bg_right:I = 0x7f0200a8

.field public static final record_dv:I = 0x7f0200a9

.field public static final record_icon_cancel:I = 0x7f0200aa

.field public static final record_icon_cancel_dim:I = 0x7f0200ab

.field public static final record_icon_pause:I = 0x7f0200ac

.field public static final record_icon_play:I = 0x7f0200ad

.field public static final record_icon_rec:I = 0x7f0200ae

.field public static final record_icon_stop:I = 0x7f0200af

.field public static final record_progress_bar:I = 0x7f0200b0

.field public static final record_progress_bg:I = 0x7f0200b1

.field public static final record_single_button_bg:I = 0x7f0200b2

.field public static final select_all_selector:I = 0x7f0200b3

.field public static final selectall_bg_selector:I = 0x7f0200b4

.field public static final selector_tw_btn_radio:I = 0x7f0200b5

.field public static final spinner_mtrl_am:I = 0x7f0200b6

.field public static final stat_notify_memo_sync:I = 0x7f0200b7

.field public static final stat_notify_voicerecorder:I = 0x7f0200b8

.field public static final take_a_picture:I = 0x7f0200b9

.field public static final tw_btn_check_off_disabled_focused_holo_light:I = 0x7f0200ba

.field public static final tw_btn_check_off_disabled_holo_light:I = 0x7f0200bb

.field public static final tw_btn_check_off_focused_holo_light:I = 0x7f0200bc

.field public static final tw_btn_check_off_holo_light:I = 0x7f0200bd

.field public static final tw_btn_check_off_pressed_holo_light:I = 0x7f0200be

.field public static final tw_btn_check_on_disabled_focused_holo_light:I = 0x7f0200bf

.field public static final tw_btn_check_on_disabled_holo_light:I = 0x7f0200c0

.field public static final tw_btn_check_on_focused_holo_light:I = 0x7f0200c1

.field public static final tw_btn_check_on_holo_light:I = 0x7f0200c2

.field public static final tw_btn_check_on_pressed_holo_light:I = 0x7f0200c3

.field public static final tw_btn_check_to_on_mtrl_000:I = 0x7f0200c4

.field public static final tw_btn_check_to_on_mtrl_001:I = 0x7f0200c5

.field public static final tw_btn_check_to_on_mtrl_002:I = 0x7f0200c6

.field public static final tw_btn_check_to_on_mtrl_003:I = 0x7f0200c7

.field public static final tw_btn_check_to_on_mtrl_004:I = 0x7f0200c8

.field public static final tw_btn_check_to_on_mtrl_005:I = 0x7f0200c9

.field public static final tw_btn_check_to_on_mtrl_006:I = 0x7f0200ca

.field public static final tw_btn_check_to_on_mtrl_007:I = 0x7f0200cb

.field public static final tw_btn_check_to_on_mtrl_008:I = 0x7f0200cc

.field public static final tw_btn_check_to_on_mtrl_009:I = 0x7f0200cd

.field public static final tw_btn_check_to_on_mtrl_010:I = 0x7f0200ce

.field public static final tw_btn_check_to_on_mtrl_011:I = 0x7f0200cf

.field public static final tw_btn_check_to_on_mtrl_012:I = 0x7f0200d0

.field public static final tw_btn_check_to_on_mtrl_013:I = 0x7f0200d1

.field public static final tw_btn_check_to_on_mtrl_014:I = 0x7f0200d2

.field public static final tw_btn_check_to_on_mtrl_015:I = 0x7f0200d3

.field public static final tw_btn_default_focused_holo_light:I = 0x7f0200d4

.field public static final tw_btn_default_normal_holo_light:I = 0x7f0200d5

.field public static final tw_btn_default_pressed_holo_light:I = 0x7f0200d6

.field public static final tw_btn_delete_focused_holo_light:I = 0x7f0200d7

.field public static final tw_btn_delete_holo_light:I = 0x7f0200d8

.field public static final tw_btn_delete_pressed_holo_light:I = 0x7f0200d9

.field public static final tw_btn_radio_off_focused_holo_light:I = 0x7f0200da

.field public static final tw_btn_radio_off_holo_light:I = 0x7f0200db

.field public static final tw_btn_radio_off_pressed_holo_light:I = 0x7f0200dc

.field public static final tw_btn_radio_on_focused_holo_light:I = 0x7f0200dd

.field public static final tw_btn_radio_on_holo_light:I = 0x7f0200de

.field public static final tw_btn_radio_on_pressed_holo_light:I = 0x7f0200df

.field public static final tw_btn_radio_to_off_mtrl_015:I = 0x7f0200e0

.field public static final tw_btn_radio_to_on_mtrl_015:I = 0x7f0200e1

.field public static final tw_cab_background_top_holo_light:I = 0x7f0200e2

.field public static final tw_drawer_bg_holo_light:I = 0x7f0200e3

.field public static final tw_drawer_list_line_holo_light:I = 0x7f0200e4

.field public static final tw_ic_ab_back_holo_light:I = 0x7f0200e5

.field public static final tw_list_divider_holo_light:I = 0x7f0200e6

.field public static final tw_list_focused_holo_light:I = 0x7f0200e7

.field public static final tw_list_icon_minus_focused_holo_light:I = 0x7f0200e8

.field public static final tw_list_icon_minus_holo_light:I = 0x7f0200e9

.field public static final tw_list_icon_minus_pressed_holo_light:I = 0x7f0200ea

.field public static final tw_list_icon_reorder_holo_light:I = 0x7f0200eb

.field public static final tw_list_pressed_holo_dark:I = 0x7f0200ec

.field public static final tw_list_pressed_holo_light:I = 0x7f0200ed

.field public static final tw_list_selected_holo_dark:I = 0x7f0200ee

.field public static final tw_menu_dropdown_panel_holo_light:I = 0x7f0200ef

.field public static final tw_no_item_popup_bg_holo_light:I = 0x7f0200f0

.field public static final tw_preference_contents_list_divider_holo_light:I = 0x7f0200f1

.field public static final tw_scrollbar_holo_light:I = 0x7f0200f2

.field public static final tw_select_all_bg_holo_dark:I = 0x7f0200f3

.field public static final tw_select_all_bg_holo_light:I = 0x7f0200f4

.field public static final tw_spinner_ab_default_holo_light:I = 0x7f0200f5

.field public static final tw_spinner_ab_focused_holo_light:I = 0x7f0200f6

.field public static final tw_spinner_ab_pressed_holo_light:I = 0x7f0200f7

.field public static final tw_spinner_ab_selected_holo_light:I = 0x7f0200f8

.field public static final tw_spinner_divider_horizontal_light:I = 0x7f0200f9

.field public static final tw_spinner_mtrl_am_alpha:I = 0x7f0200fa

.field public static final tw_undo_popup_back_dark:I = 0x7f0200fb

.field public static final tw_undo_popup_back_holo_dark:I = 0x7f0200fc

.field public static final tw_undo_popup_back_holo_light:I = 0x7f0200fd

.field public static final unchecked_bitmap_color_list:I = 0x7f0200fe

.field public static final unchecked_radio_button_bitmap_color_list:I = 0x7f0200ff

.field public static final undobar:I = 0x7f020100

.field public static final undobar_button:I = 0x7f020101

.field public static final undobar_button_focused:I = 0x7f020102

.field public static final undobar_button_pressed:I = 0x7f020103

.field public static final undobar_divider:I = 0x7f020104

.field public static final voice_delete_drawable:I = 0x7f020105

.field public static final voice_memo_quickpanel_btn_pause:I = 0x7f020106

.field public static final voice_memo_quickpanel_btn_play:I = 0x7f020107

.field public static final voice_memo_quickpanel_btn_record:I = 0x7f020108

.field public static final voice_memo_quickpanel_btn_stop:I = 0x7f020109

.field public static final voice_seekbar_drawable:I = 0x7f02010a

.field public static final widget_innerline:I = 0x7f02010b

.field public static final widget_memo_preview:I = 0x7f02010c

.field public static final widget_shadow:I = 0x7f02010d

.field public static final widget_voice:I = 0x7f02010e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
