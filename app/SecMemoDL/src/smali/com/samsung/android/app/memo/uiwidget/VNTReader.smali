.class public Lcom/samsung/android/app/memo/uiwidget/VNTReader;
.super Ljava/lang/Object;
.source "VNTReader.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field DEBUG:Z

.field public TEXT_VNOTE:Ljava/lang/String;

.field private mCharSet:Ljava/lang/String;

.field private mContent:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mIntent:Landroid/content/Intent;

.field public mIsMultiVnt:Z

.field public mIsVntCorrupt:Z

.field private mMultilBodies:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mStrBulider:Ljava/lang/StringBuilder;

.field private memoItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-class v0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "i"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mStrBulider:Ljava/lang/StringBuilder;

    .line 64
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->DEBUG:Z

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContent:Ljava/lang/String;

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIntent:Landroid/content/Intent;

    .line 72
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIsMultiVnt:Z

    .line 76
    iput-boolean v1, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIsVntCorrupt:Z

    .line 78
    const-string v0, "text/x-vnote"

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TEXT_VNOTE:Ljava/lang/String;

    .line 81
    iput-object p1, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    .line 82
    iput-object p2, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIntent:Landroid/content/Intent;

    .line 84
    if-eqz p2, :cond_0

    .line 85
    invoke-direct {p0, p2}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->getVNTFileData(Landroid/content/Intent;)V

    .line 87
    :cond_0
    return-void
.end method

.method private addToList(Ljava/lang/String;)[Ljava/lang/String;
    .locals 10
    .param p1, "quotedPrintable"    # Ljava/lang/String;

    .prologue
    const/16 v9, 0xa

    .line 412
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 413
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    .line 414
    .local v4, "length":I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 415
    .local v6, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v4, :cond_1

    .line 433
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 434
    .local v2, "finalLine":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_0

    .line 435
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 437
    :cond_0
    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    .line 438
    .local v5, "lines":[Ljava/lang/String;
    return-object v5

    .line 416
    .end local v2    # "finalLine":Ljava/lang/String;
    .end local v5    # "lines":[Ljava/lang/String;
    :cond_1
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 417
    .local v1, "ch":C
    if-ne v1, v9, :cond_3

    .line 418
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 419
    new-instance v0, Ljava/lang/StringBuilder;

    .end local v0    # "builder":Ljava/lang/StringBuilder;
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 415
    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 420
    :cond_3
    const/16 v8, 0xd

    if-ne v1, v8, :cond_4

    .line 421
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 422
    new-instance v0, Ljava/lang/StringBuilder;

    .end local v0    # "builder":Ljava/lang/StringBuilder;
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 423
    .restart local v0    # "builder":Ljava/lang/StringBuilder;
    add-int/lit8 v8, v4, -0x1

    if-ge v3, v8, :cond_2

    .line 424
    add-int/lit8 v8, v3, 0x1

    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 425
    .local v7, "nextCh":C
    if-ne v7, v9, :cond_2

    .line 426
    add-int/lit8 v3, v3, 0x1

    .line 429
    goto :goto_1

    .line 430
    .end local v7    # "nextCh":C
    :cond_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private static final check(III)I
    .locals 3
    .param p0, "lowerBound"    # I
    .param p1, "upperBound"    # I
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 494
    if-lt p2, p0, :cond_0

    if-gt p2, p1, :cond_0

    .line 495
    return p2

    .line 497
    :cond_0
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "field out of bounds.  max="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " value="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static final ctoi(Ljava/lang/String;I)I
    .locals 4
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 486
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 487
    .local v0, "c":C
    const/16 v1, 0x30

    if-lt v0, v1, :cond_0

    const/16 v1, 0x39

    if-gt v0, v1, :cond_0

    .line 488
    add-int/lit8 v1, v0, -0x30

    return v1

    .line 490
    :cond_0
    new-instance v1, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected numeric character.  Got \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private encodeString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "originalString"    # Ljava/lang/String;
    .param p2, "targetCharset"    # Ljava/lang/String;

    .prologue
    .line 442
    invoke-static {p2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    .line 443
    .local v2, "charset":Ljava/nio/charset/Charset;
    invoke-virtual {v2, p1}, Ljava/nio/charset/Charset;->encode(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 444
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    new-array v1, v4, [B

    .line 445
    .local v1, "bytes":[B
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 447
    :try_start_0
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v1, p2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 450
    :goto_0
    return-object v4

    .line 448
    :catch_0
    move-exception v3

    .line 449
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v4, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    const-string v5, " encodeString "

    invoke-static {v4, v5, v3}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 450
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private getMemo(Landroid/content/Intent;)V
    .locals 11
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    const v10, 0x7f0b0013

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 167
    new-instance v4, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;-><init>(Landroid/content/Context;)V

    .line 168
    .local v4, "vm":Lcom/samsung/android/app/memo/util/vnote/VNoteManager;
    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v5

    const-string v6, "content"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 169
    invoke-virtual {p0, p1}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->readContent(Landroid/content/Intent;)V

    .line 204
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v5

    const-string v6, "file"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 171
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, p1}, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->readFile(Landroid/content/Context;Landroid/content/Intent;)Ljava/lang/StringBuilder;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mStrBulider:Ljava/lang/StringBuilder;

    .line 172
    const/4 v2, 0x0

    .line 173
    .local v2, "startOffset":I
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mStrBulider:Ljava/lang/StringBuilder;

    if-eqz v5, :cond_2

    .line 174
    :goto_1
    const/4 v5, -0x1

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mStrBulider:Ljava/lang/StringBuilder;

    const-string v7, "==\n"

    invoke-virtual {v6, v7, v2}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;I)I

    move-result v2

    if-ne v5, v2, :cond_3

    .line 179
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mStrBulider:Ljava/lang/StringBuilder;

    if-nez v5, :cond_5

    .line 180
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 181
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 182
    .local v1, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 183
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_4

    .line 184
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    .line 185
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 186
    iput-boolean v9, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIsVntCorrupt:Z

    goto :goto_0

    .line 175
    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "path":Ljava/lang/String;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_3
    add-int/lit8 v2, v2, 0x3

    .line 176
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mStrBulider:Ljava/lang/StringBuilder;

    const-string v6, "="

    invoke-virtual {v5, v2, v6}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 189
    .restart local v0    # "file":Ljava/io/File;
    .restart local v1    # "path":Ljava/lang/String;
    .restart local v3    # "uri":Landroid/net/Uri;
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    .line 190
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 191
    sput v8, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vntTag:I

    .line 192
    iput-boolean v9, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIsVntCorrupt:Z

    goto :goto_0

    .line 195
    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "path":Ljava/lang/String;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_5
    sget v5, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vntTag:I

    const/4 v6, 0x5

    if-eq v5, v6, :cond_6

    .line 196
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    .line 197
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 198
    sput v8, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vntTag:I

    .line 199
    iput-boolean v9, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIsVntCorrupt:Z

    goto/16 :goto_0

    .line 202
    :cond_6
    sput v8, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vntTag:I

    goto/16 :goto_0
.end method

.method private getMemoItemCount()I
    .locals 4

    .prologue
    .line 131
    new-instance v1, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;

    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;-><init>(Landroid/content/Context;)V

    .line 132
    .local v1, "vm":Lcom/samsung/android/app/memo/util/vnote/VNoteManager;
    const/4 v0, 0x0

    .line 133
    .local v0, "itemCount":I
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 134
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 135
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TEXT_VNOTE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 136
    iget-object v2, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIntent:Landroid/content/Intent;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->getItemCount(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    .line 139
    :cond_0
    return v0
.end method

.method private getVNTFileData(Landroid/content/Intent;)V
    .locals 5
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 90
    const-string v1, ""

    .line 91
    .local v1, "sFilepath":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 92
    .local v2, "uri":Landroid/net/Uri;
    if-nez v2, :cond_0

    .line 93
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .end local v2    # "uri":Landroid/net/Uri;
    check-cast v2, Landroid/net/Uri;

    .line 94
    .restart local v2    # "uri":Landroid/net/Uri;
    :cond_0
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 95
    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "file"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 96
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 97
    const-string v3, ".vnt"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, ".VNT"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 98
    const-string v3, ".Vnt"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 99
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->readVntFile()V

    .line 108
    :cond_2
    :goto_0
    return-void

    .line 100
    :cond_3
    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "content"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 101
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/samsung/android/app/memo/MemoDataHandler;->getContentName(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "fileName":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    sget-object v4, Lcom/samsung/android/app/migration/utils/Utils;->SNB_TMP_FOLDER_PATH:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 103
    iget-object v3, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    invoke-static {v3, v2, v1}, Lcom/samsung/android/app/memo/util/FileHelper;->writeFileFromURI(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V

    .line 104
    invoke-virtual {p0}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->readVntFile()V

    .line 105
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/samsung/android/app/memo/util/FileHelper;->deleteFileItem(Ljava/io/File;)V

    goto :goto_0
.end method

.method private getVntData(Landroid/content/Intent;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "i"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    new-instance v4, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;

    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;-><init>(Landroid/content/Context;)V

    .line 144
    .local v4, "vm":Lcom/samsung/android/app/memo/util/vnote/VNoteManager;
    const/4 v3, 0x0

    .line 145
    .local v3, "uri":Landroid/net/Uri;
    const-string v5, "android.intent.extra.STREAM"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 146
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "android.intent.extra.STREAM"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "uri":Landroid/net/Uri;
    check-cast v3, Landroid/net/Uri;

    .line 151
    .restart local v3    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    .line 152
    :try_start_0
    invoke-virtual {v4, v3}, Lcom/samsung/android/app/memo/util/vnote/VNoteManager;->decodeVNote(Landroid/net/Uri;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mMultilBodies:Ljava/util/ArrayList;

    .line 153
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mMultilBodies:Ljava/util/ArrayList;

    invoke-direct {p0, v5}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->setTokeningBody(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 154
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->memoItems:Ljava/util/ArrayList;

    .line 155
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-nez v6, :cond_3

    .line 163
    .end local v1    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    :goto_2
    iget-object v5, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->memoItems:Ljava/util/ArrayList;

    return-object v5

    .line 147
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 148
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    goto :goto_0

    .line 155
    .restart local v1    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    :try_start_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 156
    .local v2, "str":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->setVntContent(Ljava/lang/StringBuilder;)V

    .line 157
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->memoItems:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContent:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 160
    .end local v1    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "str":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Ljava/io/IOException;
    sget-object v5, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    const-string v6, " getVntData "

    invoke-static {v5, v6, v0}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private handleOneValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 16
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "targetCharset"    # Ljava/lang/String;
    .param p3, "encoding"    # Ljava/lang/String;

    .prologue
    .line 347
    const/4 v10, 0x0

    .line 348
    .local v10, "nextCh":C
    if-eqz p3, :cond_6

    .line 349
    const-string v12, "QUOTED-PRINTABLE"

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 351
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 352
    .local v1, "builder":Ljava/lang/StringBuilder;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v7

    .line 353
    .local v7, "length":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-lt v6, v7, :cond_0

    .line 367
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 370
    .local v11, "quotedPrintable":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->addToList(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 372
    .local v9, "lines":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    .end local v1    # "builder":Ljava/lang/StringBuilder;
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 373
    .restart local v1    # "builder":Ljava/lang/StringBuilder;
    array-length v13, v9

    const/4 v12, 0x0

    :goto_1
    if-lt v12, v13, :cond_4

    .line 381
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, "UTF-8"

    invoke-virtual {v12, v13}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 387
    .local v2, "bytes":[B
    :goto_2
    :try_start_1
    invoke-static {v2}, Lorg/apache/commons/codec/net/QuotedPrintableCodec;->decodeQuotedPrintable([B)[B
    :try_end_1
    .catch Lorg/apache/commons/codec/DecoderException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 394
    :try_start_2
    new-instance v12, Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-direct {v12, v2, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_2

    .line 404
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "bytes":[B
    .end local v6    # "i":I
    .end local v7    # "length":I
    .end local v9    # "lines":[Ljava/lang/String;
    .end local v11    # "quotedPrintable":Ljava/lang/String;
    :goto_3
    return-object v12

    .line 354
    .restart local v1    # "builder":Ljava/lang/StringBuilder;
    .restart local v6    # "i":I
    .restart local v7    # "length":I
    :cond_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 355
    .local v3, "ch":C
    const/16 v12, 0x3d

    if-ne v3, v12, :cond_3

    add-int/lit8 v12, v7, -0x1

    if-ge v6, v12, :cond_3

    .line 356
    add-int/lit8 v12, v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/lang/String;->charAt(I)C

    move-result v10

    .line 357
    const/16 v12, 0x20

    if-eq v10, v12, :cond_1

    const/16 v12, 0x9

    if-ne v10, v12, :cond_3

    .line 359
    :cond_1
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 360
    add-int/lit8 v6, v6, 0x1

    .line 353
    :cond_2
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 364
    :cond_3
    const/16 v12, 0x3d

    if-eq v10, v12, :cond_2

    .line 365
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 373
    .end local v3    # "ch":C
    .restart local v9    # "lines":[Ljava/lang/String;
    .restart local v11    # "quotedPrintable":Ljava/lang/String;
    :cond_4
    aget-object v8, v9, v12

    .line 374
    .local v8, "line":Ljava/lang/String;
    const-string v14, "="

    invoke-virtual {v8, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 375
    const/4 v14, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    invoke-virtual {v8, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 377
    :cond_5
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 382
    .end local v8    # "line":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 383
    .local v5, "e1":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .restart local v2    # "bytes":[B
    goto :goto_2

    .line 388
    .end local v5    # "e1":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v4

    .line 389
    .local v4, "e":Lorg/apache/commons/codec/DecoderException;
    sget-object v12, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    const-string v13, " handleOneValue "

    invoke-static {v12, v13, v4}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 390
    const-string v12, ""

    goto :goto_3

    .line 395
    .end local v4    # "e":Lorg/apache/commons/codec/DecoderException;
    :catch_2
    move-exception v4

    .line 396
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v12, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    const-string v13, " handleOneValue "

    invoke-static {v12, v13, v4}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 397
    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v2}, Ljava/lang/String;-><init>([B)V

    goto :goto_3

    .line 401
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "bytes":[B
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    .end local v6    # "i":I
    .end local v7    # "length":I
    .end local v9    # "lines":[Ljava/lang/String;
    .end local v11    # "quotedPrintable":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->DEBUG:Z

    if-eqz v12, :cond_7

    .line 402
    sget-object v12, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    .line 403
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "HandleOneValue completed, result is : "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->encodeString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 402
    invoke-static {v12, v13}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    :cond_7
    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->encodeString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_3
.end method

.method public static parseDateTime(Ljava/lang/String;Ljava/util/Calendar;)Z
    .locals 12
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "cal"    # Ljava/util/Calendar;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/16 v11, 0xd

    const/16 v10, 0xc

    const/16 v9, 0xb

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 455
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 456
    .local v2, "len":I
    const/16 v5, 0xf

    if-eq v2, v5, :cond_0

    const/16 v5, 0x10

    if-ne v2, v5, :cond_4

    :cond_0
    const/16 v5, 0x8

    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x54

    if-ne v5, v6, :cond_4

    move v0, v3

    .line 457
    .local v0, "dateTime":Z
    :goto_0
    const/16 v5, 0x8

    if-ne v2, v5, :cond_5

    move v1, v3

    .line 458
    .local v1, "justDate":Z
    :goto_1
    if-nez v0, :cond_1

    if-eqz v1, :cond_8

    .line 459
    :cond_1
    invoke-virtual {p1}, Ljava/util/Calendar;->clear()V

    .line 460
    invoke-static {p0, v4}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->ctoi(Ljava/lang/String;I)I

    move-result v5

    mul-int/lit16 v5, v5, 0x3e8

    invoke-static {p0, v3}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->ctoi(Ljava/lang/String;I)I

    move-result v6

    mul-int/lit8 v6, v6, 0x64

    add-int/2addr v5, v6

    const/4 v6, 0x2

    invoke-static {p0, v6}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->ctoi(Ljava/lang/String;I)I

    move-result v6

    mul-int/lit8 v6, v6, 0xa

    add-int/2addr v5, v6

    .line 461
    const/4 v6, 0x3

    invoke-static {p0, v6}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->ctoi(Ljava/lang/String;I)I

    move-result v6

    add-int/2addr v5, v6

    .line 460
    invoke-virtual {p1, v3, v5}, Ljava/util/Calendar;->set(II)V

    .line 462
    const/4 v5, 0x2

    const/4 v6, 0x4

    invoke-static {p0, v6}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->ctoi(Ljava/lang/String;I)I

    move-result v6

    mul-int/lit8 v6, v6, 0xa

    const/4 v7, 0x5

    invoke-static {p0, v7}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->ctoi(Ljava/lang/String;I)I

    move-result v7

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, -0x1

    invoke-static {v4, v9, v6}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->check(III)I

    move-result v6

    invoke-virtual {p1, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 463
    const/4 v5, 0x5

    const/16 v6, 0x1f

    const/4 v7, 0x6

    invoke-static {p0, v7}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->ctoi(Ljava/lang/String;I)I

    move-result v7

    mul-int/lit8 v7, v7, 0xa

    const/4 v8, 0x7

    invoke-static {p0, v8}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->ctoi(Ljava/lang/String;I)I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v3, v6, v7}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->check(III)I

    move-result v6

    invoke-virtual {p1, v5, v6}, Ljava/util/Calendar;->set(II)V

    .line 464
    if-eqz v0, :cond_2

    .line 465
    const/16 v5, 0x17

    const/16 v6, 0x9

    invoke-static {p0, v6}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->ctoi(Ljava/lang/String;I)I

    move-result v6

    mul-int/lit8 v6, v6, 0xa

    const/16 v7, 0xa

    invoke-static {p0, v7}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->ctoi(Ljava/lang/String;I)I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->check(III)I

    move-result v5

    invoke-virtual {p1, v9, v5}, Ljava/util/Calendar;->set(II)V

    .line 466
    const/16 v5, 0x3b

    invoke-static {p0, v9}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->ctoi(Ljava/lang/String;I)I

    move-result v6

    mul-int/lit8 v6, v6, 0xa

    invoke-static {p0, v10}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->ctoi(Ljava/lang/String;I)I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->check(III)I

    move-result v5

    invoke-virtual {p1, v10, v5}, Ljava/util/Calendar;->set(II)V

    .line 467
    const/16 v5, 0x3b

    invoke-static {p0, v11}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->ctoi(Ljava/lang/String;I)I

    move-result v6

    mul-int/lit8 v6, v6, 0xa

    const/16 v7, 0xe

    invoke-static {p0, v7}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->ctoi(Ljava/lang/String;I)I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4, v5, v6}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->check(III)I

    move-result v5

    invoke-virtual {p1, v11, v5}, Ljava/util/Calendar;->set(II)V

    .line 469
    :cond_2
    if-eqz v1, :cond_6

    .line 470
    invoke-virtual {p1, v9, v4}, Ljava/util/Calendar;->set(II)V

    .line 471
    invoke-virtual {p1, v10, v4}, Ljava/util/Calendar;->set(II)V

    .line 472
    invoke-virtual {p1, v11, v4}, Ljava/util/Calendar;->set(II)V

    .line 479
    :cond_3
    :goto_2
    return v3

    .end local v0    # "dateTime":Z
    .end local v1    # "justDate":Z
    :cond_4
    move v0, v4

    .line 456
    goto/16 :goto_0

    .restart local v0    # "dateTime":Z
    :cond_5
    move v1, v4

    .line 457
    goto/16 :goto_1

    .line 475
    .restart local v1    # "justDate":Z
    :cond_6
    const/16 v5, 0xf

    if-ne v2, v5, :cond_7

    move v3, v4

    .line 476
    goto :goto_2

    .line 478
    :cond_7
    const/16 v4, 0xf

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x5a

    if-eq v4, v5, :cond_3

    .line 482
    :cond_8
    new-instance v3, Ljava/lang/Exception;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid time (expected YYYYMMDDThhmmssZ? got \'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\')."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private setTokeningBody(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "body":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .line 501
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 505
    .local v4, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 506
    .local v2, "isDecreated":Z
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 507
    .local v5, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 508
    .local v0, "bodySize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v0, :cond_0

    .line 531
    return-object v4

    .line 509
    :cond_0
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 510
    .local v3, "line":Ljava/lang/String;
    const-string v6, ":"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 511
    iget-boolean v6, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->DEBUG:Z

    if-eqz v6, :cond_1

    .line 512
    sget-object v6, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "VNT PARSE - key \':\' contains in : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    :cond_1
    const-string v6, "DCREATED"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 514
    const/4 v2, 0x1

    .line 515
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 516
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 517
    iget-boolean v6, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->DEBUG:Z

    if-eqz v6, :cond_2

    .line 518
    sget-object v6, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "item added list, content is : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/android/app/memo/util/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    invoke-virtual {v5, v9, v6}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 508
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 521
    :cond_3
    const/4 v2, 0x0

    .line 522
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v3, v9, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 524
    :cond_4
    if-eqz v2, :cond_5

    .line 525
    const/4 v2, 0x0

    .line 526
    goto :goto_1

    .line 527
    :cond_5
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method


# virtual methods
.method public checkPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 265
    const-string v1, "BEGIN:VNOTE"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object p1, v0

    .line 291
    .end local p1    # "prefix":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 267
    .restart local p1    # "prefix":Ljava/lang/String;
    :cond_1
    const-string v1, "VERSION:1.1"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object p1, v0

    .line 268
    goto :goto_0

    .line 269
    :cond_2
    const-string v1, "BODY"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 270
    invoke-static {p1}, Lcom/samsung/android/app/memo/util/vnote/pim/VNote;->vNoteBodyParsing(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 271
    :cond_3
    const-string v1, "DCREATED:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 273
    const-string v1, "X-IRMC-LUID"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    move-object p1, v0

    .line 274
    goto :goto_0

    .line 275
    :cond_4
    const-string v1, "SUMMARY"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    move-object p1, v0

    .line 276
    goto :goto_0

    .line 277
    :cond_5
    const-string v1, "CATEGORIES"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    move-object p1, v0

    .line 278
    goto :goto_0

    .line 279
    :cond_6
    const-string v1, "CLASS"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    move-object p1, v0

    .line 280
    goto :goto_0

    .line 282
    :cond_7
    const-string v1, "LAST-MODIFIED:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    move-object p1, v0

    .line 283
    goto :goto_0

    .line 284
    :cond_8
    const-string v1, "END:VNOTE"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    move-object p1, v0

    .line 285
    goto :goto_0

    .line 286
    :cond_9
    const-string v1, "X-SH-CATEGORIES:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    move-object p1, v0

    .line 287
    goto :goto_0

    .line 288
    :cond_a
    const-string v1, "X-DCM-DATALINKID"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object p1, v0

    .line 289
    goto :goto_0
.end method

.method public getCharSet(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x8

    .line 333
    const-string v0, "UTF-8"

    .line 335
    .local v0, "charset":Ljava/lang/String;
    const-string v2, "CHARSET="

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 336
    const/16 v2, 0x3b

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 337
    .local v1, "sep":I
    if-ge v1, v3, :cond_0

    .line 338
    const/16 v2, 0x3a

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 340
    :cond_0
    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 343
    .end local v1    # "sep":I
    :cond_1
    return-object v0
.end method

.method public getVntText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContent:Ljava/lang/String;

    return-object v0
.end method

.method public readContent(Landroid/content/Intent;)V
    .locals 14
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 207
    const-string v11, "ics"

    invoke-virtual {p1, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 208
    .local v4, "data":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 210
    .local v0, "arrString":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v4, :cond_3

    .line 211
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    .line 212
    .local v3, "content":Landroid/net/Uri;
    if-eqz v3, :cond_3

    .line 213
    const/4 v8, 0x0

    .line 215
    .local v8, "is":Ljava/io/InputStream;
    :try_start_0
    iget-object v11, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    invoke-virtual {v11, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v8

    .line 216
    new-instance v7, Ljava/io/InputStreamReader;

    invoke-direct {v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 217
    .local v7, "ir":Ljava/io/InputStreamReader;
    new-instance v2, Ljava/io/BufferedReader;

    invoke-direct {v2, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 219
    .local v2, "br":Ljava/io/BufferedReader;
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    .local v10, "tmp":Ljava/lang/String;
    if-nez v10, :cond_5

    .line 227
    if-eqz v2, :cond_1

    .line 228
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V

    .line 230
    :cond_1
    if-eqz v7, :cond_2

    .line 231
    invoke-virtual {v7}, Ljava/io/InputStreamReader;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    :cond_2
    if-eqz v8, :cond_3

    .line 240
    :try_start_1
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 248
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v3    # "content":Landroid/net/Uri;
    .end local v7    # "ir":Ljava/io/InputStreamReader;
    .end local v8    # "is":Ljava/io/InputStream;
    .end local v10    # "tmp":Ljava/lang/String;
    :cond_3
    :goto_1
    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v11

    const-string v12, "content"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 249
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 250
    .local v1, "arrStringSize":I
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_2
    if-lt v9, v1, :cond_7

    .line 257
    iget-object v11, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mStrBulider:Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    if-nez v11, :cond_4

    .line 262
    .end local v1    # "arrStringSize":I
    .end local v9    # "j":I
    :cond_4
    return-void

    .line 220
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "content":Landroid/net/Uri;
    .restart local v7    # "ir":Ljava/io/InputStreamReader;
    .restart local v8    # "is":Ljava/io/InputStream;
    .restart local v10    # "tmp":Ljava/lang/String;
    :cond_5
    :try_start_2
    invoke-virtual {p0, v10}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->checkPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 221
    invoke-virtual {p0, v10}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->checkPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v11

    const-string v12, "content"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 223
    iget-object v11, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mMultilBodies:Ljava/util/ArrayList;

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->checkPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 233
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v7    # "ir":Ljava/io/InputStreamReader;
    .end local v10    # "tmp":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 234
    .local v5, "fnfe":Ljava/io/FileNotFoundException;
    :try_start_3
    sget-object v11, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    const-string v12, " readContent "

    invoke-static {v11, v12, v5}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 238
    if-eqz v8, :cond_3

    .line 240
    :try_start_4
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 241
    :catch_1
    move-exception v6

    .line 242
    .local v6, "ioe":Ljava/io/IOException;
    sget-object v11, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    const-string v12, " readContent "

    invoke-static {v11, v12, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 235
    .end local v5    # "fnfe":Ljava/io/FileNotFoundException;
    .end local v6    # "ioe":Ljava/io/IOException;
    :catch_2
    move-exception v6

    .line 236
    .restart local v6    # "ioe":Ljava/io/IOException;
    :try_start_5
    sget-object v11, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    const-string v12, " readContent "

    invoke-static {v11, v12, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 238
    if-eqz v8, :cond_3

    .line 240
    :try_start_6
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_1

    .line 241
    :catch_3
    move-exception v6

    .line 242
    sget-object v11, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    const-string v12, " readContent "

    invoke-static {v11, v12, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 237
    .end local v6    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    .line 238
    if-eqz v8, :cond_6

    .line 240
    :try_start_7
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 245
    :cond_6
    :goto_3
    throw v11

    .line 241
    :catch_4
    move-exception v6

    .line 242
    .restart local v6    # "ioe":Ljava/io/IOException;
    sget-object v12, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    const-string v13, " readContent "

    invoke-static {v12, v13, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 241
    .end local v6    # "ioe":Ljava/io/IOException;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    .restart local v7    # "ir":Ljava/io/InputStreamReader;
    .restart local v10    # "tmp":Ljava/lang/String;
    :catch_5
    move-exception v6

    .line 242
    .restart local v6    # "ioe":Ljava/io/IOException;
    sget-object v11, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    const-string v12, " readContent "

    invoke-static {v11, v12, v6}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 251
    .end local v2    # "br":Ljava/io/BufferedReader;
    .end local v3    # "content":Landroid/net/Uri;
    .end local v6    # "ioe":Ljava/io/IOException;
    .end local v7    # "ir":Ljava/io/InputStreamReader;
    .end local v8    # "is":Ljava/io/InputStream;
    .end local v10    # "tmp":Ljava/lang/String;
    .restart local v1    # "arrStringSize":I
    .restart local v9    # "j":I
    :cond_7
    iget-object v12, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mStrBulider:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    if-ge v9, v11, :cond_8

    .line 253
    iget-object v11, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mStrBulider:Ljava/lang/StringBuilder;

    const/16 v12, 0xa

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 250
    :cond_8
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2
.end method

.method public readVntFile()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 112
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TEXT_VNOTE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "application/octet-stream"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->getMemoItemCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 115
    iput-boolean v2, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIsMultiVnt:Z

    .line 116
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIntent:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->getVntData(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->memoItems:Ljava/util/ArrayList;

    .line 124
    :cond_1
    :goto_0
    return-void

    .line 118
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIsMultiVnt:Z

    .line 119
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIntent:Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->getMemo(Landroid/content/Intent;)V

    .line 120
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mIsVntCorrupt:Z

    if-nez v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mStrBulider:Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->setVntContent(Ljava/lang/StringBuilder;)V

    goto :goto_0
.end method

.method public setVntContent(Ljava/lang/StringBuilder;)V
    .locals 9
    .param p1, "strBulider"    # Ljava/lang/StringBuilder;

    .prologue
    const/4 v7, 0x0

    .line 296
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    .line 297
    .local v5, "timezone":Ljava/util/TimeZone;
    const/4 v0, 0x0

    .line 298
    .local v0, "content":Ljava/lang/String;
    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-direct {v2, v5}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 299
    .local v2, "local":Ljava/util/Calendar;
    const-string v6, "DCREATED:"

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 301
    .local v3, "sep":I
    if-ltz v3, :cond_3

    .line 302
    if-nez v3, :cond_2

    .line 303
    const/16 v6, 0x9

    const/16 v7, 0x18

    invoke-virtual {p1, v6, v7}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 304
    .local v4, "time":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x9

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 311
    :goto_0
    :try_start_0
    invoke-static {v4, v2}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->parseDateTime(Ljava/lang/String;Ljava/util/Calendar;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 320
    .end local v4    # "time":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->getCharSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mCharSet:Ljava/lang/String;

    .line 321
    const/16 v6, 0x3a

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 322
    if-ltz v3, :cond_0

    .line 323
    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 326
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mCharSet:Ljava/lang/String;

    const-string v7, "QUOTED-PRINTABLE"

    invoke-direct {p0, v0, v6, v7}, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->handleOneValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContent:Ljava/lang/String;

    .line 327
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContent:Ljava/lang/String;

    if-eqz v6, :cond_1

    .line 328
    iget-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContent:Ljava/lang/String;

    const-string v7, "\r\n"

    const-string v8, "\n"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->mContent:Ljava/lang/String;

    .line 330
    :cond_1
    return-void

    .line 306
    :cond_2
    add-int/lit8 v6, v3, 0x9

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 307
    .restart local v4    # "time":Ljava/lang/String;
    invoke-virtual {p1, v7, v3}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 312
    :catch_0
    move-exception v1

    .line 313
    .local v1, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/samsung/android/app/memo/uiwidget/VNTReader;->TAG:Ljava/lang/String;

    const-string v7, " setVntContent "

    invoke-static {v6, v7, v1}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 317
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "time":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
