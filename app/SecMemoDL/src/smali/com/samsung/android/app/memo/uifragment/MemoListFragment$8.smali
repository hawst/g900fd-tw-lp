.class Lcom/samsung/android/app/memo/uifragment/MemoListFragment$8;
.super Ljava/lang/Object;
.source "MemoListFragment.java"

# interfaces
.implements Lcom/samsung/android/app/memo/uifragment/CategoryNamingDialog$OnDialogResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

.field mListViewItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/app/memo/uifragment/CategoryListItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/memo/uifragment/MemoListFragment;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    .line 1102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$8;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    .line 1104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$8;->mListViewItems:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public onPositiveBtnClick(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "categoryName"    # Ljava/lang/String;

    .prologue
    .line 1107
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$8;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    if-nez v0, :cond_0

    .line 1108
    new-instance v0, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    iget-object v1, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$8;->this$0:Lcom/samsung/android/app/memo/uifragment/MemoListFragment;

    invoke-virtual {v1}, Lcom/samsung/android/app/memo/uifragment/MemoListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 1109
    const v2, 0x7f04000c

    iget-object v3, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$8;->mListViewItems:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1108
    iput-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$8;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    .line 1111
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$8;->mListViewItems:Ljava/util/List;

    new-instance v1, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1, p2}, Lcom/samsung/android/app/memo/uifragment/CategoryListItem;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1112
    iget-object v0, p0, Lcom/samsung/android/app/memo/uifragment/MemoListFragment$8;->mCategoryManagementArrayAdapter:Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/app/memo/uifragment/CategoryManagementArrayAdapter;->notifyDataSetChanged()V

    .line 1113
    return-void
.end method
