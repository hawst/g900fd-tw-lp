.class public Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;
.super Ljava/lang/Thread;
.source "ConvertTMemo2Task.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ConvertTMemo2Task"

.field private static instance:Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;


# instance fields
.field private context:Landroid/app/Activity;

.field private ctx:Landroid/content/Context;

.field private dialogMsg:Ljava/lang/String;

.field private filesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private isFromKies:Z

.field private isShown:Z

.field private progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;Z)V
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;
    .param p2, "ctx"    # Landroid/content/Context;
    .param p3, "dialogMsg"    # Ljava/lang/String;
    .param p5, "removeSnbFiles"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p4, "filesList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isShown:Z

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    .line 65
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    .line 66
    iput-object p2, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->ctx:Landroid/content/Context;

    .line 67
    iput-object p3, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->dialogMsg:Ljava/lang/String;

    .line 68
    iput-object p4, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->filesList:Ljava/util/ArrayList;

    .line 69
    iput-boolean p5, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isFromKies:Z

    .line 70
    iget-boolean v0, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isFromKies:Z

    if-eqz v0, :cond_0

    .line 71
    if-nez p1, :cond_1

    .end local p2    # "ctx":Landroid/content/Context;
    :goto_0
    const/4 v0, 0x1

    invoke-static {p2, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemo2RestoringInProcessFlag(Landroid/content/Context;Z)V

    .line 72
    :cond_0
    sput-object p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->instance:Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;

    .line 73
    return-void

    .restart local p2    # "ctx":Landroid/content/Context;
    :cond_1
    move-object p2, p1

    .line 71
    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;)Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;Z)V
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isShown:Z

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    return-void
.end method

.method public static getInstance()Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->instance:Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;

    return-object v0
.end method

.method private isContextAlive(Landroid/app/Activity;)Z
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 83
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isContextResumed(Landroid/app/Activity;)Z
    .locals 1
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isContextAlive(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const/4 v0, 0x1

    .line 79
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showDialog()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isContextResumed(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    new-instance v1, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task$1;-><init>(Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 100
    :cond_0
    return-void
.end method

.method private showToast(I)V
    .locals 2
    .param p1, "resId"    # I

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isContextResumed(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    new-instance v1, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task$2;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task$2;-><init>(Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 110
    :cond_0
    return-void
.end method

.method private updatePwdDialog(Ljava/lang/String;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 177
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    const v3, 0x7f0b004b

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 178
    .local v1, "pwdDialogTitle":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    .line 179
    invoke-static {p1}, Lcom/samsung/android/app/migration/utils/Utils;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 178
    invoke-static {v2, v1, v3}, Lcom/samsung/android/app/migration/dialog/PasswordDialog;->newInstance(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/app/migration/dialog/PasswordDialog;

    move-result-object v0

    .line 180
    .local v0, "pwdDialog":Lcom/samsung/android/app/migration/dialog/PasswordDialog;
    iget-object v2, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    new-instance v3, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task$4;

    invoke-direct {v3, p0, v0}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task$4;-><init>(Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;Lcom/samsung/android/app/migration/dialog/PasswordDialog;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 188
    :try_start_0
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v2

    iget-object v3, v2, Lcom/samsung/android/app/migration/Migration;->passwordLock:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :try_start_1
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v2

    iget-object v2, v2, Lcom/samsung/android/app/migration/Migration;->passwordLock:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V

    .line 188
    monitor-exit v3

    .line 193
    :goto_0
    return-void

    .line 188
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 191
    :catch_0
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v8, 0x0

    .line 114
    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    if-nez v7, :cond_5

    .line 115
    iget-object v9, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->dialogMsg:Ljava/lang/String;

    iget-boolean v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isFromKies:Z

    if-eqz v7, :cond_4

    const/4 v7, 0x1

    :goto_0
    invoke-static {v12, v9, v7}, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    .line 116
    invoke-direct {p0}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->showDialog()V

    .line 119
    :cond_0
    :goto_1
    const/4 v5, 0x0

    .line 120
    .local v5, "snbFilesCounter":I
    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->filesList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 121
    .local v6, "snbFilesTotalNumber":I
    const/4 v4, 0x0

    .local v4, "skipFile":Z
    const/4 v0, 0x0

    .line 122
    .local v0, "continueKiesRestore":Z
    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->filesList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_6

    .line 153
    :goto_3
    iget-boolean v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isShown:Z

    if-eqz v7, :cond_11

    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    invoke-direct {p0, v7}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isContextAlive(Landroid/app/Activity;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 154
    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    new-instance v9, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task$3;

    invoke-direct {v9, p0}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task$3;-><init>(Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;)V

    invoke-virtual {v7, v9}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 168
    :goto_4
    if-nez v0, :cond_2

    .line 169
    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    if-nez v7, :cond_12

    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->ctx:Landroid/content/Context;

    :goto_5
    invoke-static {v7, v8}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemo2RestoringInProcessFlag(Landroid/content/Context;Z)V

    .line 170
    const v7, 0x7f0b0054

    invoke-direct {p0, v7}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->showToast(I)V

    .line 172
    :cond_2
    iget-boolean v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isFromKies:Z

    if-nez v7, :cond_3

    .line 173
    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    if-nez v7, :cond_13

    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->ctx:Landroid/content/Context;

    :goto_6
    invoke-static {v7, v8}, Lcom/samsung/android/app/migration/utils/SharedPref;->setLocalMemosMigrationInProcessFlag(Landroid/content/Context;Z)V

    .line 174
    :cond_3
    return-void

    .end local v0    # "continueKiesRestore":Z
    .end local v4    # "skipFile":Z
    .end local v5    # "snbFilesCounter":I
    .end local v6    # "snbFilesTotalNumber":I
    :cond_4
    move v7, v8

    .line 115
    goto :goto_0

    .line 117
    :cond_5
    iget-boolean v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isShown:Z

    if-nez v7, :cond_0

    .line 118
    invoke-direct {p0}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->showDialog()V

    goto :goto_1

    .line 122
    .restart local v0    # "continueKiesRestore":Z
    .restart local v4    # "skipFile":Z
    .restart local v5    # "snbFilesCounter":I
    .restart local v6    # "snbFilesTotalNumber":I
    :cond_6
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 123
    .local v1, "path":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 125
    invoke-static {v1}, Lcom/samsung/android/app/memo/util/FileHelper;->isEncrypted(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 126
    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    invoke-direct {p0, v7}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isContextResumed(Landroid/app/Activity;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 127
    invoke-direct {p0, v1}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->updatePwdDialog(Ljava/lang/String;)V

    .line 131
    :cond_7
    :goto_7
    invoke-static {}, Lcom/samsung/android/app/migration/MemoConverter;->getInstance()Lcom/samsung/android/app/migration/MemoConverter;

    move-result-object v10

    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    if-nez v7, :cond_d

    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->ctx:Landroid/content/Context;

    :goto_8
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v11

    invoke-virtual {v11}, Lcom/samsung/android/app/migration/Migration;->getPassword()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v7, v1, v11}, Lcom/samsung/android/app/migration/MemoConverter;->convertSNB(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    .line 132
    .local v2, "res":J
    invoke-static {}, Lcom/samsung/android/app/migration/Migration;->getInstance()Lcom/samsung/android/app/migration/Migration;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/app/migration/Migration;->resetPassword()V

    .line 133
    const-wide/16 v10, -0x1

    cmp-long v7, v2, v10

    if-nez v7, :cond_e

    .line 134
    const-string v7, "ConvertTMemo2Task"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Error occurred when converting SNB file: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_8
    :goto_9
    iget-boolean v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isFromKies:Z

    if-nez v7, :cond_9

    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    if-eqz v7, :cond_9

    .line 146
    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    add-int/lit8 v5, v5, 0x1

    mul-int/lit8 v10, v5, 0x64

    div-int/2addr v10, v6

    invoke-virtual {v7, v10}, Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;->setProgress(I)V

    .line 147
    :cond_9
    iget-boolean v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isFromKies:Z

    if-eqz v7, :cond_a

    if-nez v4, :cond_a

    .line 148
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lcom/samsung/android/app/memo/util/FileHelper;->deleteFileItem(Ljava/io/File;)V

    .line 149
    :cond_a
    if-eqz v4, :cond_b

    .line 150
    const/4 v0, 0x1

    .line 151
    :cond_b
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 129
    .end local v2    # "res":J
    :cond_c
    const/4 v4, 0x1

    goto :goto_7

    .line 131
    :cond_d
    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    goto :goto_8

    .line 135
    .restart local v2    # "res":J
    :cond_e
    const-wide/16 v10, -0x3

    cmp-long v7, v2, v10

    if-nez v7, :cond_10

    .line 136
    const-string v7, "ConvertTMemo2Task"

    const-string v10, "Wrong password."

    invoke-static {v7, v10}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    if-nez v4, :cond_f

    .line 138
    new-instance v7, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/Utils;->getFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/samsung/android/app/migration/utils/Utils;->getTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lcom/samsung/android/app/memo/util/FileHelper;->deleteFileItem(Ljava/io/File;)V

    .line 139
    :cond_f
    const v7, 0x7f0b004a

    invoke-direct {p0, v7}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->showToast(I)V

    goto :goto_9

    .line 140
    :cond_10
    const-wide/16 v10, -0x4

    cmp-long v7, v2, v10

    if-nez v7, :cond_8

    .line 141
    const-string v7, "ConvertTMemo2Task"

    const-string v9, "No free space on SD card."

    invoke-static {v7, v9}, Lcom/samsung/android/app/memo/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const v7, 0x7f0b004e

    invoke-direct {p0, v7}, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->showToast(I)V

    goto/16 :goto_3

    .line 165
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "res":J
    :cond_11
    iput-object v12, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->progressDialog:Lcom/samsung/android/app/migration/dialog/ProgressBarDialog;

    .line 166
    iput-boolean v8, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->isShown:Z

    goto/16 :goto_4

    .line 169
    :cond_12
    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    goto/16 :goto_5

    .line 173
    :cond_13
    iget-object v7, p0, Lcom/samsung/android/app/migration/task/ConvertTMemo2Task;->context:Landroid/app/Activity;

    goto/16 :goto_6
.end method
