.class public Lcom/samsung/android/app/memo/share/beam/MemoZipHelper;
.super Ljava/lang/Object;
.source "MemoZipHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;
    }
.end annotation


# static fields
.field public static final RESULT_FAIL:I = -0x1

.field public static final RESULT_SUCCESS:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static unZip(Lcom/samsung/android/app/memo/share/beam/MemoZipItem;Landroid/os/Handler;)V
    .locals 3
    .param p0, "memoMemoZipItem"    # Lcom/samsung/android/app/memo/share/beam/MemoZipItem;
    .param p1, "mHandler"    # Landroid/os/Handler;

    .prologue
    .line 38
    new-instance v0, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;

    invoke-direct {v0, p1}, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;-><init>(Landroid/os/Handler;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/samsung/android/app/memo/share/beam/MemoZipItem;

    const/4 v2, 0x0

    .line 39
    aput-object p0, v1, v2

    .line 38
    invoke-virtual {v0, v1}, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper$unZipTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 41
    return-void
.end method

.method private static zip(Lcom/samsung/android/app/memo/share/beam/MemoZipItem;)Ljava/lang/Boolean;
    .locals 11
    .param p0, "a_oItem"    # Lcom/samsung/android/app/memo/share/beam/MemoZipItem;

    .prologue
    .line 107
    const/4 v0, 0x1

    .line 110
    .local v0, "bResult":Z
    :try_start_0
    new-instance v4, Ljava/io/File;

    iget-object v7, p0, Lcom/samsung/android/app/memo/share/beam/MemoZipItem;->mstrZipFilename:Ljava/lang/String;

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 111
    .local v4, "oZipFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 112
    :cond_0
    new-instance v6, Lnet/lingala/zip4j/core/ZipFile;

    iget-object v7, p0, Lcom/samsung/android/app/memo/share/beam/MemoZipItem;->mstrZipFilename:Ljava/lang/String;

    invoke-direct {v6, v7}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 113
    .local v6, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    new-instance v5, Lnet/lingala/zip4j/model/ZipParameters;

    invoke-direct {v5}, Lnet/lingala/zip4j/model/ZipParameters;-><init>()V

    .line 114
    .local v5, "parameters":Lnet/lingala/zip4j/model/ZipParameters;
    const/16 v7, 0x8

    invoke-virtual {v5, v7}, Lnet/lingala/zip4j/model/ZipParameters;->setCompressionMethod(I)V

    .line 115
    const/4 v7, 0x5

    invoke-virtual {v5, v7}, Lnet/lingala/zip4j/model/ZipParameters;->setCompressionLevel(I)V

    .line 117
    new-instance v3, Ljava/io/File;

    iget-object v7, p0, Lcom/samsung/android/app/memo/share/beam/MemoZipItem;->m_strZipRootpath:Ljava/lang/String;

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 118
    .local v3, "oSource":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 119
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v7

    array-length v7, v7

    if-lez v7, :cond_1

    .line 120
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    array-length v9, v8
    :try_end_0
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v7, 0x0

    :goto_0
    if-lt v7, v9, :cond_2

    .line 133
    .end local v3    # "oSource":Ljava/io/File;
    .end local v4    # "oZipFile":Ljava/io/File;
    .end local v5    # "parameters":Lnet/lingala/zip4j/model/ZipParameters;
    .end local v6    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :cond_1
    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    return-object v7

    .line 120
    .restart local v3    # "oSource":Ljava/io/File;
    .restart local v4    # "oZipFile":Ljava/io/File;
    .restart local v5    # "parameters":Lnet/lingala/zip4j/model/ZipParameters;
    .restart local v6    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :cond_2
    :try_start_1
    aget-object v1, v8, v7

    .line 121
    .local v1, "child":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 122
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 123
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 124
    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v6, v10, v5}, Lnet/lingala/zip4j/core/ZipFile;->addFolder(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V

    .line 120
    :cond_3
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 125
    :cond_4
    invoke-virtual {v6, v1, v5}, Lnet/lingala/zip4j/core/ZipFile;->addFile(Ljava/io/File;Lnet/lingala/zip4j/model/ZipParameters;)V
    :try_end_1
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 129
    .end local v1    # "child":Ljava/io/File;
    .end local v3    # "oSource":Ljava/io/File;
    .end local v4    # "oZipFile":Ljava/io/File;
    .end local v5    # "parameters":Lnet/lingala/zip4j/model/ZipParameters;
    .end local v6    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :catch_0
    move-exception v2

    .line 130
    .local v2, "oException":Lnet/lingala/zip4j/exception/ZipException;
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static zip(Lcom/samsung/android/app/memo/share/beam/MemoZipItem;Landroid/os/Handler;)V
    .locals 3
    .param p0, "memoZipItem"    # Lcom/samsung/android/app/memo/share/beam/MemoZipItem;
    .param p1, "mHandler"    # Landroid/os/Handler;

    .prologue
    .line 44
    const/4 v1, 0x0

    .line 45
    .local v1, "result":Z
    if-eqz p0, :cond_0

    invoke-static {p0}, Lcom/samsung/android/app/memo/share/beam/MemoZipHelper;->zip(Lcom/samsung/android/app/memo/share/beam/MemoZipItem;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    const/4 v1, 0x0

    .line 48
    :goto_0
    if-eqz p1, :cond_1

    .line 49
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 50
    .local v0, "oMessage":Landroid/os/Message;
    if-eqz v1, :cond_3

    .line 51
    const/4 v2, 0x0

    iput v2, v0, Landroid/os/Message;->what:I

    .line 52
    iget-object v2, p0, Lcom/samsung/android/app/memo/share/beam/MemoZipItem;->m_strZipRootpath:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/app/memo/share/beam/XMLUtills;->deleteDir(Ljava/lang/String;)V

    .line 57
    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 59
    .end local v0    # "oMessage":Landroid/os/Message;
    :cond_1
    return-void

    .line 46
    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    .line 55
    .restart local v0    # "oMessage":Landroid/os/Message;
    :cond_3
    const/4 v2, -0x1

    iput v2, v0, Landroid/os/Message;->what:I

    goto :goto_1
.end method
