.class Lcom/samsung/android/app/migration/dialog/ImportDialog$4;
.super Ljava/lang/Object;
.source "ImportDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/migration/dialog/ImportDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/migration/dialog/ImportDialog;

.field private final synthetic val$ctx:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/migration/dialog/ImportDialog;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog$4;->this$0:Lcom/samsung/android/app/migration/dialog/ImportDialog;

    iput-object p2, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog$4;->val$ctx:Landroid/app/Activity;

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 109
    iget-object v1, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog$4;->this$0:Lcom/samsung/android/app/migration/dialog/ImportDialog;

    # getter for: Lcom/samsung/android/app/migration/dialog/ImportDialog;->mDoNotShowAgain:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/samsung/android/app/migration/dialog/ImportDialog;->access$0(Lcom/samsung/android/app/migration/dialog/ImportDialog;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 110
    .local v0, "doNotShowAgain":Z
    iget-object v1, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog$4;->val$ctx:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/samsung/android/app/migration/utils/SharedPref;->setImportDoNotShowFlag(Landroid/content/Context;Z)V

    .line 111
    iget-object v2, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog$4;->val$ctx:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 112
    const/4 v1, 0x3

    .line 111
    :goto_0
    invoke-static {v2, v1}, Lcom/samsung/android/app/migration/utils/SharedPref;->setTMemoImportDialogState(Landroid/content/Context;I)V

    .line 113
    iget-object v1, p0, Lcom/samsung/android/app/migration/dialog/ImportDialog$4;->this$0:Lcom/samsung/android/app/migration/dialog/ImportDialog;

    invoke-virtual {v1}, Lcom/samsung/android/app/migration/dialog/ImportDialog;->dismiss()V

    .line 114
    return-void

    .line 112
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
